<?php
require_once '../core/app/debuggin.php';
require_once '../core/app/defines.php';
require_once '../core/modules/index/model/DaoEnfoque.php';
require_once '../core/modules/index/model/DaoImagenHome.php';

$DaoImagen = new DaoImagenHome();
$method = $_GET['method'];

switch ($method) {
  case 'get_all':
    json($DaoImagen -> getAllVideosHome());
    break;
  case 'save':
    $DaoImagen -> saveVideoHome($_POST['videoshome']);
    json($DaoImagen -> response);
    break;
  case 'delete':
    $DaoImagen -> deleteVideoHome($_POST);
    json($DaoImagen -> response);
    break;

  default:
    # code...
    break;
}

 ?>
