<?php
require_once '../core/modules/index/model/DaoAportacion.php';
require_once '../core/modules/index/model/DaoCategoriaAportacion.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoAportacion = new DaoAportacion();
    $titulo = "";
    $descripcion = "";
    $thumbnail = "";
    $file = "";
    $idCategoria = 0;

    if (isset($_POST['id']) && $_POST['id'] > 0) {
        $aportacion = $DaoAportacion->getById($_POST['id']);
        $titulo = $aportacion->getTitulo();
        $descripcion = $aportacion->getDescripcion();
        $thumbnail = $aportacion->getThumbnail();
        $file = $aportacion->getFile();
        $idCategoria = $aportacion->getCategory();

    }
    ?>
    <div class="form-group">
        <label for="titulo">* Título:</label>
        <input type="text" class="form-control" id="titulo" required value="<?php echo $titulo;?>">
    </div>
    <div class="form-group">
        <label for="descripcion">* Descripción:</label>
        <input type="text" class="form-control" id="descripcion" required value="<?php echo $descripcion;?>">
    </div>
    <div class="form-group">
        <label for="descripcion">* Categoría:</label>
        <select class="form-control" id="categoria">
            <option value="0" <?php if($idCategoria == 0) echo 'selected'; ?>>No ha seleccionado una categoría</option>
            <?php
          $DaoCategoriaAportacion = new DaoCategoriaAportacion();
          // echo $DaoCategoriaAportacion->getAllopcion('Padres');
          foreach ($DaoCategoriaAportacion->getAllopcion('Hijos') as $category) {
          
          ?>
              <option value="<?php echo $category->id; ?>" <?php if($idCategoria == $category->id) echo 'selected'; ?>><?php echo $category->titulo; ?></option>
          <?php
              
          }
          ?>
        </select>
    
    </div>
    <div class="form-group">
        <label for="thumbnail">* Imagen:</label>
        <input type="text" class="form-control" id="thumbnail" required value="<?php echo $thumbnail;?>" readonly="readonly">
    </div>
    <div class="form-group">
        <label for="file">* Archivo:</label>
        <input type="text" class="form-control" id="file" required value="<?php echo $file;?>" readonly="readonly">
    </div>
    <?php if (!isset($_POST['id']) || $_POST['id'] <= 0) { ?>
    <div class="form-group">
        <div class="dropzone" id="sharePhoto"></div>
    </div>
    <?php } ?>
    <div class="form-group" style="margin-top:25px;">
        <button type="submit" class="btn btn-primary pull-right" onclick="saveAportacion()" >Guardar aportación</button>
    </div>
    <script>
        $(function(){

          Dropzone.autoDiscover = false;
            var fileList = new Array;
            var i =0;
            $("div#sharePhoto").dropzone({
                acceptedFiles : '.jpg, .png, .jpeg, .pdf, .doc , .docx, .pptx, .xls, .xlsx',
                addRemoveLinks: true,
                init: function() {

                    // Hack: Add the dropzone class to the element
                    $(this.element).addClass("dropzone");

                    this.on("success", function(file, serverFileName) {
                        fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };

                        if(file.type.startsWith('image')){
                            $('#thumbnail').val(fileList[i].serverFileName);
                        }else{
                            $('#file').val(fileList[i].serverFileName);
                        }
                        
                        i++;

                    });
                    this.on("removedfile", function(file) {
                        var rmvFile = "";
                        for(f=0;f<fileList.length;f++){

                            if(fileList[f].fileName == file.name)
                            {
                                rmvFile = fileList[f].serverFileName;
                            }
                        }
                        if (rmvFile){
                            $.ajax({
                                url: "../api/front/a-deleteitems.php",
                                type: "POST",
                                data: { "fileList" : rmvFile }
                            });
                        }
                    });
                },
                url: "../api/front/a-uploads.php"
            });

        });
    </script>
<?php
}
?>