<?php
require_once 'includes/header.php';
?>
<div class="container-fluid">
  <!-- Nav tabs -->
  <!-- ================================================================= -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-picture-o"></i> &nbsp; Imágenes</a></li>
    <li role="presentation"><a href="#container-job" aria-controls="container-job" role="tab" data-toggle="tab"><i class="fa fa-video-camera"></i> &nbsp; Videos</a></li>
  </ul>
  <hr>
  <!-- Tab panes -->
  <!-- ================================================================= -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">
      <div class="Wall margin-row-bottom z-depth-1">
        <div class="Wall__header z-depth-1">
          <h3 class="display-inline text-white bolder"><i class="fa fa-picture-o"></i> Catálogo de imágenes</h3>
          <div class="pull-right" style="margin-top:-3px">
            <a href="imagen.php" class="btn btn-outline-primary">
              <i class="fa fa-plus"></i> Agregar Nuevo
            </a>
          </div>
        </div>
        <div class="Wall__content large padding">
          <div class="row margin-row-bottom">
            <div class="pull-right">
              <form class="form-inline" style="margin-right: 14px;">
                <div class="form-group">
                  <input type="text" class="form-control" id="buscar" placeholder="Buscar" onkeyup="buscador()">
                </div>
              </form>
            </div>
          </div>
          <div class="table-responsive" id="box-table-imagenes"></div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="container-job">
      <div class="Wall margin-row-bottom z-depth-1">
        <div class="Wall__header z-depth-1">
          <h3 class="display-inline text-white bolder"><i class="fa fa-list"></i> Catálogo de videos</h3>
          <div class="pull-right" style="margin-top:-3px">
            <button type="button" class="btn btn-outline-primary open-modal" data-id="">
              <i class="fa fa-plus"></i> Agregar Nuevo
            </button>
          </div>
        </div>
        <div class="Wall__content large padding">
          <div class="table-responsive">
            <table class="table table-bordered hidden-xs" id="video-slider">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Video</th>
                  <th>Descripción</th>
                  <th>En portada</th>
                  <th class="center">Actividades</th>
                </tr>
              </thead>
              <tbody id="lista-videos">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
require_once 'includes/footer.php';
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"></div>
<!-- Modal -->
<div class="modal fade" id="modal-job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-job-title">Modal title</h4>
      </div>
      <div class="modal-body" id="modal-job-body">
        ...
      </div>
    </div>
  </div>
</div>
<script>
  //modal selectors
  $modal = $('#modal-job');
  $modalTitle = $('#modal-job-title');
  $modalBody = $('#modal-job-body');
  //triger
  $containerJob = $('#container-job');
  $(function(){
    /* Load table*/
    updatePageVideos();
    //HANDLER EVENT

    /* Save and update open modal */
    $containerJob.on('click', '.open-modal', function (){
      // Open de modal
      var id = $(this).data('id');
      // clean the modal
      var title = (!id) ? '<i class="fa fa-plus"></i>&nbsp; Nuevo ' : '<i class="fa fa-pencil"></i>&nbsp; Editar ';
      $modalTitle.empty().html(title);
      $modalBody.empty().load('form_videos_home.php',{id: id});
      $modal.modal({
        backdrop: 'static',
        keyboard: true
      });
    });
    /* save and update form */
    $modalBody.on('submit', '#form-tipo-enfoque', function(e){
      e.preventDefault();
      var data = new FormData(),
      form_data = $(this).serializeArray();

      //files
       $.each($(this).find("input[type='file']"), function(it, file) {
         data.append($(this).attr('name'), file.files[0]);
       });

      $.each(form_data, function(i, val) {
        data.append(val.name, val.value);
      });

      $.ajax({
        url: 'ajax_videos_home.php?method=save',
        data: data,
        type: 'POST',
        // THIS MUST BE DONE FOR FILE UPLOADING
        contentType: false,
        processData: false,
        // ... Other options like success and etc
        success: function(response){
          if(response.status){
            //success
            $modal.modal('hide');
            updatePageVideos();
          }
          setFlash(response.msg, response.class);
        }
      });

    });

    // delete
    $containerJob.on('click', '.delete-tipo', function (){
      // Open de modal
      var id = $(this).data('id');
      if(!id){
        setFlash('Al parecer este elemento no puede eliminarse :/', 'info');
        return false;
      }
      if ( confirm("¿Desea borrar el video seleccionado?") ) {
        $.post('ajax_videos_home.php?method=delete',{id: id}, function(response){
          if(response.status){
            //success
            updatePageVideos();
          }
          setFlash(response.msg, response.class);
        });
      }
    });
  });

  function updatePageVideos(offset){
    $.getJSON("ajax_videos_home.php?method=get_all",function(response){
      var rows = '';
      $.each(response, function(index, value){
        rows += '<tr>' +
          '<td>' + value.idVideosHome + '</td>' +
          '<td>' +
            '<video width="320" height="240" controls>' +
              '<source src="files/videos-home/' + value.ruta + '" type="video/mp4">' +
            '</video>' +
          '</td>' +
          '<td>' + value.descripcion + '</td>' +
          '<td>' + value.portada + '</td>' +
          '<td class="center">' + value.acciones + '</td>' +
        '</tr>';
      });
      $('#lista-videos').empty().html(rows);
      $('#video-slider').stacktable({myClass:'visible-xs'});
    })
  }
</script>
