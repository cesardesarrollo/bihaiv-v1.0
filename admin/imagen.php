<?php
require_once 'includes/header.php';
require_once '../core/modules/index/model/DaoImagenHome.php';
require_once '../core/modules/index/model/DTO/ImagenHome.php';
?>
<div class="container-fluid">
    <div class="Wall margin-row-bottom">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-picture-o"></i> Imagen</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="imagenes.php" class="btn btn-outline-primary">
                    <i class="fa fa-reply" aria-hidden="true"></i> <span class="hidden-xs">Regresar</span>
                </a>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12" id="box-principal"></div>
            </div>
        </div>
    </div>
</div>
<?php
$llave="";
$id="";
if(isset($_REQUEST['id']) && $_REQUEST['id']>0){
  $DaoImagenHome= new DaoImagenHome();
  $imagen = $DaoImagenHome->getById($_REQUEST['id']);
  $llave=$imagen->getLlaveImg();
  $id=$imagen->getId();
}
?>
<input type="hidden" id="id-imagen" value="<?php echo $id?>"/>
<input type="hidden" id="llave-imagen" value="<?php echo $llave;?>"/>
<!--<input type="file" id="files" name="files[]" multiple="">-->
<?php
require_once 'includes/footer.php';
?>
