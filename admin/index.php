<?php
require_once 'includes/header.php';
?>
    <style>
        .modal-backdrop {
            z-index: 0;
        }
    </style>
    <div class="container">
        <h1>Bienvenidos</h1>
        <?php if( isset($_SESSION['is_admin']) AND $_SESSION['is_admin'] ===  true ): ?>
        <div class="well">
            <h3>Configuración de administrador</h3>
            <button class="btn btn-outline-success"  onclick="showModal()" data-toggle="tooltip" title="Modificar correo/usuario de administrador (destino de solicitudes de expertos)"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>           
        </div>
        <?php endif; ?>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"></div>
<?php
require_once 'includes/footer.php';
?>

<script>
    // Configura el correo a donde son enviados las solicitudes de expertos
    function showModal(){
        var html = '<div  style="z-index: 9999;" class="modal-dialog" role="document">' + 
                        '<div class="modal-content">' + 
                            '<div class="modal-header">' + 
                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + 
                            '<h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Configurar correo de administrador de solicitudes</h4>' + 
                            '</div>' + 
                            '<div class="modal-body">' + 
                            '<form>' + 
                                '<div class="form-group">' + 
                                '<label for="email" class="control-label">Email:</label>' + 
                                '<input type="email" class="form-control" id="email">' + 
                                '</div>' + 
                                '<div class="form-group">' + 
                                '<label for="email" class="control-label">Repetir Email:</label>' + 
                                '<input type="email" class="form-control" id="email2">' + 
                                '</div>' + 
                            '</form>' + 
                            '</div>' + 
                            '<div class="modal-footer">' + 
                            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>' + 
                            '<button type="button" class="btn btn-primary" onclick="saveEmailConfig()">Guardar</button>' + 
                            '</div>' + 
                        '</div>' + 
                    '</div>';

        $("#myModal").modal({
            show : true,
            backdrop : 'static',
            keyboard : false
        })
        $("#myModal").html(html)

    }

    // Configura el correo a donde son enviados las solicitudes de expertos
    function saveEmailConfig() {
        var params = new Object()
        params.method = "update_admin_email"
        params.email = $('#email').val();
        var email2 = $('#email2').val();
        var error = 0
        var errores = new Array()

        if ($('#email').val().length == 0) {
            error = "Email requerido"
            errores.push(error);
        }
        if ($('#email2').val().length == 0) {
        error = "Repite el email antes de continuar"
        errores.push(error);
        }

        if($('#email').val() != $('#email2').val()){
        error = "Los email deben coincidir, intentalo de nuevo."
        errores.push(error);
        }

        if (errores.length == 0) {
            $.post("../api/admin/expertos.php", params, function (resp) {
                if(resp[0]){
                    setFlash("Correo/usuario de administrador ha sido modificado", 'info');
                    ocultarModal();
                } else {
                    setFlash("El correo no fue posible establecer, es posible que este ya exista en la base de datos", 'warning');
                    ocultarModal();
                }
            },"json")
        } else {
            setFlash(errores.toString(), 'danger')
        }
    }
</script>
