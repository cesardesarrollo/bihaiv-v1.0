<?php
require_once 'includes/header.php';
?>
<div class="container-fluid">
    <div class="Wall margin-row-bottom">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-users"></i> Tipo de enfoque</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="tipos_enfoque.php" class="btn btn-outline-primary">
                    <i class="fa fa-reply" aria-hidden="true"></i> Regresar
                </a>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12" id="box-rol">
                  <?php
                  require_once '../core/app/debuggin.php';
                  require_once '../core/modules/index/model/DaoEnfoque.php';

                  $DaoEnfoque = new DaoEnfoque();
                  $id = $nombre = $descripcion = null;
                  if( isset($_GET['id']) AND !empty($_GET['id']) AND is_numeric($_GET['id']) AND $_GET['id'] > 0 ){
                    $id = $_GET['id'];
                    $resultSet = $DaoEnfoque -> getOneRow("SELECT * FROM tipo_enfoque WHERE id = $id");
                    if( !empty($resultSet) ){
                      $nombre = $resultSet['nombre'];
                      $descripcion = $resultSet['descripcion'];
                    }
                  }
                   ?>
                  <form id="form-tipo-enfoque">
                    <input type="hidden" name="tipo_enfoque[id]" value="<?= $id; ?>">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tipo de enfoque *</label>
                      <input name="tipo_enfoque[nombre]" type="text" class="form-control" placeholder="Tipo de enfoque..." value="<?= $nombre; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Descripción</label>
                      <textarea name="tipo_enfoque[descripcion]" class="form-control" rows="3" placeholder="Descripción del tipo de enfoque"><?= $descripcion; ?></textarea>
                    </div>
                    <div class="pull-right">
                      <button type="tipo_enfoque[submit]" class="btn btn-primary">Guardar</button>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once 'includes/footer.php'; ?>
<script>
  $(function(){
    $('#form-tipo-enfoque').on('submit', function(e){
      e.preventDefault();
      $.post('ajax_tipo_enfoque.php?method=save', $(this).serialize(),function(response){
        if(response.status){
          setFlash(response.msg, response.class);
          setTimeout(function(){
            window.location.replace("tipos_enfoque.php");
           }, 1500);
        }else{
          setFlash(response.msg, response.class);
        }
      });
    });
  })
</script>
