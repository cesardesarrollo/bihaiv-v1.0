<?php


require_once 'includes/header.php';
?>
<div class="container-fluid">
    <div class="row margin-row-bottom">
        <div class="pull-right">
            <form class="form-inline" style="margin-right: 14px;">
                <div class="form-group">
                    <input type="text" class="form-control" id="buscar" placeholder="Buscar" onkeyup="buscador()">
                </div>
            </form>
        </div>
    </div>
    <div class="Wall margin-row-bottom z-depth-1">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-certificate"></i> Catálogo de expertos</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="experto.php" class="btn btn-outline-primary" data-toggle="tooltip" title="Agregar Nuevo">
                    <i class="fa fa-plus"></i> <span class="hidden-xs hidden-sm">Agregar Nuevo</span>
                </a>
                <button class="btn btn-outline-primary"  onclick="getFormInvitacion()" data-toggle="tooltip" title="Invitar experto">
                  <i class="fa fa-paper-plane-o" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Invitar experto</span>
                </button>
             </div>
        </div>
        <div class="Wall__content large padding">
            <div class="table-responsive" id="box-table-expertos">
            </div>
        </div>
    </div>
</div>
<?php
require_once 'includes/footer.php';
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"></div>
