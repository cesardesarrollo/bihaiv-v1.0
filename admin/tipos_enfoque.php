<?php
require_once 'includes/header.php';
?>
<div class="container-fluid" id="container-job">
  <div class="Wall margin-row-bottom z-depth-1">
    <div class="Wall__header z-depth-1">
      <h3 class="display-inline text-white bolder"><i class="fa fa-list"></i> Catálogo de tipos de enfoque</h3>
      <div class="pull-right" style="margin-top:-3px">
        <a href="form_tipo_enfoque.php" class="btn btn-outline-primary">
          <i class="fa fa-plus"></i> Agregar Nuevo
        </a>
      </div>
    </div>
    <div class="Wall__content large padding">
      <div class="table-responsive">
        <table class="table table-bordered hidden-xs">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Descripción</th>
              <th class="center">Actividades</th>
            </tr>
          </thead>
          <tbody id="lista-tipos-enfoques">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php
require_once 'includes/footer.php';
?>
<!-- Modal -->
<div class="modal fade" id="modal-job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-job-title">Modal title</h4>
      </div>
      <div class="modal-body" id="modal-job-body">
        ...
      </div>
    </div>
  </div>
</div>
<script>
  //modal selectors
  $modal = $('#modal-job');
  $modalTitle = $('#modal-job-title');
  $modalBody = $('#modal-job-body');
  //triger
  $containerJob = $('#container-job');

  $(function(){
    /* Load table*/
    updatePage();
    //handler event
    // Save and update open modal
    $containerJob.on('click', '.open-modal', function (){
      // Open de modal
      var id = $(this).data('id');
      // clean the modal
      var title = (!id) ? '<i class="fa fa-plus"></i>&nbsp; Nuevo ' : '<i class="fa fa-pencil"></i>&nbsp; Editar ';
      $modalTitle.empty().html(title);
      $modalBody.empty().load('form_tipo_enfoque.php',{id: id});
      $modal.modal({
        backdrop: 'static',
        keyboard: true
      });
    });
    // delete
    $containerJob.on('click', '.delete-tipo', function (){
      // Open de modal
      var id = $(this).data('id');
      if(!id){
        setFlash('Al parecer este elemento no puede eliminarse :/', 'info');
        return false;
      }
      if ( confirm("¿Desea borrar el tipo de enfoque seleccionado?") ) {
        $.post('ajax_tipo_enfoque.php?method=delete',{id: id}, function(response){
          if(response.status){
            //success
            updatePage();
          }
          setFlash(response.msg, response.class);
        });
      }
    });
  });

  function updatePage(offset){
    $.getJSON("ajax_tipo_enfoque.php?method=get_all",function(response){
      var rows = '';
      $.each(response, function(index, value){
        rows += '<tr>' +
          '<td>' + value.id + '</td>' +
          '<td>' + value.nombre + '</td>' +
          '<td>' + value.descripcion + '</td>' +
          '<td class="center">' + value.acciones + '</td>' +
        '</tr>';
      });
      $('#lista-tipos-enfoques').empty().html(rows);
      $('.table-bordered').stacktable({myClass:'visible-xs'});
    })
  }
</script>
