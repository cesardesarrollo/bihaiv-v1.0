<?php
require_once '../core/modules/index/model/DaoCuestionario.php';
require_once '../core/modules/index/model/DaoPregunta.php';
require_once '../core/modules/index/model/DaoOpcionesRespuesta.php';
require_once '../core/modules/index/model/DTO/Cuestionario.php';
require_once '../core/modules/index/model/DaoRoles.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoCuestionario = new DaoCuestionario();
    $DaoOpcionesRespuesta = new DaoOpcionesRespuesta();
    $DaoPregunta = new DaoPregunta();
    $DaoRoles = new DaoRoles();

    $nombre = "";
    $nivel = "-1";
    $activo = "1";
    if (isset($_POST['id']) && $_POST['id'] > 0) {
        $cuestionario = $DaoCuestionario->getById($_REQUEST['id']);
        $nombre = $cuestionario->getNombre();
        $nivel = $cuestionario->getNivel();
        $activo = $cuestionario->getActivo();
    }
    ?>
    <div class="Wall margin-row-bottom">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-list-ol"></i> Cuestionario</h3>
            <div class="pull-right" style="margin-top:-3px">
                <a href="cuestionarios.php" class="btn btn-outline-primary">
                    <i class="fa fa-reply" aria-hidden="true"></i> Regresar
                </a>
            </div>
        </div>
        <div class="Wall__content large padding">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <form>
                        <div class="form-group">
                            <label for="txt__Name">Nombre:</label>
                            <input type="text" class="form-control" id="txt__Name" placeholder="Nombre del cuestionario" required value="<?php echo $nombre ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nivel:</label>
                            <select class="form-control" id="nivel">
                                <option value="0">Seleccione una opcion</option>
                                <option value="1" <?php if ($nivel == 1) { ?> selected="selected" <?php } ?> >Perfilamiento</option>
                                <option value="2" <?php if ($nivel == 2) { ?> selected="selected" <?php } ?> >Validación por unidad</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Activo:</label>
                            <select class="form-control" id="activo">
                                <option value="0" <?php if ($activo == 0) { ?> selected="selected" <?php } ?> >Desactivado</option>
                                <option value="1" <?php if ($activo == 1) { ?> selected="selected" <?php } ?> >Activado</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-info <?php if($nivel == 1) echo 'hidden'; ?>" id="msg_preguntas" role="alert">
        <i class="fa fa-info-circle"></i> Los cuestionarios que no pertenezcan al nivel de perfilamiento no llevan preguntas.
    </div>
    <div class="Wall margin-row-bottom <?php if($nivel != 1) echo 'hidden'; ?>" id="wall_preguntas_respuestas">
        <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder">Preguntas</h3>
            <div class="pull-right btn btn-outline-primary" style="margin-top:-3px" id="addPregunta" onclick="addPregunta()">
                <i class="fa fa-plus"></i> Agregar Pregunta
            </div>
        </div>
        <div class="Wall__content large padding">
            <div id="questions">
                <div class="row margin-row-bottom box-preguntas">
                    <?php
                    if (isset($_POST['id']) && $_POST['id'] > 0) {
                        $indexPre = 1;
                        foreach ($DaoPregunta->getPreguntasCuestionario($_POST['id']) as $pregunta) {
                            ?>
                            <div class="box-pregunta" id-pre="<?php echo $pregunta->getId() ?>">
                                <div class="col-xs-12 col-lg-12">
                                    <div class="form-group col-xs-11 col-lg-11">
                                        <label for="txt__Pregunta_<?php echo $pregunta->getId() ?>">Pregunta <?php echo $indexPre ?>:</label>
                                        <input type="text" class="form-control pre" id="txt__Pregunta_<?php echo $indexPre ?>" placeholder="Pregunta <?php echo $indexPre ?>" value="<?php echo $pregunta->getPregunta() ?>">
                                    </div>
                                    <div class="form-group col-xs-1 col-lg-1">
                                        <button class="btn btn-primary delete-pre"  onclick="deletePregunta(<?php echo $pregunta->getId()?>,this)">Eliminar</button>
                                    </div>
                                </div>
                                <div class="box-respuestas pre_<?php echo $pregunta->getId() ?>">
                                    <?php
                                    $opciones = $DaoOpcionesRespuesta->getOpcionesByIdPregunta($pregunta->getId());
                                    $indexResp = 1;
                                    foreach ($opciones as $opcion) {
                                        ?>
                                        <div class="col-xs-12 col-lg-12 respuesta" id-resp="<?php echo $opcion->getId() ?>" >
                                            <div class="col-xs-6 col-sm-6 col-lg-6">
                                                <div class="form-group">
                                                    <label for="txt__resp_<?php echo $opcion->getId() ?>">Respuesta <?php echo $indexResp; ?>:</label>
                                                    <input type="text" class="form-control resp" id="txt__resp_<?php echo $opcion->getId() ?>" placeholder="Respuesta " value="<?php echo $opcion->getTexto() ?>">
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5 col-lg-5">
                                                <div class="form-group">
                                                    <label for="txt__Rol_<?php echo $opcion->getId() ?>">Rol:</label>
                                                    <select class="form-control roles guardados" id-select-rol="<?php echo $opcion->getId() ?>">
                                                        <option value="0"></option>
                                                        <?php
                                                        foreach ($DaoRoles->getAll() as $rol) {
                                                            ?>
                                                            <option value="<?php echo $rol->getId() ?>" <?php if ($rol->getId() == $opcion->getRoles_idRol()) { ?> selected="selected" <?php } ?>><?php echo $rol->getNombre() ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-xs-1 col-lg-1">
                                                <button class="btn btn-primary delete-pre"  onclick="deleteRespuesta(<?php echo $opcion->getId() ?>,this)">Eliminar</button>
                                            </div>
                                        </div>
                                        <?php
                                        $indexResp++;
                                    }
                                    ?>
                                </div>
                                <div class="col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary addRespuesta pull-right" onclick="addRespuesta(this)">Agregar respuesta</button>
                                    </div>
                                </div>
                            </div> 
                            <?php
                            $indexPre++;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <br />
    <button type="submit" class="btn btn-primary pull-right" id="saveForm" onclick="saveCuestionario()">Guardar cuestionario</button>
    <?php
}

if ($_POST['action'] == "addPregunta") {
    ?>
    <div class="box-pregunta">
        <div class="col-xs-12 col-lg-12 x-pre">
            <div class="form-group col-xs-11 col-lg-11">
                <label for="txt__Pregunta_0">Pregunta:</label>
                <input type="text" class="form-control pre" id="txt__Pregunta_0" placeholder="Pregunta ">
            </div>
            <div class="form-group col-xs-1 col-lg-1">
                <button class="btn btn-primary delete-pre"  onclick="deletePre(this)">Eliminar</button>
            </div>
        </div>
        <div class="box-respuestas">
        </div>
        <div class="col-xs-12 col-lg-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary addRespuesta pull-right" onclick="addRespuesta(this)">Agregar respuesta</button>
            </div>
        </div>
    </div>
    <?php
}

if ($_POST['action'] == "addRespuesta") {
    $DaoRoles = new DaoRoles();
    ?>
    <div class="col-xs-12 col-lg-12 respuesta">
        <div class="col-xs-6 col-sm-6 col-lg-6">
            <div class="form-group">
                <label for="txt__Pregunta_">Respuesta:</label>
                <input type="text" class="form-control" id="txt__resp_" placeholder="Respuesta ">
            </div>
        </div>
        <div class="col-xs-5 col-sm-5 col-lg-5">
            <div class="form-group">
                <label for="txt__resp_">Rol:</label>
                <select class="form-control roles new-rol">
                    <option value="0"></option>
                    <?php
                    foreach ($DaoRoles->getAll() as $rol) {
                        ?>
                        <option value="<?php echo $rol->getId() ?>"><?php echo $rol->getNombre() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group col-xs-1 col-lg-1">
            <button class="btn btn-primary delete-pre"  onclick="deleteRes(this)">Eliminar</button>
        </div>
    </div>
    <?php
}