</div>
</div><!--END SITE CANVAS-->
</div>


<div id="modal-launcher"></div>

<!--SCRIPTS-->
<script src="../res/jquery/jquery.min.js"></script>
<script src="../res/toast.js"></script>
<script src="../res/bootstrap/js/bootstrap.min.js"></script>
<script src="../res/owl/owl.carousel.min.js"></script>
<script src="../res/handlebars/handlebars.js"></script>
<script src="../res/callendar/lib/moment.min.js"></script>
<script src="../res/callendar/lib/jquery-ui.custom.min.js"></script>
<script src="../res/callendar/fullcalendar.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/scripts.js"></script>
<script src="js/dropzone.js"></script>
<script src="../assets/js/functions.js"></script>

<script src="../res/fileinput/js/fileinput.min.js"></script>
<script src="../res/fileinput/js/locales/es.js"></script>

<script src="../res/stacktable.min.js"></script>
<?php
if (file_exists("js/" . $pagina . ".js")) {
    ?>
    <script src="js/<?php echo $pagina; ?>.js?a=2"></script>
    <?php
}
?>
<script>
    $(function () {

        // Toggle Nav on Click
        $('.toggle-nav').click(function () {
            // Calling a function in case you want to expand upon this.
            toggleNav();
        });
        $('body').tooltip({
          selector: '[data-toggle="tooltip"]'
        });
    });


    /*========================================
     =            CUSTOM FUNCTIONS            =
     ========================================*/
    function toggleNav() {
        if ($('#site-wrapper').hasClass('show-nav')) {
            // Do things on Nav Close
            $('#site-wrapper').removeClass('show-nav');
        } else {
            // Do things on Nav Open
            $('#site-wrapper').addClass('show-nav');
        }

        //$('#site-wrapper').toggleClass('show-nav');
    }
</script>
<script>
  var opts = {
    "closeButton" : true,
    "debug" : false,
    "positionClass" : "toast-bottom-right",
    "onclick" : null,
    "showDuration" : "600",
    "hideDuration" : "1300",
    "timeOut" : "7500",
    "extendedTimeOut" : "1000",
    "showEasing" : "swing",
    "hideEasing" : "linear",
    "showMethod" : "fadeIn",
    "hideMethod" : "fadeOut"
  };
  function set_flash(msg, clase){
    switch (clase){
      case 'danger' :
      toastr.error(msg, '¡Error!', opts);
      break;
      case 'error' :
      toastr.error(msg, '¡Error!', opts);
      break;
      case 'success' :
      toastr.success(msg, '¡Perfecto!', opts);
      break;
      case 'warning' :
      toastr.warning(msg, 'Atención', opts);
      break;
      default :
      toastr.info(msg, 'Mensaje', opts);
      break;
    }
  }
  function setFlash(msg, clase){
    set_flash(msg, clase);
  }
</script>
<!-- menu movil -->
<script>
  $(function(){
    var abrirMenu = $('#trigger-menu');
    $menuMovil = $('.pink-menu-movil')
    abrirMenu.on('click', function(){
      // Lo cierra
      if( $menuMovil.hasClass('animated slideInLeft') ){
        $menuMovil.addClass('animated slideOutLeft');
        window.setTimeout( function(){
          $menuMovil.hide();
          $menuMovil.removeClass('animated slideInLeft');
          $menuMovil.removeClass('animated slideOutLeft');
        }, 900);
      }else{
        $menuMovil.show();
        $menuMovil.addClass('animated slideInLeft');
      }
    });

    $("#trigger-menu-close").click(function() {
      $menuMovil.addClass('animated slideOutLeft');

      window.setTimeout( function(){
        $menuMovil.hide();
        $menuMovil.removeClass('animated slideInLeft');
        $menuMovil.removeClass('animated slideOutLeft');
      }, 900);

    });
  });
</script>
</body>
</html>