<?php
session_start();
require_once '../core/app/defines.php';
require_once '../core/app/debuggin.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/vendor/phpmailer/class.phpmailer.php';
require_once '../core/controller/Cuestionario.php';
require_once '../core/modules/index/model/UserData.php';

require_once '../core/modules/index/model/DaoExperto.php';

require_once 'includes/access.php';

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$DaoExperto = new DaoExperto();
$experto_id = "";

$pagina = $_SERVER['SCRIPT_FILENAME'];
$pagina = substr($pagina, 0, strpos($pagina, ".php"));
while (strpos($pagina, "/") !== false) {
    $pagina = substr($pagina, strpos($pagina, "/") + 1);
}
if (!isset($_SESSION["user_id"]) && !$_SESSION["user_id"] > 0) {
    header("Location: ../home/login.php");
}else{
    $UserData = new UserData();
    $user = $UserData->getById($_SESSION["user_id"]);
    if( empty($user -> is_admin)  ){
      
      if(isset($_SESSION['is_expert']) AND $_SESSION['is_expert'] ==  false ){

        header("Location: ../home/login.php");
        exit();

      } else if(isset($_SESSION['is_expert']) AND $_SESSION['is_expert'] ==  true ){

        $experto = $DaoExperto->getExpertByUserId($_SESSION["user_id"]);
        $experto_id = $experto->id;

      }  
    } 
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Bihaiv - Panel de control</title>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../res/icons/css/font-awesome.min.css">
    <link rel="stylesheet" href="../res/toast.css">
    <link rel="stylesheet" href="../res/animate/animate.css">
    <link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../res/owl/assets/owl.carousel.css">
    <link rel="stylesheet" href="../res/callendar/fullcalendar.min.css">
    <link rel="stylesheet" href="../assets/css/base.css">
    <link rel="stylesheet" href="css/style.css?a=2">
    <link rel="stylesheet" href="../res/fileinput/css/fileinput.min.css">
    <?php if (file_exists("css/" . $pagina . ".css")): ?>
      <link rel="stylesheet" href="css/<?php echo $pagina; ?>.css?a=2">
    <?php endif; ?>
    <style>
      ::-webkit-scrollbar {
        display: none;
      }
    </style>
  </head>
  <body>
    <section class="Profile__header z-depth-1">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="pull-left visible-xs visible-sm">
              <img src="../assets/img/home/admin/menu.png" id="trigger-menu" class="hamburguer-menu" alt="Bihaiv" height="50">
            </div>
            <img src="../assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="50px">
            <div class="pull-right">
              <a href="logout.php" class="btn btn-primary" id="dLabel" type="button" data-toggle="tooltip" title="Cerrar sesión">
                <i class="fa fa-sign-out visible-xs" aria-hidden="true"></i> <span class="hidden-xs" >Cerrar sesión</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--SITE WRAPPER-->
    <div class="site-wrapper">
      <div class="site-canvas">
        <div class="site-menu hidden-xs hidden-sm">
          <!-- IMPORTANTE! LOS LINKS QUE ACTUALICES EN EL MENU TAMBIEN
          ACTUALIZALOS EN EL MENU MOVIL, SON INDEPENDIENTES -->
          <div class="Menu">
            <div class="Menu__list" style="height: 100%;max-height: 100%;">
              <ul>
                <li class="<?=haveAccess("index.php")?>">
                  <a class="home" href="index.php">
                    <i class="fa fa-home"></i>Inicio
                  </a>
                </li>
                <li class="<?=haveAccess("cuestionarios.php")?>">
                  <a class="client" href="cuestionarios.php">
                    <i class="fa fa-list-ol"></i>Cuestionarios
                  </a>
                </li>
                <li class="<?=haveAccess("experto.php")?>">
                  <a class="supplier" href="<?php  echo (isset($_SESSION['is_expert']) AND $_SESSION['is_expert'] ===  true ) ?  'experto.php?id='.$experto_id : 'expertos.php';?>">
                    <i class="fa fa-certificate"></i><?php  echo (isset($_SESSION['is_expert']) AND $_SESSION['is_expert'] ===  true ) ?  'Perfil' : 'Expertos';?>
                  </a>
                </li>
                <li class="<?=haveAccess("actores.php")?>">
                  <a class="supplier" href="actores.php">
                    <i class="fa fa-users"></i>Actores
                  </a>
                </li>
                <li class="<?=haveAccess("roles.php")?>">
                  <a class="supplier" href="roles.php">
                    <i class="fa fa-users"></i>Roles
                  </a>
                </li>
                <li class="<?=haveAccess("tipos_enfoque.php")?>">
                  <a class="supplier" href="tipos_enfoque.php">
                    <i class="fa fa-list"></i>Tipos de enfoque
                  </a>
                </li>
                <li class="<?=haveAccess("imagenes.php")?>">
                  <a class="product" href="imagenes.php">
                    <i class="fa fa-picture-o"></i>Multimedia
                  </a>
                </li>
                <li class="<?=haveAccess("videos.php")?>">
                  <a class="service" href="videos.php">
                    <i class="fa fa-file-video-o"></i>Videos
                  </a>
                </li>
                <li class="<?=haveAccess("reportes.php")?>">
                  <a class="service" href="reportes.php">
                    <i class="fa fa-times"></i>Reportados
                  </a>
                </li>
                <li class="<?=haveAccess("aportaciones.php")?>">
                  <a class="service" href="aportaciones.php">
                    <i class="fa fa-book"></i>Biblioteca
                  </a>
                </li>
                <li class="<?=haveAccess("aportaciones_bihaiv.php")?>">
                  <a class="service" href="aportaciones_bihaiv.php">
                    <i class="fa fa-book"></i>Aportaciones de Bihaiv
                  </a>
                </li>
                <li class="<?=haveAccess("categorias_aportacion.php")?>">
                  <a class="service" href="categorias_aportacion.php">
                    <i class="fa fa-bookmark"></i>Categorías de aportación
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div><!--END SITE MENU-->
        <!-- MENU MOVIL -->
        <!-- IMPORTANTE! LOS LINKS QUE ACTUALICES EN EL MENU MOVIL TAMBIEN
        ACTUALIZALOS EN EL MENU NORMAL, SON INDEPENDIENTES -->
        <div class="pink-menu-movil z-depth-2 hidden-md hidden-lg">
          <div class="header-menu">
            <h1 class="menu-pink-title">MENU</h1>
            <a href="#" id="trigger-menu-close">x</a>
          </div>
          <ul>
            <li class="<?=haveAccess("index.php")?>">
              <a href="index.php"><i class="fa fa-home"></i>&nbsp; Inicio</a>
            </li>
            <li class="<?=haveAccess("cuestionarios.php")?>">
              <a href="cuestionarios.php"><i class="fa fa-list-ol"></i>&nbsp; Cuestionarios</a>
            </li>
            <li class="<?=haveAccess("expertos.php")?>">
              <a href="expertos.php"><i class="fa fa-certificate"></i>&nbsp; Expertos</a>
            </li>
            <li class="<?=haveAccess("actores.php")?>">
              <a href="actores.php"><i class="fa fa-users"></i>&nbsp; Actores</a>
            </li>
            <li class="<?=haveAccess("roles.php")?>">
              <a href="roles.php"><i class="fa fa-users"></i>&nbsp; Roles</a>
            </li>
            <li class="<?=haveAccess("tipos_enfoque.php")?>">
              <a href="tipos_enfoque.php"><i class="fa fa-list"></i>&nbsp; Tipos de enfoque</a>
            </li>
            <li class="<?=haveAccess("imagenes.php")?>">
              <a href="imagenes.php"><i class="fa fa-picture-o"></i>&nbsp; Multimedia</a>
            </li>
            <li class="<?=haveAccess("videos.php")?>">
              <a href="videos.php"><i class="fa fa-file-video-o"></i>&nbsp; Videos</a>
            </li>
            <li class="<?=haveAccess("reportes.php")?>">
              <a href="reportes.php"><i class="fa fa-times"></i>&nbsp; Reportados</a>
            </li>
            <li class="<?=haveAccess("aportaciones.php")?>">
              <a href="aportaciones.php"><i class="fa fa-book"></i>&nbsp; Biblioteca</a>
            </li>
            <li class="<?=haveAccess("aportaciones_bihaiv.php")?>">
              <a href="aportaciones_bihaiv.php"><i class="fa fa-book"></i>&nbsp; Aportaciones de Bihaiv</a>
            </li>
            <li class="<?=haveAccess("categorias_aportacion.php")?>">
              <a href="categorias_aportacion.php"><i class="fa fa-bookmark"></i>&nbsp; Categorías de aportación</a>
            </li>
          </ul>
        </div>
        <!-- end menu movil -->
        <div class="message-alert z-depth-3">
          <div class="icon">
            <i class=""></i>
          </div>
          <div class="message">
            <p></p>
          </div>
        </div>
        <!--CONTENT APPLICATION-->
        <div class="content-app">
