<?php
require_once 'includes/header.php';
require_once('../core/modules/index/model/DaoSeccionAportacion.php');
$DaoSeccionAportacion = new DaoSeccionAportacion();

?>
<div class="container-fluid">
    <div class="row margin-row-bottom">
      <div class="Wall margin-row-bottom z-depth-1">
          <div class="Wall__header z-depth-1">
            <h3 class="display-inline text-white bolder"><i class="fa fa-users"></i> Catálogo de Aportaciones de Bihaiv</h3>
  			<div class="pull-right" style="margin-top:-3px">
  				<a href="aportacion_bihaiv.php" class="btn btn-outline-primary">
  					<i class="fa fa-plus"></i> Agregar Nueva
  				</a>
  			</div>
          </div>
          <div class="Wall__content large padding">
              <div class="table-responsive" id="box-table-aportaciones">

              </div>
          </div>
      </div>
    </div>
</div>

<?php
require_once 'includes/footer.php';
?>
