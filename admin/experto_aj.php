<?php
session_start();
require_once '../core/app/debuggin.php';
require_once '../core/modules/index/model/DaoEstados.php';
require_once '../core/modules/index/model/DaoExperto.php';
require_once '../core/modules/index/model/DaoRedesExperto.php';
require_once '../core/modules/index/model/DTO/Experto.php';
require_once '../core/modules/index/model/DTO/RedesExperto.php';

require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoExperto = new DaoExperto();
    $DaoEstados = new DaoEstados();
    $UserData = new UserData();

    $nombre = "";
    $email = "";
    $ubicacion = "";
    $organizacion = "";
    $tipoOrganizacion = "";
    $ramoExperiencia = "";
    $aniosExperiencia = "";
    $cargoActual = "";
    $cargoAnterior = "";
    $bibliografia = "";
    $imagen = "";
    $telefono="";
    $facebook="";
    $twitter="";
    $linkedin="";
    $llaveImagen="";
    if (isset($_POST['id']) && $_POST['id'] > 0) {

        $experto = $DaoExperto->getById($_REQUEST['id']);
        $user = $UserData->getById($experto->getUsuarios_idUsuarios());

        $nombre = $user->name;
        $email = $user->email;
        $llaveImagen= $user->imagen;

        $ubicacion = $experto->getUbicacion();
        $organizacion = $experto->getOrganizacion();
        $tipoOrganizacion = $experto->getTipoOrganizacion();
        $ramoExperiencia = $experto->getRangoExperiencia();
        $aniosExperiencia = $experto->getAniosExperiencia();
        $cargoActual = $experto->getCargoActual();
        $cargoAnterior = $experto->getCargoAnterior();
        $bibliografia = $experto->getBibliografia();
        $telefono=$experto->getTelefono();

        $lat = $experto -> getLat();
        $lng = $experto -> getLng();

        if (strlen($user->imagen) > 0) {
            $imagen = 'style="background-image: URL(files/' . $user->imagen . ')"';
        }

        $DaoRedesExperto = new DaoRedesExperto();
        $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'facebook');
        if($RedesExperto->getId()>0){
           $facebook=$RedesExperto->getUrl();
        }
        $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'twitter');
        if($RedesExperto->getId()>0){
           $twitter=$RedesExperto->getUrl();
        }
        $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'linkedin');
        if($RedesExperto->getId()>0){
           $linkedin=$RedesExperto->getUrl();
        }
    }
    ?>
    <div class="col-xs-12 col-md-12 col-lg-12">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 box-img" <?php echo $imagen; ?>></div>
      <button  class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 btn btn-primary" onclick="mostrarFinder()">Cargar imagen</button>
    </div>
    <!-- <form id="form_experto"> -->
    <div class="col-xs-12 col-md-12 col-lg-12 seccion">
        <div class="form-group">
            <label for="nombre"><span class="requerido">*</span>Nombre:</label>
            <input type="text" class="form-control" id="nombre" maxlength="100" required value="<?php echo $nombre; ?>">
        </div>
        <div class="form-group">
            <label for="email"><span class="requerido">*</span>Email:</label>
            <input type="email" class="form-control" id="email" <?php if($_POST['id']>0) echo 'disabled'; ?> required value="<?php echo $email; ?>">
     
        </div>
        <div class="form-group">
            <label for="pass"><span class="requerido">*</span>Contraseña:</label>
            <input type="password" class="form-control" id="pass" minlength="8" <?php if(!isset($_POST['id'])){ echo 'required'; } ?>>
        </div>
        <div class="form-group">
            <label for="confirmar"><span class="requerido">*</span>Confirmar contraseña:</label>
            <input type="password" class="form-control" id="confirmar" minlength="8" <?php if(!isset($_POST['id'])){ echo 'required'; } ?>>
        </div>
        <div class="form-group">
            <label for="ubicacion"><span class="requerido">*</span>Ubicación:</label>
            <select class="form-control" id="ubicacion">
                <option value="0">Seleccione uno...</option>
                <?php
                foreach ($DaoEstados->getAll() as $estado) {
                    ?>
                    <option value="<?php echo $estado->getId() ?>" <?php if ($ubicacion == $estado->getId()) { ?> selected="selected" <?php } ?>><?php echo $estado->getNombre() ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="organizacion"><span class="requerido">*</span>Organización a la que pertenece:</label>
            <input type="text" class="form-control" id="organizacion"  maxlength="70" required value="<?php echo $organizacion; ?>">
        </div>
        <div class="form-group">
            <label for="tipo-organizacion"><span class="requerido">*</span>Tipo de organización:</label>
            <input type="text" class="form-control" id="tipo-organizacion"  required value="<?php echo $tipoOrganizacion; ?>">
        </div>
    </div>
    <div class="col-xs-12 col-md-12 col-lg-12 seccion" id="box-informacion-experto">
        <div class="form-group">
            <label for="ramo-experiencia"><span class="requerido">*</span>Ramo de experiencia:</label>
            <input type="text" class="form-control" id="ramo-experiencia" maxlength="100" required value="<?php echo $ramoExperiencia; ?>">
        </div>
        <div class="form-group">
            <label for="anios-experiencia"><span class="requerido">*</span>Años de experiencia en el sector:</label>
            <input type="text" class="form-control" id="anios-experiencia"  required value="<?php echo $aniosExperiencia; ?>">
        </div>
        <div class="form-group">
            <label for="cargo-actual"><span class="requerido">*</span>Cargo actual:</label>
            <input type="text" class="form-control" id="cargo-actual" maxlength="70" required value="<?php echo $cargoActual; ?>">
        </div>
        <div class="form-group">
            <label for="cargo-anterior"><span class="requerido"> </span>Cargo anterior:</label>
            <input type="text" class="form-control" id="cargo-anterior" maxlength="70" required value="<?php echo $cargoAnterior; ?>">
        </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
          <label for="cargo-anterior"><span class="requerido"> *</span>Ubicación (<small>Arrastra el pin hasta tu ubicación</small>): </label>
          <input type="hidden" id="lat"  class="form-control" id="" maxlength="70" required value="<?php echo $lat; ?>">
          <input type="hidden" id="lng"  class="form-control" id="" maxlength="70" required value="<?php echo $lng; ?>">
      </div>
      <div id="map" style="height: 350px !important;"></div>
      <br>
    </div>
    <div class="col-xs-12 col-md-12 col-lg-12 seccion" id="box-informacion-experto">
        <div class="form-group">
            <label for="bibliografia"><span class="requerido">*</span>Biografía:</label>
            <textarea  class="form-control" id="bibliografia" maxlength="500" required placeholder="(500 caracteres)"><?php echo $bibliografia; ?></textarea>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 col-lg-12 seccion" id="box-informacion-experto">
        <div class="form-group">
            <i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook:</label>
            <input type="url" class="form-control" id="facebook" placeholder="Ej. http://www.facebook.com/mifanpage" value="<?php echo $facebook; ?>">
        </div>
        <div class="form-group">
            <i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter:</label>
            <input type="url" class="form-control" id="twitter"  placeholder="Ej. http://www.twitter.com/miusuario" value="<?php echo $twitter; ?>">
        </div>
        <div class="form-group">
            <i class="fa fa-linkedin-square" aria-hidden="true"></i> Linkedln:</label>
            <input type="url" class="form-control" id="linkedin" placeholder="Ej. http://www.linkedin.com/in/miperfil"value="<?php echo $linkedin; ?>">
        </div>
        <div class="form-group">
            <i class="fa fa-phone-square" aria-hidden="true"></i> Teléfono:</label>
            <input type="text" class="form-control" id="telefono" value="<?php echo $telefono; ?>">
        </div>
    </div>
    <button type="buttton" class="btn btn-primary pull-right" onclick="saveExperto()" >Guardar experto</button>
    <input type="hidden" id="llave-imagen" value="<?php echo $llaveImagen;?>"/>
    <!-- </form> -->
    <?php
}

if (isset($_GET['action']) && $_GET['action'] == "uploadAttachment") {
    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $DaoExperto = new DaoExperto();
            $namefile = $DaoExperto->generarKey() . ".jpg";
            if ($_GET['id'] > 0) {
                $UserData = new UserData();
                $experto=$DaoExperto->getById($_GET['id']);
                $user = $UserData->getById($experto->getUsuarios_idUsuarios());
                if ($user->imagen!=$_GET['llave'] && strlen($user->imagen)>0) {
                    //Eliminar la foto anterior
                    if(file_exists('files/' . $user->imagen)){
                       unlink('files/' . $user->imagen);
                    }
                }
            }
            $ubicacion = "files/" . $namefile;
            move_uploaded_file($_FILES["file"]["tmp_name"], $ubicacion);
            echo $namefile;
        }
    } elseif (isset($_GET['action'])) {

        if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {


            if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $dataFile = file_get_contents('php://input');
                if (isset($_POST['fileExplorer'])) {
                    //If the browser support readAsArrayBuffer ()
                    //PHP handles spaces in base64 encoded string differently
                    //so to prevent corruption of data, convert spaces to +
                    $dataFile = $_POST['fileExplorer'];
                    $dataFile = str_replace(' ', '+', $dataFile);
                }
                $content = base64_decode($dataFile);
            }

            $DaoExperto = new DaoExperto();
            $namefile = $DaoExperto->generarKey() . ".jpg";
            if ($_GET['id'] > 0) {
                $UserData = new UserData();
                $experto=$DaoExperto->getById($_GET['id']);
                $user = $UserData->getById($experto->getUsuarios_idUsuarios());
                if ($user->imagen!=$_GET['llave'] && strlen($user->imagen)>0) {
                    //Eliminar la foto anterior
                    if(file_exists('files/' . $user->imagen)){
                       unlink('files/' . $user->imagen);
                    }
                }
            }

            $ubicacion = "files/" . $namefile;
            file_put_contents($ubicacion, $content);
            echo $namefile;
        }
    }
}


if(isset($_POST['action']) && $_POST['action']=="saveRedes"){

    if($_POST['urlFacebook']){

        $DaoRedesExperto= new DaoRedesExperto();
        $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'facebook');
        if($RedesExperto->getId()==0){
           $RedesExperto= new RedesExperto();
           $RedesExperto->setNombre('facebook');
           $RedesExperto->setUrl($_POST['urlFacebook']);
           $RedesExperto->setExperto_idExperto($_POST['id']);
           $DaoRedesExperto->add($RedesExperto);
        }else{
           $RedesExperto->setUrl($_POST['urlFacebook']);
           $DaoRedesExperto->update($RedesExperto);
        }

    }

    if($_POST['urlTwitter']){

        $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'twitter');
        if($RedesExperto->getId()==0){
           $RedesExperto= new RedesExperto();
           $RedesExperto->setNombre('twitter');
           $RedesExperto->setUrl($_POST['urlTwitter']);
           $RedesExperto->setExperto_idExperto($_POST['id']);
           $DaoRedesExperto->add($RedesExperto);
        }else{
           $RedesExperto->setUrl($_POST['urlTwitter']);
           $DaoRedesExperto->update($RedesExperto);
        }
        
    }

    if($_POST['urlLinkedin']){

        $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'linkedin');
        if($RedesExperto->getId()==0){
           $RedesExperto= new RedesExperto();
           $RedesExperto->setNombre('linkedin');
           $RedesExperto->setUrl($_POST['urlLinkedin']);
           $RedesExperto->setExperto_idExperto($_POST['id']);
           $DaoRedesExperto->add($RedesExperto);
        }else{
           $RedesExperto->setUrl($_POST['urlLinkedin']);
           $DaoRedesExperto->update($RedesExperto);
        }

    }


    if ($_SESSION['is_expert']){
        // Es experto
        $salida = array("status" => true, "message" => "Correcto", "is_expert" => true, "expert_id" => $_POST['id'], "type" => "success" );
    } else  if ($_SESSION['is_admin']) {
        // Es admin
        $salida = array("status" => true, "message" => "Correcto", "is_expert" => false, "expert_id" => null, "type" => "success" );
    } else {
        $salida = array("status" => false, "message" => "Error", "is_expert" => false, "expert_id" => null, "type" => "error" );
    }

    echo json_encode( $salida );

}
