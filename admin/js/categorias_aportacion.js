function updatePage(offset){
    var params= new Object()
        params.action="updatePage";
        params.offset=offset
    $.post("categorias_aportacion_aj.php",params,function(resp){
           $('#box-table-categorias-aportacion').html(resp)
    })    
}

function deleteCategory(id){
    if(confirm("Está seguro de querer eliminar la categoría?")){
    var params= new Object()
        params.method="delete";
        params.id=id
        $.post("../api/admin/categorias_aportacion.php",params,function(resp){
              updatePage();
        })    
    }
}


function search(){
 var params= new Object()
     params.action="updatePage"
     params.buscar='"%'
     if($.trim($('#buscar').val())){
        params.buscar+=$('#buscar').val();
     }
     params.buscar+='%"';

  $.post("categorias_aportacion_aj.php",params, function(resp){
    $('#box-table-categorias-aportacion').html(resp);
  });
}

$(document).ready(function(){	
    updatePage();    
});