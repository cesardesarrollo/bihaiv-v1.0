$(document).ready(function(){
    updatePage()

})

function updatePage(offset){
    var params= new Object()
        params.action="updatePage";
        params.offset=offset
    $.post("actores_aj.php",params,function(resp){
           $('#box-table-actores').html(resp)
    })
}

function deleteOrganizacion(id){
    if(confirm("Está seguro de querer eliminar al actor?")){
    var params= new Object()
        params.method="delete";
        params.id=id
        $.post("../api/admin/organizacion.php",params,function(resp){
              updatePage()
        })
    }
}


function getActorsReport(){
    // Marcadores de actores
    $.ajax({
        url: "../api/admin/organizacion.php",
        method: 'POST',
        dataType: 'JSON',
        data: {
            method: 'all'
        }
    })
    .done(function(data) {

        JSONToCSVConvertor(data, "Reporte de Actores", true);

    })
    .fail(function(_err) {
        console.log(_err);
    });
}

function buscador(){
 var params= new Object()
     params.action="updatePage"
     params.buscar="%"
     if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
     }
  var funcionExito=function funcionExito(resp){
         $('#box-table-actores').html(resp)
  }
  $.post("actores_aj.php",params,funcionExito);
}



  // Función para convertir un JSON en CSV
  function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
      var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
      var CSV = '';
      CSV += ReportTitle + '\r\n\n';
      if (ShowLabel) {
          var row = "";
          //Títulos de columnas
          for (var index in arrData[0]) {
              //Comas
              row += index + ',';
          }
          row = row.slice(0, -1);
          CSV += row + '\r\n';
      }
      //Renglones
      for (var i = 0; i < arrData.length; i++) {
          var row = "";
          //Columnas
          for (var index in arrData[i]) {
              row += '"' + arrData[i][index] + '",';
          }
          row.slice(0, row.length - 1);
          CSV += row + '\r\n';
      }
      if (CSV == '') {
          setFlash("Invalid data", 'warning');
          return;
      }
      var fileName = "Bihaiv_";
      fileName += ReportTitle.replace(/ /g,"_");
      var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
      var link = document.createElement("a");
      link.href = uri;
      link.style = "visibility:hidden";
      link.download = fileName + ".csv";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
  }
