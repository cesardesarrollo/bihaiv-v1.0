$(document).ready(function () {
    updatePage();

    $('body').on('change', '#nivel', function(){
        if($(this).val() == 1){
            $('#wall_preguntas_respuestas').removeClass('hidden');
            $('#msg_preguntas').addClass('hidden');
        }else{
            $('#wall_preguntas_respuestas').addClass('hidden');
            $('#msg_preguntas').removeClass('hidden');
        }
    });
})

function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.id = $('#id-cuestionario').val()
    $.post("cuestionario_aj.php", params, function (resp) {
        $('#box-cuestionario').html(resp);
    })
}

function addPregunta() {
    var params = new Object()
    params.action = "addPregunta"
    $.post("cuestionario_aj.php", params, function (resp) {
        $('.box-preguntas').append(resp)
    })
}

function addRespuesta(Obj) {
    var params = new Object()
    params.action = "addRespuesta"
    $.post("cuestionario_aj.php", params, function (resp) {
        $(Obj).closest('.box-pregunta').find('.box-respuestas').append(resp)
    })
}

var idCuestionario = 0
var tipoAccion = "";
function saveCuestionario() {
    var accion = "save";
    if ($('#id-cuestionario').val() > 0) {
        accion = "edit";
    }
    tipoAccion = accion;
    var params = new Object();
    params.method = accion;
    params.id = $('#id-cuestionario').val();
    params.nombre = $('#txt__Name').val();
    params.nivel = $('#nivel').val();
    params.activo = $('#activo').val();

    var error = 0
    var errores = new Array()
    if ($('#txt__Name').val().length == 0) {
        error = "Nombre requerido"
        errores.push(error);
    }
    if ($('#nivel option:selected').val() == 0) {
        error = "Nivel requerido"
        errores.push(error);
    }

    var question_count = 0;
    $('.box-pregunta').each(function(){
        if ($(this).find('.form-control.pre').val().length == 0) {
            question_count++;
        }
    });

    if (question_count > 0 && params.nivel == 1) {
        error = "Hay "+ question_count +" pregunta(s) inválida(s)";
        errores.push(error);
    }

    var answer_count = 0;
    $('.box-respuestas .respuesta').each(function(){
        if ($(this).find('input.form-control').val().length == 0 || $(this).find('select.form-control').val().length == 0 || $(this).find('select.form-control').val() == 0) {
            answer_count++;
        }
    });

    if (answer_count > 0 && params.nivel == 1) {
        error = "Hay "+ answer_count +" respuesta(s) inválida(s)";
        errores.push(error);
    }

    if (errores.length == 0) {
        $.post("../api/admin/cuestionarios.php", params, function (resp) {
            console.log(resp);
            idCuestionario = resp.id
            if(resp.nivel == 1) {
                savePreguntas(resp.id);
            }else {
                setFlash("Datos guardados con éxito", 'success');
                setTimeout(function(){
                    window.location = "cuestionarios.php";
                },1800);
            }
        }, "json");
    } else {
        $string = "Error\n\n";
        $.each(errores, function (key, val) {
            $string += "-" + val + "\n"
        });
        setFlash($string, 'warning');
    }
}

function savePreguntas(idCuestionario) {
    var ordenPre = 1;
    var count = 1;
    var numElementos = $('.box-pregunta').length;
    $('.box-pregunta').each(function () {
        var ObjPre = $(this);
        var idPre = 0;
        var accion = "save";
        var textoPre = $(this).find('.form-control.pre').val();
        if ($(this).attr('id-pre') > 0) {
            idPre = $(this).attr('id-pre');
            accion = "edit";
        }
        var params = new Object();
        params.method = accion;
        params.id = idPre;
        params.pregunta = textoPre;
        params.orden = ordenPre;
        params.Cuestionario_idCuestionario = idCuestionario;
        var lastQuestion = (count == numElementos);

        $.post("../api/admin/preguntas.php", params, function (resp) {
            saveRespuestas(idCuestionario, resp.id, ObjPre, lastQuestion);
        }, "json");

        count++;
        ordenPre++;
    })
}

function saveRespuestas(id, idPre, ObjPre, lastQuestion) {
    ObjPre.attr('id-pre', idPre);

    var ordenResp = 1;
    var objResp = ObjPre.find('.box-respuestas .respuesta');
    var numElementos = objResp.length;

    objResp.each(function () {
        var accion = "save";
        var idResp = 0;
        if ($(this).attr('id-resp') > 0) {
            idResp = $(this).attr('id-resp');
            accion = "edit";
        }
        var params = new Object();
        params.method = accion;
        params.id = idResp;
        params.texto = $(this).find('input').val();
        params.rol = $(this).find('select option:selected').val();
        params.id_pre = idPre;
        params.orden = ordenResp;
        var lastAnswer = (numElementos == ordenResp);

        $.post("../api/admin/respuestas.php", params, function (resp) {
            if (lastQuestion && lastAnswer) {
                setFlash("Datos guardados con éxito", 'success');
                setTimeout(function(){
                    window.location = "cuestionarios.php";
                },1800);
            }
        });
        ordenResp++;
    });
}

function deleteRes(Obj) {
    $(Obj).closest('.respuesta').remove()
}

function deletePre(Obj) {
    $(Obj).closest('.box-pregunta').remove()
}

function deletePregunta(id_pre, Obj) {
    if (confirm("¿Está seguro de eliminar la pregunta?")) {
        var params = new Object()
            params.method = "delete"
            params.id = id_pre
            $.post("../api/admin/preguntas.php", params, function (resp) {
                deletePre(Obj)
            })
    }
}

function deleteRespuesta(id_resp, Obj) {
    if (confirm("¿Está seguro de eliminar la respuesta?")) {
        var params = new Object()
        params.method = "delete"
        params.id = id_resp
        $.post("../api/admin/respuestas.php", params, function (resp) {
            deleteRes(Obj)
        })
    }
}

function deleteRespuestaSeriada(id_resp, Obj) {
    var params = new Object()
    params.method = "delete"
    params.id = id_resp
    $.post("../api/admin/respuestas.php", params, function (resp) {
        deleteRes(Obj)
    })
}
