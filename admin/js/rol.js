$(document).ready(function () {
    updatePage()

})

function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.id = $('#id-rol').val()
    $.post("rol_aj.php", params, function (resp) {
        $('#box-rol').html(resp)
    })
}


function saveRol() {
    var accion = "save";
    if ($('#id-rol').val() > 0) {
        accion = "edit"
    }
    var params = new Object()
    params.method = accion;
    params.imagen = $('#llave-imagen').val();
    params.id=$('#id-rol').val();
    params.nombre=$('#txt__Name').val();
    params.descripcion=$('#txt__Descripcion').val();
    params.description=$('#txt__Description').val();
    params.descripcion_corta=$('#txt__DescripcionCorta').val();
    params.short_description=$('#txt__ShortDescription').val();
    params.activo=$('#estatus option:selected').val();

    var error = 0
    var errores = new Array()
    if ($('#txt__Name').val().length == 0) {
        error = "Nombre requerido"
        errores.push(error);
    }
    if ($('#txt__Descripcion').val().length == 0) {
        error = "Descripción requerido"
        errores.push(error);
    }
    if($('#estatus option:selected').val()==-1){
        error = "Estatus requerido"
        errores.push(error);
    }
    if ($('#llave-imagen').val().length == 0) {
        error = "Una imagen es requerida"
        errores.push(error);
    }
    if (errores.length == 0) {
        $.post("../api/admin/roles.php", params, function (resp) {
            setFlash("Datos guardados con éxito", 'success');
            window.location = "roles.php"
        }, "json")
    } else {
        $string="Error\n\n";
        $.each(errores, function(key, val) {
            $string+="-"+val+"\n"
        })
        setFlash($string, 'warning')
    }
}




//Funciones para subir los archivos al serever
function mostrarFinder() {
    $('#files').click()
}

if ($('#files').length) {
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
}


function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object

        if (evt.dataTransfer) {
            var files = evt.dataTransfer.files
        } else {
            var files = evt.target.files;
        }

        var count = 0;
        var bin, name, type, size;


        //Funcion loadStartImg
        var loadStartImg = function () {
            //alert("Inicia")
        }

        //Funcion loadEndImg
        var loadEndImg = function (e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function (e) {
                var pc = parseInt((e.loaded / e.total * 100));
                //$(ObjX).text("Adjuntando...")
                var mns = 'Cargando ...' + pc + '%'
                //alerta(mns);
                if (pc == 100) {
                    //$(ObjX).text("Adjuntar archivos")
                    //ocultar_error_layer()
                }
            }, false);



            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    $('#files').val('')
                    $('.box-img').attr('style','background-image: URL(files/'+xhr.responseText+')')
                    $('#llave-imagen').val(xhr.responseText)
                }
            }

            xhr.open('POST', 'rol_aj.php?action=uploadAttachment&id='+$('#id-rol').val()+'&llave='+$('#llave-imagen'), true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {

                xhr.open('POST', 'rol_aj.php?action=uploadAttachment&base64=ok&FileName=' + name + '&TypeFile=' + type+'&id='+$('#id-rol').val()+'&llave='+$('#llave-imagen'), true);
                xhr.setRequestHeader('UP-FILENAME', utf8_encode(name));
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                //Encode BinaryString to base64
                if (reader.readAsBinaryString) {
                    xhr.send(window.btoa(bin));
                } else {
                    xhr.send("fileExplorer=" + window.btoa(bin));
                }
            }

            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }



        //Funcion loadErrorImg
        var loadErrorImg = function (evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    setFlash('File Not Found!', 'warning');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    setFlash('File is not readabl33e', 'warning');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    setFlash('An error occurred reading this file.', 'warning');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

             // Only process image files.
             if (!f.type.match('image.*')) {
             continue;
             }

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size

                    if (reader.readAsBinaryString) {
                        bin = e.target.result
                    } else {
                        //Explorer
                        //Convert ArrayBuffer to BinaryString
                        bin = "";
                        bytes = new Uint8Array(reader.result);
                        var length = bytes.byteLength;
                        for (var i = 0; i < length; i++) {
                            bin += String.fromCharCode(bytes[i]);
                        }
                    }
                };
            })(f);

            preview.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    /*
                     var li = document.createElement('li');
                     $(li).attr('id','img_'+count)
                     li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
                     $('#visor_subir_img').append(li)
                     $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
                     $('#ruta_img').val($('#files').val())
                     count++;
                     */
                };
            })(f);


            if (reader.readAsBinaryString) {
                //Read in the image file as a binary string.
                reader.readAsBinaryString(f);
            } else {
                //Explorer
                //Contendrá los datos del archivo/objeto BLOB como un objeto ArrayBuffer.
                reader.readAsArrayBuffer(f)
            }


            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                //IE 10
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }

                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        setFlash('The File APIs are not fully supported in this browser.', 'warning');
    }

}







function utf8_encode(argString) {
    //  discuss at: http://phpjs.org/functions/utf8_encode/
    // original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: sowberry
    // improved by: Jack
    // improved by: Yves Sucaet
    // improved by: kirilloid
    // bugfixed by: Onno Marsman
    // bugfixed by: Onno Marsman
    // bugfixed by: Ulrich
    // bugfixed by: Rafal Kukawski
    // bugfixed by: kirilloid
    //   example 1: utf8_encode('Kevin van Zonneveld');
    //   returns 1: 'Kevin van Zonneveld'

    if (argString === null || typeof argString === 'undefined') {
        return '';
    }

    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = '',
            start, end, stringl = 0;

    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode(
                    (c1 >> 6) | 192, (c1 & 63) | 128
                    );
        } else if ((c1 & 0xF800) != 0xD800) {
            enc = String.fromCharCode(
                    (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
                    );
        } else { // surrogate pairs
            if ((c1 & 0xFC00) != 0xD800) {
                throw new RangeError('Unmatched trail surrogate at ' + n);
            }
            var c2 = string.charCodeAt(++n);
            if ((c2 & 0xFC00) != 0xDC00) {
                throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
            }
            c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
            enc = String.fromCharCode(
                    (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
                    );
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }

    if (end > start) {
        utftext += string.slice(start, stringl);
    }

    return utftext;
}
