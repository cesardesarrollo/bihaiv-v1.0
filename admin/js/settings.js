'use strict';

function addRow( index ){
	var index = parseInt(index);
	var template = '<div class="row margin-row-bottom">' +
		'<div class="col-xs-12 col-lg-12">' +
			 '<div class="form-group">'+
			    '<label for="txt__Pregunta_' + (index + 1) +'">Pregunta ' + (index + 1) +':</label>'+
			    '<input type="text" class="form-control" id="txt__Pregunta_1" placeholder="Pregunta 1">'+
			  '</div>'+
		'</div>'+
		'<div class="col-xs-12 col-lg-12">'+
			'<div class="col-xs-3 col-sm-3 col-lg-3">' +
				'<div class="form-group">' +
				    '<label for="txt__Pregunta_1">Respuesta 1:</label>' +
				    '<input type="text" class="form-control" id="txt__Respuesta_' + (index + 1) +'" placeholder="Respuesta 1">' +
				  '</div>' +
			'</div>' +
			'<div class="col-xs-3 col-sm-3 col-lg-3">' +
				'<div class="form-group">' +
				    '<label for="txt__Pregunta_1">Respuesta 2:</label>' +
				    '<input type="text" class="form-control" id="txt__Respuesta_' + (index + 1) +'" placeholder="Respuesta 1">' +
				  '</div>' +
			'</div>' +
			'<div class="col-xs-3 col-sm-3 col-lg-3">' +
				'<div class="form-group">' +
				    '<label for="txt__Pregunta_1">Respuesta 3:</label>' +
				    '<input type="text" class="form-control" id="txt__Respuesta_' + (index + 1) +'" placeholder="Respuesta 1">' +
				  '</div>' +
			'</div>' +
			'<div class="col-xs-3 col-sm-3 col-lg-2">' +
				'<div class="form-group">' +
				    '<label for="txt__Pregunta_1">Respuesta 4:</label>' +
				    '<input type="text" class="form-control" id="txt__Respuesta_' + (index + 1) +'" placeholder="Respuesta 1">' +
				'</div>' +
			'</div>' +
			'<div class="col-xs-3 col-sm-3 col-lg-1">' +
				'<div class="form-group">' +
				    '<label for="">Agregar</label>' +
				   	'<button onclick="addRow(' + (index + 1) +')" class="btn btn-primary btn-block">' +
						'<i class="fa fa-plus"></i>' +
					'</button>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';

	$("#questions").append(template);
}