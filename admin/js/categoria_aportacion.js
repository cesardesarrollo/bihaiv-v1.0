function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.id = $('#id-categoria-aportacion').val()
    $.post("categoria_aportacion_aj.php", params, function (resp) {
        $('#box-categoria-aportacion').html(resp);
        validateParentCategory($('#idPadre'));
    });
}


function saveCategory() {
    var accion = "save";
    if ($('#id-categoria-aportacion').val() > 0) {
        accion = "edit"
    }

    var params = new Object()
    params.method = accion
    params.id = $('#id-categoria-aportacion').val()
    params.titulo = $('#titulo').val()
    params.activo = $('#activo').val()
    params.idPadre = $('#idPadre').val()
    params.idSeccion = $('#idSeccion').val();

    var error = 0
    var errores = new Array()

    if ($('#titulo').val().length == 0) {
        error = "Título es un campo requerido";
        errores.push(error);
    }

    if ($('#activo').val().length == 0) {
        error = "Es necesario seleccionar si la categoría será visible";
        errores.push(error);
    }

    if ($('#idPadre').val().length == 0) {
        error = "Es necesario seleccionar si la categoría tiene un padre";
        errores.push(error);
    }

    if ($('#idSeccion').val().length == 0 || $('#idSeccion').val() == 0 ) {
        error = "Es necesario seleccionar la sección a la que pertenece";
        errores.push(error);
    }

    if (errores.length == 0) {
        $.post("../api/admin/categorias_aportacion.php", params, function (resp) {
            setFlash("Datos guardados con éxito", 'success');
            window.location = "categorias_aportacion.php";
        }, "json");
    } else {
        var string="Error\n\n";
        $.each(errores, function(key, val) {
            string += "-"+val+"\n"
        });

        setFlash(string, 'warning');
    }
}

function validateParentCategory(select){
    var section = $('#idSeccion');
    var sectionVal = select.find(':selected').data('section');

    if(select.val() != 0){
        if(section.find('option[value="'+sectionVal+'"]').length){
            section.prop('disabled',true).val(sectionVal);
        }else{
            section.prop('disabled',true).val(0);
            setFlash('Al parecer la categoría padre que has elegido no tiene una sección asignada, es necesario modificarla y asignarle una primero.', 'warning');
        }
    }else{
        section.prop('disabled',false);
    }
}

$(document).ready(function () {
    $('body').on('change','#idPadre',function(){
        validateParentCategory($(this));
    });

    updatePage();
});
