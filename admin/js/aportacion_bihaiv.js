$(document).ready(function () {
    updatePage()

})

function updatePage() {
    var params = new Object()
    params.action = "updatePage";
    params.id = $('#id-aportacion').val()
    $.post("aportacion_bihaiv_aj.php", params, function (resp) {
        $('#box-aportacion').html(resp)
    })
}


function saveAportacion() {
    var accion = "save_bihaiv";
    if ($('#id-aportacion').val() > 0) {
        accion = "edit"
    }
    var params = new Object()
    params.method = accion
    params.id=$('#id-aportacion').val()
    params.titulo=$('#titulo').val()
    params.descripcion=$('#descripcion').val()
    params.thumbnail=$('#thumbnail').val()
    params.file=$('#file').val()
    params.categoria = '0';

    var error = 0
    var errores = new Array()
    if ($('#titulo').val().length == 0) {
        error = "Titulo requerido"
        errores.push(error);
    }
    if ($('#descripcion').val().length == 0) {
        error = "Descripción requerido"
        errores.push(error);
    }
    if ($('#thumbnail').val().length == 0) {
        error = "Imagen JPG, JPEG o PNG requerida"
        errores.push(error);
    }
    if ($('#file').val().length == 0) {
        error = "Archivo PDF, DOC, DOCX, PPTX, XLS o XLSX requerido"
        errores.push(error);
    }
    if (errores.length == 0) {
        $.post("../api/admin/aportaciones.php", params, function (resp) {
            setFlash("Datos guardados con éxito", 'success');
            window.location = "aportaciones_bihaiv.php"
        }, "json")
    } else {
        $string="Error\n\n";
        $.each(errores, function(key, val) {
            $string+="-"+val+"\n"
        })
        setFlash($string, 'warning');
    }
}
