<?php
require_once '../core/modules/index/model/DaoOrganizacion.php';
require_once '../core/modules/index/model/DTO/Organizacion.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/vendor/phpmailer/class.phpmailer.php';
require_once '../core/controller/Cuestionario.php';
require_once '../core/modules/index/model/UserData.php';
require_once '../core/modules/index/model/ProfileData.php';



if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-expertos').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-expertos">
        <thead>
            <tr>
                <th>#</th>
                <th style="width: 95px;">Foto</th>
                <th>Nombre</th>
                <th>Ubicación</th>
                <th>Teléfono</th>
                <th style="max-width: 150px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;">Web</th>
                <th>Fecha de registro</th>
                <th class="center">Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset=0;
            $limit=10;
            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset=$_POST['offset'];
            }
            $buscar="";
            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $buscar= $_POST['buscar'];
            }
            $DaoOrganizacion = new DaoOrganizacion();
            $ProfileData= new ProfileData();
            $UserData = new UserData();
            $count = 1;
            foreach ($DaoOrganizacion->getAll($offset,$limit,$buscar) as $actor) {
                $user = $UserData->getById($actor->getUsuarios_idUsuarios());
                $profile=$ProfileData->getByUserId($user->id);
                $imagen = "";
                if(isset($profile->image)){
                    $imagen = 'style="background-image: URL(../storage/users/'.$user->id.'/profile/' . $profile->image .')"';
                }
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><div class="box-avatar" <?php echo $imagen; ?>></div></td>
                    <td><?php echo $user->name ?></td>
                    <td><?php echo $actor->getUbicacion() ?></td>
                    <td><?php echo $actor->getTelefono() ?></td>
                    <td style="max-width: 150px;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    white-space: nowrap;"><?php echo $actor->getUrlWeb() ?></td>
                    <td><?php echo $user->created_at ?></td>
                    <td class="center">
                        <button type="button" class="btn btn-default" onclick="deleteOrganizacion(<?php echo $actor->getId() ?>)">Eliminar</button>
                    </th>
                </tr>
                <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        $count = count($DaoOrganizacion->getAll(null,null,null));
        if($count>10){
        ?>
            <ul class="pagination">
                <?php
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                   <?php
                }
                $numPaginas=1;
                for($i=1;$i<=$count;$i++){
                    if(($i%10)==1){
                    ?>
                     <li <?php if(($i-1)==$offset){ ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                    <?php
                       $numPaginas++;
                    }
                }
                if($offset+10<$count){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset+10;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                     <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </nav>
    <?php
}
