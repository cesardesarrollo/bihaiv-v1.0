<?php
require_once '../core/modules/index/model/DaoCuestionario.php';
require_once '../core/modules/index/model/DTO/Cuestionario.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';


if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-cuestionarios').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-cuestionarios">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Nivel</th>
                <th>Fecha de alta</th>
                <th>Activo</th>
                <th class="center">Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset=null;
            $limit=10;
            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset=$_POST['offset'];
            }
            $buscar="";
            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $buscar= $_POST['buscar'];
            }
            $DaoCuestionario = new DaoCuestionario();
            $count = 1;
            foreach ($DaoCuestionario->getAll($offset,$limit,$buscar) as $cuestionario) {

                $nivel="Validación por unidad";
                if($cuestionario->getNivel()==1){
                  $nivel="Perfilamiento";
                }
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><?php echo $cuestionario->getNombre(); ?></td>
                    <td><?php echo $nivel ?></td>
                    <td><?php echo $cuestionario->getFechaCreacion() ?></td>
                    <td><?php echo $cuestionario->getActivo() ?></td>
                    <td class="center">
                        <a href="cuestionario.php?id=<?php echo $cuestionario->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                        <button type="button" class="btn btn-default" onclick="deleteCuestionario(<?php echo $cuestionario->getId() ?>)">Eliminar</button>
                        </th>
                </tr>
                <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        //$count=50;
        if($count>10){
        ?>
            <ul class="pagination">
                <?php
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                   <?php
                }
                $numPaginas=1;
                for($i=1;$i<=$count;$i++){
                    if(($i%10)==1){
                    ?>
                     <li <?php if(($i-1)==$offset){ ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                    <?php
                       $numPaginas++;
                    }
                }
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset+10;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                     <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </nav>
    <?php
}
