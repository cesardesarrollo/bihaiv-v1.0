<?php
require_once '../core/modules/index/model/DaoImagenHome.php';
require_once '../core/modules/index/model/DTO/ImagenHome.php';

require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    $DaoImagenHome = new DaoImagenHome();

    $titulo = "";
    $descripcion = "";
    $description = "";
    $img = "";
    //$textoBoton="";
    $urlBoton="";
    $portada="";
    if (isset($_POST['id']) && $_POST['id'] > 0) {

        $imagen = $DaoImagenHome->getById($_REQUEST['id']);
        $titulo = $imagen->getTitulo();
        $descripcion = $imagen->getDescripcion();
        $description = $imagen->getDescription();
        $portada=$imagen->getPortada();

        if (strlen($imagen->getLlaveImg()) > 0) {
            $img = 'style="background-image: URL(files/' . $imagen->getLlaveImg() . ')"';
        }
    }
    ?>
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0 col-lg-3">
        <div class="box-img" <?php echo $img; ?> id="box-img">
            <span>Presiona sobre el area o arrastra una imagen</span>
        </div>
        <!--<button  class="col-xs-12 col-md-12 col-lg-12 btn btn-primary" onclick="mostrarFinder()">Cargar imagen</button>-->
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-0 col-lg-9 seccion">
        <form id="form-img">
          <div class="form-group">
              <label for="nombre"><span class="requerido">*</span>Título:</label>
              <input type="text" class="form-control" id="nombre" maxlength="100"  value="<?php echo $titulo; ?>">
          </div>
          <div class="form-group">
              <label for="descripcion"><span class="requerido">*</span>Descripción:</label>
              <textarea  class="form-control" id="descripcion" maxlength="500" ><?php echo $descripcion; ?></textarea>
          </div>
          <div class="form-group">
              <label for="description"><span class="requerido">*</span>Descripción en inglés:</label>
              <textarea  class="form-control" id="description" maxlength="500" ><?php echo $description; ?></textarea>
          </div>
          <div class="form-group">
              <label for="portada"><span class=""> </span>Incluir en carrusel de portada:</label>
              <input type="checkbox" class="form-control" id="portada" <?php echo ($portada==1)?'checked="checked"':''; ?>>
          </div>
          <button type="submit" class="btn btn-primary pull-right">Guardar imagen</button>
        </form>
    </div>
    <script>
      $(function(){
        $('#form-img').on('submit', function(e){
          e.preventDefault();
          saveImagen();
        })
      });
    </script>
    <div id="preview-template" style="display: none;"></div>
    <?php
}
