<?php
require_once '../core/modules/index/model/DaoVideosHome.php';
require_once '../core/modules/index/model/DTO/VideosHome.php';

require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';


if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-expertos').stacktable({myClass:'visible-xs'});
      })
    </script>
    <table class="table table-bordered hidden-xs" id="lista-expertos">
        <thead>
            <tr>
                <th>#</th>
                <th style="width: 95px;">Video</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>En Portada</th>
                <th class="center">Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset = null;
            $limit = 10;
            if (isset($_POST['offset']) && $_POST['offset'] > 0) {
                $offset = $_POST['offset'];
            }
            $buscar = "";
            if (isset($_POST['buscar']) && strlen($_POST['buscar']) > 0) {
                $buscar = $_POST['buscar'];
            }
            $DaoVideosHome = new DaoVideosHome();
            $count = 1;
            foreach ($DaoVideosHome->getAllYoutube($offset, $limit, $buscar) as $video) {
                ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td>
                        <iframe width="200" height="200" src="https://www.youtube.com/embed/<?php echo $video->getRuta()?>" frameborder="0" allowfullscreen></iframe>
                    </td>
                    <td><?php echo $video->getNombre() ?></td>
                    <td><?php echo $video->getDescripcion() ?></td>
                    <td><?php echo ($video->getPortada()==1)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-times-circle"></i>'; ?></td>
                    <td class="center">
                        <a href="video.php?id=<?php echo $video->getId() ?>"><button type="button" class="btn btn-default">Editar</button></a>
                        <button type="button" class="btn btn-default" onclick="deleteVideo(<?php echo $video->getId() ?>)">Eliminar</button>
                        </th>
                </tr>
                <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        //$count=50;
        if ($count > 1) {
            ?>
            <ul class="pagination">
                <?php
                if ($offset >= 10) {
                    ?>
                    <li><a onclick="updatePage(<?php echo $offset - 10; ?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                    <?php
                }
                $numPaginas = 1;
                for ($i = 1; $i <= $count; $i++) {
                    if (($i % 10) == 1) {
                        ?>
                        <li <?php if (($i - 1) == $offset) { ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i - 1; ?>)"><?php echo $numPaginas ?></a></li>
                            <?php
                            $numPaginas++;
                        }
                    }
                    if ($offset >= 10) {
                        ?>
                    <li><a onclick="updatePage(<?php echo $offset + 10; ?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                    <?php
                }
                ?>
            </ul>
            <?php
        }
        ?>
    </nav>
    <?php
}
