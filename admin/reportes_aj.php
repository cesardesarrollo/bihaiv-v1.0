<?php
require_once '../core/app/defines.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/ReportData.php';
require_once '../core/modules/index/model/UserData.php';

if ($_POST['action'] == "updatePage") {
    ?>
    <script>
      $(function(){
        $('#lista-reportes').stacktable({myClass:'visible-xs visible-sm visible-md'});
      })
    </script>
    <table class="table table-bordered hidden-xs hidden-sm hidden-md " id="lista-reportes">
        <thead>
            <tr>
                <th>#</th>
                <th>Clasificación</th>
                <th>Reporta</th>
                <th>Reportado</th>
                <th>Reporte aceptado</th>
                <th>Reporte revisado</th>
                <th>Fecha de creación</th>
                <th>Comentario</th>
                <th>Bitácora de observaciones</th>
                <th>Actividades</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $offset=null;
            $limit=10;
            if(isset($_POST['offset']) && $_POST['offset']>0){
                $offset=$_POST['offset'];
            }
            $buscar="";
            if(isset($_POST['buscar']) && strlen($_POST['buscar'])>0){
              $buscar= $_POST['buscar'];
            }
            $ReportData = new ReportData();
            $count = 1;

            $UserData = new UserData();

            foreach ($ReportData->getAllAdvanced($offset,$limit,$buscar) as $report) {
                $sender = $UserData->getById($report->sender_id);
                $sender_name = $sender->getFullname();
                $receptor = $UserData->getById($report->receptor_id);
                $receptor_name = $receptor->getFullname();
                ?>
                <tr>
                    <td><?php echo $report->id; ?></td>
                    <td><?php echo $report->type; ?></td>
                    <td><?php echo $sender_name; ?></td>
                    <td><?php echo $receptor_name; ?></td>
                    <td><?php echo ($report->is_accepted == 1)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-times-circle"></i>' ?></td>
                    <td><?php echo ($report->is_readed == 1)?'<i class="fa fa-check-circle"></i>':'<i class="fa fa-times-circle"></i>' ?></td>
                    <td><?php echo $report->created_at; ?></td>
                    <td><?php echo $report->comment; ?></td>
                    <td><?php echo $report->comments_response; ?></td>
                    <td class="center">
                        <button type="button" class="btn btn-success" onclick="validate(<?php echo $report->id ?>)">Validar</button>
                        <button type="button" class="btn btn-primary" onclick="read(<?php echo $report->id ?>)">Revisado</button>
                        <button type="button" class="btn btn-default" onclick="del(<?php echo $report->id ?>)">Eliminar</button>
                    </td>
                </tr>
            <?php
                $count++;
            }
            ?>
        </tbody>
    </table>
    <nav>
        <?php
        //$count=50;
        if($count>1){
        ?>
            <ul class="pagination">
                <?php
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset-10;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                   <?php
                }
                $numPaginas=1;
                for($i=1;$i<=$count;$i++){
                    if(($i%10)==1){
                    ?>
                     <li <?php if(($i-1)==$offset){ ?> class="active" <?php } ?>><a onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                    <?php
                       $numPaginas++;
                    }
                }
                if($offset>=10){
                    ?>
                     <li><a onclick="updatePage(<?php echo $offset+10;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                     <?php
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </nav>
    <?php
}
