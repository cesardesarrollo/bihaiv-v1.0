/**
* Gulp - comprimir imagenes (assets/img): gulp compressimages
* Gulp - minificar css y minificar/uglificar js: gulp
*/

'use strict';

var gulp      = require('gulp');
var uglify    = require('gulp-uglify');
var concat    = require('gulp-concat');
var pump      = require('pump');
var imagemin  = require('gulp-imagemin');
var minifyCSS = require('gulp-minify-css');
var useref    = require('gulp-useref');
var gulpIf    = require('gulp-if');
var uncss     = require('gulp-uncss');

var phpcbf = require('gulp-phpcbf');
var gutil = require('gulp-util');


var output_paths = {  
  html: './dist/core/modules/index/view/layout.php',
  images: './assets/img/**/*.*'
}

var source_paths = {  
  html: './core/modules/index/view/layout.php',
  images: './assets/img/**/*.*'
}

gulp.task('compressjs', (cb) =>
    pump([
          gulp.src([
            'res/bootstrap/js/bootstrap.min.js',
            'res/bootstrap-sweetalert/dist/sweetalert.min.js',
            'res/jquery-validate/dist/jquery.validate.min.js',
            'res/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js',
            'res/handlebars/handlebars.js',
            'res/callendar/lib/moment.min.js',
            'res/callendar/lib/jquery-ui.custom.min.js',
            'res/callendar/fullcalendar.min.js',
            'res/jquery-ui/jquery.ui.js',
            'res/dropzone/dist/min/dropzone.min.js',
            'res/timepicker/jquery.timepicker.min.js',
            'res/date-pair/dist/datepair.min.js',
            'res/date-pair/dist/jquery.datepair.min.js',
            'res/tagit_3/js/typeahead.tagging.js',
            'res/bloodhound.js',
            'res/mojs-master/build/mo.min.js',
            'res/fullpage/vendors/scrolloverflow.min.js',
            'res/fullpage/dist/jquery.fullpage.min.js',
            'res/icmap.js',
            'res/toast.js',
            'res/html2canvas.js',
            'core/lang/lang.js'
          ]),
          concat('all.min.js'),
          uglify({
            mangle: true,
            compress: true
          }),
          gulp.dest('js')
      ],
      cb
    )
);

gulp.task('compresscss', (cb) =>
    pump([
          gulp.src([
            'res/bootstrap/css/bootstrap.min.css',
            'res/messages.css',
            'res/toast.css',
            'res/material.css',
            'res/font-awesome/css/font-awesome.min.css',
            'res/animate/animate.css',
            'res/owl.carousel.2.0.0-beta.2.4/assets/owl.carousel.css',
            'res/owl.carousel.2.0.0-beta.2.4/assets/owl.theme.default.min.css',
            'res/callendar/fullcalendar.min.css',
            'res/tagit_3/css/typeahead.tagging.css',
            'res/jquery-ui/jquery.ui.css',
            'res/dropzone/dist/min/dropzone.min.css',
            'res/timepicker/jquery.timepicker.css',
            'res/fullpage/dist/jquery.fullpage.css',
            'assets/css/base.css',
            'assets/css/new_styles.css',
            'res/bootstrap-sweetalert/dist/sweetalert.css'
          ]),
          concat('all.min.css'),
          minifyCSS({keepBreaks:true}),
          gulp.dest('css')
      ],
      cb
    )
);

gulp.task('compressimages', (cb) =>
    pump([
          gulp.src(source_paths.images),
          imagemin(),
          gulp.dest(output_paths.images)
      ],
      cb
    )
);

gulp.task('useref', function(cb) { 
    pump([
          gulp.src(source_paths.html),
          useref(),
          gulpIf('*.js', uglify({
            mangle: true,
            compress: true
          })),
          gulpIf('*.css', minifyCSS( {keepBreaks: true} )),
          gulp.dest(output_paths.html)
      ],
      cb
    )
});

gulp.task('phpcbf', function () {
  return gulp.src(['core/modules/index/model/**/*.php', 'api/**/*.php'])
  .pipe(phpcbf({
    bin: './vendor/bin/phpcbf',
    standard: 'PEAR',
    warningSeverity: 0
  }))
  .on('error', gutil.log)
  .pipe(gulp.dest('dist'));
});


gulp.task('watch', ['compressjs','compresscss'], function() {  
  //gulp.watch(source_paths.html, ['compressjs'])
  //gulp.watch(source_paths.html, ['compresscss'])
});

gulp.task('default', ['watch']); 

