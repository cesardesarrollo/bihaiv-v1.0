<section id="Header__fixed" class="Header__fixed z-depth-1">
      <div class="Header__nav">
        <div class="container">
          <div class="pull-left">
            <!-- LOGO -->
            <figure class="figure">
              <a href="<?=APP_PATH?>">
                <img src="assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="50">
              </a>
            </figure>
          </div>
          <div class="pull-right">
            <!-- LOGIN - REGISTRO -->
            <?php if(!Session::exists("user_id")):?>
            <a href="home/login.php" class="btn btn-header-menu-fixed"><?php echo translate('Cómo funciona'); ?></a>
            <a href="home/login.php" class="btn btn-header-menu-fixed"><?php echo translate('Iniciar sesión'); ?></a>
            <a href="home/registro.php" class="btn btn-header-menu-fixed"><?php echo translate('Únete'); ?></a>
            <?php endif; ?>
            <!-- LENGUAJE -->
            <?php if( isset($_COOKIE['bihaiv_lang'])): ?>
              <?php if($_COOKIE['bihaiv_lang'] === "es"): ?>
              <a href="./?action=setlang&lang=en" class="btn btn-header-menu-fixed"><i class="fa fa-flag"></i> En</a>
              <?php else: ?>
              <a href="./?action=setlang&lang=es" class="btn btn-header-menu-fixed"><i class="fa fa-flag"></i> Es</a>
              <?php endif ?>
            <?php else: ?>
              <a href="./?action=setlang&lang=en" class="btn btn-header-menu-fixed"><i class="fa fa-flag"></i> En</a>
            <?php endif ?>
            <!-- BUSCADOR DE ACTORES -->
            <?php if(Session::exists("user_id")):?>
            <form class="navbar-form navbar-left" role="search">
              <div class="form-group">
                <input type="hidden" name="view" value="search">
                <input type="text" class="form-control" name="q" placeholder="<?php echo translate('Buscar actores'); ?> ...">
              </div>
              <button type="submit" class="btn btn-primary">&nbsp;<i class="fa fa-search"></i>&nbsp;</button>
            </form>
            <?php endif; ?>

            <!-- MENÚ CON SESIÓN -->
            <?php
            if(Session::exists("user_id")):
            if(!Session::$user->is_admin && Session::$user->tipoRel==1):
            $conversations = ConversationData::getConversations($_SESSION["user_id"]);
            $nmsgs = 0;
            foreach ($conversations as $conversation) {
            $nmsg = MessageData::countUnReadsByUC($_SESSION["user_id"],$conversation->id);
            $nmsgs += $nmsg->c;
            }
            $nnots = NotificationData::countUnReads($_SESSION["user_id"]);
            ?>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown messages-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="label <?php if($nnots->c>0){ echo "label-danger"; }else{ echo "label-default"; }?>"><?php echo $nnots->c;?></span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <?php if($nnots->c>0):
                  $notifications = NotificationData::getLast5($_SESSION["user_id"]);
                  ?>
                  <!-- NOTIFICACIONES -->
                  <li class="dropdown-header"><?php echo $nnots->c; ?> <?php echo translate('Notificaciones'); ?></li>
                  <?php
                  foreach($notifications as $noti):
                    $s = $noti->getSender();
                    $sp = ProfileData::getByUserId($s->id);
                    $img_url ="";
                    if($sp->image!="assets/img/home/perfil.png"){
                      $img_url = "storage/users/".$s->id."/profile/".$sp->image;
                    }else{
                      $img_url = $sp->image;
                    }
                    ?>
                    <li class="message-preview">
                      <a href="./?view=notifications" >
                        <span class="avatar"><img src="<?php echo $img_url; ?>" style='width:40px;'></span>
                        <span class="name"><?php echo $s->getFullname(); ?></span>
                        <span class="message">
                          <?php echo NotificationData::getMessageById($noti->id,false); ?>
                        </span>
                        <span class="time"><i class="fa fa-clock-o"></i><?php echo $noti->created_at; ?></span>
                      </a>
                    </li>
                    <li class="divider"></li>
                  <?php endforeach; ?>
                  <?php endif; ?>
                  <li><a href="./?view=notifications"><?php echo translate('Ver notificaciones'); ?></a></li>
                </ul>
              </li>
              <!-- AMIGOS -->
              <li><a href="./?view=friendreqs"><i class="fa fa-users"></i> <?php $fq = FriendData::countUnReads(Session::$user->id); if($fq->c>0){ echo "<span class='label label-danger'>$fq->c</span>";} else{ echo "<span class='label label-default'>0</span>";} ?></a></li>
              <!-- MENSAJES -->
              <li><a href="./?view=conversations"><i class="fa fa-envelope-o"></i> <?php if($nmsgs>0){ echo "<span class='label label-danger'>$nmsgs</span>";} else{ echo "<span class='label label-default'>0</span>";} ?></a></li>
              <!-- MENÚ DEL ACTOR -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo Session::$user->name;?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="./?view=home"><?php echo translate('Perfil'); ?></a></li>
                  <!--<li><a href="./?view=user&id=<?php echo Session::$user->id; ?>">Perfil Publico</a></li>-->
                  <li><a href="./?view=editinformation"><?php echo translate('Editar Información'); ?></a></li>
                  <li><a href="./?view=configuration"><?php echo translate('Configuración'); ?></a></li>
                  <li class="divider"></li>
                  <li><a href="./?action=processlogout"><?php echo translate('Salir'); ?></a></li>
                </ul>
              </li>
            </ul>
          <?php endif; // if session exist ?>
          <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
