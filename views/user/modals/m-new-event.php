<?php
session_start();
require_once('../../../core/lang/lang.php');
?>
<style type="text/css">
  /* edit */
.ui-autocomplete, #map_canvas {
    z-index: 1051 !important;
    list-style: none;
    padding: 0;
}
</style>
<div class="modal fade" id="m-new-event" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="saveEvent">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Nuevo Evento')?></h4>
        </div>
        <div class="modal-body">
            <div class="row row-margin-bottom">
              <div class="col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="txt__Name"><?=translate('Nombre')?>:</label>
                    <input type="text" class="form-control" id="txt__Name" placeholder="<?=translate('Nombre del evento')?>" required data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="cmb__Type"><?=translate('Tipo de Evento')?>:</label>
                    <select class="form-control" id="cmb__Type" required>
                      <option value=""><?=translate('Selecciona un tipo')?></option>
                      <option value="FESTIVAL"><?=translate('Festival')?></option>
                      <option value="TALLER"><?=translate('Taller')?></option>
                      <option value="CONFERENCIA"><?=translate('Conferencia')?></option>
                      <option value="EXPOSICION"><?=translate('Exposición')?></option>
                    </select>
                  </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="txt__InitialDate"><?=translate('Fecha de inicio')?>:</label>
                    <input type="text" class="form-control" autocomplete="off" id="txt__InitialDate" placeholder="<?=translate('Fecha de inicio')?>" required data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="txt__EndDate"><?=translate('Fecha final')?>:</label>
                    <input type="text" class="form-control" autocomplete="off" id="txt__EndDate" placeholder="<?=translate('Fecha final')?>" required data-validate-empty>
                  </div>
              </div>
              <div id="timeValidator">
                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="txt__StartHour"><?=translate('Hora inicio')?>:</label>
                      <input type="text" class="form-control time start"   autocomplete="off" id="txt__StartHour" required>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="txt__EndHour"><?=translate('Hora fin')?>:</label>
                     <input type="text" class="form-control time end" autocomplete="off" id="txt__EndHour" required data-validate-empty>
                    </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="txt__Url"><?=translate('URL al evento')?>:</label>
                    <input type="text" class="form-control" id="txt__Url" placeholder="<?=translate('URL al evento')?>">
                  </div>
              </div>
              <div class="col-sm-6 col-md-6 col-lg-6">
                  <div class="form-group">
                    <label for="txt__Access"><?=translate('Acceso')?>:</label>
                    <select type="text" class="form-control" id="txt__Access" required>
                      <option value="PUBLICO"><?=translate('Público')?></option>
                      <option value="PRIVADO"><?=translate('Privado')?></option>
                    </select>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="txt__Place"><?=translate('Lugar del evento')?>:</label>
                  <input type="text" name="_ubicacion" id="txt__Place" class="form-control"  placeholder="<?=translate('Mi ubicación')?>" required data-validate-empty>
                  <label class="hide">Latitud</label>
                  <input type="hidden" name="lat" id="lat" />
                  <label class="hide">Longitud</label>
                  <input type="hidden" name="lng" id="lng" />
                </div>
                <div class="form-group">
                  <div id="map_canvas" style="width: 400px; height: 280px; margin: 0 auto;"></div>
                  <div id="callback_result" class="hidden"></div>
                  <label><?=translate('¿Actualizar marcador?')?></label>
                  <select name="reverseGeocode" id="reverseGeocode" class="form-control">
                    <option value="false" selected><?=translate('No')?></option>
                    <option value="true"><?=translate('Si')?></option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description"><?=translate('Descripción')?>:</label>
                    <textarea class="form-control" id="txt__Description" placeholder="<?=translate('Descripción del evento')?>" required data-validate-empty></textarea>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="cmb__Initiatives"><?=translate('Ligar con iniciativa')?>:</label>
                    <select type="text" class="form-control" id="cmb__Initiatives">
                    </select>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="objetivos_list"><?=translate('Objetivos')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="objetivos_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <button type="button" class="btn btn-primary" id="btnShowObjetive"><?=translate('Añadir Objetivo')?></button>
                  </div>
                  <div class="form-group hide" id="inputObjetive">
                    <div class="row">
                      <div class="col-sm-12 col-md-12 col-lg-9">
                        <input type="text" name="new_objetive" id="txt__NewObjetive" class="form-control"  placeholder="<?=translate('Añade un nuevo objetivo')?>" >
                      </div>
                      <div class="col-sm-12 col-md-12 col-lg-3">
                        <button type="button" class="btn btn-primary" id="btnAddObjetive"><?=translate('Confirmar')?></button>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="invitados_list"><?=translate('Añadir Colaboradores')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="invitados_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="dropzone" id="eventPhoto"></div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert" role="alert" id="panel-alert">
                    <span id="icon"> </span> <span id="msg"> </span>
                  </div>
              </div>
              <input type="hidden" id="txt__user">
              <input type="hidden" id="EstateCity">
              <input type="hidden" id="EstateColony">
              <input type="hidden" id="EstateState">
              <input type="hidden" id="EstateCountry">
              <input type="hidden" id="EstateZipcode">
              <input type="hidden" id="EstateAddress">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
            <button id="btnAddEvent" type="submit" class="btn btn-primary"><?=translate('Guardar')?> <i class="fa fa-floppy-o"></i></button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  $(function () {

    // Listado de objetivos
    var btnShowObjetive = $('#btnShowObjetive');
    var btnAddObjetive  = $('#btnAddObjetive');
    var inputObjetive   = $('#inputObjetive');
    $objetivos_list     = $('#objetivos_list');
    $invitados_list     = $('#invitados_list');

    btnShowObjetive.on('click',showObjetive);
    function showObjetive(e){
      e.preventDefault();

      if(inputObjetive.hasClass('hide')){
        inputObjetive.removeClass('hide');
      }
    }

    btnAddObjetive.off('click').on('click', addObjetive);
    function addObjetive(e){
      e.preventDefault();

      // Valida campo de descripción
      var descripcion = $('#txt__NewObjetive').val();
      if(descripcion == "" || description == null){
        setFlash('<?=translate('label errordescobjetivo')?>.', 'error');
        return false;
      }
      $.post('api/front/objetivo.php', {'method': 'save', 'descripcion': descripcion})
        .done(function( _res ) {
          console.log(_res);
          if(+_res > 0){
            setFlash('<?=translate('label addobjetivo')?>', 'success');
            $('#txt__NewObjetive').val('');
            // Destruir y construir listado
            $objetivos_list.selectpicker('destroy');
            cargarListaObjetivos();
          } else {
            setFlash('El objetivo no se ha añadido: ' + _res, 'error');
          }
        })
        .error(function( _err ){
          console.log( _err );
        });
    }

    function cargarListaObjetivos(){
      $.post('api/front/objetivo.php', {'method': 'all'})
        .done(function( _res ) {
          var options = '';
          $objetivos_list.html('');
          $.each(_res,function(index, value){
            options += '<option value=' + value.id + '>' + value.descripcion + '</option>';
          });
          $objetivos_list.append(options);
          $objetivos_list.selectpicker({
            style: 'btn-primary',
            size: 4,
            liveSearch: true,
            width: "100%",
            maxOptions: 5,
            actionsBox: false,
            title: '<?=translate('label listaobjetivos')?>.',
            tickIcon: 'fa fa-key'
          });
          initModal();
        })
        .error(function( _err ){
          console.log( _err );
        });
    }

    // Carga de sugerencias de colaboradores
    function cargarListaInvitados(){

      var user_id = $("#txt__user").val();
      $.post('api/front/sugerencia.php', {'method': 'actorsByEnfoque', 'user_id': user_id, 'friends_inyect': true, 'same_rol': true })
        .done(function( _res ) {
          var _res = JSON.parse(_res);
          var options = '';
          $invitados_list.html('');
          $.each(_res,function(index, value){
            options += '<option value=' + value.user_id + '>' + value.user_name + '</option>';
          });
          $invitados_list.append(options);
          $invitados_list.selectpicker({
            style: 'btn-primary',
            size: 4,
            liveSearch: true,
            width: "100%",
            maxOptions: 5,
            actionsBox: false,
            title: '<?=translate('label listainvitados')?>.',
            tickIcon: 'fa fa-key'
          });
        })
        .error(function( _err ){
          console.log( _err );
        });
    }

    function initModal(){
      /*
      * Maps
      */
      var addresspickerMap = $('#txt__Place').addresspicker({
          regionBias: "mx",
          mapOptions: {
            zoom: 16,
            center: new google.maps.LatLng('20.7217961, -103.38968779999999'),
            scrollwheel: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false
          },
          elements: {
              map:                          "#map_canvas",
              lat:                          "#lat",
              lng:                          "#lng",
              type:                         "#type",
              locality:                     '#EstateCity',
              sublocality:                  '#EstateColony',
              administrative_area_level_1:  '#EstateState',
              country:                      '#EstateCountry',
              postal_code:                  '#EstateZipcode',
              route:                        '#EstateAddress'
          }
      });
      addresspickerMap;

      var gmarker = addresspickerMap.addresspicker("marker");
      gmarker.setVisible(true);
      addresspickerMap.addresspicker("updatePosition");
      var map = addresspickerMap.addresspicker("map");

      $('#reverseGeocode').change(function(){
        $("#address").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
      });
    }

    $('#m-new-event').on("shown.bs.modal",function(){
      cargarListaInvitados();
      cargarListaObjetivos();
    });

  });

  // Combo Iniciativas
  $cmbInitiatives = $('#cmb__Initiatives');
  $.ajax({
    url: "api/front/iniciativa.php",
    method: 'POST',
    dataType: 'JSON',
    data: {
      method:  'byUserIdAndInCollaboration'
    }
  })
  .done(function(data) {
    var opciones = "<option value=''><?=translate('Selecciona una..')?></option>";
    $.each(data, function (i, options) {
      opciones = opciones + '<option value="' + options.idIniciativa + '" >' + options.titulo + '</option>';
    });
    $cmbInitiatives.html(opciones);
  })
  .fail(function(_err) {
    console.log(_err);
  });

  // Datepickers
  $("#txt__InitialDate, #txt__EndDate").datepicker({ dateFormat: 'yy-mm-dd' });
  $('#txt__StartHour').timepicker({ 'step': 30  });
  $("#txt__StartHour").keypress(function(event) {event.preventDefault();});
  $('#txt__EndHour').timepicker({ 'step': 30  });//.timepicker('option', { useSelect: true });
  //$('#txt__EndHour').timepicker({ 'step': 30 }).timepicker('option', { useSelect: true });
  $('#timeValidator .time').timepicker({
    'showDuration': true,
    'timeFormat': 'g:ia'
  });

  var timeOnlyExampleEl = document.getElementById('timeValidator');
  var timeOnlyDatepair = new Datepair(timeOnlyExampleEl);

  //PHOTOS
  Dropzone.autoDiscover = false;
  var photo_name = "";
  var fileList = new Array;
  var i =0;
  $("div#eventPhoto").dropzone({
    acceptedFiles : '.jpeg, .png, .gif, .jpg',
    dictDefaultMessage: "¡<?= translate('label cargarFotoEvent')?>!",
    addRemoveLinks: true,
    init: function() {

        // Hack: Add the dropzone class to the element
        $(this.element).addClass("dropzone");

        this.on("success", function(file, serverFileName) {
            fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };
            photo_name = fileList[i].serverFileName;
            i++;
        });
        this.on("removedfile", function(file) {
            var rmvFile = "";
            for(f=0;f<fileList.length;f++){

                if(fileList[f].fileName == file.name)
                {
                    rmvFile = fileList[f].serverFileName;
                }
            }
            if (rmvFile){
                $.ajax({
                    url: "api/front/a-deleteitems.php",
                    type: "POST",
                    data: { "fileList" : rmvFile }
                });
            }
        });
    },
    url: "api/front/e-uploads.php"
  });

  //$('#timepicker1').timepicker();
  var evento = new Evento();
  //REQUEST SAVE
  $(document).off('submit').on('submit', '#saveEvent', function(e){
      e.preventDefault();
      
      if ( !isEmptyString( $(this).attr('id') ) ){
        if($objetivos_list.val() == null || $objetivos_list.val().length < 3){
          setFlash('<?=translate('label error3maxObjetivo')?>', 'warning');
          return false;
        } else if($objetivos_list.val() == null || $objetivos_list.val().length > 5){
          setFlash('<?=translate('label error5maxObjetivo')?>', 'warning');
          return false;
        }
        /* validate dates */
        var startDate = $("#txt__InitialDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
        var endDate = $("#txt__EndDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
         var currentDate = new Date();
        //setFlash(startDate,'warning');
        if( endDate<currentDate){
          setFlash("<?= translate('label errorfechalfinal')?>", 'warning');
          return false;
        }
        /* valida fecha final con inicial */
        if( (moment(startDate).format('Y-M-D')) > (moment(endDate).format('Y-M-D')) ) {
          setFlash("<?=translate('label errorfechainicial')?>", 'warning');
          return false;
        }
        var data = {
          '_title'        : $("#txt__Name").val(),
          '_eventType'    : $("#cmb__Type").val(),
          '_initialDate'  : $("#txt__InitialDate").val(),
          '_endDate'      : $("#txt__EndDate").val(),
          '_access'       : $("#txt__Access").val(),
          '_url'          : $("#txt__Url").val(),
          '_place'        : $("#txt__Place").val(),
          '_initiatives'  : $("#cmb__Initiatives").val(),
          '_lat'          : $("#lat").val(),
          '_lng'          : $("#lng").val(),
          '_photoEvent'   : photo_name,
          '_description'  : $("#txt__Description").val(),
          '_initialHour'  : $("#txt__StartHour").val(),
          '_endHour'      : $("#txt__EndHour").val(),
          '_userid'       : $("#txt__user").val(),
          '_objetivos'    : $objetivos_list.val(),
          '_colaboradores': $invitados_list.val(),
          '_estado'       : $("#EstateState").val(),
          '_pais'         : $("#EstateCountry").val(),
          '_ciudad'       : $("#EstateCity").val(),
          '_method'       : 'save'
        }
        evento._set(data);

      }

  });

</script>
