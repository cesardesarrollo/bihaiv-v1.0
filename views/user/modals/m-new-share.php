<?php
session_start();
require_once('../../../core/lang/lang.php');
?>
<div class="modal fade" id="m-new-share" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="saveShare">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Nueva Aportación')?></h4>
        </div>
        <div class="modal-body">
            <div class="row row-margin-bottom">
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Name">* <?=translate('Título')?>:</label>
                    <input type="text" class="form-control" id="txt__Name" placeholder="<?=translate('Nombre de la aportación')?>" required data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description">* <?=translate('Descripción')?>:</label>
                    <textarea class="form-control" id="txt__Description" placeholder="<?=translate('Descripción de la aportación')?>" required data-validate-empty></textarea>
                  </div>
              </div>
              <!-- Estos campos no se deben visualizar -->
              <div style="display: block;">
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                      <label for="thumbnail">* <?=translate('Imagen')?>:</label>
                      <input type="text" class="form-control" id="thumbnail" required readonly>
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                      <label for="file">* <?=translate('Archivo')?>:</label>
                      <input type="text" class="form-control" id="file" required readonly>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="dropzone" id="sharePhoto"></div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert" role="alert" id="panel-alert">
                    <span id="icon"> </span> <span id="msg"> </span>
                  </div>
              </div>
              <input type="hidden" id="txt__user">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
            <button id="btnAddShare" type="submit" class="btn btn-primary"><?=translate('Guardar')?> <i class="fa fa-floppy-o"></i></button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  function validateFilesAttached(image_file, document_file){
    var error = '';
    var result = true;

    if(image_file == '') error = '<?=translate("Además de un documento, es necesario subir una imagen JPG, JPEG o PNG")?>';
    if(document_file == '') error = '<?=translate("Además de una imagen, es necesario subir un archivo PDF, DOC, DOCX, PPTX, XLS o XLSX")?>';

    if(error != ''){
      result = false;

      if(!$("#row-msg").is(':visible')){
        $("#row-msg").slideDown('fast');
      }

      $("#panel-alert").addClass('alert-warning');
      $("#icon").html('<i class="fa fa-warning"></i>');

      $("#msg").html(error);
    }else{
      $("#row-msg").slideUp('fast');
    }

    return result;
  }

  $(function(){
    Dropzone.autoDiscover = false;
    var fileList = new Array;
    var i =0;
    $("div#sharePhoto").dropzone({
        acceptedFiles : '.pdf, .doc , .docx, .png, .jpg, .jpeg',
        addRemoveLinks: true,
        init: function() {
            // Hack: Add the dropzone class to the element
            $(this.element).addClass("dropzone");
            this.on("success", function(file, serverFileName) {
                fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };

                if(file.type.startsWith('image')){
                    $('#thumbnail').val(fileList[i].serverFileName);
                }else{
                    $('#file').val(fileList[i].serverFileName);
                }

                i++;
            });
            this.on("removedfile", function(file) {
                var rmvFile = "";
                for(f=0;f<fileList.length;f++){

                    if(fileList[f].fileName == file.name)
                    {
                        rmvFile = fileList[f].serverFileName;
                    }
                }

                /* delete from inputs */
                /* img*/
                if( $('#thumbnail').val() == rmvFile ){
                  $('#thumbnail').val('');
                }
                /* file*/
                if( $('#file').val() == rmvFile ){
                  $('#file').val('');
                }

                if (rmvFile){
                    $.ajax({
                        url: "api/front/a-deleteitems.php",
                        type: "POST",
                        data: { "fileList" : rmvFile }
                    });
                }
            });
        },
        url: "api/front/a-uploads.php"
    });
    var share = new Aportacion();
    $(document).off('submit').on('submit', '#saveShare', function(e){
        e.preventDefault();
        if ( !isEmptyString( $(this).attr('id') )){
          if( validateFilesAttached($('#thumbnail').val(), $('#file').val()) ){
            var data = {
              '_title'        : $("#txt__Name").val(),
              '_description'  : $("#txt__Description").val(),
              '_type'         : 3,
              '_photoCover'   : $('#thumbnail').val(),
              '_file'         : $('#file').val(),
              '_category'     : 0,
              '_userid'       : parseInt( $("#txt__user").val() ),
              '_method'       : 'save'
            }
            share._set(data);
          }
        }
    });
  });

</script>
