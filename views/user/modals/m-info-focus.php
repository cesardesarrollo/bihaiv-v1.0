<?php 
session_start();
require('../../../core/lang/lang.php');
?>
<div class="modal fade" id="modal-info-focus" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="editFocus">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Editar mi enfoque')?></h4>
        </div>
        <div class="modal-body">
            <div class="row row-margin-bottom">
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description"><?=translate('Descripción')?>:</label>
                    <textarea class="form-control" id="txt__Description" required data-validate-empty></textarea>
                  </div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert" role="alert" id="panel-alert">
                    <span id="icon"> </span> <span id="msg"> </span>
                  </div>
              </div>
              <input type="hidden" id="txt__user">
              <input type="hidden" id="txt__post">
              <input type="hidden" id="txt__id">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
            <button id="btnSubmit" type="submit" class="btn btn-primary"><?=translate('Guardar cambios')?> <i class="fa fa-floppy-o"></i></button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var focus = new Enfoque();
$(document).off('submit').on('submit', '#editFocus', function(e){
    e.preventDefault();
    if ( !isEmptyString( $(this).attr('id') ) ){
        var data = {
          '_description'  : $("#txt__Description").val(),
          '_type'   : 1,
          '_method' : 'edit',
          '_userid' : parseInt( $("#txt__user").val() ),
          '_post_id' : parseInt( $("#txt__post").val() ),
          '_id' : parseInt( $("#txt__id").val() )
        };
        focus._set(data);
    }
});
</script>