<?php 
session_start();
require_once('../../../core/lang/lang.php');
require_once '../../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../../core/modules/index/model/DaoUbicaciones.php';
$DaoOrganizacion = new DaoOrganizacion();
$organizacion    = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);

$DaoUbicaciones = new DaoUbicaciones();
$Ubicaciones    = new Ubicaciones();
$Ubicaciones    = $DaoUbicaciones->getMainByOrgId( $organizacion->id );

?>
<div class="modal fade" id="m-info-initiative" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="editLead">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Iniciativa')?></h4>
        </div>
        <div class="modal-body">
            <div class="row margin-row-bottom">
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for="txt__Name"><?=translate('Nombre')?>:</label>
                        <input type="text" class="form-control" id="txt__Name" placeholder="<?=translate('Nombre')?>" data-validate-empty required>
                      </div>
                  </div>
                    <div id="timeValidator"  class="col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for="txt__StartHour"><?=translate('Hora inicio')?>:</label>
                        <input type="text" class="form-control time start" id="txt__StartHour" required>
                      </div>
                      <div class="form-group">
                        <label for="txt__EndHour"><?=translate('Hora fin')?>:</label>
                        <input type="text" class="form-control time end" id="txt__EndHour" required data-validate-empty>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-sm-12 col-md-4 col-lg-6">
                  <div class="form-group">
                    <label for="txt__InitialDate"><?=translate('Fecha de inicio')?>:</label>
                    <input type="text" class="form-control" id="txt__InitialDate" autocomplete="off" placeholder="<?=translate('Fecha de inicio')?>" required data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-12 col-md-4 col-lg-6">
                  <div class="form-group">
                    <label for="txt__EndDate"><?=translate('Fecha final')?>:</label>
                    <input type="text" class="form-control" id="txt__EndDate" autocomplete="off" placeholder="<?=translate('Fecha final')?>" required data-validate-empty>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description"><?=translate('Descripción')?>:</label>
                    <textarea class="form-control" placeholder="<?=translate('Descripción de la iniciativa')?>" id="txt__Description" required data-validate-empty></textarea>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="invitados_list"><?=translate('Colaboradores Actuales')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="form-control" id="invitados_current_list" readonly>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12" id="label_add_collaborators">
                  <div class="form-group">
                    <label for="invitados_list"><?=translate('Añadir Colaboradores')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="invitados_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="eventos_list"><?=translate('Eventos Actuales')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="form-control" id="eventos_current_list" readonly>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12" id="label_add_events">
                  <div class="form-group">
                    <label for="eventos_list"><?=translate('Añadir Eventos')?>:</label>
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="form-group">
                          <select class="selectpicker form-control show-menu-arrow" id="eventos_list" multiple>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="cmb__Alcanze"><?=translate('Alcance')?>:</label>
                  <!--  onchange="seteaUbicacion(this.options[this.selectedIndex].text)"  -->
                  <select class="form-control" name="idAlcanze" id="cmb__Alcanze" value=""  >
                    <option value=""><?=translate('Selecciona una opción')?></option>
                    <?php
                    $alcanzes = $DaoUbicaciones->getAlcanzes();
                    foreach ($alcanzes as $alcanze ) {
                      echo '<option value="'.$alcanze['id'].'" name="'.$alcanze['nombre'].'">'.$alcanze['nombre'].'</option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="form-group">
                  <label for=""><?=translate('Ubicacion(es) de alcance')?>:</label>
                  <button id="btn_agregar_ubicacion" type="button" class="btn btn-primary pull-right" onclick='$("#newUbication").slideToggle("fast");'>
                    <?=translate('Agregar ubicación')?>
                  </button>
                  <div id="newUbication" class="row margin-row-top" style="display:none;" id="cmb__Country_field">
                    <div class="col-lg-6 col-md-6">
                      <label for="cmb__Country"><?=translate('País')?>:</label>
                      <select class="form-control" id="cmb__Country" ></select>
                    </div>
                    <div class="col-lg-6 col-md-6" id="cmb__State_field">
                      <label for="cmb__State"><?=translate('Estado')?>:</label>
                      <select class="form-control" id="cmb__State" ></select>
                    </div>
                    <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__Municipe_field">
                      <label for="cmb__Municipe"><?=translate('Municipio')?>:</label>
                      <select class="form-control" id="cmb__Municipe" ></select>
                    </div>
                    <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__city_field">
                      <label for="cmb__city"><?=translate('Localidad')?>:</label>
                      <select class="form-control" id="cmb__city"></select>
                    </div>
                    <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__Influencia_field">
                      <label for="cmb__Influencia"><?=translate('¿Influye en el ecosistema?')?>:</label>
                      <select class="form-control" id="cmb__Influencia">
                        <option value="1" ><?=translate('Con Influencia')?> </option>
                        <option value="0" ><?=translate('Sin Influencia')?> </option>
                      </select>
                    </div>
                    <div class="col-xs-6 col-lg-6 margin-row-top">
                      <button id="addUbication" type="button" class="btn btn-primary pull-right">
                        <?=translate('Guardar')?>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <table class="table table-bordered bihaiv_table margin-row-top hide" id="otrasUbicaciones">
                    <thead>
                      <tr>
                        <td><?=translate('País')?></td>
                        <td><?=translate('Estado')?></td>
                        <td><?=translate('Municipio')?></td>
                        <td><?=translate('Localidad')?></td>
                        <td><?=translate('Influye')?></td>
                        <th><?=translate('Eliminar')?></th>
                      </tr>
                    </thead>
                    <tbody id="lstUbications">
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <figure class="figure">
                    <img id="img-iniciativa" src="" class="img-responsive">
                  </figure>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="dropzone" id="initiativePhoto"></div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert" role="alert" id="panel-alert">
                    <span id="icon"> </span> <span id="msg"> </span>
                  </div>
              </div>
              <input type="hidden" id="txt__iniciativa">
              <input type="hidden" id="txt__post">
              <input type="hidden" id="txt__user">
              <input type="hidden" id="action">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
            <button type="submit" class="btn btn-primary btn-edit"><?=translate('Guardar cambios')?> <i class="fa fa-floppy-o"></i></button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

  var view = false;
  function initUbicaciones(){

    $("#cmb__Alcanze").on("change", function(e){
      e.preventDefault();
      seteaUbicacion(this.options[this.selectedIndex].text);
    });

    // Ubicaciones
    var pais_id             = $("#pais_id");
    var estado_id           = $("#estado_id");
    var municipio_id        = $("#municipio_id");
    var localidad_id        = $("#localidad_id");

    var cmb__Country        = $("#cmb__Country");
    var cmb__State          = $("#cmb__State");
    var cmb__Municipe       = $("#cmb__Municipe");
    var cmb__city           = $("#cmb__city");

    var pais_id_field       = $("#cmb__Country_field");
    var estado_id_field     = $("#cmb__State_field");
    var municipio_id_field  = $("#cmb__Municipe_field");
    var localidad_id_field  = $("#cmb__city_field");
    var influencia_field    = $("#cmb__Influencia_field");

    // Carga Paises
    $.post('api/front/paises.php', {'method' : 'all'})
    .done(function( data ){
      cmb__Country.html('');
      var option = '<option value="">Selecciona un país</option>';
      $.each(JSON.parse(data), function(k,v){
        option += '<option id="' + v.prefijo + '" value="' + v.id + '">' + v.nombre + '</option>';
      })
      cmb__Country.html(option);
      option = "";
    });

    cmb__Country.on('change',function(e){
      e.preventDefault();

      var id_country = $(this).val();
      if(estado_id_field.length){

        cmb__State.html('');
        $.post('api/front/estados.php', {'method' : 'byPaisId', 'id' : id_country})
        .done(function(data){
          var option = '<option value=""><?= translate('Selecciona un estado')?></option>';
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.abrev + '" value="' + v.id + '">' + v.nombre + '</option>';
          });
          cmb__State.html(option);
          option = "";
        })
      }
    });

    cmb__State.on('change',function(e){
      e.preventDefault();

      var id_state = $(this).val();
      if(municipio_id_field.length){

        cmb__Municipe.html('');
        $.post("api/front/municipios.php", {'method': 'byEstadoId', 'id' : id_state})
        .done(function(data){
        var option = '<option value=""><?= translate('Selecciona un municipio')?></option>';
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.clave + '" value="' + v.id + '">' + v.nombre + '</option>';
          });
          cmb__Municipe.html(option);
          option = "";
        });
      }
    });

    cmb__Municipe.on('change',function(e){
      e.preventDefault();

      var id_municipe = $(this).val();
      if(localidad_id_field.length){

        cmb__city.html('');
        $.ajax({
          url   : 'api/front/localidades.php',
          type  : 'POST',
          data  : { method : 'byMunicipioId', id : id_municipe},
          async : false,
        })
        .done(function(data){
        var option = '<option value=""><?= translate('Selecciona una localidad')?></option>';
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.clave + '" value="' + v.id + '">' + v.nombre + '</option>';
          });
          cmb__city.html(option);
          option = "";
        })
        .fail(function(err){
          console.log( err );
        })
      }
    });

    function seteaUbicacion(alcanze){
      // Esconde
      if(!pais_id_field.hasClass('hide')){
        pais_id_field.addClass('hide');
      }
      if(!estado_id_field.hasClass('hide')){
        estado_id_field.addClass('hide');
      }
      if(!municipio_id_field.hasClass('hide')){
        municipio_id_field.addClass('hide');
      }
      if(!localidad_id_field.hasClass('hide')){
        localidad_id_field.addClass('hide');
      }
      if(!influencia_field.hasClass('hide')){
        influencia_field.addClass('hide');
      }

      // nacional
      if (alcanze === 'NACIONAL'){
        console.log(pais_id_field);
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(!estado_id_field.hasClass('hide')){
          estado_id_field.addClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // regional
      else if (alcanze === 'REGIONAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // estatal
      else if (alcanze === 'ESTATAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // local
      else if (alcanze === 'LOCAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(municipio_id_field.hasClass('hide')){
          municipio_id_field.removeClass('hide');
        }
        if(localidad_id_field.hasClass('hide')){
          localidad_id_field.removeClass('hide');
        }
        if(influencia_field.hasClass('hide')){
          influencia_field.removeClass('hide');
        }
      }
    }
    seteaUbicacion(document.getElementById('cmb__Alcanze').options[document.getElementById('cmb__Alcanze').selectedIndex].text);


    function activaEliminacion(){
      // Elimina ubicación
      $('.delUbication').on('click', delUbication);

      function delUbication(e){
        e.preventDefault();
        var id = $(this).attr('name');
        $.post('api/front/ubicaciones.php', {'method' : 'delete_ubicacion_iniciativa', 'id': id})
        .done(function( data ){
          if (data) {
            drawLstUbications();
            alert('Ubicación eliminada');
          } else {
            alert("Error al eliminar ubicación");
          }
        })
      }
    }

    function drawLstUbications(){
      $('#lstUbications').html('');

      var iniciativa_id = $("#txt__iniciativa").val();

      $.ajax({
        url   : 'api/front/ubicaciones.php',
        type  : 'POST',
        data  : {'method': 'byIniciativaId', 'iniciativa_id': iniciativa_id},
        async : false,
      })
      .done(function(data){

        var otrasUbicaciones = $('#otrasUbicaciones');
        if(JSON.parse(data).length > 0){
          otrasUbicaciones.removeClass("hide");
        } else {
          if(!otrasUbicaciones.hasClass("hide")){
            otrasUbicaciones.addClass("hide");
          }
        }

        var row = "";

        $.each(JSON.parse(data), function(k,v){
          row += "<tr>"
          row += "  <td>" + v.pais_nombre + "</td>"
          row += "  <td>" + v.estado_nombre + "</td>"
          row += "  <td>" + v.municipio_nombre + "</td>"
          row += "  <td>" + v.localidad_nombre + "</td>"
          row += "  <td>" + v.influencia + "</td>"
          if(!view){
            row += "  <td><button class='btn btn-primary delUbication' name='" + v.id + "'>X</button></td>"
          } else {
            row += "  <td></td>"
          }
          row += "</tr>";
        });
        $('#lstUbications').html(row);
      })
      .fail(function(err){
        console.log( err );
      });
      activaEliminacion();
    }

    // Agregar ubicación de sucursal/alcanze/oficina
    $('#addUbication').on('click',addUbication);
    function addUbication(e){
      e.preventDefault();

      var data = {
        'method'        : 'save_ubicaciones_iniciativas',
        'iniciativa_id' : $("#txt__iniciativa").val(),
        'pais_id'       : $("#cmb__Country").val(),
        'estado_id'     : $("#cmb__State").val(),
        'municipio_id'  : $("#cmb__Municipe").val(),
        'localidad_id'  : $("#cmb__city").val(),
        'is_main'       : $("#cmb__Influencia").val()
      }
      $.ajax({
        url   : 'api/front/ubicaciones.php',
        type  : 'POST',
        data  : data,
        async : false,
      })
      .done(function(data){
        $('#otrasUbicaciones').removeClass("hide");
        drawLstUbications();
        // Limpieza de formulario
        $("#cmb__Country").val('');
        $("#cmb__State").empty();
        $("#cmb__Municipe").empty();
        $("#cmb__city").empty();
      })
      .fail(function(err){
        console.log( err );
      })
    }
    drawLstUbications();

  }

  $(function () {

    //PHOTOS
    Dropzone.autoDiscover = false;
    var photo_name = "";
    var fileList = new Array;
    var i =0;
    $("div#initiativePhoto").dropzone({
      acceptedFiles : '.jpeg, .png, .gif, .jpg',
      dictDefaultMessage: "¡<?= translate('label cargarFotoIniciative') ?>!",
      addRemoveLinks: true,
      init: function() {

          // Hack: Add the dropzone class to the element
          $(this.element).addClass("dropzone");

          this.on("success", function(file, serverFileName) {
              fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };
              photo_name = fileList[i].serverFileName;
              i++;
          });
          this.on("removedfile", function(file) {
              var rmvFile = "";
              for(f=0;f<fileList.length;f++){

                  if(fileList[f].fileName == file.name)
                  {
                      rmvFile = fileList[f].serverFileName;
                  }
              }
              if (rmvFile){
                  $.ajax({
                      url: "api/front/a-deleteitems.php",
                      type: "POST",
                      data: { "fileList" : rmvFile }
                  });
              }
          });
      },
      url: "api/front/i-uploads.php"
    });

    $("#txt__InitialDate").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#txt__EndDate").datepicker({ dateFormat: 'yy-mm-dd' });
    $('#txt__StartHour').timepicker({ 'step': 30 });
    $("#txt__EndHour").timepicker({'step' : 30});
    $('#timeValidator .time').timepicker({
      'showDuration': true,
      'timeFormat': 'g:ia'
    });

    var timeOnlyExampleEl = document.getElementById('timeValidator');
    var timeOnlyDatepair = new Datepair(timeOnlyExampleEl);

    //EDIT INITIATIVE
    var iniciativa = new Iniciativas();
    $(document).off('submit').on('submit', '#editLead', function(e){
        e.preventDefault();
        
        var imagen = (photo_name) ? photo_name : $("#img-iniciativa").attr('data-img');
        var startDate = $("#txt__InitialDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
        var endDate = $("#txt__EndDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
         var currentDate = new Date();
        //setFlash(startDate,'warning');
        if( endDate<currentDate){
          setFlash("<?= translate('label errorfechalfinal')?>", 'warning');
          return false;
        }
        /* valida fecha final con inicial */
        if( (moment(startDate).format('Y-M-D')) > (moment(endDate).format('Y-M-D')) ) {
          setFlash("<?=translate('label errorfechainicial')?>", 'warning');
          return false;
        }
        if ( !isEmptyString( $(this).attr('id') ) ){
            var data = {
              '_id'           : $("#txt__iniciativa").val(),
              '_userid'       : $("#txt__user").val(),
              '_postId'       : $("#txt__post").val(),
              '_title'        : $("#txt__Name").val(),
              '_initialDate'  : $("#txt__InitialDate").val(),
              '_endDate'      : $("#txt__EndDate").val(),
              '_description'  : $("#txt__Description").val(),
              '_actors'       : $("#invitados_list").val(),
              '_events'       : $("#eventos_list").val(),
              '_initialHour'  : $("#txt__StartHour").val(),
              '_endHour'      : $("#txt__EndHour").val(),
              '_idAlcanze'    : $("#cmb__Alcanze").val(),
              '_photoCover'   : imagen,
              '_method'       : 'edit'
            }
            iniciativa._set(data);
        }
    });

    function initModal(){

      // Si es solo vista o modo edición
      if($("#action").val() === 'view'){ 
        view = true;
      }
      if(view){
    		$("#btn_agregar_ubicacion, #label_add_collaborators, #label_add_events").css('display', 'none');
      }

      // Inicia configuración de initUbicaciones
      initUbicaciones();

      // Invitados relacionados
      $invitados_current_list = $('#invitados_current_list');
      function cargarListaInvitadosActuales(){
        var iniciativa_id = $("#txt__iniciativa").val();
        $.post('api/front/colaboracion.php', {'method': 'byIdIniciativa', 'iniciativa_id': iniciativa_id })
          .done(function( _res ) {
            var _res = JSON.parse(_res);
            var options = '';
            $invitados_current_list.html('');
            var key_array = [];
            $.each(_res,function(index, value){
              key_array.push(value.user_id);
              options += '<option value=' + value.user_id + '>' + value.user_name + '</option>';
            });
            $invitados_current_list.append(options);

            // Invitados no relacionados sugeridos
            $invitados_list     = $('#invitados_list');
            function cargarListaInvitados(){

              var user_id = $("#txt__user").val();

              $.post('api/front/sugerencia.php', {'method': 'actorsByEnfoqueExcept', 'user_id': user_id, 'key_array': key_array, 'friends_inyect': true, 'same_rol': true  })
                .done(function( _res ) {
                  var _res = JSON.parse(_res);
                  var options = '';
                  $invitados_list.html('');
                  $.each(_res,function(index, value){
                    options += '<option value=' + value.user_id + '>' + value.user_name + '</option>';
                  });
                  $invitados_list.append(options);
                  $invitados_list.selectpicker({
                    style: 'btn-primary',
                    size: 4,
                    liveSearch: true,
                    width: "100%",
                    maxOptions: 5,
                    actionsBox: false,
                    title: '<?=translate('label listainvitados')?>.',
                    tickIcon: 'fa fa-key'
                  });
                })
                .error(function( _err ){
                  console.log( _err );
                });
            }
            cargarListaInvitados();
          })
          .error(function( _err ){
            console.log( _err );
          });
      }
      cargarListaInvitadosActuales();


      // Eventos relacionados
      $eventos_current_list = $('#eventos_current_list');
      function cargarListaEventosActuales(){
        var iniciativa_id = $("#txt__iniciativa").val();
        $.post('api/front/iniciativa.php', {'method': 'iniciativaEventos', 'iniciativa_id': iniciativa_id })
          .done(function( _res ) {
            var _res = JSON.parse(_res);
            var options = '';
            $eventos_current_list.html('');
            var key_array = [];
            $.each(_res,function(index, value){
              key_array.push(value.id);
              options += '<option value=' + value.id + '>' + value.titulo + '</option>';
            });
            $eventos_current_list.append(options);

            // Eventos no relacionados sugeridos
            $eventos_list     = $('#eventos_list');
            function cargarListaEventos(key_array){

                  console.log('key_array',  key_array );
              var user_id = $("#txt__user").val();
              $.post('api/front/events.php', {'method': 'all', 'action': 'getAllEventsExceptOwns', 'key_array': key_array })
                .done(function( _res ) {
                  var _res = JSON.parse(_res);
                  var options = '';
                  $eventos_list.html('');
                  $.each(_res,function(index, value){
                    options += '<option value=' + value.idEvento + '>' + value.titulo + '</option>';
                  });
                  $eventos_list.append(options);
                  $eventos_list.selectpicker({
                    style: 'btn-primary',
                    size: 4,
                    liveSearch: true,
                    width: "100%",
                    maxOptions: 5,
                    actionsBox: false,
                    title: '<?=translate('label listaeventos')?>.',
                    tickIcon: 'fa fa-key'
                  });
                })
                .error(function( _err ){
                  console.log( _err );
                });
            }
            cargarListaEventos(key_array);
          })
          .error(function( _err ){
            console.log( _err );
          });
      }
      cargarListaEventosActuales();
    }

    $('#m-info-initiative').on("shown.bs.modal", initModal);

  });
</script>
