<?php 
  session_start();
  require('../../../core/lang/lang.php');

?>
<style type="text/css">
  /* edit */
.ui-autocomplete, #map_canvas {
    z-index: 1051 !important;
    list-style: none;
    padding: 0;
}
</style>
<div class="modal fade" id="m-info-event" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="exportEvent" method="post" action="api/front/phptoics.php">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Evento')?></h4>
        </div>
        <div class="modal-body">
          <div class="row row-margin-bottom">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="txt__Name"><?=translate('Nombre')?>:</label>
                  <input type="text" class="form-control" id="txt__Name" placeholder="<?=translate('Nombre del evento')?>" required data-validate-empty>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="cmb__Type"><?=translate('Tipo de Evento')?>:</label>
                  <select class="form-control" id="cmb__Type" required>
                    <option value=""><?=translate('Selecciona un tipo')?></option>
                    <option value="FESTIVAL"><?=translate('Festival')?></option>
                    <option value="TALLER"><?=translate('Taller')?></option>
                    <option value="CONFERENCIA"><?=translate('Conferencia')?></option>
                    <option value="EXPOSICION"><?=translate('Exposición')?></option>
                  </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="form-group">
                  <label for="txt__InitialDate"><?=translate('Fecha de inicio')?>:</label>
                  <input type="text" class="form-control" id="txt__InitialDate" autocomplete="off" placeholder="<?=translate('Fecha de inicio')?>" required data-validate-empty>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="form-group">
                  <label for="txt__EndDate"><?=translate('Fecha final')?>:</label>
                  <input type="text" class="form-control" id="txt__EndDate" autocomplete="off" placeholder="<?=translate('Fecha final')?>" required data-validate-empty onblur="isValidDate()">
                </div>
            </div>
            <div id="timeValidator">
                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="txt__StartHour"><?=translate('Hora inicio')?>:</label>
                      <input type="text" class="form-control time start" id="txt__StartHour" required>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                      <label for="txt__EndHour"><?=translate('Hora fin')?>:</label>
                      <input type="text" class="form-control time end" id="txt__EndHour" required data-validate-empty>
                    </div>
                </div>
              </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="txt__Url"><?=translate('URL al evento')?>:</label>
                  <input type="text" class="form-control" id="txt__Url" placeholder="<?=translate('URL al evento')?>" data-validate-empty>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="txt__Access"><?=translate('Acceso')?>:</label>
                  <select type="text" class="form-control" id="txt__Access" required>
                    <option value="PUBLICO"><?=translate('Público')?></option>
                    <option value="PRIVADO"><?=translate('Privado')?></option>
                  </select>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="txt__Place"><?=translate('Lugar del evento')?>:</label>
                <input type="text" name="_ubicacion" id="txt__Place" class="form-control"  placeholder="<?=translate('Mi ubicación')?>" required data-validate-empty>
                <label class="hide">Latitud</label>
                <input type="hidden" name="lat" id="lat" />
                <label class="hide">Longitud</label>
                <input type="hidden" name="lng" id="lng" />
              </div>
              
              <div class="form-group">
                <div id="map_canvas" style="width: 400px; height: 280px; margin: 0 auto;"></div>
                <!--
                <label><?=translate('¿Actualizar marcador?')?> </label>
                <select name="reverseGeocode" id="reverseGeocode" class="form-control">
                  <option value="false" selected><?=translate('No')?></option>
                  <option value="true"><?=translate('Si')?></option>
                </select>-->
              </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="txt__Description"><?=translate('Descripción')?>:</label>
                  <textarea class="form-control" id="txt__Description" placeholder="<?=translate('Descripción del evento')?>" required data-validate-empty></textarea>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="cmb__Initiatives"><?=translate('Ligar con iniciativa')?>:</label>
                  <select type="text" class="form-control" id="cmb__Initiatives">
                  </select>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="invitados_list"><?=translate('Colaboradores Actuales')?>:</label>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <select class="form-control" id="invitados_current_list" readonly>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="invitados_list"><?=translate('Añadir Colaboradores')?>:</label>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <select class="selectpicker form-control show-menu-arrow" id="invitados_list" multiple>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <figure class="figure">
                  <img id="img-event" src="" class="img-responsive">
                </figure>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="dropzone" id="eventPhoto"></div>
            </div>
            <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                <div class="alert" role="alert" id="panel-alert">
                  <span id="icon"> </span> <span id="msg"> </span>
                </div>
            </div>
            <!-- Exportación ICS -->
            <input type="hidden" id="txt__id" name="txt__id" />
            <input type="hidden" id="filename" name="filename" />
            <input type="hidden" id="datestart" name="datestart" />
            <input type="hidden" id="dateend" name="dateend" />
            <input type="hidden" id="summary" name="summary" />
            <input type="hidden" id="uri" name="uri" />
            <input type="hidden" id="address" name="address" />
            <input type="hidden" id="description_cal" name="description_cal" />
            <!-- Google maps -->
            <input type="hidden" id="EstateCity">
            <input type="hidden" id="EstateColony">
            <input type="hidden" id="EstateState">
            <input type="hidden" id="EstateCountry">
            <input type="hidden" id="EstateZipcode">
            <input type="hidden" id="EstateAddress">
            <input type="hidden" id="txt__user">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
          <?php if(isset($_SESSION['user_id'])): ?>
          <span id="ctrlBtns"></span>
          <?php endif; ?>
          <button id="btnExportEvent" type="submit" class="btn btn-success"><?=translate('Descarga ICS')?> <i class="fa fa-floppy-o"></i></button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  // Date pickers
  $("#txt__InitialDate, #txt__EndDate").datepicker({ dateFormat: 'yy-mm-dd' });

  $('#txt__StartHour').timepicker({ 'step': 30 });
  $("#txt__EndHour").timepicker({'step' : 30});
  $('#timeValidator .time').timepicker({
    'showDuration': true,
    'timeFormat': 'g:ia'
  });

  var timeOnlyExampleEl = document.getElementById('timeValidator');
  var timeOnlyDatepair = new Datepair(timeOnlyExampleEl);

  // Activar botones de edición/eliminación
  function activeCtrlButtons() {
    // Eliminar evento
    $('#btnEliminateEvent').on('click', deleteevent);
    function deleteevent(e){
      e.preventDefault();

      var evento = new Evento();
      var data = {
        '_id'    : $("#txt__id").val(),
        '_method': 'delete'
      }
      evento._set(data);
    }

    // Editar evento
    $('#btnUpdateEvent').on('click', updateevent);
    function updateevent(e){
      e.preventDefault();

      var imagen = (photo_name) ? photo_name : $("#img-event").attr('data-img');
        var startDate = $("#txt__InitialDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
        var endDate = $("#txt__EndDate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker('getDate');
         var currentDate = new Date();
        //setFlash(startDate,'warning');
        if( endDate<currentDate){
          setFlash("<?= translate('label errorfechalfinal')?>", 'warning');
          return false;
        }
        /* valida fecha final con inicial */
        if( (moment(startDate).format('Y-M-D')) > (moment(endDate).format('Y-M-D')) ) {
          setFlash("<?=translate('label errorfechainicial')?>", 'warning');
          return false;
        }
      var evento = new Evento();
      var data = {
        '_title'        : $("#txt__Name").val(),
        '_eventType'    : $("#cmb__Type").val(),
        '_initialDate'  : $("#txt__InitialDate").val(),
        '_endDate'      : $("#txt__EndDate").val(),
        '_access'       : $("#txt__Access").val(),
        '_url'          : $("#txt__Url").val(),
        '_place'        : $("#txt__Place").val(),
        '_initiatives'  : $("#cmb__Initiatives").val(),
        '_lat'          : $("#lat").val(),
        '_lng'          : $("#lng").val(),
        '_photoEvent'   : imagen,
        '_description'  : $("#txt__Description").val(),
        '_initialHour'  : $("#txt__StartHour").val(),
        '_endHour'      : $("#txt__EndHour").val(),
        '_userid'       : $("#txt__user").val(),
        '_colaboradores': $invitados_list.val(),
        '_id'           : $("#txt__id").val(),
        '_estado'       : $("#EstateState").val(),
        '_pais'         : $("#EstateCountry").val(),
        '_ciudad'       : $("#EstateCity").val(),
        '_method'       : 'edit'
      }
      evento._set(data);
    }
  }

  // Deshabilita forma
  function disableForm(){
    //$('#exportEvent').find('input, select, textarea').attr("disabled", true);
    $('#eventPhoto').css('display','none');

    $("#txt__Name").attr("disabled", true);
    $("#cmb__Type").attr("disabled", true);
    $("#txt__InitialDate").attr("disabled", true);
    $("#txt__EndDate").attr("disabled", true);
    $("#txt__Access").attr("disabled", true);
    $("#txt__Url").attr("disabled", true);
    $("#txt__Place").attr("disabled", true);
    $("#cmb__Initiatives").attr("disabled", true);
    $("#lat").attr("disabled", true);
    $("#lng").attr("disabled", true);
    $("#txt__Description").attr("disabled", true);
    $("#txt__StartHour").attr("disabled", true);
    $("#txt__EndHour").attr("disabled", true);
    $("#txt__user").attr("disabled", true);
    $invitados_list.attr("disabled", true);
    $("#EstateState").attr("disabled", true);
    $("#EstateCountry").attr("disabled", true);
    $("#EstateCity").attr("disabled", true);

  }

  // Validación de usuario propietario para edición
  function getUserLoged() {

    var session_user_id = "<?php if(isset($_SESSION['org_id'])) { echo @$_SESSION['org_id']; } ?>";
    session_user_id = +session_user_id;

    var org_id_event = $('#txt__user').val();
    org_id_event = +org_id_event;
    var str_btns = "";
    if (org_id_event == session_user_id){
      str_btns = '<button type="button" class="btn btn-primary" id="btnUpdateEvent"><?=translate('Guardar cambios')?> <i class="fa fa-floppy-o"></i></button> <button type="button" class="btn btn-danger" id="btnEliminateEvent"><?=translate('Eliminar')?> <i class="fa fa-floppy-o"></i></button>';
    }
    if(str_btns!==''){
      // Usuario propietario de perfil
      $('#ctrlBtns').html(str_btns);
      activeCtrlButtons();
    } else {
      // Usuario visitante
      disableForm();
    }
  }

  //PHOTOS
  Dropzone.autoDiscover = false;
  var photo_name = "";
  var fileList = new Array;
  var i =0;
  $("div#eventPhoto").dropzone({
    acceptedFiles : '.jpeg, .png, .gif, .jpg',
    dictDefaultMessage: "¡<?= translate('label cargafoto')?>!",
    addRemoveLinks: true,
    init: function() {

        // Hack: Add the dropzone class to the element
        $(this.element).addClass("dropzone");

        this.on("success", function(file, serverFileName) {
            fileList[i] = {"serverFileName" : serverFileName, "fileName" : file.name,"fileId" : i };
            photo_name = fileList[i].serverFileName;
            i++;
        });
        this.on("removedfile", function(file) {
            var rmvFile = "";
            for(f=0;f<fileList.length;f++){

                if(fileList[f].fileName == file.name)
                {
                    rmvFile = fileList[f].serverFileName;
                }
            }
            if (rmvFile){
                $.ajax({
                    url: "api/front/a-deleteitems.php",
                    type: "POST",
                    data: { "fileList" : rmvFile }
                });
            }
        });
    },
    url: "api/front/e-uploads.php"
  });

  function init() {
    // Revisa si es usuario propierario o visitante
    getUserLoged();



    /*
    * Maps
    */
    function iniciaMapa(){
      var addresspickerMap = $('#txt__Place').addresspicker({
          regionBias: "mx",
          mapOptions: {
            zoom: 16,
            center: new google.maps.LatLng('20.7217961, -103.38968779999999'),
            scrollwheel: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false
          },
          elements: {
              map:                          "#map_canvas",
              lat:                          "#lat",
              lng:                          "#lng",
              type:                         "#type",
              locality:                     '#EstateCity',
              sublocality:                  '#EstateColony',
              administrative_area_level_1:  '#EstateState',
              country:                      '#EstateCountry',
              postal_code:                  '#EstateZipcode',
              route:                        '#EstateAddress'
          }
      });
      addresspickerMap;

      var gmarker = addresspickerMap.addresspicker("marker");
      gmarker.setVisible(true);
      addresspickerMap.addresspicker("updatePosition");
      var map = addresspickerMap.addresspicker("map");

      $('#reverseGeocode').change(function(){
        $("#address").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
      });
    }
    
    // Invitados relacionados
    $invitados_current_list     = $('#invitados_current_list');
    function cargarListaInvitadosActuales(){
      var evento_id = $("#txt__id").val();
      $.post('api/front/colaboracion.php', {'method': 'byIdEvento', 'evento_id': evento_id })
        .done(function( _res ) {
          var _res = JSON.parse(_res);
          var options = '';
          $invitados_current_list.html('');
          var key_array = [];
          $.each(_res,function(index, value){
            key_array.push(value.user_id);
            options += '<option value=' + value.user_id + '>' + value.user_name + '</option>';
          });
          $invitados_current_list.append(options);

          // Invitados no relacionados sugeridos
          $invitados_list     = $('#invitados_list');
          function cargarListaInvitados(){

            var user_id = "<?=$_SESSION['user_id']?>";
            $.post('api/front/sugerencia.php', {'method': 'actorsByEnfoqueExcept', 'user_id': +user_id, 'key_array': key_array, 'friends_inyect': true, 'same_rol': true })
              .done(function( _res ) {
                var _res = JSON.parse(_res);
                var options = '';
                $invitados_list.html('');
                $.each(_res,function(index, value){
                  options += '<option value=' + value.user_id + '>' + value.user_name + '</option>';
                });
                $invitados_list.append(options);
                $invitados_list.selectpicker({
                  style: 'btn-primary',
                  size: 4,
                  liveSearch: true,
                  width: "100%",
                  maxOptions: 5,
                  actionsBox: false,
                  title: '<?=translate('label listainvitados')?>.',
                  tickIcon: 'fa fa-key'
                });

                // Inicia mapa al final
                iniciaMapa();
              })
              .error(function( _err ){
                console.log( _err );
              });
          }
          cargarListaInvitados();

        })
        .error(function( _err ){
          console.log( _err );
        });
    }
	  cargarListaInvitadosActuales();
  }

  $(function () {
  // Cuando el modal se ha terminado de dibujar
  $('#m-info-event').on("shown.bs.modal", init);
  $('#exportEvent').on('submit',function(e){
    e.preventDefault();
    
    // LLena formulario para exportación a ICS
    $("#summary").val($("#txt__Name").val());
    $("#datestart").val($("#txt__InitialDate").val() + " " + $("#txt__StartHour").val());
    $("#dateend").val($("#txt__EndDate").val() + " " + $("#txt__EndHour").val());
    $("#uri").val($("#txt__Url").val());
    $("#address").val($("#txt__Place").val());

    var description = 'Descripción: ' + $("#txt__Description").val() + '    ||    ' +
                      'Hora de inicio: ' + $("#txt__StartHour").val() + '    ||    ' + 
                      'Hora de término: ' + $("#txt__EndHour").val() + '    ||    ' + 
                      'Acceso: ' + $("#txt__Access").val() + '    ||    ' + 
                      'Tipo de evento: ' + $("#cmb__Type").val() + '    ||    ' + 
                      'Url del evento: ' + $("#txt__Url").val();

    $("#description_cal").val(description);
    $("#filename").val($("#txt__id").val() + "-" + $("#txt__Name").val().replace(" ", "") + ".ics");

    this.submit();
  });
  
 });
</script>