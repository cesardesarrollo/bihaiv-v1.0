<?php 
session_start();
require_once('../../../core/lang/lang.php');
?>
<div class="modal fade" id="m-sugest-profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="formVincularCon">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><?=translate('Sugerir a')?> <span id="userMsg" class="bolder"></span></h4>
      </div>
      <div class="modal-body">
          <input type="hidden" id="txt__user">
          <input type="hidden" id="txt__ValueUser">
          <div class="form-group">
            <label for="descripcion"><?=translate('Actores con los que debería vincularse')?>:</label>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group">
                  <select class="selectpicker form-control show-menu-arrow" id="actors_list" multiple>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
            <div class="alert" role="alert" id="panel-alert">
              <span id="icon"> </span> <span id="msg"> </span>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
        <button type="submit" class="btn btn-primary"><?=translate('Enviar solicitud')?> <i class="fa fa-user-plus"></i></button>
      </div>
        </form>
    </div>
  </div>
</div>
<script>
  $(function(){

    $actors_list = $('#actors_list');

    // Carga de actores
    function cargarListaActores(){
    $.post('api/front/userdata.php', {'method': 'getMyFriendsToVinculate', 'vincularA': $("#txt__ValueUser").attr('data-user-id') })
      .done(function( _res ) {
        var _res = JSON.parse(_res);
        var options = '';
        $actors_list.html('');
        $.each(_res,function(index, value){
          options += '<option value=' + value.user_id + '>' + value.name + '</option>';
        });
        $actors_list.append(options);
        $actors_list.selectpicker({
          style: 'btn-primary',
          size: 4,
          liveSearch: true,
          width: "100%",
          actionsBox: true,
          title: '<?=translate('Actores con los que debería vincularse')?>',
          tickIcon: 'fa fa-user'
        });
      })
      .error(function( _err ){
        console.log( _err );
      });
    }

    $('#m-sugest-profile').on('shown.bs.modal', function() {

      cargarListaActores();

      $('#userMsg').text($("#txt__ValueUser").attr('data-user-name'));

       $(document).off('submit').on('submit', '#formVincularCon', function(e){
          //console.log($actors_list.val());
          e.preventDefault();
          var vinculacion = new Vinculaciones();
          if ( !isEmptyString( $(this).attr('id') ) ){
              var data = {
                '_vincularA'      : $("#txt__ValueUser").attr('data-user-id'),
                '_vincularCon'    : $actors_list.val(),
                '_action'         : 'vincularcon',
                '_method'         : 'save'
              }
              vinculacion._set(data);
          }
        });
    });
  });
</script>