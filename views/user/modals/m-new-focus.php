<?php 
session_start();
require('../../../core/lang/lang.php');
?>
<div class="modal fade" id="m-new-focus" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="saveFocus">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?=translate('Nuevo Enfoque')?></h4>
        </div>
        <div class="modal-body">
            <div class="row row-margin-bottom">
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description"><?=translate('Tipo de enfoque')?>:</label>
                    <select id="select-tipo-enfoque"  class="form-control" name="">
                    </select>
                  </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="txt__Description"><?=translate('Descripción')?>:</label>
                    <textarea class="form-control" id="txt__Description" required data-validate-empty></textarea>
                  </div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert" role="alert" id="panel-alert">
                    <span id="icon"> </span> <span id="msg"> </span>
                  </div>
              </div>
              <div id="row-alert" class="col-sm-12 col-md-12 col-lg-12 margin-row-top">
                  <div class="alert alert-warning text-center" role="alert">
                      <i class="fa fa-warning"></i> <?=translate('¡Solo tienes 1 día para la edición de este registro!')?>
                      <br><br>
                      <button id="btnExplanation" type="button" onclick="showExplanation()" class="btn btn-default"><?=translate('Ver más')?></button>
                  </div>
              </div>
              <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
                  <div class="alert alert-success text-center" role="alert" id="panel-alert">
                      <?=translate('¡Solo tienes 1 día para la edición de este registro!')?>
                  </div>
              </div>
              <div id="row-msg-explained" style="display:none;" class="col-sm-12 col-md-12 col-lg-12 margin-row-top">
                  <div class="alert alert-success text-center" role="alert" id="panel-alert">
                      <p class="text-justify">
                          <?=translate('Texto descripción del porque solo un día para editar enfoque')?>
                      </p>
                  </div>
              </div>
              <input type="hidden" id="txt__user">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
            <button id="btnSubmit" type="submit" class="btn btn-primary"><?=translate('Guardar')?> <i class="fa fa-floppy-o"></i></button>
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  var $selectTipoEnfoque = $('#select-tipo-enfoque');
  var focus = new Enfoque();
  $(document).off('submit').on('submit', '#saveFocus', function(e){
      e.preventDefault();
      if ( !isEmptyString( $(this).attr('id') ) ){
          var data = {
            '_description'  : $("#txt__Description").val(),
            '_tipo_enfoque' : $selectTipoEnfoque.val(),
            '_type'         : 1,
            '_method'       : 'save',
            '_userid'       : parseInt( $("#txt__user").val() ),
            '_method'       : 'save'
          };
          focus._set(data);
      }
  });
  $(function(){
    // POPULATE SELECT
    $.post('api/front/enfoque.php', {method: 'get_tipo_enfoque'},function(response){
      if(response.status){
        // SUCCESS
        $.each(response.data,function(index, value){
          $selectTipoEnfoque.append(new Option(value.nombre, value.id));
        });
      }
      $selectTipoEnfoque.selectpicker({
        style: 'btn-default',
        size: 4,
        liveSearch : true
      });
    });
  });
  //ACCEPT TERMS OF EDITION
  function showExplanation(){
    $("#row-msg-explained").slideToggle('fast', function(){
      if( $("#row-msg-explained").is(':hidden') ){
        $("#btnExplanation").text("<?=translate('Ver más')?>");
      } else {
        $("#btnExplanation").text("<?=translate('Ver menos')?>");
      }

    });
  }
  function enableSave(){
  if( $("#txt__Description").val() != "" ){
    $("#btnSubmit").removeAttr('disabled');
    $("#row-alert").slideToggle();
  }
  else
    $("#txt__Description").focus();
}
</script>
