<?php 
session_start();
require_once('../../../core/lang/lang.php');
?>
 <div class="modal fade" id="m-report-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="formReport">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?=translate('Reportar a')?> <span id="userMsg" class="bolder"></span></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="txt__ValueUser">
        <div class="form-group">
          <label for="cmb__report"><?=translate('Tipo de reporte')?>:</label>
          <select class="form-control" id="cmb__report">
            <option value=""><?=translate('Seleccionar una opción')?></option>
            <option value="SPAM"><?=translate('Spam')?></option>
            <option value="ABUSIVE"><?=translate('Abusivo')?></option>
            <option value="OTHER"><?=translate('Otro')?></option>
          </select>
        </div>
        <div class="form-group">
          <label for="txt__Message"><?=translate('Comentarios')?>:</label>
          <textarea class="form-control" id="txt__Message" placeholder="<?=translate('Comentarios')?>." required></textarea>
        </div>
        <div id="row-msg" class="col-sm-12 col-md-12 col-lg-12 margin-row-top" style="display:none;">
            <div class="alert" role="alert" id="panel-alert">
              <span id="icon"> </span> <span id="msg"> </span>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?=translate('Cerrar')?> <i class="fa fa-times"></i></button>
        <button type="submit" class="btn btn-primary"><?=translate('Reportar')?> <i class="fa fa-ban"></i></button>
      </div>
        </form>
    </div>
  </div>
</div>
<script>
  $('#m-report-user').on('shown.bs.modal', function() {
    $('#userMsg').text($("#txt__ValueUser").attr('data-user-name'));
    var report = new Reportar();
    $(document).off('submit').on('submit', '#formReport', function(e){
        e.preventDefault();
        if( !isEmptyString( $(this).attr('id'))){
          var data = {
            _receptor : $("#txt__ValueUser").attr('data-user-id'),
            _msg      : $("#txt__Message").val(),
            _type     : $("#cmb__report").val(),
            _method   : 'save'
          };
          report._set(data);
        }
    })
  })
</script>