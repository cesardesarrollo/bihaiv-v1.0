
<html lang="<?php echo translate('Inicio'); ?>">

<section class="Profile__nav">
  <!-- Este nav bar se oculta en xs y sm -->
  <nav class="navbar navbar-default main-navbar hidden-xs hidden-sm" style="background-color:#f37064 !important">
    <div class="container">
      <div class="  main-navbar" id="bs-example-navbar-collapse-2">
        <ul class="nav navbar-nav" style="height:100%;">
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === '') ? 'active' : ''?>">
            <a href="<?=APP_PATH?>"><?php echo translate('Inicio'); ?></a>
          </li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'forum') ? 'active' : ''?>">
            <a href="?view=forum"><?php echo translate('Foro de discusión'); ?></a>
          </li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'calendar') ? 'active' : ''?>">
            <a href="?view=calendar"><?php echo translate('Eventos'); ?></a>
          </li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'map') ? 'active' : ''?>">
            <a href="?view=map"><?php echo translate('Mapa'); ?></a>
          </li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'catalogs') ? 'active' : ''?>">
            <a href="?view=catalogs"><?php echo translate('Roles'); ?></a>
          </li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'experts') ? 'active' : ''?>">
            <a href="?view=experts"><?php echo translate('Expertos'); ?></a>
          </li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'best_practices') ? 'active' : ''?>">
            <a href="?view=best_practices"><?php echo translate('Biblioteca'); ?></a>
          </li>
          <?php if(isset($_SESSION["user_id"])){ ?>
          <li role="separator" class="divider"></li>
          <li class="<?=(isset($_GET['view']) && $_GET['view'] === 'home') ? 'active' : ''?>">
            <a href="?view=home"><?php echo translate('Red Social'); ?></a>
          </li>
          <?php } ?>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <!-- =========================================================================== -->
  <div class="menu-movil visible-xs visible-sm">
    <div class="col-md-12">
      <button type="button" name="button" class="btn btn-menu btn-block trigger-menu" id="trigger-menu" >Menu</button>
    </div>
  </div>
</section>
<!-- menu movil -->
<div class="pink-menu-movil">
  <div class="header-menu">
    <h1 class="menu-pink-title">MENU</h1>
    <a href="#" id="trigger-menu-close">x</a>
  </div>
  <ul>
    <li><a href="<?=APP_PATH?>"><?php echo translate('Inicio'); ?></a></li>
    <li><a href="?view=forum"><?php echo translate('Foro de discusión'); ?></a></li>
    <li><a href="?view=calendar"><?php echo translate('Eventos'); ?></a></li>
    <li><a href="?view=map"><?php echo translate('Mapa'); ?></a</li>
    <li><a href="?view=catalogs"><?php echo translate('Roles'); ?></a></li>
    <li><a href="?view=experts"><?php echo translate('Expertos'); ?></a></li>
    <li><a href="?view=best_practices"><?php echo translate('Biblioteca'); ?></a></li>
    <?php if(isset($_SESSION["user_id"])): ?>
      <li><a href="?view=home"><?php echo translate('Red Social'); ?></a></li>
    <?php endif; ?>
  </ul>
</div>
<script>
  $(function(){
    var abrirMenu = $('#trigger-menu');
    abrirMenu.on('click', function(){
      $('.pink-menu-movil').show();
      $('.pink-menu-movil').addClass('animated slideInDown');
    });

    $("#trigger-menu-close").click(function() {
      $('.pink-menu-movil').addClass('animated slideOutUp');

      window.setTimeout( function(){
        $('.pink-menu-movil').hide();
        $('.pink-menu-movil').removeClass('animated slideInDown');
        $('.pink-menu-movil').removeClass('animated slideOutUp');
      }, 900);

    });
  });
</script>
