<footer>
  <div class="container">
    <div class="col-md-3">
      <a href="<?=APP_PATH?>">
        <img src="assets/img/home/logo_bihaiv-white.png" class="img-footer  center-block" />
      </a>
    </div>
    <div class="col-md-3">
      <ul>
        <li>
          <a href="<?=APP_PATH?>" class="footer-title">
              <?=translate('Inicio') ?>
          </a>
        </li>
        <li>
          <a href="<?=APP_PATH?>?view=forum" class="footer-title">
              <?=translate('Foro de discusión') ?>
          </a>
        </li>
        <li>
          <a href="<?=APP_PATH?>?view=calendar" class="footer-title">
              <?=translate('Eventos') ?>
          </a>
        </li>
        <li>
          <a href="<?=APP_PATH?>?view=map" class="footer-title">
              <?=translate('Mapa') ?>
          </a>
        </li>
      </ul>
    </div>
    <div class="col-md-3">
      <ul>
        <li>
          <a href="<?=APP_PATH?>?view=experts" class="footer-title">
              <?=translate('Expertos') ?>
          </a>
        </li>
        <li>
          <a href="<?=APP_PATH?>?view=catalogs" class="footer-title">
               <?=translate('Roles') ?>
          </a>
        </li>
        <li>
          <a href="<?=APP_PATH?>?view=best_practices" class="footer-title">
             <?=translate('Biblioteca del Ecosistema') ?>
          </a>
        </li>
      </ul>
    </div>
    <div class="col-md-3">
      <!-- Sin session -->
      <?php if(!Session::exists("user_id")):?>
        <span class="footer-title">Acceso Miembros</span>
        <br><br>
        <form action="?action=processlogin" role="search" method="post">
          <input autocomplete="off" type="hidden" name="token" value="0">
          <div class="form-group">
            <label for="txt__Username">Usuario:</label>
            <input autocomplete="off" type="email" id="txt__Username" name="email" class="form-control" placeholder="Email" value="" required="">
          </div>
          <div class="form-group">
            <label for="txt__Password">Contraseña:</label>
            <input autocomplete="off" type="password" id="txt__Password" name="password" class="form-control" placeholder="Contraseña" value="" required="">
          </div>
          <button id="oAuth" type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
          <div class="checkbox">
            <label>
              <input autocomplete="off" name="recuerdame" type="checkbox"> Recuérdame
            </label>
          </div>
          <div class="form-group margin-row-top">
            <a href="<?=APP_PATH?>home/recordar-contrasena.php">Olvidaste tu Contraseña?</a><br>
          </div>
        </form>
        <!-- con session iniciada -->
      <?php else:?>
      <?php endif;?>
    </div>
  </div>
  <section class="Links">
    <div class="container">
      <div class="pull-left">
        <span class="">Copyright © <?php echo date('Y') ?> <a href="http://mitefmexico.org" target="_NEW"> MIT Enterprise Forum México </a> </span>
      </div>
      <div class="pull-right">
        <ol class="breadcrumb no-margin no-padding-top no-padding-bottom">
          <li>
            <a href="https://www.facebook.com/MITenterpriseforumMexico">
              <img src="assets/img/home/FB.PNG" alt="" height="20">
            </a>
          </li>
          <li>
            <a href="https://twitter.com/MITEFMexico">
              <img src="assets/img/home/TWITTER.PNG" alt="" height="20">
            </a>
          </li>
          <li>
            <a href="https://www.youtube.com/channel/UCaIVuCcA0bup9rNk_mMzWEw">
              <img src="assets/img/home/YOUTUBE.PNG" alt="" height="20">
            </a>
          </li>
        </ol>
      </div>
    </div>
  </section>
</footer>
