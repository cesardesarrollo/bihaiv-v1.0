    <section id="Header" class="Header z-depth-1" style="min-height: 72px;">
      <div class="Header__nav">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=APP_PATH?>">
                  <figure class="figure">
                    <img src="assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="40">
                  </figure>
                </a>
              </div>
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="min-height: 74px;">
                <!-- SESSION MENU -->
                <?php if(Session::exists("user_id")):?>
                <div class="logged">
                  <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                      <input type="hidden" name="view" value="search">
                      <input type="text" class="form-control" name="q" placeholder="<?php echo translate('Buscar actores'); ?> ...">
                    </div>
                    <button type="submit" class="btn btn-primary">&nbsp;<i class="fa fa-search"></i>&nbsp;</button>
                  </form>
                  <ul class="nav navbar-nav navbar-right">
                  <?php
                   if(Session::exists("user_id")):
                      if(!Session::$user->is_admin && Session::$user->tipoRel==1):
                        $conversations = ConversationData::getConversations($_SESSION["user_id"]);
                        $nmsgs = 0;

                        foreach ($conversations as $conversation) {
                          $nmsg = MessageData::countUnReadsByUC($_SESSION["user_id"],$conversation->id);
                          $nmsgs += $nmsg->c;
                        }
                      $nnots = NotificationData::countUnReads($_SESSION["user_id"]);
                  ?>
                    <li class="dropdown messages-dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="label <?php if($nnots->c>0){ echo "label-danger"; }else{ echo "label-default"; }?>"><?php echo $nnots->c;?></span> <b class="caret"></b></a>
                      <ul class="dropdown-menu notifications-ul">
                        <?php if($nnots->c>0): ?>
                          <!-- NOTIFICACIONES -->
                          <li class="dropdown-header"><?php echo $nnots->c; ?> <?php echo translate('Notificaciones'); ?></li>
                          <?php

                            if (strlen($_SESSION["user_id"])>0) 
                            {
                              $notificaciones = NotificationData::getLast5ByUserIdInArray($_SESSION["user_id"]);
                              $array_notificaciones = array();
                              foreach ($notificaciones as $notificacion) {
                                
                                if (!NotificationData::getMessageById($notificacion['id'])) {
                                  $mensaje = strip_tags(utf8_decode($notificacion['message'])) . " <small>(Esta notificación ya no se encuentra vigente)</small>";
                                } else {
                                  $mensaje = utf8_decode($notificacion['message']);
                                }

                                if ($mensaje){
                                  $array_push = array("mensaje" => $mensaje, 'created_at' => $notificacion['created_at']);
                                  array_push($array_notificaciones, $array_push );
                                }
                              }
                            }

                            foreach($array_notificaciones as $noti): ?>

                              <li class="media notification">
                                <div class="media-body">
                                  <h5 class="notification-heading"><?php echo $noti['mensaje']; ?>
                                    <br>
                                    <i class="fa fa-clock-o"></i> <?php echo $noti['created_at']; ?>
                                  </h5>
                                </div>
                                <div class="ripple-container"></div>
                              </li>
                              <li class="divider"></li>
          
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <li><a href="./?view=notifications"><?php echo translate('Ver notificaciones'); ?></a></li>
                      </ul>
                    </li>
                    <li><a href="./?view=friendreqs"><i class="fa fa-users"></i> <?php $fq = FriendData::countUnReads(Session::$user->id); if($fq->c>0){ echo "<span class='label label-danger'>$fq->c</span>";} else{ echo "<span class='label label-default'>0</span>";} ?></a></li>
                    <li><a href="./?view=conversations"><i class="fa fa-envelope-o"></i> <?php if($nmsgs>0){ echo "<span class='label label-danger'>$nmsgs</span>";} else{ echo "<span class='label label-default'>0</span>";} ?></a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo Session::$user->name;?> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="./?view=home"><?php echo translate('Perfil'); ?></a></li>
                        <!--<li><a href="./?view=user&id=<?php echo Session::$user->id; ?>">Perfil Publico</a></li>-->
                        <li><a href="./?view=editinformation"><?php echo translate('Editar Información'); ?></a></li>
                        <li><a href="./?view=configuration"><?php echo translate('Configuración'); ?></a></li>
                        <li class="divider"></li>
                        <li><a href="./?action=processlogout"><?php echo translate('Salir'); ?></a></li>
                      </ul>
                    </li>
                    <?php endif; // if session exist ?>
                   <?php endif; ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-flag"></i> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="./?action=setlang&lang=es">Español</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="./?action=setlang&lang=en">Inglés</a></li>
                        </ul>
                      </li>
                  </ul>
                </div>
                <?php else: ?>
                  <div class="logged">
                    <ul class="nav navbar-nav navbar-right">
                      <li>
                        <a href="./?view=como_funciona" class="btn btn-header-menu"><?php echo translate('Cómo funciona'); ?></a>
                      </li>
                      <li>
                        <a href="home/login.php" class="btn btn-header-menu"><?php echo translate('Iniciar sesión'); ?></a>
                      </li>
                      <li>
                        <a href="home/registro.php" class="btn btn-header-menu"><?php echo translate('Únete'); ?></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-flag"></i> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="./?action=setlang&lang=es">Español</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="./?action=setlang&lang=en">Inglés</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                <?php endif ?>

              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
      </div>
    </section>
