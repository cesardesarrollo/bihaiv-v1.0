function Mensaje(){
	this.mensaje = {};
	this.base_url = 'api/front/mensaje.php';	
}

/*
 *	SEND MESSAGE
 *	==================
 */
Mensaje.prototype._send = function(){
	var data = {};
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.mensaje
	})
	.done(function( _res ){
		self.mensaje = {};
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-send-msg").modal('hide');
				var params= {};
      			params._method = "all";
      			params._userid = id_user;
				self._set(params);
			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-send-msg").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/*
 *	SET MESSAGE
 *	===========
 */
Mensaje.prototype._set = function( _data ){
	this.mensaje = {};
	this.mensaje.receptor_id = _data._receptor;
	this.mensaje.content 	 = _data._content;
	this.mensaje.method 	 = _data._method;
	if( this.mensaje.method === 'save'){
		this._send();
	}
}