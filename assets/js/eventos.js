/*
 *	CLASE EVENTOS
 *	===============
 */
function Evento(){
	this.evento = {};
	this.base_url = "api/front/events.php";
}

/*
 *	TODAS LAS EVENTOS POR USUARIO REGRESADOS EN FULLCALENDAR
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Evento.prototype._getallInCalendar = function(){
	var data = {};
	var self = this;

	self.evento.method = "all";
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.evento
	})
	.done( function( _res ){
      var Calendars = function() {
        var showDetail = function(id) {
            self._showEventById('m-info-event',id);
        };
        return {
            showDetail: showDetail
        };
      }();
      $('#' + self.evento.calendarId).fullCalendar({
          eventClick: function(calEvent, jsEvent, view) {
            Calendars.showDetail(calEvent.id);
          },
          editable: false,
          firstDay: 0,
          eventLimit: false, // allow "more" link when too many events
          events: _res

      });

	})
	.fail( function( _err ){
	})
};

/*
 *	TODAS LAS EVENTOS POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Evento.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.evento
	})
	.done( function( _res ){
		self._draw(_res);
	})
	.fail( function( _err ){
	})
};
/*
 *	EVENTO POR ID
 * 	@params : {id_aportacion}
 *	==========================
 */
Evento.prototype._byId = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.evento
	})
	.done(function( _res ){
		self._drawById( _res );
	})
	.fail(function( _err ){
	})
};
/*
 * 	NUEVA EVENTO
 *	@params : {informacion aportacion}
 *	=======================================
 */
Evento.prototype._save = function(){
	
	// Se bloquea botón de guardar
	$('#btnAddEvent').attr('disabled',true);

	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.evento,
		dataType: 'JSON'
	})
	.done(function( _res ){
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
				
		//	alert(self.evento.endDate);
			// NUEVO EVENTO
			var fechafinal=addDays(self.evento.endDate, 1);

	       newevent = {
	            id: 	_res.id,
	            title: 	self.evento.titulo,
	            start: 	self.evento.initialDate,
	            end:  	fechafinal
	        }
	   		$('#calendar').fullCalendar('renderEvent', newevent, true);

			setTimeout(function(){
				$("#m-new-event").modal('hide');
			}, 1000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			
			setTimeout(function(){
				$("#m-new-event").modal('hide');
			}, 1000);
		}
		self.evento = {};
	})
	.fail(function( _err ){
	})
};
/*
 *	EDITAR NUEVA EVENTO
 *	@params : {info_aportacion}
 *	==============================
 */
 addDays = function(d, days)
{
	var dat = new Date(d);
    dat.setDate(dat.getDate() + days);
    return dat;
}

Evento.prototype._edit = function(){

	// Se bloquea botón de guardar
	$('#btnAddEvent').attr('disabled',true);

	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.evento,
		dataType: 'JSON'
	})
	.done(function( _res ){

		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			// Se bloquea botón de guardar
			$('#btnAddEvent').attr('disabled',true);
			//Actualizar evento
			newevent = {
	            id: 	self.evento.id,
	            title: 	self.evento.titulo,
	            start: 	self.evento.initialDate,
	            end:  	addDays(self.evento.endDate, 1)
	        }
	        $('#calendar').fullCalendar('removeEvents', self.evento.id);
			$('#calendar').fullCalendar('renderEvent', newevent, true);
			
			setTimeout(function(){
				$("#m-info-event").modal('hide');
			}, 1000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			
			setTimeout(function(){
				$("#m-info-event").modal('hide');
			}, 1000);
		}

	})
	.fail(function( _err ){
	})
};
/*
 *	ELIMINAR EVENTO
 *	@param : id_aportacion
 *	=========================
 */
Evento.prototype._delete = function(){

	// Se bloquea botón de guardar
	$('#btnAddEvent').attr('disabled',true);

	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.evento
	})
	.done(function( _res ){

		if( +_res > 0 ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( "Eliminado" );
			
	   		$('#calendar').fullCalendar('removeEvents', self.evento.id);
			setTimeout(function(){
				$("#m-info-event").modal('hide');
			}, 1000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( "Error" );
			
			setTimeout(function(){
				$("#m-info-event").modal('hide');
			}, 1000);
		}
	})
	.fail(function( _err ){
	})
};
/*
 *	SET DATA LEAD
 *	==================
 */
Evento.prototype._set = function( _data ){
	this.evento = {};
	this.evento.titulo 		= _data._title || null;
	this.evento.type 		= _data._eventType || null;
	this.evento.initialDate = _data._initialDate || null;
	this.evento.endDate 	= _data._endDate || null;
	this.evento.initialHour = _data._initialHour || null;
	this.evento.endHour 	= _data._endHour || null;
	this.evento.place 		= _data._place || null;
	this.evento.lat 		= _data._lat || null;
	this.evento.lng 		= _data._lng || null;
	this.evento.photoEvent 	= _data._photoEvent || null;

	this.evento.descripcion = _data._description || null;
	this.evento.access 		= _data._access || null;
	this.evento.url 		= _data._url || null;
	this.evento.method 		= _data._method || null;
	this.evento.action 		= _data._action || null;
	this.evento.user_id		= _data._userid || null;
	this.evento.initiatives	= _data._initiatives || null;
	this.evento.id 			= _data._id || null;
	this.evento.calendarId 	= _data._calendarId || null;
	this.evento.objetivos 	= _data._objetivos || null;
	this.evento.colaboradores 	= _data._colaboradores || null;

	this.evento.pais 	= _data._pais || null;
	this.evento.estado 	= _data._estado || null;
	this.evento.ciudad 	= _data._ciudad || null;

	if( this.evento.method === 'allInCalendar')
	{
		this._getallInCalendar();
	}
	if( this.evento.method === 'all')
	{
		this._getall();
	}
	else if( this.evento.method === 'byId')
	{
		this._byId();
	}
	else if( this.evento.method === 'save' )
	{
		this._save();
	}
	else if( this.evento.method === 'edit')
	{
		this._edit();
	}
	else if( this.evento.method === 'delete')
	{
		this._delete()
	}

};


function cargaIniciativas(id){

	// Combo Iniciativas
	$cmbInitiatives = $('#cmb__Initiatives');
	$.ajax({
		url: "api/front/iniciativa.php",
		method: 'POST',
		dataType: 'JSON',
		data: {
			method:  'byUserIdAndInCollaboration'
		}
	})
	.done(function(data) {
		var opciones = "";
		$.each(data, function (i, options) {
			opciones = opciones + '<option ' + ((id == options.idIniciativa)? 'selected' : '') + ' value="' + options.idIniciativa + '" >' + options.titulo + '</option>';
		});
		$cmbInitiatives.html(opciones);
	})
	.fail(function(_err) {
		console.log(_err);
	});

}

/*
 * POR ID
 *	==================
 */
Evento.prototype._drawById = function( _data ){
	//FILL DATA

	cargaIniciativas(_data.iniciativa_id);
	
	$("#row-msg").html("");
	$("#eventPhoto").val("");
	$("#txt__Name").val(_data.titulo);
	$("#cmb__Type").val(_data.tipoEvento);
	$("#txt__InitialDate").val(_data.fechaEvento);
	$("#txt__EndDate").val(_data.fechaFin);
	$("#txt__Access").val(_data.acceso);
	$("#txt__Url").val(_data.url);
	$("#txt__Place").val(_data.lugar);
	$("#lat").val(_data.lat);
	$("#lng").val(_data.lng);
	$("#txt__Description").val(_data.textoLargo);
	$("#txt__id").val(_data.id);
	$("#cmb__Initiatives").val(_data.iniciativa_id);
	$("#txt__StartHour").val(_data.horaInicio);
	$("#txt__EndHour").val(_data.horaFin);
	//$("#txt__user").val(_data.user_id);
	$("#txt__user").val(_data.Usuario_id);
	if(_data.llaveImg !== null){
		$("#img-event").attr('src', 'uploads/eventos/' + _data.llaveImg);
		$("#img-event").attr('data-img', _data.llaveImg);
	} else {
		$("#img-event").css('display', 'none');
	}
};


/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Evento.prototype._draw = function( _data ){
	var self = this;
	var action = self.evento.action;
	if( action === 'currentEvents' ){
		if ( _data.length > 0){
			$("#month-events").html( "" );
			var template = Handlebars.compile( $("#month-events-tmpl").html() );
			var html = template( _data );
			$("#month-events").html( html );
		} else {
			$("#month-events").html( '<h4>No hay eventos disponibles en esta categoría!</h4>' );
		}
	} else if( action === 'coomingEvents'){
		if ( _data.length > 0){
			$("#cooming-events").html( "" );
			var template = Handlebars.compile( $("#month-events-tmpl").html() );
			var html = template( _data );
			$("#cooming-events").html( html );
		} else {
			$("#cooming-events").html( '<h4>No hay eventos disponibles en esta categoría!</h4>');
		}
	} else if( action === 'lastEvents'){
		if ( _data.length > 0){
			$("#last-events").html( "" );
			var template = Handlebars.compile( $("#month-events-tmpl").html() );
			var html = template( _data );
			$("#last-events").html( html );
		} else {
			$("#last-events").html( '<h4>No hay eventos disponibles en esta categoría!</h4>');
		}
	}
};
Evento.prototype._drawFilter = function( _data ){
	var action = _data.type;
	_data = _data.data;
	if( action == 'currents' ){
		if ( _data.length > 0){
			$("#month-events").html( "" );
			var template = Handlebars.compile( $("#month-events-tmpl").html() );
			var html = template( _data );
			$("#month-events").html( html );
		} else {
			setFlash('No hay eventos disponibles en esta categoría.', 'info');
			$("#month-events").html( '<h4>No hay eventos disponibles en esta categoría!</h4>' );
		}
	} else if( action === 'coomings'){
		if ( _data.length > 0){
			$("#cooming-events").html( "" );
			var template = Handlebars.compile( $("#month-events-tmpl").html() );
			var html = template( _data );
			$("#cooming-events").html( html );
		} else {
			setFlash('No hay eventos disponibles en esta categoría.', 'info');
			$("#cooming-events").html( '<h4>No hay eventos disponibles en esta categoría!</h4>');
		}
	} else if( action === 'latest'){
		if ( _data.length > 0){
			$("#last-events").html( "" );
			var template = Handlebars.compile( $("#month-events-tmpl").html() );
			var html = template( _data );
			$("#last-events").html( html );
		} else {
			setFlash('No hay eventos disponibles en esta categoría.', 'info');
			$("#last-events").html( '<h4>No hay eventos disponibles en esta categoría!</h4>');
		}
	}
};
Evento.prototype._drawCarousel = function( _data ){
	var template = "";
	for (var i = 0; i <= _data.length - 1; i++) {
		if ( i <= 3 ){
			template += '<div class="item">' +
	        '<img class="cover-img" style="background-image: url(uploads/eventos/' + _data[i]._avatar + ');">' +
	        // '<img src="uploads/eventos/' + _data[i]._avatar + '" alt="'+_data[i]._titulo+'">' +
	        '<h1 class="Events--title" style="    text-shadow: 1px 1px 5px rgba(0,0,0,0.6);">' + _data[i]._titulo +
	        '<span><i class="fa fa-clock-o"></i> ' + _data[i]._fecha + '</span>' +
	        '<span><i class="fa fa-map-marker"></i> ' + _data[i]._lugar + '</span></h1></div>';
		}
	}
	$("#owl-demo").append( template );

	$('#owl-demo').owlCarousel({
		loop:false,
		margin:0,
		nav:true,
		dots: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});
};


/*
 *	Dibujar detalle de evento
 *	=====================
 */
Evento.prototype._drawDetailById = function( _data ){

	var template = Handlebars.compile( $("#detail-event-tmpl").html() );
	var html = template( _data );
	$("#detail-event").html( html );

	function initMapV2() {
        var myLatLng = {
          lat: parseFloat(_data.lat),
          lng: parseFloat(_data.lng)
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 16,
          center: myLatLng
        });
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
        });
    }
    initMapV2();
};


/*
 *	GET LAST EVENTS
 *	=====================
 */
Evento.prototype._lastEvents = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.evento
	})
	.done( function( _res ){
	})
	.fail( function( _err ){
	})
};
/*
 *	GET NEXT EVENTS
 *	=====================
 */
Evento.prototype._coomingEvents = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.evento
	})
	.done( function( _res ){
		self._draw(_res);
	})
	.fail( function( _err ){
	})
};
/*
 *	EVENTS FAV BUTTON
 *	====================
 */
Evento.prototype._addfav = function( _data ){
	var self = this;
	var id_usuario = _data._usuario;
	//alert( _data._post )
	if( $("#fav-" + _data._index + ' i').hasClass('active') ){
		//REMOVE LIKES
      	$("#fav-" + _data._index + ' i').removeClass('active');
    } else {
    	//ADD LIKES
    	var params = {
    		post : _data._post,
    		method : 'addFav',
    		type : 2 //1 Publicaciones, 2 Eventos
    	};
    	$.ajax({
    		url 	: self.base_url,
			method 	: 'POST',
			async 	: false,
			data 	: params,
			dataType: 'JSON'
    	}).done(function( _res ){
    		if( _res.status ){
      			$("#fav-" + _data._index + ' i').addClass('active');
      			self.evento = {};
      			var action = '';
      			if( _data._tipo == 1) {action = 'currentEvents'};
      			if( _data._tipo == 2) {action = 'coomingEvents'};
      			if( _data._tipo == 3) {action = 'lastEvents'};
      			var params = {};
				params._method = "all";
				params._action = action;
				params._userid = id_usuario;
      			self._set(params);
    		}
    	}).error(function( _err ){
    	})

    }
};
/*
 *	SHOW MODAL BY ID
 *	==================
 */
Evento.prototype._showEventById = function(modal,id){
	$.ajax({
		url: 'views/user/modals/' + modal + '.php?token=' + Math.random(),
		method: 'GET'
	})
	.done(function(_res) {
		if($('#modal-launcher #m-info-event').length == 0){
			$('#modal-launcher').html(_res);
		}
		
		// Combo Iniciativas
		$cmbInitiatives = $('#cmb__Initiatives');
		$.ajax({
			url: "api/front/iniciativa.php",
			method: 'POST',
			dataType: 'JSON',
			data: {
			method:  'byUserIdAndInCollaboration'
			}
		})
		.done(function(data) {
    		var opciones = "<option value=''><?=translate('Selecciona una..')?></option>";
			$.each(data, function (i, options) {
				opciones = opciones + '<option value="' + options.idIniciativa + '" >' + options.titulo + '</option>';
			});
			$cmbInitiatives.html(opciones);
			if(!$('#m-info-event').is(':visible')){
				$("#m-info-event").modal({
					show : true,
					backdrop : 'static',
					keyboard : false
				});
			}

			// Setea evento
			$("#txt__id").val( id );
			var evento = new Evento();
			var params = {};
			params._method = "byId";
			params._id = id;
			evento._set(params);

		})
		.fail(function(_err) {
		});

	})
	.fail(function(_err) {
		console.log(_err);
	});
}

/*
 *	Obtener todos los evento
 *	=====================
 */
Evento.prototype._getAllEvents = function(){
	var self = this;
	$.ajax({
		url 	: 'api/front/events.php',
		method 	: 'POST',
		dataType: 'JSON',
		data 	: {'method' : 'all', 'action' : 'getAllEvents'}
	})
	.done( function( _res ){
		self._drawCarousel(_res);
	})
	.fail( function( _err ){
	})

};


/*
 *	Obtener detalle de evento
 *	=====================
 */
Evento.prototype._getById = function(id){
	var self = this;
	$.ajax({
		url 	: 'api/front/events.php',
		method 	: 'POST',
		dataType: 'JSON',
		data 	: {'method' : 'byId', 'id' : id}
	})
	.done( function( _res ){
		self._drawDetailById( _res );
	})
	.fail( function( _err ){
	})
};


function getCurrentEvents()
{
	$("#txt__CurrentFilterDate").attr('data-filter-date', 'currentEvents');
	$("#cmb_CurrentEvents").attr('data-filter-type', 'currentEvents');
	$("#txt__CurrentEventName").attr('data-filter-name', 'currentEvents');
	var evento = new Evento();
	evento.evento = {};
	var params = {};
	params._method = "all";
	params._action = 'currentEvents';
	evento._set(params);
}

function getLastEvents()
{
	$("#txt__LatestEventDate").attr('data-filter-date', 'lastEvents');
	$("#cmb_LatestEvents").attr('data-filter-type', 'lastEvents');
	$("#txt__CoomingEventName").attr('data-filter-name', 'lastEvents');
	var evento = new Evento();
	evento.evento = {};
	var params = {};
	params._method = "all";
	params._action = "lastEvents";
	evento._set(params);
}

function getCoomingEvents()
{
	$("#txt__CoomingEventsDate").attr('data-filter-date', 'coomingEvents');
	$("#cmb_CoomingEvents").attr('data-filter-type', 'coomingEvents');
	$("#txt__LatestEventName").attr('data-filter-name', 'coomingEvents');
	var evento = new Evento();
	evento.evento = {};
	var params = {};
	params._method = "all";
	params._action = "coomingEvents";
	evento._set(params);
}

function addFav(index, post, tipo, id_user)
{
	var evento = new Evento();
	var params = {};
	params._post = post;
	params._index = index;
	params._tipo = tipo;
	params._usuario = id_user;
	evento._addfav( params );
}

/*
 *	FILTER BY EVENT TYPE
 *	========================
 */
function eventFilter( type ){
	var event_type, event_date, event_name, data = {};
	if ( type === 'currents')
	{
		data.event_type = $("#cmb_CurrentEvents").val();
		data.event_date = $("#txt__CurrentFilterDate").val();
		data.event_name = $("#txt__CurrentEventName").val();
	}
	else if( type === 'latest')
	{
		data.event_type = $("#cmb_LatestEvents").val();
		data.event_date = $("#txt__LatestEventDate").val();
		data.event_name = $("#txt__LatestEventName").val();
	}
	else if ( type === 'coomings')
	{
		data.event_type = $("#cmb_CoomingEvents").val();
		data.event_date = $("#txt__CoomingEventsDate").val();
		data.event_name = $("#txt__CoomingEventName").val();
	}
	data.method = 'filterEvent';
	data.type 	= type;
	//REQUEST
	$.ajax({
		url  : 'api/front/events.php',
		type : 'POST',
		async: false,
		data : data
	}).done(function(res){

		var evento = new Evento();
		evento._drawFilter(res);
	}).fail(function(err){
		setFlash('Ocurrio un error inesperado.', 'danger');
	})
};

function eventAssist(event_id, el){
	var button = el;
	$.ajax({
		url 	: 'api/front/asistenteevento.php',
		async 	: false,
		type 	: 'POST',
		data 	: {'method' : 'save', 'Evento_idEvento' : event_id}
	}).done(function(res){
		var res = JSON.parse(res);
		if(res.status){
			$(button).removeClass('btn-primary');
			$(button).addClass('btn-success');
			$(button).text( res.msg );
		} else {
			$(button).removeClass('btn-primary');
			$(button).addClass('btn-warning');
			$(button).text( res.msg );
		}
	}).error(function(err){
	})
}


//HELPER TO VALIDATE EVENT PARTICIPATION
Handlebars.registerHelper('ifassist', function(options){
	if(options.tipoEvento !== "lastEvents"){
		if( options.user_asistenteevento === "ASISTE"){
			return "<button class='btn btn-success-outline' onclick='eventAssist(" + options._idevento + ", this)'>"+
				'Asistiré <i class="fa fa-check"></i></button>';
		} else if ( options.user_asistenteevento === null){
			return "<button class='btn btn-primary-outline' onclick='eventAssist(" + options._idevento + ", this)'>" +
				'Asistir <i class="fa fa-plus"></i></button>';
		}
	}

})
