/*
 *	CLASE INICIATIVA
 *	===============
 */
function Iniciativas(){
	this.iniciativa = {};
	this.base_url = "api/front/iniciativa.php";
}

/*
 *	TODAS LAS INICIATIVA POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Iniciativas.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.iniciativa
	})
	.done( function( _res ){
		var limit_start = _res.length + 1;
		var limit_end   = limit_start + 5;
		$("#initiatives-more").attr('limit-start', 0)
		$("#initiatives-more").attr('limit-end', limit_end)
		self._draw(_res);
	})
	.fail( function( _err ){
		//console.log( _err );
		$("#leads").html(_err.responseText);
	})
};

/*
 *	INICIATIVA POR ID
 * 	@params : {id_iniciativa}
 *	==========================
 */
Iniciativas.prototype._byId = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.iniciativa
	})
	.done(function( _res ){
		self._drawById( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/*
 * 	NUEVA INICIATIVA
 *	@params : {informacion enfoque}
 *	=======================================
 */
Iniciativas.prototype._save = function(){

	// Se bloquea botón de guardar
	$('#btnSave').attr('disabled',true);

	var self = this;
	var id_user = self.iniciativa.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.iniciativa,
		dataType: 'JSON'
	})
	.done(function( _res ){
		self.iniciativa = {};
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );

			setTimeout(function(){
				$("#m-new-initiative").modal('hide');
				var params= {};
      			params._method = "all";
      			params._userid = id_user;
				self._set(params);
			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			
			setTimeout(function(){
				$("#m-new-event").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/* HERE COMES NEW CHALLENGER!!! */
Iniciativas.prototype._save_new = function(){

	// Se bloquea botón de guardar
	$('#btnSave').attr('disabled',true);

	var self = this;
	var id_user = self.iniciativa.user_id;
	$.ajax({
		url 		: self.base_url,
		method 		: 'POST',
		async 		: false,
		data 		: self.iniciativa,
		dataType 	: 'JSON'
	})
	.done(function( _res ){
		self.iniciativa = {};
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			
			setTimeout(function(){
				$("#m-new-initiative").modal('hide');
				var params= {};
      			params._method = "all";
      			params._userid = id_user;
				self._set(params);
			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			
			setTimeout(function(){
				$("#m-new-event").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/*
 *	ELIMINAR INICIATIVA
 *	@param : id_iniciativa
 *	=========================
 */
Iniciativas.prototype._delete = function(){
	var self = this;
	var id_user = self.iniciativa.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.iniciativa
	})
	.done(function( _res ){
		var params= {};
		params._method = "all";
		params._userid = id_user;
		self._set(params);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};

/*
 *	EDITAR INICIATIVA
 *	@param : {data}
 *	=========================
 */
Iniciativas.prototype._update = function(){

	// Se bloquea botón de guardar
	$('#btnSave').attr('disabled',true);

	var self = this;
	var id_user = self.iniciativa.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.iniciativa
	})
	.done(function( _res ){
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			
			setTimeout(function(){
				$("#m-info-initiative").modal('hide');
				var params = {};
				params._method = "all";
				params._userid = id_user;
				self._set(params);
			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
						
			setTimeout(function(){
				$("#m-info-initiative").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	});
};

/*
 *	SET DATA INICIATIVA
 *	==================
 */
Iniciativas.prototype._set = function( _data ){

	this.iniciativa.title 		= _data._title || null;
	this.iniciativa.initialDate = _data._initialDate || null;
	this.iniciativa.endDate 	= _data._endDate || null;
	this.iniciativa.endDate 	= _data._endDate || null;
	this.iniciativa.place 		= _data._place || null;
	this.iniciativa.description = _data._description || null;
	this.iniciativa.llaveImg 	= _data._photoCover || null;
	this.iniciativa.initialHour = _data._initialHour || null;
	this.iniciativa.endHour 	= _data._endHour || null;
	this.iniciativa.actors 		= _data._actors || null;
	this.iniciativa.events 		= _data._events || null;
	this.iniciativa.method 		= _data._method || null;
	this.iniciativa.lead_id 	= _data._id || null;
	this.iniciativa.user_id		= _data._userid || null;
	this.iniciativa.post_id		= _data._postId || null;
	this.iniciativa.objetivos 	= _data._objetivos || null;
	this.iniciativa.idAlcanze 	= _data._idAlcanze || null;
	this.iniciativa.limit_param1 = _data._limit_param1 || null;
	this.iniciativa.limit_param2 = _data._limit_param2 || null;
	this.iniciativa.tipo		= 2;
	this.iniciativa.ubicaciones	= _data._ubications || null;

	if( this.iniciativa.method === 'all')
	{
		this._getall();
	}
	else if( this.iniciativa.method === 'byId')
	{
		this._byId();
	}
	else if( this.iniciativa.method === 'save' )
	{
		this._save();
	}
	else if( this.iniciativa.method === 'save_new' )
	{
		this._save_new();
	}
	else if( this.iniciativa.method === 'edit')
	{
		this._update();
	}
	else if( this.iniciativa.method === 'delete')
	{
		this._delete();
	}
	if( this.iniciativa.method === 'allByLimits')
	{
		this._getall();
	}

};
/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Iniciativas.prototype._draw = function( _data ){
	if ( _data.length > 0 ){
		$("#initiatives-more").css('display','block');
		var template = Handlebars.compile( $("#lead-template").html() );
		var html = template( _data );
		$("#leads").html( html );
	
		console.log(_data);
		$rateYoObjetivosIniciativa = $(".rateYo-obejtivos-iniciativa").rateYo({
			rating    : 0,
			starWidth : "16px",
			ratedFill : '#fae617',
			precision : 0,
			spacing   : "2px",
			onSet: function (rating, rateYoInstance) {
				if( parseInt(rating) > 0 ){
					var data = {
						'objetivo_id'       : $(this).data('idobjetivoiniciativa'),
						'calificacion'  	: rating,
						'organizacion_id' 	: null,
						'method'         	: 'update_rating',
						'iniciativa_id'     : $(this).data('iniciativaid')
					};
					// Ajax request to save qualification
					$.post('api/front/iniciativa.php', data, function(response){
					
						setFlash(response.msg, response.class);
					});
				}else{
					setFlash('Para calificar es necesario ingresar al menos una estrella', 'warning');
				}
			}
		});

	} else {
		$("#leads").html(_data);
		$("#initiatives-more").css('display','none');
	}
};
/*
 *	INICIATIVA POR ID
 *	==================
 */
Iniciativas.prototype._drawById = function( _data ){

	//FILL DATA
	$("#txt__iniciativa").val(_data[0].idIniciativa);
	$("#txt__post").val(_data[0].post_id);
	$("#txt__user").val(_data[0].user_id);
	$("#title").html( _data[0].titulo );
	$("#txt__Name").val( _data[0].titulo );
	$("#txt__InitialDate").val( _data[0].fechaIniciativa );
	$("#txt__EndDate").val( _data[0].fechaFin );
	$("#txt__Description").val( _data[0].textoCorto );
	$("#txt__Place").val( _data[0].lugar );
	$("#txt__StartHour").val( _data[0].horaIni );
	$("#txt__EndHour").val(_data[0].horaFin);
	$("#cmb__Alcanze").val(_data[0].idAlcanze);
	if(_data[0].llaveImg !== null){
		$("#img-iniciativa").attr('src', 'uploads/iniciativas/' + _data[0].llaveImg);
		$("#img-iniciativa").attr('data-img', _data[0].llaveImg);
	} else {
		$("#img-iniciativa").css('display', 'none');
	}
};
/*
 *	SHOW MODAL BY ID
 *	==================
 */
function showById(event, iniciativaId, post, user, action){
	event.preventDefault();
	//SHOW MODAL
  	$("#modal-launcher").load('views/user/modals/m-info-initiative.php?token=' + Math.random(), function(){
    	$("#m-info-initiative").modal({
     	 	show : true,
     	 	backdrop : 'static',
      		keyboard : false
    	});
    	if( action !== 'edit'){
    		$(".btn-edit").css('display', 'none');
    		$("#editLead :input, textarea").each(function(){
    			if( $(this).attr('type') !== 'button')
    				$(this).prop('disabled', true);
    		});
    		$("#btn_agregar_ubicacion").prop('disabled', true);
    		$("#initiativePhoto").css('display', 'none');
    		$("#action").val('view');
    	}
    	$("#txt__id").val( post );
    	var iniciativa = new Iniciativas();
	    var params = {};
	    params._method = "byId";
	    params._id = iniciativaId;
	    params._userid = user;
	    params._postId = post;
    	iniciativa._set(params);
  	});
}

/*
*	DELETE
*	==================
*/
function deleteInitiative(event, iniciativaId, postId, userId){
	event.preventDefault();

	/* =========================================================================
	*	sweet alert, docuemntacion -> http://lipis.github.io/bootstrap-sweetalert/
	* =========================================================================*/
	swal({
		title: "Eliminar",
		text: "¿Estas seguro de eliminar esta iniciativa?",
		type: "warning",
		showCancelButton: true,
		closeOnConfirm: false,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Eliminar",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false,
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			/* Eliminada */
			var iniciativa = new Iniciativas();
			var params = {};
			params._method = "delete";
			params._id = iniciativaId;
			params._postId = postId;
			params._userid = userId;
			iniciativa._set(params);
			swal("Eliminada", "Iniciativa eliminada con éxito.", "success")
		} else {
			/* Cancelada */
			swal("Cancelado", "Tu iniciativa no fue eliminada.", "error");
		}
	});
	/* =========================================================================*/
}


/*
*	MORE
*	==================
*/
$("#initiatives-more").on('click', function(e){
	var iniciativa = new Iniciativas();
	e.preventDefault();
	var limit_start = $("#initiatives-more").attr('limit-start');
	var limit_end = $("#initiatives-more").attr('limit-end');
	var data = {
		_limit_param1 : parseInt(limit_start),
		_limit_param2 : parseInt(limit_end),
		_userid 	  : $("#initiatives-more").attr('data-user'),
		_method 	  : 'allByLimits'
	};
	iniciativa._set( data );
})
