'use strict';

function openModal(modal, user){
	$("#modal-launcher").load('views/user/modals/' + modal + '.php?token=' + Math.random(), function(){
		$("#" + modal).modal({
			show : true,
			backdrop : 'static',
			keyboard : false
		});
		$("#txt__user").val( user );
	})
}

function openModalActions(modal, user, username){
  //var e = event;
  //e.preventDefault();
  $("#modal-launcher").load('views/user/modals/' + modal + '.php?token=' + Math.random(), function(){
    $("#" + modal).modal({
      show : true,
      backdrop : 'static',
      keyboard : false
    });
    $("#txt__ValueUser").attr('data-user-id', user);
    $("#txt__ValueUser").attr('data-user-name', username);
    $("#txt__user").val( user );
  })
}


function getDocument(url, view){
	//alert(document);
	if( view === 'view_1' ){
		// Load file
		$("#i-pdf-1").attr('src', url);
		// show modal
		$('#modal-biblioteca-1').modal('show');
	} else if( view === 'view_2'){
		$("#i-pdf-2").attr('src', url)
	} else if( view === 'view_3'){
		$("#i-pdf-3").attr('src', url)
	}
};

 function showInfoRol( rol ){
    $("#modal-launcher").load('views/user/modals/m-info-rol.php?token=' + Math.random(), function(){
      $("#m-info-rol").modal({
        show : true,
        backdrop : 'static',
        keyboard : false
      })
      $.ajax({
        url : 'api/front/roles.php',
        type : 'POST',
        async : false,
        data : { 'rol' : rol }
      })
      .done(function( res ){
      var value = JSON.parse(res);
        $("#photo_rol").attr('src', 'admin/files/' + value.imagen);
        $("#name").text( value.nombre );
        $("#description").text( value.descripcion );
      })
      .error(function( err ){
        console.log( err );
      })

    })
  }

  function expertModalEvents(){
    var showChar = 500;
    var ellipsestext = "...";
    var moretext = "Mostrar más >";
    var lesstext = "Mostrar menos";
    

    $('p.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
  }

 function showInfoExpert( id ){
    $("#modal-launcher").load('views/user/modals/m-info-experts.php?token=' + Math.random(), function(){
      $("#m-info-rol").modal({
        show : true,
        backdrop : 'static',
        keyboard : false
      })

      $.ajax({
      	url : 'api/admin/expertos.php',
      	type : 'POST',
      	async : false,
      	data : { 'id' : id, 'method' : 'byUserId'}
      })
      .done(function( res ){
  		  var value = JSON.parse(res);
        $("#ramo").text( value[0]._tipoOrganizacion );
        $("#img-expert").css({
          'height': '180px',
          'width': '170px',
          'background-color': 'white',
          'background': 'url(admin/files/' + value[0]._image + ')',
          'background-position': 'center center',
          'background-size': 'cover',
          'position': 'relative',
          'left': '20px',
          'top': '-75px',
          'margin-bottom': '-65px'
        });
        $("#experiencia").text( value[0]._anios );
        $("#cargo_actual").text( value[0]._cargoActual );
        $("#cargo_anterior").text( value[0]._cargoAnterior );
      	$("#bio").text( value[0]._bio );

        expertModalEvents();
        
        if(value[0]._facebook != null){
          $("#m-info-rol .fa-facebook").parent().prop('href', value[0]._facebook ).parent().removeClass('hidden');
        }else{
          $("#m-info-rol .fa-facebook").parent().prop('href', '#' ).parent().addClass('hidden');
        }

        if(value[0]._twitter != null){
          $("#m-info-rol .fa-twitter").parent().prop('href', value[0]._twitter ).parent().removeClass('hidden');
        }else{
          $("#m-info-rol .fa-twitter").parent().prop('href', '#' ).parent().addClass('hidden');
        }

        if(value[0]._linkedin != null){
          $("#m-info-rol .fa-linkedin").parent().prop('href', value[0]._linkedin ).parent().removeClass('hidden');
        }else{
          $("#m-info-rol .fa-linkedin").parent().prop('href', '#' ).parent().addClass('hidden');
        }
      })
      .error(function( err ){
      	console.log( err );
      })

    })
  }
//VALIDATE EMPTY STRING
function isEmptyString(form){
    //GET ALL INPUTS
    var empty;
    $("form#" + form + " input, form#" + form + " select").each(function(){
        var input = $(this);
        var attr = $(input).attr('data-validate-empty');
        if( typeof attr !== typeof undefined && attr !== false){
          var val = $(input).val().trim();
          if( val === ""){
             // debugger;
              var inputId = $(input).attr('id');
              $(input).val('');
              $("#" + inputId).focus();
              $("#row-msg").slideToggle('fast');
              $("#panel-alert").addClass('alert-warning');
              $("#icon").html('<i class="fa fa-warning"></i>');
              $("#msg").html('No puedes dejar campos vacios!');
              setTimeout(function(){
                $("#row-msg").slideToggle('fast');
              }, 3000);
              empty = true;
          } else {
              empty = false;
          }
        } else {
          empty = false;
        }
    });
    return empty;
};

/*
 *  VALIDATE DATE RANGE
 *  ======================
 */
function isValidDate(){

  var start_date = $("#txt__InitialDate").val(),
      end_date   = $("#txt__EndDate").val();
  if ( !isValidRangeDate( start_date, end_date ) ){
    $("#txt__EndDate").val('');
    $("#txt__EndDate").focus();
    $("#row-msg").slideToggle('fast');
    $("#panel-alert").addClass('alert-warning');
    $("#icon").html('<i class="fa fa-warning"></i>');
    $("#msg").html('La fecha final no puede ser menor que la fecha de inicio!');
    return false;
  } else {
    return true;
  }
}
function isValidRangeDate(startDate, endDate){
    return Date.parse( startDate) < Date.parse( endDate );
}


/*
 *  GENERA CADENA RANDOM
 *  ======================
 */
function generaLlave()
{
    var llave = "";
    var string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 16; i++ )
        llave += string.charAt(Math.floor(Math.random() * string.length));
    return llave;
}
