/*
 *	CLASE TEMA
 *	===============
 */
function Discusiones(){
	this.discusion 	= {};
	this.search		= {};
	this.comment 	= {};
	this.rating 	= {};
	this.base_url 	= "api/front/discusion.php";
}


/*
 *	TODAS LOS TEMAS POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Discusiones.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done( function( _res ){
		//console.log( _res )
		self._draw(_res);
	})
	.fail( function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._getall_search = function(){
	var data = {};
	var self = this;

	//AJAX REQUEST
	$.ajax({
		url 		: self.base_url,
		method 		: 'POST',
		dataType	: 'JSON',
		data 		: self.search
	})
	.done( function( _res ){
		//console.log( _res )
		self._draw_search(_res);
	})
	.fail( function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._orderComents = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done( function( _res ){
		self._draw_only_comments_by_topic(_res);
	})
	.fail( function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._getall_most_visited = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done( function( _res ){
		//console.log( _res )
		self._draw_most_visited(_res);
	})
	.fail( function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._getall_most_voted = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done( function( _res ){
		//console.log( _res )
		self._draw_most_voted(_res);
	})
	.fail( function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._getall_my_topics = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done( function( _res ){
		//console.log( _res )
		self._draw_my_topics(_res);
	})
	.fail( function( _err ){
		$mostVotedContainer = $('#settings');
		$mostVotedContainer.html(_err.responseText);
		console.log( _err );
	})
};
/*
 *	TEMA POR ID
 * 	@params : {id_aportacion}
 *	==========================
 */
Discusiones.prototype._byId = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done(function( _res ){
		// console.log(_res);
		self._drawById( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._addVisit = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done(function( _res ){
		console.log(_res);
	})
	.fail(function( _err ){
	})
};
/*
 * 	NUEVA TEMA
 *	@params : {informacion enfoque}
 *	=======================================
 */
Discusiones.prototype._save = function(){
	var self = this;
	var id_user = self.discusion.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.discusion,
		dataType: 'JSON'
	})
	.done(function( _res ){
		self.discusion = {};
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-new-topic").modal('hide');
				var params= {};

				// ENTRADAS MAS NUEVAS
      			params._method = "all";
      			params._userid = id_user;
				self._set(params);

				// MIS ENTRADAS
      			params._method = "all_my_topics";
      			params._userid = id_user;
				self._set(params);

			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-new-topic").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 * 	NUEVA TEMA
 *	@params : {informacion enfoque}
 *	=======================================
 */
Discusiones.prototype._save_comment = function(){
	var self = this;
	var organizacion_id	= self.comment.organizacion_id;
	var tema_id					= self.comment.tema_id;
	$.ajax({
		url				: self.base_url,
		method		: 'POST',
		async			: false,
		data			: self.comment,
		dataType	: 'JSON'
	})
	.done(function( _res ){
		self.comment = {};
		if( _res.status ){
			// limpia el formulario
			$('#new-comment-topic')[0].reset();
			// console.log(_res);
			self._draw_only_comments_by_topic(_res);
			$('html, body').animate({
				scrollTop: $("#discusion_scroll").offset().top
			}, 2000);
		}
		setFlash(_res.msg, _res.class);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._rate_comment = function(){
	var self = this;
	var tema_id = self.comment.tema_id;
	/*---------------- debuggin ----------------*/
	// console.clear();
	// console.log(self.comment);
	// return false;
	/*---------------- debuggin ----------------*/
	$.ajax({
		url				: self.base_url,
		method		: 'POST',
		async			: false,
		data			: self.comment,
		dataType	: 'JSON'
	})
	.done(function( _res ){
		self.comment = {};
		if( !_res.status ){
			setFlash(_res.msg, _res.class);
		}

		if(_res.likes){
			// actualizar contador de likes
			$('.count-likes-'+ _res.comentario_id).empty().append(_res.likes);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
Discusiones.prototype._update_rating_topic = function(){
	var self = this;
	$.ajax({
		url				: self.base_url,
		method		: 'POST',
		async			: false,
		data			: self.rating,
		dataType	: 'JSON'
	})
	.done(function( _res ){
		setFlash(_res.msg, _res.class);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	EDITAR NUEVA TEMA
 *	@params : {info_aportacion}
 *	==============================
 */
Discusiones.prototype._edit = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.discusion,
		dataType: 'JSON'
	})
	.done(function( _res ){
		//console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	ELIMINAR TEMA
 *	@param : id_aportacion
 *	=========================
 */
Discusiones.prototype._delete = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done(function( _res ){
		//console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	EDITAR TEMA
 *	@param : {data}
 *	=========================
 */
Discusiones.prototype._update = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.discusion
	})
	.done(function( _res ){
		//console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	});
};
/*
 *	SET DATA TEMA
 *	==================
 */
Discusiones.prototype._set = function( _data ){
	this.discusion.roles 			= _data._roles || null;
	this.discusion.titulo 			= _data._title || null;
	this.discusion.descripcion		= _data._description || null;
	this.discusion.privacidad 		= _data._privacy || null;
	this.discusion.method 			= _data._method || null;
	this.discusion.id 				= _data._id || null;
	this.discusion.user_id			= _data._userid || null;
	this.discusion.invitados		= _data._invitados || null;

	if( this.discusion.method === 'all')
	{
		this._getall();
	}
	else if( this.discusion.method === 'all_most_visited')
	{
		this._getall_most_visited();
	}
	else if( this.discusion.method === 'all_most_voted')
	{
		this._getall_most_voted();
	}
	else if( this.discusion.method === 'all_my_topics')
	{
		this._getall_my_topics();
	}
	else if( this.discusion.method === 'byId')
	{
		this._byId();
	}
	else if( this.discusion.method === 'save' )
	{
		this._save();
	}
	else if( this.discusion.method === 'edit')
	{
		this._update();
	}
	else if( this.discusion.method === 'delete')
	{
		this._delete();
	}
	else if( this.discusion.method === 'add_visit')
	{
		this._addVisit();
	}
	// re order coments
	else if( this.discusion.method === 'order_by_date_asc')
	{
		this._orderComents();
	}
	else if( this.discusion.method === 'order_by_plus_likes')
	{
		this._orderComents();
	}
	else if( this.discusion.method === 'order_by_minus_likes')
	{
		this._orderComents();
	}
	else if( this.discusion.method === 'order_by_date_desc')
	{
		this._orderComents();
	}

};
/*
 *	SET DATA TEMA
 *	==================
 */
Discusiones.prototype._set_comment = function(_data){
	this.comment.id 				= _data._id || null;
	this.comment.method 			= _data._method || null;
	this.comment.comentario 		= _data._comentario || null;
	this.comment.organizacion_id	= _data._organizacion_id || null;
	this.comment.tema_id 			= _data._tema_id || null;

	if( this.comment.method === 'all'){
		// this._getall();
	}else if( this.comment.method === 'save_comment' ){
		this._save_comment();
	}else if( this.comment.method === 'byId'){
		this._byId();
	}else if( this.comment.method === 'rate_comment'){
		this._rate_comment();
	}

};
Discusiones.prototype._set_rating = function(_data){
	this.rating.method 			= _data._method || null;
	this.rating.calificacion 	= _data._calificacion || null;
	this.rating.organizacion_id	= _data._organizacion_id || null;
	this.rating.tema_id 		= _data._tema_id || null;
	if( this.rating.method === 'update_rating'){
		this._update_rating_topic();
	}
};
Discusiones.prototype._set_search = function(_data){
	this.search.method 			= _data._method || null;
	this.search.organizacion_id	= _data._organizacion_id || null;
	this.search.search 			= _data._search || null;

	if( this.search.method === 'search_topics'){
		this._getall_search();
	}
};
/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Discusiones.prototype._draw = function( _data ){
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#discusion-template").html() );
		var html = template( _data );
		$("#discusion").html( html );
	} else {
		$("#discusion").html('<h3 class="text-center">' + lang.translate('Aún no tienes discusiones, ¡Registra una!') + '</h3>');
	}
};
Discusiones.prototype._draw_search = function( _data ){
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#discusion-template").html() );
		var html = template( _data );
		$("#search-results").html( html );
	}else{
		setFlash(lang.translate('Lo sentimos no se encontraron resultados para tu búsqueda, intenta con una búsqueda nueva'), 'info');
		$("#search-results").html('<center><img src="assets/img/home/not_found.png"></center><br><h3 class="text-center">' + lang.translate('Lo sentimos no se encontraron resultados para tu búsqueda, intenta con una búsqueda nueva') + '.</h3>');
	}
};
Discusiones.prototype._draw_most_visited = function( _data ){
	$mostVisitedContainer = $('#profile');
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#discusion-template").html() );
		var html = template( _data );
		$mostVisitedContainer.html( html );
	} else {
		$mostVisitedContainer.html('<h3 class="text-center">' + lang.translate('Aún no tienes discusiones, ¡Registra una!') + '</h3>');
	}
};
Discusiones.prototype._draw_most_voted = function( _data ){
	$mostVotedContainer = $('#messages');
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#discusion-template").html() );
		var html = template( _data );
		$mostVotedContainer.html( html );
	} else {
		$mostVotedContainer.html('<h3 class="text-center">' + lang.translate('Aún no tienes discusiones, ¡Registra una!') + '</h3>');
	}
};
Discusiones.prototype._draw_my_topics = function( _data ){
	$mostVotedContainer = $('#settings');
	if ( _data.length >0){
		var template = Handlebars.compile( $("#discusion-template").html() );
		var html = template( _data );
		$mostVotedContainer.html( html );
	} else {
		$mostVotedContainer.html(_data);
	}
};



/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Discusiones.prototype._drawById = function( _data ){

	var discusion = _data.discusion;
	var comentario = _data.comentario;

	if ( discusion.length > 0 ){
		var template = Handlebars.compile( $("#discusion-template").html() );
		// Solo es necesario mandar un indice
		discusion = discusion[0];
		var html = template(discusion);
		// var html = template( discusion );
		$("#discusion").html( html );
	} else {
		$("#discusion").html('<h3 class="text-center">' + lang.translate('Aún no tienes discusiones, ¡Registra una!') + '</h3>');
	}

	if ( comentario.length > 0 ){
		var template_comentario = Handlebars.compile( $("#discusion_comentario-template").html() );
		var html = template_comentario( comentario );
		$("#discusion_comentario").html( html );
	} else {
		$("#discusion_comentario").html('<h3 class="text-center">' + lang.translate('Aún no tienes comentarios para este tema') + '</h3>');
	}
};

Discusiones.prototype._draw_only_comments_by_topic = function( _data ){
	var comentario = _data.comentario;
	if ( comentario.length > 0 ){
		var template_comentario = Handlebars.compile( $("#discusion_comentario-template").html() );
		var html = template_comentario( comentario );
		$("#discusion_comentario").html( html );
	} else {
		$("#discusion_comentario").html('<h3 class="text-center">' + lang.translate('Aún no tienes comentarios para este tema') + '</h3>');
	}
};
