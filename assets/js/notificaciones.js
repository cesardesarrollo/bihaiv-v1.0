/*
 *	CLASE NOTIFICACIONES
 *	===============
 */
function Notificaciones(){
	this.notificacion = {};
	this.base_url = "api/front/notificaciones.php";
}
/*
 *	TODAS LAS NOTIFICACIONES POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Notificaciones.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.notificacion
	})
	.done( function( _res ){
		var limit_start = _res.length + 1;
		var limit_end   = limit_start + 10;
		$("#notifications-more").attr('limit-start', 0);
		$("#notifications-more").attr('limit-end', limit_end);
		if (_res.length > 0){
			$("#notifications-more").css('display','block');
		} else{
			$("#notifications-more").css('display','none');
		}
		//console.log(_res);
		self._draw(_res);
	})
	.fail( function( _err ){
		$("#notificacion").html(_err.responseText);
		console.log( _err );
	})
};
/*
 *	SET DATA LEAD
 *	==================
 */
Notificaciones.prototype._set = function( _data ){
	this.notificacion = {};
	this.notificacion.method		= _data._method || null;
	this.notificacion.limit_param1	= _data._limit_param1 || null;
	this.notificacion.limit_param2	= _data._limit_param2 || null;
	this.notificacion.user_id		= _data._userid || null;

	if( this.notificacion.method === 'all')
	{
		this._getall();
	}
	if( this.notificacion.method === 'allByLimits')
	{
		this._getall();
	}
};
/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Notificaciones.prototype._draw = function( _data ){
	if ( _data.length > 0 ){
		var template = Handlebars.compile( $("#notificacion-template").html() );
		var html = template( _data );
		$("#notificacion").html( html );
	} else {
		$("#notificacion").html( '<h3 class="text-center">' + lang.translate('Aún no tienes notificaciones, ¡Comienza a vincularte!') + '</h3>' );
	}
};
/*
 *	HELPER
 *	=========================
 */
Handlebars.registerHelper('if', function(conditional, options) {
  if(conditional!=="") {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});


/*
*	MORE
*	==================
*/
$("#notifications-more").on('click', function(e){
	var n = new Notificaciones();
	e.preventDefault();
	var limit_start = $("#notifications-more").attr('limit-start');
	var limit_end = $("#notifications-more").attr('limit-end');
	var data = {
		_limit_param1 : parseInt(limit_start),
		_limit_param2 : parseInt(limit_end),
		_userid 	  : $("#notifications-more").attr('data-user'),
		_method 	  : 'allByLimits'
	};
	n._set( data );
})
