/*
 *	CLASE APORTACIONES
 *	===============
 */
function Aportacion(){
	this.aportacion = {};
	this.base_url = "api/front/aportacion.php";
}
/*
 *	TODAS LAS APORTACIONES POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
Aportacion.prototype._getall = function(){
	var data = {};
	var self = this;
	//AJAX REQUEST
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		dataType: 'JSON',
		data 	: self.aportacion
	})
	.done( function( _res ){
		self._draw(_res);
	})
	.fail( function( _err ){
		console.log( _err );
	})
};


/*
 *	TODAS LAS APORTACIONES DE UN LISTADO POR USUARIO
 *	@params : {id_usuario logueado || usuario visitado}
 *	=====================================================
 */
  Aportacion.prototype._getAllList = function(list){
    var data = {};
    var self = this;
    self.aportacion.method = 'all_best_practices';
    self.aportacion.user_id	= 1;
    //AJAX REQUEST
    $.ajax({
      url   : self.base_url,
      method  : 'POST',
      dataType: 'JSON',
      data  : self.aportacion
    })
    .done( function( _res ){
      self._drawList(_res, list);
    })
    .fail( function( _err ){
      console.log( _err );
    })
  };


/*
 *	APORTACION POR ID
 * 	@params : {id_aportacion}
 *	==========================
 */
Aportacion.prototype._byId = function(){
	var self = this;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		//dataType: 'JSON',
		data 	: self.aportacion
	})
	.done(function( _res ){
		console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 * 	NUEVA APORTACION
 *	@params : {informacion aportacion}
 *	=======================================
 */
Aportacion.prototype._save = function(){
	var self = this;
	var id_user = self.aportacion.user_id;

	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.aportacion,
		dataType: 'JSON'
	})
	.done(function( _res ){
		self.aportacion = {};
		if( _res.status ){
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-success');
			$("#icon").html('<i class="fa fa-check-square"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddEvent").css('display', 'none');
			setTimeout(function(){
				$("#m-new-share").modal('hide');
				var params= {};
      			params._method = "all";
      			params._userid = id_user;
				self._set(params);
			}, 2000);

		} else {
			$("#row-msg").slideToggle('fast');
			$("#panel-alert").addClass('alert-danger');
			$("#icon").html('<i class="fa fa-times-circle"></i>');
			$("#msg").html( _res.msg );
			$("#btnAddShare").css('display', 'none');
			setTimeout(function(){
				$("#m-new-share").modal('hide');
			}, 2000);
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	EDITAR NUEVA APORTACION
 *	@params : {info_aportacion}
 *	==============================
 */
Aportacion.prototype._edit = function(){
	var self = this;
	self.aportacion.action = self.http.edit;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		data 	: self.aportacion,
		dataType: 'JSON'
	})
	.done(function( _res ){
		console.log( _res );
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
Aportacion.prototype._update_rating = function(){
	var self = this;
	// self.aportacion.action = self.http.edit;
	$.ajax({
		url 		: self.base_url,
		method 		: 'POST',
		async 		: false,
		data 		: self.aportacion,
		dataType	: 'JSON'
	})
	.done(function( _res ){
		setFlash(_res.msg, _res.class);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	ELIMINAR APORTACION
 *	@param : id_aportacion
 *	=========================
 */
Aportacion.prototype._delete = function(){
	var self = this;
	var id_user = self.aportacion.user_id;
	$.ajax({
		url 	: self.base_url,
		method 	: 'POST',
		async 	: false,
		dataType: 'JSON',
		data 	: self.aportacion
	})
	.done(function( _res ){
		var params= {};
		params._method = "all";
		params._userid = id_user;
		self._set(params);
	})
	.fail(function( _err ){
		console.log( _err );
	})
};
/*
 *	SET DATA LEAD
 *	==================
 */
Aportacion.prototype._set = function( _data ){
	this.aportacion 				= {};
	this.aportacion.titulo 			= _data._title || null;
	this.aportacion.categoria 		= _data._category || 0;
	this.aportacion.photoCover 		= _data._photoCover || null;
	this.aportacion.file 			= _data._file || null;
	this.aportacion.descripcion		= _data._description || null;
	this.aportacion.method 			= _data._method || null;
	this.aportacion.user_id			= _data._userid || null;
	this.aportacion.tipo			= _data._type || null;
	this.aportacion.id 				= _data._id || null;
	this.aportacion.calificacion	= _data._calificacion || null;

	if( this.aportacion.method === 'all')
	{
		this._getall();
	}
	else if( this.aportacion.method === 'byId')
	{
		this._byId();
	}
	else if( this.aportacion.method === 'save' )
	{
		this._save();
	}
	else if( this.aportacion.method === 'edit')
	{
		this._update();
	}
	else if( this.aportacion.method === 'delete')
	{
		this._delete()
	}
	else if( this.aportacion.method === 'mejores-list')
	{
		this._getAllList('mejores-list');
	}
	else if( this.aportacion.method === 'update_rating')
	{
		this._update_rating();
	}

};
/*
 *	DRAW TEMPLATE HANDLEBARS
 * 	===========================
 */
Aportacion.prototype._draw = function( _data ){
	var total_length = _data.length;
	//CREATE NEW DATA
	var data = [],
		thumbnail = "";
	for (var i = 0; i <= total_length - 1; i++) {
		data.push({
			calificacion	: _data[i]._calificacion,
			descripcion		: _data[i]._descripcion,
			fechaCreado		: _data[i]._fechaCreacion,
			id				: _data[i]._idPublicacion,
			idAportacion	: _data[i]._idAportacion,
			post_id			: _data[i]._postId,
			thumbnail		: _data[i]._thumbnail,
			urlFile			: _data[i]._file,
			tipo			: _data[i]._tipo,
			titulo			: _data[i]._titulo,
			user_id			: _data[i]._usuario,
			stars			: _data[i]._stars
		});
	};
	if( data.length > 0 ){
		
		var template = Handlebars.compile( $("#shares-template").html() );
		var html = template( data );
		$("#shares").html( html );
	} else {
		// Felipe solución $("#shares").html(_data);
		$("#shares").html('<h3 class="text-center">' + lang.translate('Aún no tienes aportaciones, ¡Registra una!') + '</h3>');
	}
};

Aportacion.prototype._drawList = function( _data, list ){
if ( _data.length > 0 ){
	var template = Handlebars.compile( $("#" + list + "-template").html() );
	var html = template( _data );
	$("#" + list).html( html );
} else {
	$("#" + list).html('<h3 class="text-center">' + lang.translate('Aún no tienes documentos') + '</h3>');
}
}

/*
 *	HELPER APORTACIONES
 *	=========================
 */
Handlebars.registerHelper('if', function(conditional, options){
	return options.fn(this);
})

/*
 *	DELETE
 *	==================
 */
function deleteAportation(event, eventoId,userId){
	event.preventDefault();
	swal({
		title: "Eliminar",
		text: "¿Estas seguro de eliminar esta aportación?",
		type: "warning",
		showCancelButton: true,
		closeOnConfirm: false,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Eliminar",
		cancelButtonText: "Cancelar",
		closeOnCancel: false
	}, function (isConfirm) {
		if (isConfirm) {
			/* Eliminada */
			var aportacion = new Aportacion();
			var params = {};
			params._method 	= "delete";
			params._id 		= eventoId;
			params._userid 	= userId;
			aportacion._set(params);
			swal("Eliminada", "Aportacion eliminada con éxito.", "success")
		} else {
			/* Cancelada */
			swal("Cancelado", "Tu aportación no fue eliminada.", "error");
		}
	});

}
