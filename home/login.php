<?php 
	require_once('../core/autoload.php');
?>	
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Bihaiv | Iniciar Sesión</title>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/home.css">
</head>
<body class="body-login">
	<div class="Header__login">
		<div class="container">
			<div class="pull-left">
				<figure class="figure">
					<a href="<?php echo APP_PATH?>">
						<img src="../assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="50">
					</a>
				</figure>
			</div>
			<div class="pull-right">
				<a href="./registro.php" class="btn btn-primary"><?php echo translate('Únete'); ?></a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
				<div class="Form__login large z-depth-2">
					<h1 class="text-white no-margin margin-row-bottom"><?php echo translate('Iniciar Sesión'); ?></h1>
					<form action="../?action=processlogin" onsubmit="return validarEmail(event, this.email.value, this)" role="search" method="post" >
						<div class="form-group">
							<label for="txt__Username"><?php echo translate('Usuario'); ?>:</label>					    	
						<input autocomplete="off" value="" type="email" id="txt__Username" name="email" class="form-control" placeholder="<?php echo translate('Email'); ?>"  required>
						</div>
						<div class="form-group">
							<?php if(isset($_GET['referer'])){  
								$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
							?>
							<input type="hidden" name="referer" value="<?php echo $referer ?>">
							<?php } ?>
							<label for="txt__Password"><?php echo translate('Contraseña'); ?>:</label>
							<input autocomplete="off" value="" type="password" id="txt__Password" name="password" class="form-control" placeholder="<?php echo translate('Contraseña'); ?>"  required>
						</div>
						<button id="oAuth" type="submit" class="btn btn-primary btn-block"><?php echo translate('Iniciar Sesión'); ?></button>
						<div class="checkbox">
							<label>
								<input autocomplete="off" name="recuerdame" type="checkbox" > <?php echo translate('Recuérdame'); ?>
							</label>
							</div>
						<div class="form-group margin-row-top">
							<a href="recordar-contrasena.php"><?php echo translate('Olvidaste tu Contraseña?'); ?></a><br>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--SCRIPTS-->
	<script src="../res/jquery/jquery.min.js"></script>
	<script src="../res/bootstrap/js/bootstrap.min.js"></script>
	<script>
		function validarEmail(e, valor, form) {
			e.preventDefault();
			if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
				form.submit();
			} else {
				alert("<?php echo translate('¡La dirección de email es incorrecta!'); ?>");
				return false;
			}
		}
	</script>
</body>
</html>