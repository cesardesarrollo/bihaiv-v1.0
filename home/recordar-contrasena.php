<?php 
	require_once('../core/autoload.php');
	require_once('../core/modules/index/model/UserData.php');
?>	
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Bihaiv | <?=translate('Recordar contraseña')?></title>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/home.css">
	</head>
	<body class="body-login">
		<div class="Header__login">
			<div class="container">
				<div class="pull-left">
					<figure class="figure">
						<a href="<?php echo APP_PATH?>">
							<img src="../assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="50">
						</a>
					</figure>
				</div>
				<div class="pull-right">
					<a href="./login.php" class="btn btn-primary"><?=translate('¡Iniciar sesión!')?></a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
					<div class="Form__login large z-depth-2">
						<h1 class="text-white no-margin margin-row-bottom"><?=translate('Recordar contraseña')?></h1>
						<form action="../?action=processrememberpassword" role="search" method="post" >
						  	<div class="form-group">
						    	<label for="txt__Username"><?=translate('Email')?>:</label>					    	
							<input autocomplete="off" type="email" id="txt__Username" name="email" class="form-control" placeholder="<?=translate('Email')?>"  required>
						  	</div>
						  	<button id="oAuth" type="submit" class="btn btn-primary btn-block"><?=translate('Enviar contraseña')?></button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--SCRIPTS-->
		<script src="../res/jquery/jquery.min.js"></script>
		<script src="../res/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>