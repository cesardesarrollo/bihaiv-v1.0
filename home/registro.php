<?php
include ('../core/autoload.php');
include ('../core/modules/index/model/DaoPaises.php');
$DaoPaises = new DaoPaises();
$paises = $DaoPaises->getAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Bihaiv | <?php echo translate('Registro'); ?></title>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../res/owl/assets/owl.carousel.css">
	<link rel="stylesheet" href="../assets/css/home.css">
</head>
	<body class="body-login">
		<div class="Header__login">
			<div class="container">
				<div class="pull-left">
					<figure class="figure">
						<a href="<?=APP_PATH?>">
							<img src="../assets/img/home/logo_bihaiv.png" alt="Bihaiv" height="50">
						</a>
					</figure>
				</div>
				<div class="pull-right">
					<a href="login.php" class="btn btn-primary"><?php echo translate('Iniciar Sesión'); ?></a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
					<div class="Form__login margin-top-min large z-depth-2">
						<h1 class="text-white no-margin margin-row-bottom"><?php echo translate('Registro'); ?></h1>
						<p class="text-danger"><?php echo translate('¡Todos los campos son requeridos!'); ?></p>

						<div class="col-xs-12 col-md-6 col-lg-6">
							<form id="forma" role="form" method="post" action="../?action=processregister">
							  <div class="form-group">
							    <label for="txt__Organization"><?php echo translate('Organización'); ?>: <span class="text-danger">*</span></label>
							    <input autocomplete="off" type="text" name="name" class="form-control" id="txt__Organization" placeholder="<?php echo translate('Organización'); ?>" required>
							  </div>
							  <div class="form-group">
							    <label for="txt__Location"><?php echo translate('País'); ?>: <span class="text-danger">*</span></label>
								<select autocomplete="off" name="country" class='form-control' required>
								<?php
								foreach ($paises as $pais) {
									echo "<option value='".$pais->id."'>".$pais->nombre."</option>";
								}
								?>
								</select>
							  </div>
							  <div class="form-group">
							    <label for="txt__CompanyEmail"><?php echo translate('Correo Organizacional'); ?>: <span class="text-danger">*</span></label>
							    <input autocomplete="off" type="email" name="email" class="form-control" id="txt__CompanyEmail" placeholder="<?php echo translate('Correo Organizacional'); ?>">
							  </div>
							</div>
							<div class="col-xs-12 col-md-6 col-lg-6">
							  <div class="form-group">
							    <label for="txt__Password"><?php echo translate('Contraseña'); ?>: <span class="text-danger">*</span></label>
							    <input autocomplete="off" type="password" name="password" class="form-control" id="txt__Password" placeholder="********" minlength="8" required>
							  </div>
							  <div class="form-group">
							    <label for="txt__PasswordVerify"><?php echo translate('Repetir Contraseña'); ?>: <span class="text-danger">*</span></label>
							    <input autocomplete="off" type="password" name="confirm_password" class="form-control" id="txt__PasswordVerify" placeholder="********" minlength="8" required>
							  </div>
							  <div class="form-group">
							    <label for="txt__Rfc"><?php echo translate('RFC'); ?>: <span class="text-danger">*</span></label>
							    <input autocomplete="off" type="text" name="rfc" class="form-control" id="txt__Rfc" placeholder="<?php echo translate('RFC'); ?>" required maxlength="13">
							  </div>
					          <div class="checkbox">
					              <label>
					                <input type="checkbox" name="terminos" required><?php echo translate('Acepto términos y condiciones'); ?> <span class="text-danger">*</span>
													<br>
													<a href="../?view=terminos_condiciones"><?php echo translate('Ver términos y condiciones'); ?></a>
												</label>
					          </div>
							  <input type="submit" class="btn btn-primary btn-block" value="<?php echo translate('Registrarme'); ?>">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--SCRIPTS-->
		<script src="../res/jquery/jquery.min.js"></script>
		<script src="../res/bootstrap/js/bootstrap.min.js"></script>
		<script src="../res/owl/owl.carousel.min.js"></script>
		<script src="../res/handlebars/handlebars.js"></script>
		<script src="../assets/js/functions.js"></script>
		<script>
			$(document).on('submit', '#forma', function(e){
			  e.preventDefault();
				var txt__Password 			= $("#txt__Password").val();
				var txt__PasswordVerify = $("#txt__PasswordVerify").val();
				var txt__Rfc						= $("#txt__Rfc").val();
				var txt__CompanyEmail		= $("#txt__CompanyEmail").val();
        var vsExprReg 					= /^[a-z0-9\s.,_\-\@\&\/]+$/i;

				if(!$("#txt__CompanyEmail").val()){
					alert('Por favor ingresa tu correo electrónico, tiene que ser una estructura valida. Ejemplo name@example.com')
					return false;
				}
				if (txt__Password.length < 8){
					alert("<?php echo translate('El password debe contener al menos 8 caracteres.'); ?>");
					$("#txt__Password").focus();
					return false;
				}
				if (txt__Password !== txt__PasswordVerify) {
					alert("<?php echo translate('Los passwords ingresados no coinciden.'); ?>");
					$("#txt__Password").focus();
					return false;
				}
				if (!vsExprReg.test(txt__Password)){
					alert("<?php echo translate('Ha introducido caracteres invalidos.'); ?>");
					$("#txt__Password").focus();
					return false;
				}
				if (!vsExprReg.test(txt__CompanyEmail)){
					alert("<?php echo ('Solo puedes ingresar estos caracteres especiales: \"@-_.\" '); ?>");
					$("#txt__CompanyEmail").focus();
					return false;
				}
				if (txt__Rfc.trim().length === 0) {
					alert("<?php echo translate('RFC incorrecto.'); ?>");
					$("#txt__Rfc").focus();
					return false;
				}
				if (!vsExprReg.test(txt__Rfc)){
					alert("<?php echo translate('Ha introducido caracteres invalidos.'); ?>");
					$("#txt__Rfc").focus();
					return false;
				}
				this.submit();
			});
		</script>
	</body>
</html>
