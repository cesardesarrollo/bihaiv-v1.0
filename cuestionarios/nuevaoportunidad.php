<?php
	require_once("../core/autoload.php");
	require_once('../core/modules/index/model/UserData.php');
	require_once('../core/modules/index/model/CuestionarioData.php');
	require_once('../core/modules/index/model/CuestionariosUsuarioData.php');
	require_once('../core/modules/index/model/EstatusCuestionarioData.php');
	require_once('../core/modules/index/model/DaoOrganizacion.php');
	require_once('../core/modules/index/model/DaoRoles.php');
	require_once('../core/modules/index/model/DaoAportacion.php');
	require_once('../core/modules/index/model/DaoPublicaciones.php');
	require_once('../core/modules/index/model/DaoEvaluacion.php');
	require_once('../core/modules/index/model/DaoPregunta.php');
	require_once('../core/modules/index/model/DaoOpcionesRespuesta.php');

	$userdata = new UserData();
	$database = new Database();
	$database->connect();

	$user_id_ref = $_REQUEST['user_id'];	//User id evaluando
	$evaluar = @$_REQUEST['evaluar'];

	$CuestionarioData = new CuestionarioData();
	$CuestionariosUsuarioData = new CuestionariosUsuarioData();
	$EstatusCuestionarioData = new EstatusCuestionarioData();
	$DaoEvaluacion = new DaoEvaluacion();

	// Obtiene el cuestionario correspondiente
	$Cuestionario = $CuestionarioData->getCuestionario(2);
	// Obtiene cuestionariousuario correspondiente
	$CuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user_id_ref,$Cuestionario->idCuestionario);
	// Obtiene estatus
	$EstatusCuestionario = $EstatusCuestionarioData->getByidCuestionariosUsuario($CuestionariosUsuario->idCuestionariosUsuario);
	// Datos de usuario a evaluar
	$user = $userdata->getById($user_id_ref);
	// Estatus del cuestionario
	$estado = $EstatusCuestionario->estatus;
	$fechaEstatus = $EstatusCuestionario->fechaEstatus;
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    	<link rel="icon" href="assets/img/home/url.png" type="image/x-icon" />
		<title>Re evaluación de aspirante a actor de la comunidad Bihaiv</title>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../res/icons/css/font-awesome.min.css">
		<link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/base.css">
	</head>
	<body role="document">
		<section class="Profile__header">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-lg-4">
						<figure class="figure">
							<img src="../assets/img/home/logo_behaiv.png" alt="Behaiv.com" height="50">
						</figure>
					</div>
				</div>
			</div>
		</section>
		<section class="Profile__nav">
			<div class="container">
				<h2>Re evaluación de aspirante a actor de la comunidad Bihaiv</h2>
			</div>
		</section>
		<section class="Questions__Company">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 relative Company__Name">
					<h2>Organización: <strong><?php echo trim($user->name);?></strong></h2>
				</div>
			</div>
		</section>


		<div class="container padding-row">
			<?php
			/*nuevaoportunidad.php?user_id=*/
			//Comprueba que si se trata de situación donde ya fue re evaluado, que el tiempo que ha pasado desde su fecha de reasignacion a la actual supere los 6 meses
			if ($estado == 5){
				// Comprueba la caducidad
				if(!Cuestionario::vigencia($fechaEstatus,"tercer_oportunidad")){ //true si aun tiene vigencia
					Core::alert("Aún no han transcurrido 6 meses desde el último intento de registro a Bi Haiv, por lo tanto aún no es posible solicitar una nueva oportunidad.");
					$continua = false;
					return;
				}else $continua = true;
			}else $continua = true;
			if($continua){
				if(isset($evaluar)){
					//Si es una tercera oportunidad, se debe eliminar y comenzar la bitácora del usuario nuevamente
					if ($estado == 5){

						$data = array("idCuestionariosUsuario"=>$CuestionariosUsuario->idCuestionariosUsuario,"estatus"=>0);
						$EstatusCuestionarioData->set($data);
						$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();
						
						/*
						$query = "DELETE FROM `acceso_bitacora_cuest_validacion` WHERE `id_user` = ".$user->id."";
						$sql = mysql_query($query);
						$query = "DELETE FROM `acceso_bitacora_intento_de_registro` WHERE `actor_nombre` = '".$user->name."'";
						$sql = mysql_query($query); */

						Registro::enviar_email("cuestionario_perfilamiento","",$user->id,"","", 0);

					}else if ($estado != 4){

						echo "<h3>Se ha iniciado el proceso de evaluación por expertos de la comunidad.</h3>";
						Registro::enviar_email("reevaluacion","",$user->id,"","", 0);
						
						/*Update registro en bitácora de seguimiento*/
						$data = array("idCuestionariosUsuario"=>$CuestionariosUsuario->idCuestionariosUsuario,"estatus"=>4);
						$EstatusCuestionarioData->set($data);
						$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();

						/*Comienza proceso de evaluación, ahora es enviado a 6 expertos*/
						$expertos = Cuestionario::getExpertos($user->id);
				
						if(count($expertos) == 0) {
							echo "No existen expertos, nadie podrá validarte.";
							exit;
						}

						if(count($expertos) >= 3) {
							$max = 3;
						} else if(count($expertos) >= 2) {
							$max = count($expertos);
						} else if(count($expertos) == 1) {
							echo "Es necesario que el sistema cuente con 2 expertos registrados para continuar con el procedimiento.";
							exit;
						}

						$claves_aleatorias = array_rand($expertos, $max);

						foreach ($claves_aleatorias as $key) {
							$expertos_random[] = $expertos[$key]->id;
						}

						foreach ($expertos_random as $value) {
							// Registro de evaluación creada
					        $x = new Evaluacion();
					        $x->setFechaEvaluacion(date('Y-m-d H:i:s')); 
					        $x->setIdCuestionariosUsuario($CuestionariosUsuario->idCuestionariosUsuario);
					        $x->setUsuario_id($value);
					        $x->setConfidencialidad(0); 
					        $x->setRespuesta(0); 
							$evaluaciones = $DaoEvaluacion->add($x);
							
							if ($evaluaciones > 0) { 
								Registro::enviar_email("resultados_a_experto","",$value,"",$user->id, 0);
							}

						}

						/*Update registro en bitácora de seguimiento*/
						$data = array("idCuestionariosUsuario"=>$CuestionariosUsuario->idCuestionariosUsuario,"estatus"=>4);
						$EstatusCuestionarioData->set($data);
						$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();
						
					}else if ($estado == 4){
						echo "Esta acción ya no es válida.";
					}
				}
			?>
			
			<?php

			if(!isset($evaluar)) :

			// Obtener organización
			$sql = "SELECT * FROM  `organizacion` WHERE `Usuarios_idUsuarios` = '".$user->id."'";
			$query = Executor::doit($sql);
			$organizacion = Model::one($query[0],new Organizacion());


			/*El usuario y prosigue a cargar cuestionario activo de perfilamiento*/
			$sql = "SELECT * FROM cuestionario WHERE activo = 1 and nivel  = 1 LIMIT 0,1";
			$query = Executor::doit($sql);
			$cuestionario = Model::one($query[0],new Cuestionario());
			echo "<h2>Cuestionario: ".$cuestionario->nombre."</h2>";
			echo "<form method='post'>";
				echo "<div class='row'><div class='col-md-12'>";
					echo "<ol>";
						$sql = "SELECT * FROM pregunta WHERE Cuestionario_idCuestionario = ".$cuestionario->idCuestionario." ORDER BY Orden";
						$query = Executor::doit($sql);
						$preguntas = Model::many($query[0],new Pregunta());
						foreach ($preguntas as $pregunta) {
						echo "<li><strong>".$pregunta->Pregunta."</strong>";

							$sql = "SELECT * FROM opcionesrespuesta WHERE Pregunta_idPregunta = ".$pregunta->idPregunta." ORDER BY Orden";
							$query = Executor::doit($sql);
							$respuestas = Model::many($query[0],new OpcionesRespuesta());
							echo "<ul class='list-inline'>";
								foreach ($respuestas as $respuesta) {
									$sql = "SELECT * FROM respuestasusuario WHERE user_id = ".$organizacion->id." AND  OpcionesRespuesta_idOpcionesRespuesta = ".$respuesta->idOpcionesRespuesta."";
									$query = Executor::doit($sql);
									if ($query[0]->num_rows == 1) {
										echo "<li>".$respuesta->Texto."</li>";
									}
								}
							echo "</ul>";


						echo "</li>";
						}
					echo "</ol>";
				echo "</div>";
			echo "</div>";

			?>
			<h2>Datos  de validación</h2>
			<div class="form-group">
				<label for="">Teléfono:</label>
				<p><?=$organizacion->telefono?></p>
			</div>
			<div class="form-group">
				<label for="">Url:</label>
				<p><?=$organizacion->urlWeb?></p>
			</div>
			<div class="form-group">
				<label for="">Ubicación:</label>
				<p><?=$organizacion->ubicacion?></p>
			</div>
			<div class="form-group">
				<label for="">Misión:</label>
				<p><?=$organizacion->mision?></p>
			</div>
			<div class="form-group">
				<label for="">Visión:</label>
				<p><?=$organizacion->vision?></p>
			</div>
			<div class="form-group">
				<label for="">Acerca de mi:</label>
				<p><?=$organizacion->quienSoy?></p>
			</div>
			<?php
				// Obtener aportaciones
				$sql = "SELECT * FROM  `publicaciones` WHERE `user_id` = '".$organizacion->id."'";
				$query = Executor::doit($sql);
				$publicaciones = Model::many($query[0],new Publicaciones());
				if (count($publicaciones) > 0) {
					echo "<h2>Documentos del actor para evaluar</h2>";
					foreach ($publicaciones as $key => $value) {
						$sql = "SELECT * FROM  `aportacion` WHERE `post_id` = '".$publicaciones[$key]->id."'";
						$query = Executor::doit($sql);
						$aportacion = Model::one($query[0],new Aportacion());
						if (count($aportacion) > 0) {
							echo "<a href='".$aportacion->thumbnail."'>";
								echo $aportacion->titulo;
							echo "</a> ";
							echo "<p>".$aportacion->descripcion."</p>";
						}
					}
				}
			?>
			
			<form>
				<input type="hidden" value="<?php echo $user->id;?>" name="user_id" />
				<button class="btn btn-success" name="evaluar" value="ok">Solicitar nueva evaluación</button>
			</form>
			<?php
					endif;
				}
			?>
			</div> <!-- /container -->
			<br><br>
			<script src="../res/jquery/jquery.min.js"></script>
			<script src="../res/bootstrap/js/bootstrap.min.js"></script>
		</body>
	</HTML>