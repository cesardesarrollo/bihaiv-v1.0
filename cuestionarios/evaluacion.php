<?php 
	session_start();
	require_once("../core/autoload.php");
	if(!isset($_SESSION['user_id']) || (int)$_SESSION['user_id'] !== (int)$_REQUEST['evaluador'] ){
		//Core::alert((string)$_SESSION['user_id']. ' ' .(string)$_REQUEST['evaluador']);
		Core::alert("Accede al sistema con tus credenciales y regresa a esta página");
		Core::redir(APP_PATH."home/login.php?referer=evaluacion");
		exit;
	}

	require_once('../core/modules/index/model/UserData.php');
	require_once('../core/modules/index/model/CuestionarioData.php');
	require_once('../core/modules/index/model/CuestionariosUsuarioData.php');
	require_once('../core/modules/index/model/EstatusCuestionarioData.php');
	require_once('../core/modules/index/model/DaoOrganizacion.php');
	require_once('../core/modules/index/model/DaoRoles.php');
	require_once('../core/modules/index/model/DaoAportacion.php');
	require_once('../core/modules/index/model/DaoPublicaciones.php');
	require_once('../core/modules/index/model/DaoEvaluacion.php');
    //require_once('../../core/lang/lang.php');
	$userdata = new UserData();
	$database = new Database();
	$database->connect();

	$user_id_ref = $_REQUEST['user_id'];	//User id evaluando
	$evaluador   = $_REQUEST['evaluador'];	//User id evaluador

	// Datos de usuario a evaluar y evaluador
	$user = $userdata->getById($user_id_ref);
	$user_evaluador = $userdata->getById($evaluador);

	// Valida que usuarios existan
	if(count($user_evaluador) < 1 || count($user) < 1 ){
		Core::alert("Operación no permitida");
		exit;
	}

	$CuestionarioData = new CuestionarioData();
	$CuestionariosUsuarioData = new CuestionariosUsuarioData();
	$EstatusCuestionarioData = new EstatusCuestionarioData();
	$DaoEvaluacion = new DaoEvaluacion();

	// Obtiene el cuestionario correspondiente
	$Cuestionario = $CuestionarioData->getCuestionario(2);
	// Obtiene cuestionariousuario correspondiente
	$CuestionariosUsuario = $CuestionariosUsuarioData->getByUserCuestionario($user->id,$Cuestionario->idCuestionario);
	// Obtiene estatus
	$EstatusCuestionario = $EstatusCuestionarioData->getByidCuestionariosUsuario($CuestionariosUsuario->idCuestionariosUsuario);
	// Estatus del cuestionario
	$estado = (int)$EstatusCuestionario->estatus;

	// Valida que evaluador tenga permitido validar
	$evaluacionValida = $DaoEvaluacion->getByIdCuestionariosUsuarioIdUsuarios($CuestionariosUsuario->idCuestionariosUsuario,$user_evaluador->id);

	if(empty($evaluacionValida->id)) {
		Core::alert("Operación no válida.");
		exit;
	} else {
		if ((int)$evaluacionValida->respuesta > 0){	
			Core::alert("Este cuestionario ya fue validado.");
			exit;
		}
	}
?>
<!doctype HTML>
<HTML Lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    	<link rel="icon" href="assets/img/home/url.png" type="image/x-icon" />
		<title>Evaluación de aspirante a actor | Bihaiv</title>
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="../res/icons/css/font-awesome.min.css">
		<link rel="stylesheet" href="../res/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/base.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.1.1/jquery.rateyo.min.css">
	</head>
	<body role="document">
		<section class="Profile__header">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-lg-4">
						<figure class="figure">
							<img src="../assets/img/home/logo_behaiv.png" alt="Behaiv.com" height="50">
						</figure>
					</div>
				</div>
			</div>
		</section>
		<section class="Profile__nav">
			<div class="container">
				<h2>Evaluación para la organización</h2>
			</div>
		</section>
		<section class="Questions__Company">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 relative Company__Name">
					<p><strong>Organización:</strong> <?php echo trim($user->name);?></p>
					<?php if ( isset($_SESSION['user_id']) AND is_numeric($_SESSION['user_id']) ): 
					$DaoOrganizacion = new DaoOrganizacion();
					$org = $DaoOrganizacion->getByUserId( $user->id );?>
              <!-- Validamos que el usuario sea diferente al id del perfil -->
                  
                <?php endif; ?>
					<p class="lead">Evaluador (tu): <?php echo trim($user_evaluador->name);?></p>
				</div>
			</div>
		</section>

  		<div class="container padding-row">
			<?php
			if($estado === 4 || $estado === 2){

				if (isset($_REQUEST['aprobar'])){

					echo "<h1> Resultado de aprobación : ".$_REQUEST['aprobar']."</h1>";

					// Registro de evaluacion de actor evaluando
					$EvaluacionEvaluador = $DaoEvaluacion->getByIdCuestionariosUsuarioIdUsuarios($CuestionariosUsuario->idCuestionariosUsuario,$user_evaluador->id);

					$retroalimentacion = strip_tags(trim($_REQUEST['retroalimentacion']));
					if ($_REQUEST['aprobar'] === "si") {
						$aprobar = 1;
					} else {
						$aprobar = 2;
					}
					$confidencialidad = $_REQUEST['confidencialidad'];

					// Actualizar Evaluación
                    $evaluacion = $DaoEvaluacion->getById($EvaluacionEvaluador->id);
					$evaluacion->setFechaEvaluacion(date('Y-m-d H:i:s')); 
			        $evaluacion->setRespuesta($aprobar);
			        $evaluacion->setComentarios($retroalimentacion); 
			        $evaluacion->setConfidencialidad($confidencialidad);
					$evaluaciones = $DaoEvaluacion->update($evaluacion);

					// Actores
					if($estado === 2) {
						$estatus = 3;
						$tipoRel = 1;
					}
					// Expertos
					else if($estado === 4) {
						$estatus = 5;
						$tipoRel = 2;
					}

					$contestaron_si = $DaoEvaluacion->getCountByIdCuestionariosUsuarioTipoRelRespuesta($CuestionariosUsuario->idCuestionariosUsuario,$tipoRel,1);
					$contestaron_no = $DaoEvaluacion->getCountByIdCuestionariosUsuarioTipoRelRespuesta($CuestionariosUsuario->idCuestionariosUsuario,$tipoRel,2);
					$contestaron = $DaoEvaluacion->getCountByIdCuestionariosUsuarioTipoRelRespuesta($CuestionariosUsuario->idCuestionariosUsuario,$tipoRel,3);
					$enviado = $DaoEvaluacion->getCountByIdCuestionariosUsuarioTipoRelRespuesta($CuestionariosUsuario->idCuestionariosUsuario,$tipoRel,4);

					if($contestaron===$enviado){
						
						/*Si a todos los actores que se les há enviado ya han contestado*/
						if ($contestaron>0){

							if ($contestaron_no >= 2)
								$aprovacion = false;
							else
								$aprovacion = true;

							if($aprovacion)
								Registro::enviar_email("finalizacion_aprovada_evaluacion","",$user->id,"","",0);
							else
								Registro::enviar_email("finalizacion_rechazada_evaluacion","",$user->id,"","", 0);

							$data = array("idCuestionariosUsuario"=>$CuestionariosUsuario->idCuestionariosUsuario,"estatus"=>$estatus);
							$EstatusCuestionarioData->set($data);
							$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();

						}
					} else {

						// Si aún no contestan todos se verifica que no haya caducado el plazo de 15 días
						// Si caduca el plazo desde la creación del cuestionario para el usuario, se tomará como total de evaluaciones las actuales


						// Aquí cambia por fecha de creacion de CuestionarioUsuario   fechaCreacion
						if(!Cuestionario::vigencia($CuestionariosUsuario->fechaCreacion,"plazo_evaluacion")){
						//if(!Cuestionario::vigencia($evaluacion->fechaEvaluacion,"plazo_evaluacion")){

							// La vigencia terminó y se finaliza 
							// Si al menos uno ha validado
							if ($contestaron>0){
								if ($contestaron_no >= 2)
									$aprovacion = false;
								else
									$aprovacion = true;

								if($aprovacion)
									Registro::enviar_email("finalizacion_aprovada_evaluacion","",$user->id,"","",0);
								else
									Registro::enviar_email("finalizacion_rechazada_evaluacion","",$user->id,"","",0);

								$data = array("idCuestionariosUsuario"=>$CuestionariosUsuario->idCuestionariosUsuario,"estatus"=>$estatus);
								$EstatusCuestionarioData->set($data);
								$EstatusCuestionarioCreado = $EstatusCuestionarioData->add();
							}	
						} 
					}
				}
			?>

			<?php if(!isset($_REQUEST['aprobar']) && ($estado === 4 || $estado === 2)){ ?>
			<form method="post">
				<input type="hidden" value="<?php echo $user_id_ref;?>" name="user_id" />
				<input type="hidden" value="<?php echo $evaluador;?>" name="evaluador" />
				<?php 
					// Obtener organización
					$sql = "SELECT * FROM  `organizacion` WHERE `Usuarios_idUsuarios` = '".$user->id."'";
					$query = Executor::doit($sql);
					$organizacion = Model::one($query[0],new Organizacion());
				?>
				  <h2>Datos  de validación</h2>
				  <div class="form-group">
				    <label for="">Telefono:</label>
				    <p><?=$organizacion->telefono?></p>
				  </div>
				  <div class="form-group">
				    <label for="">Url:</label>
				    <p><?=$organizacion->urlWeb?></p>
				  </div>
				  <div class="form-group">
				    <label for="">Ubicación:</label>
				    <p><?=$organizacion->ubicacion?></p>
				  </div>
				  <div class="form-group">
				    <label for="">Misión:</label>
				    <p><?=$organizacion->mision?></p>
				  </div>
				  <div class="form-group">
				    <label for="">Visión:</label>
				    <p><?=$organizacion->vision?></p>
				  </div>
				  <div class="form-group">
				    <label for="">Acerca de mi:</label>
				    <p><?=$organizacion->quienSoy?></p>
				  </div>
				<?php 
					// Obtener aportaciones
					$sql = "SELECT * FROM  `publicaciones` WHERE `user_id` = '".$organizacion->id."'";
					$query = Executor::doit($sql);
					$publicaciones = Model::many($query[0],new Publicaciones());

					if (count($publicaciones) > 0) {
						echo "<h2>Documentos del actor para evaluar</h2>";
						foreach ($publicaciones as $key => $value) {
							$sql = "SELECT * FROM  `aportacion` WHERE `post_id` = '".$publicaciones[$key]->id."'";
							$query = Executor::doit($sql);
							$aportacion = Model::one($query[0],new Aportacion());

							if (count($aportacion) > 0) {
								echo "<a href='".$aportacion->thumbnail."'>";
								echo $aportacion->titulo;
								echo "</a> ";
								echo "<p>".$aportacion->descripcion."</p>";
							}
						}
					}
				?>

				  <div class="form-group">
				    <label for="">Retroalimentación:</label>
				    <p><textarea class="form-control" id="retroalimentacion" name="retroalimentacion"></textarea></p>
				  </div>
				  <div class="form-group">
				    <label for="">Confidencialidad:</label>
				    <p>
				    	<select class="form-control" id="confidencialidad" name="confidencialidad">
							<option value="0">Informar mi identidad a usuario evaluado</option>
							<option value="1">Mantener mi identidad anónima</option>
						</select>
				    </p>
				  </div>
				  <div class="form-group">
				  		<label>Califica a </label><strong><?php echo ' ' . trim($user->name);?></strong>
				  		<p style="">
		                    <input type="hidden" id="calificador" value="<?php echo $_SESSION['user_id']; ?>">
		                    <input type="hidden" id="organizacionId" value="<?php echo $org->id;; ?>">
		                    <div style="" id="rateYo-organizacion" class="rateYo-organizacion"></div>
		              	</p>
				  </div>
				<hr/>
				<button class="btn btn-success" name="aprobar" value="si">APROBAR</button>
				<button class="btn btn-danger" name="aprobar" value="no">RECHAZAR</button>
			</form>
			<?php 
				} 
			} else echo "Esta acción ya no es posible realizarse.";
			?>
		</div> <!-- /container -->
		<script src="../res/jquery/jquery.min.js"></script>
		<script src="../res/bootstrap/js/bootstrap.min.js"></script>
     	<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.1.1/jquery.rateyo.min.js"></script>
		<script src="../res/toast.js"></script>
    	<link rel="stylesheet" type="text/css" href="../res/toast.css">
     	<script>
     	var opts = {
		    "closeButton" : true,
		    "debug" : false,
		    "positionClass" : "toast-bottom-right",
		    "onclick" : null,
		    "showDuration" : "600",
		    "hideDuration" : "1300",
		    "timeOut" : "7500",
		    "extendedTimeOut" : "1000",
		    "showEasing" : "swing",
		    "hideEasing" : "linear",
		    "showMethod" : "fadeIn",
		    "hideMethod" : "fadeOut"
		  };
		function set_flash(msg, clase){
		    switch (clase){
		      case 'danger' :
		      toastr.error(msg, '¡Error!', opts);
		      break;
		      case 'error' :
		      toastr.error(msg, '¡Error!', opts);
		      break;
		      case 'success' :
		      toastr.success(msg, '¡Perfecto!', opts);
		      break;
		      case 'warning' :
		      toastr.warning(msg, 'Atención', opts);
		      break;
		      default :
		      toastr.info(msg, 'Mensaje', opts);
		      break;
		    }
		  }
		  function setFlash(msg, clase){
		    set_flash(msg, clase);
		  }
                    $(function(){
                      var url_api_actores = '../api/front/actores.php';
                      var ratePerfilDefault = 0;
                      var calificador       = $('#calificador').val();
                      var organizacionId    = $('#organizacionId').val();
                      // GET current calificación
                      $.post(url_api_actores, {
                        'user_id' : calificador,
                        'organizacion_calificada_id' : organizacionId,
                        'method': 'get_rating'
                      },function(response){
                        if(response.status){
                          ratePerfilDefault = parseInt(response.calificacion);
                        }
                       
                        $rateYo = $(".rateYo-organizacion").rateYo({
                          rating    : ratePerfilDefault,
                          starWidth : "16px",
                          ratedFill : '#fae617',
                          precision : 0,
                          spacing   : "2px",
                          onSet: function (rating, rateYoInstance) {
                            // valida calificacion
                            if( parseInt(rating) > 0 ){
                              //valida id de calificador
                              if( parseInt( calificador ) > 0 ){
                                // Valida id de organizacion a calificar
                                if( parseInt( organizacionId ) > 0 ){
                                  var data = {
                                    'calificacion'                : rating,
                                    'user_id'                     : calificador,
                                    'organizacion_calificada_id'  : organizacionId,
                                    'method'                      : 'update_rating',
                                  };
                                  $.post(url_api_actores, data, function(response){
                                    setFlash(response.msg, response.class);
                                  });
                                }else{
                                  setFlash('Ocurrio un error inesperado ', 'warning');
                                }
                              }else{
                                setFlash('Ocurrio un error inesperado ', 'warning');
                              }

                            }else{
                              setFlash('No puedes otorgar una calificacion menor a 1 intentalo de nuevo. ', 'warning');
                            }
                          }
                        });
                      });
                    });
                  </script>
	</body>
</HTML>
