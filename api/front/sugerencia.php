<?php
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoSugerencia.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';

$DaoSugerencia   = new DaoSugerencia();
$DaoOrganizacion = new DaoOrganizacion();

// Se configura para inyectar o no amigos en listado de sugerencias
if (isset($_POST['friends_inyect'])) {
    $DaoSugerencia->setFriendsInyect($_POST['friends_inyect']);
}

// Se configura para incluir o no relación con roles en listado de sugerencias
if (isset($_POST['same_rol'])) {
    $DaoSugerencia->setSameRol($_POST['same_rol']);
}

$action = $_POST['method'];

switch($action){

case "allByOrgIdExceptCurrents":

    if (isset($_POST['key_array'])) {
        $key_current = $_POST['key_array'];
    } else {
        $key_current = [];
    }

    if (strlen($_POST['user_id'])>0) {
        $org = $DaoOrganizacion->getById($_POST['user_id']);
        $miUserId = $org->Usuarios_idUsuarios;
        $sugerencias = array();

        //ACTORES POR ENFOQUES Y ROL DENTRO DE ALCANCE
        $actorsByEnfoque = $DaoSugerencia->getActorsByEnfoque($miUserId);
        if (count($actorsByEnfoque) <= 0) {
            $actorsByEnfoque = array();
        }

        //ACTORES DEL MISMO ROL
        $actorsByRol = $DaoSugerencia->getActorsByRol($miUserId);
        if (count($actorsByRol) <= 0) {
            $actorsByRol = array();
        }

        // AMIGOS DE AMIGOS (fofs)
        $friendsOfFriend = $DaoSugerencia->getFriendsOfFriend($miUserId);
        if (count($friendsOfFriend) <= 0) {
            $friendsOfFriend = array();
        }

        $sugerencias = array_merge($actorsByEnfoque, $actorsByRol, $friendsOfFriend);
        $sugerenciasAll = Core::unique_multidim_array_except($sugerencias, 'user_id', $key_current);
        $sugerencias = array_slice($sugerenciasAll, 0, 4);
        echo json_encode($sugerencias);

        /* Felipe adaptación
        if (count($sugerencias)>0)
        echo json_encode($sugerencias);
        else
        echo "<h3 class=\"text-center\">".translate('label noSugerencias')."</h3>";
        */

    } else {
        echo "Error";
    }

    break;

case "actorsByEnfoque":

    if (strlen($_POST['user_id'])>0) {
        $miUserId = $_POST['user_id'];
        $sugerencias = array();

        //ACTORES POR ENFOQUES Y ROL DENTRO DE ALCANCE
        $actorsByEnfoque = $DaoSugerencia->getActorsByEnfoque($miUserId);
        if (count($actorsByEnfoque) <= 0) {
            $actorsByEnfoque = array();
        }

        $sugerencias = array_merge($actorsByEnfoque);
        $sugerenciasAll = Core::unique_multidim_array($sugerencias, 'user_id');
        $sugerencias = array_slice($sugerenciasAll, 0, 4);
        echo json_encode($sugerencias);

    }

    break;

case "actorsByEnfoqueExcept":

    if (isset($_POST['key_array'])) {
        $key_current = $_POST['key_array'];
    } else {
        $key_current = [];
    }

    if (strlen($_POST['user_id'])>0) {
        //$org = $DaoOrganizacion->getById($_POST['user_id']);
        //$miUserId = $org->Usuarios_idUsuarios;
        $miUserId = $_POST['user_id'];
        $sugerencias = array();

        //ACTORES POR ENFOQUES Y ROL DENTRO DE ALCANCE
        $actorsByEnfoque = $DaoSugerencia->getActorsByEnfoque($miUserId);
        if (count($actorsByEnfoque) <= 0) {
            $actorsByEnfoque = array();
        }

        $sugerencias = array_merge($actorsByEnfoque);
        $sugerenciasAll = Core::unique_multidim_array_except($sugerencias, 'user_id', $key_current);
        $sugerencias = array_slice($sugerenciasAll, 0, 4);
        echo json_encode($sugerencias);
    }

    break;

case "all":

    if (strlen($_POST['user_id'])>0) {
        $miUserId = $_POST['user_id'];
        $sugerencias = array();

        //ACTORES POR ENFOQUES Y ROL DENTRO DE ALCANCE
        $actorsByEnfoque = $DaoSugerencia->getActorsByEnfoque($miUserId);
        if (count($actorsByEnfoque) <= 0) {
            $actorsByEnfoque = array();
        }

        //ACTORES DEL MISMO ROL
        $actorsByRol = $DaoSugerencia->getActorsByRol($miUserId);
        if (count($actorsByRol) <= 0) {
            $actorsByRol = array();
        }

        // AMIGOS DE AMIGOS (fofs)
        $friendsOfFriend = $DaoSugerencia->getFriendsOfFriend($miUserId);
        if (count($friendsOfFriend) <= 0) {
            $friendsOfFriend = array();
        }

        $sugerencias = array_merge($actorsByEnfoque, $actorsByRol, $friendsOfFriend);
        $sugerenciasAll = Core::unique_multidim_array($sugerencias, 'user_id');
        $sugerencias = array_slice($sugerenciasAll, 0, 4);
        echo json_encode($sugerencias);

    } else {
        echo "Error";
    }

    break;

case "delete":
    break;

case "edit":
    break;

case "save":
    break;

case "byId":
    break;

}
