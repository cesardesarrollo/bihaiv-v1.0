<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/app/defines.php';
require_once '../../core/modules/index/model/DaoMap.php';

require_once '../../core/modules/index/model/DaoExperto.php';
require_once '../../core/modules/index/model/DaoUsuarios.php';
require_once '../../core/modules/index/model/DaoRoles.php';
require_once '../../core/modules/index/model/DaoIniciativa.php';
require_once '../../core/modules/index/model/DaoEstados.php';

$action = $_REQUEST['method'];

$DaoMap       = new DaoMap();
$Usuarios     = new DaoUsuarios();
$Roles        = new DaoRoles();
$Expertos     = new DaoExperto();
$Iniciativas  = new DaoIniciativa();

$DaoEstados   = new DaoEstados();
$Estados      = new Estados();

switch($action){
  //Actores
case "get_markers":
    $response = $DaoMap -> getMarkers($_REQUEST);
    if ($_POST['estado']) {
        $Estados = $DaoEstados->getByName($_POST['estado']);
        if (!empty($Estados->id)) {
            if (count($response['actores']) > 0) {
                $array_nuevo = array();

                foreach ($response['actores'] as $actor) {
                    if ($actor['idEstado'] == $Estados->id) {
                        array_push($array_nuevo, $actor);
                    }
                } 
                $response['actores'] = [];
                $response['actores'] = $array_nuevo;
            }
        }
    }
    print_r(json_encode($response));
    break;

  // Expertos
case "get_expertos":
    $expertos = json_decode(json_encode($Expertos -> getAll()), true);

    $array_respuesta = array();
    $array_push = [];
    foreach ($expertos as $experto) {
        $array_push = array("value"   => $experto['id'],
                            "option" => $experto['organizacion']);
        array_push($array_respuesta,  $array_push);
    }
    print_r(json_encode($array_respuesta));
    break;

  // Roles
case "get_roles":
    $roles = json_decode(json_encode($Roles -> getAll()), true);
    
    $array_respuesta = array();
    $array_push = [];
    foreach ($roles as $rol) {
        $array_push = array("value"   => $rol['id'],
                            "option" => $rol['nombre']);
        array_push($array_respuesta,  $array_push);
    }
    print_r(json_encode($array_respuesta));
    break;

  // Categorias de actividades
case "get_activities_categories":
    $array_respuesta = array( ["value" => "INICIATIVAS","option" => "Iniciativas"],
                          ["value" => "EVENTOS","option" => "Eventos"]);

    print_r(json_encode($array_respuesta));
    break;

  // Actores
case "get_actores":
    $actores = json_decode(json_encode($Usuarios -> getAll()), true);
    
    $array_respuesta = array();
    $array_push = [];
    foreach ($actores as $actor) {
        $array_push = array("value"   => $actor['id'],
                            "option" => $actor['name']);
        array_push($array_respuesta,  $array_push);
    }
    print_r(json_encode($array_respuesta));
    break;

  // Eventos
case "get_eventos":
    $actores = array( ["id" => "TALLER","name" => "Taller"],
                  ["id" => "FESTIVAL","name" => "Festival"],
                  ["id" => "CONFERENCIA","name" => "Conferencia"],
                  ["id" => "EXPOSICION","name" => "Exposición"] );

    $array_respuesta = array();
    $array_push = [];
    foreach ($actores as $actor) {
        $array_push = array("value"   => $actor['id'],
                          "option" => $actor['name']);
        array_push($array_respuesta,  $array_push);
    }
    print_r(json_encode($array_respuesta));
    break;

  // Iniciativas
case "get_iniciativas":
    $iniciativas = json_decode(json_encode($Iniciativas -> getAll()), true);

    $array_respuesta = array();
    $array_push = [];
    foreach ($iniciativas as $actor) {
        $array_push = array("value"   => $actor['idIniciativa'],
                            "option" => $actor['titulo']);
        array_push($array_respuesta,  $array_push);
    }
    print_r(json_encode($array_respuesta));
    break;

}
