<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DaoIniciativa.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoColaboracion.php';
require_once '../../core/modules/index/model/NotificationData.php';
require_once '../../core/modules/index/model/DaoObjetivo.php';
require_once '../../core/modules/index/model/FriendData.php';
require_once '../../core/modules/index/model/DaoEvento.php';
require_once '../../core/modules/index/model/UserData.php';
require_once '../../core/modules/index/model/DaoUbicacionesIniciativas.php';
require_once '../../core/lang/lang.php';
$UserData = new UserData();

$DaoPublicaciones= new DaoPublicaciones();
$Publicaciones = new Publicaciones();

$DaoIniciativa= new DaoIniciativa();
$Iniciativa = new Iniciativa();

$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$DaoEvento = new DaoEvento();
$Evento = new Evento();

$FriendData = new FriendData();

$DaoColaboracion = new DaoColaboracion();
$Colaboracion = new Colaboracion();

$DaoObjetivo      = new DaoObjetivo();

$action = $_POST['method'];
$tablename = "iniciativa";


switch($action){

case "allByLimits":
    if (strlen($_POST['user_id'])>0) {
        $limit_param1 = ($_POST['limit_param1']) ? $_POST['limit_param1'] : 0;
        $limit_param2 = ($_POST['limit_param2']) ? $_POST['limit_param2'] : 5;

        $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
        $query = "select p.id as _publicacionId, p.fechaCreado as publicacionCreado, p.tipo as publicacionTipo, p.user_id as publicacionUsuario,
                ini.idIniciativa as _Id, ini.titulo as _Titulo, ini.textoCorto as _descripcion, ini.fechaIniciativa as _fecha, ini.horaIni as _horaInicio,
                ini.horaFin as _horaFin, ini.llaveImg as _thumbnail, ini.lugar as _lugar, ini.post_id as _postId
                from publicaciones as p INNER JOIN iniciativa as ini ON p.id = ini.post_id  WHERE p.active=1 AND p.tipo = 2 AND p.user_id = " .$org->id ." ORDER BY p.id DESC LIMIT ".$limit_param1.",".$limit_param2;
        $response = $DaoIniciativa->advancedQuery($query);

        $responsenuevo = array();
        $colaboradoresenuevo = array();

        foreach ($response as $iniciativa) {
            $colaboradores = $DaoColaboracion->getTrueColaboradoresByIdIniciativa($iniciativa['_Id']);
            foreach ($colaboradores as $colaborador) {
                $organizacion = $DaoOrganizacion->getById($colaborador['user_id']);
                // Si el avatar es el de default
                if ($organizacion->avatar === "assets/img/home/perfil.png") {
                    $organizacion->avatar = "../../../../assets/img/home/perfil.png";
                }
                $colaborador['organizacion'] = $organizacion;
                $user = $UserData->getById($organizacion->Usuarios_idUsuarios);
                $colaborador['user'] = $user;
                array_push($colaboradoresenuevo, $colaborador);
            }
            if (count($colaboradoresenuevo)>0) {
                $iniciativa['colaboradores'] = $colaboradoresenuevo;
            } else {
                $iniciativa['colaboradores'] = "";
            }
              $colaboradoresenuevo = [];

            /*
            * Objetivos ligados a la iniciativa
            */
            $iniciativa_id = $iniciativa['_Id'];
            $sql = "SELECT
              o.id, o.descripcion, o_i.*
          FROM
              objetivo_iniciativa o_i
                  JOIN
              objetivo o ON o.id = o_i.objetivo_id
          WHERE
        o_i.iniciativa_id = $iniciativa_id";
            $iniciativas_has_objetivos = $DaoIniciativa -> getAllRows($sql);

            /* Constantes de formula */
            $universo_actores = $descargas = 0;
            if ($universoResultSet = $DaoIniciativa -> getOneRow("SELECT COUNT(*) as universo FROM colaboracion WHERE Iniciativa_idIniciativa = $iniciativa_id") ) {
                $universo_actores = $universoResultSet['universo'];
            }
            if ($descargasResultSet = $DaoIniciativa -> getOneRow("SELECT COUNT(*) as descargas FROM colaboracion WHERE Iniciativa_idIniciativa = $iniciativa_id AND estatus = 1") ) {
                $descargas = $descargasResultSet['descargas'];
            }

            foreach ($iniciativas_has_objetivos as $k => $v) {
                $iniciativas_has_objetivos[$k]['stars'] = '';
                $iniciativas_has_objetivos[$k]['calificacion'] = 0;
                // ---------------------------------------------------------------------
                /*
                * En caso de estar colaborando  entonces puedo calificar el objetivo
                */
                /* Calidad de un objetivo */
                $calidad = 0; //Promedio de calificación
                 $sql="SELECT
                IFNULL(  AVG(oihc.calificacion)  , 0  ) AS calificacion,
                oihc.organizacion_id,
                oihc.objetivo_id,
                oihc.iniciativa_id,
                o.descripcion
            FROM
                objetivo_iniciativa_has_calificacion oihc
                    JOIN
                objetivo o
            ON o.id = oihc.objetivo_id
            WHERE
          o.id =".$v['objetivo_id']." and oihc.iniciativa_id=".$iniciativa['_Id'];
                if ($calidadResultSet = $DaoIniciativa -> getOneRow($sql) ) {
                    $calidad = $calidadResultSet['calificacion'];
                }

     
                $iniciativas_has_objetivos[$k]['calificacion'] = round($calidad);
                /* End formula ---------------------------------------------------------*/
                if (isset($_SESSION['user_id']) AND is_numeric($_SESSION['user_id']) ) {
                    $organizacion_visitante = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
                    // /* Borrame ------------------------ */$organizacion_visitante->id = 6;
                    $sql = "SELECT
                  c.idColaboracion AS colaboracion_id,
                  c.fechaCreado AS created_at,
                  c.estatus,
                  c.evento_id,
                  c.Iniciativa_idIniciativa AS iniciativa_id,
                  c.user_id AS organizacion_id
              FROM
                  colaboracion c
              WHERE
                  c.Iniciativa_idIniciativa = $iniciativa_id
            AND c.estatus = 1 AND c.user_id = $organizacion_visitante->id";
                    $participacion = $DaoIniciativa -> getOneRow($sql);
                    /* Si el resultSet no esta vacio significa que aceptamos
                    * la particiapación, por lo tanto podemos calificar el objetivo
                    */
                    if (!empty($participacion) ) {
                        $iniciativas_has_objetivos[$k]['stars'] = '<div class="rateYo-obejtivos-iniciativa" data-calificacion="0" data-idobjetivoiniciativa="'.$v['objetivo_id'].'" data-iniciativaid="'.$v['iniciativa_id'].'"></div>';
                    } else {
                        /*
                        * No participe en esta inciativa asi que solo puedo ver la
                        * calificacion no puedo calificar.
                        */
                        $iniciativas_has_objetivos[$k]['stars'] = getStars($iniciativas_has_objetivos[$k]['calificacion']);
                    }
                }
                /* Agregar Objetivos a las iniciativas */
                $iniciativa['objetivos'] = $iniciativas_has_objetivos;
            }
            /*
            * Agrega iniciativa al response final
            *---------------------------------------------------------------------*/
            array_push($responsenuevo, $iniciativa);
        }
        if (count($responsenuevo)>0) {
            echo json_encode($responsenuevo);
        } else {
            echo "<h3 class=\"text-center\">".translate('label noIniciativas')."</h3>";
        }
    } else {
        echo "Error";
    }

    break;


case "all":
    if (strlen($_POST['user_id'])>0) {
        $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
        $query = "select p.id as _publicacionId, p.fechaCreado as publicacionCreado, p.tipo as publicacionTipo, p.user_id as publicacionUsuario,
                ini.idIniciativa as _Id, ini.titulo as _Titulo, ini.textoCorto as _descripcion, ini.fechaIniciativa as _fecha, ini.horaIni as _horaInicio,
                ini.horaFin as _horaFin, ini.llaveImg as _thumbnail, ini.lugar as _lugar, ini.post_id as _postId
                from publicaciones as p INNER JOIN iniciativa as ini ON p.id = ini.post_id  WHERE p.active=1 AND p.tipo = 2 AND p.user_id = " .$org->id ." ORDER BY p.id DESC LIMIT 0,5";
        $response = $DaoIniciativa->advancedQuery($query);
        //$responseColaboradores =  array();

        $responsenuevo = array();
        $colaboradoresenuevo = array();

        foreach ($response as $iniciativa) {
            $colaboradores = $DaoColaboracion->getTrueColaboradoresByIdIniciativa($iniciativa['_Id']);
            foreach ($colaboradores as $colaborador) {
                $organizacion = $DaoOrganizacion->getById($colaborador['user_id']);
                $colaborador['organizacion'] = $organizacion;
                array_push($colaboradoresenuevo, $colaborador);
            }
            if (count($colaboradoresenuevo)>0) {
                $iniciativa['colaboradores'] = $colaboradoresenuevo;
            } else {
                $iniciativa['colaboradores'] = "";
            }
              $colaboradoresenuevo = [];
            /*
            * Objetivos ligados a la iniciativa
            */
            $iniciativa_id = $iniciativa['_Id'];
            $sql = "SELECT
              o.id, o.descripcion, o_i.*
          FROM
              objetivo_iniciativa o_i
                  JOIN
              objetivo o ON o.id = o_i.objetivo_id
          WHERE
        o_i.iniciativa_id = $iniciativa_id";
            $iniciativas_has_objetivos = $DaoIniciativa -> getAllRows($sql);

            /* Constantes de formula */
            $universo_actores = $descargas = 0;
            if ($universoResultSet = $DaoIniciativa -> getOneRow("SELECT COUNT(*) as universo FROM colaboracion WHERE Iniciativa_idIniciativa = $iniciativa_id") ) {
                $universo_actores = $universoResultSet['universo'];
            }
            if ($descargasResultSet = $DaoIniciativa -> getOneRow("SELECT COUNT(*) as descargas FROM colaboracion WHERE Iniciativa_idIniciativa = $iniciativa_id AND estatus = 1") ) {
                $descargas = $descargasResultSet['descargas'];
            }

            foreach ($iniciativas_has_objetivos as $k => $v) {
                $iniciativas_has_objetivos[$k]['stars'] = '';
                $iniciativas_has_objetivos[$k]['calificacion'] = 0;
                // ---------------------------------------------------------------------
                /*
                * En caso de estar colaborando  entonces puedo calificar el objetivo
                */
                /* Calidad de un objetivo */
                $calidad = 0; //Promedio de calificación
                $sql="SELECT
                IFNULL(  AVG(oihc.calificacion)  , 0  ) AS calificacion,
                oihc.organizacion_id,
                oihc.objetivo_id,
                oihc.iniciativa_id,
                o.descripcion
            FROM
                objetivo_iniciativa_has_calificacion oihc
                    JOIN
                objetivo o
            ON o.id = oihc.objetivo_id
            WHERE
          o.id =".$v['objetivo_id']." and oihc.iniciativa_id=".$iniciativa['_Id'];
                // $sql = "SELECT
                //       IFNULL(  AVG(oihc.calificacion)  , 0  ) AS calificacion,
                //       oihc.organizacion_id,
                //       oihc.objetivo_id,
                //       o.descripcion
                //   FROM
                //       objetivo_iniciativa_has_calificacion oihc
                //           JOIN
                //       objetivo o ON o.id = oihc.objetivo_id
                //   WHERE
                // o.id = ".$v['objetivo_id'];
                if ($calidadResultSet = $DaoIniciativa -> getOneRow($sql) ) {
                    $calidad = $calidadResultSet['calificacion'];
                }
                /* Formula -------------------------------------------------------------*/
                /* Calcular alcance e interes */
                $numerador = (0.0025 * $descargas) * ( $universo_actores - 1);
                $denominador = 0.25 * ( $universo_actores - $descargas );
                if ($numerador == 0 OR $denominador == 0) {
                    $alcance = $interes = 0;
                } else {
                    $alcance = $interes = sqrt($numerador / $denominador);
                }
                /* Asignar Calificacion */
                // Evalua el nivel de confianza
                $calificacion = 0;
                if ($alcance < 1.28) {
                    $calificacion = 1;
                } elseif ($alcance >= 1.28 AND $alcance < 1.44) {
                    $calificacion = 1.41421356;
                } elseif ($alcance >= 1.44 AND $alcance < 1.65) {
                    $calificacion = 1.73205081;
                } elseif ($alcance >= 1.65 AND $alcance < 1.96) {
                    $calificacion = 2;
                } elseif ($alcance >= 1.96) {
                    $calificacion = 2.23606798;
                }
                /* Calcular nivel de impacto */
                $impacto = round((0.5 * $calidad) * ($alcance * $interes));
                $iniciativas_has_objetivos[$k]['calificacion'] = round($calidad);
                /* End formula ---------------------------------------------------------*/
                if (isset($_SESSION['user_id']) AND is_numeric($_SESSION['user_id']) ) {
                    $organizacion_visitante = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
                    // /* Borrame ------------------------ */$organizacion_visitante->id = 6;
                    $sql = "SELECT
                  c.idColaboracion AS colaboracion_id,
                  c.fechaCreado AS created_at,
                  c.estatus,
                  c.evento_id,
                  c.Iniciativa_idIniciativa AS iniciativa_id,
                  c.user_id AS organizacion_id
              FROM
                  colaboracion c
              WHERE
                  c.Iniciativa_idIniciativa = $iniciativa_id
            AND c.estatus = 1 AND c.user_id = $organizacion_visitante->id";
                    $participacion = $DaoIniciativa -> getOneRow($sql);
                    /* Si el resultSet no esta vacio significa que aceptamos
                    * la particiapación, por lo tanto podemos calificar el objetivo
                    */
                    if (!empty($participacion) ) {
                        $iniciativas_has_objetivos[$k]['stars'] = '<div class="rateYo-obejtivos-iniciativa" data-calificacion="0" data-idobjetivoiniciativa="'.$v['objetivo_id'].'"></div>';
                    } else {
                        /*
                        * No participe en esta inciativa asi que solo puedo ver la
                        * calificacion no puedo calificar.
                        */
                        $iniciativas_has_objetivos[$k]['stars'] = getStars($iniciativas_has_objetivos[$k]['calificacion']);
                    }
                }
                /* Agregar Objetivos a las iniciativas */
                $iniciativa['objetivos'] = $iniciativas_has_objetivos;
            }
            /*
            * Agrega iniciativa al response final
            *---------------------------------------------------------------------*/
            array_push($responsenuevo, $iniciativa);
        }
        if (count($responsenuevo)>0) {
            echo json_encode($responsenuevo);
        } else {
            echo "<h3 class=\"text-center\">".translate('label noIniciativas')."</h3>";
        }
    } else {
        echo "Error";
    }

    break;

case "allByUserIdAndInCollaborationsForTagging":

    if (strlen($_SESSION['user_id'])>0) {
        $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
        // Todas las iniciativas creadas por usuario
        $iniciativas = $DaoIniciativa->getByUserId($org->id);
        // Todas las colaboraciones del usuario
        $colaboraciones = $DaoColaboracion->getByUserId($org->id);
        // Barrido de colaboraciones por su id iniciativa
        foreach ($colaboraciones as $colaboracion) {
            $iniciativa = $DaoIniciativa->getById($colaboracion->Iniciativa_idIniciativa);
            array_push($iniciativas, $iniciativa);
        }
        $iniciativaForTagging = array();
        foreach ($iniciativas as $iniciativa) {
            array_push($iniciativaForTagging, $iniciativa->titulo . "-" . $iniciativa->id);
        }
        echo json_encode($iniciativaForTagging);
    } else {
        echo "Error";
    }

    break;

case "delete":

    $iniciativa_id = $DaoIniciativa->delete($_POST['lead_id'], $_POST['post_id']);
    if ($iniciativa_id > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => "Tu iniciativa ha sido eliminada exitosamente!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => "Ah ocurrido un error intentando eliminar tu iniciativa, intenta más tarde." )));
    }

    break;

case "edit":

    $Iniciativa->setTitulo($_POST['title']);
    $Iniciativa->setTextoCorto($_POST['description']);
    $Iniciativa->setHoraIni($_POST['initialHour']);
    $Iniciativa->setHoraFin($_POST['endHour']);
    $Iniciativa->setLugar($_POST['place']);
    $Iniciativa->setLlaveImg($_POST['llaveImg']);
    $Iniciativa->setFechaIniciativa($_POST['initialDate']);
    $Iniciativa->setFechaFin($_POST['endDate']);
    $Iniciativa->setPost_idPost($_POST['post_id']);
    $Iniciativa->setId($_POST['lead_id']);
    $Iniciativa->setIdAlcanze($_POST['idAlcanze']);
    $iniciativa_id = $DaoIniciativa->update($Iniciativa);

    /*
    * EVENTOS
    * @description: Se agregan eventos relacionados
    * ================================
    */
    if (!empty($_POST['events'])) {
        $arr_evento = $_POST['events'];
        foreach ($arr_evento as $evento_id) {
            $DaoEvento->addIniciativaEvento($iniciativa_id, $evento_id);
        }
    }

    /*
    * COLABORACIONES
    * @description: Envío de notificaciones a colaboradores y creación de colaboraciones
    * ================================
    */
    if (!empty($_POST['actors'])) {
        foreach ($_POST['actors'] as $k => $actor_id) {

            // Colaboración
            $amigo_id = $actor_id;
            $Colaboracion->setFechaCreado(date('Y-m-d'));
            $Colaboracion->setEstatus(0);
            $Colaboracion->setIniciativa_idIniciativa($iniciativa_id);
            $Colaboracion->setEventoId(0);
            $org = $DaoOrganizacion->getByUserId($amigo_id);
            $Colaboracion->setUsuario_id($org->id);
            $DaoColaboracion->add($Colaboracion);

            // Crear notificación al colaborador
            $notification = new NotificationData();
            $notification->not_type_id = 5; // colaboracion
            $notification->type_id = 5; // nueva iniciativa
            $notification->ref_id = $iniciativa_id; // =  id de iniciativa
            $notification->receptor_id = $amigo_id; // a quien va dirigida la notificacion
            $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
            $notification->id = $notification->add();

            $notification->message = $notification->getMessageById($notification->id);
            $notification->updateMessage();
        }
    }

    if ($iniciativa_id > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => "Tu iniciativa ha sido actualizada exitosamente!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => "Ah ocurrido un error intentando actualizar tu iniciativa, intenta más tarde." )));
    }

    break;

case "save_new":


    $Publicaciones->setFechaCreado(date('Y-m-d H:i:s'));
    $Publicaciones->setTipo($_POST['tipo']);

    $Publicaciones->setIsPublic(0);
    $Publicaciones->setByExpert(0);
    $Publicaciones->setByAdmin(0);

    $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    $Publicaciones->setUser_id($org->id);
    $id = $DaoPublicaciones->add($Publicaciones);
    // Si se crea la publicación
    if ($id > 0) {
        $Iniciativa->setTitulo($_POST['title']);
        $Iniciativa->setTextoCorto($_POST['description']);
        $Iniciativa->setHoraIni($_POST['initialHour']);
        $Iniciativa->setHoraFin($_POST['endHour']);
        $Iniciativa->setLugar($_POST['place']);
        $Iniciativa->setLlaveImg($_POST['llaveImg']);
        $Iniciativa->setFechaIniciativa($_POST['initialDate']);
        $Iniciativa->setFechaFin($_POST['endDate']);
        $Iniciativa->setPost_idPost($id);
        $iniciativa_id = $DaoIniciativa->add($Iniciativa);

        // Si se crea la iniciativa
        if ($iniciativa_id > 0 ) {


            /*
            * UBICACIONES
            * @description: Se agregan ubicaciones relacionados
            * ================================
            */
            if (!empty($_POST['ubicaciones']) &&  count($_POST["ubicaciones"]) > 0) {

                $DaoUbicacionesIniciativas = new DaoUbicacionesIniciativas();
                $UbicacionesIniciativas = new UbicacionesIniciativas();

                $arr_ubicaciones = $_POST['ubicaciones'];
                foreach ($arr_ubicaciones as $ubicacion) {
                    $UbicacionesIniciativas->setIdIniciativa($iniciativa_id);
                    if (!empty($ubicacion['pais_id'])) {       $UbicacionesIniciativas->setIdPais($ubicacion['pais_id']);
                    }
                    if (!empty($ubicacion['estado_id'])) {     $UbicacionesIniciativas->setIdEstado($ubicacion['estado_id']);
                    }
                    if (!empty($ubicacion['municipio_id'])) {  $UbicacionesIniciativas->setIdMunicipio($ubicacion['municipio_id']);
                    }
                    if (!empty($ubicacion['localidad_id'])) {  $UbicacionesIniciativas->setIdLocalidad($ubicacion['localidad_id']);
                    }
                    if (!empty($ubicacion['influencia'])) {    $UbicacionesIniciativas->setIsMain($ubicacion['influencia']);
                    }

                    $idUbicacion = $DaoUbicacionesIniciativas->add($UbicacionesIniciativas);

                }
            }

            /*
            * EVENTOS
            * @description: Se agregan eventos relacionados
            * ================================
            */
            if (!empty($_POST['events'])) {
                $arr_evento = $_POST['events'];
                foreach ($arr_evento as $evento_id) {
                    $DaoEvento->addIniciativaEvento($iniciativa_id, $evento_id);
                }
            }
            /*
            * OBJETIVOS
            * @description: Se agregan objetivos relacionados
            * ================================
            */
            if (!empty($_POST['objetivos'])) {
                $arr_objetivo = $_POST['objetivos'];
                foreach ($arr_objetivo as $objetivo_id) {
                    $DaoObjetivo->addObjetivoIniciativa($objetivo_id, $iniciativa_id);
                }
            }
            /*
            * NOTIFICACIONES
            * @description: Envío de notificaciones a amigos
            * ================================
            */
            $friends = $FriendData->getFriends($_SESSION["user_id"]);
            foreach ($friends as $friend) {
                $friend_id = ($friend->receptor_id !== $_SESSION["user_id"]) ? $friend->receptor_id : $friend->sender_id;

                // Crear notificación
                $notification = new NotificationData();
                $notification->not_type_id = 8; // creó
                $notification->type_id = 5; // iniciativa
                $notification->ref_id = $iniciativa_id; // =  id de iniciativa
                $notification->receptor_id = $friend_id; // a quien va dirigida la notificacion
                $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                $notification->id = $notification->add();

                $notification->message = $notification->getMessageById($notification->id);
                $notification->updateMessage();
            }

            /*
            * COLABORACIONES
            * @description: Envío de notificaciones a colaboradores y creación de colaboraciones
            * ================================
            */
            if (!empty($_POST['actors'])) {
                foreach ($_POST['actors'] as $k => $actor_id) {

                    // Colaboración
                    $amigo_id = $actor_id;
                    $Colaboracion->setFechaCreado(date('Y-m-d'));
                    $Colaboracion->setEstatus(0);
                    $Colaboracion->setIniciativa_idIniciativa($iniciativa_id);
                    $Colaboracion->setEventoId(0);
                    $org = $DaoOrganizacion->getByUserId($amigo_id);
                    $Colaboracion->setUsuario_id($org->id);
                    $DaoColaboracion->add($Colaboracion);

                    // Crear notificación al colaborador
                    $notification = new NotificationData();
                    $notification->not_type_id = 5; // colaboracion
                    $notification->type_id = 5; // nueva iniciativa
                    $notification->ref_id = $iniciativa_id; // =  id de iniciativa
                    $notification->receptor_id = $amigo_id; // a quien va dirigida la notificacion
                    $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                    $notification->id = $notification->add();

                    $notification->message = $notification->getMessageById($notification->id);
                    $notification->updateMessage();
                }
            }
        }
    }
    if ($iniciativa_id > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => translate('label guardarIniciativa')."!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' =>  translate('label errorGuardarIniciativa') )));
    }

    break;

case "byUserIdAndInCollaboration":
    if (strlen($_SESSION['user_id'])>0) {
        $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
        $iniciativas = $DaoIniciativa->getByUserIdAndInCollaboration($org->id);
        echo json_encode($iniciativas);
    } else {
        echo "Error";
    }
    break;

case "byId":

    $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
    $query = "SELECT *
        FROM publicaciones as p INNER JOIN ".$tablename." as t ON p.id = t.post_id  WHERE p.tipo = 2 AND p.user_id = ". $org->id ." and  t.idIniciativa=" . $_POST['lead_id'];
    $response = $DaoIniciativa->advancedQuery($query);
    /* fix rapido */
    foreach ($response as $k => $v) {
        $response[$k]['user_id'] = $_POST['user_id'];
    }
    echo json_encode($response);

    break;

case "getByIniciativaId":
    if (strlen($_POST['iniciativa_id'])>0) {
        $iniciativa = $DaoIniciativa->getById($_POST['iniciativa_id']);
        echo json_encode($iniciativa);
    } else {
        echo "Error";
    }
    break;

case "iniciativaEventos":
    if (strlen($_POST['iniciativa_id'])>0) {
        $iniciativas = $DaoIniciativa->getIniciativaEventos($_POST['iniciativa_id']);

        $arr_evento = array();
        foreach ($iniciativas as $iniciativa) {
            $evento = $DaoEvento->getById($iniciativa['idEvento']);
            array_push($arr_evento, array('id' => $evento->id, 'titulo' => $evento->titulo));
        }

        echo json_encode($arr_evento);
    } else {
        echo "Error";
    }
    break;

case "update_rating":
    $toSend = $_POST;
    $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    $toSend['organizacion_id'] = $org -> id;
    $id = $DaoIniciativa -> updateRating($toSend);
    if ($id > 0 ) {
        json(
            array(
            'status'      => true,
            'msg'         => translate('label guardarCal'),
            'class'       => 'success'
            )
        );
    } else {
        json(
            array(
            'status'  => false,
            'msg'     => translate('label errorcalificarIniciativa'),
            'class'   => 'warning'
            )
        );
    }
    break;
  
case "allByLimits":

    $limit_param1 = 0;
    $limit_param2 = 10;
    if (!empty($_POST['limit_param1'])) { $limit_param1 = $_POST['limit_param1'];
    }
    if (!empty($_POST['limit_param2'])) { $limit_param2 = $_POST['limit_param2'];
    }

    if (strlen($_POST['user_id'])>0) {
        $iniciativas = $DaoIniciativa->getByUserIdWithLimits($_POST['user_id'], $limit_param1, $limit_param2);
        echo json_encode($iniciativas);
    }

    break;

}

function getStars($calificacion)
{
    $stars = '';
    for ($i = 1; $i <= 5 ; $i++) {
        if ($calificacion == 0) {
            $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
        } elseif ($calificacion < $i) {
            $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
        } else {
            $stars .= '<i style="color: orange;" class="fa fa-star" aria-hidden="true"></i>';
        }
    }
    return $stars;
}
