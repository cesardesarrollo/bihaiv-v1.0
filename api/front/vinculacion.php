<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/FriendData.php';
require_once '../../core/modules/index/model/UserData.php';
require_once '../../core/modules/index/model/NotificationData.php';
require_once '../../core/lang/lang.php';

$method = $_POST['method'];

switch($method){

case "all":

    break;

case "delete":
    break;

case "edit":
    break;

case "save":

    $accion = $_POST['accion'];
    if ($accion=='vincularme') {

        if (isset($_SESSION["user_id"])) {
            $fs = FriendData::getFriendship($_SESSION["user_id"], $_POST["receptor_id"]);
            if (!is_object($fs)) {
                $fs = new FriendData();
                $fs->sender_id = $_SESSION["user_id"];
                $fs->receptor_id = $_POST["receptor_id"];
                $resp = $fs->add();
                if ($resp[0]) {
                    print_r(json_encode(array('status' => true, 'msg' => translate('label procesoE') )));
                } else {
                    print_r(json_encode(array('status' => false, 'msg' =>  translate('label errorGenerico') )));
                }
            } else {
                print_r(json_encode(array('status' => false, 'msg' =>  translate('label errorGenerico') )));
            }
        }

    } elseif ('vincularcon') {

        $vincularA = ($_POST['vincularA']);
        if ($_POST['vincularCon'] != null && count($_POST['vincularCon']) > 0) {
            $array_push = array();
            $UserData = new UserData();
            $userA = $UserData->getById($vincularA);

            $vincularCon = ($_POST['vincularCon']);
            for ($i=0; $i <= count($vincularCon) - 1 ; $i++) {
            
                $userCon = $UserData->getById($vincularCon[$i]);
                $fs = FriendData::getFriendship($vincularA, $vincularCon[$i]);
                if (!is_object($fs)) {


                    // Si no existe una solicitud de amistad se envia a vincularA un correo 
                    // y notificacion sugiriendo accesar al perfil y agregarlo como conexion a VincularCon
              
                    /*
                    * NOTIFICACIONES
                    * @description: Envío de notificaciones a actores de rol especificado
                    * ================================
                    */
                    $notification = new NotificationData();
                    $notification->not_type_id = 11; // invitó
                    $notification->type_id = 10; // amistad
                    $notification->ref_id = $vincularCon[$i]; // a quien va dirigida la sugerencia
                    $notification->receptor_id = $vincularA; // =  id de amistad sugiriendo
                    $notification->sender_id = $_SESSION["user_id"]; // usuario que sugiere
                    $notification->id = $notification->add();

                    $notification->message = $notification->getMessageById($notification->id);
                    $notification->updateMessage();

                    array_push($array_push, array('status' => true, 'msg' => "¡Proceso exitoso!" ));


                } else {
                    array_push($array_push, array('status' => true, 'msg' => "Los actores $userA->name y $userCon->name ya son amigos o existe ya una solicitud de conexión" ));
                }          
            }
            print_r(json_encode($array_push));
        } else {
            print_r(json_encode(array('status' => false, 'msg' => "Ah ocurrido un error, intenta más tarde." )));
        }
    }

    break;


case "byId":

    break;

}
