<?php

session_start();
// Load all the basic shit
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/modules/index/model/DaoEnfoque.php';
require_once '../../core/modules/index/model/DaoColaboracion.php';
require_once '../../core/modules/index/model/DTO/Enfoque.php';
require_once '../../core/modules/index/model/DTO/Colaboracion.php';
require_once '../../core/modules/index/model/DaoIniciativa.php';
require_once '../../core/modules/index/model/DTO/Iniciativa.php';
require_once '../../core/modules/index/model/DaoEvento.php';
require_once '../../core/modules/index/model/DTO/Evento.php';
/**
* 
 * Include PHPExcel 
*/
require_once '../../core/php_excel/Classes/PHPExcel.php';
/**
* 
 * Include Html2pdf 
*/
require_once '../../core/pdflibrary/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;


$DaoEnfoque= new DaoEnfoque();
$Enfoque = new Enfoque();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = $_POST['method'];
    switch($action){
    case "charts":
        $response = array(
        'status'  => true,
        'msg'     => '',
        'class'   => '',
        'data'    => array(
          'impacto-ecosistema'  => array( 'stars' => '', 'score' => 0),
          'incidencias'         => array( 'stars' => '', 'score' => 0),
          'enfoques'            => array( 'stars' => '', 'score' => 0),
          'actividad'           => array( 'stars' => '', 'score' => 0),
          'aportaciones'        => array( 'stars' => '', 'score' => 0),
          'colaboraciones'      => array( 'stars' => '', 'score' => 0)
        )
        );
        if (!isset($_POST['organizacion_id']) OR !is_numeric($_POST['organizacion_id']) ) {
            /* Error, no se envio el id de organización */
            $response['status'] = false;
            $response['msg']    = 'No se envio id de la organización';
            $response['class']  = 'warning';
        } else {
            $organizacion_id = $_POST['organizacion_id'];
            /* GET INCIDENCIA DATA -------------------------------------------------*/
            $query = "SELECT
            IFNULL((AVG(calificacion)), 0) AS calificacion,
            organizacion_calificada_id
        FROM
            organizacion_has_calificacion
        WHERE
            organizacion_calificada_id = $organizacion_id";
            $resultSet = $DaoEnfoque -> getOneRow($query);
            $promedio = round($resultSet['calificacion']);
            $promedio = ($promedio < 1) ? 1 : $promedio;
            $response['data']['incidencias']['score'] = $promedio;
            $response['data']['incidencias']['stars'] = getStarsTags($promedio);
            /* -------------------------------------------------------------------*/
            /* GET ENFOQUES DATA -------------------------------------------------*/
             $query = "SELECT
              p.id AS _idPublicacion,
              e.idEnfoque AS _idEnfoque,
              IFNULL((AVG(ce.calificacion)), 0) AS _calificacion,
              e.post_id AS _idPost
          FROM
              publicaciones AS p
                  INNER JOIN
              enfoque AS e ON p.id = e.post_id
                  LEFT JOIN
              calificacionenfoque ce ON ce.enfoque_idEnfoque = e.idEnfoque
          WHERE
              p.tipo = 1
                  AND p.user_id = $organizacion_id
          GROUP BY _idPublicacion
          ORDER BY p.id DESC
        LIMIT 0 , 5";
            $resultSet = $DaoEnfoque -> getAllRows($query);
            $total = 0;
            foreach ($resultSet as $k => $v) {
                $total += $v['_calificacion'];
            }
            if (count($resultSet) == 0) {
                $promedio = 0;
            } else {
                $promedio = round($total / count($resultSet));
            }
            $promedio = ($promedio < 1) ? 1 : $promedio;
            $response['data']['enfoques']['score'] = $promedio;
            $response['data']['enfoques']['stars'] = getStarsTags($promedio);

            /* GET APORTACIONES DATA */
            $query = "SELECT
              p.id AS _idPublicacion,
              p.fechaCreado AS _fechaCreacion,
              p.tipo AS _tipo,
              p.user_id AS _usuario,
              ap.idAportacion AS _idAportacion,
              ap.titulo AS _titulo,
              ap.descripcion AS _descripcion,
              ap.thumbnail AS _thumbnail,
              ap.post_id AS _postId,
              IFNULL(ROUND(AVG(ca.calificacion)), 0) AS _calificacion,
              ap.visitas as _descargas
          FROM
              publicaciones AS p
                  INNER JOIN
              aportacion AS ap ON p.id = ap.post_id
                  LEFT JOIN
              calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
          WHERE
              p.active = 1 AND p.tipo = 3
                  AND p.user_id = $organizacion_id
          GROUP BY _idPublicacion
          ORDER BY p.fechaCreado DESC
        LIMIT 0 , 5";
            $resultSet = $DaoEnfoque -> getAllRows($query);
            /* Constantes de formula */
            $universo_actores = 0;
            if ($universoResultSet = $DaoEnfoque -> getOneRow('SELECT COUNT(*) as universo FROM user WHERE is_active = 1') ) {
                $universo_actores = $universoResultSet['universo'];
            }
            $descargas = 0;
            foreach ($resultSet as $k => $v) {
                $descargas = $v['_descargas'];
                $calidad = $v['_calificacion'];
                /* Formula -------------------------------------------------------------*/
                /* Calcular alcance e interes */
                $numerador = (0.0025 * $descargas) * ( $universo_actores - 1);
                $denominador = 0.25 * ( $universo_actores - $descargas );
                $alcance = $interes = sqrt($numerador / $denominador);
                /* Asignar Calificacion */
                // Evalua el nivel de confianza
                $calificacion = 0;
                if ($alcance < 1.28) {
                    $calificacion = 1;
                } elseif ($alcance >= 1.28 AND $alcance < 1.44) {
                    $calificacion = 1.41421356;
                } elseif ($alcance >= 1.44 AND $alcance < 1.65) {
                    $calificacion = 1.73205081;
                } elseif ($alcance >= 1.65 AND $alcance < 1.96) {
                    $calificacion = 2;
                } elseif ($alcance >= 1.96) {
                    $calificacion = 2.23606798;
                }
                /* Calcular nivel de impacto */
                $impacto =  (0.5 * $calidad) * ($alcance * $interes) ;
                $resultSet[$k]['_calificacion'] = $impacto;
                $total = 0;
                foreach ($resultSet as $k => $v) {
                    $total += $v['_calificacion'];
                }
                $promedio = round($total / count($resultSet));
                $promedio = ($promedio < 1) ? 1 : $promedio;
                $response['data']['aportaciones']['score'] = $promedio;
                $response['data']['aportaciones']['stars'] = getStarsTags($promedio);
            }
        
            /* -------------------------------------------------------------------*/
            /* ACTIVIADA  (eventos e iniciativas) */
            /* GET INICIATIVAS DATA */
            $promedioactividad=0;
             $query = " select t.organizacion_id, round(IFNULL(avg(t.calificacion), 0)) as promedio from (select * from ";
            $query.=" objetivo_evento_has_calificacion as c union ALL    select * from objetivo_iniciativa_has_calificacion) as t  where ";
            $query.=" organizacion_id=".$organizacion_id." group by t.organizacion_id ";
            $resultSet = $DaoEnfoque -> getAllRows($query);
            $promedioactividad=0;
            foreach ($resultSet as $key => $valor) {
                $promedioactividad=$valor['promedio'];
            }  
            // $rs = $DaoEnfoque ->getOneRow($sql);
        
            /* if (!$rs)
            {
            $promedioactividad=$rs['promedio'];
            }*/
            // echo $resultSet;
            /* Constantes de formula */
            /**/
            $response['data']['actividad']['score'] = $promedioactividad;
            $response['data']['actividad']['stars'] = getStarsTags($promedioactividad);
        
            /* -------------------------------------------------------------------*/
            /* GET COLABORACIONES DATA */

             /* Variables de formula */
             $daocolaboracion=new DaoColaboracion();
             $daoEvento=new  DaoEvento();
             $daoIniciativa=new DaoIniciativa();
             $sql="select sum(cnt) as descargas from
             (select count(*) as cnt from (select e.idevento, count(c.idcolaboracion) as cantidad from evento as e left join colaboracion as c on e.idEvento=c.evento_id  where e.user_id=".$organizacion_id." group by idevento) as t where t.cantidad>0 union all
          select count(*) as cnt from colaboracion where user_id=".$organizacion_id." )  as t";
             $resu=$DaoEnfoque -> getAllRows($sql);
             $descargas=0; $calificaron=0;
            foreach ($resu as  $valor) {
                $descargas=$valor["descargas"];
            } 
              $sql="select sum(cnt) as calificaron from
          (select count(*) as cnt from (select e.idevento, count(c.idcolaboracion) as cantidad from evento as e left join
           colaboracion as c on e.idEvento=c.evento_id  where e.user_id=".$organizacion_id."  and c.estatus=1  group by idevento) as t where t.cantidad>0 union all
          select count(*) as cnt from colaboracion where user_id=".$organizacion_id."  and estatus=1 )  as t";
             $resul=$DaoEnfoque -> getAllRows($sql);
            foreach ($resul as  $valor) {
                 $calificaron=$valor["calificaron"];
            } 
            // $daoColaboracion->getgetEventosByIdUsu
            $UNIVERSO     = 0;
            $DESCARGAS    =$descargas;
            $calidad      = 5;

            $descargas    = $DESCARGAS;
            if ($universoResultSet = $DaoEnfoque->getOneRow('SELECT COUNT(*) as universo FROM user WHERE is_valid = 1 AND is_active = 1 AND activo = 1 AND tipoRel = 1') ) {
                $UNIVERSO = $universoResultSet['universo'];
            }

            /* Calcular alcance */
            $numerador   = (0.0025 * $DESCARGAS) * ( $UNIVERSO - 1 );
            $denominador = 0.25 * ( $UNIVERSO - $DESCARGAS );

            if ($DESCARGAS == $UNIVERSO) {
                $CONFIANZA = 1.96;
            } else {
                if ($denominador == 0) {
                    $CONFIANZA = 1.96;
                } else {
                    $CONFIANZA = sqrt($numerador / $denominador);
                }
            }

            $alcance = 0;
            if ($CONFIANZA < 1.28) {
                $alcance = 1;
            } elseif ($CONFIANZA >= 1.28 AND $CONFIANZA < 1.44) {
                $alcance = 1.41421356;
            } elseif ($CONFIANZA >= 1.44 AND $CONFIANZA < 1.65) {
                $alcance = 1.73205081;
            } elseif ($CONFIANZA >= 1.65 AND $CONFIANZA < 1.96) {
                $alcance = 2;
            } elseif ($CONFIANZA >= 1.96) {
                $alcance = 2.23606798;
            }

            /*
            * Asignar Calificacion a interes y Evalua el nivel de confianza
            */

            /* Calcular interes */
            $numerador = (0.0025 * $calificaron) * ( $descargas - 1 );
            $denominador = 0.25 * ( $descargas - $calificaron );

            if ($calificaron == $descargas) {
                $confianza = 1.96;
            } else {
                if ($denominador == 0) {
                    $confianza = 1.96;
                } else {
                    $confianza = sqrt($numerador / $denominador);
                }
            }
            $promedio=0;
            $interes = 0;
            if ($confianza < 1.28) {
                $interes = 1;
                $promedio=1;
            } elseif ($confianza >= 1.28 AND $confianza < 1.44) {
                $interes = 1.41421356;
                $promedio=2;
            } elseif ($confianza >= 1.44 AND $confianza < 1.65) {
                $interes = 1.73205081;
                $promedio=3;
            } elseif ($confianza >= 1.65 AND $confianza < 1.96) {
                $interes = 2;
                $promedio=4;
            } elseif ($confianza >= 1.96) {
                $interes = 2.23606798;
                $promedio=5;
            }
            if (($descargas==0) && ($calificaron==0)) {
                $promedio=0;
            }
            //   echo "console.log(".$descargas.":". $calificaron.");";
            /* Calcular nivel de impacto */
            /*$impacto = round( (0.5 * $calidad) + (0.5  $alcance * $interes) );
            $impacto = ( $impacto < 1 ) ? 1 : $impacto;*/

             $response['data']['colaboraciones']['score'] = $promedio;
             $response['data']['colaboraciones']['stars'] = getStarsTags($promedio);
            /*---------------------------------------*/
    
            /*
            * Promedio final para impacto ecosistema
            */
            $total = 0;
            foreach ($response['data'] as $k => $v) {
                $total += $v['score'];
            }
            $promedioactividad = round($total / (count($response['data']) - 1));
            $promedioactividad = ($promedioactividad < 1) ? 1 : $promedioactividad;
            $response['data']['impacto-ecosistema']['score'] = $promedioactividad;
            $response['data']['impacto-ecosistema']['stars'] = getStarsTags($promedioactividad);
        }
        json($response);
        break;
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $action = $_GET['method'];
    switch($action){
    case "reporte_excel_enfoques":
        if (!isset($_GET['organizacion_id']) OR empty($_GET['organizacion_id']) ) {
            $response['status'] = false;
            $response['class']  = 'warning';
            $response['msg']    = 'missing or invalid argument "organizacion_id"';
            json($response);
        }
        $organizacion_id = $_GET['organizacion_id'];
        $query = "SELECT
        e.texto AS enfoque,
        p.fechaCreado AS created_at,
        ce.calificacion calificacion,
        u.name as user_name
        FROM
            publicaciones AS p
                INNER JOIN
            enfoque AS e ON p.id = e.post_id
                LEFT JOIN
            calificacionenfoque ce ON ce.enfoque_idEnfoque = e.idEnfoque
            LEFT JOIN
          organizacion o ON o.id = ce.organizacion_id
            LEFT JOIN
          user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            p.tipo = 1 AND p.user_id = $organizacion_id
      ORDER BY created_at DESC";
        $enfoquesResultSet = $DaoEnfoque -> advancedQuery($query);

        $data_array = array();
        foreach ($enfoquesResultSet as $key => $value) {
            $data_array[] = array_values($value);
        }

        $excel_data = array(
        'title'           => 'Reporte de Enfoques',
        'columns_headers' => array('Enfoque', 'Fecha de creación', 'Calificación', 'Usuario'),
        'data'            => $data_array
        );
        report_excel($excel_data);
        break;

    case 'reporte_pdf_enfoques':
        if (!isset($_GET['organizacion_id']) OR empty($_GET['organizacion_id']) ) {
            $response['status'] = false;
            $response['class']  = 'warning';
            $response['msg']    = 'missing or invalid argument "organizacion_id"';
            json($response);
        }
        $organizacion_id = $_GET['organizacion_id'];
        $query = "SELECT
        e.texto AS enfoque,
        p.fechaCreado AS created_at,
        ce.calificacion calificacion,
        u.name as user_name
        FROM
            publicaciones AS p
                INNER JOIN
            enfoque AS e ON p.id = e.post_id
                LEFT JOIN
            calificacionenfoque ce ON ce.enfoque_idEnfoque = e.idEnfoque
            LEFT JOIN
          organizacion o ON o.id = ce.organizacion_id
            LEFT JOIN
          user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            p.tipo = 1 AND p.user_id = $organizacion_id
      ORDER BY created_at DESC";
        $enfoquesResultSet = $DaoEnfoque -> advancedQuery($query);
        pdf_enfoques($enfoquesResultSet);
        break;

    case "reporte_excel_aportaciones":
        if (!isset($_GET['organizacion_id']) OR empty($_GET['organizacion_id']) ) {
            $response['status'] = false;
            $response['class']  = 'warning';
            $response['msg']    = 'missing or invalid argument "organizacion_id"';
            json($response);
        }
        $organizacion_id = $_GET['organizacion_id'];
        $query = "SELECT
            ap.titulo AS _titulo,
            ap.descripcion AS _descripcion,
            p.fechaCreado AS created_at,
            ca.calificacion AS calificacion,
            u.name AS user_name
        FROM
            publicaciones AS p
                INNER JOIN
            aportacion AS ap ON p.id = ap.post_id
                LEFT JOIN
            calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
            LEFT JOIN
            organizacion o ON o.id = ca.organizacion_id
                LEFT JOIN
            user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            p.active = 1 AND p.tipo = 3
                AND p.user_id = $organizacion_id AND calificacion > 0
      ORDER BY created_at DESC";
        $aportacionesResultSet = $DaoEnfoque -> advancedQuery($query);

        $data_array = array();
        foreach ($aportacionesResultSet as $key => $value) {
            $data_array[] = array_values($value);
        }
        $excel_data = array(
        'title'           => 'Reporte de Enfoques',
        'columns_headers' => array('Aportación','Descripción', 'Fecha de creación', 'Calificación', 'Usuario'),
        'data'            => $data_array
        );
        report_excel($excel_data);
        break;

    case 'reporte_pdf_aportaciones':
        if (!isset($_GET['organizacion_id']) OR empty($_GET['organizacion_id']) ) {
            $response['status'] = false;
            $response['class']  = 'warning';
            $response['msg']    = 'missing or invalid argument "organizacion_id"';
            json($response);
        }
        $organizacion_id = $_GET['organizacion_id'];
        $query = "SELECT
            ap.titulo AS _titulo,
            ap.descripcion AS _descripcion,
            p.fechaCreado AS created_at,
            ca.calificacion AS calificacion,
            u.name AS user_name
        FROM
            publicaciones AS p
                INNER JOIN
            aportacion AS ap ON p.id = ap.post_id
                LEFT JOIN
            calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
            LEFT JOIN
            organizacion o ON o.id = ca.organizacion_id
                LEFT JOIN
            user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            p.active = 1 AND p.tipo = 3
                AND p.user_id = $organizacion_id AND calificacion > 0
      ORDER BY created_at DESC";
        $aportacionesResultSet = $DaoEnfoque -> advancedQuery($query);
        pdf_aportaciones($aportacionesResultSet);
        break;

    default:
        $response['status'] = false;
        $response['class']  = 'warning';
        $response['msg']    = 'El tipo de reporte no es valido';
        json($response);
        break;
    }
}

/*GET STARS HTML*/
function getStarsTags($number = 0)
{
    $stars = '';
    for ($i = 1; $i <= 5 ; $i++) {
        if ($number == 0) {
            // $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
        } elseif ($number < $i) {
            // $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
        } else {
            $stars .= '<i style="color: yellow;" class="fa fa-star" aria-hidden="true"></i>';
        }
    }
    return $stars;
}
/* EXCEL
* @Parametros (Todos son obligatorios)
  $excel_data = array(
    'title'           => 'Reporte de Enfoques',
    'columns_headers' => array('Enfoque', 'Fecha de creación', 'Calificación', 'Usuario'),
    'data'            => array(
      '0' => array(
        'Isaí',
        'González',
        '23',
        'job_722@hotmail.com'
      )
    )
  );
*/
function report_excel($excel_data)
{
    error_reporting(E_ALL);
    ini_set('display_errors', true);
    ini_set('display_startup_errors', true);
    date_default_timezone_set('America/Mexico_City');
    if (PHP_SAPI == 'cli') {
        die('Este reporte solo puede ser ejecutado desde un navegador web');
    }
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();
    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Bihaiv")
        ->setLastModifiedBy("Bihaiv")
        ->setTitle("Office 2007 XLSX Report Document")
        ->setSubject("Office 2007 XLSX Report Document")
        ->setDescription("Reporte generado en Bihavi.")
        ->setKeywords("Bihaiv Reporte")
        ->setCategory("Reporte");
    // Add some data
    // Header
    $objPHPExcel->getActiveSheet()->mergeCells('A1:'.chr(64 + count($excel_data['columns_headers'])).'1');
    $objPHPExcel->setActiveSheetIndex(0) -> setCellValue('A1', $excel_data['title']);
    // APLY CELL COLOR
    cellColor('A1', 'F37064', 'FFFFFF', $objPHPExcel);
    // HEADER COLUMNS
    foreach ($excel_data['columns_headers'] as $k => $v) {
        $objPHPExcel -> setActiveSheetIndex(0) -> setCellValue(chr(65 + $k).(2), $v);
    }
    // FILL WHIT DATA
    foreach ($excel_data['data'] as $k => $v) {
        foreach ($v as $key => $value) {
            $objPHPExcel -> setActiveSheetIndex(0) -> setCellValue(chr(65 + $key).($k+3), $value);
        }
    }
    // Redimenciona las columnas para evitar caracteres ocultos
    foreach (range('A', chr(64 + count($excel_data['columns_headers']))) as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) -> setAutoSize(true);
    }
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle($excel_data['title']);
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$excel_data['title'].'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
}
/*
* @ Params
* cells             'A1' string
* background color  'FFFFFF' (hex string)
* font color        'FFFFFF' (hex string)
* phpObject         object
*/
function cellColor($cells,$color,$font_color, $objPHPExcel)
{
    $objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray(
        array(
        'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array(
          'rgb' => $color
        )
        )
        )
    );
    // Font array
    $styleArray = array(
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'font'  => array(
      'color' => array('rgb' => $font_color),
    )
    );
    $objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($styleArray);
}
/* PDF
*/
function pdf_enfoques($data)
{
    try {
        // get the HTML
        ob_start();
        // HTML SHIT GOES HERE >:v
        ?>
        <style type="text/css">
        table { vertical-align: top; }
        tr    { vertical-align: top; }
        td    { vertical-align: top; }
        table.page_footer {
        width: 100%;
        border: none;
        border-top: solid 1mm #C0584F;
        background-color: white;
        border-radius: 2px;
        padding: 2mm
        }
        </style>
        <page backtop="14mm" backbottom="0mm" backleft="2mm" backright="0mm" style="font-size: 12pt">
        <bookmark title="Lettre" level="0" ></bookmark>
        <table cellspacing="0" style="width: 100%; text-align: center; font-size: 14px">
        <tr>
          <td style="width: 75%; text-align: left;">
            <h2>Reporte de Enfoques</h2>
          </td>
          <td style="width: 25%; color: #444444;">
            <img style="width: 100%;" src="../../assets/img/home/logo_bihaiv.png" alt="Logo"><br>
          </td>
        </tr>
        </table>
        <br>
        <br>
        <br>
        <table cellspacing="0" style="    width: 100%;
        border: none;
        background: #f37064;
        text-align: center;
        font-size: 11pt;
        color: white;
        padding: 1.5mm;
        border-radius: 2px 2px 0px 0px;">
        <tr>
          <th style="width: 32%">Enfoque</th>
          <th style="width: 32%">Fecha de creación</th>
          <th style="width: 13%">Califiación</th>
          <th style="width: 23%">Usuario</th>
        </tr>
        </table>
        <?php foreach ($data as $k => $v): ?>
        <table cellspacing="0" style="    width: 100%;
          border: none;
          background: #ffebe9;
          text-align: center;
          font-size: 11pt;
          color: black;
          padding: 1.5mm;
          border-radius: 0px;">
          <tr>
            <td style="width: 32%; text-align: center"><?php echo $v['enfoque']; ?></td>
            <td style="width: 32%; text-align: center"><?php echo $v['created_at']; ?></td>
            <td style="width: 13%; text-align: center"><?php echo $v['calificacion']; ?></td>
            <td style="width: 23%; text-align: center;"><?php echo $v['user_name']; ?></td>
          </tr>
        </table>
        <?php endforeach; ?>
        <br>
        <page_footer>
        <table class="page_footer">
          <tr>
            <td style="width: 33%; text-align: left;">
              &nbsp;
            </td>
            <td style="width: 34%; text-align: center">
              <img style="width: 150px;" src="../../assets/img/home/logo_bihaiv.png" alt="Logo"><br>
            </td>
            <td style="width: 33%; text-align: right">
              &nbsp;
            </td>
          </tr>
        </table>
        </page_footer>
      </page>
        <?php
        $content = ob_get_clean();

        $html2pdf = new Html2Pdf('P', 'A4', 'fr');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content);
        $html2pdf->Output('Reporte de enfoques.pdf');
    } catch (Html2PdfException $e) {
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

}
function pdf_aportaciones($data)
{
    try {
        // get the HTML
        ob_start();
        // HTML SHIT GOES HERE >:v
        ?>
        <style type="text/css">
        table { vertical-align: top; }
        tr    { vertical-align: top; }
        td    { vertical-align: top; }
        table.page_footer {
        width: 100%;
        border: none;
        border-top: solid 1mm #C0584F;
        background-color: white;
        border-radius: 2px;
        padding: 2mm
        }
        </style>
        <page backtop="14mm" backbottom="0mm" backleft="2mm" backright="0mm" style="font-size: 12pt">
        <bookmark title="Lettre" level="0" ></bookmark>
        <table cellspacing="0" style="width: 100%; text-align: center; font-size: 14px">
        <tr>
          <td style="width: 75%; text-align: left;">
            <h2>Reporte de Aportaciones</h2>
          </td>
          <td style="width: 25%; color: #444444;">
            <img style="width: 100%;" src="../../assets/img/home/logo_bihaiv.png" alt="Logo"><br>
          </td>
        </tr>
        </table>
        <br>
        <br>
        <br>
        <table cellspacing="0" style="    width: 100%;
        border: none;
        background: #f37064;
        text-align: center;
        font-size: 11pt;
        color: white;
        padding: 1.5mm;
        border-radius: 2px 2px 0px 0px;">
        <tr>
          <th style="width: 32%">Aportación</th>
          <th style="width: 32%">Fecha de creación</th>
          <th style="width: 13%">Califiación</th>
          <th style="width: 23%">Usuario</th>
        </tr>
        </table>
        <?php foreach ($data as $k => $v): ?>
        <table cellspacing="0" style="    width: 100%;
          border: none;
          background: #ffebe9;
          text-align: center;
          font-size: 11pt;
          color: black;
          padding: 1.5mm;
          border-radius: 0px;">
          <tr>
            <td style="width: 32%; text-align: center"><?php echo $v['_titulo']; ?></td>
            <td style="width: 32%; text-align: center"><?php echo $v['created_at']; ?></td>
            <td style="width: 13%; text-align: center"><?php echo $v['calificacion']; ?></td>
            <td style="width: 23%; text-align: center;"><?php echo $v['user_name']; ?></td>
          </tr>
        </table>
        <?php endforeach; ?>
        <br>
        <page_footer>
        <table class="page_footer">
          <tr>
            <td style="width: 33%; text-align: left;">
              &nbsp;
            </td>
            <td style="width: 34%; text-align: center">
              <img style="width: 150px;" src="../../assets/img/home/logo_bihaiv.png" alt="Logo"><br>
            </td>
            <td style="width: 33%; text-align: right">
              &nbsp;
            </td>
          </tr>
        </table>
        </page_footer>
      </page>
        <?php
        $content = ob_get_clean();

        $html2pdf = new Html2Pdf('P', 'A4', 'fr');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content);
        $html2pdf->Output('Reporte de enfoques.pdf');
    } catch (Html2PdfException $e) {
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

}
