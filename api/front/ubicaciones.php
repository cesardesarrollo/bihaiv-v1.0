<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoUbicaciones.php';
require_once '../../core/modules/index/model/DaoUbicacionesIniciativas.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoPaises.php';
require_once '../../core/modules/index/model/DaoEstados.php';
require_once '../../core/modules/index/model/DaoMunicipios.php';
require_once '../../core/modules/index/model/DaoLocalidades.php';
require_once '../../core/lang/lang.php';

$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$DaoUbicaciones = new DaoUbicaciones();
$Ubicaciones = new Ubicaciones();

$DaoUbicacionesIniciativas = new DaoUbicacionesIniciativas();
$UbicacionesIniciativas = new UbicacionesIniciativas();

$DaoPaises = new DaoPaises();
$Paises = new Paises();

$DaoEstados = new DaoEstados();
$Estados = new Estados();

$DaoMunicipios = new DaoMunicipios();
$Municipios = new Municipios();

$DaoLocalidades = new DaoLocalidades();
$Localidades = new Localidades();

$action = $_POST['method'];
$tablename = "ubicaciones";


switch($action){

case "all":
    echo json_encode($DaoUbicaciones->getAll());
    break;

case "delete":
    echo $DaoUbicaciones->delete($_POST['id']);
    break;

case "edit":
    break;

case "save":
    if (strlen($_POST['user_id'])>0) {
        $Organizacion = $DaoOrganizacion->getByUserId($_POST['user_id']);
        $Ubicaciones->setIdPais($_POST['pais_id']);
        if (isset($_POST['estado_id'])) {
            $Ubicaciones->setIdEstado($_POST['estado_id']);
        }
        if (isset($_POST['municipio_id'])) {
            $Ubicaciones->setIdMunicipio($_POST['municipio_id']);
        }
        if (isset($_POST['localidad_id'])) {
            $Ubicaciones->setIdLocalidad($_POST['localidad_id']);
        }
        if (isset($_POST['is_main'])) {
            $Ubicaciones->setIsMain($_POST['is_main']);
        }
        $Ubicaciones->setIdOrganizacion($Organizacion->id);
        $id = $DaoUbicaciones->add($Ubicaciones);
        if ($id>0) {
            echo json_encode(array('status' => true, 'msg' => translate('label addRegistro'), 'data'=>$id));
        } else {
            echo "Error";
        }
    } else {
        echo "Error";
    }
    break;


case "byId":
    echo json_encode($DaoUbicaciones->getById($_POST['id']));
    break;




  // Para ubicaciones de iniciativas
case "byIniciativaId":
    
    if (strlen($_POST['iniciativa_id'])>0) {
        $Ubicaciones = $DaoUbicacionesIniciativas->getByIniciativaId($_POST['iniciativa_id']);

        $response = array();
        foreach ($Ubicaciones as $ubicacion) {

            if (!empty($ubicacion->idPais)) {

                $Paises = $DaoPaises->getById($ubicacion->idPais);

                if (!empty($ubicacion->idEstado)) {
                    $Estados = $DaoEstados->getById($ubicacion->idEstado);
                } else {
                    $Estados->nombre = ' ';
                }
                if (!empty($ubicacion->idMunicipio)) {
                    $Municipios = $DaoMunicipios->getById($ubicacion->idMunicipio);
                } else {
                    $Municipios->nombre = ' ';
                }
                if (!empty($ubicacion->idLocalidad)) {
                    $Localidades = $DaoLocalidades->getById($ubicacion->idLocalidad);
                } else {
                    $Localidades->nombre = ' ';
                }
                array_push(
                    $response, array('id'=>$ubicacion->id,
                                      'pais_nombre'=>$Paises->nombre,
                                      'estado_nombre'=>$Estados->nombre,
                                      'municipio_nombre'=>$Municipios->nombre,
                                      'localidad_nombre'=>$Localidades->nombre,
                                      'influencia'=>($ubicacion->is_main)?'INFLUYE':'NO INFLUYE'
                    )
                );

            }

        }

        echo json_encode($response);
    } else {
        echo "Error";
    }

    break;


case "delete_ubicacion_iniciativa":
    echo $DaoUbicacionesIniciativas->delete($_POST['id']);
    break;



case "save_ubicaciones_iniciativas":
    if (strlen($_POST['iniciativa_id'])>0) {
        $UbicacionesIniciativas->setIdIniciativa($_POST['iniciativa_id']);
        $UbicacionesIniciativas->setIdPais($_POST['pais_id']);
        if (isset($_POST['estado_id'])) {
            $UbicacionesIniciativas->setIdEstado($_POST['estado_id']);
        }
        if (isset($_POST['municipio_id'])) {
            $UbicacionesIniciativas->setIdMunicipio($_POST['municipio_id']);
        }
        if (isset($_POST['localidad_id'])) {
            $UbicacionesIniciativas->setIdLocalidad($_POST['localidad_id']);
        }
        if (isset($_POST['is_main'])) {
            $UbicacionesIniciativas->setIsMain($_POST['is_main']);
        }
        $id = $DaoUbicacionesIniciativas->add($UbicacionesIniciativas);
        if ($id>0) {
            echo json_encode(array('status' => true, 'msg' => "Se agrego correctamente tu registro", 'data'=>$id));
        } else {
            echo "Error";
        }
    } else {
        echo "Error";
    }
    break;






case "byUserId":
    
    if (strlen($_POST['user_id'])>0) {
        $Organizacion = $DaoOrganizacion->getByUserId($_POST['user_id']);
        $Ubicaciones = $DaoUbicaciones->getByOrgId($Organizacion->id);

        $response = array();
        foreach ($Ubicaciones as $ubicacion) {
            $Paises = $DaoPaises->getById($ubicacion->idPais);

            if (!empty($ubicacion->idEstado)) {
                $Estados = $DaoEstados->getById($ubicacion->idEstado);
            } else {
                $Estados->nombre = ' ';
            }
            if (!empty($ubicacion->idMunicipio)) {
                $Municipios = $DaoMunicipios->getById($ubicacion->idMunicipio);
            } else {
                $Municipios->nombre = ' ';
            }
            if (!empty($ubicacion->idLocalidad)) {
                $Localidades = $DaoLocalidades->getById($ubicacion->idLocalidad);
            } else {
                $Localidades->nombre = ' ';
            }
            array_push(
                $response, array('id'=>$ubicacion->id,
                                    'pais_nombre'=>$Paises->nombre,
                                    'estado_nombre'=>$Estados->nombre,
                                    'municipio_nombre'=>$Municipios->nombre,
                                    'localidad_nombre'=>$Localidades->nombre,
                                    'influencia'=>($ubicacion->is_main)?'INFLUYE':'NO INFLUYE'
                )
            );
        }

        echo json_encode($response);
    } else {
        echo "Error";
    }

    break;


}
