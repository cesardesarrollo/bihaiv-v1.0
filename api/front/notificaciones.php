<?php
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoAportacion.php';
require_once '../../core/modules/index/model/NotificationData.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DaoIniciativa.php';
require_once '../../core/lang/lang.php';
$DaoOrganizacion  = new DaoOrganizacion();
$DaoAportacion    = new DaoAportacion();
$NotificationData = new NotificationData(); 
$DaoPublicaciones = new DaoPublicaciones();
$Publicaciones    = new Publicaciones();
$DaoIniciativa    = new DaoIniciativa();
$Iniciativa       = new Iniciativa();

$action = $_POST['method'];

switch($action){


case "getMessageById":

    if (strlen($_POST['sujeto_id'])>0) {
        $mensaje = $NotificationData->getMessageById($_POST['sujeto_id'], false);
        echo json_encode($mensaje);
    }

    break;

case "all":

    if (strlen($_POST['user_id'])>0) {
        $notificaciones = $NotificationData->getAllOfFriendsByUserId($_POST['user_id']);
        $array_notificaciones = array();
        foreach ($notificaciones as $notificacion) {
        
            if (!$NotificationData->getMessageById($notificacion['id'])) {
                if ($notificacion['message'] !== "") {
                    $mensaje = strip_tags(utf8_decode($notificacion['message'])) . " <small>(".translate('label novigentenotificacion').")</small>";
                }
            } else {
                $mensaje = utf8_decode($notificacion['message']);
            }

            if ($mensaje) {
                $array_push = array("mensaje"  => $mensaje);
                array_push($array_notificaciones, $array_push);
            }
        }
        echo json_encode($array_notificaciones);

    }

    break;

case "allByLimits":

    $limit_param1 = 0;
    $limit_param2 = 10;
    if (!empty($_POST['limit_param1'])) {
        $limit_param1 = $_POST['limit_param1'];
    }
    if (!empty($_POST['limit_param2'])) {
        $limit_param2 = $_POST['limit_param2'];
    }

    if (strlen($_POST['user_id'])>0) {
        $notificaciones = $NotificationData->getAllOfFriendsByUserIdWithLimits($_POST['user_id'], $limit_param1, $limit_param2);
        $array_notificaciones = array();
        foreach ($notificaciones as $notificacion) {
            $mensaje = "";
            if (!$NotificationData->getMessageById($notificacion['id'])) {
                if ($notificacion['message'] !== "") {
                    $mensaje = strip_tags(utf8_decode($notificacion['message'])) . " <small>(".translate('label novigentenotificacion').")</small>";
                }
            } else {
                $mensaje = utf8_decode($notificacion['message']);
            }

            if ($mensaje) {
                $array_push = array("mensaje"  => $mensaje);
                array_push($array_notificaciones, $array_push);
            }
        }
        echo json_encode($array_notificaciones);

    }

    break;

case "delete":
    break;

case "edit":
    break;

case "save":
    break;

case "byId":
    break;

}
