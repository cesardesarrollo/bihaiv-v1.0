<?php
 /*
  * OBTIENE DATOS GEOGRÁFICOS, ACTIVIDAD Y COLABORACIÓN
  *==============================
  */
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/UserData.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DaoIniciativa.php';
require_once '../../core/modules/index/model/DaoUbicaciones.php';
require_once '../../core/modules/index/model/DaoUbicacionesIniciativas.php';
require_once '../../core/modules/index/model/DaoUbicacionesEventos.php';
require_once '../../core/modules/index/model/DaoEstados.php';
require_once '../../core/modules/index/model/DaoPaises.php';
require_once '../../core/modules/index/model/DaoEvento.php';
require_once '../../core/modules/index/model/DaoColaboracion.php';

$UserData         = new UserData();
$DaoOrganizacion  = new DaoOrganizacion();
$DaoPublicaciones = new DaoPublicaciones();
$DaoEvento        = new DaoEvento();
$DaoUbicaciones   = new DaoUbicaciones();
$DaoEstados       = new DaoEstados();
$DaoIniciativa    = new DaoIniciativa();
$DaoColaboracion  = new DaoColaboracion();
$Organizacion     = new Organizacion();
$Publicaciones    = new Publicaciones();

$action = $_POST['method'];

switch($action){

case "allActors":

    $organizaciones = array();
    $users = $UserData->getAll();
    foreach ($users as $user):
        $actorUser = $UserData->getOnlyUsersById($user->id);
        if (is_object($actorUser)) :
            $organizacion = $DaoOrganizacion->getByUserId($actorUser->id);
            array_push($organizaciones, $organizacion);
        endif;
    endforeach;
    print_r(json_encode($organizaciones));

    break;

case "allEvents":

    $eventos = $DaoEvento->getAllEvents();
    print_r(json_encode($eventos));

    break;

case "activityEvents":

    /* Actividad de eventos
    *==============================
    */
    $estado = "";
    if (isset($_POST['estado'])) {
        $estado = $_POST['estado'];
    }
    $eventos = $DaoEvento->getAllEventsForReport($estado);
    Core::json($eventos);

    break;


case "activityInitiatives":

    /*
    *  Actividad de iniciativas | Coordenadas
    *==============================
    */
    $iniciativas = $DaoIniciativa->getAll();
    $array_ubicaciones = array();

    foreach ($iniciativas as $iniciativa):

        // Por iniciativa
        $iniciativa_id = $iniciativa['idIniciativa'];

        $DaoIniciativa              = new DaoIniciativa();
        $Iniciativa                 = new Iniciativa();
        $DaoUbicaciones             = new DaoUbicaciones();
        $DaoEstados                 = new DaoEstados();
        $DaoPaises                  = new DaoPaises();
        $DaoColaboracion            = new DaoColaboracion();
        $Estados                    = new Estados();
        $Paises                     = new Paises();
        $DaoUbicacionesIniciativas  = new DaoUbicacionesIniciativas();

        // Iniciativa
        $Iniciativa   = $DaoIniciativa->getById($iniciativa_id);

        // Alcance de Iniciativa
        $alcance_id     = $Iniciativa->getIdAlcanze();

        //  NACIONAL
        if ($alcance_id === 4) {
            $pais_id = $DaoUbicacionesIniciativas->getPaisIdByIniId($Iniciativa->getId());
            $estados = $DaoEstados->getByPaisId($pais_id);

            if (!empty($pais_id)) {

                foreach ($estados as $estado):
                    $Estados = $DaoEstados->getById($estado->idEstado);
                    $Paises  = $DaoPaises->getById($pais_id);
                    array_push(
                        $array_ubicaciones, (array(
                        "Titulo" => $Iniciativa->getTitulo(),
                        "Descripción" => $Iniciativa->getTextoCorto(),
                        "Fecha de inicio" => $Iniciativa->getFechaIniciativa(),
                        "Hora de inicio" => $Iniciativa->getHoraIni(),
                        "Hora de finalización" => $Iniciativa->getHoraFin(),
                        "Ubicación"=> $Estados->getNombre().", ".$Paises->getNombre()
                        ))
                    );
                endforeach;

            }
        }

        //  REGIONAL = 1, ESTATAL = 2, LOCAL = 3
        if ($alcance_id !== 4) {
            $ubicaciones = $DaoUbicacionesIniciativas->getActiveByIniId($Iniciativa->getId());

            foreach ($ubicaciones as $ubicacion):

                if (!empty($ubicacion->idEstado)) {

                    $Estados = $DaoEstados->getById($ubicacion->idEstado);
                    $Paises  = $DaoPaises->getById($ubicacion->idPais);

                    array_push(
                        $array_ubicaciones, array(
                        "Titulo" => $Iniciativa->getTitulo(),
                        "Descripción" => $Iniciativa->getTextoCorto(),
                        "Fecha de inicio" => $Iniciativa->getFechaIniciativa(),
                        "Hora de inicio" => $Iniciativa->getHoraIni(),
                        "Hora de finalización" => $Iniciativa->getHoraFin(),
                        "Ubicación"=> $Estados->getNombre().", ".$Paises->getNombre()
                        )
                    );
       
                }
            endforeach;
        }

    endforeach;

    Core::json($array_ubicaciones);

    break;

case "colaboracion":

    /*
    * Obtiene colaboración de iniciativas
    *==============================
    */
    $iniciativas = $DaoIniciativa->getAll();

    //Core::debug($iniciativas);

    $array_estados = array();
    $array_estados_new = array();

    function estados_by_ini_id($iniciativa_id)
    {

        $DaoIniciativa    = new DaoIniciativa();
        $Iniciativa       = new Iniciativa();
        $DaoOrganizacion  = new DaoOrganizacion();
        $DaoPublicaciones = new DaoPublicaciones();
        $DaoUbicaciones   = new DaoUbicaciones();
        $DaoUbicacionesIniciativas  = new DaoUbicacionesIniciativas();
        $DaoEstados       = new DaoEstados();
        $DaoColaboracion  = new DaoColaboracion();
        $Estados          = new Estados();


        // Organización
        $Iniciativa   = $DaoIniciativa->getById($iniciativa_id);
        // Alcance de organizacion
        $alcance_id   = $Iniciativa->getIdAlcanze();
        // Obtiene alcance de actor
        $array_estados = array();
      
        //Core::debug($Iniciativa);

        //Core::debug($alcance_id);

        if (isset($alcance_id)) {

            //  NACIONAL
            if ($alcance_id == 4) {
                $UbicacionesIniciativas = $DaoUbicacionesIniciativas->getPaisIdByIniId($Iniciativa->getId());

                //Core::debug($UbicacionesIniciativas);

                if (!empty($UbicacionesIniciativas)) {
                    if (!empty($UbicacionesIniciativas[0]->idPais)) {
                        $estados = $DaoEstados->getByPaisId($UbicacionesIniciativas[0]->idPais);
              
                        //Core::debug($estados);

                        foreach ($estados as $estado):
                            if (!empty($estado->iso)) {
                  
                                //Core::debug($estado->iso);

                                array_push($array_estados, $estado->iso);
                            }
                        endforeach;
              
                        //Core::debug($array_estados);

                    }
                }
            }

            //  REGIONAL = 1, ESTATAL = 2, LOCAL = 3
            if (isset($alcance_id) && $alcance_id != 4) {
                $ubicaciones = $DaoUbicacionesIniciativas->getActiveByIniId($Iniciativa->getId());
          
                //Core::debug($ubicaciones);

                foreach ($ubicaciones as $ubicacion):
                    if (!empty($ubicacion->idEstado)) {
                        $Estados = $DaoEstados->getById($ubicacion->idEstado);
              
                        //Core::debug($Estados);

                        array_push($array_estados, $Estados->getISO());
                    }
                endforeach;
            }
  
            //Core::debug($array_estados);

        }

        return $array_estados;
    }


    foreach ($iniciativas as $iniciativa):
        // Por iniciativa
        $estados_by_ini_id = estados_by_ini_id($iniciativa['idIniciativa']);
        $estados_by_ini_id_count = count($estados_by_ini_id);

        //Core::debug($estados_by_ini_id);
      
        //Core::debug($estados_by_ini_id_count);

        if ($estados_by_ini_id_count > 0) {

            // Colaboradores de iniciativa | mismo alcance de la iniciativa
            $colaboradores = $DaoColaboracion->getTrueColaboradoresByIdIniciativa($iniciativa['idIniciativa']);
            $colaboradores_count = count($colaboradores);
  
            //Core::debug($colaboradores);

            //Core::debug($colaboradores_count);

            if ($colaboradores_count > 0) {

                // Se multiplican estados por cada colaborador
                for ($x = 0; $x < $estados_by_ini_id_count; $x++){
      
                    //Core::debug($x);

                    for ($y = 0; $y < $colaboradores_count; $y++){
          
                        //Core::debug($y);

                        $estados_by_ini_id[] = $estados_by_ini_id[$x];
                
                        //Core::debug($estados_by_ini_id[$x]);
                    }          
                }
            }
        }


        //Core::debug($estados_by_ini_id);
        //Core::debug($estados_by_ini_new);

        if (count($estados_by_ini_id) > 0) { array_push($array_estados_new, $estados_by_ini_id);
        }
        $estados_by_ini_id = [];

    endforeach;


    /*
    * Obtiene COLABORACIÓN de eventos
    *================================
    */
    $eventos = $DaoEvento->getAllEvents();

    function estados_by_eve_id($evento_id)
    {

        $DaoEvento              = new DaoEvento();
        $Evento                 = new Evento();
        $DaoOrganizacion        = new DaoOrganizacion();
        $DaoPublicaciones       = new DaoPublicaciones();
        $DaoUbicaciones         = new DaoUbicaciones();
        $DaoEstados             = new DaoEstados();
        $DaoColaboracion        = new DaoColaboracion();
        $Estados                = new Estados();
        $DaoUbicacionesEventos  = new DaoUbicacionesEventos();

        // Evento
        $Evento = $DaoEvento->getById($evento_id);
        $array_estados = array();
        $ubicaciones = $DaoUbicacionesEventos->getByEventId($Evento->getId());
        foreach ($ubicaciones as $ubicacion):
            if (!empty($ubicacion->idEstado)) {
                $Estados = $DaoEstados->getById($ubicacion->idEstado);
                array_push($array_estados, $Estados->getISO());
            }
        endforeach;

        return $array_estados;
    }

    foreach ($eventos as $evento):

        // Por evento
        $estados_by_eve_id = estados_by_eve_id($evento['_idevento']);
        $estados_by_eve_id_count = count($estados_by_eve_id);

        // Colaboradores de evento
        $colaboradores = $DaoColaboracion->getTrueColaboradoresByIdEvento($evento['_idevento']);
        $colaboradores_count = count($colaboradores);

        // Se multiplican estados por cada colaborador
        for ($x = 0; $x < $estados_by_eve_id_count; $x++){
            for ($y = 0; $y < $colaboradores_count; $y++){
                $estados_by_eve_id[] = $estados_by_eve_id[$x];
            }          
        }

        if (count($estados_by_eve_id) > 0) { array_push($array_estados_new, $estados_by_eve_id);
        }
        $estados_by_eve_id = [];
     
    endforeach;

    // Listado de claves ISO de cada estado
    $array_isos = array();
    foreach ($array_estados_new as $array_edo) {
        if (count($array_edo) > 0) {
            foreach ($array_edo as $estado) {
                array_push($array_isos, $estado);
            }
        }
    }

    // Cuenta de estados (Arreglo solo debe traer una cadena (clave ISO) por valor)
    $array_counts = array_count_values($array_isos);

    $array_final = array();
    array_push($array_final, array('States','Estado','Colaboración'));
    // Armado de arreglo final incluyendo el nombre de estados
    foreach ($array_counts as $key => $value) {
        $Estados = $DaoEstados->getByISO($key);
        array_push($array_final, array($key, $Estados[0]->nombre, $value));
    }

    Core::json($array_final);

    break;


case "colaboracionIniciativas":

    /*
    * Obtiene colaboración de iniciativas 
    *==============================
    */
    $iniciativas = $DaoIniciativa->getAll();
    $array_estados = array();

    function estados_by_ini_id($iniciativa_id)
    {

        $DaoIniciativa    = new DaoIniciativa();
        $Iniciativa       = new Iniciativa();
        $DaoOrganizacion  = new DaoOrganizacion();
        $DaoPublicaciones = new DaoPublicaciones();
        $DaoUbicaciones   = new DaoUbicaciones();
        $DaoUbicacionesIniciativas  = new DaoUbicacionesIniciativas();
        $DaoEstados       = new DaoEstados();
        $DaoColaboracion  = new DaoColaboracion();
        $Estados          = new Estados();

        // Organización
        $Iniciativa   = $DaoIniciativa->getById($iniciativa_id);
        // Alcance de organizacion
        $alcance_id     = $Iniciativa->getIdAlcanze();
        // Obtiene alcance de actor
        $array_estados = array();

        //  NACIONAL
        if ($alcance_id == 4) {
            $UbicacionesIniciativas = $DaoUbicacionesIniciativas->getPaisIdByIniId($Iniciativa->getId());
            if (!empty($UbicacionesIniciativas[0]->idPais)) {
                $estados = $DaoEstados->getByPaisId($UbicacionesIniciativas[0]->idPais);
                foreach ($estados as $estado):
                    if (!empty($estado->iso)) {
                        array_push($array_estados, $estado->iso);
                    }
                endforeach;
            }
        }

        //  REGIONAL = 1, ESTATAL = 2, LOCAL = 3
        if (isset($alcance_id) && $alcance_id !== 4) {
            $ubicaciones = $DaoUbicacionesIniciativas->getActiveByIniId($Iniciativa->getId());
            foreach ($ubicaciones as $ubicacion):
                if (!empty($ubicacion->idEstado)) {
                    $Estados = $DaoEstados->getById($ubicacion->idEstado);
                    array_push($array_estados, $Estados->getISO());
                }
            endforeach;
        }
        return $array_estados;
    }

    foreach ($iniciativas as $iniciativa):
        // Por iniciativa
        $estados_by_ini_id = estados_by_ini_id($iniciativa['idIniciativa']);
        $estados_by_ini_id_count = count($estados_by_ini_id);

        // Colaboradores de iniciativa
        $colaboradores = $DaoColaboracion->getTrueColaboradoresByIdIniciativa($iniciativa['idIniciativa']);
        $colaboradores_count = count($colaboradores);

        // Se multiplican estados por cada colaborador
        for ($x = 0; $x < $estados_by_ini_id_count; $x++){
            for ($y = 0; $y < $colaboradores_count; $y++){
                $estados_by_ini_id[] = $estados_by_ini_id[$x];
            }          
        }

        if (count($estados_by_ini_id) > 0) { array_push($array_estados, $estados_by_ini_id);
        }
        $estados_by_ini_id = [];

    endforeach;

    // Listado de claves ISO de cada estado
    $array_isos = array();
    foreach ($array_estados as $array_edo) {
        if (count($array_edo) > 0) {
            foreach ($array_edo as $estado) {
                array_push($array_isos, $estado);
            }
        }
    }

    // Cuenta de estados (Arreglo solo debe traer una cadena (clave ISO) por valor)
    $array_counts = array_count_values($array_isos);
    $array_final = array();
    array_push($array_final, array('States','Estado','Colaboración'));
    // Armado de arreglo final incluyendo el nombre de estados
    foreach ($array_counts as $key => $value) {
        $Estados = $DaoEstados->getByISO($key);
        array_push($array_final, array($key, $Estados[0]->nombre, $value));
    }

    Core::json($array_final);

    break;


case "colaboracionEventos":

    /*
    * Obtiene ISOs de estados con eventos
    *================================
    */
    //Barrer eventos todos
    $eventos = $DaoEvento->getAllEvents();
    $array_estados = array();

    function estados_by_eve_id($evento_id)
    {

        $DaoEvento              = new DaoEvento();
        $Evento                 = new Evento();
        $DaoOrganizacion        = new DaoOrganizacion();
        $DaoPublicaciones       = new DaoPublicaciones();
        $DaoUbicaciones         = new DaoUbicaciones();
        $DaoEstados             = new DaoEstados();
        $DaoColaboracion        = new DaoColaboracion();
        $Estados                = new Estados();
        $DaoUbicacionesEventos  = new DaoUbicacionesEventos();

        // Evento
        $Evento = $DaoEvento->getById($evento_id);
        $array_estados = array();
        $ubicaciones = $DaoUbicacionesEventos->getByEventId($Evento->getId());
        foreach ($ubicaciones as $ubicacion):
            if (!empty($ubicacion->idEstado)) {
                $Estados = $DaoEstados->getById($ubicacion->idEstado);
                array_push($array_estados, $Estados->getISO());
            }
        endforeach;

        return $array_estados;
    }

    foreach ($eventos as $evento):

        // Por evento
        $estados_by_eve_id = estados_by_eve_id($evento['_idevento']);
        $estados_by_eve_id_count = count($estados_by_eve_id);

        // Colaboradores de evento
        $colaboradores = $DaoColaboracion->getTrueColaboradoresByIdEvento($evento['_idevento']);
        $colaboradores_count = count($colaboradores);

        // Se multiplican estados por cada colaborador
        for ($x = 0; $x < ($estados_by_eve_id_count - 1); $x++){
            for ($y = 0; $y < ($colaboradores_count - 1); $y++){
                $estados_by_eve_id[] = $estados_by_eve_id[$x];
            }          
        }

        if (count($estados_by_eve_id) > 0) { array_push($array_estados, $estados_by_eve_id);
        }
        $estados_by_eve_id = [];
     
    endforeach;

    // Listado de claves ISO de cada estado
    $array_isos = array();
    foreach ($array_estados as $array_edo) {
        if (count($array_edo) > 0) {
            foreach ($array_edo as $estado) {
                array_push($array_isos, $estado);
            }
        }
    }

    // Cuenta de estados (Arreglo solo debe traer una cadena (clave ISO) por valor)
    $array_counts = array_count_values($array_isos);
    $array_final = array();
    array_push($array_final, array('States','Estado','Colaboración'));
    // Armado de arreglo final incluyendo el nombre de estados
    foreach ($array_counts as $key => $value) {
        $Estados = $DaoEstados->getByISO($key);
        array_push($array_final, array($key, $Estados[0]->nombre, $value));
    }

    Core::json($array_final);

    break;


case "delete":
    break;

case "edit":
    break;

case "save":
    break;

case "byId":
    break;


}
