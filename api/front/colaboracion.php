<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/modules/index/model/UserData.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DaoIniciativa.php';
require_once '../../core/modules/index/model/DaoEvento.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoColaboracion.php';
require_once '../../core/modules/index/model/NotificationData.php';

$DaoPublicaciones= new DaoPublicaciones();
$Publicaciones = new Publicaciones();

$DaoIniciativa= new DaoIniciativa();
$Iniciativa = new Iniciativa();

$DaoEvento= new DaoEvento();
$Evento = new Evento();

$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$DaoColaboracion = new DaoColaboracion();

$action = $_POST['method'];
$tablename = "colaboracion";

switch($action){

case "all":
    if (strlen($_POST['user_id'])>0) {
        $org = $DaoOrganizacion->getByUserId($_POST['user_id']);

        $colaboraciones = $DaoColaboracion->getByUserId($org->id);

        $array_colaboracion = array();
        foreach ($colaboraciones as $colaboracion) {

            /* Es una iniciativa */
            if ($colaboracion->Iniciativa_idIniciativa > 0) {
                $iniciativa = $DaoIniciativa->getById($colaboracion->Iniciativa_idIniciativa);

                if (is_object($iniciativa)) {
                    $publicacion = $DaoPublicaciones->getById($iniciativa->Post_idPost);
                    $org = $DaoOrganizacion->getById($publicacion->user_id);
            
                    //QUIEN CREO LA INICIATIVA
                    $participacon = UserData::getById($org->Usuarios_idUsuarios);
            
                    //QUIEN ESTA COLABORANDO CON INICIATIVA/EVENTO
                    $participa = UserData::getById($_POST['user_id']);

                    $array_push = array("user_id_colaborador"   => $participa->id,
                                "user_name_colaborador" => $participa->name,
                                "user_id_creador"       => $participacon->id,
                                "user_name_creador"     => $participacon->name,
                                "titulo"                => $iniciativa->titulo
                                );
                    array_push($array_colaboracion,  $array_push);

                }
            }
            /* Es un evento */
            else if ($colaboracion->evento_id > 0) {
                $Evento = $DaoEvento->getById($colaboracion->evento_id);

                if (is_object($Evento)) :

                    //QUIEN CREO LA EVENTO
                    $participacon = UserData::getById($Evento->actorId);

                    if (is_object($participacon)) :

                        //QUIEN ESTA COLABORANDO CON EVENTO
                        $participa = UserData::getById($_POST['user_id']);

                        if (is_object($participa)) :

                            $array_push = array("user_id_colaborador"   => $participa->id,
                                    "user_name_colaborador" => $participa->name,
                                    "user_id_creador"       => $participacon->id,
                                    "user_name_creador"     => $participacon->name,
                                    "titulo"                => $Evento->titulo
                                              );
                            array_push($array_colaboracion,  $array_push);

                        endif;
                    endif;
                endif;
            }
        }
        if (count($array_colaboracion)>0) {
            echo json_encode($array_colaboracion);
        } else {
            echo "<h3 class=\"text-center\">".translate('label noColaboracion')."</h3>";
        }
    } else {
        echo "Error";
    }
    break;

case "delete":
    break;

case "update":

    if (strlen($_POST['idColaboracion'])>0) {
        $colaboracion = $DaoColaboracion->getById($_POST['idColaboracion']);
        $colaboracion->setId($_POST['idColaboracion']);
        $colaboracion->setEstatus($_POST['estatus']);
        $response = $DaoColaboracion->update($colaboracion);

        $org = $DaoOrganizacion->getById($colaboracion->getUsuario_id());
        $user_invitado = UserData::getById($org->Usuarios_idUsuarios);
        $user_invitado_name = $user_invitado->getFullname();

        // Envía notificación a quien invita a colaborar
        if (is_numeric($response) && $response > 0) {

            /* Es una iniciativa */
            if ($colaboracion->Iniciativa_idIniciativa > 0) {
                $iniciativa = $DaoIniciativa->getById($colaboracion->Iniciativa_idIniciativa);

                if (is_object($iniciativa)) {
                    $publicacion = $DaoPublicaciones->getById($iniciativa->Post_idPost);
                    $org = $DaoOrganizacion->getById($publicacion->user_id);
            
                    //QUIEN CREÓ LA INICIATIVA
                    $participacon = UserData::getById($org->Usuarios_idUsuarios);
           
                    if (is_object($participacon)) :

                        $invitacion = ($_POST['estatus'] == 1) ? "aceptada" : "rechazada";

                        $emailto = $participacon->email;
                        $subject = "La participación del actor $user_invitado_name a la iniciativa $iniciativa->titulo fue $invitacion";
                        $body    = "La participación del actor $user_invitado_name a la iniciativa $iniciativa->titulo fue $invitacion";
                        $altbody = "La participación del actor $user_invitado_name a la iniciativa $iniciativa->titulo fue $invitacion";

                        Core::mail($emailto, $subject, $body, $altbody);
           
                    endif;

                }
            }
            /* Es un evento */
            else if ($colaboracion->evento_id > 0) {
                $Evento = $DaoEvento->getById($colaboracion->evento_id);

                if (is_object($Evento)) :

                    //QUIEN CREO LA EVENTO
                    $participacon = UserData::getById($Evento->actorId);

                    if (is_object($participacon)) :

                        $invitacion = ($_POST['estatus'] == 1) ? "aceptada" : "rechazada";

                        $emailto = $participacon->email;
                        $subject = "La participación del actor $user_invitado_name al evento $Evento->titulo fue $invitacion";
                        $body    = "La participación del actor $user_invitado_name al evento $Evento->titulo fue $invitacion";
                        $altbody = "La participación del actor $user_invitado_name al evento $Evento->titulo fue $invitacion";

                        Core::mail($emailto, $subject, $body, $altbody);

                    endif;
                endif;
            }
        }   

        echo json_encode($response);

    } else {
        echo "Error";
    }
    break;

case "save":
    break;

case "byId":
    break;

case "byIdEvento":
    $userdata = new UserData();
    $colaboraciones = $DaoColaboracion->getTrueByIdEvento($_POST['evento_id']);
    $array_colaboracion = array();
    foreach ($colaboraciones as $colaboracion) {
        $org = $DaoOrganizacion->getById($colaboracion['user_id']);
        // Si el avatar es el de default
        if ($org->avatar === "assets/img/home/perfil.png") {
            $org->avatar = "../../../../assets/img/home/perfil.png";
        }
        $user = $userdata->getById($org->Usuarios_idUsuarios);
        array_push($array_colaboracion, array('user_id' => $user->id, 'user_name' => $user->name, 'avatar' => $org->avatar ));
    }
    echo json_encode($array_colaboracion);
    break;

case "byIdIniciativa":
    $userdata = new UserData();
    $colaboraciones = $DaoColaboracion->getTrueColaboradoresByIdIniciativa($_POST['iniciativa_id']);
    $array_colaboracion = array();
    foreach ($colaboraciones as $colaboracion) {
        $org = $DaoOrganizacion->getById($colaboracion['user_id']);
        $user = $userdata->getById($org->Usuarios_idUsuarios);
        array_push($array_colaboracion, array('user_id' => $user->id, 'user_name' => $user->name));
    }
    echo json_encode($array_colaboracion);
    break;

}
