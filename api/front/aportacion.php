<?php
/**
 * Aportacion
 * 
 * PHP Version 5.6.15
 *
 * @category API
 * @package  Bihaiv_1.0
 * @author   César Alonso Magaña G. <cesar_alonso_m_g@hotmail.com>
 * @license  www.bihaiv.com @SyeSoftware
 * @link     api/front/aportacion.php
 **/

session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/lang/lang.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DTO/Publicaciones.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DTO/Organizacion.php';
require_once '../../core/modules/index/model/DaoAportacion.php';
require_once '../../core/modules/index/model/DTO/Aportacion.php';
require_once '../../core/modules/index/model/FriendData.php';
require_once '../../core/modules/index/model/NotificationData.php';

$DaoPublicaciones= new DaoPublicaciones();
$Publicaciones = new Publicaciones();

$DaoAportacion8= new DaoAportacion();
$Aportacion = new Aportacion();

$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$FriendData= new FriendData();

$action = $_POST['method'];
$tablename = "aportacion";

switch($action){
case "all":
    if (isset($_POST['user_id']) AND is_numeric($_POST['user_id']) AND $_POST['user_id'] > 0) {
        $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
        /* HERE COMES NEW CHALLENGER */
        $query = "SELECT
                    p.id AS _idPublicacion,
                    p.fechaCreado AS _fechaCreacion,
                    p.tipo AS _tipo,
                    p.user_id AS _usuario,
                    ap.idAportacion AS _idAportacion,
                    ap.titulo AS _titulo,
                    ap.descripcion AS _descripcion,
                    ap.thumbnail AS _thumbnail,
                    ap.file AS _file,
                    ap.post_id AS _postId,
                    IFNULL(ROUND(AVG(ca.calificacion)), 0) AS _calificacion,
                    COUNT(calificacion) AS _calificaron,
                    ap.visitas as _descargas
                FROM+
                    publicaciones AS p
                INNER JOIN
                    aportacion AS ap ON p.id = ap.post_id
                LEFT JOIN
                    calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
                WHERE
                    p.active = 1 AND p.tipo = 3
                    AND p.user_id = $org->id
                GROUP BY _idPublicacion
                ORDER BY p.fechaCreado DESC
                LIMIT 0 , 5";
        $response = $DaoPublicaciones -> getAllRows($query);


        if (!$response) {
            $response = array();
        } else {
            foreach ($response as $k => $v) {


                /* Variables de formula */
                $UNIVERSO     = 0;
                $DESCARGAS    = $v['_descargas'];
                $calidad      =$v['_calificacion'];

                $descargas    = $DESCARGAS;
                $calificaron  = $v['_calificaron'];

                if ($universoResultSet = $DaoPublicaciones->getOneRow('SELECT COUNT(*) as universo FROM user WHERE is_valid = 1 AND is_active = 1 AND activo = 1 AND tipoRel = 1') ) {
                    $UNIVERSO = $universoResultSet['universo'];
                }

                /* Calcular alcance */
                $numerador   = (0.0025 * $DESCARGAS) * ( $UNIVERSO - 1 );
                $denominador = 0.25 * ( $UNIVERSO - $DESCARGAS );

                if ($DESCARGAS == $UNIVERSO) {
                    $CONFIANZA = 1.96;
                } else {
                    if ($denominador == 0) {
                        $CONFIANZA = 1.96;
                    } else {
                        $CONFIANZA = sqrt($numerador / $denominador);
                    }
                }

                $alcance = 0;
                if ($CONFIANZA < 1.28) {
                    $alcance = 1;
                } elseif ($CONFIANZA >= 1.28 AND $CONFIANZA < 1.44) {
                    $alcance = 1.41421356;
                } elseif ($CONFIANZA >= 1.44 AND $CONFIANZA < 1.65) {
                    $alcance = 1.73205081;
                } elseif ($CONFIANZA >= 1.65 AND $CONFIANZA < 1.96) {
                    $alcance = 2;
                } elseif ($CONFIANZA >= 1.96) {
                    $alcance = 2.23606798;
                }

                /*
                * Asignar Calificacion a interes y Evalua el nivel de confianza
                */

                /* Calcular interes */
                $numerador = (0.0025 * $calificaron) * ( $descargas - 1 );
                $denominador = 0.25 * ( $descargas - $calificaron );

                if ($calificaron == $descargas) {
                    $confianza = 1.96;
                } else {
                    if ($denominador == 0) {
                        $confianza = 1.96;
                    } else {
                        $confianza = sqrt($numerador / $denominador);
                    }
                }

                $interes = 0;
                if ($confianza < 1.28) {
                    $interes = 1;
                } elseif ($confianza >= 1.28 AND $confianza < 1.44) {
                    $interes = 1.41421356;
                } elseif ($confianza >= 1.44 AND $confianza < 1.65) {
                    $interes = 1.73205081;
                } elseif ($confianza >= 1.65 AND $confianza < 1.96) {
                    $interes = 2;
                } elseif ($confianza >= 1.96) {
                    $interes = 2.23606798;
                }

                /* Calcular nivel de impacto */
                $impacto = round((0.5 * $calidad) + (0.5 * $alcance * $interes));


                $v['_calificacion'] = ( $impacto < 1 ) ? 1 : $impacto;
                /* End formula ---------------------------------------------------------*/

                $stars = '';
                if (isset($_SESSION['user_id'])) {

                    if ($_POST['user_id'] == $_SESSION['user_id']) {
                        /*
                        * Mis aportaciones, en este caso la calificacion es solo informativa
                        * por lo tanto no puedo calificar mis aportaciones.
                        */
                        // strellas
                        $stars = '';
                        for ($i = 1; $i <= 5 ; $i++) {
                            if ($v['_calificacion'] == 0) {
                                $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
                            } elseif ($v['_calificacion'] < $i) {
                                $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
                            } else {
                                $stars .= '<i style="color: orange;" class="fa fa-star" aria-hidden="true"></i>';
                            }
                        }
                    } else {
                        /*
                        * Las aportaciones pertenecen a otro actor por lo tanto puedo
                        * calificarlas.
                        */
                        // $stars = '<div class="rateYo-aportaciones" data-calificacion="'.$v['_calificacion'].'" data-idaportacion="'.$v['_idAportacion'].'"></div>';
                        $stars = '<div class="rateYo-aportaciones" data-calificacion="'.($k+1).'" data-idaportacion="'.$v['_idAportacion'].'"></div>';
                    }
                }
          
                $response[$k]['_stars'] = $stars;
            }
        }
      
        echo json_encode($response);

    } else {
        echo "Error";
    }
    break;

case "all_best_practices":
    if (isset($_POST['user_id']) AND is_numeric($_POST['user_id']) AND $_POST['user_id'] > 0  ) {
        $org = $DaoOrganizacion->getByUserId($_POST['user_id']);

        /* HERE COMES NEW CHALLENGER */
        $query = "SELECT
            p.id AS _idPublicacion,
            p.fechaCreado AS _fechaCreacion,
            p.tipo AS _tipo,
            p.user_id AS _usuario,
            ap.idAportacion AS _idAportacion,
            ap.titulo AS _titulo,
            ap.descripcion AS _descripcion,
            ap.thumbnail AS _thumbnail,
            ap.file AS _file,
            ap.post_id AS _postId,
            IFNULL(ROUND(AVG(ca.calificacion)), 0) AS _calificacion
        FROM
            publicaciones AS p
                INNER JOIN
            aportacion AS ap ON p.id = ap.post_id
                LEFT JOIN
            calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
        WHERE
            p.active = 1 AND p.tipo = 3
                AND p.user_id = $org->id
        GROUP BY _idPublicacion
        ORDER BY p.fechaCreado DESC
        LIMIT 0 , 5";
        $response = $DaoPublicaciones->advancedQuery($query);
        foreach ($response as $k => $v) {
            $stars = '';
            for ($i = 1; $i <= 5 ; $i++) {
                if ($v['_calificacion'] == 0) {
                    $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
                } elseif ($v['_calificacion'] < $i) {
                    $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
                } else {
                    $stars .= '<i style="color: orange;" class="fa fa-star" aria-hidden="true"></i>';
                }
            }
            $response[$k]['_stars'] = $stars;
        }
        echo json_encode($response);
    } else {
        echo "Error";
    }
    break;

case "delete":

    $Aportacion = $DaoAportacion->getById($_POST['id']);
    $aportacion_id = $DaoAportacion->delete($_POST['id'], $Aportacion->Post_idPost);
    if ($aportacion_id > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => translate('label eliminadaAportacion')."!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => translate('label erroreliminarAportacion') )));
    }

    break;

case "edit":
    break;

case "save":
    $Publicaciones->setFechaCreado(date('Y-m-d h:i:s'));
    $Publicaciones->setTipo($_POST['tipo']);
      
    $Publicaciones->setIsPublic(0);
    $Publicaciones->setByExpert(0);
    $Publicaciones->setByAdmin(0);

    $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    $Publicaciones->setUser_id($org->id);
    $id = $DaoPublicaciones->add($Publicaciones);

    if ($id>0) {
        $Aportacion->setFecha(date('Y-m-d h:i:s'));
        $Aportacion->setTitulo($_POST['titulo']);
        $Aportacion->setThumbnail($_POST['photoCover']);
        $Aportacion->setFile($_POST['file']);
        $Aportacion->setCategory($_POST['categoria']);
        $Aportacion->setDescripcion($_POST['descripcion']);
        $Aportacion->setPost_idPost($id);
        $aportacion_id = $DaoAportacion->add($Aportacion);

        $friends = $FriendData->getFriends($_SESSION["user_id"]);
        foreach ($friends as $friend) {
            $friend_id = ($friend->receptor_id !== $_SESSION["user_id"]) ? $friend->receptor_id : $friend->sender_id;

            // Crear notificación
            $notification = new NotificationData();
            $notification->not_type_id = 8; // crea
            $notification->type_id = 3; // colaboracion
            $notification->ref_id = $aportacion_id; // =  id de colaboración
            $notification->receptor_id = $friend_id; // en este caso nos referimos a quien va dirigida la notificacion
            $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
            $notification->id = $notification->add();

            $notification->message = $notification->getMessageById($notification->id);
            $notification->updateMessage();
        }

    }
    if ($aportacion_id > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => translate('label guardarAportacion')."!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' =>translate('label errorGenerico') )));
    }
    break;

case "byId":
    break;

case "update_rating":
    $toSend = $_POST;
    $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
    $toSend['organizacion_id'] = $org -> id;
    $DaoAportacion -> updateRating($toSend);
    json($DaoAportacion -> response);
    break;

case "aportaciones_visitas":
    if (isset($_POST['aportacion_id']) AND is_numeric($_POST['aportacion_id'])) {
        $aportacion_id = $_POST['aportacion_id'];
        if ($DaoAportacion -> addVisitAportacion($aportacion_id)) {
            $response['msg']    = translate('label descarga');
            $response['class']  = "success";
            json($response);
        }
    }
    break;

case "aportaciones_bihaiv":
    $data = $DaoPublicaciones -> getAllBySectionBihaiv($_POST);
    json($data);
    break;
    
case "aportaciones_ecosistema":
    $data = $DaoPublicaciones -> getAllBySectionEcosistema($_POST);
    json($data);
    break;

case "count_calendario":
    $mesTrans=translate('months');
    $meses = array(
    '0'   => array('mes' =>  $mesTrans[0], 'count' => 0),
    '1'   => array('mes' =>  $mesTrans[1], 'count' => 0),
    '2'   => array('mes' =>  $mesTrans[2], 'count' => 0),
    '3'   => array('mes' =>  $mesTrans[3], 'count' => 0),
    '4'   => array('mes' =>  $mesTrans[4], 'count' => 0),
    '5'   => array('mes' =>  $mesTrans[5], 'count' => 0),
    '6'   => array('mes' =>  $mesTrans[6], 'count' => 0),
    '7'   => array('mes' =>  $mesTrans[7], 'count' => 0),
    '8'   => array('mes' =>  $mesTrans[8], 'count' => 0),
    '9'   => array('mes' =>  $mesTrans[9], 'count' => 0),
    '10'  => array('mes' =>  $mesTrans[10], 'count' => 0),
    '11'  => array('mes' =>  $mesTrans[11], 'count' => 0),
    );
    // breakpoint($meses);
    if ($_POST['tipo'] == 'bihaiv' ) {
        foreach ($meses as $k => $v) {
            $data = $DaoPublicaciones -> getPublicacionesMes($k+1, $_POST['tipo']);
            if (!empty($data)) {
                $meses[$k]['count'] = $data['count'];
            }
        }
    } elseif ($_POST['tipo'] == 'ecosistema' ) {
        foreach ($meses as $k => $v) {
            $data = $DaoPublicaciones -> getPublicacionesMes($k+1, $_POST['tipo']);
            if (!empty($data)) {
                $meses[$k]['count'] = $data['count'];
            }
        }
    }
    json($meses);
    break;
}
