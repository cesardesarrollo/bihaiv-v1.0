<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/modules/index/model/DaoObjetivo.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/lang/lang.php';
$DaoObjetivo= new DaoObjetivo();
$Objetivo = new Objetivo();
$DaoOrganizacion=new DaoOrganizacion();
$method = $_POST['method'];
$tablename = "objetivo";

//$FocusData = new FocusData();

switch($method){

case "all":

    json($DaoObjetivo->getAll());

    break;

  
case "allByEventoId":
    if (isset($_POST['evento_id'])) {
        $DaoObjetivo->getAllByEventId($_POST['evento_id']);
    }
    break;

case "update_rating":
    $toSend = $_POST;
    $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    $toSend['organizacion_id'] = $org -> id;
    $id = $DaoObjetivo -> updateRating($toSend);
    if ($id > 0 ) {
        json(
            array(
            'status'      => true,
            'msg'         => translate('label guardarCal'),
            'class'       => 'success'
            )
        );
    } else {
        json(
            array(
            'status'  => false,
            'msg'     => translate('label errorGenerico'),
            'class'   => 'warning'
            )
        );
    }
    break;
  
case "allByLimits":

    $limit_param1 = 0;
    $limit_param2 = 10;
    if (!empty($_POST['limit_param1'])) {
        $limit_param1 = $_POST['limit_param1'];
    }
    if (!empty($_POST['limit_param2'])) {
        $limit_param2 = $_POST['limit_param2'];
    }

    if (strlen($_POST['user_id'])>0) {
        $iniciativas = $DaoIniciativa->getByUserIdWithLimits($_POST['user_id'], $limit_param1, $limit_param2);
        echo json_encode($iniciativas);
    }

    break;

case "delete":

    $objetivo_id = $DaoObjetivo->delete($_POST['id']);

    break;

case "edit":
    
    break;

case "save":

    $Objetivo->setDescripcion($_POST['descripcion']);
    echo $DaoObjetivo->add($Objetivo);

    break;

}
