<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/modules/index/model/DaoEvento.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoObjetivo.php';
require_once '../../core/modules/index/model/HeartData.php';
require_once '../../core/modules/index/model/FriendData.php';
require_once '../../core/modules/index/model/NotificationData.php';
require_once '../../core/modules/index/model/DaoColaboracion.php';
require_once '../../core/modules/index/model/DaoUbicacionesEventos.php';
require_once '../../core/modules/index/model/DaoPaises.php';
require_once '../../core/modules/index/model/DaoEstados.php';
require_once '../../core/modules/index/model/DaoLocalidades.php';
require_once '../../core/lang/lang.php';
$DaoEvento        = new DaoEvento();
$Evento           = new Evento();
$FriendData       = new FriendData();
$DaoOrganizacion  = new DaoOrganizacion();
$DaoObjetivo      = new DaoObjetivo();
$DaoUbicacionesEventos   = new DaoUbicacionesEventos();
$DaoPaises        = new DaoPaises();
$DaoEstados       = new DaoEstados();
$DaoLocalidades   = new DaoLocalidades();

$user_id = null;
if (isset($_SESSION['user_id'])) {
    $Organizacion = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    $user_id = $Organizacion->id;
}

$DaoColaboracion    = new DaoColaboracion();
$Colaboracion       = new Colaboracion();
$UbicacionesEventos = new UbicacionesEventos();

$method = $_POST['method'];

switch($method){

case "all":
    $action = $_POST['action'];
    if ($action == 'currentEvents') {
        $events = $DaoEvento->getEventos($user_id);
        if (count($events)>0) {
            print_r(json_encode($events));
        } else {
            echo "";
        }
    }
    else if ($action == 'coomingEvents' ) {
        $events = $DaoEvento->getCoomingEvents($user_id);
        print_r(json_encode($events));
    }
    else if ($action == 'coomingEventsForCalendar' ) {
        $events = $DaoEvento->getCoomingEventsForCalendar($_POST['user_id']);
        print_r(json_encode($events));
    }
    else if ($action == 'lastEvents') {
        $events = $DaoEvento->getLastEvents($user_id);
        print_r(json_encode($events));
    }
    else if ($action == 'coomingEventsForTagging') {
        $events = $DaoEvento->getCoomingEventsForTagging();
        print_r(json_encode($events));
    }
    else if ($action === 'getAllEvents' ) {
        $events = $DaoEvento->getAllEvents();
        print_r(json_encode($events));
    }
    else if ($action === 'getAllEventsExceptOwns' ) {
        $key_array = (isset($_POST['key_array'])) ? $_POST['key_array'] : [];
        $events = $DaoEvento->getAllEventsExceptOwns($key_array);
        print_r(json_encode($events));
    }

    break;

case "delete":

    $NotificationData = new NotificationData();
    // Eliminar objetivos relacionadas
    $DaoObjetivo->deleteByEventId($_POST['id']);
    // Eliminar colaboraciones relacionadas
    $DaoColaboracion->deleteByEventId($_POST['id']);
    // Eliminar notificaciones relacionadas
    $NotificationData->deleteByEventId($_POST['id']);
    // Eliminar iniciativa relacionada
    $DaoEvento->deleteIniciativaEventos($_POST['id']);
    // Elimina Evento
    echo $DaoEvento->delete($_POST['id']);

    break;

case "edit":

    $Evento->setTitulo($_POST['titulo']);
    $Evento->setCreado(date('Y-m-d'));
    $Evento->setTextoCorto($_POST['descripcion']);
    $Evento->setTextoLargo($_POST['descripcion']);
    $Evento->setIniciativaId($_POST['initiatives']);
    $Evento->setFechaEvento($_POST['initialDate']);
    $Evento->setFechaFin($_POST['endDate']);
    $Evento->setHoraInicio($_POST['initialHour']);
    $Evento->setLugar($_POST['place']);
    $Evento->setLat($_POST['lat']);
    $Evento->setLng($_POST['lng']);
    $Evento->setHoraFin($_POST['endHour']);
    $Evento->setUsuario_id($Organizacion->id);
    $Evento->setLlaveImg($_POST['photoEvent']);
    $Evento->setAcceso($_POST['access']);
    $Evento->setUrl($_POST['url']);
    $Evento->setTipoEvento($_POST['type']);
    $Evento->setId($_POST['id']);
    $evento_id = $DaoEvento->update($Evento);

    if ($evento_id > 0 ) {

        /*
        * UBICACIÓN
        * @description: Se agrega una ubicación
        * ================================
        */
        if (!empty($_POST["estado"])) {
            $UbicacionesEventos->setIdEvento($evento_id);
            if (!empty($_POST['pais']) ) {    $UbicacionesEventos->setIdPais($_POST['pais']);
            }
            if (!empty($_POST['estado']) ) {  $UbicacionesEventos->setIdEstado($_POST['estado']);
            }
            if (!empty($_POST['ciudad']) ) {  $UbicacionesEventos->setIdLocalidad($_POST['ciudad']);
            }
            $ubicacionevento_id = $DaoUbicacionesEventos->update($UbicacionesEventos);
        }

        /*
        * COLABORACIONES
        * @description: Envío de notificaciones a colaboradores y creación de colaboraciones
        * ================================
        */
        if (!empty($_POST["colaboradores"])) {
            $colaboradores = $_POST["colaboradores"];
            foreach ($colaboradores as $colaborador) {

                // Colaboración
                $Colaboracion->setFechaCreado(date('Y-m-d'));
                $Colaboracion->setEstatus(0);
                $Colaboracion->setIniciativa_idIniciativa(0);
                $Colaboracion->setEventoId($evento_id);
                $org = $DaoOrganizacion->getByUserId($colaborador);
                $Colaboracion->setUsuario_id($org->id);
                $DaoColaboracion->add($Colaboracion);

                // Crear notificación al colaborador
                $notification = new NotificationData();
                $notification->not_type_id = 5; // colaborar
                $notification->type_id = 4; // evento
                $notification->ref_id = $evento_id; // =  id de evento
                $notification->receptor_id = $colaborador; // en este caso nos referimos a quien va dirigida la notificacion
                $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                $notification->id = $notification->add();

                $notification->message = $notification->getMessageById($notification->id);
                $notification->updateMessage();
            }
        }
    }
    if ($evento_id > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' =>translate('label actualizarEvento') ."!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => translate('label errorActualizar') )));
    }

    break;

case "save":

    $Evento->setTitulo($_POST['titulo']);
    $Evento->setCreado(date('Y-m-d'));
    $Evento->setTextoCorto($_POST['descripcion']);
    $Evento->setTextoLargo($_POST['descripcion']);
    $Evento->setIniciativaId($_POST['initiatives']);
    $Evento->setFechaEvento($_POST['initialDate']);
    $Evento->setFechaFin($_POST['endDate']);
    $Evento->setHoraInicio($_POST['initialHour']);
    $Evento->setLugar($_POST['place']);
    $Evento->setLat($_POST['lat']);
    $Evento->setLng($_POST['lng']);
    $Evento->setHoraFin($_POST['endHour']);
    $Evento->setUsuario_id($Organizacion->id);
    $Evento->setLlaveImg($_POST['photoEvent']);
    $Evento->setAcceso($_POST['access']);
    $Evento->setUrl($_POST['url']);
    $Evento->setTipoEvento($_POST['type']);
    $evento_id = $DaoEvento->add($Evento);

    /*
    * UBICACIÓN
    * @description: Se agrega una ubicación
    * ================================
    */
    if ($evento_id > 0 ) {
        if (!empty($_POST["estado"])) {
            $UbicacionesEventos->setIdEvento($evento_id);
            if (!empty($_POST['pais']) ) {
                $pais = $DaoPaises->getByName($_POST['pais']);
                $UbicacionesEventos->setIdPais($pais->id);
            }
            if (!empty($_POST['estado']) ) {
                $estado = $DaoEstados->getByName($_POST['estado']);
                $UbicacionesEventos->setIdEstado($estado->id);
            }
            if (!empty($_POST['ciudad']) ) {
                $ciudad = $DaoLocalidades->getByName($_POST['ciudad']);
                $UbicacionesEventos->setIdLocalidad($ciudad->id);
            }
            $ubicacionevento_id = $DaoUbicacionesEventos->add($UbicacionesEventos);
        }
    }

    /*
    * OBJETIVOS
    * @description: Se agregan objetivos
    * ================================
    */
    if ($evento_id > 0 ) {
        if (!empty($_POST["objetivos"])) {
            $arr_objetivo = $_POST['objetivos'];
            foreach ($arr_objetivo as $objetivo_id) {
                $DaoObjetivo->addObjetivoEvento($objetivo_id, $evento_id);
            }
        }
    }

    /*
    * NOTIFICACIONES
    * @description: Envío de notificaciones a amigos
    * ================================
    */
    if ($evento_id > 0 ) {
        $friends = $FriendData->getFriends($_SESSION["user_id"]);
        foreach ($friends as $friend) {
            $friend_id = ($friend->receptor_id !== $_SESSION["user_id"]) ? $friend->receptor_id : $friend->sender_id;

            // Crear notificación
            $notification = new NotificationData();
            $notification->not_type_id = 8; // creó
            $notification->type_id = 4; // evento
            $notification->ref_id = $evento_id; // =  id de evento
            $notification->receptor_id = $friend_id; // en este caso nos referimos a quien va dirigida la notificacion
            $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
            $notification->id = $notification->add();

            $notification->message = $notification->getMessageById($notification->id);
            $notification->updateMessage();
        }

        /*
        * COLABORACIONES
        * @description: Envío de notificaciones a colaboradores y creación de colaboraciones
        * ================================
        */
        if (!empty($_POST["colaboradores"])) {
            $colaboradores = $_POST["colaboradores"];
            foreach ($colaboradores as $colaborador) {

                 // Colaboración
                 $Colaboracion->setFechaCreado(date('Y-m-d'));
                 $Colaboracion->setEstatus(0);
                 $Colaboracion->setIniciativa_idIniciativa(0);
                 $Colaboracion->setEventoId($evento_id);
                 $org = $DaoOrganizacion->getByUserId($colaborador);
                 $Colaboracion->setUsuario_id($org->id);
                 $DaoColaboracion->add($Colaboracion);

                 // Crear notificación al colaborador
                 $notification = new NotificationData();
                 $notification->not_type_id = 5; // colaborar
                 $notification->type_id = 4; // evento
                 $notification->ref_id = $evento_id; // =  id de evento
                 $notification->receptor_id = $colaborador; // en este caso nos referimos a quien va dirigida la notificacion
                 $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                 $notification->id = $notification->add();

                 $notification->message = $notification->getMessageById($notification->id);
                 $notification->updateMessage();
            }
        }

        print_r(json_encode(array('id' => $evento_id, 'status' => true, 'msg' =>  translate('label guardarEvento')."!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => translate('label errorGuardar') )));
    }
    break;

case "byId":
    $event = $DaoEvento->getById($_POST['id']);
    print_r(json_encode($event));
    break;

case "addFav":
    $Likes = new HeartData();
    $Likes->type_id = $_POST['type'];
    $Likes->ref_id = $_POST['post'];
    $Likes->user_id = $_SESSION['user_id'];
    $response = $Likes->add();
    if ($response[0] == 1 ) {
        print_r(json_encode(array('status' => true , 'msg' => translate('Like correcto'))));
    } else {
        print_r(json_encode(array('status' => false , 'msg' => translate('Like correcto'))));
    }
    break;

case "filterEvent":
    $type = $_POST['type'];
    $event_type = $_POST['event_type'];
    $event_date = $_POST['event_date'];
    $event_name = $_POST['event_name'];
    $event = $DaoEvento->filterEvents($type, $event_type, $event_date, $event_name);
    json(
        array(
        'type' => $type,
        'data' => $event
        )
    );
    break;
}
