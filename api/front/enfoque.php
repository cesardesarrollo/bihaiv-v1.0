<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
//require_once('../../core/modules/index/model/FocusData.php');
require_once '../../core/lang/lang.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DTO/Publicaciones.php';


require_once '../..//core/modules/index/model/DaoEnfoque.php';
require_once '../..//core/modules/index/model/DTO/Enfoque.php';

require_once '../..//core/modules/index/model/DaoOrganizacion.php';
require_once '../..//core/modules/index/model/DTO/Organizacion.php';

$DaoPublicaciones= new DaoPublicaciones();
$Publicaciones = new Publicaciones();

$DaoEnfoque= new DaoEnfoque();
$Enfoque = new Enfoque();

$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$action = $_POST['method'];
$tablename = "enfoque";

//$FocusData = new FocusData();

switch($action){

case "all":
    $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
    /* HERE COMES NEW CHALLENGER ---------------------------------------------- */
    $query = "SELECT
          p.id AS _idPublicacion,
          p.fechaCreado AS _fechaCreado,
          p.user_id AS _idUser,
          p.tipo AS _tipo,
          e.idEnfoque AS _idEnfoque,
          e.texto AS _texto,
          IFNULL(ROUND(AVG(ce.calificacion)), 0) AS _calificacion,
          e.post_id AS _idPost,
          (IF(DATE_ADD(p.fechaCreado,
                  INTERVAL 1 MINUTE) >= p.fechaCreado,
              'true',
              'false')) AS _statusPerCreationDate,
          (if (DATE_ADD(p.fechaCreado, INTERVAL 1 DAY) > DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s'),
              'true',
              'false')) AS _statusPerDay
      FROM
          publicaciones AS p
              INNER JOIN
          enfoque AS e ON p.id = e.post_id
              LEFT JOIN
          calificacionenfoque ce ON ce.enfoque_idEnfoque = e.idEnfoque
      WHERE
          p.tipo = 1 AND p.user_id = $org->id
      GROUP BY _idPublicacion
      ORDER BY p.id DESC
    LIMIT 0 , 5";
    $response = $DaoPublicaciones -> getAllRows($query);
    foreach ($response as $k => $v) {

        $stars = '';
        if (isset($_SESSION['user_id'])) {

            if ($_POST['user_id'] == $_SESSION['user_id']) {
                /*
                * Mis aportaciones, en este caso la calificacion es solo informativa
                * por lo tanto no puedo calificar mis aportaciones.
                */
                // strellas
                for ($i = 1; $i <= 5 ; $i++) {
                    if ($v['_calificacion'] == 0) {
                        $stars .= '<i style="color: orange;" class="fa fa-star-o margin-left-sm" aria-hidden="true"></i>';
                    } elseif ($v['_calificacion'] < $i) {
                        $stars .= '<i style="color: orange;" class="fa fa-star-o margin-left-sm" aria-hidden="true"></i>';
                    } else {
                        $stars .= '<i style="color: orange;" class="fa fa-star margin-left-sm" aria-hidden="true"></i>';
                    }
                }
            } else {
                /*
                * Las aportaciones pertenecen a otro actor por lo tanto puedo
                * calificarlas.
                */
                $stars = '<div style="top: -20px !important;" class="rateYo-enfoques" data-idenfoque="'.$v['_idEnfoque'].'"></div>';
            }
        
        }

        $response[$k]['_stars'] = $stars;
    }
    if (count($response)>0) {
        echo json_encode($response);
    } else {    
        echo "<h3 class=\"text-center\">".translate('label noEnfoques')."</h3>";
    }
    break;

case "delete":
    break;

case "edit":
    $Enfoque->setTexto($_POST['descripcion']);
    $Enfoque->setPost_idPost($_POST['post_id']);
    $Enfoque->setId($_POST['id']);
    $response = $DaoEnfoque->update($Enfoque);
    if ($response != 0) {
        print_r(json_encode(array('status' => true, 'msg' => translate('label editarEnfoque'))));
    } else {
        print_r(json_encode(array('status' => false, 'msg' =>translate('label errorGenerico'))));
    }
    break;

case "save":
    //GET ORGANIZATION ID
    $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    $Publicaciones->setFechaCreado(date('Y-m-d H:i:s'));
    $Publicaciones->setTipo($_POST['tipo']);
    $Publicaciones->setUser_id($org->id);

    $Publicaciones->setIsPublic(0);
    $Publicaciones->setByExpert(0);
    $Publicaciones->setByAdmin(0);

    $id = $DaoPublicaciones->add($Publicaciones);
    if ($id>0) {
        $Enfoque->setTexto($_POST['descripcion']);
        $Enfoque->setTipoEnfoque($_POST['tipo_enfoque']);
        $Enfoque->setPost_idPost($id);
        $response = $DaoEnfoque->add($Enfoque);
    }
    print_r(json_encode(array('status' => true, 'msg' => translate('label guardarEnfoque'), 'data'=>$response)));

    break;

case "getTotal":
    //GET ORGANIZATION ID
    $org = $DaoOrganizacion->getByUserId($_SESSION['user_id']);
    //GET TOTAL FOCUS
    $queryTotalFocus = "select count(idEnfoque) as total
      from enfoque e inner join publicaciones p on e.post_id = p.id
      where p.user_id = " . $org->id;
    $totalFocus = $DaoPublicaciones->advancedQuery($queryTotalFocus);
    if ($totalFocus[0]['total'] >= 5 ) {
        print_r(json_encode(array('status' => false, 'msg'=>translate('label error5enfoques'))));
    } else {
        print_r(json_encode(array('status' => ture, 'msg'=>'')));
    }
    break;

case "byId":
    echo json_encode($DaoEnfoque->getById($_POST['id']));
    break;
case "update_rating":
    $toSend = $_POST;
    $org = $DaoOrganizacion->getByUserId($_POST['user_id']);
    $toSend['organizacion_id'] = $org -> id;
    $id = $DaoEnfoque -> updateRating($toSend);
    if ($id > 0 ) {
        print_r(
            json_encode(
                array(
                'status'      => true,
                'msg'         => translate('label guardarCal'),
                'class'       => 'success'
                )
            )
        );
    } else {
        print_r(
            json_encode(
                array(
                'status'  => false,
                'msg'     => translate('label errorcalificacionenfoque'),
                'class'   => 'warning'
                )
            )
        );
    }
    break;

case "get_tipo_enfoque":
    $DaoEnfoque -> getListTipoEnfoque();
    json($DaoEnfoque -> response);
    break;
}
