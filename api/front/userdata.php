<?php
if (!isset($_SESSION)) {
    session_start();
}
  require_once '../../core/autoload.php';
  require_once '../../core/app/debuggin.php';
  require_once '../../core/modules/index/model/UserData.php';
  require_once '../../core/modules/index/model/FriendData.php';
  require_once '../../core/modules/index/model/DaoOauth.php';

  $method = $_POST['method'];

if ($method === 'getMyFriendsToVinculate') {

    $vincularA = $_SESSION["user_id"];
    if (isset($_POST['vincularA'])) {
        $vincularA = $_POST['vincularA'];
    }

    $frs = FriendData::getFriends($_SESSION["user_id"]);
    $my_friends = array();

    foreach ($frs as $fr):

        if ($fr->receptor_id == $_SESSION["user_id"]) {
            if ($fr->sender_id != $vincularA) {

                $amistad = FriendData::getFriendship($vincularA, $fr->sender_id);
                // Si no hay una amistad
                if (!is_object($amistad)) {

                    $new_array = array("user_id" => $fr->sender_id, "name" => $fr->getSender()->getFullname());
                    array_push($my_friends, $new_array);

                }

            }
        } else {
            if ($fr->receptor_id != $vincularA) {

                $amistad = FriendData::getFriendship($vincularA, $fr->receptor_id);
                // Si no hay una amistad
                if (!is_object($amistad)) {

                    $new_array = array("user_id" => $fr->receptor_id, "name" => $fr->getReceptor()->getFullname());
                    array_push($my_friends, $new_array);

                }
            }
        }
      
    endforeach;
    print_r(json_encode($my_friends));
}


if ($method === 'getMyFriends') {

    $frs = FriendData::getFriends($_SESSION["user_id"]);
    $my_friends = array();

    $vincularA = $_SESSION["user_id"];
    if (isset($_POST['vincularA'])) {
        $vincularA = $_POST['vincularA'];
    }

    foreach ($frs as $fr):
        if ($fr->receptor_id == $_SESSION["user_id"]) {
            if ($fr->sender_id != $vincularA) {
                array_push($my_friends, $fr->getSender()->getFullname() . "-" . $fr->sender_id);
            }
        } else {
            if ($fr->receptor_id != $vincularA) {
                array_push($my_friends, $fr->getReceptor()->getFullname() . "-" . $fr->receptor_id);
            }
        }
    endforeach;
    print_r(json_encode($my_friends));
}

  
elseif ($method === 'getMyFriends_new') {
    $frs = FriendData::getFriends($_SESSION["user_id"]);
    $my_friends = array();
    $vincularA = $_SESSION["user_id"];
    if (isset($_POST['vincularA'])) {
        $vincularA = $_POST['vincularA'];
    }
    foreach ($frs as $fr):
        if ($fr->receptor_id == $_SESSION["user_id"]) {
            if ($fr->sender_id != $vincularA) {
                array_push($my_friends, array('name' => $fr->getSender()->getFullname(), 'id' => $fr->sender_id));
            }
        } else {
            if ($fr->receptor_id != $vincularA) {
                array_push($my_friends, array('name' => $fr->getReceptor()->getFullname(), 'id' => $fr->receptor_id));
            }
        }
    endforeach;
    json($my_friends);
}
elseif ($method === 'linkTwitterAccount' ) {

    $twitter = Core::initTwitterAPI();

    // get the request token
    $reply = $twitter->oauth_requestToken(
        [
        'oauth_callback' => APP_PATH . '?view=editinformation'
        ]
    );

    // store the token
    $twitter->setToken($reply->oauth_token, $reply->oauth_token_secret);
    $_SESSION['oauth_token'] = $reply->oauth_token;
    $_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;
    $_SESSION['oauth_verify'] = true;

    // redirect to auth website
    $auth_url = $twitter->oauth_authorize();
    
    print_r(json_encode(array( 'auth_url' => $auth_url )));
} elseif ($method === 'unlinkTwitterAccount' ) {
    if (isset($_POST['id']) && !empty($_POST['id']) && $_POST['id'] > 0) {
        $DaoOauth = new DaoOauth();
        $oauths   = $DaoOauth->getOauthsByIdUsu($_POST['id']);

        foreach ($oauths as $oauth) {
            if ($oauth->getServicio() == 'Twitter') {
                $oauth->setAccessToken('');
                $oauth->setAccessTokenSecret('');
                $oauth->setUrlRed('');

                $DaoOauth->update($oauth);
            }
        }
    }
    
    $auth_url = APP_PATH . '?view=editinformation';
    
    print_r(json_encode(array( 'auth_url' => $auth_url )));
}
