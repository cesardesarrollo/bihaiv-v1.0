<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoLocalidades.php';

$DaoLocalidades = new DaoLocalidades();
$Localidades = new Localidades();

$action = $_POST['method'];
$tablename = "localidades";
//$FocusData = new FocusData();
switch($action){

case "all":
    echo json_encode($DaoLocalidades->getAll());
    break;

case "delete":
    break;

case "edit":
    break;

case "save":
    break;

case "byMunicipioId":
    echo json_encode($DaoLocalidades->getByMunicipioId($_POST['id']));
    break;

case "byId":
    echo json_encode($DaoLocalidades->getById($_POST['id']));
    break;

}
