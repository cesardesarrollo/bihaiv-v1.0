<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoEstados.php';

$DaoEstados = new DaoEstados();
$Estados = new Estados();

$action = $_POST['method'];
$tablename = "estados";

//$FocusData = new FocusData();

switch($action){

case "all":
    echo json_encode($DaoEstados->getAll());
    break;

case "delete":
    break;


case "edit":
    break;

case "save":
    break;

case "byPaisId":
    echo json_encode($DaoEstados->getByPaisId($_POST['id']));
    break;

case "byId":
    echo json_encode($DaoEstados->getById($_POST['id']));
    break;

}
