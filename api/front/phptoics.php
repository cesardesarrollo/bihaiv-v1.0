<?php
require_once '../../core/app/debuggin.php';
require_once '../../core/controller/Core.php';
date_default_timezone_set('America/Mexico_City');
define('DATE_ICAL', 'Ymd\THis\Z');
$summary     = $_POST['summary'];
$datestart   = fecha_job($_POST['datestart']);
$dateend     = fecha_job($_POST['dateend']);
$address     = $_POST['address'];
$uri         = $_POST['uri'];
$description = $_POST['description_cal'];
$filename    = $_POST['filename'];

function fecha_job($str)
{
    $unix = strtotime($str);
    return  $unix;
}

function getFecha($Fecha)
{
    $Y=date("Y", strtotime($Fecha));
    $m=date("m", strtotime($Fecha));
    $d=date("d", strtotime($Fecha));
    $h=date("H", strtotime($Fecha));
    $i=date("i", strtotime($Fecha));
    $s=date("s", strtotime($Fecha));
    $fechaNueva=mktime($h, $i, $s, $m, $d, $Y);
    return $fechaNueva;
}

function dateToCal($timestamp) 
{
    $date = gmdate('Ymd\THis\Z', $timestamp);
    return $date;
}

function escapeString($string) 
{
    return preg_replace('/([\,;])/', '\\\$1', $string);
}

header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $filename);

echo "BEGIN:VCALENDAR"."\n";
echo "VERSION:2.0"."\n";
echo "PRODID:-//hacksw/handcal//NONSGML v1.0//EN"."\n";
echo "CALSCALE:GREGORIAN"."\n";
echo "BEGIN:VEVENT"."\n";
echo "DTSTART:". dateToCal($datestart)."\n";
echo "DTEND:". dateToCal($dateend)."\n";
echo "UID:". uniqid()."\n";
echo "DTSTAMP:". dateToCal(time())."\n";
echo "LOCATION:". escapeString($address)."\n";
echo "DESCRIPTION:". escapeString($description)."\n";
echo "RL;VALUE=URI". escapeString($uri)."\n";
echo "SUMMARY:".escapeString($summary)."\n";
echo "END:VEVENT"."\n";
echo "END:VCALENDAR"."\n";
?>
