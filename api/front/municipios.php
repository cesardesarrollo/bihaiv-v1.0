<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoMunicipios.php';

$DaoMunicipios = new DaoMunicipios();
$Municipios = new Municipios();

$action = $_POST['method'];
$tablename = "municipios";

//$FocusData = new FocusData();

switch($action){

case "all":
    echo json_encode($DaoMunicipios->getAll());
    break;

case "delete":
    break;


case "edit":
    break;

case "save":
    break;

case "byEstadoId":
    echo json_encode($DaoMunicipios->getByEstadoId($_POST['id']));
    break;

case "byId":
    echo json_encode($DaoMunicipios->getById($_POST['id']));
    break;

}
