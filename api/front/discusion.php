<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/app/defines.php';
require_once '../../core/modules/index/model/DaoDiscusion.php';
require_once '../../core/modules/index/model/UserData.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoRoles.php';
require_once '../../core/modules/index/model/DaoComment.php';
require_once '../../core/modules/index/model/FriendData.php';
require_once '../../core/modules/index/model/NotificationData.php';
require_once '../../core/lang/lang.php';

$DaoDiscusion = new DaoDiscusion();
$Discusion = new Discusion();

$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = new Organizacion();

$FriendData = new FriendData();

// Coments
$DaoComment = new DaoComment();
$Comment = new Comment();

$DaoRoles = new DaoRoles();

$action = $_POST['method'];
$tablename = "tema";

switch($action){

  // Son los nuevos
case "all":
    $discusiones  = $DaoDiscusion -> getAll();
    $arr_nuevos = array();
    foreach ($discusiones as $discusion) {
        $Organizacion = $DaoOrganizacion->getById($discusion['organizacion_id']);
        $promedio_votacion = $discusion['calificacion'];
        $user = UserData::getById($Organizacion -> Usuarios_idUsuarios);
        // strellas
        $stars = '';
        for ($i = 1; $i <= 5 ; $i++) {
            if ($promedio_votacion == 0) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif ($promedio_votacion < $i) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } else {
                $stars .= '<i style="color: yellow;" class="fa fa-star" aria-hidden="true"></i>';
            }
        }
        $array_push = array(
        "idTema"      => $discusion['id'],
        "privacidad"  => ($discusion['privacidad'] === 'PUBLICO') ? 'true' : '',
        "user_name"   => $user->name,
        "user_avatar" => 'storage/users/'.$Organizacion -> Usuarios_idUsuarios.'/profile/'.$Organizacion -> avatar,
        "created_at"  => $discusion['created_at'],
        "titulo"      => $discusion['titulo'],
        "descripcion" => $discusion['descripcion'],
        'voted_average' => $promedio_votacion,
        'stars'         => $stars
        );
        if ($discusion['privacidad'] === 'PUBLICO') {
            array_push($arr_nuevos,  $array_push);
        } else if (isset($_SESSION['user_id']) && ($discusion['privacidad'] === 'PRIVADO')) {
            $UserOrganizacion = $DaoOrganizacion->getByUserId($_SESSION['user_id']);

            // Se revisa si está en lista de invitados
            $invitado = $DaoDiscusion->IsInvitedByTemaAndOrgId($discusion['id'], $UserOrganizacion->id);
            if ($invitado) {
                array_push($arr_nuevos,  $array_push);
            } else {
                // Si es tema propio
                $isMine = $DaoDiscusion->IsCreatedByMe($discusion['id'], $UserOrganizacion->id);
                if ($isMine) {
                    array_push($arr_nuevos,  $array_push);
                }
            }

        }
    }

    print_r(json_encode($arr_nuevos));

    break;

case "all_most_visited":
    $discusiones  = $DaoDiscusion -> getAllMostVisited();
    $arr_nuevos = array();
    foreach ($discusiones as $discusion) {
        $Organizacion = $DaoOrganizacion->getById($discusion['organizacion_id']);
        $promedio_votacion = $discusion['calificacion'];
        $user = UserData::getById($Organizacion -> Usuarios_idUsuarios);
        // strellas
        $stars = '';
        for ($i = 1; $i <= 5 ; $i++) {
            if ($promedio_votacion == 0) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif ($promedio_votacion < $i) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } else {
                $stars .= '<i style="color: yellow;" class="fa fa-star" aria-hidden="true"></i>';
            }
        }
        $array_push = array(
        "idTema"      => $discusion['id'],
        "privacidad"  => ($discusion['privacidad'] === 'PUBLICO') ? 'true' : '',
        "user_name"   => $user->name,
        "user_avatar" => 'storage/users/'.$Organizacion -> Usuarios_idUsuarios.'/profile/'.$Organizacion -> avatar,
        "created_at"  => $discusion['created_at'],
        "titulo"      => $discusion['titulo'],
        "descripcion" => $discusion['descripcion'],
        'voted_average' => $promedio_votacion,
        'stars'         => $stars
        );
        if ($discusion['privacidad'] === 'PUBLICO') {
            array_push($arr_nuevos,  $array_push);
        } else if (isset($_SESSION['user_id']) && ($discusion['privacidad'] === 'PRIVADO')) {
            $UserOrganizacion = $DaoOrganizacion->getByUserId($_SESSION['user_id']);

            // Se revisa si está en lista de invitados
            $invitado = $DaoDiscusion->IsInvitedByTemaAndOrgId($discusion['id'], $UserOrganizacion->id);
            if ($invitado) {
                array_push($arr_nuevos,  $array_push);
            } else {
                // Si es tema propio
                $isMine = $DaoDiscusion->IsCreatedByMe($discusion['id'], $UserOrganizacion->id);
                if ($isMine) {
                    array_push($arr_nuevos,  $array_push);
                }
            }
        }
    }
    $arr_nuevos;
    print_r(json_encode($arr_nuevos));
    break;

case "all_most_voted":
    $discusiones  = $DaoDiscusion -> getAllMostVoted();
    $arr_nuevos = array();
    foreach ($discusiones as $discusion) {
        $Organizacion = $DaoOrganizacion->getById($discusion['organizacion_id']);
        $promedio_votacion = $discusion['calificacion'];
        $user = UserData::getById($Organizacion -> Usuarios_idUsuarios);
        // strellas
        $stars = '';
        for ($i = 1; $i <= 5 ; $i++) {
            if ($promedio_votacion == 0) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif ($promedio_votacion < $i) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } else {
                $stars .= '<i style="color: yellow;" class="fa fa-star" aria-hidden="true"></i>';
            }
        }
        $array_push = array(
        "idTema"        => $discusion['id'],
        "privacidad"    => ($discusion['privacidad'] === 'PUBLICO') ? 'true' : '',
        "user_name"     => $user->name,
        "user_avatar"   => 'storage/users/'.$Organizacion -> Usuarios_idUsuarios.'/profile/'.$Organizacion -> avatar,
        "created_at"    => $discusion['created_at'],
        "titulo"        => $discusion['titulo'],
        "descripcion"   => $discusion['descripcion'],
        'voted_average' => $promedio_votacion,
        'stars'         => $stars
        );
        if ($discusion['privacidad'] === 'PUBLICO') {
            array_push($arr_nuevos,  $array_push);
        } else if (isset($_SESSION['user_id']) && ($discusion['privacidad'] === 'PRIVADO')) {
            $UserOrganizacion = $DaoOrganizacion->getByUserId($_SESSION['user_id']);

            // Se revisa si está en lista de invitados
            $invitado = $DaoDiscusion->IsInvitedByTemaAndOrgId($discusion['id'], $UserOrganizacion->id);
            if ($invitado) {
                array_push($arr_nuevos,  $array_push);
            } else {
                // Si es tema propio
                $isMine = $DaoDiscusion->IsCreatedByMe($discusion['id'], $UserOrganizacion->id);
                if ($isMine) {
                    array_push($arr_nuevos,  $array_push);
                }
            }
        }
    }

    print_r(json_encode($arr_nuevos));
    break;

case "search_topics":
    // set vars
    $keywords         = $_POST['search'];
    $organizacion_id  = $_POST['organizacion_id'];
    // ResultSet
    $discusiones      = $DaoDiscusion -> getTopicSearch($keywords, $organizacion_id);
    $arr_nuevos       = array();
    foreach ($discusiones as $discusion) {
        $Organizacion = $DaoOrganizacion->getById($discusion['organizacion_id']);
        $promedio_votacion = $discusion['calificacion'];
        $user = UserData::getById($Organizacion -> Usuarios_idUsuarios);
        // strellas
        $stars = '';
        for ($i = 1; $i <= 5 ; $i++) {
            if ($promedio_votacion == 0) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif ($promedio_votacion < $i) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } else {
                $stars .= '<i style="color: yellow;" class="fa fa-star" aria-hidden="true"></i>';
            }
        }
        $array_push = array(
        "idTema"        => $discusion['id'],
        "privacidad"    => ($discusion['privacidad'] === 'PUBLICO') ? 'true' : '',
        "user_name"     => $user->name,
        "user_avatar"   => 'storage/users/'.$Organizacion -> Usuarios_idUsuarios.'/profile/'.$Organizacion -> avatar,
        "created_at"    => $discusion['created_at'],
        "titulo"        => $discusion['titulo'],
        "descripcion"   => $discusion['descripcion'],
        'voted_average' => $promedio_votacion,
        'stars'         => $stars
        );
        if ($discusion['privacidad'] === 'PUBLICO') {
            array_push($arr_nuevos,  $array_push);
        } else if (isset($_SESSION['user_id']) && ($discusion['privacidad'] === 'PRIVADO')) {

            // Se revisa si está en lista de invitados
            $invitado = $DaoDiscusion->IsInvitedByTemaAndOrgId($discusion['id'], $UserOrganizacion->id);
            if ($invitado) {
                array_push($arr_nuevos,  $array_push);
            } else {
                // Si es tema propio
                $isMine = $DaoDiscusion->IsCreatedByMe($discusion['id'], $UserOrganizacion->id);
                if ($isMine) {
                    array_push($arr_nuevos,  $array_push);
                }
            }
        }
    }
    print_r(json_encode($arr_nuevos));
    break;

case "all_my_topics":
    // set vars
    $organizacion_id  = $_POST['user_id'];
    // ResultSet
    $discusiones      = $DaoDiscusion -> getMyTopics($organizacion_id);
    $arr_nuevos       = array();
    foreach ($discusiones as $discusion) {
        $Organizacion = $DaoOrganizacion->getById($discusion['organizacion_id']);
        $promedio_votacion = $discusion['calificacion'];
        $user = UserData::getById($Organizacion -> Usuarios_idUsuarios);
        // strellas
        $stars = '';
        for ($i = 1; $i <= 5 ; $i++) {
            if ($promedio_votacion == 0) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } elseif ($promedio_votacion < $i) {
                $stars .= '<i style="color: yellow;" class="fa fa-star-o" aria-hidden="true"></i>';
            } else {
                $stars .= '<i style="color: yellow;" class="fa fa-star" aria-hidden="true"></i>';
            }
        }
        $array_push = array(
        "idTema"        => $discusion['id'],
        "privacidad"    => ($discusion['privacidad'] === 'PUBLICO') ? 'true' : '',
        "user_name"     => $user->name,
        "user_avatar"   => 'storage/users/'.$Organizacion -> Usuarios_idUsuarios.'/profile/'.$Organizacion -> avatar,
        "created_at"    => $discusion['created_at'],
        "titulo"        => $discusion['titulo'],
        "descripcion"   => $discusion['descripcion'],
        'voted_average' => $promedio_votacion,
        'stars'         => $stars
        );
        array_push($arr_nuevos,  $array_push);
    }
    
    print_r(json_encode($arr_nuevos));
    
    break;

case "delete":
    break;

case "edit":
    break;

case "save":

    $Discusion->setRoles($_POST['roles']);
    $Discusion->setTitulo($_POST['titulo']);
    $Discusion->setDescripcion($_POST['descripcion']);
    $Discusion->setPrivacidad($_POST['privacidad']);
    $Discusion->setUser_id($_POST['user_id']);
    $discusion_id = $DaoDiscusion -> add($Discusion);

    //  Envío de notificaciones
    if ($discusion_id > 0 ) {
        if ($_POST['privacidad'] === "PUBLICO" || ($_POST['privacidad'] === "PRIVADO" AND empty($_POST['invitados']))) {
            $organizacionesByRol = $DaoOrganizacion->getAllByRolId($_POST['roles']);
            if (count($organizacionesByRol)>0) {
                foreach ($organizacionesByRol as $organizacion) {
                    /*
                    * NOTIFICACIONES
                    * @description: Envío de notificaciones a actores de rol especificado
                    * ================================
                    */
                    $notification = new NotificationData();
                    $notification->not_type_id = 8; // creó
                    $notification->type_id = 9; // foro
                    $notification->ref_id = $discusion_id; // =  id de foro
                    $notification->receptor_id = $organizacion->Usuarios_idUsuarios; // a quien va dirigida la notificacion
                    $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                    $notification->id = $notification->add();

                    $notification->message = $notification->getMessageById($notification->id);
                    $notification->updateMessage();
                }
            }
        } else if ($_POST['privacidad'] === "PRIVADO" AND !empty($_POST['invitados'])) {
            foreach ($_POST['invitados'] as $user ){
                /*
                * NOTIFICACIONES
                * @description: Envío de notificaciones a actores invitados a foro privado
                * ================================
                */
                $notification = new NotificationData();
                $notification->not_type_id = 8; // creó
                $notification->type_id = 9; // foro
                $notification->ref_id = $discusion_id; // =  id de foro
                $notification->receptor_id = $user; // a quien va dirigida la notificacion
                $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                $notification->id = $notification->add();

                $notification->message = $notification->getMessageById($notification->id);
                $notification->updateMessage();
            }
        }
    }

    if ($discusion_id > 0 ) {
        /*
        * BUSCAR ACTORES INVITADOS Y MANDAR UN CORREO
        */
        if ($_POST['privacidad'] === 'PRIVADO' AND !empty($_POST['invitados']) ) {
            foreach ($_POST['invitados'] as $user ){
                $user = UserData::getById($user);
                $Organizacion = $DaoOrganizacion->getByUserId($user->id);
                $subject = "Bihaiv - Te han invitado a particiar en el foro";
                $body = '<h2> Hola ' . $user->name . '</h2>
                  <p>Has sido invitado a participar en un tema privado en Bihaiv</p>
                  <p>Para participar en el foro haz click en la siguiente liga:</p>
                  <p><a href="' . APP_PATH . 'invitacion/participacion_tema.php?org='.$Organizacion->id.'&tema='.$discusion_id.'">Aceptar invitación</a></p>';
                $mail_response = Core::mail($user -> email, $subject, $body, strip_tags($body));
            }
        }
        print_r(json_encode(array('status' => true, 'msg' => translate('label guardarDiscusion') )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => translate('label errorGuardarDiscusion') )));
    }
    break;

case "save_comment":
    $Comment -> setComentario($_POST['comentario']);
    $Comment -> setOrganizacion_id($_POST['organizacion_id']);
    $Comment -> setTema_id($_POST['tema_id']);

    $comment_id = $DaoComment -> add($Comment);

    if ($comment_id > 0 ) {

        $Discusion = $DaoDiscusion->getById($_POST['tema_id']);
        $invitados = $DaoDiscusion->getInvitadosByTemaId($_POST['tema_id']);

        if ($Discusion->privacidad === "PUBLICO" || ($Discusion->privacidad === "PRIVADO" AND empty($invitados))) {
            $organizacionesByRol = $DaoOrganizacion->getAllByRolId($Discusion->roles);
            if (count($organizacionesByRol)>0) {
                foreach ($organizacionesByRol as $organizacion) {
                    /*
                    * NOTIFICACIONES
                    * @description: Envío de notificaciones a actores de rol especificado
                    * ================================
                    */
                    $notification = new NotificationData();
                    $notification->not_type_id = 8; // creó
                    $notification->type_id = 9; // foro
                    $notification->ref_id = $discusion_id; // =  id de foro
                    $notification->receptor_id = $organizacion->Usuarios_idUsuarios; // a quien va dirigida la notificacion
                    $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                    $notification->id = $notification->add();

                    $notification->message = $notification->getMessageById($notification->id);
                    $notification->updateMessage();
                }
            }
        } else if ($Discusion->privacidad === "PRIVADO" AND !empty($invitados)) {
            foreach ($invitados as $organizacion_id ){

                $Organizacion = $DaoOrganizacion->getById($organizacion_id);
                /*
                * NOTIFICACIONES
                * @description: Envío de notificaciones a actores invitados a foro privado
                * ================================
                */
                $notification = new NotificationData();
                $notification->not_type_id = 8; // creó
                $notification->type_id = 9; // foro
                $notification->ref_id = $discusion_id; // =  id de foro
                $notification->receptor_id = $Organizacion->Usuarios_idUsuarios; // a quien va dirigida la notificacion
                $notification->sender_id = $_SESSION["user_id"]; // usuario implicado
                $notification->id = $notification->add();

                $notification->message = $notification->getMessageById($notification->id);
                $notification->updateMessage();
            }
        }
        print_r(
            json_encode(
                array(
                'status'      => true,
                'msg'         => translate('label guardarComentario')."!",
                'class'       => 'success',
                'comment_id'  => $comment_id,
                'comentario'  => $DaoDiscusion -> getCommentsByTopic($_POST['tema_id'])
                )
            )
        );
    } else {
        print_r(
            json_encode(
                array(
                'status'  => false,
                'msg'     => translate('label errorguardarComentario'),
                'class'   => 'danger'
                )
            )
        );
    }
    break;

case "rate_comment":
    $DaoComment -> rate_comment($_POST);
    json($DaoComment -> response);
    break;

case "add_visit":
    $comment_id = $DaoDiscusion -> addVisit($_POST);
    if ($comment_id > 0 ) {
        print_r(
            json_encode(
                array(
                'status'      => true,
                'msg'         => translate('label addvisita'),
                'class'       => 'success',
                )
            )
        );
    } else {
        print_r(
            json_encode(
                array(
                'status'  => false,
                'msg'     => translate('label erroraddvisita'),
                'class'   => 'danger'
                )
            )
        );
    }
    break;

case "update_rating":
    $id = $DaoComment -> updateRating($_POST);
    // breakpoint($id);
    if ($id > 0 ) {
        print_r(
            json_encode(
                array(
                'status'      => true,
                'msg'         => translate('label graciascalificar'),
                'class'       => 'success'
                )
            )
        );
    } else {
        print_r(
            json_encode(
                array(
                'status'  => false,
                'msg'     => translate('label erorrgraciascalificar'),
                'class'   => 'warning'
                )
            )
        );
    }
    break;

case "byId":
    if (isset($_POST['id'])) {
        $discusion = $DaoDiscusion -> getById($_POST['id']);

        /*
        * Lo mejor era editar el metodo getById para que en lugar de mandar un array
        * con muchos arreglos mandara un unico array asociativo, pero como no tengo idea
        * de donde mas lo utilizan puse este parche feo.
        */
        $discusion = $discusion[0];
        $Organizacion = $DaoOrganizacion -> getById($discusion['organizacion_id']);
        $user = UserData::getById($Organizacion -> Usuarios_idUsuarios);
        $Roles = $DaoRoles -> getById($Organizacion -> Roles_idRol);
        $array_tema = array(
        "idTema"            => $discusion['id'],
        "user_name"         => $user -> name,
        "user_avatar"       => 'storage/users/'.$user -> id.'/profile/'.$Organizacion -> avatar,
        "created_at"        => $discusion['created_at'],
        "tema_titulo"       => $discusion['titulo'],
        "tema_descripcion"  => $discusion['descripcion'],
        'rate_YO'           => '',
        'user_descripcion'  => $Organizacion -> quienSoy,
        'user_rol'          => $Roles -> nombre

        );

        if (isset($_SESSION['org_id']) && $_SESSION['org_id'] != $discusion['organizacion_id']) {
            /*
            * El tema del foro no me pertenece entonces puedo calificar
            */
            $array_tema['rate_YO'] = '<small>Califica este tema</small><div id="rateYo"></div>';
        }

        $arr_temas = array();
        array_push($arr_temas, $array_tema);
        // Aquí agregar los comentarios correspondientes al tema
        $arr_comentario = $DaoDiscusion -> getCommentsByTopic($discusion['id']);

        //Aquí agregar al arreglo comentarios
        $arr_discusion = array(
        'discusion'   => $arr_temas,
        'comentario'  => $arr_comentario
        );
        print_r(json_encode($arr_discusion));
    } else {
        print_r("Error");
    }
    break;

  // GET ONLY COMMENTS BY TOPIC
  // mas viejos
case "order_by_date_asc":
    if (isset($_POST['id'])) {
        // Aquí agregar los comentarios correspondientes al tema
        $arr_comentario = $DaoDiscusion -> getOnlyCommentsByTopicOld($_POST['id']);
        //Aquí agregar al arreglo comentarios
        $arr_discusion = array(
        'comentario'  => $arr_comentario
        );
        print_r(json_encode($arr_discusion));
    } else {
        print_r("Error");
    }
    break;
  // Mas nuevos
case "order_by_date_desc":
    if (isset($_POST['id'])) {
        // Aquí agregar los comentarios correspondientes al tema
        $arr_comentario = $DaoDiscusion -> getCommentsByTopic($_POST['id']);
        //Aquí agregar al arreglo comentarios
        $arr_discusion = array(
        'comentario'  => $arr_comentario
        );
        print_r(json_encode($arr_discusion));
    } else {
        print_r("Error");
    }
    break;
  // con mas likes
case "order_by_plus_likes":
    if (isset($_POST['id'])) {
        // Aquí agregar los comentarios correspondientes al tema
        $arr_comentario = $DaoDiscusion -> getOnlyCommentsByPlusLikes($_POST['id']);
        //Aquí agregar al arreglo comentarios
        $arr_discusion = array(
        'comentario'  => $arr_comentario
        );
        print_r(json_encode($arr_discusion));
    } else {
        print_r("Error");
    }
    break;
  // con menos likes
case "order_by_minus_likes":
    if (isset($_POST['id'])) {
        // Aquí agregar los comentarios correspondientes al tema
        $arr_comentario = $DaoDiscusion -> getOnlyCommentsByMinusLikes($_POST['id']);
        //Aquí agregar al arreglo comentarios
        $arr_discusion = array(
        'comentario'  => $arr_comentario
        );
        print_r(json_encode($arr_discusion));
    } else {
        print_r("Error");
    }
    break;

case "commment_by_id":
    if (isset($_POST['comment_id'])) {
        $comment = $DaoDiscusion -> getCommentsById($_POST['comment_id']);
        $status = (!empty($comment)) ? true : false;
        $comentario = array(
        'comentario'  => $comment[0],
        'status'      => $status
        );
        print_r(json_encode($comentario));
    }
    break;
}
