<?php
require_once '../../core/modules/index/model/DaoRedesExperto.php';
require_once '../../core/modules/index/model/DTO/RedesExperto.php';

$DaoRedesExperto=new DaoRedesExperto();
switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['nombre'])==0) {
        $Obj['error']="inserta nombre";
    } else if (strlen($_POST['url'])==0) {
        $Obj['error']="inserta url";      

    } else {

        $Obj= new RedesExperto();
        $Obj->setNombre($_POST['nombre']);
        $Obj->setUrl($_POST['url']);
        $id=$DaoRedesExperto->add($Obj);
                    
        $Obj=$DaoRedesExperto->getById($id);                    
    }

    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoRedesExperto->getById($_POST['id']);
        $Obj->setNombre($_POST['nombre']);
        $Obj->setUrl($_POST['url']);
        $DaoRedesExperto->update($Obj);

        $Obj=$DaoRedesExperto->getById($_POST['id']);

    } else {
                $Obj['error']="inserta _id";
    }
    break;
case 'byId':

           $Obj=$DaoRedesExperto->getById($_POST['id']);
    break;
case 'all':
           $Obj=$DaoRedesExperto->getAll($_POST['id']);
    break;
case 'delete':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoRedesExperto->delete($_POST['id']);
    } else {

        $Obj['error']="inserta id";
    }
    break;
case 'activos':
             $Obj=$DaoRedesExperto->getAll();
    break;
}
echo json_encode($Obj);
 
