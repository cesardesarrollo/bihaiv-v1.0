<?php
require_once '../../core/modules/index/model/DaoPregunta.php';
require_once '../../core/modules/index/model/DTO/Pregunta.php';
require_once '../../core/modules/index/model/DaoOpcionesRespuesta.php';

$DaoPregunta=new DaoPregunta();
$DaoOpcionesRespuesta=new DaoOpcionesRespuesta();

switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['pregunta'])==0) {
        $Obj['error']="inserta pregunta";
    } else {

            $Obj= new Pregunta();
            $Obj->setPregunta($_POST['pregunta']);
            $Obj->setCuestionario_idCuestionario($_POST['Cuestionario_idCuestionario']);
            $Obj->setOrden($DaoPregunta->getNetxOrden());
            $id=$DaoPregunta->add($Obj);

            $Obj=$DaoPregunta->getById($id);                    
    }
    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoPregunta->getById($_POST['id']);
        $Obj->setPregunta($_POST['pregunta']);
        $Obj->setOrden($_POST['orden']);
        $DaoPregunta->update($Obj);

        $Obj=$DaoPregunta->getById($_POST['id']);
    } else {
        $Obj['error']="inserta _id";
    }
    break;
case 'byId':

           $Obj=$DaoPregunta->getById($_POST['id']);
    break;
case 'all':
           $Obj=$DaoPregunta->getPreguntasCuestionario($_POST['id_cues']);
    break;
case 'delete':
    if (strlen($_POST['id'])>0) {
        $DaoOpcionesRespuesta->deleteByIdPre($_POST['id']);
        $DaoPregunta->delete($_POST['id']);
        $Obj= new Pregunta();
    } else {
        $Obj['error']="inserta id";
    }
    break;
}
echo json_encode($Obj);


 
