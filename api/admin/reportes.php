<?php
require_once '../../core/app/defines.php';
require_once '../../core/controller/Database.php';
require_once '../../core/controller/Executor.php';
require_once '../../core/controller/Model.php';
require_once '../../core/modules/index/model/ReportData.php';


$ReportData = new ReportData();
switch ($_POST['method']) {
case 'validate':
    $Obj = $ReportData->getById($_POST['id']);
    $Obj = $Obj->accept("Validado: ".date('Y-m-d H:i:s')." > ".$_POST['comments_response']);
    break;

case 'read':
    $Obj = $ReportData->getById($_POST['id']);
    $Obj = $Obj->read("Revisado: ".date('Y-m-d H:i:s')." > ".$_POST['comments_response']);
    break;

case 'delete':
    if (strlen($_POST['id'])>0) {
        $Obj = $ReportData->getById($_POST['id']);
        $Obj = $Obj->delete();
    } else {
        $Obj['error']="elimina id";
    }
    break;

}
echo json_encode($Obj);
 
