<?php
session_start();
require_once '../../core/autoload.php';
require_once '../../core/controller/Core.php';
require_once '../../core/controller/Database.php';
require_once '../../core/controller/Executor.php';
require_once '../../core/controller/Model.php';
require_once '../../core/modules/index/model/DTO/Publicaciones.php';
require_once '../../core/modules/index/model/DaoPublicaciones.php';
require_once '../../core/modules/index/model/DaoAportacion.php';
require_once '../../core/modules/index/model/DaoOrganizacion.php';
require_once '../../core/modules/index/model/DaoSeccionAportacion.php';
require_once '../../core/modules/index/model/UserData.php';

$UserData = new UserData();
$Publicaciones = new Publicaciones();
$DaoPublicaciones = new DaoPublicaciones();
$DaoAportacion = new DaoAportacion();
$Aportacion = new Aportacion();
$DaoOrganizacion = new DaoOrganizacion();
$DaoSeccionAportacion = new DaoSeccionAportacion();

switch ($_POST['method']) {
case 'save':
    
    $Publicaciones->setFechaCreado(date('Y-m-d H:i:s'));
    $Publicaciones->setTipo(3);
    $admin = $UserData->getAdmin();
    
    if (isset($_SESSION['experto_id']) &&  $_SESSION['experto_id'] > 0 ) {

        $Publicaciones -> setUser_id($_SESSION['experto_id']);
        $Publicaciones -> setByExpert(1);
        $Publicaciones -> setByAdmin(0);
        $Publicaciones -> setIsPublic(1);

    } else if (isset($_SESSION['is_admin'])) {

        $org    = $DaoOrganizacion -> getByUserId($admin->id);
        $Publicaciones -> setUser_id($org->id);
        $Publicaciones -> setByAdmin(1);
        $Publicaciones -> setByExpert(0);
        $Publicaciones -> setIsPublic(1);

    } else {
        print_r(json_encode(array('status' => false, 'msg' => "¡Error!" )));
        exit;
    }

    $id = $DaoPublicaciones->add($Publicaciones);
    if ($id>0) {
        $Aportacion->setFecha(date('Y-m-d H:i:s'));
        $Aportacion->setTitulo($_POST['titulo']);
        $Aportacion->setThumbnail($_POST['thumbnail']);
        $Aportacion->setFile($_POST['file']);
        $Aportacion->setCategory($_POST['categoria']);
        $Aportacion->setDescripcion($_POST['descripcion']);
        $Aportacion->setPost_idPost($id);

        $response = $DaoAportacion->add($Aportacion);
    }
    if ($response > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => "Tu aportación ha sido creada exitosamente!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => "Ah ocurrido un error intentando crear tu aportación, intenta más tarde." )));
    }
    break;

    /* Aplica solo para aportaciones de bihaiv */
case 'save_bihaiv':
    $Publicaciones -> setFechaCreado(date('Y-m-d H:i:s'));
    $Publicaciones -> setTipo(5);
    $admin  = $UserData -> getAdmin();

    if (isset($_SESSION['experto_id'])  &&  $_SESSION['experto_id'] > 0 ) {

        $Publicaciones -> setUser_id($_SESSION['experto_id']);
        $Publicaciones -> setByExpert(1);
        $Publicaciones -> setByAdmin(0);
        $Publicaciones -> setIsPublic(1);

    } else if (isset($_SESSION['is_admin']) ) {

        $org    = $DaoOrganizacion -> getByUserId($admin->id);
        $Publicaciones -> setUser_id($org->id);
        $Publicaciones -> setByAdmin(1);
        $Publicaciones -> setByExpert(0);
        $Publicaciones -> setIsPublic(1);

    } else {
        print_r(json_encode(array('status' => false, 'msg' => "¡Error!" )));
        exit;
    }


    $id = $DaoPublicaciones -> add($Publicaciones);
    if ($id > 0 ) {
        $Aportacion -> setFecha(date('Y-m-d H:i:s'));
        $Aportacion -> setTitulo($_POST['titulo']);
        $Aportacion -> setThumbnail($_POST['thumbnail']);
        $Aportacion -> setFile($_POST['file']);
        $Aportacion -> setCategory($_POST['categoria']);
        $Aportacion -> setDescripcion($_POST['descripcion']);
        $Aportacion -> setPost_idPost($id);
        $response = $DaoAportacion->add($Aportacion);
    }
    if ($response > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => "Tu aportación ha sido creada exitosamente!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => "Ah ocurrido un error intentando crear tu aportación, intenta más tarde." )));
    }
    break;

case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoAportacion->getById($_POST['id']);
        $Obj->setTitulo($_POST['titulo']);
        $Obj->setDescripcion($_POST['descripcion']);
        $Obj->setCategory($_POST['categoria']);
        $DaoAportacion->update($Obj);

        $Obj=$DaoAportacion->getById($_POST['id']);

    } else {
        $Obj['error']="inserta _id";
    }
    echo json_encode($Obj);

    break;

case 'byId':
    $Obj=$DaoAportacion->getById($_POST['id']);
    break;

case 'delete':
    if (strlen($_POST['id'])>0) {

        $Aportacion = $DaoAportacion->getById($_POST['id']);
        $idPublicacion = $Aportacion->getPost_idPost();
        $Obj=$DaoAportacion->delete($_POST['id']);
        $DaoPublicaciones->delete($idPublicacion);

    } else {
        $Obj['error']="elimina aportación";
    }

    echo json_encode($Obj);

    break;

case 'configuration':
    if (is_array($_POST['items'])) {
        foreach ($_POST['items'] as $item) {
            $Obj=$DaoSeccionAportacion->getById($item['id']);
            $activo = 0;
            if ($item['checked'] == 'checked') { $activo = 1;
            }

            $Obj->setActivo($activo);
            $DaoSeccionAportacion->update($Obj);
        }
    } else {
        $Obj['error']="arreglo inválido";
    }
    echo json_encode($Obj);

    break;
}
