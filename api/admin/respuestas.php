<?php
require_once '../../core/modules/index/model/DaoOpcionesRespuesta.php';
require_once '../../core/modules/index/model/DTO/OpcionesRespuesta.php';


$DaoOpcionesRespuesta=new DaoOpcionesRespuesta();

switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['texto'])==0) {
        $Obj['error']="inserta texto";
    } else if (strlen($_POST['rol'])==0) {
        $Obj['error']="inserta rol";      
    } else if (strlen($_POST['id_pre'])==0) {
        $Obj['error']="inserta id_pre";   
    } else {
        $Obj= new OpcionesRespuesta();
        $Obj->setTexto($_POST['texto']);
        $Obj->setPregunta_idPregunta($_POST['id_pre']);
        $Obj->setRoles_idRol($_POST['rol']);
        $Obj->setOrden($_POST['orden']);
        $id=$DaoOpcionesRespuesta->add($Obj);

        $Obj=$DaoOpcionesRespuesta->getById($id);   
    }
    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoOpcionesRespuesta->getById($_POST['id']);
        $Obj->setTexto($_POST['texto']);
        $Obj->setRoles_idRol($_POST['rol']);
        $Obj->setOrden($_POST['orden']);
        $DaoOpcionesRespuesta->update($Obj);

        $Obj=$DaoOpcionesRespuesta->getById($_POST['id']);
    } else {
        $Obj['error']="inserta _id";
    }
    break;
case 'byId':

           $Obj=$DaoOpcionesRespuesta->getById($_POST['id']);
    break;
case 'all':
           $Obj=$DaoOpcionesRespuesta->getOpcionesByIdPregunta($_POST['id_pre']);
    break;
case 'delete':
    if (strlen($_POST['id'])>0) {
        $DaoOpcionesRespuesta->delete($_POST['id']);
        $Obj= new Pregunta();
    } else {
        $Obj['error']="inserta id";
    }
    break;
}
echo json_encode($Obj);

 
