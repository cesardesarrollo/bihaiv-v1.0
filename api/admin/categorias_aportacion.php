<?php
require_once '../../core/autoload.php';
require_once '../../core/controller/Database.php';
require_once '../../core/controller/Executor.php';
require_once '../../core/controller/Model.php';
require_once '../../core/modules/index/model/DaoCategoriaAportacion.php';
require_once '../../core/modules/index/model/UserData.php';

$CategoriaAportacion = new CategoriaAportacion();
$DaoCategoriaAportacion = new DaoCategoriaAportacion();

switch ($_POST['method']) {
case 'save':
    $CategoriaAportacion->setTitulo($_POST['titulo']);
    $CategoriaAportacion->setIdPadre($_POST['idPadre']);
    $CategoriaAportacion->setIdSeccion($_POST['idSeccion']);
    $CategoriaAportacion->setActivo($_POST['activo']);

    $response = $DaoCategoriaAportacion->add($CategoriaAportacion);

    if ($response > 0 ) {
        print_r(json_encode(array('status' => true, 'msg' => "Tu categoría ha sido creada exitosamente!" )));
    } else {
        print_r(json_encode(array('status' => false, 'msg' => "Ha ocurrido un error intentando crear tu categoría, intenta más tarde." )));
    }

    break;

case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj = $DaoCategoriaAportacion->getById($_POST['id']);
        $Obj->setTitulo($_POST['titulo']);
        $Obj->setIdPadre($_POST['idPadre']);
        $Obj->setIdSeccion($_POST['idSeccion']);
        $Obj->setActivo($_POST['activo']);
        $DaoCategoriaAportacion->update($Obj);

        $Obj=$DaoCategoriaAportacion->getById($_POST['id']);

    } else {
        $Obj['error'] = "No fue posible guardar los cambios de la categoría";
    }

    echo json_encode($Obj);
 
    break;

case 'byId':
    $Obj = $DaoCategoriaAportacion->getById($_POST['id']);
    break;

case 'delete':
    if (strlen($_POST['id'])>0) {

        $Obj = $DaoCategoriaAportacion->delete($_POST['id']);

    } else {
        $Obj['error'] = "No fue posible eliminar la categoría";
    }

    echo json_encode($Obj);
 
    break;
}

