<?php
session_start();
require_once '../../core/modules/index/model/DaoImagenHome.php';
require_once '../../core/modules/index/model/DTO/ImagenHome.php';

$DaoImagenHome=new DaoImagenHome();
switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['titulo'])==0) {
        $Obj['error']="inserta título";
    }if (strlen($_POST['llave'])==0) {
        $Obj['error']="inserta ubicación";
    } else {

        $Obj= new ImagenHome();
        $Obj->setTitulo($_POST['titulo']);
        $Obj->setDescripcion($_POST['descripcion']);
        $Obj->setDescription($_POST['description']);
        $Obj->setLlaveImg($_POST['llave']);
        $Obj->setPortada(($_POST['portada']=='true')?1:0);
        $id=$DaoImagenHome->add($Obj);
            
        $Obj=$DaoImagenHome->getById($id);                    
    }

    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoImagenHome->getById($_POST['id']);
        $Obj->setTitulo($_POST['titulo']);
        $Obj->setDescripcion($_POST['descripcion']);
        $Obj->setDescription($_POST['description']);
        $Obj->setLlaveImg($_POST['llave']);
        $Obj->setPortada(($_POST['portada']=='true')?1:0);
        $DaoImagenHome->update($Obj);

        $Obj=$DaoImagenHome->getById($_POST['id']);

    } else {
        $Obj['error']="inserta _id";
    }
    break;
case 'byId':

    $Obj=$DaoImagenHome->getById($_POST['id']);
    break;
case 'all':
    $Obj=$DaoImagenHome->getAll();
    break;
case 'delete':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoImagenHome->getById($_POST['id']);
        $ruta="../../admin/files/".$Obj->getLlaveImg();
        if (file_exists($ruta)) {
            unlink($ruta);
            $DaoImagenHome->delete($_POST['id']);
        }
        $Obj=new ImagenHome();
    } else {
        $Obj['error']="inserta id";
    }
    break;
}
echo json_encode($Obj);
 
