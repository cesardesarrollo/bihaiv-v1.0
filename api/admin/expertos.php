<?php

require_once '../../core/autoload.php';
require_once '../../core/modules/index/model/DaoExperto.php';
require_once '../../core/controller/Database.php';
require_once '../../core/app/debuggin.php';
require_once '../../core/controller/Executor.php';
require_once '../../core/controller/Model.php';
require_once '../../core/modules/index/model/UserData.php';
$UserData = new UserData();
$DaoExperto = new DaoExperto();


switch ($_POST['method']) {

    
case 'getRepeatedEmail':

    if (strlen($_POST['email']) == 0) {
        $Obj['error'] = "ingresa email";
    } else {
        $Obj = $UserData->isRepeatedEmail($_POST['email']);
    }

    break;

// Modifica email de administrador, siendo este su usuario de acceso al sistema y el destino de envío de formularios de solicitudes de aspirantes a expetos bihaiv.
case 'update_admin_email':

    if (strlen($_POST['email']) == 0) {
        $Obj['error'] = "actualiza email";
    } else {

        $UserData->email = $_POST['email'];
        $admin = $UserData->getAdmin();
        $UserData->id = $admin->id;
        $Obj = $UserData->update_email();
    }

    break;

case 'save':

    if (strlen($_POST['nombre']) == 0) {
        $Obj['error'] = "inserta nombre";
    } else if (strlen($_POST['email']) == 0) {
        $Obj['error'] = "inserta email";
    } else {

        $UserData->name = $_POST['nombre'];
        $UserData->email = $_POST['email'];
        $UserData->is_valid = "1";
        $UserData->is_active = "1";
        $UserData->tipoRel = "2";
        $user = $UserData->addUser();

        if ($user['1'] > 0) {
            $UserData->id=$user['1'];
            $Obj = new Experto();
            $Obj->setUsuarios_idUsuarios($user['1']);

            if (isset($_POST['password']) && strlen($_POST['password'])>0) {
                 $UserData->id=$user['1'];
                 $UserData->password = sha1(md5($_POST["password"]));
                 $UserData->update_passwd();
                 $UserData->activate();
            }
            if (isset($_POST['rutaimagen']) && strlen($_POST['rutaimagen'])>0) {
                 $UserData->imagen=$_POST['rutaimagen'];
                 $UserData->updateImagenUser();
            }
            if (isset($_POST['ubicacion'])) {
                $Obj->setUbicacion($_POST['ubicacion']);
            }
            if (isset($_POST['organizacion'])) {
                $Obj->setOrganizacion($_POST['organizacion']);
            }
            if (isset($_POST['tipoOrganizacion'])) {
                $Obj->setTipoOrganizacion($_POST['tipoOrganizacion']);
            }
            if (isset($_POST['ramoExperiencia'])) {
                $Obj->setRangoExperiencia($_POST['ramoExperiencia']);
            }
            if (isset($_POST['aniosExperiencia'])) {
                $Obj->setAniosExperiencia($_POST['aniosExperiencia']);
            }
            if (isset($_POST['cargoActual'])) {
                $Obj->setCargoActual($_POST['cargoActual']);
            }
            if (isset($_POST['cargoAnterior'])) {
                $Obj->setCargoAnterior($_POST['cargoAnterior']);
            }
            if (isset($_POST['bibliografia'])) {
                $Obj->setBibliografia($_POST['bibliografia']);
            }
            if (isset($_POST['nonce'])) {
                $nonce=$DaoExperto->generarKey();
                $Obj->setNonce($nonce);
            }
            if (isset($_POST['acepto'])) {
                $Obj->setAcepto($_POST['acepto']);
            }
            if (isset($_POST['lat'])) {
                $Obj->setLat($_POST['lat']);
            }
            if (isset($_POST['lng'])) {
                $Obj->setLng($_POST['lng']);
            }
            if (isset($_POST['telefono'])) {
                $Obj->setTelefono($_POST['telefono']);
            }
            $id=$DaoExperto->add($Obj);
            $Obj=$DaoExperto->getById($id);

        } else {
            $Obj['error'] = "Ha ocurrido un problema al crear el usuario, es posible que se deba a que el email ingresado ya existe en la base de datos, intente con otro.";
        }
    }

    break;

case 'edit':

    if (strlen($_POST['id']) > 0) {

        $Obj = $DaoExperto->getById($_POST['id']);
        $UserData->name = $_POST['nombre'];
        $UserData->email = $_POST['email'];

        $UserData->is_active = "1";
        $UserData->is_valid = "1";

        $UserData->id=$Obj->getUsuarios_idUsuarios();
        $UserData->updateUser();

        if (isset($_POST['password']) && strlen($_POST['password'])>0) {
            $UserData->password = sha1(md5($_POST["password"]));
            $UserData->update_passwd();
        }
        if (isset($_POST['rutaimagen']) && strlen($_POST['rutaimagen'])>0) {
             $UserData->imagen=$_POST['rutaimagen'];
             $UserData->updateImagenUser();
        }
        if (isset($_POST['ubicacion'])) {
            $Obj->setUbicacion($_POST['ubicacion']);
        }
        if (isset($_POST['organizacion'])) {
            $Obj->setOrganizacion($_POST['organizacion']);
        }
        if (isset($_POST['tipoOrganizacion'])) {
            $Obj->setTipoOrganizacion($_POST['tipoOrganizacion']);
        }
        if (isset($_POST['ramoExperiencia'])) {
            $Obj->setRangoExperiencia($_POST['ramoExperiencia']);
        }
        if (isset($_POST['aniosExperiencia'])) {
            $Obj->setAniosExperiencia($_POST['aniosExperiencia']);
        }
        if (isset($_POST['cargoActual'])) {
            $Obj->setCargoActual($_POST['cargoActual']);
        }
        if (isset($_POST['cargoAnterior'])) {
            $Obj->setCargoAnterior($_POST['cargoAnterior']);
        }
        if (isset($_POST['bibliografia'])) {
            $Obj->setBibliografia($_POST['bibliografia']);
        }
        if (isset($_POST['nonce'])) {
            $Obj->setNonce($_POST['nonce']);
        }
        if (isset($_POST['acepto'])) {
            $Obj->setAcepto($_POST['acepto']);
        }
        if (isset($_POST['lat'])) {
            $Obj->setLat($_POST['lat']);
        }
        if (isset($_POST['lng'])) {
            $Obj->setLng($_POST['lng']);
        }
        if (isset($_POST['telefono'])) {
            $Obj->setTelefono($_POST['telefono']);
        }
        $DaoExperto->update($Obj);
    } else {
        $Obj['error'] = "inserta _id";
    }
    break;
case 'byId':

       $Obj=$DaoExperto->getById($_POST['id']);
    break;

case 'byUserId':

       $Obj=$DaoExperto->getByUserId($_POST['id']);
    break;

case 'all':

        $Obj=$DaoExperto->getAll();
    break;
    
case 'delete':

    if (strlen($_POST['id']) > 0) {
        $Obj = $DaoExperto->getById($_POST['id']);
        $UserData->disable($Obj->getUsuarios_idUsuarios());
    } else {
        $Obj['error'] = "inserta id";
    }
    break;
}
echo json_encode($Obj);
