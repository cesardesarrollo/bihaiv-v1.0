<?php
require_once '../../core/modules/index/model/DaoRoles.php';
require_once '../../core/modules/index/model/DTO/Roles.php';

$DaoRoles=new DaoRoles();
switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['nombre'])==0) {
        $Obj['error']="inserta nombre";
    } else if (strlen($_POST['descripcion'])==0) {
        $Obj['error']="inserta descripción";      

    } else {

        $Obj= new Roles();
        $Obj->setNombre($_POST['nombre']);
        $Obj->setDescripcion($_POST['descripcion']);
        //$Obj->setDescripcionCorta($_POST['descripcion_corta']);
        $Obj->setDescription($_POST['description']);
        //$Obj->setShortDescription($_POST['short_description']);
        $Obj->setActivo($_POST['activo']);
        $Obj->setImagen($_POST['imagen']);
        $id=$DaoRoles->add($Obj);
        $Obj=$DaoRoles->getById($id);                    
    }

    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoRoles->getById($_POST['id']);
        $Obj->setNombre($_POST['nombre']);
        $Obj->setDescripcion($_POST['descripcion']);
        //$Obj->setDescripcionCorta($_POST['descripcion_corta']);
        $Obj->setDescription($_POST['description']);
        //$Obj->setShortDescription($_POST['short_description']);
        $Obj->setActivo($_POST['activo']);
        $Obj->setImagen($_POST['imagen']);
        $DaoRoles->update($Obj);

        $Obj=$DaoRoles->getById($_POST['id']);

    } else {
            $Obj['error']="inserta _id";
    }
    break;
case 'byId':

           $Obj=$DaoRoles->getById($_POST['id']);
    break;
case 'all':
           $Obj=$DaoRoles->getRoles();
    break;
case 'delete':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoRoles->delete($_POST['id']);
    } else {

        $Obj['error']="elimina id";
    }
    break;
case 'activos':
             $Obj=$DaoRoles->getAll();
    break;
}
echo json_encode($Obj);
 
