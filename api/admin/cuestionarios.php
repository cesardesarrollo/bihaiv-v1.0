<?php
require_once '../../core/app/debuggin.php';
require_once '../../core/modules/index/model/DaoCuestionario.php';
require_once '../../core/modules/index/model/DaoPregunta.php';
require_once '../../core/modules/index/model/DaoOpcionesRespuesta.php';

$DaoCuestionario = new DaoCuestionario();
$DaoPregunta = new DaoPregunta();
$DaoOpcionesRespuesta = new DaoOpcionesRespuesta();
switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['nombre'])==0) {
        $Obj['error']="inserta nombre";
    } else if (strlen($_POST['nivel'])==0) {
        $Obj['error']="inserta nivel";
    } else {

        $Obj= new Cuestionario();
        $Obj->setNombre($_POST['nombre']);
        $Obj->setFechaCreacion(date('Y-m-d H:i:s'));
        $Obj->setNivel($_POST['nivel']);
        $Obj->setActivo(1);
        $id=$DaoCuestionario->add($Obj);

        $Obj=$DaoCuestionario->getById($id);
    }
    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoCuestionario->getById($_POST['id']);
        $Obj->setNombre($_POST['nombre']);
        $Obj->setNivel($_POST['nivel']);
        $Obj->setActivo($_POST['activo']);

        if ($Obj->getNivel() != 1) {
            $preguntas = $DaoCuestionario -> getPreguntasCuestionario($Obj->id);
                      
            foreach ($preguntas as $k => $v) {
                $DaoCuestionario -> getOpcionesRespuestaAndDelete($v['idPregunta']);
                $DaoCuestionario -> deletePregunta($v['idPregunta']);
            }
        }

        $DaoCuestionario->update($Obj);

        $Obj=$DaoCuestionario->getById($_POST['id']);

    } else {
        $Obj['error']="inserta _id";
    }
    break;
case 'byId':

           $Obj=$DaoCuestionario->getById($_POST['id']);
    break;
case 'all':
           $Obj=$DaoCuestionario->getAll();
    break;
case 'delete':
    if (is_numeric($_POST['id']) AND $_POST['id']  > 0) {
        // Falta implementar que se guarde un historial de cuestionarios/preguntas/respuestas
        $Cuestionario = $DaoCuestionario->getById($_POST['id']);
        //Buscar cuestionarios Usuarios
        $cuestionariosUsuario = $DaoCuestionario -> getCuestionariosUsuario($Cuestionario->id);
        // buscar estatus cuestionario
        $estatuscuestionario = array();
        foreach ($cuestionariosUsuario as $k => $v) {
            $element = $DaoCuestionario -> getEstatusCuestionario($v['idCuestionariosUsuario']);
            if (!empty($element)) {
                $estatuscuestionario[] = $element;
            }
        }
        /* Borrar estatus cuestionario */
        foreach ($estatuscuestionario as $k => $v) {
            $DaoCuestionario -> deleteEstatusCuestionario($v['idEstatusCuestionario']);
        }
        /* Borrar cuestionario usuario */
        foreach ($cuestionariosUsuario as $k => $v) {
            $DaoCuestionario -> deleteCuestionarioUsuario($v['idCuestionariosUsuario']);
        }
        /* -------------------------------------------------------------------- */
        // buscar preguntas
        $preguntas = $DaoCuestionario -> getPreguntasCuestionario($Cuestionario->id);
        //buscar opciones respuesta y eliminalas
        foreach ($preguntas as $k => $v) {
            /* Borrar opciones respuesta */
            $DaoCuestionario -> getOpcionesRespuestaAndDelete($v['idPregunta']);
            /* Borrar pregunta */
            $DaoCuestionario -> deletePregunta($v['idPregunta']);
        }

        /* Borrar cuestionario */
        $Obj[] = $DaoCuestionario->delete($Cuestionario->id);
    } else {
        $Obj['error']="elimina id";
    }
    break;
}
echo json_encode($Obj);
