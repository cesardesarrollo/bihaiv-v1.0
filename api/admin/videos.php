<?php
session_start();
require_once '../../core/modules/index/model/DaoVideosHome.php';
require_once '../../core/modules/index/model/DTO/VideosHome.php';

$DaoVideosHome=new DaoVideosHome();
switch ($_POST['method']) {
case 'save':
    if (strlen($_POST['nombre'])==0) {
        $Obj['error']="inserta nombre";
    } else if (strlen($_POST['url'])==0) {
        $Obj['error']="inserta url";      

    } else {

        $Obj= new VideosHome();
        $Obj->setNombre($_POST['nombre']);
        $Obj->setDescripcion($_POST['descripcion']);
        $Obj->setRuta($_POST['url']);
        $Obj->setPortada(($_POST['portada']=='true')?1:0);
        $Obj->setSeccion("YOUTUBE");
        $id=$DaoVideosHome->add($Obj);
                    
        $Obj=$DaoVideosHome->getById($id);                    
    }

    break;
case 'edit':
    if (strlen($_POST['id'])>0) {
        $Obj=$DaoVideosHome->getById($_POST['id']);
        $Obj->setNombre($_POST['nombre']);
        $Obj->setDescripcion($_POST['descripcion']);
        $Obj->setRuta($_POST['url']);
        $Obj->setPortada(($_POST['portada']=='true')?1:0);
        $Obj->setSeccion("YOUTUBE");
        $DaoVideosHome->update($Obj);

        $Obj=$DaoVideosHome->getById($_POST['id']);

    } else {
                $Obj['error']="inserta _id";
    }
    break;
case 'byId':

           $Obj=$DaoVideosHome->getById($_POST['id']);
    break;
case 'all':
           $Obj=$DaoVideosHome->getAllYoutube();
    break;
case 'delete':
    if (strlen($_POST['id'])>0) {
        $DaoVideosHome->delete($_POST['id']);
        $Obj=new VideosHome();
    } else {
        $Obj['error']="inserta id";
    }
    break;
}
echo json_encode($Obj);
 
