# BI HAIV
Red social


Desarrollado por: 

    SyeSoftware


Programado por:

    César Alonso Magaña Gavilanes 

    Omar Martínez Sanches

    Christian Santos García

    Job Isai Celaya


 Instalación:

 	Instalar base de datos: modelos/bihaiv_network_21Dic2016_final.sql.

 	Agregar carpetas y permisos de escritura: uploads [iniciativas,eventos,portaciones,cuestionarios], storage, docs y admin/files.

 	Configurar archivos:
 		
 		#core/controller/Database.php
		
		#core/app/defines.php
		
		#core/modules/index/model/DTO/Cnn.php

		#admin/includes/access.php


No necesario:

 	Para ejecutar tareas Gulp: npm install.

 	Para ejecutar Php Code Sniffer: composer install.
