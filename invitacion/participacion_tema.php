<?php
require_once '../core/modules/index/model/DaoUsuarios.php';
require_once '../core/modules/index/model/DTO/Usuarios.php';
//
require_once '../core/app/defines.php';
require_once '../core/app/debuggin.php';
require_once '../core/controller/Database.php';
require_once '../core/controller/Executor.php';
require_once '../core/controller/Model.php';
require_once '../core/modules/index/model/UserData.php';

if ( isset($_GET['org']) && isset($_GET['tema']) ) {
  //SUCCESS
  $organizacion_id = $_GET['org'];
  $temas_id = $_GET['tema'];
  $sql = "INSERT IGNORE INTO `tema_participa` (`tema_id`, `organizacion_id`) VALUES ('$temas_id', '$organizacion_id')";
  $resultSet = Executor::doit($sql);
  header('Location: '.APP_PATH.'?view=forum-detail&id='.$temas_id);
}else{
  // ERROR
  header('Location: '.APP_PATH.'home/login.php');
}
/*
  if (isset($_GET['action']) && $_GET['action'] == "uploadAttachment") {
    if (count($_FILES) > 0) {
      if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      } else {

        $DaoExperto = new DaoExperto();
        $experto = $DaoExperto->getByNonce($_GET['nonce']);

        $UserData = new UserData();
        $user = $UserData->getById($experto->getUsuarios_idUsuarios());
        if (strlen($user->imagen) > 0) {
          //Eliminar la foto anterior
          unlink('../admin/files/' . $user->imagen);
        }
        //Agregar nuevo foto
        $namefile = $DaoExperto->generarKey() . ".jpg";
        $UserData->imagen = $namefile;
        $UserData->id = $experto->getUsuarios_idUsuarios();
        $UserData->updateImagenUser();

        $ubicacion = "../admin/files/" . $namefile;
        move_uploaded_file($_FILES["file"]["tmp_name"], $ubicacion);
      }
    } elseif (isset($_GET['action'])) {

      if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      } else {


        if (isset($_GET['base64'])) {
          // If the browser does not support sendAsBinary ()
          $dataFile = file_get_contents('php://input');
          if (isset($_POST['fileExplorer'])) {
            //If the browser support readAsArrayBuffer ()
            //PHP handles spaces in base64 encoded string differently
            //so to prevent corruption of data, convert spaces to +
            $dataFile = $_POST['fileExplorer'];
            $dataFile = str_replace(' ', '+', $dataFile);
          }
          $content = base64_decode($dataFile);
        }

        $DaoExperto = new DaoExperto();
        $experto = $DaoExperto->getByNonce($_GET['nonce']);

        $UserData = new UserData();
        $user = $UserData->getById($experto->getUsuarios_idUsuarios());
        if (strlen($user->imagen) > 0) {
          //Eliminar la foto anterior
          unlink('../admin/files/' . $user->imagen);
        }
        //Agregar nuevo foto
        $namefile = $DaoExperto->generarKey() . ".jpg";
        $UserData->imagen = $namefile;
        $UserData->id = $experto->getUsuarios_idUsuarios();
        $UserData->updateImagenUser();

        $ubicacion = "../admin/files/" . $namefile;
        file_put_contents($ubicacion, $content);
      }
    }
  }

  if($_POST['action']=="updateNonce"){
    $DaoExperto= new DaoExperto();
    $experto=$DaoExperto->getById($_POST['id']);
    $experto->setNonce('');
    $experto->setAcepto(1);
    $DaoExperto->update($experto);
  }
  if($_POST['action']=="saveRedes"){
    $DaoRedesExperto= new DaoRedesExperto();
    $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'facebook');
    if($RedesExperto->getId()==0){
      $RedesExperto= new RedesExperto();
      $RedesExperto->setNombre('facebook');
      $RedesExperto->setUrl($_POST['urlFacebook']);
      $RedesExperto->setExperto_idExperto($_POST['id']);
      $DaoRedesExperto->add($RedesExperto);
    }else{
      $RedesExperto->setUrl($_POST['urlFacebook']);
      $DaoRedesExperto->update($RedesExperto);
    }

    $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'twitter');
    if($RedesExperto->getId()==0){
      $RedesExperto= new RedesExperto();
      $RedesExperto->setNombre('twitter');
      $RedesExperto->setUrl($_POST['urlTwitter']);
      $RedesExperto->setExperto_idExperto($_POST['id']);
      $DaoRedesExperto->add($RedesExperto);
    }else{
      $RedesExperto->setUrl($_POST['urlTwitter']);
      $DaoRedesExperto->update($RedesExperto);
    }

    $RedesExperto=$DaoRedesExperto->getByNombre($_POST['id'],'linkedin');
    if($RedesExperto->getId()==0){
      $RedesExperto= new RedesExperto();
      $RedesExperto->setNombre('linkedin');
      $RedesExperto->setUrl($_POST['urlLinkedin']);
      $RedesExperto->setExperto_idExperto($_POST['id']);
      $DaoRedesExperto->add($RedesExperto);
    }else{
      $RedesExperto->setUrl($_POST['urlLinkedin']);
      $DaoRedesExperto->update($RedesExperto);
    }
    echo $_POST['id'];
  }
*/
