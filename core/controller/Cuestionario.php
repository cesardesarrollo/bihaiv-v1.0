<?php 
error_reporting(E_ALL ^ E_DEPRECATED);

class Cuestionario extends Database {

	public function Cuestionario(){
		$Database = new Database();
		$Database->connect();
	}
	
	public static function r_print($print){
		echo "<pre>";
		print_r($print);
		echo "</pre>";
	}

	public static function sendMail($emailto,$subject,$body,$altbody){

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'mail.syesoftware.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'no-reply@syesoftware.com';                 // SMTP username
		$mail->Password = 'Syesoftware2015';                           // SMTP password
		$mail->Port = 26;                                    // TCP port to connect to
		$mail->setFrom('no-reply@syesoftware.com', 'Bihaiv');
		$mail->addAddress($emailto);               // Name is optional
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body    = $body;
		$mail->AltBody = $altbody;

		if(!$mail->send()) {
		    return 'EL mensaje no pudo enviarse. Error: ' . $mail->ErrorInfo;
		} else {
		    return 'El mensaje ha sido enviado';
		}
	}

	public static function vigencia($fecha_comparando,$nivel){

		$segundos_trans = strtotime(date('Y-m-d')) - strtotime($fecha_comparando);
		$dias_trans = intval($segundos_trans/60/60/24);

		switch($nivel){
			case "perfilamiento":
				if($dias_trans > 5)
					return false;
				else
					return true;
			break;

			case "segunda_oportunidad":
				if($dias_trans > 5)
					return false;
				else
					return true;
			break;

			case "plazo_evaluacion":
				if($dias_trans > 5) // Debieran ser 15
					return false;
				else
					return true;
			break;

			case "tercer_oportunidad":
				if($dias_trans > 180)
					return true;
				else
					return false;
			break;

			case "maximo_para_evaluar":
				if($dias_trans > 5)
					return true;
				else
					return false;
			break;
		}
	}

	public static function getExpertos($userid_a_validar){
		$userdata = new UserData();
		$user = $userdata->getById($userid_a_validar);
		$expertos = $userdata->getExpertosByCountry($userid_a_validar,$user->id_country);
		return $expertos;
	}

	public static function getActores($userid_a_validar){
		$userdata = new UserData();
		$user = $userdata->getById($userid_a_validar);
		$actores = $userdata->getActoresByCountry($userid_a_validar,$user->id_country);
		return $actores;
	}

	
	public static function getActoresExcept($userid_a_validar, $except){
		$userdata = new UserData();
		$user = $userdata->getById($userid_a_validar);

		$actores = $userdata->getActoresByCountryExcept($userid_a_validar,$user->id_country, $except);
		return $actores;
	}

	public static function getActoresExceptCustom($userid_a_validar, $except){
		$userdata = new UserData();
		$user = $userdata->getById($userid_a_validar);

		$actores = $userdata->getActoresByCountryExceptCustom($userid_a_validar,$user->id_country, $except);
		return $actores;
	}

	public static function getUser($user_id){
		/*Obtener datos de usuario desde base de datos */
		$sql = mysql_query("SELECT * FROM user WHERE id = ".$user_id." ");
		$row = mysql_fetch_assoc($sql);
		return $row;
	}

	public static function url_cuestionarios() 
	{
	    return APP_PATH."cuestionarios/";
	}

}

?>