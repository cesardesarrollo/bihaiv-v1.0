<?php
// 13 mayo 2016
include 'app/defines.php';
// 24 Agosto 2016 Traducción
require 'lang/lang.php';

// PhpMailer
require 'vendor/phpmailer/class.phpmailer.php';

// Twitter Library
require 'vendor/codebird-php/src/codebird.php';

//Core
include "controller/Core.php";
include "controller/View.php";
include "controller/Module.php";
include "controller/Database.php";
include "controller/Executor.php";
include "controller/forms/lbForm.php";
include "controller/forms/lbInputText.php";
include "controller/forms/lbInputPassword.php";
include "controller/forms/lbValidator.php";
include "controller/Model.php";
include "controller/Bootload.php";
include "controller/Action.php";
include "controller/Request.php";
include "controller/Get.php";
include "controller/Post.php";
include "controller/Cookie.php";
include "controller/Session.php";
include "controller/Lb.php";
include "controller/Form.php";
include "controller/class.upload.php";

// 13 mayo 2016
include 'controller/Cuestionario.php';
include 'controller/Registro.php';

?>
