<?php

  $logged = false;
  if(isset($_SESSION["user_id"])){ $logged = true; }

  if(isset($_GET["id"])){
    $user = UserData::getById($_GET["id"]);
    //$profile = ProfileData::getByUserId($_GET["id"]);
    if(!$user -> is_active){
      /*
      usario inexistente, ya que no se encuentra activo
      */
      Core::redir("./");
    }
    /* Valida que lo visite un usuario valido
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $userVisitant = UserData::getById($_SESSION["user_id"]);
      if(!$userVisitant->is_valid) {
        Core::redir("./");
      }
    }*/

  } else {
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $user = UserData::getById($_SESSION["user_id"]);
      //$profile = ProfileData::getByUserId($_SESSION["user_id"]);
      if(!$user->is_valid || $user->is_admin || $user->tipoRel!=1) {
        Core::redir("./");
      }
    }
  }
  require_once('./core/modules/index/model/DaoEvento.php');
  require_once('./core/modules/index/model/DaoOrganizacion.php');
  $DaoOrganizacion = new DaoOrganizacion();

  if(isset($_GET["id"])){
    $user = UserData::getById($_GET["id"]);
    $organizacion = $DaoOrganizacion->getByUserId($_GET["id"]);
  } else {
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $user = UserData::getById($_SESSION["user_id"]);
      $organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);
      if(!$user->is_valid) {
        Core::redir("./");
      }
    }
  }
?>
<!-- CSS AND JS -->
<!-- SELECT 2 -->
<link rel="stylesheet" href="res/select2/css/select2.min.css">
<script type="text/javascript" src="res/select2/js/select2.min.js"></script>
<!-- SELECT BTN -->
<link rel="stylesheet" href="res/selectButton/css/bootstrap-select.min.css">
<script type="text/javascript" src="res/selectButton/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="res/selectButton/js/defaults-es_CL.min.js"></script>
<script type="text/javascript" src="res/js/highcharts.js"></script>

<div class="container">
    <?php include('./views/main_menu.php'); ?>
    <section class="Profile__cover">
      <figure class="figure" style="
      height: 315px;
      box-shadow: inset 0 -120px 150px -32px rgba(12,12,12,0.9);
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      <?php if($organizacion->portada!="assets/img/home/PORTADA.jpg"):?>
        background: url(<?php echo "storage/users/".$user->id."/profile/".$organizacion->portada; ?>) no-repeat center center;
      <?php else:?>
        background: url('assets/img/home/PORTADA.jpg') center center;
      <?php endif; ?>
      ">
      </figure>
    </section>
    <?php
      $from = "logout";
      if(isset($_SESSION["user_id"])){ $from="logged"; }
      Action::execute("_userprofileinfo",array("user"=>$user,"organizacion"=>$organizacion ,"from"=>$from));
    ?>
    <section class="Profile__wall" style="padding-top: 200px;">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-12 col-lg-12">
             <?php Action::execute("_usermodsugerencia",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
          </div>
        </div>
        <div class="row padding-row">
          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-8">
            <?php Action::execute("_usermodenfoque",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
            <?php Action::execute("_usermodiniciativas",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
            <?php Action::execute("_usermodaportaciones",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
            <?php if( $logged ): ?>
            <?php Action::execute("_usermodcolaboraciones",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
            <?php endif; ?>
            <?php Action::execute("_usermodeventos",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
            <?php if( $logged ): ?>
            <?php Action::execute("_usermodnoticias",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
            <?php endif; ?>
            <?php Action::execute("_usersocialprofile",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
            <?php Action::execute("_usermodgraficas",array("user"=>$user,"organizacion"=>$organizacion,"from"=>$from ));?>
          </div>
        </div>
      </div>
    </section>
    <div id="modal-launcher"></div>
  </div>
