<?php

  $logged = false;
  if(isset($_SESSION["user_id"])){ $logged = true; }

/*
  if(isset($_GET["id"])){
    $user = UserData::getById($_GET["id"]);
    $profile = ProfileData::getByUserId($_GET["id"]);
  } else {
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $user = UserData::getById($_SESSION["user_id"]);
      $profile = ProfileData::getByUserId($_SESSION["user_id"]);
      if(!$user->is_valid || $user->is_admin || $user->tipoRel!=1) {
        Core::redir("./");
      }
    }
  }
  require_once('./core/modules/index/model/DaoEvento.php');
  require_once('./core/modules/index/model/DaoOrganizacion.php');
  $DaoOrganizacion = new DaoOrganizacion();

  if(isset($_GET["id"])){
    $user = UserData::getById($_GET["id"]);
    $organizacion = $DaoOrganizacion->getByUserId($_GET["id"]);
  } else {
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $user = UserData::getById($_SESSION["user_id"]);
      $organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);
      if(!$user->is_valid) {
        Core::redir("./");
      }
    }
  }*/

  $show = 2;
  if(isset($_GET['show']) && $_GET['show'] > 1){
    $show = $_GET['show'];
  }
  
  $DaoSeccionAportacion = new DaoSeccionAportacion();
  $datosSeccion = $DaoSeccionAportacion->getById($show);
?>
<!-- CSS AND JS -->
<link rel="stylesheet" href="assets/css/contributions_categories.css">
<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-aportaciones.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?php if($_COOKIE['bihaiv_lang']=='es'){ echo $datosSeccion->getTitulo();} else { echo $datosSeccion->getTitle();}  ?></h1>
            <p class="description-gray" style="padding-right:120px">
              <?php if($_COOKIE['bihaiv_lang']=='es'){ echo $datosSeccion->getDescripcion();} else { echo $datosSeccion->getDescription();}   ?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
</div>
<div class="container">
  <div class="col-md-12 contributions-categories-container">
    <div class="container-fluid">
      <div class="row padding-row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
          <md-card class="_md">
            <md-card-header>
              <md-card-header-text>
                <span class="md-title text-center">
                  <b><?=translate('Índice')?></b>
                </span>
              </md-card-header-text>
            </md-card-header>
            <md-card-content>
              <?php Action::execute("_contributioncategories", [ 'show' => $show ]); ?>
            </md-card-content>
          </md-card>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <md-card class="_md">
            <md-card-content  id="contribution_pdf_preview">
              <p class="text-center"><?=translate('Vista previa del archivo')?></p>
            </md-card-content>
          </md-card>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
          <md-card class="_md">
            <md-card-header>
              <md-card-header-text>
                <span class="md-title text-center">
                  <b><?=translate('Aportaciones recientes')?></b>
                </span>
              </md-card-header-text>
            </md-card-header>
          </md-card>
          <?php Action::execute("_recentcontributions", [ 'show' => $show ]); ?>
        </div>
      </div>
    </div>
  </div>
  <div id="modal-launcher"></div>
</div>
