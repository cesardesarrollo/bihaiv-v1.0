<style type="text/css">
.fnt{
font-size: 14px;

}
</style>

<?php
//Comprueba sesión activa
if(!isset($_SESSION["user_id"])){ Core::redir("./"); }

require_once 'core/modules/index/model/DaoOrganizacion.php';
require_once 'core/modules/index/model/DaoUbicaciones.php';
require_once 'core/modules/index/model/DaoOauth.php';

$DaoOrganizacion = new DaoOrganizacion();
$organizacion    = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);

$infoUser = UserData::userInformation( $_SESSION['user_id']);

$DaoUbicaciones = new DaoUbicaciones();
$Ubicaciones    = new Ubicaciones();
$Ubicaciones    = $DaoUbicaciones->getMainByOrgId( $organizacion->id );

$DaoOauth       = new DaoOauth();
$oauths         = $DaoOauth->getOauthsByIdUsu( $organizacion->id );
$twitter_linked = false;

if (isset($_GET['oauth_verifier']) && isset($_SESSION['oauth_verify'])) {
  $twitter = Core::initTwitterAPI();

  // verify the token
  $twitter->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
  unset($_SESSION['oauth_verify']);

  // get the access token
  $reply = $twitter->oauth_accessToken([
    'oauth_verifier' => $_GET['oauth_verifier']
  ]);

  // store the token (which is different from the request token!)
  foreach ($oauths as $oauth) {
    if ($oauth->getServicio() == 'Twitter') {
      $twitter->setToken($reply->oauth_token, $reply->oauth_token_secret);
      $account = $twitter->account_settings();

      if(isset($account->screen_name) && !empty($account->screen_name)){
        $oauth->setAccessToken($reply->oauth_token);
        $oauth->setAccessTokenSecret($reply->oauth_token_secret);
        $oauth->setUrlRed('https://twitter.com/'.$account->screen_name);

        $DaoOauth->update($oauth);
      }
    }
  }

  // send to same URL, without oauth GET parameters
  Core::redir("./?view=editinformation");
}else{
  foreach ($oauths as $oauth) {
    if ( $oauth->getServicio() == 'Twitter' && !empty($oauth->getAccessToken()) && !empty($oauth->getAccessTokenSecret()) && !empty($oauth->getUrlRed()) ) {
      $twitter_linked = true;
    }
  }
}
?>
<div class="container padding-top-100">
  <div class="row">
    <div class="col-md-3">
    <?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>$organizacion ,"from"=>"logged"));?>
    <?php Action::execute("_infomenu",array());?>
    </div>
    <div class="col-md-7">
      <h1><?=translate('Editar Información')?></h1>
      <form role="form" enctype="multipart/form-data" method="post" action="./?action=updateinformation">
        <div class="form-group">
          <label id='lbImgPerfil' for="inputImgPerfil" class="label label-default fnt"><?=translate('Imagen de Perfil')?> (200x180)</label>
          <input type="file" id="inputImgPerfil" name="image" accept="image/*" style="display: none">
        </div>
        <div class="form-group">
          <label id='lbImgPortada' for="inputImgPortada" class="label label-default fnt"><?=translate('Imagen de Portada')?> (1140x315) </label>
          <input type="file" id="inputImgPortada"  name="portada" accept="image/*" style="display: none">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Teléfono')?>:</label>
          <input type="text" name="_telefono" class="form-control"  placeholder="<?=translate('Teléfono')?>" value="<?=utf8_encode(@$infoUser->_telefono) ?>">
        </div>
        <div class="form-group">
          <label for="txt__Email"><?=translate('Correo Electrónico')?>:</label>
          <input type="text" name="_email" class="form-control" id="txt__Email"  placeholder="<?=translate('Correo Electrónico')?>" value="<?=utf8_encode(@$infoUser->_email) ?>" readonly>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Url')?>:</label>
          <input type="text" name="_url" class="form-control" placeholder="<?=translate('Dirección de tu sitio')?>" value="<?=utf8_encode(@$infoUser->_urlweb) ?>">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Dirección principal')?>:</label>
          <input type="text" name="_ubicacion" id="address" class="form-control"  placeholder="<?=translate('Mi ubicación')?>" value="<?=utf8_encode(@$infoUser->_ubicacion) ?>">
          <label class="hide">Latitud</label>
          <input type="hidden" name="lat" id="lat" value="<?=utf8_encode(@$infoUser->_lat) ?>" />
          <label class="hide">Longitud</label>
          <input type="hidden" name="lng" id="lng" value="<?=utf8_encode(@$infoUser->_lng) ?>" />
        </div>
        <div class="form-group">
          <div id="map_canvas" style="width: 400px; height: 280px; margin: 0 auto;"></div>
          <div id="callback_result" class="hidden"></div>
          <label for="exampleInputEmail1"><?=translate('¿Actualizar marcador?')?> </label>
          <select name="reverseGeocode" id="reverseGeocode" class="form-control">
            <option value="false" selected><?=translate('No')?></option>
            <option value="true"><?=translate('Si')?></option>
          </select>
        </div>

        <div class="form-group">
          <label for="cmb__Alcanze"><?=translate('Alcance')?>:</label>
          <select class="form-control" name="idAlcanze" id="cmb__Alcanze" value="<?php  echo $organizacion->idAlcanze; ?>"  onchange="seteaUbicacion(this.options[this.selectedIndex].text)"  >
            <option value=""><?=translate('Alcance')?></option>
            <?php
            $alcanzes = $DaoUbicaciones->getAlcanzes();
            foreach ($alcanzes as $alcanze ) {
              echo '<option '.(($alcanze['id'] == $organizacion->idAlcanze)?'selected':'').' value="'.$alcanze['id'].'">'.$alcanze['nombre'].'</option>';
            }
            ?>
          </select>
        </div>

        <div class="form-group">
          <label for=""><?=translate('Ubicacion(es) de alcance')?>:</label>
          <button type="button" class="btn btn-primary pull-right" onclick='$("#newUbication").slideToggle("fast");'>
            <?=translate('Agregar ubicación')?>
          </button>
          <div id="newUbication" class="row margin-row-top" style="display:none;" id="cmb__Country_field">
            <div class="col-lg-6 col-md-6">
              <label for="cmb__Country"><?=translate('País')?>:</label>
              <select class="form-control" id="cmb__Country" onchange="getStateByCountry()"></select>
            </div>
            <div class="col-lg-6 col-md-6" id="cmb__State_field">
              <label for="cmb__State"><?=translate('Estado')?>:</label>
              <select class="form-control" id="cmb__State" onchange="getMunicipeByState()"></select>
            </div>
            <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__Municipe_field">
              <label for="cmb__Municipe"><?=translate('Municipio')?>:</label>
              <select class="form-control" id="cmb__Municipe" onchange="getLocalidadesByMunicipe()"></select>
            </div>
            <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__city_field">
              <label for="cmb__city"><?=translate('Localidad')?>:</label>
              <select class="form-control" id="cmb__city"></select>
            </div>
            <div class="col-lg-6 col-md-6 margin-row-top" id="cmb__Influencia_field">
              <label for="cmb__Influencia"><?=translate('¿Influye en el ecosistema?')?>:</label>
              <select class="form-control" id="cmb__Influencia">
                <option value="1" ><?=translate('Con Influencia')?> </option>
                <option value="0" ><?=translate('Sin Influencia')?> </option>
              </select>
            </div>
            <div class="col-xs-6 col-lg-6 margin-row-top">
              <button id="addUbication" type="button" class="btn btn-primary pull-right">
                <?=translate('Guardar')?>
              </button>
            </div>
          </div>
          <table class="table table-bordered bihaiv_table margin-row-top hide" id="otrasUbicaciones">
            <thead>
              <tr>
                <td><?=translate('País')?></td>
                <td><?=translate('Estado')?></td>
                <td><?=translate('Municipio')?></td>
                <td><?=translate('Localidad')?></td>
                <td><?=translate('Influye')?></td>
                <th><?=translate('Eliminar')?></th>
              </tr>
            </thead>
            <tbody id="lstUbications">
            </tbody>
          </table>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Misión')?>:</label>
          <textarea class="form-control" name="_mision" maxlength="250"><?=utf8_encode(@$infoUser->_mision) ?></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Visión')?>:</label>
          <textarea class="form-control" name="_vision" maxlength="250"><?=utf8_encode(@$infoUser->_vision) ?></textarea>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Acerca de mi')?>:</label>
          <textarea class="form-control" name="_quienSoy" maxlength="500"><?=utf8_encode(@$infoUser->_quiensoy) ?></textarea>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-xs-6 col-md-6 col-lg-6">
              <div class="input-group input-group-sm">
                <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-facebook"></i></span>
                <input type="url" class="form-control" name="_facebook" placeholder="<?=translate('Perfil de facebook')?>" aria-describedby="sizing-addon3" placeholder="Ej. http://www.facebook.com/mifanpage" value="<?=utf8_encode(@$infoUser->_Facebook) ?>">
              </div>
            </div>
            <div class="col-xs-6 col-md-6 col-lg-6">

              <?php if($twitter_linked == true) { ?>

              <div class="input-group input-group-sm">
                <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-twitter"></i></span>
                <input type="text" class="form-control" name="_twitter" placeholder="<?=translate('Perfil de twitter')?>" aria-describedby="sizing-addon3" value="<?=utf8_encode(@$infoUser->_Twitter) ?>" readonly>
              </div>
              <div class="form-group text-center" style="margin-top: 10px">
                <button type="button" class="btn btn-default" id="linkTwitterAccount"><i class="fa fa-twitter"></i> <?=translate('Desvincular twitter')?></button>
              </div>

              <?php }else{ ?>

              <div class="form-group text-center">
                <button type="button" class="btn btn-info" id="linkTwitterAccount"><i class="fa fa-twitter"></i> <?=translate('Vincular twitter')?></button>
              </div>

              <?php } ?>

            </div>
          </div>
        </div>
        <input type="hidden" id="EstateCity" name="EstateCity">
        <input type="hidden" id="EstateColony" name="EstateColony">
        <input type="hidden" id="EstateState" name="EstateState">
        <input type="hidden" id="EstateCountry" name="EstateCountry">
        <input type="hidden" id="EstateZipcode" name="EstateZipcode">
        <input type="hidden" id="EstateAddress" name="EstateAddress">
        <button type="submit" class="btn btn-primary"><?=translate('Actualizar Información')?></button>
      </form>
    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>
<br><br><br><br><br>

<script>
    // Ubicaciones
    var pais_id       = $("#pais_id");
    var estado_id     = $("#estado_id");
    var municipio_id  = $("#municipio_id");
    var localidad_id  = $("#localidad_id");

    function seteaUbicacion(alcanze){

      var pais_id_field       = $("#cmb__Country_field");
      var estado_id_field     = $("#cmb__State_field");
      var municipio_id_field  = $("#cmb__Municipe_field");
      var localidad_id_field  = $("#cmb__city_field");
      var influencia_field    = $("#cmb__Influencia_field");

      // Esconde
      if(!pais_id_field.hasClass('hide')){
        pais_id_field.addClass('hide');
      }
      if(!estado_id_field.hasClass('hide')){
        estado_id_field.addClass('hide');
      }
      if(!municipio_id_field.hasClass('hide')){
        municipio_id_field.addClass('hide');
      }
      if(!localidad_id_field.hasClass('hide')){
        localidad_id_field.addClass('hide');
      }
      if(!influencia_field.hasClass('hide')){
        influencia_field.addClass('hide');
      }

      // nacional
      if (alcanze == 'NACIONAL'){
        console.log(pais_id_field);
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(!estado_id_field.hasClass('hide')){
          estado_id_field.addClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // regional
      else if (alcanze == 'REGIONAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // estatal
      else if (alcanze == 'ESTATAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(!municipio_id_field.hasClass('hide')){
          municipio_id_field.addClass('hide');
        }
        if(!localidad_id_field.hasClass('hide')){
          localidad_id_field.addClass('hide');
        }
        if(!influencia_field.hasClass('hide')){
          influencia_field.addClass('hide');
        }
      }
      // local
      else if (alcanze == 'LOCAL'){
        if(pais_id_field.hasClass('hide')){
          pais_id_field.removeClass('hide');
        }
        if(estado_id_field.hasClass('hide')){
          estado_id_field.removeClass('hide');
        }
        if(municipio_id_field.hasClass('hide')){
          municipio_id_field.removeClass('hide');
        }
        if(localidad_id_field.hasClass('hide')){
          localidad_id_field.removeClass('hide');
        }
        if(influencia_field.hasClass('hide')){
          influencia_field.removeClass('hide');
        }
      }
    }
    seteaUbicacion(document.getElementById('cmb__Alcanze').options[document.getElementById('cmb__Alcanze').selectedIndex].text);


    // Eliminada
    function seteaUbicacionPrincipal(option){
      // Pais
      pais_id.append(option);
      pais_id.val(<?php echo $Ubicaciones->getIdPais(); ?>);

      // Estado
      $.post('api/front/estados.php', {'method' : 'byPaisId', 'id' : pais_id.val()})
      .done(function(data){
        var option = '<option value=""><?=translate('Selecciona un estado')?></option>';
        <?php if(!empty($Ubicaciones->getIdEstado())): ?>
        $.each(JSON.parse(data), function(k,v){
          option += '<option id="' + v.abrev + '" value="' + v.id + '" ' + ((v.id == <?php echo $Ubicaciones->getIdEstado(); ?>)? "selected" : "" ) + '>' + v.nombre + '</option>';
        });
        <?php endif; ?>
        estado_id.append(option);

        // Municipio
        $.post("api/front/municipios.php", {'method': 'byEstadoId', 'id' : estado_id.val()})
        .done(function(data){
          var option = '<option value=""><?=translate('Selecciona un municipio')?></option>';
          <?php if(!empty($Ubicaciones->getIdMunicipio())): ?>
          $.each(JSON.parse(data), function(k,v){
            option += '<option id="' + v.abrev + '" value="' + v.id + '" ' + ((v.id == <?php echo $Ubicaciones->getIdMunicipio(); ?>)? "selected" : "" ) + '>' + v.nombre + '</option>';
          });
          <?php endif; ?>
          municipio_id.append(option);

          // Localidad
          $.post("api/front/localidades.php", {'method': 'byMunicipioId', 'id' : municipio_id.val()})
          .done(function(data){
            var option = '<option value=""><?=translate('Selecciona una localidad')?></option>';
            <?php if(!empty($Ubicaciones->getIdLocalidad())): ?>
            $.each(JSON.parse(data), function(k,v){
              option += '<option id="' + v.abrev + '" value="' + v.id + '" ' + ((v.id == <?php echo $Ubicaciones->getIdLocalidad(); ?>)? "selected" : "" ) + '>' + v.nombre + '</option>';
            });
            <?php endif; ?>
            localidad_id.append(option);

          });
        });
      });
    }

    $.post('api/front/paises.php', {'method' : 'all'})
    .done(function( data ){
      $("#cmb__Country").html('');
      var option = '<option value=""><?=translate('Selecciona un país')?></option>';
      $.each(JSON.parse(data), function(k,v){
        option += '<option id="' + v.prefijo + '" value="' + v.id + '">' + v.nombre + '</option>';
        console.log(v);
      })
      $("#cmb__Country").append(option);
      // Inicia encadenamiento con datos de ubicación principal
      <?php if(count($Ubicaciones) > 0): ?>
      seteaUbicacionPrincipal(option);
      <?php endif; ?>
    });

    function getStateByCountry(ubicacion){
      if(ubicacion === "main"){
        estado_id.html('');
        var id_country = pais_id.val();
      } else {
        $("#cmb__State").html('');
        var id_country = $("#cmb__Country").val();
      }
      $.post('api/front/estados.php', {'method' : 'byPaisId', 'id' : id_country})
      .done(function(data){
        var option = '<option value=""><?=translate('Selecciona un estado')?></option>';
        $.each(JSON.parse(data), function(k,v){
          option += '<option id="' + v.abrev + '" value="' + v.id + '">' + v.nombre + '</option>';
        });
        if(ubicacion === "main"){
          estado_id.append(option);
        } else {
          $("#cmb__State").append(option);
        }
      })
    }

    function getMunicipeByState(ubicacion){
      if(ubicacion === "main"){
        municipio_id.html('');
        var id_state = estado_id.val();
      } else {
        $("#cmb__Municipe").html('');
        var id_state = $("#cmb__State").val();
      }
      $.post("api/front/municipios.php", {'method': 'byEstadoId', 'id' : id_state})
      .done(function(data){
      var option = '<option value=""><?=translate('Selecciona un municipio')?></option>';
        $.each(JSON.parse(data), function(k,v){
          option += '<option id="' + v.clave + '" value="' + v.id + '">' + v.nombre + '</option>';
        });
        if(ubicacion === "main"){
          municipio_id.append(option);
        } else {
          $("#cmb__Municipe").append(option);
        }
      });
    }

    function getLocalidadesByMunicipe(ubicacion){
      if(ubicacion === "main"){
        localidad_id.html('');
        var id_municipe = municipio_id.val();
      } else {
        $("#cmb__city").html('');
        var id_municipe = $("#cmb__Municipe").val();
      }
      $.ajax({
        url   : 'api/front/localidades.php',
        type  : 'POST',
        data  : { method : 'byMunicipioId', id : id_municipe},
        async : false,
      })
      .done(function(data){
      var option = '<option value=""><?=translate('Selecciona una localidad')?></option>';
        $.each(JSON.parse(data), function(k,v){
          option += '<option id="' + v.clave + '" value="' + v.id + '">' + v.nombre + '</option>';
        });
        if(ubicacion === "main"){
          localidad_id.append(option);
        } else {
          $("#cmb__city").append(option);
        }
      })
      .fail(function(err){
        console.log( err );
      })
    }

    function drawLstUbications(){
      $('#lstUbications').html('');
      $.ajax({
        url   : 'api/front/ubicaciones.php',
        type  : 'POST',
        data  : {'method': 'byUserId', 'user_id': <?php echo $_SESSION["user_id"] ?>},
        async : false,
      })
      .done(function(data){

        var otrasUbicaciones = $('#otrasUbicaciones');
        if(JSON.parse(data).length > 0){
          otrasUbicaciones.removeClass("hide");
        } else {
          if(!otrasUbicaciones.hasClass("hide")){
            otrasUbicaciones.addClass("hide");
          }
        }

        var row = "";
        $.each(JSON.parse(data), function(k,v){
          row += "<tr>"
          row += "  <td>" + v.pais_nombre + "</td>"
          row += "  <td>" + v.estado_nombre + "</td>"
          row += "  <td>" + v.municipio_nombre + "</td>"
          row += "  <td>" + v.localidad_nombre + "</td>"
          row += "  <td>" + v.influencia + "</td>"
          row += "  <td><span class='btn btn-primary' onclick='delUbication(" + v.id + ")'>X</span></td>"
          row += "</tr>";
        });
        $('#lstUbications').html(row);
      })
      .fail(function(err){
        console.log( err );
      })
    }

    // Elimina ubicación
    function delUbication(id){
      $.post('api/front/ubicaciones.php', {'method' : 'delete', 'id': id})
      .done(function( data ){
        if (data) {
          drawLstUbications();
          alert('<?=translate('Ubicación eliminada')?>');
        } else {
          alert("<?=translate('Error al eliminar ubicación')?>");
        }
      })
    }

    // Agregar ubicación de sucursal/alcanze/oficina
    $('#addUbication').on('click',addUbication);
    function addUbication(e){
      e.preventDefault();

      var data = {
        'method'       : 'save',
        'user_id'      : <?php echo $_SESSION["user_id"]?>,
        'pais_id'      : $("#cmb__Country").val(),
        'estado_id'    : $("#cmb__State").val(),
        'municipio_id' : $("#cmb__Municipe").val(),
        'localidad_id' : $("#cmb__city").val(),
        'is_main'      : $("#cmb__Influencia").val()
      }
      $.ajax({
        url   : 'api/front/ubicaciones.php',
        type  : 'POST',
        data  : data,
        async : false,
      })
      .done(function(data){
        $('#otrasUbicaciones').removeClass("hide");
        drawLstUbications();
        // Limpieza de formulario
        $("#cmb__Country").val('');
        $("#cmb__State").empty();
        $("#cmb__Municipe").empty();
        $("#cmb__city").empty();
      })
      .fail(function(err){
        console.log( err );
      })
    }
    drawLstUbications();

$(function(){
    $('#linkTwitterAccount').click(function(){
      var method = 'linkTwitterAccount';
      var id = 0;

      $(this).prop('disabled', true);
      $(this).html('<i class="fa fa-spinner"></i> Procesando');

      if($(this).hasClass('btn-default')){
        method = 'unlinkTwitterAccount';
        id = <?php echo $organizacion->id ?>;
      }

      $.ajax({
        url   : 'api/front/userdata.php',
        method : 'POST',
        data : { method: method, id: id }
      })
      .done( function( _res ){
        var response = jQuery.parseJSON( _res );
        if(typeof response.auth_url != 'undefined' && !$.isEmptyObject(response.auth_url)){
          window.location.replace(response.auth_url);
        }
      })
      .fail( function( _err ){
        console.log( _err );
      });
    });
   
   var inpPerfil = document.getElementById('inputImgPerfil');

      inpPerfil.onclick = function () {
            $('#lbImgPerfil').removeClass();
            $('#lbImgPerfil').addClass('label label-default fnt');  
      };

      inpPerfil.onchange = function () {
         // alert(this.value);
          if(this.value=='')
          {
            $('#lbImgPerfil').removeClass();
            $('#lbImgPerfil').addClass('label label-default fnt');
            //  $(this).addClass('selected');
          }
          else
          {
            $('#lbImgPerfil').removeClass();
             $('#lbImgPerfil').addClass('label label-success fnt')
          // $('#lbImgPerfil').addClass('upload-button selected');
           // $('#lbImgPerfil').html('<?=translate('Imagen de Perfil')?> '+' (200x180)');
          }
      };

      var inpPortada = document.getElementById('inputImgPortada');

      inpPortada.onclick = function () {
            $('#lbImgPortada').removeClass();
            $('#lbImgPortada').addClass('label label-default fnt');  
      };

      inpPortada.onchange = function () {
         // alert(this.value);
          if(this.value=='')
          {
            //  $(this).addClass('selected');
          }
          else
          {
            $('#lbImgPortada').removeClass();
            $('#lbImgPortada').addClass('label label-success fnt');
           // $('#lbImgPerfil').html('<?=translate('Imagen de Perfil')?> '+' (200x180)');
          }
      };

    /*
     * Maps
     */
    var addresspickerMap = $('#address').addresspicker({
        regionBias: "mx",
        updateCallback: showCallback,
        mapOptions: {
            zoom: 16,
            center: new google.maps.LatLng('20.7217961, -103.38968779999999'),
            scrollwheel: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false
        },
        elements: {
            map:                          "#map_canvas",
            lat:                          "#lat",
            lng:                          "#lng",
            type:                          "#type",
            locality:                     '#EstateCity',
            sublocality:                  '#EstateColony',
            administrative_area_level_1:  '#EstateState',
            country:                      '#EstateCountry',
            postal_code:                  '#EstateZipcode',
            route:                        '#EstateAddress'

        }
    });

    var gmarker = addresspickerMap.addresspicker("marker");
    gmarker.setVisible(true);

    addresspickerMap.addresspicker("updatePosition");

    var map = addresspickerMap.addresspicker("map");
      google.maps.event.trigger(map,"resize");

    $('#reverseGeocode').change(function(){
      $("#address").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
    });

    function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
    }

  });

</script>
