<?php

//Comprueba sesión activa
if(!isset($_SESSION["user_id"])){ Core::redir("./"); }

require_once 'core/modules/index/model/DaoOrganizacion.php';
$DaoOrganizacion = new DaoOrganizacion();

// $levels =LevelData::getAll();
$organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);
?>
<div class="container padding-top-100">
<div class="row">
  <div class="col-md-3">
      <?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>$organizacion ,"from"=>"logged"));?>
  </div>
  <div class="col-md-7">
    <h1><?=translate('Configuración')?></h1>
    <?php
    Session::alert("password_updated","Contrase&ntilde;a actualizada exitosamente!","success");
    ?>
    <form role="form" method="post" action="./?action=updateconfiguration">
      <div class="form-group">
        <label for="exampleInputEmail1"><?=translate('Nombre de usuario')?></label>
        <input type="text" name="name" value="<?php echo Session::$user->name; ?>" class="form-control"  placeholder="<?=translate('Tu nombre')?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1"><?=translate('Email')?></label>
        <input type="text" name="email" value="<?php echo Session::$user->email; ?>" class="form-control"  placeholder="<?=translate('Tu email')?>" readonly>
      </div>
    <!--
      <div class="checkbox">
        <label>
          <input type="checkbox" name="is_active" <?php if(Session::$user->is_active){ echo "checked"; }?>> Usuario activo
        </label>
      </div>
    -->
      <button type="submit" class="btn btn-primary"><?=translate('Guardar Configuración')?></button>
    </form>
    <br>
    <h1><?=translate('label cambiarcontra')?></h1>
    <form role="form" method="post" action="./?action=updatepassword" id="changepassword">
      <div class="form-group">
        <label for="exampleInputEmail1"><?=translate('Contraseña Actual')?></label>
        <input type="password" required name="password" value="" class="form-control"  placeholder="<?=translate('Contraseña Actual')?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1"><?=translate('Contraseña Nueva')?></label>
        <input type="password" min  length="8" size="8" required name="new_password" id="new_password" value="" class="form-control"  placeholder="<?=translate('Contraseña Nueva')?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1"><?=translate('Repetir Contraseña')?></label>
        <input type="password" min  length="8" size="8" required name="confirm_password" id="confirm_password" value="" class="form-control"  placeholder="<?=translate('Repetir Contraseña')?>">
      </div>
      <button type="submit" class="btn btn-primary"><?=translate('Actualizar Contraseña')?></button>
    </form>
    <script>
    $("#changepassword").submit(function(e){
      if($("#new_password").val()!=$("#confirm_password").val()){
        e.preventDefault();
        alert("<?=translate('Las contraseñas no coinciden')?>");
      }
    });
    </script>
      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
</div>
<br><br><br><br><br>
