<div class="container">

    <?php include('./views/main_menu.php'); ?>

    <section class="Profile__wall">
      <div class="container-fluid">
        <div class="row margin-row-top">
          <div class="">
            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                    Mejores Prácticas
                  </a>
                </li>
                <li role="presentation">
                  <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                    Aportaciones del ecosistema
                  </a>
                </li>
                <!--<li role="presentation">
                      <a href="#estudio" aria-controls="estudio" role="tab" data-toggle="tab">
                      Estudio de dinámicas del ecosistema
                    </a>
                  </li>-->
          </ul>
          <!-- Tab panes -->
          <div class="tab-content margin-row-top">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
              <div class="row" id="mejores-list">
                <!-- El contenido se carga aqui -->
              </div>
              <!-- MODAL PARA CARGAR VISTAS PREVIAS -->
              <div class="modal fade" id="modal-biblioteca-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body" style="height: 80vh;">
                      <iframe id="i-pdf-1" width="100%" height="100%" style="border: 1px solid #f5f6f6;"></iframe>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END - MODAL PARA CARGAR VISTAS PREVIAS -->
              <!-- <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="bp__menu">
                  <ul id="mejores-list">
                  </ul>
                </div>
              </div> -->
              <!-- <div class="col-sm-12 col-md-9 col-lg-9">
                Button trigger modal
                <div class="bp__book">
                  <iframe id="i-pdf-1" width="100%" height="100%" style="border: 1px solid #f5f6f6;"></iframe>
                </div>
              </div> -->
            </div>
            <div role="tabpanel" class="tab-pane fade" id="profile">
              <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="bp__menu">
                  <ul id="aportaciones-list">
                  </ul>
                </div>
              </div>
              <div class="col-sm-12 col-md-9 col-lg-9">
                <div class="bp__book">
                  <iframe id="i-pdf-2" width="100%" height="100%" style="border: 1px solid #f5f6f6;"></iframe>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="estudio">
              <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="bp__menu">
                  <ul id="estudio-list">
                  </ul>
                </div>
              </div>
              <div class="col-sm-12 col-md-9 col-lg-9">
                <div class="bp__book">
                  <iframe id="i-pdf-3" width="100%" height="100%" style="border: 1px solid #f5f6f6;"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    <div id="modal-launcher"></div>
</div>



  <script id="mejores-list-template" type="x-text/handlebars" >
    {{#each this}}
      <div class="col-xs-12 col-sm-6 col-md-4">
        <md-card class="_md">
          <img src="assets/img/home/card-blank.png" class="md-card-image">
          <md-card-title>
            <md-card-title-text>
              <span class="md-headline">{{_titulo}}</span>
              <!-- <span class="md-subhead">Sub head</span> -->
            </md-card-title-text>
          </md-card-title>
          <md-card-content>
            <p>
              {{_descripcion}}
            </p>
            {{{_stars}}}
          </md-card-content>
          <md-card-actions layout="row" layout-align="start center" class="layout-align-start-center layout-row">
            <md-card-icon-actions>
              <button class="md-raised md-button md-ink-ripple" type="button" onclick="getDocument('uploads/aportaciones/{{_thumbnail}}', 'view_1')">Ver documento</button>
            </md-card-icon-actions>
          </md-card-actions>
        </md-card>
      </div>
    {{/each}}
  </script>

  <script id="aportaciones-list-template" type="x-text/handlebars" >
    {{#each this}}
        <li>
          <a class="btn btn-primary" href="#" onclick="getDocument('uploads/aportaciones/{{_thumbnail}}', 'view_2')">{{_titulo}}</a>
        </li>
    {{/each}}
  </script>
  <script id="estudio-list-template" type="x-text/handlebars" >
    {{#each this}}
        <li>
          <a class="btn btn-primary" href="#" onclick="getDocument('uploads/aportaciones/{{_thumbnail}}', 'view_3')">{{_titulo}}</a>
        </li>
    {{/each}}
  </script>
<script>
  $(function(){
    var aportacion = new Aportacion();
    var params = {};
    params._method = "mejores-list";
    aportacion._set(params);
  });
</script>
