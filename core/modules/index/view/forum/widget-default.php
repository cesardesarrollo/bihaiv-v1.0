<?php

  $logged = false;
  if(isset($_SESSION["user_id"])){ $logged = true; }

  /*
  if(!isset($_SESSION["user_id"])){
    Core::redir("./home/login.php");
  }*/
?>

<!-- Multiselect CDN -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/i18n/defaults-es_CL.min.js"></script>
<!-- END Multiselect CDN -->

<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-foro.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?=translate('Foro de Discusión')?></h1>
            <p class="description-gray">
              <?=translate('Descripción corta sección foro')?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
</div>
<div class="container">
  <div class="col-sm-12 col-lg-12 margin-row-top">
    <div class="panel panel-card">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <h3 class="title" ></h3>
            <p>
              <?=translate('Descripción sección foro')?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="Profile__wall">
    <div class="Forum">
      <div class="row padding-row">
        <div class="col-xs-12 col-lg-12">
        </div>
      </div>
      <div class="row row-margin-bottom">
        <div class="col-md-12">
          <div class="col-md-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="tabsForum">
              <li role="presentation" class="active">
                <a href="#discusion" aria-controls="discusion" role="tab" data-toggle="tab"><?=translate('Temas nuevos')?></a>
              </li>
              <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?=translate('Más visitados')?></a>
              </li>
              <li role="presentation">
                <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><?=translate('Más votados')?></a>
              </li>
              <?php if($logged): ?>
              <li role="presentation">
                <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><?=translate('Mis temas')?></a>
              </li>
              <?php endif; ?>
              <li role="presentation">
                <a href="#buscador-tab" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-search"></i> &nbsp; <?=translate('Buscador')?></a>
              </li>
              <?php if($logged): ?>
              <li role="presentation">
                <button type="button" class="btn btn-primary" style="padding: 11px;" onclick="openModal('m-new-topic',<?php echo @SESSION::$organizacion->id; ?>)">
                  <i class="fa fa-plus fa-fw" ></i>&nbsp;  <?=translate('Nuevo tema')?>
                </button>
              </li>
              <?php endif; ?>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding:20px;">
              <div role="tabpanel" class="tab-pane fade in active" id="discusion"></div>
              <div role="tabpanel" class="tab-pane fade" id="profile">
              </div>
              <div role="tabpanel" class="tab-pane fade" id="messages">
              </div>
              <div role="tabpanel" class="tab-pane fade" id="settings">
              </div>
              <div role="tabpanel" class="tab-pane fade" id="buscador-tab">
                <div class="row">
                  <div class="col-md-12">
                    <div class="Card z-depth-1">
                      <div class="Card--content" style="text-align: right;">
                        <form class="form-inline" id="search-form">
                          <div class="form-group">
                            <input type="text" id="search-input" placeholder="<?=translate('Ingresa tu búsqueda...')?>">
                          </div>
                          <style>
                          #search-input{
                            margin-top: 0;
                            background: 0 0;
                            padding: 2px 2px 1px;
                            border-width: 0 0 1px;
                            line-height: 26px;
                            height: 34px;
                            -ms-flex-preferred-size: 26px;
                            border-radius: 0;
                            border-style: solid;
                            width: 100%;
                            box-sizing: border-box;
                            float: left;
                            border-color: rgba(0,0,0,0.12);
                            font-size: 100%;
                            vertical-align: baseline;
                            webkit-transition: 0.2s all ease-in-out;
                            -moz-transition: 0.2s all ease-in-out;
                            -o-transition: 0.2s all ease-in-out;
                            -ms-transition: 0.2s all ease-in-out;
                            transition: 0.2s all ease-in-out;
                            }#search-input:focus,#search-input:active{
                              padding-bottom: 0;
                              border-width: 0 0 2px;
                              border-color: #f37064;
                              outline: none !important;
                            }
                            </style>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="search-results"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <div id="modal-launcher"></div>
</div>
<style>
  .avatar-foro{
    background-position: center center;
    background-size: cover;
    background-color: #f37064;
  }
  .privacy-badge{
    border: solid #f37064 1px;
    color: #f37064;
    padding: 6px 12px;
    margin-bottom: 40px;
    font-size: 14px;
  }
  .margin-row-bottom-8 {
    margin-bottom: 8px !important;
  }
  .Answer{
    padding: 15px 0px 15px 0px;
    border-radius: 2px;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);
  }
</style>
<!-- DISCUSIÓN TEMPLATE -->
<script id="discusion-template" type="x-text/handlebars">
    {{#each this}}
		<div class="row margin-row-bottom-8">
			<div class="">
    			<div class="Answer">
    				<div class="col-xs-12 col-md-2 col-lg-2 text-center">
    					<figure class="figure margin-row-bottom">
    						<div class="img-rounded avatar-foro center-block" style="background-image: url({{user_avatar}})"></div>
    					</figure>
              {{#if privacidad }}
                <p>
                  <span class="privacy-badge" style="">
                    <i class="fa fa-unlock" aria-hidden="true"></i> <?=translate('PÚBLICO')?>
                  </span>
                </p>
              {{else}}
                <p>
                  <span class="privacy-badge" style="">
                    <i class="fa fa-lock" aria-hidden="true"></i> <?=translate('PRIVADO')?>
                  </span>
                </p>
              {{/if}}
    					<p class="margin-row-bottom">{{created_at}}</p>
    					<p class="margin-row-bottom">{{{stars}}}</p>
    				</div>
    				<div class="col-xs-12 col-md-10 col-lg-10">
    					<h5><strong>{{user_name}}</strong> pregunta:</h5>
    					<h4><a href="?view=forum-detail&id={{idTema}}">{{titulo}}</a></h4>
    					<p>
    						{{descripcion}}
    					</p>
    					<p>
    						<a href="?view=forum-detail&id={{idTema}}" class="btn btn-primary"><?=translate('Ver tema')?></a>
    					</p>
    				</div>
    			</div>
			</div>
		</div>
    {{/each}}
</script>
<script>
  $(function(){
    // ENTRADAS MAS NUEVAS
    var discusion = new Discusiones();
    var params = {
      '_method'  : "all"
    };
    discusion._set(params);
    // ENTRADAS MAS VISITADAS
    var discusion2 = new Discusiones();
    var params = {
      '_method'  : "all_most_visited"
    };
    discusion2._set(params);
    // ENTRADAS MEJOR CALIFICADAS
    var discusion2 = new Discusiones();
    var params = {
      '_method'  : "all_most_voted"
    };
    discusion2._set(params);

    <?php if($logged): ?>
    // MIS ENTRADAS
    var misEntradas = new Discusiones();
    var params = {
      '_method'  : "all_my_topics",
      '_userid'  : '<?=(isset($_SESSION["user_id"])) ? SESSION::$organizacion->id : "" ?>'
    };
    misEntradas._set(params);
    <?php endif; ?>

    // SEERCH BOX
    $formSearch = $('#search-form');
    $formSearch.on('submit',function(e){
      e.preventDefault();
      var search = new Discusiones();
      var parameters = {
        '_method'           : 'search_topics',
        '_search'           : $('#search-input').val(),
        '_organizacion_id'  : '<?=(isset($_SESSION["user_id"])) ? SESSION::$organizacion->id : "" ?>'
      }
      search._set_search(parameters);
    });
  });
</script>
<script>
  $('#tabsForum a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })
</script>
