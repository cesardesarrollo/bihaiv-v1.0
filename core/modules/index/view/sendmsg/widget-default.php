<?php
$user = UserData::getById($_GET["id"]);
?>
<div class="container padding-top-100">
  <div class="row">
      <div class="col-md-3">
      <?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>Session::$organizacion,"from"=>"logged" ));?>
      <?php Action::execute("_mainmenu",array());?>
      </div>
      <div class="col-md-7">
      <h2><?=translate('Enviar Mensaje')?></h2>
      <form role="form" method="post" action="./?action=sendmsg">
        <div class="form-group">
          <label for="exampleInputEmail1"><?=translate('Usuario')?></label>
          <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $user->getFullname(); ?>" placeholder="<?=translate('Email')?>">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1"><?=translate('Mensaje')?></label>
          <textarea class="form-control" name="content" required placeholder="<?=translate('Mensaje')?>" rows="5"></textarea>
        </div>
        <input type="hidden" name="ref" value="new">
        <input type="hidden" name="receptor_id" value="<?php echo $user->id; ?>">
        <button type="submit" class="btn btn-default"><?=translate('Enviar')?></button>
      </form>

      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
</div>
<br><br><br><br><br>