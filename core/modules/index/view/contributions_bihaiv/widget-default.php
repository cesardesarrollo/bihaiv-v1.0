<?php


  $logged = false;
  if(isset($_SESSION["user_id"])){ $logged = true; }

/*
  if(isset($_GET["id"])){
    $user = UserData::getById($_GET["id"]);
    $profile = ProfileData::getByUserId($_GET["id"]);
  } else {
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $user = UserData::getById($_SESSION["user_id"]);
      $profile = ProfileData::getByUserId($_SESSION["user_id"]);
      if(!$user->is_valid || $user->is_admin || $user->tipoRel!=1) {
        Core::redir("./");
      }
    }
  }
  require_once('./core/modules/index/model/DaoEvento.php');
  require_once('./core/modules/index/model/DaoOrganizacion.php');
  $DaoOrganizacion = new DaoOrganizacion();

  if(isset($_GET["id"])){
    $user = UserData::getById($_GET["id"]);
    $organizacion = $DaoOrganizacion->getByUserId($_GET["id"]);
  } else {
    if(!isset($_SESSION["user_id"])){
      Core::redir("./");
    } else {
      $user = UserData::getById($_SESSION["user_id"]);
      $organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);
      if(!$user->is_valid) {
        Core::redir("./");
      }
    }
  }*/
  $show = 2;
  if(isset($_GET['show']) && $_GET['show'] > 1){
    $show = $_GET['show'];
  }
  $DaoSeccionAportacion = new DaoSeccionAportacion();
  $datosSeccion = $DaoSeccionAportacion->getById($show);

?>
<!-- CSS AND JS -->
<link rel="stylesheet" href="assets/css/contributions_categories.css">

<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
  <?php include('./views/main_menu.php'); ?>
  <!-- here comes new challenger -->
  <div class="gray-header col-md-12">
    <div class="gray-text">
      <div class="container">
        <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
          <!-- img -->
          <span class="helper"></span>
          <img src="assets/img/home/header-aportaciones.png" class="img-header img-resposive" />
        </div>
        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
          <?php 
            if (isset($_COOKIE['bihaiv_lang'])): 
              if ($_COOKIE['bihaiv_lang'] == 'es'): 
          ?>
          <h1 class="title-gray"><?php echo $datosSeccion->getTitulo(); ?></h1>
          <p class="description-gray">
            <?php echo $datosSeccion->getDescripcion(); ?>
          </p>
          <?php 
              else:
          ?>
          <h1 class="title-gray"><?php echo $datosSeccion->getTitle(); ?></h1>
          <p class="description-gray">
            <?php echo $datosSeccion->getDescription(); ?>
          </p>
          <?php
              endif;
            else:
          ?>
          <h1 class="title-gray"><?php echo $datosSeccion->getTitulo(); ?></h1>
          <p class="description-gray">
            <?php echo $datosSeccion->getDescripcion(); ?>
          </p>
          <?php
            endif;
          ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End here comes new challenger -->
</div>
<div class="container">
  <br>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-card">
        <div class="panel-body">
          <form class="form-inline" id="filtersform">
            <div class="form-group">
              <label for="exampleInputName2"><?= translate('Mostrar')?></label>
              <select class="form-control" id="selectFilter">
                <option value="fecha_desc"><?= translate('Mas recientes')?></option>
                <option value="fecha_asc"><?= translate('Mas viejos')?></option>
                <option value="most_visited"><?= translate('Mas vistas')?></option>
                <option value="less_visited"><?= translate('Menos vistas')?></option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> &nbsp;  <?= translate('Filtrar')?></button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-xs-12  col-sm-8  col-md-9 col-lg-10">
      <div class="row" id="contributions-grid">
      </div>
    </div>
    <div class="col-xs-12  col-sm-4  col-md-3 col-lg-2">
      <div class="row">
        <div class="col-md-12">
          <ul class="list-group" id="calendario-container">
          </ul>
        </div>
      </div>
    </div>
  </div>
  <br>
  <br>
  <div id="modal-launcher"></div>
  <!-- handlebars template -->
  <script id="aportacion-template" type="x-text/handlebars" >
      {{#each this}}
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <md-card class="_md">
          <md-card-header>
            <img class="img-responsive" src="{{thumbnail}}" />
            <div class="img-placeholder"></div>
            <h4>{{titulo}}</h4>
          </md-card-header>
          <md-card-content>
            <small><i class="fa fa-calendar"></i> &nbsp; {{fechaCreado}}</small>
            <br>
            <small><i class="fa fa-eye"></i> &nbsp; <span class="badge">{{visitas}}</span></small>
          </md-card-content>
          <?php if($logged): ?>
          <div class="">
            <a href="{{file}}" target="_blank" class="btn btn-block btn-primary register-View" data-aportacionid="{{idAportacion}}" ><?=translate('Descargar')?></a>
          </div>
          <?php endif; ?>
        </md-card>
      </div>
      {{/each}}
  </script>
  <script id="calendario-template" type="x-text/handlebars" >
      {{#each this}}
        <li class="list-group-item">{{mes}}<span class="badge">{{count}}</span> </li>
      {{/each}}
  </script>



  <script>
    var aportacionesContainer = $('#contributions-grid');
    var aportacionTemplate    = $('#aportacion-template');

    var form                  = $('#filtersform');
    var selectFilter          = $('#selectFilter');

    var calendarioContainer   = $('#calendario-container');
    var calendarioTemplate    = $('#calendario-template');

    $(function(){
      var parametros = {
        method : 'aportaciones_bihaiv',
        order_by: 'fecha_desc'
      };
      getAportacionesBihaiv(parametros);
      /* Load calendario */
      getCountCalendario('bihaiv');
      /* handler events */
      form.on('submit', function(e){
        e.preventDefault();
        var parametros = {
          method : 'aportaciones_bihaiv',
          order_by: selectFilter.val()
        };
        getAportacionesBihaiv(parametros);
      });

      /* Count visitas */
      /* Handler click event on delegate selector */
      $("#contributions-grid").on('click', '.register-View', function () {
        $.post('api/front/aportacion.php',{method: 'aportaciones_visitas',aportacion_id: $(this).data('aportacionid')});
        var parametros = {
          method : 'aportaciones_bihaiv',
          order_by: 'fecha_desc'
        };
        getAportacionesBihaiv(parametros);
      });

    });

    function getAportacionesBihaiv(parametros){
      $.post('api/front/aportacion.php', parametros, function(data){
        var template = Handlebars.compile( aportacionTemplate.html() );
        aportacionesContainer.empty().html( template( data ) );
      });
    }
    function getCountCalendario(tipo){
      /*
      * Tipo es un string, puede ser 'ecosistema' o 'bihaiv'
      */
      $.post('api/front/aportacion.php', {method: 'count_calendario', tipo: tipo}, function(data){
        console.log(data);
        var template = Handlebars.compile( calendarioTemplate.html() );
        calendarioContainer.html( template( data ) );
      });
    }
  </script>
</div>
