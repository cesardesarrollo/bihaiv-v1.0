<div  class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-expertos.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?php echo translate('Cómo funciona'); ?></h1>
            <p class="description-gray">
              <?=translate("Titulo Como Funciona")?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
    <div class="cards-container col-md-12">

      <div class="panel panel-card">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <?=translate("Texto descriptivo Como Funciona")?>
            </div>
            <div class="col-md-4">
              <center>
                  <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
              </center>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-card">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <h3 class="title" >1</h3>
              <p>
                <?=translate("Paso 1")?>
              </p>
            </div>
            <div class="col-md-4">
              <center>
                  <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
              </center>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-card">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <h3 class="title" >2</h3>
              <p>
                <?=translate("Paso 2")?>
              </p>
            </div>
            <div class="col-md-4">
              <center>
                  <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
              </center>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-card">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <h3 class="title" >3</h3>
              <p>
                <?=translate("Paso 3")?>
              </p>
            </div>
            <div class="col-md-4">
              <center>
                  <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
              </center>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-card">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <h3 class="title" >4</h3>
              <p>
                <?=translate("Paso 4")?>
              </p>
              <div class="row">
                <div class="col-md-9 col-md-offset-3">
                  <?=translate("Texto descriptivo paso 4")?>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <center>
                  <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
              </center>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-card">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8">
              <h3 class="title" ></h3>
              <?=translate("Fin Como Funciona")?>
            </div>
            <div class="col-md-4">
              <center>
                  <img src="assets/img/home/temp-sprite.png" class="img-resposive" />
              </center>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="modal-launcher"></div>
</div>
