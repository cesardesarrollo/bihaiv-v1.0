<?php

//Comprueba sesión activa
if(!isset($_SESSION["user_id"])){ Core::redir("./"); }

$user = UserData::getById($_GET["uid"]);
$frs = FriendData::getFriends($_GET["uid"]);

require_once 'core/modules/index/model/DaoOrganizacion.php';

if($user!=null):
  $DaoOrganizacion = new DaoOrganizacion();
  $organizacion = $DaoOrganizacion->getByUserId($user->id);
?>
<div class="container padding-top-100">
  <div class="row">
      <div class="col-md-3">
        <?php
          $from = "logout";
          if(isset($_SESSION["user_id"])){ $from="logged"; }
          Action::execute("_userbadge",array("user"=>$user,"organizacion"=>$organizacion ,"from"=>$from));
          Action::execute("_usermenu",array("user"=>$user,"from"=>$from));
        ?>
      </div>
      <div class="col-md-7">
      <h2><?=translate('Amigos')?></h2>
      <?php if(count($frs)>0):?>
        <table class="table table-bordered">
        <thead>
          <th><?=translate('Amigo')?></th>
          <th></th>
        </thead>
        <?php foreach($frs as $fr):?>
          <tr>
            <td>
            <?php  
              if($fr->receptor_id==$user->id){
                echo $fr->getSender()->getFullname();
              }else{
                echo $fr->getReceptor()->getFullname();          
              }
            ?>
            </td>
            <td style="width:90px;">
              <a href="./?view=home&id=<?php if($fr->sender_id==$user->id){ echo $fr->receptor_id; }else { echo $fr->sender_id; } ?>" class="btn btn-default btn-xs"><?=translate('Ver Perfil')?></a>
            </td>
          </tr>
        <?php 
        $fr->read();
        endforeach; ?>
        </table>
      <?php else:?>
        <div class="jumbotron">
        <h2><?=translate('No hay Amigos')?></h2>
        </div>
      <?php endif;?>
      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
<?php endif; ?>
<br><br><br><br><br>