<?php
require_once './core/modules/index/model/DaoImagenHome.php';
require_once './core/modules/index/model/DaoVideosHome.php';
$ImagenHome = new DaoImagenHome();
$VideosHome = new DaoVideosHome();
?>
<style>
  #map {
    height: 100%;
  }
  #floating-panel {
    position: absolute;
    top: 10px;
    left: 25%;
    z-index: 999;
    background-color: #fff;
    padding: 5px;
    border: 1px solid #999;
    text-align: center;
    font-family: 'Roboto','sans-serif';
    line-height: 30px;
    padding-left: 10px;
  }
  .owl-carousel .item-video, .item {
    height: 600px !important;
  }
  .Slider{
    margin-bottom: 60px;
  }
	/* Centered texts in each section
	* --------------------------------------- */
  .box-description-slide{
    word-wrap: break-word;
  }
  .box-description-slide-video{
    position: absolute;
    top: 39vh;
    z-index: 100;
    max-width: 80vw;
    width: 80vw;
    left: 10vw;
    word-wrap: break-word;
  }
  .box-description-slide h3,
  .box-description-slide-video h3 {
    color: white;
    margin:0;
    font-size: 40px;
    text-shadow: 4px 4px 6px rgba(0,0,0,0.4);
    font-family: raleway;
    font-weight: 600;
  }
	.section{
		text-align:center;
    overflow : hidden;
    position:relative;
	}
	#myVideo{
		position: relative;
		right: 0;
		bottom: 0;
		top:0;
		right:0;
		width: 100%;
		height: 100%;
		background-size: 100% 100%;
 		background-color: black; /* in case the video doesn't fit the whole page*/
    background-image: /* our video */;
    background-position: center center;
    background-size: contain;
    object-fit: cover; /*cover video background */
    z-index:3;
	}
	/* Layer with position absolute in order to have it over the video
	* --------------------------------------- */
	#section0 .layer{
		position: absolute;
		z-index: 100;
		width: 100%;
		left: 0;
		top: 43%;
	}
	/*solves problem with overflowing video in Mac with Chrome */
	#section0{
		overflow: hidden;
	}
	/* Bottom menu
	* --------------------------------------- */
	#infoMenu li a {
		color: #fff;
	}
</style>
<?php include ('views/header_fixed.php'); ?>
<div id="fullpage">
  <div class="section " id="section1" style="padding-top:55px">
    <?php
    /*
    * VIDEO DEL HOME
    *  Se carga solo un video de que este activado para el home, en caso de
    * que no se active ninguno entonces no se carga nada.
    */
    $videoHome = $ImagenHome -> getVideoHome();
    ?>
    <?php if (!empty($videoHome) AND file_exists('admin/files/videos-home/'.$videoHome['ruta']) ): ?>
      <div class="slide" id="slide1">
        <video data-autoplay data-keepplaying loop muted id="myVideo">
          <source src="admin/files/videos-home/<?php echo $videoHome['ruta']; ?>" type="video/mp4">
        </video>
        <div class="container">
          <div class="box-description-slide-video">
            <!-- Descripción del video del home -->
            <?php if (isset($_COOKIE['bihaiv_lang'])): ?>
              <?php if ($_COOKIE['bihaiv_lang'] == 'es'): ?>
                <!-- Español -->
                <h3 class=""> <?php echo $videoHome['descripcion']; ?></h3>
              <?php else: ?>
                <h3 class=""> <?php echo $videoHome['description']; ?></h3>
              <?php endif; ?>
            <?php else: ?>
              <!-- Español -->
              <h3 class=""> <?php echo $videoHome['descripcion']; ?></h3>
            <?php endif; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php $counter = 2; foreach ($ImagenHome -> getAllInPortada() as $img) :?>
      <div class="slide" id="slide<?php echo $counter?>" style="
        padding: 37vh 0 0 0;
        background: url('admin/files/<?php echo $img->getLlaveImg()?>') no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        ">
        <div style="margin-left: 10vw;width: 80vw;">
          <div class="box-description-slide">
            <h3 class="">
                <?php
                  if ( isset($_COOKIE['bihaiv_lang']) && $_COOKIE['bihaiv_lang'] == "en") {
                    echo $img->getDescription();
                  } else {
                    echo $img->getDescripcion();
                  }
                ?>
            </h3>
          </div>
      </div>
    </div>
    <?php $counter++; endforeach; ?>
  </div>
  <div class="section" id="section2" style="padding-top:60px">
        <div class="Panal">
            <div class="container">
              <div class="row margin-row-top">
                <div class="col-md-12">
                  <h2 class="section-title text-center uppercase bolder">
                    <?php echo translate('Explora bihaiv'); ?> <hr/>
                  </h2>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row margin-row-top">
                <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-3 col-md-6 col-md-offset-4 col-lg-6 col-lg-offset-4">
                  <div class="panalContainer">
                    <div class="profileIcon">
                      <a class="icon" href="?view=catalogs" ></a>
                        <div class="popover position-roles hidden-xs">
                          <div class="arrow"></div>
                          <h4><?=translate('Roles'); ?></h4>
                          <div class="popover-content">
                            <p>
                              <?=translate('Panal Descripción corta Roles')?>
                              <br>
                              <a href="?view=catalogs"><?=translate('Ver más')?></a>
                            </p>
                          </div>
                      </div>
                  </div>
                  <div class="socialIcon">
                    <a class="icon" href="?view=map"></a>
                      <div class="popover position-map hidden-xs">
                        <div class="arrow"></div>
                        <h4><?=translate('Mapa del ecosistema'); ?></h4>
                        <div class="popover-content">
                          <p>
                            <?=translate('Panal Descripción corta Mapa')?>
                            <br>
                            <a href="?view=map"><?=translate('Ver más')?></a>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div class="videoIcon">
                    <a class="icon" href="?view=best_practices"></a>
                    <div class="popover position-library hidden-xs">
                      <div class="arrow"></div>
                      <h4><?=translate('Biblioteca del Ecosistema'); ?></h4>
                      <div class="popover-content">
                      <p>
                        <?=translate('Panal Descripción corta Biblioteca')?>
                        <br>
                        <a href="?view=best_practices"><?=translate('Ver más')?></a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="foroIcon">
                  <a class="icon" href="?view=forum"></a>
                  <div class="popover position-forum hidden-xs">
                    <div class="arrow"></div>
                    <h4><?=translate('Foro de Discusión'); ?></h4>
                    <div class="popover-content">
                      <p>
                        <?=translate('Panal Descripción corta Discusión')?>
                        <br>
                        <a href="?view=forum"><?=translate('Ver más')?></a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="calendarioIcon">
                  <a class="icon" href="?view=calendar"></a>
                  <div class="popover position-calendar hidden-xs">
                    <div class="arrow"></div>
                    <h4><?=translate('Eventos'); ?></h4>
                    <div class="popover-content">
                    <p>
                      <?=translate('Panal Descripción corta Eventos')?>
                      <br>
                      <a href="?view=calendar"><?=translate('Ver más')?></a>
                    </p>
                  </div>
                </div>
              </div>
              <div class="bdIcon">
                <a class="icon" href="?view=experts"></a>
                <div class="popover position-expertises hidden-xs">
                  <div class="arrow"></div>
                  <h4><?=translate('Consejo de Expertos'); ?></h4>
                  <div class="popover-content">
                  <p>
                    <?=translate('Panal Descripción corta Expertos')?>
                    <br>
                    <a href="?view=experts"><?=translate('Ver más')?></a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!--END SECOND SECTION-->

<div class="section padding-row" id="section3" style="padding-top:75px">

  <div class="History map-container-home">
      <h2 class="section-title float-title z-depth-half" style="z-index:100">
        <?php echo translate('Historias del ecosistema'); ?>
        <hr class="white"/>
      </h2>
      <?php
      require_once './core/modules/index/model/DaoVideosHome.php';
      $DaoVideosHome = new DaoVideosHome();
      $VideosHomes = $DaoVideosHome->getAllYoutube();
      ?>
      <?php $counter_videos = 1; foreach( $VideosHomes as $v): ?>
        <div class="slide slide_history" id="slide<?php echo $counter_videos ?>">
          <iframe width="100%" src="https://www.youtube.com/embed/<?php echo $v->ruta?>" frameborder="0" allowfullscreen></iframe>
        </div>
      <?php $counter_videos++; endforeach; ?>
  </div>
</div> <!--END THIRD SECTION-->
<div class="section" id="section4" style="padding-top:75px">
  <div class="map-container-home section" id="section3">
    <h2 class="section-title float-title z-depth-half">
      <?php echo translate('Mapa del ecosistema'); ?> <br>
      <a href="?view=map" class="btn btn-outline-white"><?=translate('Ver mapa')?></a>
      <hr class="white"/>
    </h2>
    <div id="map_canvas" style="width: 100%; height: 100vh; margin: 0 auto;"></div>
  </div>
</div><!--END FOURTH SECTION-->
<div class="section" id="section5" style="padding-top:75px">
  <footer style="height:100%">
    <div class="container">
      <div class="col-md-3">
        <a href="<?=APP_PATH?>">
          <img src="assets/img/home/logo_bihaiv-white.png" class="img-footer  center-block" />
        </a>
      </div>
      <div class="col-md-3">
        <ul>
          <li>
            <a href="<?=APP_PATH?>" class="footer-title">
                <?=translate('Inicio')?>
            </a>
          </li>
          <li>
            <a href="<?=APP_PATH?>?view=forum" class="footer-title">
                <?=translate('Foro de Discusión')?>
            </a>
          </li>
          <li>
            <a href="<?=APP_PATH?>?view=calendar" class="footer-title">
                <?=translate('Eventos')?>
            </a>
          </li>
          <li>
            <a href="<?=APP_PATH?>?view=map" class="footer-title">
                <?=translate('Mapa')?>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul>
          <li>
            <a href="<?=APP_PATH?>?view=experts" class="footer-title">
                <?=translate('Expertos')?>
            </a>
          </li>
          <li>
            <a href="<?=APP_PATH?>?view=catalogs" class="footer-title">
                <?=translate('Roles')?>
            </a>
          </li>
          <li>
            <a href="<?=APP_PATH?>?view=best_practices" class="footer-title">
                <?=translate('Biblioteca del Ecosistema')?>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-md-3">
        <!-- Sin session -->
        <?php if(!Session::exists("user_id")):?>
          <span class="footer-title"><?=translate('Acceso Miembros')?></span>
          <br><br>
          <form action="?action=processlogin" role="search" method="post">
            <input autocomplete="off" type="hidden" name="token" value="0">
            <div class="form-group">
              <label for="txt__Username"><?=translate('Usuario')?>:</label>
              <input autocomplete="off" type="email" id="txt__Username" name="email" class="form-control" placeholder="Email" value="" required="">
            </div>
            <div class="form-group">
              <label for="txt__Password"><?=translate('Contraseña')?>:</label>
              <input autocomplete="off" type="password" id="txt__Password" name="password" class="form-control" placeholder="Contraseña" value="" required="">
            </div>
            <button id="oAuth" type="submit" class="btn btn-primary btn-block"><?=translate('Iniciar Sesión')?></button>
            <div class="checkbox">
              <label>
                <input autocomplete="off" name="recuerdame" type="checkbox"><?=translate('Recuérdame')?>
              </label>
            </div>
            <div class="form-group margin-row-top">
              <a href="<?=APP_PATH?>home/recordar-contrasena.php"><?=translate('Olvidaste tu Contraseña?')?></a><br>
            </div>
          </form>
          <!-- con session iniciada -->
        <?php else:?>
        <?php endif;?>
      </div>
    </div>
    <section class="Links">
      <div class="container">
        <div class="pull-left">
          <span class="">Copyright © <?php echo date('Y') ?> <a href="http://mitefmexico.org" target="_NEW"> MIT Enterprise Forum México </a> </span>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb no-margin no-padding-top no-padding-bottom">
            <li>
              <a href="https://www.facebook.com/MITenterpriseforumMexico">
                <img src="assets/img/home/FB.PNG" alt="" height="20">
              </a>
            </li>
            <li>
              <a href="https://twitter.com/MITEFMexico">
                <img src="assets/img/home/TWITTER.PNG" alt="" height="20">
              </a>
            </li>
            <li>
              <a href="https://www.youtube.com/channel/UCaIVuCcA0bup9rNk_mMzWEw">
                <img src="assets/img/home/YOUTUBE.PNG" alt="" height="20">
              </a>
            </li>
          </ol>
        </div>
      </div>
    </section>
  </footer>
</div>
</div>

<script>
  /*
  // MAPA DE CALOR POR COORDENADAS
  */
  var map, heatmap;
  function initMap(points) {
    map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 5,
      center: {
        lat:  24.7334598,
        lng: -103.0381253
      },
      scrollwheel: false,
      styles: [
        {
          featureType: "all",
          stylers: [
            { saturation: -80 }
          ]
        },{
          elementType: "geometry",
          stylers: [
            { hue: "#f37064" },
            { saturation: 50 }
          ]
        },{
          featureType: "poi.business",
          elementType: "labels",
          stylers: [
            { visibility: "on" }
          ]
        }
      ]
    });
    heatmap = new google.maps.visualization.HeatmapLayer({
      data:  points,
      map: map
    });
  }

  $(function(){
    //Mapa de Calor de Actividad
    function getEventos(points){
      return $.post("api/front/coordenadas.php", {
              method : 'activityEvents'
      }).pipe(function(eventos){
          $.each(eventos, function(k,v){
            if (+v.lat > 0){
              points.push(new google.maps.LatLng( v.lat, v.lng ) );
            }
          });
          
          /* Inicializar mapa de Calor */
          initMap(points);
      });
    }  

    // INICIATIVAS
    $.post("api/front/coordenadas.php", { method : 'activityInitiatives' })
    .then(function(iniciativas) {
      var points = [];
      var geocoder = new google.maps.Geocoder();

      if(iniciativas.length>0){
        $.each(iniciativas, function(index, value) {
          geocoder.geocode({
              'address': value.Ubicación
          }, function(results, status) {
              if (status == 'OK') {
                  points.push( new google.maps.LatLng( results[0].geometry.location.lat(), results[0].geometry.location.lng() ));

                  if(points.length === iniciativas.length){
                    getEventos(points);
                  }
              }
          });
        });
      } else {
        getEventos(points);
      }
    });


    /* FullPage */
    $fullpageSelector = $('#fullpage');
    var slider1Interval;

    var timmingSlider_1 = 10000;

    $fullpageSelector.fullpage({
      verticalCentered: false,
      afterRender: function(){
        /* set automatic slider */
        slider1Interval = setInterval(function(){
          $.fn.fullpage.moveSlideRight();
        },timmingSlider_1);
      },
      afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){
        if(index == 1){
          clearInterval(slider1Interval);
          slider1Interval = setInterval(function(){
            $.fn.fullpage.moveSlideRight();
          },timmingSlider_1);
        }else{
          clearInterval(slider1Interval);
        }
      },
      css3 : false
    });
  });
</script>
