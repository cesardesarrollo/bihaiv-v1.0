<?php if(isset($_SESSION["user_id"])){ Core::redir("./");}
  //RECUPERACIÓN DE CUENTAS DE USUARIOS
  if (isset($_GET['e']) && isset($_GET['c']) ) {
    $email = $_GET['e'];
    $code = $_GET['c'];
    $user = UserData::getByEmailSha($email);
    if($user->is_active == 1){
      if(isset($_POST['password'])){
        if (!isset($_POST['password']) || !isset($_POST['repassword'])){
          Core::alert(translate('Debe ingresar todos los campos'));
        } else if ($_POST['password'] !== $_POST['repassword']){
          Core::alert(translate('Las contraseñas no coinciden'));
        } else if (strlen($_POST['password']) < 8){
          Core::alert(translate('Password debe contener al menos 8 caracteres'));
        } else{
          $sql = "UPDATE user SET password = '".sha1(md5($_POST['password']))."' WHERE sha1(md5(email)) = '$email' AND sha1(md5(code)) = '$code'";
          $response = Executor::doit($sql);
          if($response[0]==1){
            // Validar $response para ejecutar lo siguiente
            $msg = translate('Texto de correo enviado');
            Core::mail($user->email,"Bihaiv - ".translate('!Cambio de contraseña exitoso¡'),$msg,$msg);
            Core::alert(translate('!Cambio de contraseña exitoso¡'));
            Core::redir('./');
          } else {
            Core::alert(translate('Error al guardar nuevo password'));
          }
        }
      }
      ?>
      <div style="background-color: white;">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
              <!-- <section class="Profile__wall"> -->
                <br><br><br>
                <br><br><br>
                <h1 class="text-black no-margin margin-row-bottom"><?=translate('Cambia tu password')?></h1>
                <form action="./?view=recoveraccount&e=<?php echo $email; ?>&c=<?php echo $code; ?>" role="search" method="post" id="formRecupera" >
                  <div class="form-group">
                    <label for="password"><?=translate('Password')?>:</label>
                    <input autocomplete="off" type="password" id="password" name="password" class="form-control" placeholder="<?=translate('Password')?>" required>
                  </div>
                  <div class="form-group">
                    <label for="repassword"><?=translate('Repite password')?>:</label>
                    <input autocomplete="off" type="password" id="repassword" name="repassword" class="form-control" placeholder="<?=translate('Repite password')?>" required>
                  </div>
                  <button id="oAuth" type="submit" class="btn btn-primary btn-block"><?=translate('Cambia tu passsword')?></button>
                </form>
                <br><br><br>
              <!-- </section> -->
            </div>
          </div>
        </div>
      </div>
  <?php
    } else {
      Core::alert("<?=translate('¡Esta cuenta no ha sido activada!')?>");
    }
  }
  ?>
