<?php // if(!isset($_SESSION["user_id"])){ Core::redir("./");} ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/i18n/defaults-es_CL.min.js"></script>
<?php
/* HERE COMES NEW CHALLENGER!!! (PHP) */
$Usuarios = new DaoUsuarios();
$actores = $Usuarios -> getAll();

$Roles = new DaoRoles();
$roles_list = $Roles -> getAll();

$Expertos = new DaoExperto();
$expertos = json_decode(json_encode($Expertos -> getAll()), true);
?>
<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
    <?php include('./views/main_menu.php'); ?>
    <!-- here comes new challenger -->
    <div class="gray-header col-md-12">
      <div class="gray-text">
        <div class="container">
          <div class="img-helper col-xs-6 col-xs-offset-3 col-sm-3 col-sm-push-9 col-sm-offset-0 col-md-3 col-md-push-9 col-md-offset-0">
            <!-- img -->
            <span class="helper"></span>
            <img src="assets/img/home/header-map.png" class="img-header img-resposive" />
          </div>
          <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3">
            <h1 class="title-gray"><?=translate('Mapa del Ecosistema')?></h1>
            <p class="description-gray">
              <?=translate('Descripción corta sección mapa')?>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End here comes new challenger -->
</div>
<div class="container">
  <div class="col-sm-12 col-lg-12 margin-row-top">
      <div class="panel panel-card">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <h3 class="title" ></h3>
                <p>
                <?=translate('Descripción sección mapa')?>
                </p>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-12 margin-row-bottom">
      <div class="panel panel-card">
        <div class="panel-body">
          <br>
          <!-- <md-card class="_md"> -->
          <!-- <md-card-content> -->
          <div class="col-md-12 margin-row-bottom">
            <!-- SEARCH FORM -->
            <form class="form-inline" id="form-map" style="text-align: left;">
              <div class="row">

                <div class="col-md-3 col-lg-3">
                  <div class="form-group">
                    <label for="selectCategoria" class="x" style="display:none;"> <?=translate('Categoría')?>: </label>
                    <select id="selectCategoria" class="" style="display:none;">
                      <option value=""><?=translate('Selecciona una..')?></option>
                      <option value="ACTORES"><?=translate('Actores')?></option>
                      <option value="ACTIVIDAD"><?=translate('Actividad')?></option>
                      <option value="COLABORACION"><?=translate('Colaboración')?></option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3">
                  <div class="form-group" id="labelMostrar">
                    <label for="txt__StartDate" class="x" style="display:none;"> <?=translate('Mostrar')?>: </label>
                    <select id="categoriaFiltro" class="" style="display:none;" data-live-search="true" multiple>
                      <option value="all"><?=translate('Todos')?></option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3">
                  <div class="form-group" id="labelEstados">
                    <label for="estado" class="x" style="display:none;"> <?=translate('Estado')?>: </label>
                    <!-- <input id="estado"  type="text" class="form-control input-map" placeholder=""> -->
                    <select id="estado" class="" style="display:none;" data-live-search="true">
                      <option value=""><?=translate('Selecciona un estado')?></option>
                      <option value="Aguascalientes">Aguascalientes</option>
                      <option value="Baja California Sur">Baja California Sur</option>
                      <option value="Baja California Norte">Baja California Norte</option>
                      <option value="Campeche">Campeche</option>
                      <option value="Chiapas">Chiapas</option>
                      <option value="Chihuahua">Chihuahua</option>
                      <option value="Coahuila">Coahuila</option>
                      <option value="Colima">Colima</option>
                      <option value="Ciudad de México">Ciudad de México</option>
                      <option value="Durango">Durango</option>
                      <option value="Guanajuato">Guanajuato</option>
                      <option value="Guerrero">Guerrero</option>
                      <option value="Hidalgo">Hidalgo</option>
                      <option value="Jalisco">Jalisco</option>
                      <option value="Estado de México">Estado de México</option>
                      <option value="Michoacán">Michoacán</option>
                      <option value="Morelos">Morelos</option>
                      <option value="Nayarit">Nayarit</option>
                      <option value="Nuevo León">Nuevo León</option>
                      <option value="Oaxaca">Oaxaca</option>
                      <option value="Puebla">Puebla</option>
                      <option value="Querétaro">Querétaro</option>
                      <option value="Quintana Roo">Quintana Roo</option>
                      <option value="San Luis Potosí">San Luis Potosí</option>
                      <option value="Sinaloa">Sinaloa</option>
                      <option value="Sonora">Sonora</option>
                      <option value="Tabasco">Tabasco</option>
                      <option value="Tamaulipas">Tamaulipas</option>
                      <option value="Tlaxcala">Tlaxcala</option>
                      <option value="Veracruz">Veracruz</option>
                      <option value="Yucatán">Yucatán</option>
                      <option value="Zacatecas">Zacatecas</option>
                    </select>
                  </div>
                </div>
               </div>
            </form>
            </div>
          </div>
          <!-- </md-card-content> -->
          <!-- </md-card> -->
      </div>
      <div id="row-filters" class="row row-margin-bottom" style="display:none">
        <div id="row-filters-template"></div>
      </div>
    </div>
  </div>
<div class="col-md-12 no-padding row-margin-bottom" style="height:100%; overflow:hidden;">
  <div id="map" style="height: 100vh; width:100%;"></div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group pull-right" id="labelDescargar">
      <br>
      <button type="button" class="btn btn-success hide" id="btn_descargareporte"><i class="fa fa-file"></i>&nbsp; <?=translate('Descargar Reporte')?></button>
    </div>
  </div>
</div>
<div id="modal-launcher"></div>

<script>
  // GENERAR REPORTES
  function generaReportes(reporte_data, titulo) {
    <?php 
    // Solo usuarios logeados
    if(isset($_SESSION["user_id"])):
    ?>
    $('#btn_descargareporte').removeClass('hide');
    $('#btn_descargareporte').off('click').on('click',descargareporte);
    <?php endif; ?>
    function descargareporte(e){
      e.preventDefault();
      var reporte_name = "";
      
      if(titulo == 'Actividad'){
        JSONToCSVConvertor(reporte_data.eventos, "<?=translate('Reporte Actividad Por Eventos')?>", true);
        JSONToCSVConvertor(reporte_data.iniciativas, "<?=translate('Reporte Actividad Por Iniciativas')?>", true);
      } else if(titulo == 'Actores'){
          JSONToCSVConvertor(reporte_data.actores, "<?=translate('Actores')?>", true);
      } else if(titulo == 'Colaboración'){
        // Colaboración
        JSONToCSVConvertor(JSON.stringify(reporte_data), "<?=translate('Colaboración por estados')?>", true);
      }
    }
  }

  /*
  // MAPA POR ESTADOS
  */
  function getEstatesChart() {
    var estates = [];
    $.post('api/front/coordenadas.php', {method : 'colaboracion'}, function(response){
      ///JSON to CSV
      generaReportes(response, "Colaboración");
      // Dibuja Mapa
      drawRegionsMap(response);
    });
  }

  //Cargar el API de visualización y el paquete GeoChart.
  google.load('visualization', '1', {'packages': ['geochart']});
  //una devolución de llamada que se ejecutará cuando se carga el API de visualización de Google.
  google.setOnLoadCallback();
  function drawRegionsMap(data) {
    // se crea un arreglo, donde se indica que la primera linea como se va a particionar el mapa, nombre del estado y tamaño de la colaboración.
    var data = google.visualization.arrayToDataTable( data );
    var options =
    {
      legend: 'none', // se quita el slider indicador de colaboración
      //tooltip: {trigger:'none'}, //oculta el tooltip
      region: 'MX',   // region a dibujar en el mapa
      resolution: 'provinces',    //forma en la que se seccionará el mapa
      colorAxis: {colors: ['LightYellow', 'Salmon'] }
    };
    //se instacia y se dibuja el gráfico
    var chart = new google.visualization.GeoChart(document.getElementById('map'));
    chart.draw(data, options);
  };

  /*
  // MAPA DE CALOR POR COORDENADAS
  */
  var map, heatmap;
  function initMapHeat(points) {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: {
        lat:  24.5522147,
        lng: -102.7978886
      },
      scrollwheel: false,
      styles: [
        {
          featureType: "all",
          stylers: [
            { saturation: -80 }
          ]
        },{
          elementType: "geometry",
          stylers: [
            { hue: "#f37064" },
            { saturation: 50 }
          ]
        },{
          featureType: "poi.business",
          elementType: "labels",
          stylers: [
            { visibility: "on" }
          ]
        }
      ]
    });
      $('.x').show();
    heatmap = new google.maps.visualization.HeatmapLayer({
      data:  points,
      map: map
    });
  }

  // Guarda resultados desde api a objeto para exportación a CSV
  var objetoActividad = {}

  // Heatmap
  function getFromEvents(){
      return $.post("api/front/coordenadas.php", {
              method : 'activityEvents',
              estado: $selectEstado.val()
      }).pipe(function(eventos){
          // Añade resultados desde api a objeto para exportación a CSV
          objetoActividad.eventos = eventos;

          var points = [];
          $.each(eventos, function(k,v){
            if (+v.lat > 0){
              points.push(new google.maps.LatLng( v.lat, v.lng ) );
            }
          });
          return points;
      });
  }

  /* INICIALIZA EL MAPA NORMAL */
  function initMap() {
    // Specify features and elements to define styles.
    var styleArray = [
      {
        featureType: "all",
        stylers: [
          { saturation: -80 }
        ]
      },{
        elementType: "geometry",
        stylers: [
          { hue: "#f37064" },
          { saturation: 50 }
        ]
      },{
        featureType: "poi.business",
        elementType: "labels",
        stylers: [
          { visibility: "on" }
        ]
      }
    ];
    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat:  24.5522147, lng: -102.7978886},
      scrollwheel: false,
      zoom: 5,
      // Apply the map style array to the map.
      styles: styleArray,
    });
  }

  /* BUSCA UN ESTADO */
  var geocoder;
  var map;
  var markers = [];
  var address = "";
  function buscaEstado(address) {
    if (address === ""){
      initMap();
      if($selectCategoria.val()==="ACTIVIDAD"){
        $selectCategoria.change();
      }
    } else {
      geocoder = new google.maps.Geocoder();
      if (geocoder) {
        geocoder.geocode({
          'address': address
        }, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
              map.panTo(results[0].geometry.location);
              /* Smooth zoom */
              smoothZoom(map, 10, map.getZoom()); // call smoothZoom, parameters map, final zoomLevel, and starting zoom level
            } else {
              setFlash('<?=translate('No se encontraron resultados')?>', 'info');
            }
          } else {
            //setFlash("Geocode was not successful for the following reason: " + status, 'warning');
          }
        });
      }
    }
  }
  // the smooth zoom function
  function smoothZoom (map, max, cnt) {
    if (cnt >= max) {
      return;
    }
    else {
      z = google.maps.event.addListener(map, 'zoom_changed', function(event){
        google.maps.event.removeListener(z);
        smoothZoom(map, max, cnt + 1);
      });
      setTimeout(function(){map.setZoom(cnt)}, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
  }

  $(function(){
    // Inicializar mapa
       var points = [];
    initMapHeat(points);
    

    $categoriaFiltro = $('#categoriaFiltro');
    $selectEstado = $('#estado');
    $selectCategoria = $('#selectCategoria');

    $selectCategoria.css('display','none');
    $selectEstado.css('display','none');
    $categoriaFiltro.css('display','none');

    // Select Categoria
    $selectCategoria.selectpicker({
      style: 'btn-info',
      size: 8
    });

    // Select Estado
    $selectEstado.selectpicker({
      style: 'btn-info',
      size: 8
    });

    // Select Filtro
    $categoriaFiltro.selectpicker({
      style: 'btn-info',
      size: 8
    });


    $categoriaFiltro.on('change',function(e){
      e.preventDefault();
      if( $(this).val() ){
        if($(this).val()[0] == 'all'){
          $categoriaFiltro.selectpicker('deselectAll');
          $categoriaFiltro.selectpicker('val', 'all');
        } 
      }
      $selectEstado.change();
    });
 

    // Carga Filtros por categoria
    $selectCategoria.on('change',selectCategoria);
    function selectCategoria(e){
      e.preventDefault();
      var method = "";
      switch($(this).val()){
        case "ACTIVIDAD":
          objetoActividad = {};

          function getEventos(points){
            return $.post("api/front/coordenadas.php", {
                    method : 'activityEvents',
                    estado: $selectEstado.val()
            }).pipe(function(eventos){
                // Añade resultados desde api a objeto para exportación a CSV
                objetoActividad.eventos = eventos;

                $.each(eventos, function(k,v){
                  if (+v.lat > 0){
                    points.push(new google.maps.LatLng( v.lat, v.lng ) );
                  }
                });
                
                /* Inicializar mapa de Calor */
                initMapHeat(points);
                
                // JSON to CSV Iniciativas
                generaReportes(objetoActividad, "Actividad");
                
                // Según filtro de estado
                if( $selectEstado.val() != "" )
                  $selectEstado.change();
                $('#labelEstados').fadeIn();
            });
          }  

          // INICIATIVAS
          $.post("api/front/coordenadas.php", { method : 'activityInitiatives', estado: $selectEstado.val() })
          .then(function(iniciativas) {
            var points = [];
            var geocoder = new google.maps.Geocoder();

            // Añade resultados desde api a objeto para exportación a CSV
            objetoActividad.iniciativas = iniciativas;

            if(iniciativas.length>0){
              $.each(iniciativas, function(index, value) {
                geocoder.geocode({
                    'address': value.Ubicación
                }, function(results, status) {
                    if (status == 'OK') {
                        points.push( new google.maps.LatLng( results[0].geometry.location.lat(), results[0].geometry.location.lng() ));

                        if(points.length === iniciativas.length){
                          getEventos(points);
                        }
                    }
                });
              });
            } else {
              getEventos(points);
            }
          });

        break;
        case "COLABORACION":
          /* Inicializar mapa por Estados */
          getEstatesChart();
          $selectEstado.change();
          $('#labelEstados').fadeOut();
        break;
        case "ACTORES":
          /* Inicializar mapa Normal */
          initMap();
          method = 'get_roles';
          $selectEstado.change();
          $('#labelEstados').fadeIn();
        break;
      }

      var labelMostrar = $('#labelMostrar');
      var labelFiltrar = $('#labelFiltrar');

      if (method !== ""){
        labelMostrar.fadeIn();
        labelFiltrar.fadeIn();
        $categoriaFiltro.html('<option value="all"><?=translate('Todos')?></option>');
        $.ajax({
          url: "api/front/map.php",
          method: 'GET',
          dataType: 'JSON',
          data: {
            method  : method
          }
        })
        .done(function(data) {
          $categoriaFiltro.selectpicker('destroy');
          $.each(data, function (i, options) {
                $categoriaFiltro.append('<option value="' + options.value + '" >' + options.option + '</option>');
          });
          $categoriaFiltro.selectpicker({
            style: 'btn-info',
            size: 8
          });
        })
        .fail(function(_err) {
          console.log(_err);
        });

      } else {
        labelMostrar.fadeOut();
        //labelFiltrar.fadeOut();
      }
    }

    // Formulario
    $inputEstado = $('#estado');
    $selectEstado.on('change',function(e){
      e.preventDefault();

      if( $selectEstado.val() == ""){
        //$("#btn_descargareporte").addClass('hide');
      }
      if($('#selectCategoria').val()===""){
        alert("<?=translate('Debes seleccionar una categoria')?>");
        return false;
      }

      if($selectCategoria.val() == "ACTORES"){
        // Marcadores de actores
        $.ajax({
          url: "api/front/map.php",
          method: 'POST',
          dataType: 'JSON',
          data: {
            mostrar: $categoriaFiltro.val(),
            categoria: $selectCategoria.val(),
            method: 'get_markers',
            estado: $selectEstado.val()
          }
        })
        .done(function(_res) {
          //limpiar todos los marcadores
          deleteMarkers();

          // JSON to CSV
          generaReportes(_res, "Actores");

          // Agregar marcadores de actores
          $.each(_res.actores,function(index, value){
            agregarMarcadores( value, 'f37064')
          });

        })
        .fail(function(_err) {
          console.log(_err);
        });
      }

      // Busca alguna ubicacion en el mapa y se centra de nuevo
      //if($inputEstado.val()){
        buscaEstado($inputEstado.val());
      //}
    });
  });
  
  /* Funcion para agregar marcadores*/
  function agregarMarcadores(data, color = "FE7569"){

    if(data.lat && data.lng){
      // INFO WINDOWS
      var contentString = '<div id="content">'+
      '<div id="siteNotice">' +
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading">' + utf8_decode(data.Actor) + '</h3>' +
      '<div id="bodyContent">'+
        '<p>' + utf8_decode(data.Rol) + '</p>' + 
        '<img src="admin/files/' + data.RolImagen + '" width="80" class="img-responsive"><br>' + 
        '<small>' + utf8_decode(data.Ubicación) + '</small><br>' + 
        '<a href="<?=APP_PATH?>?view=home&id=' + data.ID + '" class="btn btn-primary btn-small"><?=translate("Ver Perfil")?></a>' + 
      '</div>'+
      '</div>';
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      // CONFIGURACION

      var pinColor = color;
      switch (data.Rol.toLowerCase()){
        case "generador":
          pinColor = "b8d97d";
        break;
        case "generador de conocimiento":
          pinColor = "b8d97d";
        break;
        case "articulador":
          pinColor = "fadd2d";
        break;
        case "promotor":
          pinColor = "a5a8ab";
        break;
        case "vinculador":
          pinColor = "3a3b3a";
        break;
        case "comunidad":
          pinColor = "87d4e9";
        break;
        case "habilitador":
          pinColor = "f36f62";
        break;
        case "generadores":
          pinColor = "b8d97d";
        break;
        case "generadores de conocimientos":
          pinColor = "b8d97d";
        break;
        case "articuladores":
          pinColor = "fadd2d";
        break;
        case "promotores":
          pinColor = "a5a8ab";
        break;
        case "vinculadores":
          pinColor = "3a3b3a";
        break;
        case "comunidades":
          pinColor = "87d4e9";
        break;
        case "habilitadores":
          pinColor = "f36f62";
        break;

        default:
          pinColor = "FE7569";
        break;
      }

      var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
      new google.maps.Size(21, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(10, 34));
      var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
      new google.maps.Size(40, 37),
      new google.maps.Point(0, 0),
      new google.maps.Point(12, 35));
      // ADD MARKER
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(parseFloat(data.lat),parseFloat(data.lng)),
        map: map,
        animation: google.maps.Animation.DROP,
        icon: pinImage,
        shadow: pinShadow
      });
      markers.push(marker);
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });

    }
  }
  /* Markers functions */
  // Sets the map on all markers in the array.
  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }
  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {
    setMapOnAll(null);
  }
  // Shows any markers currently in the array.
  function showMarkers() {
    setMapOnAll(map);
  }
  // Deletes all markers in the array by removing references to them.
  function deleteMarkers() {
    clearMarkers();
    markers = [];
  }

  // Función para convertir un JSON en CSV
  function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
      var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
      var CSV = '';
      CSV += ReportTitle + '\r\n\n';
      if (ShowLabel) {
          var row = "";
          //Títulos de columnas
          for (var index in arrData[0]) {
              //Comas
              row += index + ',';
          }
          row = row.slice(0, -1);
          CSV += row + '\r\n';
      }
      //Renglones
      for (var i = 0; i < arrData.length; i++) {
          var row = "";
          //Columnas
          for (var index in arrData[i]) {
              row += '"' + arrData[i][index] + '",';
          }
          row.slice(0, row.length - 1);
          CSV += row + '\r\n';
      }
      if (CSV == '') {
          alert("Invalid data");
          return;
      }
      var fileName = "Bihaiv_";
      fileName += ReportTitle.replace(/ /g,"_");
      var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
      var link = document.createElement("a");
      link.href = uri;
      link.style = "visibility:hidden";
      link.download = fileName + ".csv";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
  }

  // Formatea cadenas en utf8_decode
  function utf8_decode (str_data) {

      var tmp_arr = [],
          i = 0,
          ac = 0,
          c1 = 0,
          c2 = 0,
          c3 = 0;

      str_data += '';

      while (i < str_data.length) {
          c1 = str_data.charCodeAt(i);
          if (c1 < 128) {
              tmp_arr[ac++] = String.fromCharCode(c1);
              i++;
          } else if (c1 > 191 && c1 < 224) {
              c2 = str_data.charCodeAt(i + 1);
              tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
              i += 2;
          } else {
              c2 = str_data.charCodeAt(i + 1);
              c3 = str_data.charCodeAt(i + 2);
              tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
              i += 3;
          }
      }

      return tmp_arr.join('');
  }
</script>
