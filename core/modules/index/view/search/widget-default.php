<?php if(!isset($_SESSION["user_id"])){ Core::redir("./");}?>
<div class="container padding-top-100">
  <div class="row">
    <div class="col-md-3">
      <?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>Session::$organizacion,"from"=>"logged" ));?>
      <?php Action::execute("_mainmenu",array());?>
    </div>
    <div class="col-md-7">
      <h2><?=translate('Buscar')?></h2>
      <p class="text-muted"><?=translate('Término')?>: <?php echo $_GET["q"]; ?></p>
      <?php
      $users = UserData::getLike($_GET["q"],$_SESSION["user_id"]);
      ?>
      <?php if(count($users)>0): ?>
      <table class="table table-bordered">
        <thead>
          <th>&nbsp;<i class="fa fa-picture-o"></i></th>
          <th><?=translate('Usuario')?></th>
        </thead>
        <?php 
        foreach($users as $u):
        ?>
        <tr>
          <td style="width:60px;">
            <?php 
              // Avatar
              if($u->avatar!="assets/img/home/perfil.png"){

                if($u->avatar==""){
                  $img_url = "assets/img/home/perfil.png";
                } else {
                  $img_url = "storage/users/".$u->id."/profile/".$u->avatar;
                }
              }else {
                $img_url = $u->avatar;
              } 
            ?>
            <img src="<?php echo $img_url; ?>" class="img-responsive">
          </td>
          <td>
            <p><b><a href="./?view=home&id=<?php echo $u->id;?>"><?php echo $u->getFullname(); ?></a></b></p>
          </td>
        </tr>
        <?php endforeach; ?>
      </table>
      <?php else:?>
      <div class="jumbotron">
        <h2><?=translate('No hay Resultados')?></h2>
      </div>
      <?php endif; ?>
    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>
</div>