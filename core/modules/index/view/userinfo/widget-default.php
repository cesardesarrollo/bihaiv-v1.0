<?php
require_once 'core/modules/index/model/DaoOrganizacion.php';

$user = UserData::getById($_GET["uid"]);
if($user!=null):
  $DaoOrganizacion = new DaoOrganizacion();
  $organizacion = $DaoOrganizacion->getByUserId($user->id);
?>
<div class="container padding-top-100">
  <div class="row">
    <div class="col-md-3">
      <?php
      $from = "logout";
      if(isset($_SESSION["user_id"])){ $from="logged"; }
      Action::execute("_userbadge",array("user"=>$user,"organizacion"=>$organizacion ,"from"=>$from));
      Action::execute("_usermenu",array("user"=>$user,"from"=>$from));
      ?>
    </div>
    <div class="col-md-7">
      <h2>Información</h2>
      <table class="table table-bordered">
        <tr>
          <td><b><?=translate('Nombre')?></b></td>
          <td><?php echo ($user->getFullname()); ?></td>
        </tr>
        <tr>
          <td><b><?=translate('Teléfono')?>:</b></td>
          <td><?php echo ($organizacion->telefono); ?></td>
        </tr>
        <tr>
          <td><b><?=translate('Url')?>:</b></td>
          <td><?php echo ($organizacion->urlWeb); ?></td>
        </tr>
        <tr>
          <td><b><?=translate('Ubicación')?>:</b></td>
          <td><?php echo ($organizacion->ubicacion); ?></td>
        </tr>
        <tr>
          <td><b><?=translate('Misión')?>:</b></td>
          <td><?php echo ($organizacion->mision); ?></td>
        </tr>
        <tr>
          <td><b><?=translate('Visión')?>:</b></td>
          <td><?php echo ($organizacion->vision); ?></td>
        </tr>
        <tr>
          <td><b><?=translate('Acerca de mi')?>:</b></td>
          <td><?php echo ($organizacion->quienSoy); ?></td>
        </tr>
      </table>
    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>
</div>
<?php else:?>
<div class="container">
<div class="row">
  <div class="col-md-9">
    <div class="jumbotron">
      <h2><?=translate('No se encontro el usuario')?></h2>
    </div>
  </div>
</div>
</div>
<?php endif; ?>
<br><br>
<br><br>
<br><br>