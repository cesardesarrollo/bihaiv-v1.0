<?php
$conversation = ConversationData::getById($_GET["id"]);
$msgs = MessageData::getAllByConversationId($_GET["id"]);
?>
<div class="container padding-top-100">
  <div class="row">
      <div class="col-md-3">
  <?php Action::execute("_userbadge",array("user"=>Session::$user,"organizacion"=>Session::$organizacion,"from"=>"logged" ));?>
  <?php Action::execute("_mainmenu",array());?>
      </div>
      <div class="col-md-7">
      <h2><?=translate('Conversación')?></h2>
      <?php if(count($msgs)>0):?>
        <table class="table table-bordered">
        <thead>
          <th><?=translate('Mensajes')?></th>
        </thead>
        <tr>
          <td>
            <form role="form" method="post" action="./?action=sendmsg">
              <div class="form-group">
                <textarea class="form-control" name="content" required placeholder="<?=translate('Mensaje')?>" rows="3"></textarea>
              </div>
              <input type="hidden" name="ref" value="conversation">
              <input type="hidden" name="conversation_id" value="<?php echo $conversation->id; ?>">
              <button type="submit" class="btn btn-default"><?=translate('Enviar')?></button>
            </form>
          </td>
        </tr>
        <?php foreach($msgs as $msg):?>
          <tr>
            <td>
            <b><?php  
            echo $msg->getUser()->getFullname();          
            ?></b>
            <p><?php echo $msg->content; ?></p>
            <p style="font-size:10px;" class="text-muted"><?php echo $msg->created_at?></p>

            </td>
          </tr>
        <?php 
        $msg->read(Session::$user->id);
        endforeach; ?>
        </table>
      <?php else:?>
        <div class="jumbotron">
        <h2><?=translate('No hay Mensajes')?></h2>
        </div>
      <?php endif;?>
      </div>
      <div class="col-md-2">
      </div>
    </div>
  </div>
</div>
<br><br><br><br><br>