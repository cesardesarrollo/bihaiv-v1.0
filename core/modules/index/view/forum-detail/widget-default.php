<?php

  require_once('./core/modules/index/model/DaoOrganizacion.php');

  $logged = false;
  if(isset($_SESSION["user_id"])){ $logged = true; }

  if(!isset($_GET["id"])){
    Core::redir("./home/login.php");
    exit;
  } else {

    $DaoDiscusion = new DaoDiscusion();
    $Discusion = new Discusion();
    $Discusion = $DaoDiscusion->getById($_GET["id"]);

    if($Discusion[0]['privacidad'] === "PRIVADO"){
      if(!isset($_SESSION["user_id"])){
        Core::redir("./home/login.php");
        exit;
      } else {

        $DaoOrganizacion = new DaoOrganizacion();
        $Organizacion = new Organizacion();
        $Organizacion = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);

        // Se revisa si está en lista de invitados
        $invitado = $DaoDiscusion->IsInvitedByTemaAndOrgId($Discusion[0]['id'], $Organizacion->id);
        if(!$invitado){
          // Si es tema propio
          $isMine = $DaoDiscusion->IsCreatedByMe($Discusion[0]['id'], $Organizacion->id);
          if(!$isMine){
            Core::redir("./home/login.php");
            exit;
          }
        } 
      }
    }
  }

  
?>
<div class="container-fluid" style="margin-left:0; margin-right:0;padding-left:0; padding-right:0; ">
  <?php include('./views/main_menu.php'); ?>
  <section class="padding-row">
    <div class="container">
      <div id="discusion"></div>
      <div class="Card z-depth-1" id="discusion_scroll" style="padding: 15px !important;text-align: right;">
        <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle sort-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-filter" aria-hidden="true"></i> &nbsp; <?=translate('Ordenar comentarios')?> <span class="caret"></span>
          </button>
          <ul class="dropdown-menu z-depth-1 no-border">
            <li><a href="#!" class="triger-sort-comments" data-orderby="order_by_plus_likes"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> &nbsp; <?=translate('Mas likes')?></a></li>
            <li><a href="#!" class="triger-sort-comments" data-orderby="order_by_minus_likes"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> &nbsp; <?=translate('Menos likes')?></a></li>
            <li><a href="#!" class="triger-sort-comments" data-orderby="order_by_date_desc"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> &nbsp; <?=translate('Nuevos')?></a></li>
            <li><a href="#!" class="triger-sort-comments" data-orderby="order_by_date_asc"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> &nbsp; <?=translate('Viejos')?></a></li>
          </ul>
        </div>
        <style>
          .no-border{
            border-radius: 0px !important;
            border: none !important;
          }
          .sort-button{
            border-color: #e16a5f;
            color: #f37064;
            background-color: white;
          }.sort-button:hover{
            background-color: #f37064 !important;
            color: white !important;
          }.sort-button:active{
            background-color: #f37064 !important;
            color: white !important;
          }
          }.sort-button:focus{
            background-color: #f37064 !important;
            color: white !important;
          }
          .open>.dropdown-toggle.btn-default.sort-button {
            color: white !important;
            background-color: #f37064 !important;
            border-color: #f37064 !important;
          }
          .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
            color: white !important;
            text-decoration: none;
            background-color: #f37064 !important;
          }
        </style>
      </div>
      <div class="margin-row-bottom" style="padding:20px 0px;">
        <div id="discusion_comentario"></div>
      </div>
    </div>
  </section>
  <div id="modal-launcher"></div>
</div>
<script>
	$(".view-more").on('click', function(e){
		e.preventDefault();
		var answer = $(this).parent().parent().parent();
		$(this).parent().find('span.answer-more').slideToggle();
		$(answer).addClass('active');
		$(this).css('display', 'none');
	});
	$(".view-less").on('click', function(e){
		e.preventDefault();
		var answer = $(this).parent().parent().parent().parent();
		$(this).parent().slideToggle();
		$(this).parent().parent().find('a.view-more').css('display', 'inline-block');
		$(answer).removeClass('active');
		$(this).css('display', 'none');
	});
</script>
<!-- TEMA PRINCIPAL TEMPLATE -->
<script id="discusion-template" type="x-text/handlebars">
	<div class="row margin-row-bottom padre-row">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<div class="Card Card-avatar">
				<div class="Card--Avatar">
					<figure class="figure">
            <div class="avatar" style="background-image: url({{user_avatar}});" ></div>
					</figure>
				</div>
				<div class="Card--Info">
					<p><i class="fa fa-user"></i> {{user_name}}</p>
					<p><i class="fa fa-star"></i> {{user_rol}}</p>
					<p class="text-justify"><i class="fa fa-align-justify"></i> {{user_descripcion}}</p>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
			<div class="Card second-card">
				<div class="Card--title">
					<h3>{{tema_titulo}}</h3>
				</div>
				<div class="Card--content">
					<p>
						{{tema_descripcion}}
					</p>
					<p>
						<a class="badge" href="#">{{tema_rol}}</a>
					</p>
          {{{rate_YO}}}
          <hr>
          <form id="new-comment-topic">
  					<div class="form-group">
  						<label for="txt__Answer"><?=translate('Escribir respuesta')?>:</label>
  						<?php if($logged): ?>
              <textarea class="form-control" id="txt__Answer" placeholder="<?=translate('Escribir respuesta')?>"></textarea>
              <?php endif; ?>
  					</div>
            <input type="hidden" id="txt__user" value="<?php echo SESSION::$organizacion->id; ?>">
            <input type="hidden" id="id_tema" value="<?php echo $_GET['id']; ?>">
  					<div class="form-group">
              <button type="submit" class="btn btn-primary"><?=translate('Responder')?></button>
  					</div>
          </form>
				</div>
			</div>
		</div>
	</div>
</script>
<!-- HERE COMES NEW CHALLENGER !!! -->
<style>
  #rateYo{
    padding: 0px !important;
    margin-bottom: 15px !important;
    margin-top: 5px !important;
  }
  .figure{
    height: 100%;
  }
  .figure > .avatar{
    background-position: center;
    background-size: cover;
    border-radius: 2px;
    height: 100%;
  }
  .figure > .avatar-comment{
    background-position: center;
    background-size: cover;
    margin: 0 auto;
  }
  .Card-avatar{
    padding: 0px !important;
  }
  .Card{
    padding: 20px;
    border-radius: 2px;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);
  }
  .Answer{
    padding: 15px 0px 15px 0px;
    border-radius: 2px;
    box-shadow: 0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);
  }
  .Card--Info{
    margin-top: 25px;
    padding: 0px 20px 20px 20px;
  }
  @media (min-width: 768px) {
    .padre-row{
      display: flex;
    }
  }
  @media (max-width: 768px) {
    .second-card{
      margin-top: 15px;
    }
  }
  .second-card{
    height: 100%;
  }
  .margin-row-8{
    margin-bottom: 8px;
  }
</style>
<!-- COMENTARIOS TEMPLATE -->
<script id="discusion_comentario-template" type="x-text/handlebars">
    {{#each this}}
		<div class="margin-row-8">
			<div class="Answer">
				<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 text-center">
					<figure class="figure margin-row-bottom">
            <div class="img-rounded avatar-comment" style="background-image: url({{avatar}});" ></div>
					</figure>
          <p class="margin-row-bottom">{{created_at}}</p>
					<p class="margin-row-bottom">
            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
            <span class="count-likes-{{id}}">
              {{likes}}
            <span>
          </p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-10 col-lg-10">
					<h4><a href="?view=home&id={{usuarios_id}}">{{name}}</a></h4>
					<p>
						{{comentario}}
					</p>
          <p>
            <a class="btn btn-primary califica-comentario-trigger" data-idcomment="{{id}}" style=" float: right; ">
              <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <?=translate('Me gusta')?>
            </a>
          </p>
				</div>
			</div>
		</div>
    {{/each}}
</script>
<?php if (isset($_GET['id']) AND is_numeric($_GET['id'])): ?>
  <script>
    $(function(){
      var discusion = new Discusiones();
      var params = {
        '_method' : "byId",
        '_id'     : <?php echo $_GET['id']; ?>
      };
      discusion._set(params);
      var discusion2 = new Discusiones();
      var params2 = {
        '_method' : "add_visit",
        '_id'     : <?php echo $_GET['id']; ?>
      };
      discusion2._set(params2);
      // HERE COMES NEW CHALLENGER
      $orderButton = $('.triger-sort-comments');
      $orderButton.on('click',function(e){
        e.preventDefault();
        var discusionOrder = new Discusiones();
        var params3 = {
          '_method' : $(this).data('orderby'),
          '_id'     : <?php echo $_GET['id']; ?>
        };
        discusionOrder._set(params3);
      });
      // Califica comentarios
      $("#discusion_comentario").on('click', '.califica-comentario-trigger', function (e) {
        var discusion = new Discusiones();
        var data = {
          '_method'           : 'rate_comment',
          '_id'               : $(this).data('idcomment'),
          '_organizacion_id'  : $('#txt__user').val(),
          '_tema_id'          : $('#id_tema').val()
        };
        discusion._set_comment(data);
      });

    });
  </script>
<?php endif; ?>
<script>
  $(function () {
    // Selectors
    $form = $('#new-comment-topic');
    $comentario       = $('#txt__Answer');
    $organization_id  = $('#txt__user');
    $tema_id          = $('#id_tema');

    // inicializa rateYo
    $rateYo = $("#rateYo").rateYo({
      rating    : 0,
      starWidth : "16px",
      ratedFill : '#fae617',
      precision : 0,
      spacing   : "2px",
      onSet: function (rating, rateYoInstance) {
        if( parseInt(rating) > 0 ){
          if(!$organization_id.val()){
            setFlash('<?=translate("Debes iniciar sesión para realizar esta acción")?>', 'warning');
            return false;
          }
          var discusion = new Discusiones();
          var data = {
            '_calificacion'     : rating,
            '_organizacion_id'  : $organization_id.val(),
            '_tema_id'          : $tema_id.val(),
            '_method'           : 'update_rating',
          };
          discusion._set_rating(data);
        }else{
          setFlash('<?=translate("Para calificar es necesario ingresar al menos una estrella")?>', 'warning');
        }
      }
    });

    // Handler events
    $form.on('submit',function(e){
      e.preventDefault();
      // VALIDATE ALL INPUTS
      if(!$comentario.val()){
        setFlash('<?=translate("Escribe una respuesta antes de enviarla")?>', 'warning');
        return false;
      }
      if(!$organization_id.val()){
        setFlash('<?=translate("Debes iniciar sesión para realizar esta acción")?>', 'warning');
        return false;
      }
      var discusion = new Discusiones();
      var data = {
        '_comentario'       : $comentario.val(),
        '_organizacion_id'  : $organization_id.val(),
        '_tema_id'          : $tema_id.val(),
        '_method'           : 'save_comment',
      };
      discusion._set_comment(data);
    });
  });
</script>
