<section class="Panal">
    <div class="container">
      <div class="col-xs-12 col-md-6 col-md-offset-4 col-lg-6 col-lg-offset-4">
        <div class="Header__navigation">
          <div class="panalContainer">
            <div class="profileIcon">
              <a href="jomsocial/groups" ></a>
              <h3 class="hidden-xs">Roles</h3>
            </div>
            <div class="socialIcon">
              <a href="mapa-del-ecositema"></a>
              <h3 class="hidden-xs">Mapa</h3>
            </div>
            <div class="videoIcon">
              <a href="jomsocial/videos"></a>
              <h3 class="hidden-xs">Mejores Prácticas</h3>
            </div>
            <div class="foroIcon">
              <a href="forum"></a>
              <h3 class="hidden-xs">Foro</h3>
            </div>
            <div class="calendarioIcon">
              <a href="eventos-en-el-ecositema"></a>
              <h3 class="hidden-xs">Calendario</h3>
            </div>
            <div class="bdIcon">
              <a href="jomsocial/groups/display?categoryid=7"></a>
              <h3 class="hidden-xs">Expertos</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="Header__footer"></div>
</section>
<section class="Slider">
  <div class="owl-carousel">
    <div class="item">
      <img src="assets/img/home/Banner.jpg" alt="">
    </div>
    <div class="item">
      <img src="assets/img/home/banner_2.jpg" alt="">
    </div>
    <div class="item">
      <img src="assets/img/home/banner_3.jpg" alt="">
    </div>
  </div>
</section>
<section class="History padding-row">
  <div class="container">
    <h2 class="section-title text-center uppercase bolder">
    Historias del ecosistema
    </h2>
    <div class="row margin-row-top">
      <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
        <iframe width="100%" height="300" src="https://www.youtube.com/embed/l5zwWkma4Fk" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
        <iframe width="100%" height="300" src="https://www.youtube.com/embed/l5zwWkma4Fk" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
        <iframe width="100%" height="300" src="https://www.youtube.com/embed/l5zwWkma4Fk" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</section>
<section class="Map padding-row">
  <h2 class="section-title text-center text-white uppercase bolder">
  mapa del ecosistema
  </h2>
  <div class="container">
    <div class="row margin-row-top">
      <div class="col-xs-12 col-lg-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14933.353748093383!2d-103.39917789999998!3d20.655805700000002!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1463064413080" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function() {
    $('.owl-carousel').owlCarousel({
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true,
      loop:true,
      margin:0,
      nav : false,
      responsive:{
        0:{
          items:1
        },
        600:{
          items:1
        },
        1000:{
          items:1
        }
      }
    });
  });
</script>
