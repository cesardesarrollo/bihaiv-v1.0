<?php
$from = $params["from"];
$user = $params["user"];
$organizacion = $params["organizacion"];

$url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
$gotourl = "";
$id = 0;
$is_mine = false;

if ($from == "logged" && $user->id == $_SESSION["user_id"]) {
    $id = $_SESSION["user_id"];  
    $gotourl = "./?view=home";
    $is_mine = true;
} else { 
    $gotourl = "./?view=home&id=".$user->id; 
    $id = $user->id;  
    $is_mine = false;
}
?>

<div class="Wall margin-row-bottom">
  <div class="Wall__header">
    <figure class="figure display-inline">
      <img src="assets/img/home/colaboracion.png" class="img-responsive">
    </figure>
    <p class="display-inline text-white"><?php echo translate('Colaboraciones de')?> 
      <a class="bolder" href="<?php echo $gotourl; ?>">
        <?php echo $user->getFullname(); ?>
      </a>
    </p>
  </div>
  <div class="Wall__content padding">
    <!--FOR EACH-->
    <div id="collaborations"></div>
    <!--END FOR EACH-->
  </div>
</div>

 <script id="collaboration-template" type="x-text/handlebars" >
      {{#each this}}
        <div class="row row-colaboration">
          <div class="col-sm-12 col-lg-12">
            <p>
              <a href="?view=home&id={{user_id_colaborador}}">{{user_name_colaborador}}</a> <?php echo translate('esta colaborando en')?> {{titulo}} <?php echo translate('creada por')?> <a href="?view=home&id={{user_id_creador}}"> {{user_name_creador}} </a>
            </p>
          </div>
        </div>
      {{/each}}
  </script>
<script>
  $(function(){
    var colaboraciones = new Colaboraciones();
    var params = {};
    params._method = 'all';
    params._user_id = <?php echo $id ?>;
    colaboraciones._set(params);
  })
</script>
