<?php  

  $logged = false;
  if (isset($_SESSION["user_id"])) { 
    $logged = true; 
  }

  $from = $params["from"];
  $user = $params["user"];
  $organizacion = $params["organizacion"];
  $url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
  $gotourl = "";
  $id = 0;
  $is_mine = false;

  if ($from == "logged" && ($user->id == @$_SESSION["user_id"]) ) {
      $id = @$_SESSION["user_id"];
      $gotourl = "./?view=home";
      $is_mine = true;
  } else {
      $gotourl = "./?view=home&id=".$user->id;
      $id = $user->id;
      $is_mine = false;
  }
?>
  <div class="Wall margin-row-bottom">
    <div class="Wall__header">
      <figure class="figure display-inline">
        <img src="assets/img/home/iniciativa.png" class="img-responsive">
      </figure>
      <p class="display-inline text-white"><?php echo translate('Iniciativas de'); ?>
        <a class="bolder" href="<?php echo $gotourl; ?>">
            <?php echo $user->getFullname(); ?>
        </a>
      </p>
        <?php if ($is_mine ) { ?>
      <div class="pull-right" style="margin-top:0px">
        <button onclick="openModal('m-new-initiative','<?php echo $id ?>')" class="btn btn-outline-primary btn-xs">
          <i class="fa fa-plus"></i><span class="hidden-xs"><?php echo translate('Agregar Nuevo'); ?> </span>
        </button>
      </div>
        <?php } ?>
    </div>
    <div class="Wall__content padding">
      <!--FOR EACH INICIATIVAS-->
      <div id="leads"></div>
      <div class="Wall__footer padding">
        <div class="pull-right">
          <a id="initiatives-more" class="btn btn-default" data-user="<?php echo $id ?>" href="#"><?php echo translate('label vermas')?></a>
        </div>
      </div>
    </div>
  </div>
  <script id="lead-template" type="x-text/handlebars">
    {{#each this}}
      <div class="row hoverable margin-row-bottom row-separator padding-row-sm">
        <div class="col-xs-12 col-md-4 col-lg-3 margin-row-bottom">
          <figure class="figure" style=" height: 130px; max-width: 100%; background-color: whitesmoke; width: 100%; background: url('uploads/iniciativas/{{_thumbnail}}') no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover;background-size: cover; border-radius: 2px;box-shadow: 0 1px 3px 0 rgba(0,0,0,0.2),0 1px 1px 0 rgba(0,0,0,0.14),0 2px 1px -1px rgba(0,0,0,0.12);" >
          </figure>
        </div>
        <div class="col-xs-12 col-md-8 col-lg-9 margin-row-bottom">
            <?php if ($is_mine ) { ?>
            <div class="wall-edit-button" onclick="showById(event,'{{_Id}}', '{{_postId}}', <?php echo $id ?>, 'edit')" title="<?php echo translate('Editar Iniciativa'); ?> {{titulo}}">
              <i class="fa fa-pencil"></i>
            </div>
            <div class="wall-edit-button" onclick="deleteInitiative(event, '{{_Id}}', '{{_postId}}', '<?php echo $id ?>')" style="right:45px;" title="<?php echo translate('Eliminar Iniciativa'); ?> {{titulo}}">
              <i class="fa fa-trash"></i>
            </div>
            <?php } ?>
          <h2 class="title--initiative bolder">{{_Titulo}}</h2>
          <div class="row">
            <div class="col-xs-1 col-lg-1 text-center">
              <i class="fa fa-clock-o"></i>
            </div>
            <div class="col-xs-11 col-lg-11">
              <h4 class="date--initiative thin margin-left-negative">{{_fecha}} {{_horaInicio}} </h4>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <span class="margin-row-top">{{_descripcion}}</span>
              <hr>
              <h4><?php echo translate('Objetivos'); ?></h4>
              <ul class="list-group">
                {{#each objetivos}}
                <li id="leads-template" class="list-group-item">
                  <i class="fa fa-dot-circle-o"></i> {{descripcion}}
                  <div class="pull-right"> {{{stars}}} </div>
                </li>
                {{/each}}
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-lg-12">

          {{#if colaboradores}}
          <div class="col-xs-6 col-md-4 col-lg-3 margin-row-bottom">
            <label for=""><?php echo translate('Colaboradores'); ?>:</label>
          </div>
          <div class="col-xs-6 col-md-8 col-lg-9 actors margin-row-bottom" id="colaboradores">
            {{#each colaboradores}}
              <a href="?view=home&id={{organizacion/Usuarios_idUsuarios}}" title="{{user/name}}">
                <figure class="figure">
                  <img src="<?php APP_PATH ?>storage/users/{{organizacion/Usuarios_idUsuarios}}/profile/{{organizacion/avatar}}" alt="{{user/name}}" height="40">
                </figure>
              </a>
            {{/each}}
          </div>
          {{/if}}

        </div>
        <?php if ($logged ) : ?>
        <div class="col-xs-12 col-lg-12">
          <a href="#" class="btn btn-primary pull-right show-initiative" onclick="showById(event,'{{_Id}}', '{{_postId}}', <?php echo $id ?>, 'show')"><?php echo translate('Ver'); ?>
          </a>
        </div>
        <?php endif; ?>
      </div>
    {{/each}}
  </script>
  <script>
    $(function(){
      var iniciativa = new Iniciativas();
      var params= new Object();
      params._method = "allByLimits";
      params._userid = <?php echo $id?>;
      iniciativa._set(params);

      // Rate objetivos iniciativas
      $(document).ajaxStop(function() {
        // Vars 2 send
        var user_id = '<?php echo @$_SESSION["user_id"]; ?>';
      });
    });
  </script>
