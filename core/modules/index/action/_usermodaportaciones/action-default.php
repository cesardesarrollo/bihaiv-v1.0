<?php

$logged = false;
if (isset($_SESSION["user_id"])) { $logged = true; 
}

$from = $params["from"];
$user = $params["user"];
$organizacion = $params["organizacion"];

$url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
$gotourl = "";
$id = 0;
$is_mine = false;

if ($from == "logged" && $user->id == @$_SESSION["user_id"]) {
    $id = @$_SESSION["user_id"];
    $gotourl = "./?view=home";
    $is_mine = true;
} else {
    $gotourl = "./?view=home&id=".$user->id;
    $id = $user->id;
    $is_mine = false;
}
?>
  <div class="Wall margin-row-bottom">
    <div class="Wall__header">
      <figure class="figure display-inline">
        <img src="assets/img/home/aportacion.png" class="img-responsive">
      </figure>
      <p class="display-inline text-white"><?php echo translate('Aportaciones de')?>
        <a class="bolder" href="<?php echo $gotourl; ?>">
            <?php echo $user->getFullname(); ?>
        </a>
      </p>
        <?php if ($is_mine ) { ?>
        <div class="pull-right" style="margin-top:0px">
          <button onclick="openModal('m-new-share', '<?php echo $id ?>')" class="btn btn-outline-primary btn-xs">
            <i class="fa fa-plus"></i><span class="hidden-xs"> <?php echo translate('Agregar Nuevo')?> </span>
          </button>
        </div>
        <?php } ?>
    </div>
    <div class="Wall__content padding">
      <!--FOR EACH-->
      <div id="shares">
        <?php if ($is_mine ) { ?>
          <h3><?php echo translate('!Este usuario aún no registra aportaciones!')?></h3>
        <?php } ?>
      </div>
      <!--<div class="Wall__footer padding">
      </div>
      END FOR EACH-->
    </div>
  </div>
  <script id="shares-template" type="x-text/handlebars" >
      {{#each this}}
        <div class="row hoverable margin-row-bottom row-separator padding-row-sm">
          <div class="col-xs-12 col-md-4 col-lg-3 margin-row-bottom">
            <?php if ($logged) : ?>
            <a href="uploads/aportaciones/{{urlFile}}" download title="<?php echo translate('Descargar')?>" data-aportacionid="{{idAportacion}}"  class="register-View">
            <?php endif; ?>
            <figure class="figure" style="
              height: 100px;
              max-width: 100%;
              background: url('uploads/aportaciones/{{thumbnail}}') no-repeat center center;
              background-color: whitesmoke;
              no-repeat center center fixed;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
              box-shadow: 0 1px 3px 0 rgba(0,0,0,0.2),0 1px 1px 0 rgba(0,0,0,0.14),0 2px 1px -1px rgba(0,0,0,0.12);
              border-radius: 2px;
            ">
            <?php if ($logged) : ?>
            </figure>
              <label class="small descargar-aportacion"><?php echo translate('Click para descargar')?></label>
            </a>
            <?php endif; ?>
          </div>
          <div class="col-xs-12 col-md-8 col-lg-9 margin-row-bottom">

            <?php if ($is_mine ) { ?>
              <div class="wall-edit-button" onclick="deleteAportation(event, '{{idAportacion}}', '<?php echo $id ?>')" title="<?php echo translate('Eliminar Aportación'); ?> {{titulo}}">
                <i class="fa fa-trash"></i>
              </div>
            <?php } ?>

            <h2 class="title--initiative bolder">{{titulo}}</h2>
            <h4 class="date--initiative bolder">{{fechaCreado}}</h4>
            <span class="margin-row-top">{{descripcion}}</span>
            <br>
            {{{stars}}}
          </div>
          <!--
          <div class="col-xs-12 col-lg-12">
            <button class="btn btn-danger pull-right delAportation" value="{{id}}"><?php echo translate('Eliminar')?></a>
          </div>-->
        </div>
      {{/each}}
  </script>
  <script>
    $(function(){
      var aportacion = new Aportacion();
      var params = {};
      params._method = "all";
      params._userid = <?php echo $id ?>;
      aportacion._set(params);

      $('.delAportation').on('click',delAportation);
      function delAportation(e){

        e.preventDefault();
        var aportacion = new Aportacion();
        var params = {};
        params._method = "delete";
        params._userid = <?php echo $id ?>;
        params._id = this.val();
        aportacion._set(params);

      }
    });
  </script>
  <!-- CALIFICAR APORTACIONES SIEMPRE Y CUANDO NO ME PERTENEZCAN -->
    <?php if (!$is_mine AND ( isset($_SESSION['user_id']) AND is_numeric($_SESSION['user_id']) ) ) : ?>
    <script>
      /* Registra la descarga de una de las aportaciones */
      $(function(){
        /* Handler click event on delegate selector */
        $("#shares").on('click', '.register-View', function () {
          $.post('api/front/aportacion.php',{method: 'aportaciones_visitas',aportacion_id: $(this).data('aportacionid')});
        });

      });
      $(document).ajaxStop(function() {
        // Vars 2 send
        var user_id = "<?php echo $_SESSION['user_id']; ?>";
        var rateYoAportacion = $(".rateYo-aportaciones").rateYo({
          rating    : 0,
          starWidth : "16px",
          ratedFill : '#fae617',
          precision : 0,
          spacing   : "2px",
          onSet: function (rating, rateYoInstance) {
            if ( parseInt(rating) > 0 ){
              var data = {
                '_id'           : $(this).data('idaportacion'),
                '_calificacion' : rating,
                '_userid'       : user_id,
                '_method'       : 'update_rating',
              };
              var ratingAportacion = new Aportacion();
              ratingAportacion._set(data);
            } else {
              setFlash('<?php echo translate('Para calificar es necesario ingresar al menos una estrella')?>', 'warning');
            }
          }
        });
      });
    </script>
    <?php endif; ?>
