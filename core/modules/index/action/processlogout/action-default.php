<?php
/**
* @description Proceso para cerrar sesión
**/

    Session::delete("user_id");
    session_destroy();
    Core::redir("./");

    setcookie("id_usuario", "", time() - 360000);
    setcookie("marca_aleatoria_usuario", "", time() - 360000);

?>
