<?php
    //Sugerencias
    $from = $params["from"];
    $user = $params["user"];
    $organizacion = $params["organizacion"];

    $url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
    $gotourl = "";
    $id = 0;
    $is_mine = false;

    if ($from == "logged" && $user->id == $_SESSION["user_id"]) {
        $id = $_SESSION["user_id"];
        $gotourl = "./?view=home";
        $is_mine = true;
    } else {
        $gotourl = "./?view=home&id=".$user->id;
        $id = $user->id;
        $is_mine = false;
    }

    if ($is_mine) {
?>

     <div class="Contacts margin-row-bottom">
       <div class="row">
      <div class="close-row-sugerencias" title="<?php echo translate('Ocultar sugerencias'); ?>..." onclick="toggleSuggest()">
              <i class="fa fa-caret-up"></i>
          </div>
          <div id="sugerencia"></div>
     </div>
     </div>
     <script id="sugerencia-template" type="x-text/handlebars" >
     {{#each this}}
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
          <div class="Contacts__profile">
            <div class="Contacts__profile--identifyer">
              <a class="popOver" data-toggle="tooltip" data-placement="left" title="{{tipo}}">
                <figure class="figure">
                  <img src="assets/img/home/{{tipo_img}}" alt="">
                </figure>
              </a>
            </div>
            <figure class="figure">
                      <img class="Contacts__profile--img" src="{{profile_image}}" alt="">
            </figure>
            <p class="Contacts__profile--name">
              <a href="./?view=home&id={{user_id}}">{{user_name}}</a>
            </p>
            <p class="Contacts__profile--role">
              {{roles_nombre}}
            </p>
            <p class="Contacts__profile--button">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default" onclick="vincularme('{{ user_id}}')"><?php echo translate('Vincularme'); ?></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#" onclick="openModalActions('m-sugest-profile', '{{user_id}}', '{{user_name}}')"><?php echo translate('Sugerir vinculación'); ?></a></li>
                    <li><a href="#" onclick="openModalActions('m-send-msg', '{{user_id}}', '{{user_name}}')"><?php echo translate('Mandar mensaje'); ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#" onclick="openModalActions('m-report-user', '{{user_id}}', '{{user_name}}')"><?php echo translate('Reportar'); ?></a></li>
                  </ul>
                </div>
            </p>
          </div>
        </div>
     {{/each}}
     </script>
     <script>
    $(function(){
        $('[data-toggle="tooltip"]').tooltip()
        var sugerencia = new Sugerencias();
        var params = {};
        params._method = "all";
        params._userid = <?php echo $id ?>;
        sugerencia._set(params);
    });

    function toggleSuggest(){
        $('#sugerencia').slideToggle('fast', function(){
            if ( $("#sugerencia").is(':hidden') ){
                $(".close-row-sugerencias").attr('title', 'Mostrar sugerencias...');
                $(".close-row-sugerencias i").removeClass('fa-caret-up');
                $(".close-row-sugerencias i").addClass('fa-caret-down');
            } else {
                $(".close-row-sugerencias").attr('title', 'Ocultar sugerencias...' );
                $(".close-row-sugerencias i").removeClass('fa-caret-down');
                $(".close-row-sugerencias i").addClass('fa-caret-up');
            }
        });
    }

    function vincularme(user){
        var vinculacion = new Vinculaciones();
        var data = {
            _action : 'vincularme',
            _method : 'save',
            _receptor : user
        };
        vinculacion._set( data );

        var sugerencia = new Sugerencias();
        var params = {};
        params._method = "all";
        params._userid = <?php echo $id ?>;
        sugerencia._set(params);
    }
     </script>
<?php } ?>
