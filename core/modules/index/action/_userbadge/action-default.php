<?php
$from = $params["from"];
$user = $params["user"];
$organizacion = $params["organizacion"];
// Avatar
if ($organizacion->avatar!="assets/img/home/perfil.png" && $organizacion->avatar!="") {
    $img_url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
} else {
    $img_url = "assets/img/home/perfil.png";
}
$gotourl = "";
if ($from=="logged" && $user->id==$_SESSION["user_id"]) { $gotourl = "./?view=home"; 
}
else{ $gotourl= "./?view=home&id=".$user->id; 
}
?>
<div class="well">
    <div class="row">
        <div class="col-md-4">
            <img src="<?php echo $img_url; ?>" class="img-responsive img-thumbnail">
        </div>
        <div class="col-md-8">
            <h5><a href="<?php echo $gotourl; ?>"><?php echo $user->getFullname(); ?></a></h5>
    <?php if ($from=="logout") :?>
            <!-- NO se muestra ningun boton -->
    <?php else:?>
    <?php if ($user->id==Session::$user->id) :?>
    <?php else:
    $fs = FriendData::getFriendship($_SESSION["user_id"], $user->id); ?>
    <?php if ($fs==null) :?>
            <a href="./?action=sendfriendreq&recid=<?php echo $user->id; ?>" class="btn btn-default btn-xs"><?php echo translate('Solicitud de Amistad')?></a>
    <?php else:?>
    <?php if ($fs->is_accepted) :?>
            <button class="btn btn-primary btn-xs"><?php echo translate('Amigos')?></button>
    <?php else:?>
    <?php if ($_SESSION["user_id"]!=$user->id) :?>
            <button class="btn btn-success btn-xs"><?php echo translate('Solicitud Enviada')?></button>
    <?php else:
    $fs->read();
    ?>
            <a href="./?action=acceptfriendreq&recid=<?php echo $user->id; ?>" class="btn btn-success btn-xs"><?php echo translate('Aceptar Solicitud')?></a>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php endif; ?>
        </div>
    </div>
</div>
