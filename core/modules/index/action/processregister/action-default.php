<?php

require_once 'core/modules/index/model/DaoUbicaciones.php';
require_once 'core/modules/index/model/DaoOrganizacion.php';

$Cuestionario = new Cuestionario();
if (!empty($_POST)) {
    if ($_POST["name"]!=""&&$_POST["rfc"]!=""&&$_POST["email"]!=""&&$_POST["password"]!="") {
        $user = UserData::getByEmail($_POST["email"]);
        if ($user==null) {
            $str = "abcdefghijklmopqrstuvwxyz1234567890";
            $code = "";
            for ($i=0; $i < 6; $i++) { 
                $code .= $str[rand(0, strlen($str)-1)];
            }

            // Validación
            foreach ( $_POST as $item ) {
                if (!isset($item)) {
                    Core::alert("Todos los campos deben ser llenados.");
                    Core::goBack(1);
                    return;
                }
            }

            if ($_POST['password'] !== $_POST['confirm_password']) {
                Core::alert("Los passwords ingresados no coinciden, por favor revísalo");
                Core::goBack(1);
                return;
            }

            $user = new UserData();
            $user->name = $_POST["name"];
            $user->rfc = $_POST["rfc"];
            $user->email = $_POST["email"];
            $user->country = $_POST["country"];
            $user->password = sha1(md5($_POST["password"]));
            $user->code = $code;
            $user->tipoRel = 1;
            $u = $user->add();

            if ($u[0] && $u[1] > 0) {

                $p =  new ProfileData();
                $p->user_id = $u[1];
                $profile = $p->add();

                if ($profile[0] ) {

                    // Se añade ubicación
                    $DaoUbicaciones = new DaoUbicaciones();
                    $Ubicaciones = new Ubicaciones();

                    $idPais = $_POST["country"];
                    $idEstado = 0;
                    $idMunicipio = 0;
                    $idLocalidad = 0;

                    //GET ORGANIZATION ID
                    $DaoOrganizacion = new DaoOrganizacion();
                    $Organizacion = new Organizacion();

                    $Organizacion = $DaoOrganizacion->getByUserId($u[1]);
                    $Ubicaciones->setIsMain(1);
                    $Ubicaciones->setIsMatriz(1);
                    $Ubicaciones->setIdPais($idPais);
                    $Ubicaciones->setIdEstado($idEstado);
                    $Ubicaciones->setIdMunicipio($idMunicipio);
                    $Ubicaciones->setIdLocalidad($idLocalidad);
                    $Ubicaciones->setIdOrganizacion($Organizacion->id);
                    $ubicacion_id = $DaoUbicaciones->add($Ubicaciones);

                    // Mensaje enviado por email
                    $msg = "<h1>Registro Exitoso</h1>
						<p>Ahora debes activar tu cuenta en el siguiente link:</p>
						<p><a href='".APP_PATH."index.php?r=index/processactivation&e=".sha1(md5($_POST["email"]))."&c=".sha1(md5($code))."'>Activa tu cuenta.</a></p> ";

                    Core::mail($_POST["email"], "Registro Exitoso", $msg, $msg);
                    Core::alert("¡Registro Exitoso!, se ha enviado un email con los datos necesarios para activar su cuenta.");
                    Core::redir(APP_PATH);

                } else {
                    Core::alert("Ha ocurrido un problema al tratar de crear un perfil, intente de nuevo.");
                    Core::goBack(1);
                }

            } else {
                Core::alert("Ha ocurrido un problema al tratar de crear un usuario, intente de nuevo.");
                Core::goBack(1);
            }

        } else {
            Core::alert("El email proporcionado ya esta registrado.");
            Core::goBack(1);
        }
    } else {
        Core::alert("No puede dejar campos vacios");                
        Core::goBack(1);
    }
}

?>
