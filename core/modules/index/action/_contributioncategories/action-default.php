<?php

  $logged = false;
  
if (isset($_SESSION["user_id"])){ $logged = true; }

  $show = $params["show"];
  $DaoCategoriaAportacion = new DaoCategoriaAportacion();
  $where_parent = ['idSeccionAportacion =' => $show, 'idPadre =' => 0];
?>
<div class="panel-group" id="accordion_parent_categories" role="tablist" aria-multiselectable="true">
  <?php foreach ($DaoCategoriaAportacion->getAll([1],$where_parent) as $parent_key => $parent_category) { ?>
    <div class="panel panel-default">
      <div class="panel-heading bg-panel-primary" role="tab" id="parent_category_<?php echo $parent_key; ?>">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion_parent_categories" href="#list_category_<?php echo $parent_key; ?>" aria-expanded="true" aria-controls="list_category_<?php echo $parent_key; ?>">
            <?php echo $parent_category->getTitulo(); ?>
          </a>
        </h4>
      </div>
      <div id="list_category_<?php echo $parent_key; ?>" class="panel-parent-category panel-collapse collapse 
      <?php  if ($parent_key == 0) echo 'in'; ?>" role="tabpanel" aria-labelledby="parent_category_<?php echo $parent_key; ?>">
        <?php
        $count_child_categories = 0;
        $where_child = ['idSeccionAportacion =' => $show, 'idPadre =' => $parent_category->getId()];
        foreach ($DaoCategoriaAportacion->getAll([1],$where_child) as $child_key => $child_category) {
          $DaoAportacion = new DaoAportacion();
          $count_contribution = 0;
          ?>
          <div class="panel-group accordion_categories" id="accordion_categories_<?php echo $child_key; ?>" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="category_<?php echo $child_key; ?>">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion_categories_<?php echo $child_key; ?>" href="#list_contributions_<?php echo $child_key; ?>" aria-expanded="true" aria-controls="list_contributions_<?php echo $child_key; ?>">
                    <?php echo $child_category->getTitulo(); ?>
                  </a>
                </h4>
              </div>
              <div id="list_contributions_<?php echo $child_key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="category_<?php echo $child_key; ?>">
                <ul class="list-group">
                  <?php foreach ($DaoAportacion->getAllByCategory($child_category->getId()) as $contribution) { ?>
                    <li class="list-group-item" data-title="<?php echo $contribution->getTitulo(); ?>" data-file="<?=(($logged) ? $contribution->getFile() : '')?>"><?php echo $contribution->getTitulo(); ?></li>
                    <?php $count_contribution++; } ?>
                    <?php if ($count_contribution == 0){ ?>
                      <li class="list-group-item" data-file=""><?=translate('No hay aportaciones asociadas a este tema')?></li>
                    <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        <?php $count_child_categories++; } ?>
        <?php if ($count_child_categories == 0){ ?>
          <p><?=translate('No hay subtemas asociados')?></p>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
</div>

  <script type="text/javascript">
    $(function(){
      $('body').on('click', '.accordion_categories .list-group-item', function(){
        var container = $('#contribution_pdf_preview');
        var file = $(this).data('file');
        var title = $(this).data('title');

        if (container.length && file.length > 5){
          container.html('<p class="text-center"><?=translate("Vista previa de")?> "' + title + '"</p><iframe src="uploads/aportaciones/' + file + '">' + file + '</iframe>');
        }
      });
    });
  </script>
