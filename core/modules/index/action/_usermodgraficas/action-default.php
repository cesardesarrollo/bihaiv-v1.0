<div class="row">
  <?php
  $session_id = @$_SESSION['user_id'];
  $perfil_id = @$params['user']->id;
  $mi_ferfil = ($session_id == $perfil_id) ? true: false;
    ?>
    <?php if ($mi_ferfil) : ?>
    <center>
      <button id="takeScreenShot" class="btn btn-primary btn-block" type="button" name="button"><?php echo translate('label descargaReporte')?> </button>
    </center>
    <script>
      /*
      * Script para tomar una captura de pantalla a las graficas y enviarlo
      * a otra ventana.
      */
      $takeScreenShot = ('#takeScreenShot');
        $(function(){
          var screeenCharts = $('#screeenCharts');
          $('#takeScreenShot').on('click',function(){
            html2canvas(screeenCharts, {
              onrendered: function(canvas) {
                var dataURL = canvas.toDataURL("image/jpg");
                open('POST', 'api/front/force_download_img.php', {request: dataURL}, '_blank');
              }
            })
          });
          open = function(verb, url, data, target) {
            var form = document.createElement("form");
            form.action = url;
            form.method = verb;
            form.target = target || "_self";
            if (data) {
              for (var key in data) {
                var input = document.createElement("textarea");
                input.name = key;
                input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
                form.appendChild(input);
              }
            }
            form.style.display = 'none';
            document.body.appendChild(form);
            form.submit();
          };

        });
    </script>
    <?php endif; ?>
  <br>
  <div id="screeenCharts" style="background-color:#ffffff;">
    <!-- Nivel de impacto en el ecosistema -->
    <md-card class="_md mapa-calor-primary">
      <md-card-header>
        <md-card-header-text>
          <span class="md-title">
            <b><?php echo translate('label nivelImpacto')?></b>
          </span>
          <span class="md-subhead">
            <img id="imgAchievement"src="assets/img/home/medals/1.png" class="medal-icon img-responsive" />
          </span>
        </md-card-header-text>
      </md-card-header>
      <md-card-content>
        <div class="progress">
          <div id="progress-bar-impacto-ecosistema" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="progress-stars">
            </span>
          </div>
        </div>
        <br>
      </md-card-content>
    </md-card>
    <hr>
    <!-- Nivel de incidencia -->
    <md-card class="_md">
      <md-card-header>
        <md-card-header-text>
          <span class="md-title">
            <b><?php echo translate('label nivelIncidencia')?></b>
          </span>
          <span class="md-subhead"></span>
        </md-card-header-text>
      </md-card-header>
      <md-card-content>
        <div class="progress">
          <div id="progress-bar-incidencias" class="progress-bar  progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="progress-stars">
            </span>
          </div>
        </div>
      </md-card-content>
    </md-card>
    <hr>
    <!-- REPORTE DE ENFOQUES -->
    <md-card class="_md">
      <md-card-header>
        <md-card-header-text>
          <span class="md-title">
            <b><?php echo translate('label nivelEnfoque')?></b>
          </span>
          <span class="md-subhead"></span>
        </md-card-header-text>
      </md-card-header>
      <md-card-content>
        <div class="progress">
          <div id="progress-bar-enfoques" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="progress-stars">
            </span>
          </div>
        </div>
      </md-card-content>
    </md-card>
    <hr>
    <!-- Actividad (iniciativas y eventos ) -->
    <md-card class="_md">
      <md-card-header>
        <md-card-header-text>
          <span class="md-title">
            <b><?php echo translate('label nivelActividad')?></b>
          </span>
          <span class="md-subhead"></span>
        </md-card-header-text>
      </md-card-header>
      <md-card-content>
        <div class="progress">
          <div id="progress-bar-actividad" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="progress-stars">
            </span>
          </div>
        </div>
      </md-card-content>
    </md-card>
    <hr>
    <!-- Nivel DE APORTACIONES -->
    <md-card class="_md">
      <md-card-header>
        <md-card-header-text>
          <span class="md-title">
            <b><?php echo translate('label nivelAportacion')?></b>
          </span>
          <span class="md-subhead"></span>
        </md-card-header-text>
      </md-card-header>
      <md-card-content>
        <div class="progress">
          <div id="progress-bar-aportaciones" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="progress-stars">
            </span>
          </div>
        </div>
      </md-card-content>
    </md-card>
    <hr>
    <!-- Colaboraciones -->
    <md-card class="_md">
      <md-card-header>
        <md-card-header-text>
          <span class="md-title">
            <b><?php echo translate('label nivelColaboracion')?></b>
          </span>
          <span class="md-subhead"></span>
        </md-card-header-text>
      </md-card-header>
      <md-card-content>
        <div class="progress">
          <div id="progress-bar-colaboraciones" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <span class="progress-stars">
            </span>
          </div>
        </div>
      </md-card-content>
    </md-card>
  </div>
</div>
<script>$(function(){$('[data-toggle="tooltip"]').tooltip();});</script>
<?php if (isset($params['organizacion']) AND is_numeric($params['organizacion']->id)  ) : ?>
    <?php $organizacion_id = $params['organizacion']->id; ?>
  <script>
    $(function(){
      $.post('api/front/reportes.php',{ method: 'charts', organizacion_id: '<?php echo $organizacion_id; ?>' },function(response){
        if (response.status){
          console.log(response.data);
          $.each(response.data, function(index, value){
            if (index == 'impacto-ecosistema'){
              $('#imgAchievement').attr('src', 'assets/img/home/medals/' + value.score + '.png')
            }
            $('#progress-bar-' + index).addClass('progress-bar-' + value.score )
            $('#progress-bar-' + index + ' .progress-stars' ).empty().html(value.stars);
          })
        } else {
          console.log('Error inesperado al generar gráficas de calor.')
        }
      });
      function setValues(){
        alert();
      }
    })
  </script>
<?php endif; ?>
