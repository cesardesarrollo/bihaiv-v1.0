<?php
$from = $params["from"];
$user = $params["user"];
$organizacion = $params["organizacion"];

$url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
$gotourl = "";
$id = 0;
$is_mine = false;

if ($from == "logged" && $user->id == $_SESSION["user_id"]) {
    $id = $_SESSION["user_id"];  
    $gotourl = "./?view=home";
    $is_mine = true;
} else { 
    $gotourl = "./?view=home&id=".$user->id; 
    $id = $user->id;  
    $is_mine = false;
}

// Eventos
$DaoOrganizacion = new DaoOrganizacion();
$Organizacion = $DaoOrganizacion->getByUserId($id);
?>       
  <div class="Wall margin-row-bottom">
    <div class="Wall__header">
      <figure class="figure display-inline">
        <img src="assets/img/home/calendario.png" class="img-responsive">
      </figure>
      <p class="display-inline text-white"><?php echo translate('Eventos')?></p>
        <?php if ($is_mine ) { ?>
          <div class="pull-right" style="margin-top:0px">
            <button onclick="openModal('m-new-event','<?php echo $id ?>')" class="btn btn-outline-primary btn-xs">
            <i class="fa fa-plus"></i> <?php echo translate('Agregar Nuevo')?>
            </button>
          </div>
        <?php } ?>
    </div>
    <div class="Wall__content padding">
      <div id="calendar"></div>
    </div>
  </div>
  <div id="modal-detail" aria-hidden="true" role="dialog" data-backdrop="static" class="modal fade"></div>
  <script>
    $(document).ready(function() {
      var evento = new Evento();
      var params= new Object();
      params._method = "allInCalendar";
      params._action = "coomingEventsForCalendar";
      params._calendarId = "calendar";
      params._userid = <?php echo $Organizacion->id; ?>;
      evento._set(params);
    });
  </script>
