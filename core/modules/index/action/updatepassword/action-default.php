<?php
/**
* @description Cambiar la contraseña
**/
if (isset($_SESSION["user_id"]) && !empty($_POST)) {
    $user = UserData::getById($_SESSION["user_id"]);
    if ($user->password==sha1(md5($_POST["password"])) ) {
        if ($_POST["new_password"]===$_POST["confirm_password"] ) {
            if (strlen($_POST["new_password"]) >= 8 ) {
                $user->password = sha1(md5($_POST["new_password"]));
                $user->update_passwd();
                $_SESSION["password_updated"]=true;
                Core::alert(utf8_decode("¡La contraseña ha sido actualizada exitosamente!"));
                Core::redir("./?view=configuration");            
            } else {
                Core::alert(utf8_decode("La contraseña debe de ser de mínimo 8 caracteres"));
                Core::redir("./?view=configuration");            
            }
        } else {
            Core::alert(utf8_decode("Las contraseñas no coinciden."));
            Core::redir("./?view=configuration");            
        }
    } else {
        Core::alert(utf8_decode("La contraseña introducida es incorrecta."));
        Core::redir("./?view=configuration");
    }
}



?>
