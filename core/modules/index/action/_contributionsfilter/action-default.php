  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contributions-filter">
    <md-card class="_md">
      <md-card-content>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
              <label for="contribution-filter"><?=translate('Mostrar')?>:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <select class="form-control" id="contribution-filter">
                <option value="1" selected><?=translate('Más recientes')?></option>
                <option value="2"><?=translate('Más vistos')?></option>
                <option value="3"><?=translate('Mejor evaluados')?></option>
                <option value="4"><?=translate('Por perfil de actor')?></option>
                <option value="5"><?=translate('Por mes')?></option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 hidden" id="actor-profile">
          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
              <label for="tipoContribucion"><?=translate('Perfil')?>:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <select class="form-control extra-contribution-filter" id="actor-profile-filter">
                <?php
                $DaoRoles = new DaoRoles();

                foreach ($DaoRoles->getAll() as $rol) {
                    ?>
                    <option value="<?php echo $rol->getId() ?>"><?php echo $rol->getNombre() ?></option>
                    <?php
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 hidden" id="contribution-month">
          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
              <label for="tipoContribucion"><?=translate('Mes')?>:</label>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <select class="form-control extra-contribution-filter" id="contribution-month-filter">
                <?php
                $DaoPublicaciones = new DaoPublicaciones();

                $months = translate('months');

                foreach ($months as $m => $month) {
                  $new_m = $m+1;
                  ($new_m < 10) ? $month_id = '0'.$new_m : $month_id = $new_m;

                  $first_day = date('Y').'-'.$month_id.'-01 00:00:00';
                  $last_day = date('Y-'.$month_id.'-t', strtotime($first_day)).' 23:59:59';

                  $where_count = [
                    'a.idCategoriaAportacion =' => 0,
                    'p.fechaCreado >=' => '"'.$first_day.'"',
                    'p.fechaCreado <=' => '"'.$last_day.'"'
                  ];

                  $count_rows = $DaoPublicaciones->countAllBySection([1],$where_count);
                  $total = $count_rows[0]['count'];
                ?>
                  <option value="<?php echo $new_m; ?>" <?php if ($total == 0) echo 'disabled'; ?>><?php echo $month; ?> (<?php echo $total; ?>)</option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <button type="button" class="btn btn-primary" id="apply-contributions-filter" ><?=translate('Filtrar')?></button>
          </div>
        </div>
      </md-card-content>
    </md-card>
  </div>

  <script>
    function setContributionsFilters(){
      var params= new Object()
        params.method="updatePage";
        params.filterId=$('#contribution-filter').val();
        params.filter=$('#contribution-filter :selected').text();
        params.extraFilterId=$('.extra-contribution-filter:visible').val() || 0;
        params.extraFilter=$('.extra-contribution-filter:visible  :selected').text() || '';

        return params;
    }

    function updatePage(offset = 0){
      var params = setContributionsFilters();
        params.offset=offset;

      $.post("core/modules/index/action/_contributionsgrid/action-default.php",params,function(resp){
          $('#contributions-grid-update').html(resp);
      }) 
    }

    $(function(){
      $('body').on('click','#apply-contributions-filter', function(){
        if ($('#contributions-grid').length){
          updatePage();
        }
      });

      $('body').on('change','#contribution-filter', function(){
        if ($(this).val() == 4){
          $('#contribution-month').addClass('hidden');
          $('#actor-profile').removeClass('hidden');
        }else if ($(this).val() == 5){
          $('#actor-profile').addClass('hidden');
          $('#contribution-month').removeClass('hidden');
        } else {
          $('#actor-profile').addClass('hidden');
          $('#contribution-month').addClass('hidden');
        }
      });
    });
  </script>
