<?php

if (!empty($_POST)) {
    if ($_POST["email"]!="") {
        $user = UserData::getByEmail($_POST["email"]);
        if ($user!=null) {
            $msg = "<h1>Recordar contraseña</h1>
				<p>Has solicitado recuperar tu contraseña, da click en el  siguiente enlace para crear una nueva contraseña:</p>
				<p><a href='".APP_PATH."?view=recoveraccount&e=".sha1(md5($_POST["email"]))."&c=".sha1(md5($user->code))."'>Recupera tu cuenta:</a></p>
				";
            Core::mail($_POST["email"], "Bihaiv - Pasos a seguir para recuperar tu cuenta", $msg, $msg);
            Core::alert(utf8_decode("Email enviado con  los pasos a seguir para la recuperación de cuenta!."));
            Core::redir(APP_PATH);
        } else {
            Core::alert(utf8_decode("El email proporcionado no está registrado."));
            Core::goBack(1);
        }
    } else {
        Core::alert(utf8_decode("No puede dejar campos vacios"));
        Core::goBack(1);
    }
}
?>
