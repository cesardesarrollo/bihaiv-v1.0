<?php
  $from = $params["from"];
  $user = $params["user"];
  $organizacion = $params["organizacion"];

  $url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
  $gotourl = "";
  $id = 0;
  $is_mine = false;

  if ($from == "logged" && $user->id == $_SESSION["user_id"]) {
      $id = $_SESSION["user_id"];  
      $gotourl = "./?view=home";
      $is_mine = true;
  } else { 
      $gotourl = "./?view=home&id=".$user->id; 
      $id = $user->id;  
      $is_mine = false;
  }
?>
  <div class="Wall margin-row-bottom">
    <div class="Wall__header">
      <figure class="figure display-inline">
        <img src="assets/img/home/misredes.png" class="img-responsive">
      </figure>
      <p class="display-inline text-white"><?php echo translate('label Noticias')?>
      </p>
    </div>
    <div class="Wall__content padding">
      <!--FOR EACH-->
      <div id="notificacion">
        <?php if ($is_mine ) { ?>
          <h3><?php echo translate('label notieneNoticia')?></h3>
        <?php } ?>
      </div>
      <div class="Wall__footer padding">
        <div class="pull-right">
          <a id="notifications-more" class="btn btn-default" data-user="<?php echo $id ?>" href="#"><?php echo translate('label vermas')?></a>
        </div>
      </div>
      <!--END FOR EACH-->
    </div>
  </div>
  <script id="notificacion-template" type="x-text/handlebars" >
      {{#each this}}
        <div class="row row-colaboration">
          <div class="col-sm-12 col-lg-12">
            <p>
              {{{mensaje}}}
            </p>
          </div>
        </div>
      {{/each}}
  </script>
  <script>
    $(function(){
      var notificacion = new Notificaciones();
      var params = {};
      params._method = "allByLimits";
      params._userid = <?php echo $id ?>;
      //params._limit_param1 = 0;
      //params._limit_param2 = 5;
      notificacion._set(params);
    });
  </script>
