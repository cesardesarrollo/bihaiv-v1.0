<?php
  $user = $params["user"];
  $organizacion = $params["organizacion"];
  $from = $params["from"];

  $DaoOauth = new DaoOauth();
  $oauths = $DaoOauth->getOauthsByIdUsu($organizacion->id);

  $twitter = Core::initTwitterAPI();
  $twitter_linked = false;
  $facebook_url = '';

  foreach ($oauths as $oauth) {
    if ($oauth->getServicio() == 'Twitter' && !empty($oauth->getAccessToken()) && !empty($oauth->getAccessTokenSecret()) && !empty($oauth->getUrlRed()) ) {
      $twitter->setToken($oauth->getAccessToken(), $oauth->getAccessTokenSecret());

      $verify = $twitter->account_verifyCredentials();

      if ($verify->httpstatus == 200) {
        $twitter_linked = true;
      }
    } else if ($oauth->getServicio() == 'Facebook' ) {
      $facebook_url = $oauth->getUrlRed();
    }
  }

?>

<div id="fb-root"></div>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '311769845864426',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_LA/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
  
  <div class="Wall margin-row-bottom">
    <div class="Wall__header">
      <figure class="figure display-inline">
        <img src="assets/img/home/misredes.png" class="img-responsive">
      </figure>
      <p class="display-inline text-white"><?php echo translate('Mis redes'); ?></p>
    </div>
    <div class="Wall__content padding">
      <div class="twt-wrapper col-xs-6 col-md-6 col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                Tweets recientes
            </div>
            <div class="panel-body">
                <ul class="media-list">
                    <?php
                    if ($twitter_linked == true) {
                        $count = 0;
                        $params = [ 'user_id' => $verify->id, 'count' => 5];
                        foreach ($twitter->statuses_userTimeline($params) as $tweet) {
                            if (isset($tweet->id) && !empty($tweet->id)) {
                                $created_at = date_create($tweet->created_at);
                        ?>

                        <li class="media">
                        <a href="#" class="pull-left" style="margin-top: 22px">
                            <img src="<?php echo $tweet->user->profile_image_url; ?>" alt="" class="img-circle">
                        </a>
                        <div class="media-body">
                            <span class="text-muted pull-right">
                                <small class="text-muted"><?php echo date_format($created_at, "d/m/Y H:i:s") ?></small>
                            </span>
                            <strong class="text-success">@<?php echo $tweet->user->screen_name; ?></strong>
                            <p><?php echo $tweet->text; ?></p>
                        </div>
                      </li>
                        <?php
                            $count++;
                            }
                        }
                        if ($count == 0) {
                    ?>

                    <li class="media">
                        <p>Esta cuenta no tiene tweets aún.</p>
                    </li>

                    <?php
                        }
                    } else { 
                        if (isset($_SESSION["user_id"]) && $_SESSION["user_id"] != $user->id) {
                    ?>

                    <li class="media">
                      <p>No es posible conectarse a esta cuenta de twitter.</p>
                    </li>

                    <?php } else { ?>

                    <li class="media">
                        <p>No es posible conectarse a tu cuenta de twitter, ve a <a href="./?view=editinformation">"Editar Información"</a> y vuelve a vincular tu cuenta.</p>
                    </li>

                    <?php
                      } 
                    } 
                    ?>
                </ul>
            </div>
        </div>
      </div>
      <div class="col-xs-6 col-md-6 col-lg-6" id="facebook_timeline">
        
      </div>
    </div>
  </div>

  <script>
    function validateFacebookURL(url){
      var error_html = '<div class="alert alert-warning" role="alert"><i class="fa fa-warning"></i> El url de la página de Facebook no es válido</div>';

      var plugin_html = '<div class="fb-page" data-href="'+url+'" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-height="525" data-show-facepile="false"><blockquote cite="'+url+'" class="fb-xfbml-parse-ignore"><a href="'+url+'">Facebook</a></blockquote></div>';

      var strRegex = "^((https|http)?://)"
        + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?"
        + "(([0-9]{1,3}\.){3}[0-9]{1,3}"
        + "|"
        + "([0-9a-z_!~*'()-]+\.)*"
        + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\."
        + "[a-z]{2,6})"
        + "(:[0-9]{1,4})?"
        + "((/?)|"
        + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
      
      var re = new RegExp(strRegex);

      if (!$.isEmptyObject(url) && re.test(url)){
        $('#facebook_timeline').html(plugin_html);
      } else {
        $('#facebook_timeline').html(error_html);
      }
    }

    $(function(){
      validateFacebookURL('<?php echo $facebook_url; ?>');
    });
  </script>
