<?php

  $logged = false;
  if (isset($_SESSION["user_id"])) {
    $logged = true; 
  }

  $from = $params["from"];
  $user = $params["user"];
  $organizacion = $params["organizacion"];

  $SESSION_user_id = "";
  if (isset($_SESSION["user_id"])) {
      $SESSION_user_id = $_SESSION["user_id"];
  }

  $userInfo = UserData::userInformation($user->id);
  // Avatar
  if ($organizacion->avatar!="assets/img/home/perfil.png" && $organizacion->avatar!="") {
      $img_url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
  } else {
      $img_url = "assets/img/home/perfil.png";
  }
  $gotourl = "";
  if ($from=="logged" && $user->id==$SESSION_user_id) {
      $gotourl = "./?view=home";
  } else {
      $gotourl= "./?view=home&id=".$user->id;
  }
?>
<style>
  #user-profile-actions a{
    color:#f37064 !important;
  }
  #user-profile-actions a:hover{
    color:#fff !important;
  }
  .avatarv2{
    height: 100%;
    width: 100%;
    background-color: white;
    background-image: url(<?php echo $img_url; ?>);
    background-position: center center;
    background-size: cover;
  }
</style>
<div class="">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="Profile__info">
      <div class="col-md-12">
        <div class="Profile__info--photo">
          <img class="avatarv2 img-responsive">
            <?php if ($logged) : ?>
          <!-- HERE COMES NEW CHALLENGER!!! -->
          <ul class="list-group transparent no-margin no-padding-top no-padding-bottom" style="    position: absolute; z-index: 5; width: 100%;">
            <li class="list-group-item">
                <?php if ($SESSION_user_id==$user->id) :?>
                <a href="./?view=editinformation">
                  <i class="fa fa-pencil-square"></i><?php echo translate('Editar Información'); ?>
                </a>
                <?php else: ?>
                <a href="./?view=userinfo&uid=<?php echo $user->id; ?>">
                  <i class="fa fa-pencil-square"></i><?php echo translate('Ver información de perfil'); ?>
                </a>
                <?php endif; ?>
            </li>
            <li class="list-group-item">
              <a href="./?view=friends&uid=<?php echo $user->id; ?>">
                <i class="fa fa-users"></i><?php echo translate('Mis conexiones'); ?>
              </a>
            </li>
          </ul>
            <?php endif; ?>
        </div>
        <div class="Profile__info--title">
          <h2 class="Role--Profile">
            <span id="role">
              <span id="company" class="small">
                <?php
                if (!isset($userInfo->_role) ) {
                    echo translate('Rol sin asignar');
                } else {
                    echo @$userInfo->_role;
                }
                ?> - <?php echo @$userInfo->_urlweb ?>
              </span>
            </span>
          </h2>
          <h2 class="Company--Profile">
            <a href="<?php echo $gotourl; ?>">
                <?php echo $user->getFullname(); ?>
            </a>
            <?php if ($from=="logout") :?>
              <!-- No se muestran botones -->
            <?php else:?>
                <?php if ($user->id==Session::$user->id) :?>
                <?php else:
                $fs = FriendData::getFriendship($SESSION_user_id, $user->id);
                ?>
                <?php if ($fs==null) :?>
                  <!--<a href="./?action=sendfriendreq&recid=<?php //echo $user->id; ?>" class="btn btn-primary btn-xs"><?php //echo translate('Solicitud de Amistad'); ?></a>-->
                <?php else:?>
                    <?php if ($fs->is_accepted) :?>
                    <button class="btn btn-primary btn-xs"><?php echo translate('Amigo'); ?></button>
                    <?php else:?>
                    <?php if ($SESSION_user_id!=$user->id) :?>
                      <button class="btn btn-success btn-xs"><?php echo translate('Solicitud Enviada'); ?></button>
                    <?php else: ?>
                        <?php $fs->read(); ?>
                      <a href="./?action=acceptfriendreq&recid=<?php echo $user->id; ?>" class="btn btn-success btn-xs"><?php echo translate('Aceptar Solicitud'); ?></a>
                    <?php endif;?>
                    <?php endif;?>
                <?php endif; ?>
                <p class="Contacts__profile--button">
                  <div class="dropdown" id="user-profile-actions">
                    <button class="btn btn-outline-primary text-orange dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <?php echo translate('Acciones'); ?>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <?php if ($fs==null) :?>
                        <li><a href="./?action=sendfriendreq&recid=<?php echo $user->id; ?>"><?php echo translate('Vincularse'); ?></a></li>
                        <?php endif; ?>
                      <li><a href="#" onclick="openModalActions('m-sugest-profile', '<?php echo $user->id; ?>', '<?php echo $user->name; ?>')" data-user_name="<?php echo $user->name; ?>" data-user_id="<?php echo $user->id; ?>"><?php echo translate('Vincular con'); ?></a></li>
                      <li><a href="#>" onclick="openModalActions('m-send-msg', '<?php echo $user->id; ?>', '<?php echo $user->name; ?>')"><?php echo translate('Mandar mensaje'); ?></a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#" onclick="openModalActions('m-report-user', '<?php echo $user->id; ?>', '<?php echo $user->name; ?>')"><?php echo translate('Reportar'); ?></a></li>
                    </ul>
                  </div>
                </p>
                <?php endif; ?>
            <?php endif; ?>
          </h2>
          <p class="Description--profile">
            <span id="description" >
                <?php echo @$userInfo->_quiensoy; ?>
            </span>
          </p>
            <ol class="breadcrumb transparent no-margin no-padding-top no-padding-bottom">
                <?php if ($userInfo->_Facebook !== "") : ?>
                <li>
                  <a href="<?php echo @$userInfo->_Facebook; ?>" target="_new">
                    <img src="assets/img/home/FB.PNG" alt="" height="20">
                  </a>
                </li>
                <?php endif; ?>
                <?php if ($userInfo->_Twitter !== "") : ?>
                <li>
                  <a href="<?php echo @$userInfo->_Twitter; ?>" target="_new">
                    <img src="assets/img/home/TWITTER.PNG" alt="" height="20">
                  </a>
                </li>
                <?php endif; ?>
                <?php if ($userInfo->_lng !== "" && $userInfo->_lat !== "") : ?>
                <li>
                  <a target="new" href="https://www.google.com.mx/maps/@<?php echo $userInfo->_lat; ?>,<?php echo $userInfo->_lng; ?>">
                    <img src="assets/img/home/ubicacion.png" alt="" height="20">
                  </a>
                </li>
                <?php endif; ?>
            </ol>
            <!-- Here comes new challenger -->
            <!-- Validamos la session -->
            <?php if (isset($_SESSION['user_id']) AND is_numeric($_SESSION['user_id']) ) : ?>
              <!-- Validamos que el usuario sea diferente al id del perfil -->
                <?php if ($_SESSION['user_id'] != $params['user']->id) : ?>
                  <p style="">
                    <input type="hidden" id="calificador" value="<?php echo $_SESSION['user_id']; ?>">
                    <input type="hidden" id="organizacionId" value="<?php echo $params['organizacion']->id; ?>">
                    <div style="" class="rateYo-organizacion"></div>
                  </p>
                  <script>
                    $(function(){
                      var url_api_actores = 'api/front/actores.php';
                      var ratePerfilDefault = 0;
                      var calificador       = $('#calificador').val();
                      var organizacionId    = $('#organizacionId').val();
                      // GET current calificación
                      $.post(url_api_actores, {
                        'user_id' : calificador,
                        'organizacion_calificada_id' : organizacionId,
                        'method': 'get_rating'
                      },function(response){
                        if (response.status){
                          ratePerfilDefault = parseInt(response.calificacion);
                        }
                        $rateYo = $(".rateYo-organizacion").rateYo({
                          rating    : ratePerfilDefault,
                          starWidth : "16px",
                          ratedFill : '#fae617',
                          precision : 0,
                          spacing   : "2px",
                          onSet: function (rating, rateYoInstance) {
                            // valida calificacion
                            if ( parseInt(rating) > 0 ){
                              //valida id de calificador
                              if ( parseInt( calificador ) > 0 ){
                                // Valida id de organizacion a calificar
                                if ( parseInt( organizacionId ) > 0 ){
                                  var data = {
                                    'calificacion'                : rating,
                                    'user_id'                     : calificador,
                                    'organizacion_calificada_id'  : organizacionId,
                                    'method'                      : 'update_rating',
                                  };
                                  $.post(url_api_actores, data, function(response){
                                    setFlash(response.msg, response.class);
                                  });
                                } else {
                                  setFlash('Ocurrio un error inesperado ', 'warning');
                                }
                              } else {
                                setFlash('Ocurrio un error inesperado ', 'warning');
                              }

                            } else {
                              setFlash('No puedes otorgar una calificacion menor a 1 intentalo de nuevo. ', 'warning');
                            }
                          }
                        });
                      });
                    });
                  </script>
                <?php endif; ?>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
