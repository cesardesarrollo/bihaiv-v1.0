<?php

  $logged = false;
if (isset($_SESSION["user_id"])) { $logged = true; 
}

  $show = $params["show"];
  $DaoPublicaciones = new DaoPublicaciones();
  $where = [ 'ca.idSeccionAportacion =' => $show ];

foreach ($DaoPublicaciones->getAllBySection([1], $where, 0, 3, 'p.fechaCreado DESC') as $publicacion) {
    $fecha = new DateTime($publicacion['fechaCreado']);   

    if (strlen($publicacion['descripcion']) > 100) {
        $descripcion = substr($publicacion['descripcion'], 0, 100);
        $descripcion .= '...';
    } else {
        $descripcion = $publicacion['descripcion'];
    }
?>
<md-card class="_md">
<md-card-header>
  <md-card-header-text>
    <span class="md-title">
      <b>
        <?php if ($logged ) : ?><a href="#" class="recent_contribution" data-file="<?php echo $publicacion['file']; ?>" data-title="<?php echo $publicacion['titulo']; ?>"><?php endif; ?>
            <?php echo $publicacion['titulo']; ?>
        <?php if ($logged ) : ?></a><?php endif; ?>
      </b>
    </span>
    </span>
    <span class="md-subhead">
      <small><?php echo $fecha->format('d/m/Y'); ?></small>
    </span>
  </md-card-header-text>
</md-card-header>
<md-card-content>
  <p><?php echo $descripcion; ?></p>
</md-card-content>
</md-card>
<hr>

<?php } ?>

  <script>
    $(function(){
      $('body').on('click', 'a.recent_contribution', function(e){
        e.preventDefault();
        var container = $('#contribution_pdf_preview');
        var file = $(this).data('file');
        var title = $(this).data('title');

        if (container.length && file.length > 5){
          container.html('<p class="text-center"><?php echo translate("Vista previa de")?> "' + title + '"</p><iframe src="uploads/aportaciones/' + file + '">' + file + '</iframe>');
        }
      });
    });
  </script>
