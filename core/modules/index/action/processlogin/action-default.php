<?php
header('Content-Type: text/html; charset=utf-8');

require_once 'core/modules/index/model/UserData.php';

function RandomString($length=12, $uc=true, $n=true, $sc=false) 
{
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if ($uc==1) {
      $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if ($n==1) {
      $source .= '1234567890';
    }
    if ($sc==1) {
      $source .= '|@#~$%()=^*+&#91;&#93;{}-_';
    }
    if ($length>0) {
      $rstr = "";
      $source = str_split($source, 1);
      for ($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1, count($source));
            $rstr .= $source[$num-1];
      }
    }
    return $rstr;
}

function admCookiesToken($recuerdame='', $email='') 
{

    // Cookies de inicio de sesión
    if (@$recuerdame === 'on') {

        // Elimina token existente de cookie y tabla
        if (Cookie::exist('bihaiv_token')) {
            if (Cookie::geter('bihaiv_token') !== "") {
                Cookie::seter('bihaiv_token', "");
                $token = Cookie::geter('bihaiv_token');
                $sql = "delete from token where token = '".$token."' ";
                $delete = Executor::doit($sql);
            }
        }
        // Genera nuevo token
        $token = RandomString().sha1(md5($email));
        Cookie::seter('bihaiv_token', $token);

        $sql = "insert into token (token,fecha_creado) ";
        $sql .= "value (\"$token\",\"".date('Y-m-d H:i:s')."\")";
        $response = Executor::doit($sql);

    } else {
        if (Cookie::exist('bihaiv_token')) {
            if (Cookie::geter('bihaiv_token') !== "") {
                Cookie::seter('bihaiv_token', "");
                $token = Cookie::geter('bihaiv_token');
                $sql = "delete from token where token = '".$token."' ";
                $delete = Executor::doit($sql);
            }
        }
    }
    return true;
}

function isTheEmailForThisToken($email)
{
    // Existen cookie y tabla
    if (Cookie::exist('bihaiv_token')) {
        if (Cookie::geter('bihaiv_token') !== "") {
            $token = Cookie::geter('bihaiv_token');
            $sql = "SELECT password,email FROM user WHERE sha1(md5(email)) = '".substr($token, 12)."' LIMIT 0,1";
            $query = Executor::doit($sql);
            $user = Model::one($query[0], new UserData());
        }
    }
    if (isset($user->email)) {
        return true;
    }
    return false;
}

if (!empty($_POST)) {
    if ($_POST["email"]!=""&&$_POST["password"]!="") {
        $password = sha1(md5($_POST["password"]));
        $user = UserData::getLogin($_POST["email"], $password);
        if ($user!=null) {

            //si el usuario quiere memorizar su cuenta en este ordenador
            if (isset($_POST["recuerdame"]) && $_POST["recuerdame"] == "on") {
                //creo una marca aleatoria en el registro de este usuario
                mt_srand(time());
                //generamos un número aleatorio
                $numero_aleatorio = mt_rand(1000000, 999999999);
                //meto la marca aleatoria en la tabla de usuario
                $user_set_cookie = UserData::setCookie($numero_aleatorio, $user->id);
                if ($user_set_cookie[0]) {
                    //ahora meto una cookie en el ordenador del usuario con el identificador del usuario y la cookie aleatoria
                    setcookie("id_usuario", $user->id, time()+(60*60*24*7));
                    setcookie("marca_aleatoria_usuario", $numero_aleatorio, time()+(60*60*24*7));
                }
            }

            // Validaciones
            if (!$user->is_active) {    //Administrador debe estar activo
                Core::alert("Debes verificar tu correo electrónico para validar tu dirección de correo.");
                Core::redir("./");
            }

            // Es ADMINISTRADOR
            if ($user->is_admin) {    //Administrador debe estar activo
                Session::set("user_id", $user->id);
                Session::set("is_expert", false);
                Session::set("is_admin", true); /* Validacion para saber si es admin */
                Core::redir("./admin");
            }

            // Es ACTOR
            else if ($user->tipoRel == 1) {
                Session::set("user_id", $user->id);
                Session::set("is_expert", false);
                Session::set("is_admin", false);
          
                // Redirigiendo si referencia es evaluación o no
                if (isset($_POST['referer'])) {
                    Core::redir($_POST['referer']);
                } else {
                    Core::redir("./?view=home");
                }

            } 

            // Es EXPERTO
            else {
                if ($user->tipoRel == 2) {

                    Session::set("user_id", $user->id);
                    Session::set("is_expert", true);
                    Session::set("is_admin", false);

                    $DaoExperto = new DaoExperto();
                    $experto = $DaoExperto->getExpertByUserId($user->id);
                    Session::set("experto_id", $experto->id);

                    Core::alert("Bienvenido Experto");

                    // Redirigiendo si referencia es evaluación o no
                    if (isset($_POST['referer'])) {
                        Core::redir($_POST['referer']);
                    } else {
                        Core::redir("./admin");
                    }

                } else {
                    Core::alert("Debes verificar tu correo electrónico para seguir los pasos de validación, por ahora tu participación en Bi Haiv estará limitada.");
                    Core::redir("./");
                }
            }
        } else {
            Core::alert("Datos incorrectos");
            Core::goBack(1);
        }
    } else {
        Core::alert("Datos vacios");
        Core::goBack(1);
    }
}

?>
