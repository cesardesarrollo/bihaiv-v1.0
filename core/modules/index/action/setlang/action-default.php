<?php
    /**
    * @author cmagana
    * @description Setea cookie de lenguaje seleccionado y regresa a página anterior.
    **/
    setcookie('bihaiv_lang', $_GET['lang'], time() + (86400 * 30), "/"); // 86400 = 1 día
    echo "<script type='text/javascript'>";
    echo "window.history.back(-1)";
    echo "</script>";
?>
