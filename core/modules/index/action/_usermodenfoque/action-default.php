<?php
$from = $params["from"];
$user = $params["user"];
$organizacion = $params["organizacion"];

$url = "storage/users/".$user->id."/profile/".$organizacion->avatar;
$gotourl = "";
$id = 0;
$is_mine = false;

if ($from == "logged" && $user->id == $_SESSION["user_id"]) {
    $id = $_SESSION["user_id"];
    $gotourl = "./?view=home";
    $is_mine = true;
} else {
    $gotourl = "./?view=home&id=".$user->id;
    $id = $user->id;
    $is_mine = false;
}
?>
<div class="Wall margin-row-bottom">
  <div class="Wall__header">
    <figure class="figure display-inline">
      <img src="assets/img/home/enfoque.png" class="img-responsive">
    </figure>
    <p class="display-inline text-white"><?php echo translate('Enfoques'); ?></p>
    <?php if ($is_mine ) : ?>
      <div id="divAddFocus" class="pull-right" style="margin-top:0px; display:none;">
        <button id="btnAddFocus" onclick="openModal('m-new-focus', '<?php echo $id ?>')" class="btn btn-outline-primary btn-xs">
          <i class="fa fa-plus"></i> <span class="hidden-xs"> <?php echo translate('Agregar Nuevo'); ?> </span>
        </button>
      </div>
    <?php endif; ?>
  </div>
  <div class="Wall__content padding">
    <div id="focus"></div>
  </div>
  <div class="Wall__footer"></div>
</div>

<script id="focus-template" type="x-text/handlebars" >
  <ul class="list-group focus">
    {{#each this}}
      <li  id="leads-template" class="">
        <div class="col-xs-12 col-md-9 col-lg-9 margin-row-bottom">
          <i class="fa fa-dot-circle-o"></i> {{_texto}}
        </div>
        <div class="col-xs-12 col-md-3 col-lg-3">
          <div class="pull-left">
            <?php if ($is_mine ) { ?>
              {{#ifEnfoque _statusPerCreationDate _statusPerDay}}
                <i onclick="showFocusEdit({{_idEnfoque}});" class="fa fa-pencil icons"></i>
              {{/ifEnfoque}}
            <?php } ?>
            &nbsp;&nbsp;
            {{{_stars}}}
          </div>
        </div>
      </li>
    {{/each}}
  </ul>
</script>
<script>
  $(function(){
    var enfoque = new Enfoque();
    var params= new Object();
    params._method = "all";
    params._userid = <?php echo $id ?>;
    enfoque._set(params);
  });
  function showFocusEdit(idEnfoque){
    // var e = event;
    // e.preventDefault();
    var enfoque = new Enfoque();
    var params = {
      _id : idEnfoque,
      _method : 'byId',
      _userid : <?php echo $id ?>
    };
    enfoque._set(params);
  }
</script>
<!-- CALIFICAR APORTACIONES SIEMPRE Y CUANDO NO ME PERTENEZCAN -->
<?php if (!$is_mine AND ( isset($_SESSION['user_id']) AND is_numeric($_SESSION['user_id']) ) ) : ?>
  <script>
  $( document ).ajaxComplete(function( event,request, settings ) {
    // Vars 2 send
    var user_id = '<?php echo $_SESSION['user_id']; ?>';

    $rateYo = $(".rateYo-enfoques").rateYo({
      rating    : 0,
      starWidth : "16px",
      ratedFill : '#fae617',
      precision : 0,
      spacing   : "2px",
      onSet: function (rating, rateYoInstance) {
        if ( parseInt(rating) > 0 ){
          var data = {
            '_id'             : $(this).data('idenfoque'),
            '_qualification'  : rating,
            '_userid'         : user_id,
            '_method'         : 'update_rating',
          };
          var rating = new Enfoque();
          rating._set(data);
        } else {
          setFlash('Para calificar es necesario ingresar al menos una estrella', 'warning');
        }
      }
    });
  });
  </script>
<?php endif; ?>
