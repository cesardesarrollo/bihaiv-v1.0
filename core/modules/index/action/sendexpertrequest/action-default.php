<?php
header('Content-Type: text/html; charset=utf-8');

function isValidEmail($email) 
{
    return filter_var($email, FILTER_VALIDATE_EMAIL) 
        && preg_match('/@.+\./', $email);
}

function isValidString($string) 
{
    return hasMinLength($string)
        && hasMaxLength($string)
        && isNotEmpty($string);
}

function isNotEmpty($string) 
{
    return strlen(trim($string)) > 0;
}

function hasMinLength($string) 
{
    return strlen($string) > 5;
}

function hasMaxLength($string) 
{
    return strlen($string) <= 100;
}

if (!empty($_POST)) {
    if (isValidEmail($_POST["request_email"]) && isValidString($_POST["request_name"]) && isValidString($_POST["request_message"])) {
        $msg_user = "<h1>Envío de solicitud exitoso</h1>
            <p>El equipo de Bihaiv ha recibido tu postulación como experto, pronto un asesor te contactará para darle seguimiento a tu solicitud.</p>
            ";
        $msg_admin = "<h1>Postulación como experto</h1>
            <p>".$_POST["request_name"]." a través del portal Bihaiv ha enviado una solicitud para ser experto. Los datos proporcionados son:</p>
            <p><b>Correo electrónico: </b>".$_POST["request_email"]."</p>
            <p><b>Mensaje: </b>".$_POST["request_message"]."</p>
            ";
        $admin = UserData::getAdmin();
        if ($admin != null && isValidEmail($admin->email)) {
            Core::mail($_POST["request_email"], "Envío de solicitud exitoso", $msg_user, $msg_user);
            Core::mail($admin->email, "Postulación como experto - ".$_POST["request_name"], $msg_admin, $msg_admin);
            Core::alert("Envío de solicitud exitoso, tu postulación como experto ha sido enviada al administrador del sitio.");
            Core::redir("./?view=experts");
        } else {
            Core::alert("No fue posible enviar tu solicitud al administrador, por favor intenta más tarde.");
            Core::redir("./?view=experts");
        }
    } else if (!hasMinLength($_POST["request_name"]) || !isNotEmpty($_POST["request_name"])) {
        Core::alert("El nombre debe tener más de 5 caracteres");
        Core::goBack(1);
    } else if (!hasMaxLength($_POST["request_name"])) {
        Core::alert("El nombre no debe tener más de 100 caracteres");
        Core::goBack(1);
    } else if (!isValidEmail($_POST["request_email"])) {
        Core::alert("El correo electrónico es inválido");
        Core::goBack(1);
    } else if (!hasMinLength($_POST["request_message"]) || !isNotEmpty($_POST["request_message"])) {
        Core::alert("El mensaje debe tener más de 5 caracteres");
        Core::goBack(1);
    } else if (!hasMaxLength($_POST["request_message"])) {
        Core::alert("El mensaje no debe tener más de 100 caracteres");
        Core::goBack(1);
    } else {
        Core::alert("Por favor complete los campos requeridos");
        Core::goBack(1);
    }
} else {
    Core::alert("Por favor complete los campos requeridos");
    Core::redir("./?view=experts");
}

?>
