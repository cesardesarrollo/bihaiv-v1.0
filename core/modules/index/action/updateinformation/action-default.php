<?php
error_reporting(0);
require_once 'core/modules/index/model/DaoOrganizacion.php';
require_once 'core/modules/index/model/DaoUbicaciones.php';
require_once 'core/modules/index/model/UserData.php';
require_once 'core/modules/index/model/DaoOauth.php';

require_once 'core/modules/index/model/DaoPaises.php';
require_once 'core/modules/index/model/DaoEstados.php';
require_once 'core/modules/index/model/DaoLocalidades.php';

$DaoPaises        = new DaoPaises();
$DaoEstados       = new DaoEstados();
$DaoLocalidades   = new DaoLocalidades();

if (!empty($_POST)) {

    /* 
    *	INFO ORGANIZACION
    *	================
    */
    $DaoOrganizacion = new DaoOrganizacion();
    $org = $DaoOrganizacion->getByUserId($_SESSION["user_id"]);
    $org->setTelefono($_POST['_telefono']);
    $org->setUrlWeb($_POST['_url']);
    $org->setUbicacion($_POST['_ubicacion']);
    $org->setMision($_POST['_mision']);
    $org->setVision($_POST['_vision']);
    $org->setQuienSoy($_POST['_quienSoy']);
    $org->setIdAlcanze($_POST['idAlcanze']);
    $org->setLat($_POST['lat']);
    $org->setLng($_POST['lng']);

    /* 
    *	INFO UBICACIÓN
    *	================
    */
    $DaoUbicaciones = new DaoUbicaciones();
    $ubi = $DaoUbicaciones->getMainByOrgId($org->id);
    if (isset($_POST['pais_id'])) {
        $ubi->setIdPais($_POST['pais_id']);
    }
    if (isset($_POST['municipio_id'])) {
        $ubi->setIdMunicipio($_POST['municipio_id']);
    }
    if (isset($_POST['localidad_id'])) {
        $ubi->setIdLocalidad($_POST['localidad_id']);
    }
    if (isset($_POST['estado_id'])) {
        $ubi->setIdEstado($_POST['estado_id']);
    }

    /*
    * UBICACIÓN
    * @description: Ubicación principal
    * ================================
    */
    if (!empty($_POST["EstateState"])) {

        // ELimina existente
        $del_ubicacion = $DaoUbicaciones->getMatrizByOrgId($org->id);
        if (count($del_ubicacion) > 0) {
            $DaoUbicaciones->delete($del_ubicacion[0]->id);
        }

        // Agrega nueva
        $Ubicaciones = new Ubicaciones();
        $Ubicaciones->setIdOrganizacion($org->id);
        $Ubicaciones->setIsMatriz(1);
        $Ubicaciones->setIsMain(1);

        if (!empty($_POST['EstateCountry']) ) {
            $pais = $DaoPaises->getByName($_POST['EstateCountry']);
            $Ubicaciones->setIdPais($pais->id);
        }
        if (!empty($_POST['EstateState']) ) {
            $estado = $DaoEstados->getByName($_POST['EstateState']);
            $Ubicaciones->setIdEstado($estado->id);
        }
        if (!empty($_POST['EstateCity']) ) {
            $ciudad = $DaoLocalidades->getByName($_POST['EstateCity']);
            $Ubicaciones->setIdLocalidad($ciudad->id);
        }

        $ubicacion_id = $DaoUbicaciones->add($Ubicaciones);    
    }

    /*
    *	UPDATE USER INFORMATION
    *	==========================
    */
    $User = new UserData();
    $User->email = $_POST['_email'];
    $User->id = $_SESSION['user_id'];

    /*
    *	UPDATE OAUTH INFORMATION
    *	===========================
    */

    
    $DaoOauth = new DaoOauth();
    $oauths = $DaoOauth->getOauthsByIdUsu($org->id);

    foreach ($oauths as $oauth) {
        if ($oauth->getServicio() == 'Facebook') {
            $oauth->setUrlRed($_POST['_facebook']);

            $DaoOauth->update($oauth);
        }
    }

    // Carga Avatar
    $image = new Upload($_FILES["image"]);
    $image->image_resize          = true;
    $image->image_ratio_crop      = true;
    $image->image_y               = 200;
    $image->image_x               = 180;

    if ($image->uploaded) {
        $image->Process("storage/users/".$_SESSION["user_id"]."/profile/");
        if ($image->processed) {
            //$profile->image = $image->file_dst_name;
            $org->setAvatar($image->file_dst_name);
        } else {
            echo "Error: ".$image->error;
        }
    }

    // Carga Portada
    $portada = new Upload($_FILES["portada"]);
    $portada->image_resize          = true;
    $portada->image_ratio_crop      = true;
    $portada->image_y               = 315;
    $portada->image_x               = 1140;

    if ($portada->uploaded) {
        $portada->Process("storage/users/".$_SESSION["user_id"]."/profile/");
        if ($portada->processed) {
            //$profile->portada = $portada->file_dst_name;
            $org->setPortada($portada->file_dst_name);
        } else {
            echo "Error: ".$portada->error;
        }
    }

    $DaoOrganizacion->update($org);
    $DaoUbicaciones->update($ubi);
    $User->update_email();
    Core::redir("./?view=editinformation");
}

?>
