<?php

$offset=0;
$limit=12;
$where = [ 'a.idCategoriaAportacion =' => 0 ];
$count = 1;
$orderby = 'p.fechaCreado DESC';

if (isset($_POST['offset']) && $_POST['offset'] > 0) {
    $offset=$_POST['offset'];
}

if (isset($_POST['method']) && $_POST['method'] == 'updatePage') {
    include_once '../../../../../core/modules/index/model/DaoAportacion.php';
    include_once '../../../../../core/modules/index/model/DaoPublicaciones.php';

    include_once '../../../../../core/autoload.php';
    include_once '../../../../../core/modules/index/model/UserData.php';
    /*
    require_once '../../../../../core/app/defines.php';
    require_once '../../../../../core/controller/Database.php';
    require_once '../../../../../core/controller/Executor.php';
    require_once '../../../../../core/controller/Model.php';
    */

    
if (isset($_POST['filterId']) && $_POST['filterId'] > 0) {
        $filterId = $_POST['filterId'];
        $extraFilterId = $_POST['extraFilterId'];

    } else {
        $filterId = 1;
    }

    switch ($filterId) {
    case 1:
        $orderby = 'p.fechaCreado DESC';
        break;

    case 2:
        $orderby = 'a.visitas DESC, a.titulo ASC';
        break;

    case 3:
        $orderby = '_calificacion DESC, a.titulo ASC';
        break;

    case 4:
        $orderby = 'p.fechaCreado DESC';

        $where = [
        'a.idCategoriaAportacion =' => 0,
        'o.Roles_idRol =' => $extraFilterId
        ];
        break;

    case 5:
        ($extraFilterId < 10) ? $month_id = '0'.$extraFilterId : $month_id = $extraFilterId;

        $first_day = date('Y').'-'.$month_id.'-01 00:00:00';
        $last_day = date('Y-'.$month_id.'-t', strtotime($first_day)).' 23:59:59';
        $orderby = 'p.fechaCreado DESC';

        $where = [
        'a.idCategoriaAportacion =' => 0,
        'p.fechaCreado >=' => '"'.$first_day.'"',
        'p.fechaCreado <=' => '"'.$last_day.'"'
        ];
        break;
    }
}

$DaoPublicaciones = new DaoPublicaciones();
$count_rows = $DaoPublicaciones->countAllBySection([1], $where);
$total_rows = $count_rows[0]['count'];

foreach ($DaoPublicaciones->getAllBySection([1], $where, $offset, $limit, $orderby) as $publicacion) {
      $fecha = new DateTime($publicacion['fechaCreado']);
      $image_path = APP_PATH.'uploads/aportaciones/'.$publicacion['thumbnail'];
?>
  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
    <md-card class="_md">
      <md-card-header>
        <?php if (strlen($publicacion['thumbnail']) > 5 && is_array(@getimagesize($image_path))) { ?>
        <img class="img-responsive" src="<?php echo $image_path; ?>" />
        <?php } else { ?>
        <div class="img-placeholder"></div>
        <?php } ?>
        <h4><?php echo $publicacion['titulo'] ?></h4>
      </md-card-header>
      <md-card-content>
        <small><?php echo $fecha->format('d/m/Y'); ?></small>
      </md-card-content>
      <div class="">
        <?php if (strlen($publicacion['file']) > 5) { ?>
        <a href="uploads/aportaciones/<?php echo $publicacion['file'] ?>" class="btn btn-block btn-primary"><?php echo translate('Descargar')?></a>
        <?php } else { ?>
        <div class="btn btn-block btn-default disabled"><?php echo translate('Descargar')?></div>
        <?php } ?>
      </div>
    </md-card>
  </div>

<?php $count++; 
} ?>

  <nav class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php
    //$count=50;
    if ($total_rows >12) {
    ?>
        <ul class="pagination">
            <?php
            if ($offset>=12) {
                ?>
                 <li><a href="#" onclick="updatePage(<?php echo $offset-12;?>)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                <?php
            }
            $numPaginas=1;
            $lastOffset = 0;
            for ($i=1; $i<=$total_rows; $i++){
                if (($i%12)==1) {
                ?>
                 <li <?php if (($i-1)==$offset) { ?> class="active" <?php } ?>><a href="#" onclick="updatePage(<?php echo $i-1;?>)"><?php echo $numPaginas?></a></li>
                <?php
                   $lastOffset = $i-1;
                   $numPaginas++;
                }
            }
            if ($offset != $lastOffset ) {
                ?>
                 <li><a href="#" onclick="updatePage(<?php echo $offset+12;?>)"aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                    <?php
            }
            ?>
        </ul>
    <?php
    }
    ?>
  </nav>
