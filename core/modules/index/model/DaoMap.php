<?php
require_once 'DTO/Base.php';

class DaoMap extends base
{
    public $response = array(
    'msg'    => '',
    'status' => true,
    'class'  => ''
    );

    public function getMarkers($data)
    {

        if ($data['categoria'] ) {  $categoria    = $data['categoria'];
        }
        if ($data['mostrar'] ) {    $mostrar      = $data['mostrar'];
        }
        if ($data['estado'] ) {     $estado       = $data['estado'];
        }

        $actores      = array();
        $expertos     = array();
        $roles        = array();
        $eventos      = array();
        $iniciativas  = array();

        /* Validate actores data */
        if ($categoria === "ACTORES") {
      
            if (empty($mostrar) ) {
                $mostrar[] = 'all';
            }
            if (!empty($mostrar) ) {
                $where = "";
                $count = 0;
                foreach ($mostrar as  $rol_id) {
                    if ($rol_id == 'all') {
                        $where = " r.idRol != '' ";
                        break;
                    } else {
                        $where .= ($count == 0) ? " r.idRol = $rol_id " : " OR r.idRol = $rol_id ";
                    }
                    $count++;
                }
                $sql = "SELECT
                  u.id  as ID,
                  r.nombre as Rol,
                  u.name as Actor,
                  u.rfc as RFC,
                  u.email as Email,
                  o.telefono as Teléfono,
                  o.mision as Misión,
                  o.vision as Visión,
                  o.quienSoy as Quien_Soy,
                  a.nombre as Alcance,
                  o.ubicacion as Ubicación,
                  b.idEstado,
                  b.is_matriz as Matriz,
                  o.lat,
                  o.lng,
                  r.imagen as RolImagen
          FROM
              organizacion o
                  INNER JOIN
              user u ON u.id = o.Usuarios_idUsuarios
                  LEFT JOIN
              roles r ON r.idRol = o.Roles_idRol
                  LEFT JOIN
              alcanze a ON o.idAlcanze = a.id
                  LEFT JOIN
              ubicaciones b ON b.idOrganizacion = o.id
        WHERE u.is_active = 1 AND u.activo = 1 AND u.is_valid = 1 AND
        b.is_matriz = 1 AND $where GROUP BY u.id";
                $actores =  $this -> getAllRows($sql);

            }
        }

        /* Validate roles data */
        if ($categoria === "ROLES") {
      
            if (empty($mostrar) ) {
                $mostrar[] = 'all';
            }
            if (!empty($mostrar) ) {
                $where = "";
                $count = 0;
                foreach ($mostrar as  $roles_id) {
                    if ($roles_id == 'all') {
                        $where = " 1 = 1 ";
                        break;
                    } else {
                        $where .= ($count == 0) ? " o.Roles_idRol = $roles_id " : " OR o.Roles_idRol = $roles_id ";
                    }
                    $count++;
                }
                $sql = "SELECT
              r.nombre as Rol,
              u.name as Actor,
              u.rfc as RFC,
              u.email as Email,
              o.telefono as Teléfono,
              o.mision as Misión,
              o.vision as Visión,
              o.quienSoy as Quien_Soy,
              a.nombre as Alcance,
              o.ubicacion as Ubicación,
              b.idEstado,
              o.lat,
              o.lng
          FROM
              organizacion o
                  INNER JOIN
              user u ON u.id = o.Usuarios_idUsuarios
                  LEFT JOIN
              roles r ON r.idRol = o.Roles_idRol
                  LEFT JOIN
              ubicaciones b ON b.idOrganizacion = o.id
        WHERE u.is_active = 1 AND u.activo = 1 AND u.is_valid = 1 AND 
        b.is_matriz = 1 AND $where GROUP BY u.id";
                $roles =  $this -> getAllRows($sql);
            }

        }

        if ($categoria === "EXPERTOS") {
      
            if (empty($mostrar) ) {
                $mostrar[] = 'all';
            }
            if (!empty($mostrar) ) {
                $where = "";
                $count = 0;
                foreach ($mostrar as  $expertos_id) {
                    if ($expertos_id == 'all') {
                        $where = " 1 = 1 ";
                        break;
                    } else {
                        $where .= ($count == 0) ? " e.idExperto = $expertos_id " : " OR e.idExperto = $expertos_id ";
                    }
                    $count++;
                }
                $sql = "SELECT
              u.*, e.*, r.nombre as rol
          FROM
              experto e
                  INNER JOIN
              user u ON u.id = e.Usuarios_idUsuarios
                  LEFT JOIN
              roles r ON r.idRol = o.Roles_idRol
        WHERE u.is_active = 1 AND u.activo = 1 AND u.is_valid = 1 AND $where GROUP BY u.id";
                $expertos =  $this -> getAllRows($sql);
            }

        }

        /* Validate eventos data */
        if ($categoria === "EVENTOS") {
      
            if (empty($mostrar) ) {
                $mostrar[] = 'all';
            }
            if (!empty($mostrar) ) {
                $where = "";
                $count = 0;
                foreach ($mostrar as  $eventos_id) {
                    if ($eventos_id == 'all') {
                        $where = " 1 = 1 ";
                        break;
                    } else {
                        $where .= ($count == 0) ? " e.tipoEvento = '$eventos_id' " : " OR e.tipoEvento = '$eventos_id' ";
                    }
                    $count++;
                }
                $sql = "SELECT
              u.*, o.*, r.nombre as rol, e.*
          FROM
              organizacion o
                  INNER JOIN
              user u ON u.id = o.Usuarios_idUsuarios
                  LEFT JOIN
              roles r ON r.idRol = o.Roles_idRol
                  LEFT JOIN
              evento e ON e.user_id = o.id
        WHERE u.is_active = 1 AND u.activo = 1 AND u.is_valid = 1 AND $where GROUP BY u.id";
                $eventos =  $this -> getAllRows($sql);
            }

        }

        /* Validate iniciativas data */
        if ($categoria === "INICIATIVAS") {
      
            if (empty($mostrar) ) {
                $mostrar[] = 'all';
            }
            if (!empty($mostrar) ) {
                $where = "";
                $count = 0;
                foreach ($mostrar as  $iniciativa_id) {
                    if ($iniciativa_id == 'all') {
                        $where = " 1 = 1 ";
                        break;
                    } else {
                        $where .= ($count == 0) ? " i.idIniciativa = '$iniciativa_id' " : " OR i.idIniciativa = '$iniciativa_id' ";
                    }
                    $count++;
                }
                $sql = "SELECT
              u.*, o.*, r.nombre as rol, p.*, i.*
          FROM
              organizacion o
                  INNER JOIN
              user u ON u.id = o.Usuarios_idUsuarios
                  LEFT JOIN
              roles r ON r.idRol = o.Roles_idRol
                  LEFT JOIN
              publicaciones p ON p.user_id = o.id
                  INNER JOIN
              iniciativa i ON p.id = i.post_id
        WHERE u.is_active = 1 AND u.activo = 1 AND u.is_valid = 1 AND $where GROUP BY u.id";
                $iniciativas =  $this -> getAllRows($sql);
            }

        }

        return array('actores' => $actores, 'roles' => $roles, 'eventos' => $eventos, 'expertos' => $expertos, 'iniciativas' => $iniciativas);
    }
}
