<?php
require_once 'DTO/Base.php';
require_once 'DTO/CalificacionAportacion.php';

class DaoCalificacionAportacion extends base
{
    
    public $tableName="calificacionaportacion"; 

    public function add(CalificacionAportacion $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (calificacion, fechaCalificacion, Aportacion_idAportacion, Usuario_id) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getCalificacion(), "int"),
              $this->GetSQLValueString($x->getFechaCalificacion(), "date"),
              $this->GetSQLValueString($x->getAportacion_idAportacion(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(CalificacionAportacion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET calificacion=%s, fechaCalificacion=%s, Aportacion_idAportacion=%s, Usuario_id=%s  WHERE idCalificacionAportacion = %s",
              $this->GetSQLValueString($x->getCalificacion(), "int"),
              $this->GetSQLValueString($x->getFechaCalificacion(), "date"),
              $this->GetSQLValueString($x->getAportacion_idAportacion(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idCalificacionAportacion=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCalificacionAportacion= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function createObject($row)
    {
        $x = new CalificacionAportacion();
        $x->setId($row['idCalificacionAportacion']);
        $x->setCalificacion($row['calificacion']);
        $x->setFechaCalificacion($row['fechaCalificacion']);
        $x->setAportacion_idAportacion($row['Aportacion_idAportacion']); 
        $x->setUsuario_id($row['Usuario_id']);
        return $x;
    }
   
            
    public function getCalificacionesByIdAportacion($Id_aportacion)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Aportacion_idAportacion=".$Id_aportacion;
         return $this->advancedQuery($query);
    }


}
