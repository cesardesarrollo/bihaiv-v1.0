<?php
require_once 'DTO/Base.php';
require_once 'DTO/Amigos.php';

class DaoAmigos extends base
{
    
    public $tableName="amigos"; 

    public function add(Amigos $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (user_id_sender, user_id_receiver, fechaAceptacion, fechaSolicitud, estatus_aceptar) VALUES (%s ,%s, %s, %s, %s)",
              $this->GetSQLValueString($x->getUser_id_sender(), "int"),
              $this->GetSQLValueString($x->getUser_id_receiver(), "int"),
              $this->GetSQLValueString($x->getFechaAceptacion(), "date"),
              $this->GetSQLValueString($x->getFechaSolicitud(), "date"),
              $this->GetSQLValueString($x->getEstatus_aceptar(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(Amigos $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET user_id_sender=%s, user_id_receiver=%s, fechaAceptacion=%s, fechaSolicitud=%s, estatus_aceptar=%s  WHERE id = %s",
              $this->GetSQLValueString($x->getUser_id_sender(), "int"),
              $this->GetSQLValueString($x->getUser_id_receiver(), "int"),
              $this->GetSQLValueString($x->getFechaAceptacion(), "date"),
              $this->GetSQLValueString($x->getFechaSolicitud(), "date"),
              $this->GetSQLValueString($x->getEstatus_aceptar(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new Amigos();
        $x->setId($row['id']);
        $x->setUser_id_sender($row['user_id_sender']);
        $x->setUser_id_receiver($row['user_id_receiver']);
        $x->setFechaAceptacion($row['fechaAceptacion']); 
        $x->setFechaSolicitud($row['fechaSolicitud']);
        $x->setEstatus_aceptar($row['estatus_aceptar']); 
        return $x;
    }
     
    public function getAmigosByIdUsu($Id_usu)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE user_id_sender=".$Id_usu;
         return $this->advancedQuery($query);
    }


}
