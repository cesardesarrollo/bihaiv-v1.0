<?php
require_once 'DTO/Base.php';
require_once 'DTO/Paises.php';

class DaoPaises extends base
{
    
    public $tableName="paises"; 

    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByName($nombre)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE nombre = '".$nombre."'";
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
            return $resp;
    }

    public function createObject($row)
    {
        $x = new Paises();
        $x->setId($row['id']);
        $x->setNombre($row['nombre']);
        $x->setPrefijo($row['prefijo']);
        return $x;
    }

}
