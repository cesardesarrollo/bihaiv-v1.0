<?php
require_once 'DTO/Base.php';

require_once 'UserData.php';
require_once 'DaoOrganizacion.php';
require_once 'DaoEnfoque.php';
require_once 'DaoPublicaciones.php';
require_once 'DaoRoles.php';
require_once 'FriendData.php';
require_once 'DaoPaises.php';
require_once 'ProfileData.php';
require_once 'DaoUbicaciones.php';

class DaoSugerencia extends base
{

    public $friendsInyect = false;
    public $sameRol       = false;

    private function clean($data)
    {
        if (is_null($data)) {
            $data = "";
        }
        return $data;
    }
    
    public function setSameRol($sameRol)
    {
        $this->sameRol = $sameRol;
    }

    public function setFriendsInyect($friendsInyect)
    {
        $this->friendsInyect = $friendsInyect;
    }

    public function getActorsByEnfoque($miUserId)
    {

        $DaoOrganizacion  = new DaoOrganizacion();
        $DaoRoles         = new DaoRoles(); 
        $UserData         = new UserData();
        $FriendData       = new FriendData(); 
        $DaoPaises        = new DaoPaises(); 
        $ProfileData      = new ProfileData(); 
        $DaoEnfoque       = new DaoEnfoque(); 
        $DaoPublicaciones = new DaoPublicaciones();
        $DaoUbicaciones   = new DaoUbicaciones();

        $miOrganizacion = $DaoOrganizacion->getByUserId($miUserId);
        $miUserData     = $UserData->getById($miUserId);
        $miUserPais     = $miUserData->id_country;
        $sugerencias    = array();

        //ACTORES POR ENFOQUES Y ROL DENTRO DE ALCANCE
        $miActorPublicacionesEnfoques = $DaoPublicaciones->getEnfoquesByOrgId($miOrganizacion->id);
        $miActorEnfoques = array();
        foreach ($miActorPublicacionesEnfoques as $publicacion) {
            $miEnfoque = $DaoEnfoque->getByPostId($publicacion->id);
            $miActorEnfoques[] = $miEnfoque->tipo_enfoque;
        }
        $miActorEnfoques = array_unique($miActorEnfoques);
        // mis enfoques
        $miActorEnfoquesLength = count($miActorEnfoques);

        // coincidencia con roles
        if ($this->sameRol) { 
            $actores = $DaoOrganizacion->getAllByRolId($miOrganizacion->Roles_idRol);
        } else {
            $actores = $DaoOrganizacion->getAllByNoRolId($miOrganizacion->Roles_idRol);
        }

        // mis alcances
        $misAlcances = $DaoUbicaciones->getActiveByOrgId($miOrganizacion->id);
        $misAlcancesArray = array();
        foreach ($misAlcances as $miAlcance) {
            if (!empty($miAlcance->idEstado)) { $misAlcancesArray[] = $miAlcance->idEstado;
            }
        }

        foreach ($actores as $actor):
            // Si no soy yo mismo
            if ($actor->Usuarios_idUsuarios !== $miUserId) :
                $fs = $FriendData->getFriendship($miUserId, $actor->Usuarios_idUsuarios);

                // Si existe ya una amistad o no
                $continua = false;
                if ($this->friendsInyect) {
                    $continua = true;
                } else if (!is_object($fs)) {
                    $continua = true;
                }

                if ($continua) :
                    $actorRol = $DaoRoles->getById($actor->Roles_idRol);
                    $actorUser = $UserData->getOnlyUsersById($actor->Usuarios_idUsuarios);

                    // Si es un actor (no admin, no experto)
                    if (is_object($actorUser)) :

                        // Alcances
                        $actorAlcances = $DaoUbicaciones->getActiveByOrgId($actor->id);
                        $actorAlcancesArray = array();
                        foreach ($actorAlcances as $actorAlcance) {
                            if (!empty($actorAlcance->idEstado)) { $actorAlcancesArray[] = $actorAlcance->idEstado;
                            }
                        }

                        // Se valida que al menos un estado del alcance de un actor coincida con el alcance del usuario propietario
                        $array_intersect = array_intersect($actorAlcancesArray, $misAlcancesArray);

                        if (count($array_intersect) > 0) :

                            //Si los enfoques coinciden en mas de un 50% 
                            $actorPublicacionesEnfoques = $DaoPublicaciones->getEnfoquesByOrgId($actor->id);
                            $actorUserEnfoques = array();
                            foreach ($actorPublicacionesEnfoques as $publicacion) {
                                $actorEnfoque = $DaoEnfoque->getByPostId($publicacion->id);
                                $actorUserEnfoques[] = $actorEnfoque->tipo_enfoque;
                            }
                            $actorUserEnfoques = array_unique($actorUserEnfoques);
                            $actorEnfoquesRelacionLength = count(array_intersect($miActorEnfoques, $actorUserEnfoques));
                            if (($miActorEnfoquesLength/2) <= $actorEnfoquesRelacionLength) :
                                $actorProfile = $ProfileData->getByUserId($actor->Usuarios_idUsuarios);
                                $actorPais = $DaoPaises->getById($actorUser->id_country);
                                $profile_img = ($actor->avatar !== 'assets/img/home/perfil.png') ? "storage/users/$actorUser->id/profile/$actor->avatar" : $actor->avatar;
                                $actorsByEnfoque = array("organizacion_id"    => $this->clean($actor->id),
                                            "organizacion_idRol" => $this->clean($actor->Roles_idRol),
                                            "roles_nombre"       => $this->clean($actorRol->nombre),
                                            "user_id"            => $this->clean($actorUser->id),
                                            "user_name"          => $this->clean($actorUser->getFullname()),
                                            "user_idCountry"     => $this->clean($actorUser->id_country),
                                            "profile_image"      => $this->clean($profile_img),
                                            "paises_nombre"      => $this->clean($actorPais->nombre),
                                            "tipo"               => "Sugerimos te relaciones con este actor por compatibilizar en enfoques",
                                            "tipo_img"           => "share.png"
                                            );
                                array_push($sugerencias, $actorsByEnfoque);

                            endif;
                        endif;
                    endif;
                endif;
            endif;
        endforeach;

        return $sugerencias;
    }

    public function getActorsByRol($miUserId)
    {

        $DaoOrganizacion  = new DaoOrganizacion();
        $DaoRoles         = new DaoRoles(); 
        $UserData         = new UserData();
        $FriendData       = new FriendData(); 
        $DaoPaises        = new DaoPaises(); 
        $ProfileData      = new ProfileData(); 
        $DaoEnfoque       = new DaoEnfoque(); 
        $DaoPublicaciones = new DaoPublicaciones();

        $miOrganizacion = $DaoOrganizacion->getByUserId($miUserId);
        $miUserData =  $UserData->getById($miUserId);
        $miUserPais =  $miUserData->id_country;
        $sugerencias = array();

        //ACTORES DEL MISMO ROL
        $actores = $DaoOrganizacion->getAllByRolId($miOrganizacion->Roles_idRol);
        foreach ($actores as $actor):
            // Si no soy yo mismo
            if ($actor->Usuarios_idUsuarios !== $miUserId) :
                $fs = $FriendData->getFriendship($miUserId, $actor->Usuarios_idUsuarios);

                // Si existe ya una amistad o no
                $continua = false;
                if ($this->friendsInyect) {
                    $continua = true;
                } else if (!is_object($fs)) {
                    $continua = true;
                }

                if ($continua) :

                    $actorRol = $DaoRoles->getById($actor->Roles_idRol);
                    $actorUser = $UserData->getOnlyUsersById($actor->Usuarios_idUsuarios);
                    // Si es un actor (no admin, no experto)
                    if (is_object($actorUser)) :
                        //Validar que usuario no sea admin ni experto
                        $actorProfile = $ProfileData->getByUserId($actor->Usuarios_idUsuarios);
                        $actorPais = $DaoPaises->getById($actorUser->id_country);
                        $profile_img = ($actor->avatar !== 'assets/img/home/perfil.png') ? "storage/users/$actorUser->id/profile/$actor->avatar" : $actor->avatar;
                        $actorsByRol = array("organizacion_id"    => $this->clean($actor->id),
                                     "organizacion_idRol" => $this->clean($actor->Roles_idRol),
                                     "roles_nombre"       => $this->clean($actorRol->nombre),
                                     "user_id"            => $this->clean($actorUser->id),
                                     "user_name"          => $this->clean($actorUser->getFullname()),
                                     "user_idCountry"     => $this->clean($actorUser->id_country),
                                     "profile_image"      => $this->clean($profile_img),
                                     "paises_nombre"      => $this->clean($actorPais->nombre),
                                     "tipo"               => "Sugerimos te relaciones con este actor por compatibilizar en roles",
                                     "tipo_img"           => "share.png"
                                     );
                        array_push($sugerencias, $actorsByRol);
                    endif;
                endif;
            endif;
        endforeach;

        return $sugerencias;
    }

    public function getFriendsOfFriend($miUserId)
    {

        $DaoOrganizacion  = new DaoOrganizacion();
        $DaoRoles         = new DaoRoles(); 
        $UserData         = new UserData();
        $FriendData       = new FriendData(); 
        $DaoPaises        = new DaoPaises(); 
        $ProfileData      = new ProfileData(); 
        $DaoEnfoque       = new DaoEnfoque(); 
        $DaoPublicaciones = new DaoPublicaciones();

        $miOrganizacion = $DaoOrganizacion->getByUserId($miUserId);
        $miUserData =  $UserData->getById($miUserId);
        $miUserPais =  $miUserData->id_country;
        $sugerencias = array();

        // AMIGOS DE AMIGOS (fofs)
        $friends = $FriendData->getFriends($miUserId);
        foreach ($friends as $friend) {
            $friendId = ($friend->receptor_id !== $miUserId) ? $friend->receptor_id : $friend->sender_id;
            $fofs = $FriendData->getFriends($friendId);
            foreach ($fofs as $fof):
                $fofriendId = ($fof->receptor_id !== $friendId) ? $fof->receptor_id : $fof->sender_id;
                // Si no soy yo mismo
                if ($fofriendId !== $miUserId) :
                    $fs = $FriendData->getFriendship($miUserId, $fofriendId);

                    // Si existe ya una amistad o no
                    $continua = false;
                    if ($this->friendsInyect) {
                        $continua = true;
                    } else if (!is_object($fs)) {
                        $continua = true;
                    }
                
                    if ($continua) :

                        $fofOrganizacion = $DaoOrganizacion->getByUserId($fofriendId);
                        $fofRol = $DaoRoles->getById($fofOrganizacion->Roles_idRol);
                        $fofUser = $UserData->getOnlyUsersById($fofriendId);
                        // Si es un actor (no admin, no experto)
                        if (is_object($fofUser)) :
                            $fofPais = $DaoPaises->getById($fofUser->id_country);
                            $profile_img = ($fofOrganizacion->avatar !== 'assets/img/home/perfil.png') ? "storage/users/$fofUser->id/profile/$fofOrganizacion->avatar" : $fofOrganizacion->avatar;
                            $friendsOfFriend = array("organizacion_id"    => $this->clean($fofOrganizacion->id),
                                           "organizacion_idRol" => $this->clean($fofOrganizacion->Roles_idRol),
                                           "roles_nombre"       => $this->clean($fofRol->nombre),
                                           "user_id"            => $this->clean($fofUser->id),
                                           "user_name"          => $this->clean($fofUser->getFullname()),
                                           "user_idCountry"     => $this->clean($fofUser->id_country),
                                           "profile_image"      => $this->clean($profile_img),
                                           "paises_nombre"      => $this->clean($fofPais->nombre),
                                           "tipo"               => "Sugerimos te relaciones con este actor por ser amigo de un amigo tuyo",
                                           "tipo_img"           => "url.png"
                                           );
                            array_push($sugerencias, $friendsOfFriend);
                        endif;
                    endif;
                endif;
            endforeach;

            return $sugerencias;
        }

    }

}
