<?php
require_once 'DTO/Base.php';
require_once 'DTO/Municipios.php';

class DaoMunicipios extends base
{
    
    public $tableName="municipios"; 

    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByEstadoId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE estado_id= ".$Id;
        return $this->advancedQueryByObjetc($query);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
            return $resp;
    }

    public function createObject($row)
    {
        $x = new Municipios();
        $x->setId($row['id']);
        $x->setNombre($row['nombre']);
        $x->setClave($row['clave']);
        $x->setActivo($row['activo']);
        $x->setEstadoId($row['estado_id']);
        return $x;
    }

}
