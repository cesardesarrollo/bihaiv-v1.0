<?php
require_once 'DTO/Base.php';
require_once 'DTO/Discusion.php';
class DaoDiscusion extends base
{
    public $tableName="tema";
    public function add(Discusion $x)
    { 
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (titulo, descripcion, created_at, updated_at, privacidad, promedio, roles, organizacion_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getCreated_at(), "date"),
              $this->GetSQLValueString($x->getUpdated_at(), "date"),
              $this->GetSQLValueString($x->getPrivacidad(), "text"),
              $this->GetSQLValueString($x->getPromedio(), "int"),
              $this->GetSQLValueString($x->getRoles(), "text"),
              $this->GetSQLValueString($x->getUser_id(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }
    public function addVisit($data)
    {
        $query = "UPDATE TEMA
       SET visitors_count = visitors_count + 1
      WHERE id = ".$data['id'];
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            return $Result1;
        }
    }
    public function update(Discusion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET titulo=%s, descripcion=%s, created_at=%s, updated_at=%s, privacidad=%s, promedio=%s, organizacion_id=%s, roles=%s  WHERE id = %s",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getCreated_at(), "date"),
              $this->GetSQLValueString($x->getUpdated_at(), "date"),
              $this->GetSQLValueString($x->getPrivacidad(), "text"),
              $this->GetSQLValueString($x->getPromedio(), "int"),
              $this->GetSQLValueString($x->getUser_id(), "int"),
              $this->GetSQLValueString($x->getRoles(), "text"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }
    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idDiscusion=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getCommentsByTopic($idTema)
    {

        $query = "SELECT
            tc.*,
            o.Usuarios_idUsuarios AS usuarios_id,
            u.name,
            u.email,
            CONCAT('storage/users/',
                    o.Usuarios_idUsuarios,
                    '/profile/',
                    o.avatar) AS avatar
        FROM
            tema_comentario tc
                INNER JOIN
            organizacion o ON tc.organizacion_id = o.id
                INNER JOIN
            user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            tc.tema_id = $idTema
      ORDER BY created_at DESC";


        $comentarios = $this->advancedQuery($query);

        foreach ($comentarios as $k => $v) {
            $comentarios[$k]['name'] = utf8_decode($v['name']);
            $query = "SELECT
              COUNT(tl.tema_comentario_id) AS likes
          FROM
              tema_like tl
          WHERE
              tl.tema_comentario_id = ".$v['id']."
          GROUP BY tl.tema_comentario_id
          ORDER BY tema_comentario_id DESC";

            $likes = $this->advancedQuery($query);
            $comentarios[$k]['likes'] = (isset($likes[0])) ? $likes[0]['likes'] : 0;

        }
        return $comentarios;

    }

    public function getOnlyCommentsByTopicOld($idTema)
    {
        $query = "SELECT
            tc.*,
            o.Usuarios_idUsuarios AS usuarios_id,
            u.name,
            u.email,
            CONCAT('storage/users/',
                    o.Usuarios_idUsuarios,
                    '/profile/',
                    o.avatar) AS avatar,
            COUNT(tl.tema_comentario_id) AS likes
        FROM
            tema_comentario tc
                INNER JOIN
            organizacion o ON tc.organizacion_id = o.id
                INNER JOIN
            user u ON u.id = o.Usuarios_idUsuarios
                LEFT JOIN
            tema_like tl ON tl.tema_comentario_id = tc.id
        WHERE
            tc.tema_id = $idTema
        GROUP BY tl.tema_comentario_id
      ORDER BY created_at ASC";
        $comentarios = $this->advancedQuery($query);
        foreach ($comentarios as $k => $v) {
            $comentarios[$k]['name'] = utf8_decode($v['name']);
        }
        return $comentarios;
    }
    public function getOnlyCommentsByPlusLikes($idTema)
    {
        $query = "SELECT
          tc.*,
          o.Usuarios_idUsuarios AS usuarios_id,
          u.name,
          u.email,
          CONCAT('storage/users/',
                  o.Usuarios_idUsuarios,
                  '/profile/',
                  o.avatar) AS avatar,
          COUNT(tl.tema_comentario_id) AS likes
      FROM
          tema_comentario tc
              INNER JOIN
          organizacion o ON tc.organizacion_id = o.id
              INNER JOIN
          user u ON u.id = o.Usuarios_idUsuarios
              LEFT JOIN
          tema_like tl ON tl.tema_comentario_id = tc.id
      WHERE
          tc.tema_id = $idTema
      GROUP BY tl.tema_comentario_id
      ORDER BY likes DESC";
        $comentarios = $this->advancedQuery($query);
        foreach ($comentarios as $k => $v) {
            $comentarios[$k]['name'] = utf8_decode($v['name']);
        }
        return $comentarios;
    }
    public function getOnlyCommentsByMinusLikes($idTema)
    {
        $query = "SELECT
          tc.*,
          o.Usuarios_idUsuarios AS usuarios_id,
          u.name,
          u.email,
          CONCAT('storage/users/',
                  o.Usuarios_idUsuarios,
                  '/profile/',
                  o.avatar) AS avatar,
          COUNT(tl.tema_comentario_id) AS likes
      FROM
          tema_comentario tc
              INNER JOIN
          organizacion o ON tc.organizacion_id = o.id
              INNER JOIN
          user u ON u.id = o.Usuarios_idUsuarios
              LEFT JOIN
          tema_like tl ON tl.tema_comentario_id = tc.id
      WHERE
          tc.tema_id = $idTema
      GROUP BY tl.tema_comentario_id
      ORDER BY likes ASC";
        $comentarios = $this->advancedQuery($query);
        foreach ($comentarios as $k => $v) {
            $comentarios[$k]['name'] = utf8_decode($v['name']);
        }
        return $comentarios;
    }
    public function getCommentsById($comment_id = null)
    {
        $query = "SELECT
            tc.*,
            o.Usuarios_idUsuarios AS usuarios_id,
            u.name,
            u.email,
            CONCAT('storage/users/',
                    o.Usuarios_idUsuarios,
                    '/profile/',
                    o.avatar) AS avatar,
            t.titulo
        FROM
            tema_comentario tc
                INNER JOIN
            organizacion o ON tc.organizacion_id = o.id
                INNER JOIN
            tema t ON tc.tema_id = t.id
                INNER JOIN
            user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            tc.id = $comment_id";
        $comentarios = $this -> advancedQuery($query);
        return $comentarios;
    }
    public function getAll()
    {
        $query="SELECT
              t.*, IFNULL(ROUND(AVG(tc.calificacion)), 0)  as calificacion
          FROM
              tema t
                  LEFT JOIN
              tema_calificacion tc ON tc.tema_id = t.id
          GROUP BY t.id
          ORDER BY t.created_at DESC";
        $resultSet = $this->advancedQuery($query);
        return $resultSet;
    }
    public function getAllMostVisited()
    {
        $query="SELECT
              t.*, IFNULL(ROUND(AVG(tc.calificacion)), 0)  as calificacion
          FROM
              tema t
                  LEFT JOIN
              tema_calificacion tc ON tc.tema_id = t.id
          GROUP BY t.id
          ORDER BY t.visitors_count DESC";
        $resultSet = $this->advancedQuery($query);
        return $resultSet;
    }
    public function getAllMostVoted()
    {
        $query="SELECT
              t.*, IFNULL(ROUND(AVG(tc.calificacion)), 0)  as calificacion
          FROM
              tema t
                  LEFT JOIN
              tema_calificacion tc ON tc.tema_id = t.id
          GROUP BY t.id
          ORDER BY calificacion DESC";
        $resultSet = $this->advancedQuery($query);
        return $resultSet;
    }
    public function getTopicSearch($keywords = '', $organizacion_id = 0)
    {
        
        if ($organizacion_id === "") {
            // Público
            $query="SELECT
                t.*,
                IFNULL(ROUND(AVG(tc.calificacion)), 0) AS calificacion,
                tp.organizacion_id as organizacion_participa
            FROM

                tema t
                    LEFT JOIN
                tema_calificacion tc ON tc.tema_id = t.id
                    LEFT JOIN
                tema_participa tp ON tp.tema_id = t.id
            WHERE
                (
                    (
                        (t.titulo LIKE '%$keywords%' OR t.descripcion LIKE '%$keywords%')
                        AND
                        (t.privacidad = 'PUBLICO')
                    )
                )
            GROUP BY t.id
            ORDER BY t.created_at DESC";

        } else {
            // Privado
            $query="SELECT
                t.*,
                IFNULL(ROUND(AVG(tc.calificacion)), 0) AS calificacion,
                tp.organizacion_id as organizacion_participa
            FROM

                tema t
                    LEFT JOIN
                tema_calificacion tc ON tc.tema_id = t.id
                    LEFT JOIN
                tema_participa tp ON tp.tema_id = t.id
            WHERE
                (
                    (
                        (t.titulo LIKE '%$keywords%' OR t.descripcion LIKE '%$keywords%')
                        AND
                        (t.privacidad = 'PUBLICO')
                    )
                    OR
                    (
                        (t.titulo LIKE '%$keywords%' OR t.descripcion LIKE '%$keywords%')
                        AND
                        (t.privacidad = 'PRIVADO' AND tp.organizacion_id = $organizacion_id)
                    )
                )
            GROUP BY t.id
            ORDER BY t.created_at DESC";

        }

        $resultSet = $this->advancedQuery($query);
        return $resultSet;
    }
    public function getMyTopics($organizacion_id = null)
    {
        $query="SELECT
              t.*, IFNULL(ROUND(AVG(tc.calificacion)), 0) AS calificacion
          FROM
              tema t
                  LEFT JOIN
              tema_calificacion tc ON tc.tema_id = t.id
          WHERE
              t.organizacion_id = $organizacion_id
          GROUP BY t.id
        ORDER BY t.created_at DESC";
        $resultSet = $this->advancedQuery($query);
        return $resultSet;
    }
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
        return $this->advancedQuery($query);
    }
    public function getVotedAverage($id = 0)
    {
        $query ="SELECT
              IFNULL(ROUND(AVG(calificacion)), 0)  as calificacion
          FROM
              tema_calificacion
          WHERE
        tema_id = $id";
        $result = $this -> getOneRow($query);
        return $result['calificacion'];
    }
    public function getByUserId($Id)
    {
        $sql="SELECT * FROM ".$this->tableName." WHERE organizacion_id= ".$Id;
        $query = Executor::doit($sql);
        return Model::many($query[0], new Discusion());
    }

    
    // Revisa si el usuario ha creado
    public function IsCreatedByMe($tema_id, $organizacion_id)
    {
        $query="SELECT COUNT(*) as count FROM ".$this->tableName." WHERE organizacion_id = $organizacion_id AND id = $tema_id";
        $resultSet = $this->advancedQuery($query);
        return count($resultSet);
    }


    // Revisa si existe una invitación en un tema
    public function IsInvitedByTemaAndOrgId($tema_id, $organizacion_id)
    {
        $query="SELECT
                    tp.*
                FROM
                    tema_participa tp
                WHERE 
                    tp.tema_id = $tema_id
                AND 
                    tp.organizacion_id = $organizacion_id";
        $resultSet = $this->advancedQuery($query);
        return count($resultSet);
    }

    public function getInvitadosByTemaId($Id)
    {
        $query="SELECT organizacion_id FROM tema_participa WHERE tema_id = ".$Id;
        return $this->advancedQuery($query);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }
    public function createObject($row)
    {
        $x = new Discusion();
        $x->setId($row['id']);
        $x->setTitulo($row['titulo']);
        $x->setDescripcion($row['descripcion']);
        $x->setCreated_at($row['created_at']);
        $x->setUpdated_at($row['updated_at']);
        // bug
        /**/
        $x->setPrivacidad($row['privacidad']);
        $x->setPromedio($row['promedio']);
        $x->setUser_id($row['organizacion_id']);
        $x->setRoles($row['roles']);
        return $x;
    }
    public function getOneRow($query)
    {
        if ($results = @$this->_cnn->query($query) ) {
            if ($fields = $results -> fetch_assoc() ) {
                $results -> close();
                return $fields;
            }
            return false;
        }
    }
}
