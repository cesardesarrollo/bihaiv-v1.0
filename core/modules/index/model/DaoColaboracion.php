<?php
require_once 'DTO/Base.php';
require_once 'DTO/Colaboracion.php';

class DaoColaboracion extends base
{

    public $tableName="colaboracion";

    public function add(Colaboracion $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (fechaCreado, estatus, Iniciativa_idIniciativa, user_id, evento_id) VALUES (%s ,%s, %s, %s, %s)",
              $this->GetSQLValueString($x->getFechaCreado(), "date"),
              $this->GetSQLValueString($x->getEstatus(), "text"),
              $this->GetSQLValueString($x->getIniciativa_idIniciativa(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getEventoId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Colaboracion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET fechaCreado=%s, estatus=%s, Iniciativa_idIniciativa=%s, user_id=%s, evento_id=%s  WHERE idColaboracion = %s",
              $this->GetSQLValueString($x->getFechaCreado(), "date"),
              $this->GetSQLValueString($x->getEstatus(), "text"),
              $this->GetSQLValueString($x->getIniciativa_idIniciativa(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getEventoId(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idColaboracion=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function deleteByEventId($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE evento_id = ".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idColaboracion= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getColaboradoresByIdIniciativa($Id_ini)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Iniciativa_idIniciativa = $Id_ini";
         return $this->advancedQuery($query);
    }

    public function getTrueColaboradoresByIdIniciativa($Id_ini)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE estatus = '1' AND Iniciativa_idIniciativa = $Id_ini";
         return $this->advancedQuery($query);
    }
    
    public function getTrueColaboradoresByIdEvento($id)
    {
        $query="SELECT * FROM  ".$this->tableName." WHERE estatus = '1' AND evento_id = $id";
        //$query="SELECT * FROM colaboracion  WHERE estatus = '1' AND evento_id = $id";
         return $this->advancedQuery($query);
    }

    public function getColaboradoresByIdEvento($id)
    {
        //$query="SELECT * FROM  ".$this->tableName." WHERE  evento_id = $id";
        $query="select u.id as userid, o.id as orgid, c.evento_id, u.name  as nombreusuario from user as u INNER JOIN organizacion as o inner join ".
        " colaboracion as c on u.id=o.Usuarios_idUsuarios and  o.id=c.user_id where c.evento_id=". $id;
        return $this->advancedQuery($query);
    }

    public function getByIdIniciativaUserId($id_ini,$user_id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Iniciativa_idIniciativa=".$id_ini." AND user_id = ".$user_id;
         return $this->advancedQuery($query);
    }

    public function getByIdEventoUserId($id_eve,$user_id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE evento_id=".$id_eve." AND user_id = ".$user_id;
         return $this->advancedQuery($query);
    }

    public function getByIdEvento($id_evento)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE evento_id=".$id_evento;
         return $this->advancedQuery($query);
    }

    public function getTrueByIdEvento($id_evento)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE estatus = '1' AND  evento_id=".$id_evento;
         return $this->advancedQuery($query);
    }

    public function getByUserId($id)
    {
        $query = "select * from colaboracion where estatus = '1' and user_id=" .$id;
        return $this->advancedQueryByObjetc($query);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }

    public function createObject($row)
    {
        $x = new Colaboracion();
        $x->setId($row['idColaboracion']);
        $x->setFechaCreado($row['fechaCreado']);
        $x->setEstatus($row['estatus']);
        $x->setIniciativa_idIniciativa($row['Iniciativa_idIniciativa']);
        $x->setUsuario_id($row['user_id']);
        $x->setEventoId($row['evento_id']);
        return $x;
    }

}
