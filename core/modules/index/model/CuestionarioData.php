<?php
/**
* @author cmagana
* @class CuestionarioData
* @brief Modelo de base de datos para la tabla de cuestionario
**/

class CuestionarioData
{
    public static $tablename = "cuestionario";

    public function CuestionarioData()
    {
        $this->nombre = "";
        $this->idCuestionario = "";
        $this->fechaCreacion = "";
        $this->activo = "";
        $this->nombre = "";
    }

    public function getCuestionario($nivel)
    {
        $sql = "select idCuestionario, activo from ".self::$tablename." where activo = 1 and nivel = ".$nivel." ";
        $query = Executor::doit($sql);
        return Model::one($query[0], new CuestionarioData());
    }

    public static function delete($id)
    {
        $sql = "delete from ".self::$tablename." where id=$id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from ".self::$tablename." where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new CuestionarioData());
    }

    public static function getAll()
    {
        $sql = "select * from ".self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new CuestionarioData());

    }

}

?>
