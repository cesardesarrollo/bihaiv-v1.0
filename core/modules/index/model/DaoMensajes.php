<?php
require_once 'DTO/Base.php';
require_once 'DTO/Mensaje.php';

class DaoMensajes extends base
{

    public $tableName="mensaje";

    public function add(Mensaje $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (texto, fecha_creado, fecha_leido, user_id_sender, user_id_receiver) VALUES (%s ,%s, %s, %s, %s)",
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getFecha_leido(), "date"),
              $this->GetSQLValueString($x->getUser_id_sender(), "int"),
              $this->GetSQLValueString($x->getUser_id_receiver(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }


    public function update(Mensaje $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET texto=%s, fecha_creado=%s, fecha_leido=%s, user_id_sender=%s, user_id_receiver=%s  WHERE idMensaje = %s",
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getFecha_leido(), "date"),
              $this->GetSQLValueString($x->getUser_id_sender(), "int"),
              $this->GetSQLValueString($x->getUser_id_receiver(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new Mensaje();
        $x->setId($row['idMensaje']);
        $x->setTitulo($row['texto']);
        $x->setTexto($row['fecha_creado']);
        $x->setDateRead($row['fecha_leido']);
        $x->setDateCreated($row['user_id_sender']);
        $x->setUser_id($row['user_id_receiver']);
        return $x;
    }



    public function getMensajesByIdUser($Id_usu)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE user_id_sender=".$Id_usu;
         return $this->advancedQuery($query);
    }


}
