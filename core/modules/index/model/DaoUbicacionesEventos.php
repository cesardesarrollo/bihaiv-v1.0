<?php
require_once 'DTO/Base.php';
require_once 'DTO/UbicacionesEventos.php';

class DaoUbicacionesEventos extends base
{
    
    public $tableName="ubicaciones_eventos"; 

    public function add(UbicacionesEventos $x)
    {

        $query=sprintf(
            "INSERT INTO ".$this->tableName." (idPais, idEstado, idMunicipio, idLocalidad, idEvento) VALUES (%s ,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getIdPais(), "int"),
            $this->GetSQLValueString($x->getIdEstado(), "int"),
            $this->GetSQLValueString($x->getIdMunicipio(), "int"),
            $this->GetSQLValueString($x->getIdLocalidad(), "int"),
            $this->GetSQLValueString($x->getIdEvento(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }

    }

    public function update(UbicacionesEventos $x)
    {
        $query=sprintf(
            "UPDATE ".$this->tableName." SET idPais=%s, idEstado=%s, idMunicipio=%s, idLocalidad=%s, idEvento=%s  WHERE id = %s",
            $this->GetSQLValueString($x->getIdPais(), "int"),
            $this->GetSQLValueString($x->getIdEstado(), "int"),
            $this->GetSQLValueString($x->getIdMunicipio(), "int"),
            $this->GetSQLValueString($x->getIdLocalidad(), "int"),
            $this->GetSQLValueString($x->getIdEvento(), "int"),
            $this->GetSQLValueString($x->getId(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
                return $x->getId();
        }
    }

    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByEventId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idEvento = ".$Id;
        return $this->advancedQueryByObjetc($query);
    }    
    
    public function getPaisIdByEventId($Id)
    {
        $query="SELECT pais_id FROM ".$this->tableName." WHERE idEvento = ".$Id." LIMIT 0,1";
        return $this->advancedQueryByObjetc($query);
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
        return $resp;
    }

    public function createObject($row)
    {
        $x = new UbicacionesEventos();
        $x->setId($row['id']);
        $x->setIdPais($row['idPais']);
        $x->setIdEstado($row['idEstado']);
        $x->setIdMunicipio($row['idMunicipio']);
        $x->setIdLocalidad($row['idLocalidad']);
        $x->setIdEvento($row['idEvento']);
        return $x;
    }

    // Obtiene alcances definidos en tabla alcanze
    public function getAlcanzes()
    {
        $query="SELECT * FROM alcanze";
        return $this->advancedQuery($query);
    }
}
