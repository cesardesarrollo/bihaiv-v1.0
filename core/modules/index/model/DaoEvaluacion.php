<?php
require_once 'DTO/Base.php';
require_once 'DTO/Evaluacion.php';

class DaoEvaluacion extends base
{
    
    public $tableName="evaluacion"; 

    public function add(Evaluacion $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (idCuestionariosUsuario, Usuarios_idUsuarios, fechaEvaluacion, respuesta, Comentarios, Confidencialidad) VALUES (%s ,%s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getIdCuestionariosUsuario(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getFechaEvaluacion(), "date"),
              $this->GetSQLValueString($x->getRespuesta(), "int"),
              $this->GetSQLValueString($x->getComentarios(), "text"),
              $this->GetSQLValueString($x->getConfidencialidad(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }

    public function update(Evaluacion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET idCuestionariosUsuario=%s, Usuarios_idUsuarios=%s, fechaEvaluacion=%s, respuesta=%s, Comentarios=%s, Confidencialidad=%s  WHERE idEvaluacion = %s",
              $this->GetSQLValueString($x->getIdCuestionariosUsuario(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getFechaEvaluacion(), "date"),
              $this->GetSQLValueString($x->getRespuesta(), "int"),
              $this->GetSQLValueString($x->getComentarios(), "text"),
              $this->GetSQLValueString($x->getConfidencialidad(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idEvaluacion=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function ignore($Id)
    {
        $query = sprintf("update evaluacion set respuesta='3' where idEvaluacion=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al ignorar evaluacion: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idEvaluacion= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al obtener: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByUserId($user_id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Usuarios_idUsuarios= ".$user_id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al obtener: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByIdCuestionariosUsuarioIdUsuarios($idCuestionariosUsuario,$user_id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Usuarios_idUsuarios= ".$user_id." AND idCuestionariosUsuario = ".$idCuestionariosUsuario;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al obtener: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getdatabyOpcion($id)
    {
        $sql="select e.idEvaluacion, e.idCuestionariosUsuario, e.Usuarios_idUsuarios, e.respuesta,  e.fechaEvaluacion, DATEDIFF( now(), e.fechaEvaluacion ) as diasEvaluacion, c.estatus, c.fechaEstatus, cues.user_id from evaluacion as e INNER JOIN cuestionariosusuario as cues INNER JOIN
        estatuscuestionario as c on e.idCuestionariosUsuario=cues.idCuestionariosUsuario and e.idCuestionariosUsuario=c.idCuestionariosUsuario
        where  e.respuesta=0 and   c.estatus=".$id." order by  estatus, diasEvaluacion;";
        return self::getAllRows($sql);
    } 
    public function getUsuariosByIdCuestionarioUsuarioC($idCuestionariosUsuario)
    {

        $query="SELECT * FROM ".$this->tableName." WHERE idCuestionariosUsuario=$idCuestionariosUsuario";
        $resp= self::getAllRows($query);
        $retorno='';
        if (!empty($resp)) {
            foreach ($resp as $value) {
                $retorno.=$value['Usuarios_idUsuarios'].',';
            }
            $retorno= substr_replace($retorno, '', strlen($retorno)-1, strlen($retorno));
        }
        return $retorno;
    }

    public function getCountByIdCuestionariosUsuarioTipoRelRespuesta($idCuestionariosUsuario,$tipoRel,$respuesta)
    {

        // 0 No Contestaron
        if ($respuesta == 0) {
            $respuesta_qry = " AND t.respuesta = 0 ";
        }// 1 Contestaron 'si'
        else if ($respuesta == 1) {
            $respuesta_qry = " AND t.respuesta = 1 ";
        }// 2 Contestaron 'no'
        else if ($respuesta == 2) {
            $respuesta_qry = " AND t.respuesta = 2 ";
        }// 3 Contestaron 
        else if ($respuesta == 3) {
            $respuesta_qry = " AND t.respuesta > 0 ";
        }// 4 Todos
        else {
            $respuesta_qry = "";
        }

        $query = "SELECT * FROM " . $this->tableName . " AS t INNER JOIN user AS u ON u.id  = t.Usuarios_idUsuarios WHERE t.idCuestionariosUsuario = " . $idCuestionariosUsuario . " AND u.tipoRel = ". $tipoRel . $respuesta_qry;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $Result1->num_rows;
        }
    }

    public function getByIdCuestionarioUsuario($idCuestionariosUsuario)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCuestionariosUsuario=$idCuestionariosUsuario";
        return $this->advancedQueryByObjetc($query);
    }

    public function getUsuariosByIdCuestionarioUsuario($idCuestionariosUsuario)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCuestionariosUsuario=$idCuestionariosUsuario";
        return $this->advancedQueryByObjetc($query);
    }


    public function getWaitingByIdCuestionariosUsuario($idCuestionariosUsuario)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE respuesta = 0 AND idCuestionariosUsuario= $idCuestionariosUsuario";
        return $this->advancedQueryByObjetc($query);
    }

    public function createObject($row)
    {
        $x = new Evaluacion();
        $x->setId($row['idEvaluacion']);
        $x->setIdCuestionariosUsuario($row['idCuestionariosUsuario']);
        $x->setUsuario_id($row['Usuarios_idUsuarios']);
        $x->setFechaEvaluacion($row['fechaEvaluacion']); 
        $x->setRespuesta($row['respuesta']);
        $x->setComentarios($row['Comentarios']); 
        $x->setConfidencialidad($row['confidencialidad']);
        return $x;
    }
    
    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
        return $resp;
    }


}
