<?php
require_once 'DTO/Base.php';
require_once 'DTO/Enfoque.php';

class DaoEnfoque extends base
{
    public $tableName="enfoque";

    public function add(Enfoque $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (texto, post_id, tipo_enfoque_id) VALUES (%s ,%s ,%s)",
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getPost_idPost(), "int"),
              $this->GetSQLValueString($x->getTipoEnfoque(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }


    public function update(Enfoque $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET texto=%s, post_id=%s, tipo_enfoque_id=%s  WHERE idEnfoque = %s",
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getPost_idPost(), "int"),
              $this->GetSQLValueString($x->getTipoEnfoque(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idEnfoque=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getAll($id)
    {
        $query="SELECT * FROM ".$this->tableName . " WHERE Usuario = ". $id;
        return $this->advancedQuery($query);
    }

    public function getpromedio($sql)
    {
        // $query="SELECT * FROM ".$this->tableName . " WHERE Usuario = ". $id;
        return $this->advancedQuery($sql);
    }


    public function getByPostId($Id)
    {
        $query = "SELECT * FROM ".$this -> tableName." WHERE post_id= ".$Id;
        $Result1= $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al obtener: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function getById($Id)
    {
        $query = "SELECT * FROM ".$this -> tableName." WHERE idEnfoque= ".$Id;
        $Result1= $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al obtener: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function updateRating($data)
    {
        $query = "INSERT INTO `calificacionenfoque` (`calificacion`, `enfoque_idEnfoque`, `organizacion_id`) VALUES (".$data['qualification'].", ".$data['id'].", ".$data['organizacion_id'].") ON DUPLICATE KEY UPDATE calificacion = ".$data['qualification'];
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            /* Validamos que nuestra publicacion cumpla con los requisitos para
            * aparecer en nuestra base datos publica, en caso de que se cumplan los
            * requisitos se actualiza la bandera en la tabla publicaciones.
            */
            return $Result1;
        }
    }

    // HERE COMES NEW CHALLENGER!!!
    public function getAllTipoEnfoque()
    {
        $resultSet =  $this -> getAllRows('SELECT * FROM tipo_enfoque');
        if (!empty($resultSet)) {
            foreach ($resultSet as $k => $v) {
                $resultSet[$k]['acciones'] = '<a href="form_tipo_enfoque.php?id='.$v['id'].'" class="btn btn-default" >Editar</a>';
                $resultSet[$k]['acciones'] .= '&nbsp;';
                $resultSet[$k]['acciones'] .= '&nbsp;';
                $resultSet[$k]['acciones'] .= '<a href="#" class="btn btn-default delete-tipo" data-id="'.$v['id'].'">Eliminar</a>';
            }
        }
        return $resultSet;
    }
    public function getListTipoEnfoque()
    {
        if ($resultSet =  $this -> getAllRows('SELECT * FROM tipo_enfoque')) {
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Tipos de enfoque recuperados con éxito';
            $this -> response['data']   = $resultSet;
        } else {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Ocurrio un error inesperado';
        }
    }


    public function saveTipoEnfoque($data)
    {
        if (empty($data)) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'danger';
            $this -> response['msg']    = 'Ocurrio un error inesperado';
            return false;
        }
        if (empty($data['nombre'])) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'El nombre del tipo de enfoque no puede estar vacío';
            return false;
        }
        if (empty($data['id'])) {
            // Guardar
            $sql = "INSERT INTO `tipo_enfoque` (`nombre`, `descripcion`) VALUES ('".$data['nombre']."', '".$data['descripcion']."')";
            if (!$Result1 = $this -> _cnn -> query($sql)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
                return false;
            } else {
                $this -> response['class']  = 'success';
                $this -> response['msg']    = 'Tipo de enfoque agregado correctamente';
                return true;
            }
        } else {
            // Actualizar
            $id = $data['id'];
            $sql = "UPDATE `tipo_enfoque` SET `nombre`='".$data['nombre']."', `descripcion`='".$data['descripcion']."' WHERE `id`='$id'";
            if (!$Result1 = $this -> _cnn -> query($sql)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
                return false;
            } else {
                $this -> response['class']  = 'info';
                $this -> response['msg']    = 'Tipo de enfoque actualizado correctamente';
                return true;
            }
        }
    }
    public function deleteTipoEnfoque($data)
    {
        if (isset($data['id'])) {
            $id = $data['id'];
            $sql = "DELETE FROM `tipo_enfoque` WHERE `id`='$id'";
            if (!$Result1 = $this -> _cnn -> query($sql)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'No puedes eliminar un tipo de enfoque que ya este relacionado con el enfoque de un actor.';
                return false;
            } else {
                $this -> response['class']  = 'success';
                $this -> response['msg']    = 'Tipo de enfoque eliminado correctamente';
                return true;
            }
        } else {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
            return false;
        }
    }

    public function createObject($row)
    {
        $x = new Enfoque();
        $x->setId($row['idEnfoque']);
        $x->setTexto($row['texto']);
        $x->setPost_idPost($row['post_id']);
        $x->setTipoEnfoque($row['tipo_enfoque_id']);
        return $x;
    }

}
