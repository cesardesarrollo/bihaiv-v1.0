<?php
/**
* @author cmagana
* @class EstatusCuestionarioData
* @brief Modelo de base de datos para la tabla de estatuscuestionario
**/

class EstatusCuestionarioData
{
    public static $tablename = "estatuscuestionario";

    public function EstatusCuestionarioData()
    {
         $this->fechaEstatus = "";
         $this->estatus = 0;
         $this->idCuestionariosUsuario = 0;
    }

    public function set($data)
    {
         $this->fechaEstatus = date('Y-m-d H:i:s');
         $this->estatus = $data['estatus'];
         $this->idCuestionariosUsuario = $data['idCuestionariosUsuario'];
    }

    public function add()
    {
        $sql = "insert into ".self::$tablename."(fechaEstatus,estatus,idCuestionariosUsuario) values (\"$this->fechaEstatus\",\"$this->estatus\",\"$this->idCuestionariosUsuario\")";
        return Executor::doit($sql);
    }

    public static function getByidCuestionariosUsuario($idCuestionariosUsuario)
    {
        $sql = "select * from ".self::$tablename." where idCuestionariosUsuario = $idCuestionariosUsuario ORDER BY idEstatusCuestionario DESC LIMIT 0,1";
        $query = Executor::doit($sql);
        return Model::one($query[0], new EstatusCuestionarioData());

    }

    public static function delete($id)
    {
        $sql = "delete from ".self::$tablename." where id=$id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from ".self::$tablename." where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new EstatusCuestionarioData());
    }

    public static function getAll()
    {
        $sql = "select * from ".self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new EstatusCuestionarioData());

    }


    public static function getInEvaluationByActors()
    {
        $sql = "select * from ".self::$tablename." where estatus = 2";
        $query = Executor::doit($sql);
        return Model::many($query[0], new EstatusCuestionarioData());

    }

    public static function getInEvaluationByExperts()
    {
        $sql = "select * from ".self::$tablename." where estatus = 4";
        $query = Executor::doit($sql);
        return Model::many($query[0], new EstatusCuestionarioData());

    }

}

?>
