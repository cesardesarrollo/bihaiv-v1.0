<?php
/**
* @class NotificationData
* @brief Modelo de base de datos para la tabla de notificaciones
**/
require_once 'DTO/Base.php';
require_once "UserData.php";
require_once "FriendData.php";
require_once 'DaoAportacion.php';
require_once 'DaoIniciativa.php';
require_once 'DaoEvento.php';
require_once 'DaoDiscusion.php';
require_once 'DaoOrganizacion.php';

class NotificationData
{

    public static $tablename = "notification";

    public function NotificationData()
    {

        $this->not_type_id = "";
        $this->type_id = "";
        $this->ref_id = "";
        $this->sender_id = "";
        $this->receptor_id = "";
        $this->created_at = date('Y-m-d H:i:s');
        $this->readed_at = date('Y-m-d H:i:s');
        $this->message = "";
    }

    public function getReceptor()
    {
        $UserData = new UserData();
         return $UserData->getById($this->receptor_id);
    }

    public function getSender()
    {
        $UserData = new UserData();
         return $UserData->getById($this->sender_id);
    }

    public function updateMessage()
    {
        $sql = "update ".self::$tablename."  set  message =  \"$this->message\" where id = $this->id";
        $query = Executor::doit($sql);

        if ($query[0]) {
            if (isset($_COOKIE['bihaiv_lang']) && $_COOKIE['bihaiv_lang'] == "en") {
                // Envia correo con notificación
                $enlace = "<a href='".APP_PATH."home/login.php'>Go to Bihaiv platform</a>";
                $subject = "You have received a notification within the  Bihaiv platform";
                $body = "<p>Dear user, we have created a notification within the Bihaiv platform, please access platform to address it.</p><p> $enlace </p>
				<p>Previus view of notification: <strong>".strip_tags($this->message)."</strong>.</p>";
            } else {
                // Envia correo con notificación
                $enlace = "<a href='".APP_PATH."home/login.php'>Dirigirme a Bihaiv</a>";
                $subject = "Has recibido una notificación en Bihaiv";
                $body = "<p>Estimado usuario, se ha creado una notificación dentro de la plataforma Bihaiv, por favor, accede a la plataforma para atenderla.</p><p> $enlace </p>
				<p>Vista previa de la notificación: <strong>".strip_tags($this->message)."</strong>.</p>";
            }

            $user_receptor = $this->getReceptor($this->receptor_id);

            $email_to = $user_receptor->email;
            if ($email_to !== "") {
                $sended = Core::mail($email_to, $subject, $body, strip_tags($body));

            }
        }
    }

    public function add()
    {
        $sql = "insert into ".self::$tablename." (not_type_id,type_id,ref_id,sender_id,receptor_id,created_at, readed_at, message) ";
        $sql .= "value ($this->not_type_id,\"$this->type_id\",\"$this->ref_id\",\"$this->sender_id\",\"$this->receptor_id\",\"$this->created_at\",\"$this->created_at\",\"$this->message\")";
        $query = Executor::doit($sql);

        return $query[1];
    }

    public function del()
    {
        $sql = "delete from ".self::$tablename." where id=$this->id";
        Executor::doit($sql);
    }

    public function deleteByEventId($id)
    {
        $sql = "delete from ".self::$tablename." where type_id = 4 ref_id = $id";
        Executor::doit($sql);
    }

    public function read()
    {
        $sql = "update ".self::$tablename." set is_readed=1 where id=$this->id";
        Executor::doit($sql);
    }

    public static function readById($id)
    {
        $sql = "update ".self::$tablename." set is_readed=1 where id=$id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from ".self::$tablename." where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new NotificationData());
    }

    public static function countUnReads($id)
    {
        $sql = "select count(*) as c from ".self::$tablename." where is_readed=0 and receptor_id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new NotificationData());
    }


    public static function getByRUT($ref_id,$user_id,$type_id)
    {
        $sql = "select * from ".self::$tablename." where ref_id=$ref_id and user_id=$user_id and type_id=$type_id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new NotificationData());
    }

    public static function countByRT($r,$t)
    {
        $sql = "select count(*) as c from ".self::$tablename." where ref_id=$r and type_id=$t";
        $query = Executor::doit($sql);
        return Model::one($query[0], new NotificationData());
    }


    public static function getAll()
    {
        $sql = "select * from ".self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new NotificationData());
    }

    public static function getLast5($id)
    {
        $sql = "select * from ".self::$tablename." where is_readed=0 and receptor_id=$id limit 5";
        $query = Executor::doit($sql);
        return Model::many($query[0], new NotificationData());
    }

    // Con límites
    public static function getAllOfFriendsByUserIdWithLimits($id, $limit_param1, $limit_param2)
    {

        $base = new base();

        $FriendData = new FriendData();
        $friends = $FriendData->getFriends($id);
        $array_notifications = array();

        foreach ($friends as $friend) {
            $friend_id = ($friend->sender_id != $id) ? $friend->sender_id : $friend->receptor_id;

            $sql = "select * from ".self::$tablename." where (sender_id = $friend_id  and receptor_id = $id) ORDER BY created_at DESC LIMIT ".$limit_param1.",".$limit_param2;
            $resultados = $base->advancedQuery($sql);

            if ($resultados && count($resultados) > 0) {
                foreach ($resultados as $resultado) {
                    array_push($array_notifications, $resultado);
                }
            }else if ($resultados) {
                array_push($array_notifications, $resultados);
            }
            
        }

        return array_slice($array_notifications, $limit_param1, $limit_param2);
    }

    // Sin límites
    public static function getAllOfFriendsByUserId($id)
    {

        $base = new base();

        $FriendData = new FriendData();
        $friends = $FriendData->getFriends($id);
        $array_notifications = array();

        foreach ($friends as $friend) {
            $friend_id = ($friend->sender_id != $id) ? $friend->sender_id : $friend->receptor_id;

            $sql = "select * from ".self::$tablename." where (sender_id = $friend_id  and receptor_id = $id) ORDER BY created_at DESC";
            $resultados = $base->advancedQuery($sql);

            if ($resultados && count($resultados) > 0) {
                foreach ($resultados as $resultado) {
                    array_push($array_notifications, $resultado);
                }
            } else {
                array_push($array_notifications, $resultados);
            }
            
        }

        return $array_notifications;
    }

    public static function getLast5ByUserIdInArray($id)
    {
        
        $base = new base();
        $array_notifications = array();

        $sql = "select * from ".self::$tablename." where receptor_id = $id ORDER BY created_at DESC LIMIT 0, 5";
        $resultados = $base->advancedQuery($sql);

        if ($resultados && count($resultados) > 0) {
            foreach ($resultados as $resultado) {
                array_push($array_notifications, $resultado);
            }
        } else {
            array_push($array_notifications, $resultados);
        }
        return $array_notifications;
    }


    public static function getAllByUserIdInArray($id)
    {

        $base = new base();
        $array_notifications = array();

        $sql = "select * from ".self::$tablename." where receptor_id = $id ORDER BY created_at DESC";
        $resultados = $base->advancedQuery($sql);

        if ($resultados && count($resultados) > 0) {
            foreach ($resultados as $resultado) {
                array_push($array_notifications, $resultado);
            }
        } else {
            array_push($array_notifications, $resultados);
        }
        return $array_notifications;
    }


    public static function getAllByUserId($id)
    {
        $sql = "select * from ".self::$tablename." where receptor_id=".$id;
        $query = Executor::doit($sql);
        return Model::many($query[0], new NotificationData());
    }

    public static function getLike($q)
    {
        $sql = "select * from ".self::$tablename." where name like '%$q%'";
        $query = Executor::doit($sql);
        return Model::many($query[0], new NotificationData());
    }

    public static function getMessageById($notificacion_id,$tags = true)
    {

        $DaoIniciativa      = new DaoIniciativa();
        $DaoOrganizacion    = new DaoOrganizacion();
        $DaoAportacion      = new DaoAportacion();
        $DaoEvento          = new DaoEvento();
        $DaoDiscusion       = new DaoDiscusion();
        $notificacion       = self::getById($notificacion_id);
        $continua           = false;

        if (is_array($notificacion)) {
            $_accion = $notificacion['not_type_id'];
            $_sujeto = $notificacion['type_id'];
            $continua = true;
        } else {
            if (is_object($notificacion)) {
                $_accion = $notificacion->not_type_id;
                $_sujeto = $notificacion->type_id;
                $continua = true;
            }
        }

        if ($continua) {

            $sujetos = array(   1 => 'publicación',
              2 => 'imagen',
              3 => 'aportación',
              4 => 'evento',
              5 => 'iniciativa',
              6 => 'información',
              7 => 'unión',
              8 => 'enfoque',
              9 => 'foro',
              10 => 'conexión');

            $acciones = array(  1 => 'like',
                 2 => 'comentó',
                 3 => 'asistir',
                 4 => 'asistió',
                 5 => 'colaborar',
                 6 => 'colaboró',
                 7 => 'calificó',
                 8 => 'creó',
                 9 => 'modificó',
                 10 => 'eliminó',
                 11 => 'invitó');

            $accion     = $acciones[$_accion];
            $sujeto     = $sujetos[$_sujeto];
            $sujeto_id  = $notificacion->ref_id;
            $participa  = $notificacion->receptor_id;
            $crea       = $notificacion->sender_id;
            $created_at = $notificacion->created_at;
            $mensaje    =  "";

            //QUIEN CREA
            $crea = UserData::getById($crea);
            //AVATAR DEL CREADOR
            $org = $DaoOrganizacion->getByUserId($crea->id);
            $img_url_crea ="";
            if ($org->avatar!="assets/img/home/perfil.png" && $org->avatar!="") {
                   $img_url_crea = "storage/users/".$crea->id."/profile/".$org->avatar;
            } else {
                     $img_url_crea = "assets/img/home/perfil.png";
            }

            //QUIEN ESTÁ COLABORANDO
            $participa = UserData::getById($participa);
            //AVATAR DEL PARTIPANTE
            $org = $DaoOrganizacion->getByUserId($participa->id);
            $img_url_participa ="";
            if ($org->avatar!="assets/img/home/perfil.png" && $org->avatar!="") {
                     $img_url_participa = "storage/users/".$participa->id."/profile/".$org->avatar;
            } else {
                     $img_url_participa = "assets/img/home/perfil.png";
            }

            switch ($sujeto) {

            case 'conexión':

                //QUIEN SE SUGIERE PARA CONEXIÓN
                $sugerido = UserData::getById($sujeto_id);
                //AVATAR DEL SUGERIDO
                $org = $DaoOrganizacion->getByUserId($sugerido->id);
                $img_url_sugerido ="";
                if ($org->avatar!="assets/img/home/perfil.png" && $org->avatar!="") {
                    $img_url_sugerido = "storage/users/".$sugerido->id."/profile/".$org->avatar;
                } else {
                    $img_url_sugerido = "assets/img/home/perfil.png";
                }

                switch ($accion) {
                case 'invitó':

                    $mensaje =  "<img src='" . $img_url_crea . "' style='width:20px;height:20px'>
							 <a href='?view=home&id=" . $crea->id . "'>El actor " . $crea->name . "</a> 
							 te " . $accion . " a ser " . $sujeto . " de 
							 <img src='" . $img_url_sugerido . "' style='width:20px;height:20px'>
							 <a href='?view=home&id=" . $sugerido->id . "'>el actor " . $sugerido->name . "</a>";

                }

                break;

            case 'foro':

                switch ($accion) {
                case 'comentó':
                    $foro = $DaoDiscusion->getCommentsById($sujeto_id);
                    if (isset($foro[0]['comentario'])) {
                        $mensaje =  "<img src='" . $img_url_crea . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $crea->id . "'>El actor " . $crea->name . "</a> " . $accion . " en el " . $sujeto . " " . $foro[0]['titulo'] . "  lo siguiente: '<a href='?view=forum-detail&id=" . $foro[0]['tema_id'] . "'> " . $foro[0]['comentario'] . "</a>'.";
                    }     else {

                        $mensaje =  false; //"Esta notificación de participación en un tema del foro ya no está disponible.";
                    }

                    break;

                case 'creó':
                    $foro = $DaoDiscusion->getById($sujeto_id);
                    if (isset($foro[0]['titulo'])) {
                        $mensaje =  "<img src='" . $img_url_crea . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $crea->id . "'>El actor " . $crea->name . "</a> " . $accion . " el " . $sujeto . " con el tema: " . $foro[0]['titulo'] . ".";
                    } else {

                        $mensaje =  false; //"Esta notificación de creación de tema en el foro ya no está disponible.";
                    }

                    break;

                }

                break;

            case 'evento':

                $evento = $DaoEvento->getById($sujeto_id);
                if (is_object($evento) && $evento->titulo != "") {

                    switch ($accion) {

                    case 'creó':
                        $mensaje =  "<img src='" . $img_url_crea . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $crea->id . "'>El actor " . $crea->name . "</a>  " . $accion . " el evento " . $evento->titulo;

                        break;

                    case 'colaborar':
                        $mensaje =  "<img src='" . $img_url_participa . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $participa->id . "'>El actor " . $participa->name . "</a> ha sido invitado a " . $accion . " en " . $sujeto . " <a href='?view=calendar-detail&id=" . $evento->id . "'> " . $evento->titulo . "</a> por <img src='" . $img_url_crea . "' style='width:20px;height:20px'><a href='?view=home&id=" . $crea->id . "'>" . $crea->name . "</a>";

                        break;

                    case 'colaboró':
                        $mensaje =  "<img src='" . $img_url_participa . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $participa->id . "'>El actor " . $participa->name . "</a>  " . $accion . " en " . $sujeto . " " . $evento->titulo . " por <img src='" . $img_url_crea . "' style='width:20px;height:20px'><a href='?view=home&id=" . $crea->id . "'>" . $crea->name . "</a>";

                        break;
                    }
                } else {

                    $mensaje =  false; //"Esta notificación de evento ya no está disponible.";
                }


                break;

            case 'iniciativa':

                $iniciativa = $DaoIniciativa->getById($sujeto_id);
                if (is_object($iniciativa) && $iniciativa->titulo != "") {

                    switch ($accion) {

                    case 'creó':
                        $mensaje =  "<img src='" . $img_url_crea . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $crea->id . "'>El actor " . $crea->name . "</a>  " . $accion . " la iniciativa " . $iniciativa->titulo;

                        break;

                    case 'colaborar':
                        $mensaje =  "<img src='" . $img_url_participa . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $participa->id . "'>El actor " . $participa->name . "</a> ha sido invitado a " . $accion . " en " . $sujeto . " " . $iniciativa->titulo . " por <img src='" . $img_url_crea . "' style='width:20px;height:20px'><a href='?view=home&id=" . $crea->id . "'>" . $crea->name . "</a>";
                        break;

                    case 'colaboró':
                        $mensaje =  "<img src='" . $img_url_participa . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $participa->id . "'>El actor " . $participa->name . "</a>  " . $accion . " en " . $sujeto . " " . $iniciativa->titulo . " por <img src='" . $img_url_crea . "' style='width:20px;height:20px'><a href='?view=home&id=" . $crea->id . "'>" . $crea->name . "</a>";
                        break;
                    }
                } else {

                    $mensaje =  false; //"Esta notificación de iniciativa ya no está disponible.";
                }

                break;

            case 'aportación':

                $aportacion = $DaoAportacion->getById($sujeto_id);
                if (is_object($aportacion) && $aportacion->titulo != "") {

                    switch ($accion) {
                    case 'creó':
                        $mensaje =  "<img src='" . $img_url_crea . "' style='width:20px;height:20px'> <a href='?view=home&id=" . $crea->id . "'>El actor " . $crea->name . "</a>  " . $accion . " la " . $sujeto . " " . $aportacion->titulo;
                        break;
                    }
                } else {

                    $mensaje =  false; //"Esta notificación de aportación ya no está disponible.";
                }


                break;

            default:

                $mensaje =  false; //"Esta notificación no se encontró.";

                break;

            }
            if ($tags) {
                if ($mensaje) { return $mensaje;
                }
            } else {
                if ($mensaje) { return strip_tags($mensaje);
                }
            }

        }

    }

}

?>
