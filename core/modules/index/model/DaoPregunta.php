<?php
require_once 'DTO/Base.php';
require_once 'DTO/Pregunta.php';

class DaoPregunta extends base
{
    
    public $tableName="pregunta"; 

    public function add(Pregunta $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (Pregunta, Orden, Cuestionario_idCuestionario) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getPregunta(), "text"),
              $this->GetSQLValueString($x->getOrden(), "int"),
              $this->GetSQLValueString($x->getCuestionario_idCuestionario(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(Pregunta $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET Pregunta=%s , Orden=%s , Cuestionario_idCuestionario=%s  WHERE idPregunta = %s",
              $this->GetSQLValueString($x->getPregunta(), "text"),
              $this->GetSQLValueString($x->getOrden(), "int"),
              $this->GetSQLValueString($x->getCuestionario_idCuestionario(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idPregunta=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idPregunta= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getNetxOrden()
    {
        $query="SELECT * FROM ".$this->tableName." ORDER BY idPregunta DESC LIMIT 1";
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
                $row_consulta= $Result1->fetch_assoc();
                $totalRows_consulta= $Result1->num_rows;
            if ($totalRows_consulta>0) {
                return $row_consulta['Orden']+1;
            } else {
                return 1; 
            }
        }
    }
    
    
    public function getPreguntasCuestionario($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Cuestionario_idCuestionario= ".$Id." ORDER BY Orden ASC";
        return $this->advancedQueryByObjetc($query);
    }

    
    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
            return $resp;
    }
    public function createObject($row)
    {
        $x = new Pregunta();
        $x->setId($row['idPregunta']);
        $x->setPregunta($row['Pregunta']);
        $x->setOrden($row['Orden']);
        $x->setCuestionario_idCuestionario($row['Cuestionario_idCuestionario']); 
        return $x;
    }
    

}
