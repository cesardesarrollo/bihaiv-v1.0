<?php
require_once 'DTO/Base.php';
require_once 'DTO/Discusion_comment.php';

class DaoComment extends base
{
    public $tableName = "tema_comentario";
    public function add($x)
    {
        $query = sprintf(
            "INSERT INTO `".$this -> tableName."` (`comentario`, `created_at`, `updated_at`, `tema_id`, `organizacion_id`) VALUES (%s, %s, %s, %s, %s);",
            $this -> GetSQLValueString($x -> getComentario(), "text"),
            $this -> GetSQLValueString($x -> getCreated_at(), "date"),
            $this -> GetSQLValueString($x -> getUpdated_at(), "date"),
            $this -> GetSQLValueString($x -> getTema_id(), "int"),
            $this -> GetSQLValueString($x -> getOrganizacion_id(), "int")
        );
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            return $this -> _cnn -> insert_id;
        }
    }
    public function updateRating($data)
    {
        $query = "INSERT INTO `tema_calificacion` (`tema_id`, `calificacion`, `organizacion_id`) VALUES (".$data['tema_id'].", ".$data['calificacion'].", ".$data['organizacion_id'].") ON DUPLICATE KEY UPDATE calificacion = ".$data['calificacion'];
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            return $Result1;
        }
    }
    public function rate_comment($data)
    {
        $query = "INSERT IGNORE INTO `tema_like` (`tema_comentario_id`, `organizacion_id`) VALUES (".$data['id'].", ".$data['organizacion_id'].")";
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            $this -> response['status'] = false;
            $this -> response['msg']    = "Ah ocurrido un error intentando calificar este tema, intentalo más tarde.";
            $this -> response['class']  = 'danger';
            $this -> response['likes']  = false;
        } else {
            if ($this -> _cnn -> affected_rows == 0 ) {
                /* intento de duplicado */
                $this -> response['status'] = false;
                $this -> response['msg']    = "Ya calificaste este comentario anteriormente.";
                $this -> response['class']  = 'info';
                $this -> response['likes']  = false;
            } else {
                /* nuevo like */
                $this -> response['status'] = true;
                $this -> response['msg']    = "Gracias por calificar este tema.";
                $this -> response['class']  = 'success';
                if ($likes =  $this -> getOneRow("SELECT count(*) as likes FROM tema_like WHERE tema_comentario_id = ".$data['id'])) {
                    $this -> response['likes']  = $likes['likes'];
                    $this -> response['comentario_id']  = $data['id'];
                } else {
                    $this -> response['likes']  = false;
                }
            }
        }
    }
    public function update(Discusion $x)
    {
        $query=sprintf(
            "UPDATE ".$this->tableName." SET titulo=%s, descripcion=%s, created_at=%s, updated_at=%s, privacidad=%s, promedio=%s, organizacion_id=%s, roles=%s  WHERE id = %s",
            $this->GetSQLValueString($x->getTitulo(), "text"),
            $this->GetSQLValueString($x->getDescripcion(), "text"),
            $this->GetSQLValueString($x->getCreated_at(), "date"),
            $this->GetSQLValueString($x->getUpdated_at(), "date"),
            $this->GetSQLValueString($x->getPrivacidad(), "text"),
            $this->GetSQLValueString($x->getPromedio(), "int"),
            $this->GetSQLValueString($x->getUser_id(), "int"),
            $this->GetSQLValueString($x->getRoles(), "text"),
            $this->GetSQLValueString($x->getId(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }
    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idDiscusion=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }
    public function getCommentsByTopic($idTema)
    {
        $query="SELECT * FROM tema_comentario WHERE tema_id = " . $idTema;
        return $this->advancedQuery($query);
    }
    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
        return $this->advancedQuery($query);
    }
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
        return $this->advancedQuery($query);
    }
    public function getByUserId($Id)
    {
        $sql="SELECT * FROM ".$this->tableName." WHERE organizacion_id= ".$Id;
        $query = Executor::doit($sql);
        return Model::many($query[0], new Discusion());
    }
    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
        return $resp;
    }
    public function createObject($row)
    {
        $x = new Discusion();
        $x->setId($row['id']);
        $x->setTitulo($row['titulo']);
        $x->setDescripcion($row['descripcion']);
        $x->setCreated_at($row['created_at']);
        $x->setUpdated_at($row['updated_at']);
        $x->setPrivacidad($row['privacidad']);
        $x->setPromedio($row['promedio']);
        $x->setUser_id($row['organizacion_id']);
        $x->setRoles($row['roles']);
        return $x;
    }
}
