<?php
require_once 'DTO/Base.php';
require_once 'DTO/Roles.php';

class DaoRoles extends base
{

    public $tableName="roles";

    public function add(Roles $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (nombre, descripcion,description,descripcion_corta,short_description, imagen, activo) VALUES (%s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getDescription(), "text"),
              $this->GetSQLValueString($x->getDescripcionCorta(), "text"),
              $this->GetSQLValueString($x->getShortDescription(), "text"),
              $this->GetSQLValueString($x->getImagen(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }


    public function update(Roles $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET nombre=%s, descripcion=%s, description=%s, descripcion_corta=%s, short_description=%s, activo=%s, imagen=%s  WHERE idRol = %s",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getDescription(), "text"),
              $this->GetSQLValueString($x->getDescripcionCorta(), "text"),
              $this->GetSQLValueString($x->getShortDescription(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getImagen(), "text"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idRol=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idRol= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function getAll($offset=null,$limit=null,$buscar=null)
    {
        $query="";
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" AND (nombre LIKE '%".$buscar."%' OR descripcion LIKE '".$buscar."')";
        }


        $query="SELECT * FROM ".$this->tableName." WHERE nombre != 'Sin rol' ".$queryBuscar." ".$query;
        return $this->advancedQueryByObjetc($query);
    }

    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function createObject($row)
    {
        $x = new Roles();
        $x->setId($row['idRol']);
        $x->setNombre($row['nombre']);
        $x->setDescripcion($row['descripcion']);
        $x->setDescription($row['description']);
        $x->setDescripcionCorta($row['descripcion_corta']);
        $x->setShortDescription($row['short_description']);
        $x->setImagen($row['imagen']);
        $x->setActivo($row['activo']);
        return $x;
    }

}
