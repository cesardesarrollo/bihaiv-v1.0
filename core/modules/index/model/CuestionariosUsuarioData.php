<?php
/**
* @author cmagana
* @class CuestionarioData
* @brief Modelo de base de datos para la tabla de cuestionariosusuarios
**/
require_once 'DTO/Base.php';
require_once 'DaoOrganizacion.php';
require_once 'DaoRoles.php';

class CuestionariosUsuarioData
{
    public static $tablename = "cuestionariosusuario";

    public function CuestionariosUsuarioData()
    {
        $this->fechaCreacion = "";
        $this->idCuestionario = 0;
        $this->user_id = 0;
        $this->Roles_idRol = null;
    }
    
    public function set($idCuestionario,$user_id,$clasificacion_sistema)
    {
        
        if ($clasificacion_sistema !== 'Sin rol') {
            $sql = "select idRol from roles where nombre = '$clasificacion_sistema' LIMIT 0,1";
            $query = Executor::doit($sql);
            $roles = Model::one($query[0], new Roles());
            $this->Roles_idRol = $roles->idRol;
        }

        // Obtener organizacion
        $sql = "select id from organizacion where Usuarios_idUsuarios = '$user_id' LIMIT 0,1";
        $query = Executor::doit($sql);
        $organizacion = Model::one($query[0], new Organizacion());
        $this->fechaCreacion = date('Y-m-d H:i:s');
        $this->idCuestionario = $idCuestionario;
        $this->user_id = $organizacion->id;
        return $this;
    }

    public static function getByUserCuestionario($user_id,$idCuestionario)
    {
        // Obtener organizacion
        $sql = "select id from organizacion where Usuarios_idUsuarios = '$user_id' LIMIT 0,1";
        $query = Executor::doit($sql);
        $organizacion = Model::one($query[0], new Organizacion());
        $sql = "select * from ".self::$tablename." where user_id = $organizacion->id and idCuestionario = $idCuestionario";
        $query = Executor::doit($sql);
        return Model::one($query[0], new CuestionariosUsuarioData());
    }

    public function add()
    {
        $sql = "INSERT INTO `".self::$tablename."` (`fechaCreacion`, `idCuestionario`, `Roles_idRol`, `user_id`) VALUES (\"$this->fechaCreacion\",\"$this->idCuestionario\",\"$this->Roles_idRol\",\"$this->user_id\")";
        return Executor::doit($sql);
    }

    public static function delete($id)
    {
        $sql = "delete from ".self::$tablename." where idCuestionariosUsuario=$id";
        Executor::doit($sql);
    }
    public function del()
    {
        $sql = "delete from ".self::$tablename." where idCuestionariosUsuario=$this->id";
        Executor::doit($sql);
    }

    public function update()
    {
        $sql = "update ".self::$tablename." set username=\"$this->username\",name=\"$this->name\",rfc=\"$this->rfc\",id_country=\"$this->country\",email=\"$this->email\",is_active=\"$this->is_active\" where id=$this->id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from ".self::$tablename." where idCuestionariosUsuario = $id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new CuestionariosUsuarioData());
    }

    public static function getAll()
    {
        $sql = "select * from ".self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new CuestionariosUsuarioData());

    }

}

?>
