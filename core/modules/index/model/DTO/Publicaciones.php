<?php
class Publicaciones
{
    
    public $id;
    public $fechaCreado;
    public $tipo;
    public $user_id;
    public $active;
    public $rating;

    public $is_public;
    public $by_admin;
    public $by_expert;

    
    function __construct() 
    {
        $this->id = "";
        $this->fechaCreado = "";
        $this->tipo = "";
        $this->user_id = "";
        $this->active = 1;
        $this->rating = 0;
        $this->is_public = "";
        $this->by_admin = 0;
        $this->by_expert = 0;
    }

    
    function getId() 
    {
        return $this->id;
    }

    function getFechaCreado() 
    {
        return $this->fechaCreado;
    }

    function getTipo() 
    {
        return $this->tipo;
    }

    function getUser_id() 
    {
        return $this->user_id;
    }

    function getActive() 
    {
        return $this->active;
    }

    function getRating() 
    {
        return $this->rating;
    }

    function getIsPublic() 
    {
        return $this->is_public;
    }

    function getByAdmin() 
    {
        return $this->by_admin;
    }

    function getByExpert() 
    {
        return $this->by_expert;
    }

    function setId($id) 
    {
        $this->id = $id;
    }
    
    function setFechaCreado($fechaCreado) 
    {
        $this->fechaCreado = $fechaCreado;
    }

    function setTipo($tipo) 
    {
        $this->tipo = $tipo;
    }

    function setUser_id($user_id) 
    {
        $this->user_id = $user_id;
    }

    function setActive($active) 
    {
        $this->active = $active;
    }

    function setRating($rating) 
    {
        $this->rating = $rating;
    }

    function setIsPublic($is_public) 
    {
        $this->is_public = $is_public;
    }

    function setByAdmin($by_admin) 
    {
        $this->by_admin = $by_admin;
    }

    function setByExpert($by_expert) 
    {
        $this->by_expert = $by_expert;
    }

}
