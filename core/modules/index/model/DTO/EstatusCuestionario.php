<?php
class EstatusCuestionario
{
    
    public $id;
    public $fechaEstatus;
    public $estatus;
    public $idCuestionariosUsuario;
    
    function __construct() 
    {
        $this->id = "";
        $this->fechaEstatus = "";
        $this->estatus = "";
        $this->idCuestionariosUsuario = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getFechaEstatus() 
    {
        return $this->fechaEstatus;
    }

    function getEstatus() 
    {
        return $this->estatus;
    }

    function getIdCuestionariosUsuario() 
    {
        return $this->idCuestionariosUsuario;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setFechaEstatus($fechaEstatus) 
    {
        $this->fechaEstatus = $fechaEstatus;
    }

    function setEstatus($estatus) 
    {
        $this->estatus = $estatus;
    }

    function setIdCuestionariosUsuario($idCuestionariosUsuario) 
    {
        $this->idCuestionariosUsuario = $idCuestionariosUsuario;
    }


   
   
}
