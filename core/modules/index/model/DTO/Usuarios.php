<?php
class Usuarios
{

    public $id;
    public $nombre;
    public $email;
    public $pass;
    public $recoverPass;
    public $fecha_creado;
    public $activo;
    public $tipoRel;
    public $llaveImg;

    function __construct() 
    {
        $this->id = "";
        $this->nombre = "";
        $this->email = "";
        $this->pass = "";
        $this->recoverPass = "";
        $this->fecha_creado = "";
        $this->activo = "";
        $this->tipoRel = "";
        $this->llaveImg = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getNombre() 
    {
        return $this->nombre;
    }

    function getEmail() 
    {
        return $this->email;
    }

    function getPass() 
    {
        return $this->pass;
    }

    function getRecoverPass() 
    {
        return $this->recoverPass;
    }

    function getFecha_creado() 
    {
        return $this->fecha_creado;
    }

    function getActivo() 
    {
        return $this->activo;
    }

    function getTipoRel() 
    {
        return $this->tipoRel;
    }

    function getLlaveImg() 
    {
        return $this->llaveImg;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }

    function setEmail($email) 
    {
        $this->email = $email;
    }

    function setPass($pass) 
    {
        $this->pass = $pass;
    }

    function setRecoverPass($recoverPass) 
    {
        $this->recoverPass = $recoverPass;
    }

    function setFecha_creado($fecha_creado) 
    {
        $this->fecha_creado = $fecha_creado;
    }

    function setActivo($activo) 
    {
        $this->activo = $activo;
    }

    function setTipoRel($tipoRel) 
    {
        $this->tipoRel = $tipoRel;
    }

    function setLlaveImg($llaveImg) 
    {
        $this->llaveImg = $llaveImg;
    }





}
