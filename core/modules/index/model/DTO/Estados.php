<?php
class Estados
{
    
    public $id;
    public $clave;
    public $nombre;
    public $abrev;
    public $activo;
    public $paises_id;
    public $iso;
    
    function __construct() 
    {
        $this->id = "";
        $this->clave = "";
        $this->nombre = "";
        $this->abrev = "";
        $this->activo = "";
        $this->paises_id = "";
        $this->iso = "";
    }
    
    function getId() 
    {
        return $this->id;
    }
    function getNombre() 
    {
        return $this->nombre;
    }
    function getClave() 
    {
        return $this->clave;
    }
    function getAbrev() 
    {
        return $this->abrev;
    }
    function getActivo() 
    {
        return $this->activo;
    }
    function getPaisId() 
    {
        return $this->paises_id;
    }
    function getISO() 
    {
        return $this->iso;
    }
    function setId($id) 
    {
        $this->id = $id;
    }
    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }
    function setClave($clave) 
    {
            $this->clave = $clave;
    }
    function setAbrev($abrev) 
    {
            $this->abrev = $abrev;
    }
    function setActivo($activo) 
    {
            $this->activo = $activo;
    }
    function setPaisId($paises_id) 
    {
            $this->paises_id = $paises_id;
    }
    function setISO($iso) 
    {
            $this->iso = $iso;
    }


}
    
