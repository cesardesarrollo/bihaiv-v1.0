<?php

class ImagenHome
{

    public $id;
    public $titulo;
    public $nombre;
    public $descripcion;
    public $description;
    public $llaveImg;
    public $Usuario_id;
    public $textoBoton;
    public $linkBoton;
    public $portada;

    function __construct() 
    {
        $this->id = "";
        $this->titulo = "";
        $this->nombre = "";
        $this->descripcion = "";
        $this->description = "";
        $this->llaveImg = "";
        $this->Usuario_id = "";
        $this->textoBoton = "";
        $this->linkBoton = "";
        $this->portada = "";
    }

    function getTextoBoton() 
    {
        return $this->textoBoton;
    }

    function getLinkBoton() 
    {
        return $this->linkBoton;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function getId() 
    {
        return $this->id;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getNombre() 
    {
        return $this->nombre;
    }

    function getDescripcion() 
    {
        return $this->descripcion;
    }

    function getDescription() 
    {
        return $this->description;
    }

    function getLlaveImg() 
    {
        return $this->llaveImg;
    }

    function getPortada() 
    {
        return $this->portada;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTextoBoton($textoBoton) 
    {
        $this->textoBoton = $textoBoton;
    }

    function setLinkBoton($linkBoton) 
    {
        $this->linkBoton = $linkBoton;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }

    function setDescription($description) 
    {
        $this->description = $description;
    }

    function setLlaveImg($llaveImg) 
    {
        $this->llaveImg = $llaveImg;
    }

    function setPortada($portada) 
    {
        $this->portada = $portada;
    }

}
