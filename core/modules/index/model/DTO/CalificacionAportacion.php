<?php
class CalificacionAportacion
{
    
    public $id;
    public $calificacion;
    public $fechaCalificacion;
    public $Aportacion_idAportacion;
    public $Usuario_id;
    
    function __construct() 
    {
        $this->id = "";
        $this->calificacion = "";
        $this->fechaCalificacion = "";
        $this->Aportacion_idAportacion = "";
        $this->Usuario_id = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getCalificacion() 
    {
        return $this->calificacion;
    }

    function getFechaCalificacion() 
    {
        return $this->fechaCalificacion;
    }

    function getAportacion_idAportacion() 
    {
        return $this->Aportacion_idAportacion;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setCalificacion($calificacion) 
    {
        $this->calificacion = $calificacion;
    }

    function setFechaCalificacion($fechaCalificacion) 
    {
        $this->fechaCalificacion = $fechaCalificacion;
    }

    function setAportacion_idAportacion($Aportacion_idAportacion) 
    {
        $this->Aportacion_idAportacion = $Aportacion_idAportacion;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }


    
   
}
