<?php
class Municipios
{
    
    public $id;
    public $nombre;

    function __construct() 
    {
        $this->id = "";
        $this->clave = "";
        $this->nombre = "";
        $this->activo = "";
        $this->estado_id = "";
    }

    function getId() 
    {
        return $this->id;
    }
    function getClave() 
    {
        return $this->clave;
    }
    function getNombre() 
    {
        return $this->nombre;
    }
    function getActivo() 
    {
        return $this->activo;
    }
    function getEstadoId() 
    {
        return $this->estado_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }
    function setClave($clave) 
    {
        $this->clave = $clave;
    }
    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }
    function setActivo($activo) 
    {
        $this->activo = $activo;
    }
    function setEstadoId($estado_id) 
    {
        $this->estado_id = $estado_id;
    }

}
    
