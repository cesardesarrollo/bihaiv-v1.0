<?php
class OpcionesRespuesta
{
    
    public $id;
    public $Texto;
    public $orden;
    public $Pregunta_idPregunta;
    public $Roles_idRol;
    
    function __construct() 
    {
        $this->id = "";
        $this->Texto = "";
        $this->orden = "";
        $this->Pregunta_idPregunta = "";
        $this->Roles_idRol = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getTexto() 
    {
        return $this->Texto;
    }

    function getOrden() 
    {
        return $this->orden;
    }

    function getPregunta_idPregunta() 
    {
        return $this->Pregunta_idPregunta;
    }

    function getRoles_idRol() 
    {
        return $this->Roles_idRol;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTexto($Texto) 
    {
        $this->Texto = $Texto;
    }

    function setOrden($orden) 
    {
        $this->orden = $orden;
    }

    function setPregunta_idPregunta($Pregunta_idPregunta) 
    {
        $this->Pregunta_idPregunta = $Pregunta_idPregunta;
    }

    function setRoles_idRol($Roles_idRol) 
    {
        $this->Roles_idRol = $Roles_idRol;
    }



    
}
