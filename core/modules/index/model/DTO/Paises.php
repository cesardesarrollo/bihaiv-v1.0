<?php
class Paises
{
    
    public $id;
    public $nombre;
    public $prefijo;
    
    function __construct() 
    {
        $this->id = "";
        $this->nombre = "";
        $this->prefijo = "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getNombre() 
    {
        return $this->nombre;
    }

    function getPrefijo() 
    {
        return $this->prefijo;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }

    function setPrefijo($prefijo) 
    {
        $this->prefijo = $prefijo;
    }
    
}
    
