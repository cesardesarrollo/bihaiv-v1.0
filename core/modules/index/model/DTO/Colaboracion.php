<?php
class Colaboracion
{
    
    public $id;
    public $fechaCreado;
    public $estatus;
    public $Iniciativa_idIniciativa;
    public $Usuario_id;
    public $evento_id;
    
    function __construct() 
    {
        $this->id = "";
        $this->fechaCreado = "";
        $this->estatus = "";
        $this->Iniciativa_idIniciativa = 0;
        $this->Usuario_id = "";
        $this->evento_id = 0;
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getFechaCreado() 
    {
        return $this->fechaCreado;
    }

    function getEstatus() 
    {
        return $this->estatus;
    }

    function getIniciativa_idIniciativa() 
    {
        return $this->Iniciativa_idIniciativa;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function getEventoId() 
    {
        return $this->evento_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }
    function setFechaCreado($fechaCreado) 
    {
        $this->fechaCreado = $fechaCreado;
    }

    function setEstatus($estatus) 
    {
        $this->estatus = $estatus;
    }

    function setIniciativa_idIniciativa($Iniciativa_idIniciativa) 
    {
        $this->Iniciativa_idIniciativa = $Iniciativa_idIniciativa;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }

    function setEventoId($evento_id) 
    {
        $this->evento_id = $evento_id;
    }

}
