<?php
class SeccionAportacion
{

    public $id;
    public $titulo;
    public $descripcion;
    public $title;
    public $description;
    public $url;
    public $activo;

    function __construct() 
    {
        $this->id = 0;
        $this->titulo = "";
        $this->descripcion = "";
        $this->title = "";
        $this->description = "";
        $this->url = "";
        $this->activo = 0;
    }

    function getId() 
    {
        return $this->id;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getDescripcion() 
    {
        return $this->descripcion;
    }

    function getTitle() 
    {
        return $this->title;
    }

    function getDescription() 
    {
        return $this->description;
    }

    function getURL() 
    {
        return $this->url;
    }

    function getActivo() 
    {
        return $this->activo;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }

    function setTitle($title) 
    {
        $this->title = $title;
    }

    function setDescription($description) 
    {
        $this->description = $description;
    }

    function setURL($url) 
    {
        $this->url = $url;
    }

    function setActivo($activo) 
    {
        $this->activo = $activo;
    }
}
