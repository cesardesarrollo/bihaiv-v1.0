<?php
class Enfoque
{

    public $id;
    public $texto;
    public $Post_idPost;
    public $tipo_enfoque;

    function __construct() 
    {
        $this->id = "";
        $this->texto = "";
        $this->tipo_enfoque = "";
        $this->Post_idPost = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getTexto() 
    {
        return $this->texto;
    }
    function getTipoEnfoque() 
    {
        return $this -> tipo_enfoque;
    }

    function getPost_idPost() 
    {
        return $this->Post_idPost;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTexto($texto) 
    {
        $this->texto = $texto;
    }
    function setTipoEnfoque($tipo_enfoque) 
    {
        $this -> tipo_enfoque = $tipo_enfoque;
    }

    function setPost_idPost($Post_idPost) 
    {
        $this->Post_idPost = $Post_idPost;
    }



}
