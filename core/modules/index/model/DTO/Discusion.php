<?php
class Discusion
{

    public $id;
    public $titulo;
    public $descripcion;
    public $created_at;
    public $updated_at;
    public $privacidad;
    public $promedio;
    public $organizacion_id;
    public $roles;
    

    function __construct() 
    {
        $this->id = "";
        $this->titulo = "";
        $this->descripcion = "";
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->privacidad = "PUBLICO";
        $this->promedio = 0;
        $this->organizacion_id = 0;
        $this->roles = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getDescripcion() 
    {
        return $this->descripcion;
    }

    function getCreated_at() 
    {
        return $this->created_at;
    }

    function getUpdated_at() 
    {
        return $this->updated_at;
    }

    function getPrivacidad() 
    {
        return $this->privacidad;
    }

    function getPromedio() 
    {
        return $this->promedio;
    }

    function getUser_id() 
    {
        return $this->organizacion_id;
    }

    function getRoles() 
    {
        return $this->roles;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }

    function setCreated_at() 
    {
        $this->created_at;
    }

    function setUpdated_at() 
    {
        $this->updated_at;
    }

    function setPrivacidad($privacidad) 
    {
        $this->privacidad = $privacidad;
    }

    function setPromedio() 
    {
        $this->promedio;
    }

    function setUser_id($organizacion_id) 
    {
        $this->organizacion_id = $organizacion_id;
    }

    function setRoles($roles) 
    {
        $this->roles = $roles;
    }


}
