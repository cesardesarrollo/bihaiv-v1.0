<?php
class Ubicaciones
{
    
    public $id;
    public $idOrganizacion;
    public $idPais;
    public $idEstado;
    public $idMunicipio;
    public $idLocalidad;
    public $is_main;
    public $is_matriz;

    function __construct() 
    {
        $this->idOrganizacion = "";
        $this->idPais = "";
        $this->idEstado = "";
        $this->idMunicipio = "";
        $this->idLocalidad = "";
        $this->is_main = "";
        $this->is_matriz = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getIdPais() 
    {
        return $this->idPais;
    }

    function getIdEstado() 
    {
        return $this->idEstado;
    }

    function getIdMunicipio() 
    {
        return $this->idMunicipio;
    }

    function getIdLocalidad() 
    {
        return $this->idLocalidad;
    }
    function getIdOrganizacion() 
    {
        return $this->idOrganizacion;
    }
    
    function getIsMain() 
    {
        return $this->is_main;
    }

    function getIsMatriz() 
    {
        return $this->is_matriz;
    }
    
    function setId($id) 
    {
        $this->id = $id;
    }

    function setIdPais($idPais) 
    {
        $this->idPais = $idPais;
    }

    function setIdEstado($idEstado) 
    {
        $this->idEstado = $idEstado;
    }

    function setIdMunicipio($idMunicipio) 
    {
        $this->idMunicipio = $idMunicipio;
    }

    function setIdLocalidad($idLocalidad) 
    {
        $this->idLocalidad = $idLocalidad;
    }
    function setIdOrganizacion($idOrganizacion) 
    {
        $this->idOrganizacion = $idOrganizacion;
    }

    function setIsMain($is_main) 
    {
        $this->is_main = $is_main;
    }
    
    function setIsMatriz($is_matriz) 
    {
        $this->is_matriz = $is_matriz;
    }

}
