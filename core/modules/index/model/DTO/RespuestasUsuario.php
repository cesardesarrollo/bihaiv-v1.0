<?php
class RespuestasUsuario
{
    
    public $id;
    public $fechaRespondio;
    public $OpcionesRespuesta_idOpcionesRespuesta;
    public $Usuario_id;
    
    function __construct() 
    {
        $this->id = "";
        $this->fechaRespondio = date("Y-m-d H:i:s");
        $this->OpcionesRespuesta_idOpcionesRespuesta = "";
        $this->Usuario_id = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getFechaRespondio() 
    {
        return $this->fechaRespondio;
    }

    function getOpcionesRespuesta_idOpcionesRespuesta() 
    {
        return $this->OpcionesRespuesta_idOpcionesRespuesta;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setFechaRespondio($fechaRespondio) 
    {
        $this->fechaRespondio = $fechaRespondio;
    }

    function setOpcionesRespuesta_idOpcionesRespuesta($OpcionesRespuesta_idOpcionesRespuesta) 
    {
        $this->OpcionesRespuesta_idOpcionesRespuesta = $OpcionesRespuesta_idOpcionesRespuesta;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }


    
   
}
