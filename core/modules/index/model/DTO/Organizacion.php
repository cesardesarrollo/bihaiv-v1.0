<?php
class Organizacion
{
    
    public $id;
    public $Roles_idRol;
    public $telefono;
    public $urlWeb;
    public $ubicacion;
    public $mision;
    public $vision;
    public $quienSoy;
    public $lat;
    public $lng;
    public $idAlcanze;
    public $portada;
    public $avatar;
    public $Usuarios_idUsuarios;
    


    function __construct() 
    {
        $this->id = "";
        $this->Roles_idRol = "";
        $this->telefono = "";
        $this->urlWeb = "";
        $this->ubicacion = "";
        $this->mision = "";
        $this->vision = "";
        $this->quienSoy = "";
        $this->lat = "";
        $this->lng = "";
        $this->idAlcanze = 0;
        $this->portada = "";
        $this->avatar = "";
        $this->Usuarios_idUsuarios = "";
        $this->avatar = "";
        $this->portada = "";

    }
    
    function getId() 
    {
        return $this->id;
    }

    function getRoles_idRol() 
    {
        return $this->Roles_idRol;
    }

    function getTelefono() 
    {
        return $this->telefono;
    }

    function getUrlWeb() 
    {
        return $this->urlWeb;
    }

    function getUbicacion() 
    {
        return $this->ubicacion;
    }

    function getMision() 
    {
        return $this->mision;
    }

    function getVision() 
    {
        return $this->vision;
    }

    function getQuienSoy() 
    {
        return $this->quienSoy;
    }

    function getLat() 
    {
        return $this->lat;
    }

    function getLng() 
    {
        return $this->lng;
    }

    function getIdAlcanze() 
    {
        return $this->idAlcanze;
    }

    function getAvatar() 
    {
        return $this->avatar;
    }

    function getPortada() 
    {
        return $this->portada;
    }

    function getUsuarios_idUsuarios() 
    {
        return $this->Usuarios_idUsuarios;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setRoles_idRol($Roles_idRol) 
    {
        $this->Roles_idRol = $Roles_idRol;
    }

    function setTelefono($telefono) 
    {
        $this->telefono = $telefono;
    }

    function setUrlWeb($urlWeb) 
    {
        $this->urlWeb = $urlWeb;
    }

    function setUbicacion($ubicacion) 
    {
        $this->ubicacion = $ubicacion;
    }

    function setMision($mision) 
    {
        $this->mision = $mision;
    }

    function setVision($vision) 
    {
        $this->vision = $vision;
    }

    function setQuienSoy($quienSoy) 
    {
        $this->quienSoy = $quienSoy;
    }

    function setLat($lat) 
    {
        $this->lat = $lat;
    }

    function setLng($lng) 
    {
        $this->lng = $lng;
    }

    function setIdAlcanze($idAlcanze) 
    {
        $this->idAlcanze = $idAlcanze;
    }

    function setAvatar($avatar) 
    {
        $this->avatar = $avatar;
    }

    function setPortada($portada) 
    {
        $this->portada = $portada;
    }

    function setUsuarios_idUsuarios($Usuarios_idUsuarios) 
    {
        $this->Usuarios_idUsuarios = $Usuarios_idUsuarios;
    }

}
