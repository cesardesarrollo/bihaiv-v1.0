<?php
class Aportacion
{

    public $id;
    public $titulo;
    public $descripcion;
    public $fecha;
    public $thumbnail;
    public $file;
    public $idCategoriaAportacion;
    public $llaveAportacion;
    public $Post_idPost;

    function __construct() 
    {
        $this->id = "";
        $this->titulo = "";
        $this->descripcion = "";
        $this->fecha = "";
        $this->thumbnail = "";
        $this->file = "";
        $this->idCategoriaAportacion = "";
        $this->llaveAportacion = "";
        $this->Post_idPost = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getDescripcion() 
    {
        return $this->descripcion;
    }

    function getFecha() 
    {
        return $this->fecha;
    }

    function getThumbnail() 
    {
        return $this->thumbnail;
    }

    function getFile() 
    {
        return $this->file;
    }

    function getCategory() 
    {
        return $this->idCategoriaAportacion;
    }

    function getLlaveAportacion() 
    {
        return $this->llaveAportacion;
    }

    function getPost_idPost() 
    {
        return $this->Post_idPost;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }

    function setFecha($fecha) 
    {
        $this->fecha = $fecha;
    }

    function setThumbnail($thumbnail) 
    {
        $this->thumbnail = $thumbnail;
    }

    function setFile($file) 
    {
        $this->file = $file;
    }

    function setCategory($idCategoriaAportacion) 
    {
        $this->idCategoriaAportacion = $idCategoriaAportacion;
    }

    function setLlaveAportacion($llaveAportacion) 
    {
        $this->llaveAportacion = $llaveAportacion;
    }

    function setPost_idPost($Post_idPost) 
    {
        $this->Post_idPost = $Post_idPost;
    }



}
