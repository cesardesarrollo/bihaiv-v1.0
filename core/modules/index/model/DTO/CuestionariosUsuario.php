<?php
class CuestionariosUsuario
{
    
    public $id;
    public $fechaCreacion;
    public $Usuario_id;
    public $idCuestionario;
    public $Roles_idRol;
    
    function __construct() 
    {
        $this->id = "";
        $this->fechaCreacion = "";
        $this->Usuario_id = "";
        $this->idCuestionario = "";
        $this->Roles_idRol = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getFechaCreacion() 
    {
        return $this->fechaCreacion;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function getIdCuestionario() 
    {
        return $this->idCuestionario;
    }

    function getRoles_idRol() 
    {
        return $this->Roles_idRol;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setFechaCreacion($fechaCreacion) 
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }

    function setIdCuestionario($idCuestionario) 
    {
        $this->idCuestionario = $idCuestionario;
    }

    function setRoles_idRol($Roles_idRol) 
    {
        $this->Roles_idRol = $Roles_idRol;
    }


   
}
