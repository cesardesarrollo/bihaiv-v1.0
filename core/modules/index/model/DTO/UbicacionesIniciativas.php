<?php
class UbicacionesIniciativas
{
    
    public $id;
    public $idIniciativa;
    public $idPais;
    public $idEstado;
    public $idMunicipio;
    public $idLocalidad;
    public $is_main;

    function __construct() 
    {
        $this->idIniciativa = "";
        $this->idPais = "";
        $this->idEstado = "";
        $this->idMunicipio = "";
        $this->idLocalidad = "";
        $this->is_main = "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getIdPais() 
    {
        return $this->idPais;
    }

    function getIdEstado() 
    {
        return $this->idEstado;
    }

    function getIdMunicipio() 
    {
        return $this->idMunicipio;
    }

    function getIdLocalidad() 
    {
        return $this->idLocalidad;
    }

    function getIdIniciativa() 
    {
        return $this->idIniciativa;
    }
    
    function getIsMain() 
    {
        return $this->is_main;
    }
    

    function setId($id) 
    {
        $this->id = $id;
    }

    function setIdPais($idPais) 
    {
        $this->idPais = $idPais;
    }

    function setIdEstado($idEstado) 
    {
        $this->idEstado = $idEstado;
    }

    function setIdMunicipio($idMunicipio) 
    {
        $this->idMunicipio = $idMunicipio;
    }

    function setIdLocalidad($idLocalidad) 
    {
        $this->idLocalidad = $idLocalidad;
    }

    function setIdIniciativa($idIniciativa) 
    {
        $this->idIniciativa = $idIniciativa;
    }

    function setIsMain($is_main) 
    {
        $this->is_main = $is_main;
    }


}
