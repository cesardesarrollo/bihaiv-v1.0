<?php
class CalificacionEnfoque
{
    
    public $id;
    public $calificacion;
    public $fechaCalificacion;
    public $Enfoque_idEnfoque;
    public $Usuario_id;
    
    function __construct() 
    {
        $this->id = "";
        $this->calificacion = "";
        $this->fechaCalificacion = "";
        $this->Enfoque_idEnfoque = "";
        $this->Usuario_id = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getCalificacion() 
    {
        return $this->calificacion;
    }

    function getFechaCalificacion() 
    {
        return $this->fechaCalificacion;
    }

    function getEnfoque_idEnfoque() 
    {
        return $this->Enfoque_idEnfoque;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setCalificacion($calificacion) 
    {
        $this->calificacion = $calificacion;
    }

    function setFechaCalificacion($fechaCalificacion) 
    {
        $this->fechaCalificacion = $fechaCalificacion;
    }

    function setEnfoque_idEnfoque($Enfoque_idEnfoque) 
    {
        $this->Enfoque_idEnfoque = $Enfoque_idEnfoque;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }


}
