<?php
class CategoriaAportacion
{

    public $id;
    public $titulo;
    public $activo;
    public $idPadre;
    public $idSeccion;

    function __construct() 
    {
        $this->id = 0;
        $this->titulo = "";
        $this->activo = 1;
        $this->idPadre = 0;
        $this->idSeccion = 0;
    }

    function getId() 
    {
        return $this->id;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getActivo() 
    {
        return $this->activo;
    }

    function getIdPadre() 
    {
        return $this->idPadre;
    }

    function getIdSeccion() 
    {
        return $this->idSeccion;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setActivo($activo) 
    {
        $this->activo = $activo;
    }

    function setIdPadre($idPadre) 
    {
        $this->idPadre = $idPadre;
    }

    function setIdSeccion($idSeccion) 
    {
        $this->idSeccion = $idSeccion;
    }
}
