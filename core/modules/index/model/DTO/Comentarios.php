<?php
class Comentarios
{
    
    public $id;
    public $id_usu;
    public $id_post;
    public $texto;
    public $fecha_creado;
    
    function __construct() 
    {
        $this->id = "";
        $this->id_usu =  "";
        $this->id_post =  "";
        $this->texto =  "";
        $this->fecha_creado =  "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getId_usu() 
    {
        return $this->id_usu;
    }

    function getId_post() 
    {
        return $this->id_post;
    }

    function getTexto() 
    {
        return $this->texto;
    }

    function getFecha_creado() 
    {
        return $this->fecha_creado;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setId_usu($id_usu) 
    {
        $this->id_usu = $id_usu;
    }

    function setId_post($id_post) 
    {
        $this->id_post = $id_post;
    }

    function setTexto($texto) 
    {
        $this->texto = $texto;
    }

    function setFecha_creado($fecha_creado) 
    {
        $this->fecha_creado = $fecha_creado;
    }



}
