<?php
class Oauth
{
    
    public $id;
    public $servicio;
    public $clientId;
    public $clientSecret;
    public $apiKey;
    public $urlRed;
    public $accessToken;
    public $accessTokenSecret;
    public $refreshToken;
    public $user_id;
    
    function __construct() 
    {
        $this->id = "";
        $this->servicio = "";
        $this->clientId = "";
        $this->clientSecret = "";
        $this->apiKey = "";
        $this->urlRed = "";
        $this->accessToken = "";
        $this->accessTokenSecret = "";
        $this->refreshToken = "";
        $this->user_id = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getServicio() 
    {
        return $this->servicio;
    }

    function getClientId() 
    {
        return $this->clientId;
    }

    function getClientSecret() 
    {
        return $this->clientSecret;
    }

    function getApiKey() 
    {
        return $this->apiKey;
    }

    function getUrlRed() 
    {
        return $this->urlRed;
    }

    function getAccessToken() 
    {
        return $this->accessToken;
    }

    function getAccessTokenSecret() 
    {
        return $this->accessTokenSecret;
    }

    function getRefreshToken() 
    {
        return $this->refreshToken;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setServicio($servicio) 
    {
        $this->servicio = $servicio;
    }
    function getUsuario_id() 
    {
        return $this->user_id;
    }

    function setUsuario_id($user_id) 
    {
        $this->user_id = $user_id;
    }

    
    function setClientId($clientId) 
    {
        $this->clientId = $clientId;
    }

    function setClientSecret($clientSecret) 
    {
        $this->clientSecret = $clientSecret;
    }

    function setApiKey($apiKey) 
    {
        $this->apiKey = $apiKey;
    }

    function setUrlRed($urlRed) 
    {
        $this->urlRed = $urlRed;
    }

    function setaccessToken($accessToken) 
    {
        $this->accessToken = $accessToken;
    }

    function setAccessTokenSecret($accessTokenSecret) 
    {
        $this->accessTokenSecret = $accessTokenSecret;
    }

    function setRefreshToken($refreshToken) 
    {
        $this->refreshToken = $refreshToken;
    }


}
