<?php

class Experto
{

    public $id;
    public $ubicacion;
    public $organizacion;
    public $tipoOrganizacion;
    public $rangoExperiencia;
    public $aniosExperiencia;
    public $cargoActual;
    public $cargoAnterior;
    public $bibliografia;
    public $Usuarios_idUsuarios;
    public $nonce;
    public $acepto;
    public $lat;
    public $lng;
    public $telefono;

    function __construct() 
    {
        $this->id = "";
        $this->ubicacion = "";
        $this->organizacion = "";
        $this->tipoOrganizacion = "";
        $this->rangoExperiencia = "";
        $this->aniosExperiencia = "";
        $this->cargoActual = "";
        $this->cargoAnterior = "";
        $this->bibliografia = "";
        $this->Usuarios_idUsuarios = "";
        $this->nonce = "";
        $this->acepto = "";
        $this->lat = "";
        $this->lng = "";
        $this->telefono = "";
    }

    function getTelefono() 
    {
        return $this->telefono;
    }

    function setTelefono($telefono) 
    {
        $this->telefono = $telefono;
    }

    function getNonce() 
    {
        return $this->nonce;
    }

    function getAcepto() 
    {
        return $this->acepto;
    }

    function getLat() 
    {
        return $this->lat;
    }

    function getLng() 
    {
        return $this->lng;
    }

    function setNonce($nonce) 
    {
        $this->nonce = $nonce;
    }

    function setAcepto($acepto) 
    {
        $this->acepto = $acepto;
    }

    function setLat($lat) 
    {
        $this->lat = $lat;
    }

    function setLng($lng) 
    {
        $this->lng = $lng;
    }

    function getId() 
    {
        return $this->id;
    }

    function getUbicacion() 
    {
        return $this->ubicacion;
    }

    function getOrganizacion() 
    {
        return $this->organizacion;
    }

    function getTipoOrganizacion() 
    {
        return $this->tipoOrganizacion;
    }

    function getRangoExperiencia() 
    {
        return $this->rangoExperiencia;
    }

    function getAniosExperiencia() 
    {
        return $this->aniosExperiencia;
    }

    function getCargoActual() 
    {
        return $this->cargoActual;
    }

    function getCargoAnterior() 
    {
        return $this->cargoAnterior;
    }

    function getBibliografia() 
    {
        return $this->bibliografia;
    }

    function getUsuarios_idUsuarios() 
    {
        return $this->Usuarios_idUsuarios;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setUbicacion($ubicacion) 
    {
        $this->ubicacion = $ubicacion;
    }

    function setOrganizacion($organizacion) 
    {
        $this->organizacion = $organizacion;
    }

    function setTipoOrganizacion($tipoOrganizacion) 
    {
        $this->tipoOrganizacion = $tipoOrganizacion;
    }

    function setRangoExperiencia($rangoExperiencia) 
    {
        $this->rangoExperiencia = $rangoExperiencia;
    }

    function setAniosExperiencia($aniosExperiencia) 
    {
        $this->aniosExperiencia = $aniosExperiencia;
    }

    function setCargoActual($cargoActual) 
    {
        $this->cargoActual = $cargoActual;
    }

    function setCargoAnterior($cargoAnterior) 
    {
        $this->cargoAnterior = $cargoAnterior;
    }

    function setBibliografia($bibliografia) 
    {
        $this->bibliografia = $bibliografia;
    }

    function setUsuarios_idUsuarios($Usuarios_idUsuarios) 
    {
        $this->Usuarios_idUsuarios = $Usuarios_idUsuarios;
    }

}
