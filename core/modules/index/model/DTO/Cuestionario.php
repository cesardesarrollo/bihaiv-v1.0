<?php
class Cuestionario
{
    
    public $id;
    public $nombre;
    public $nivel;
    public $fechaCreacion;
    public $activo;

    function __construct() 
    {
        $this->id = "";
        $this->nombre = "";
        $this->nivel = "";
        $this->fechaCreacion = "";
        $this->activo = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getNombre() 
    {
        return $this->nombre;
    }

    function getNivel() 
    {
        return $this->nivel;
    }

    function getFechaCreacion() 
    {
        return $this->fechaCreacion;
    }

    function getActivo() 
    {
        return $this->activo;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }

    function setNivel($nivel) 
    {
        $this->nivel = $nivel;
    }

    function setFechaCreacion($fechaCreacion) 
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    function setActivo($activo) 
    {
        $this->activo = $activo;
    }


    
}
