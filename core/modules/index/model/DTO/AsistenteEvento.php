<?php
class AsistenteEvento
{
    
    public $id;
    public $fechaCreado;
    public $estatus;
    public $Evento_idEvento;
    public $user_id;
    
    function __construct() 
    {
        $this->id = "";
        $this->fechaCreado = date('Y-m-d H:i:s');
        $this->estatus = "";
        $this->Evento_idEvento = "";
        $this->user_id = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getFechaCreado() 
    {
        return $this->fechaCreado;
    }

    function getEstatus() 
    {
        return $this->estatus;
    }

    function getEvento_idEvento() 
    {
        return $this->Evento_idEvento;
    }

    function getUser_id() 
    {
        return $this->user_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setFechaCreado($fechaCreado) 
    {
        $this->fechaCreado = $fechaCreado;
    }

    function setEstatus($estatus) 
    {
        $this->estatus = $estatus;
    }

    function setEvento_idEvento($Evento_idEvento) 
    {
        $this->Evento_idEvento = $Evento_idEvento;
    }

    function setUser_id($user_id) 
    {
        $this->user_id = $user_id;
    }


   
}
