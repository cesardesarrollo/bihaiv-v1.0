<?php
class Localidades
{
    
    public $id;
    public $nombre;

    function __construct() 
    {
        $this->id = "";
        $this->nombre = "";
        $this->clave = "";
        $this->latitud = "";
        $this->longitud = "";
        $this->lat = "";
        $this->lng = "";
        $this->altitud = "";
        $this->activo = "";
        $this->municipio_id = "";
    }

    function getId() 
    {
        return $this->id;
    }
    function getNombre() 
    {
        return $this->nombre;
    }
    function getClave() 
    {
        return $this->clave;
    }
    function getLatitud() 
    {
        return $this->latitud;
    }
    function getLongitud() 
    {
        return $this->longitud;
    }
    function getLat() 
    {
        return $this->lat;
    }
    function getLng() 
    {
        return $this->lng;
    }
    function getAltitud() 
    {
        return $this->altitud;
    }
    function getActivo() 
    {
        return $this->activo;
    }
    function getMunicipioId() 
    {
        return $this->municipio_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }
    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }
    function setClave($clave) 
    {
        $this->clave = $clave;
    }
    function setLatitud($latitud) 
    {
        $this->latitud = $latitud;
    }
    function setLongitud($longitud) 
    {
        $this->longitud = $longitud;
    }
    function setLat($lat) 
    {
        $this->lat = $lat;
    }
    function setLng($lng) 
    {
        $this->lng = $lng;
    }
    function setAltitud($altitud) 
    {
        $this->altitud = $altitud;
    }
    function setActivo($activo) 
    {
        $this->activo = $activo;
    }
    function setMunicipioId($municipio_id) 
    {
        $this->municipio_id = $municipio_id;
    }

}
    
