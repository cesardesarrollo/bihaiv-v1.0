<?php
class Evento
{

    public $id;
    public $titulo;
    public $textoCorto;
    public $textoLargo;
    public $creado;
    public $fechaEvento;
    public $fechaFin;
    public $horaInicio;
    public $horaFin;
    public $lugar;
    public $llaveImg;
    public $Usuario_id;
    public $acceso;
    public $url;
    public $tipoEvento;
    public $lat;
    public $lng;
    public $iniciativa_id;

    public $actorName;
    public $actorId;

    function __construct() 
    {
        $this->id = "";
        $this->titulo = "";
        $this->textoCorto = "";
        $this->textoLargo = "";
        $this->creado = "";
        $this->fechaEvento = "";
        $this->fechaFin = "";
        $this->horaInicio = "";
        $this->horaFin = "";
        $this->lugar = "";
        $this->llaveImg = "";
        $this->Usuario_id = "";
        $this->acceso = "";
        $this->url = "";
        $this->tipoEvento = "";
        $this->lat = "";
        $this->lng = "";
        $this->iniciativa_id = 0;

        $this->actorName = '';
        $this->actorId = '';
    }

    function getId() 
    {
        return $this->id;
    }

    function getLat() 
    {
        return $this->lat;
    }

    function getLng() 
    {
        return $this->lng;
    }

    function getAcceso() 
    {
        return $this->acceso;
    }

    function getTipoEvento() 
    {
        return $this->tipoEvento;
    }

    function getUrl() 
    {
        return $this->url;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getTextoCorto() 
    {
        return $this->textoCorto;
    }

    function getTextoLargo() 
    {
        return $this->textoLargo;
    }

    function getCreado() 
    {
        return $this->creado;
    }

    function getFechaEvento() 
    {
        return $this->fechaEvento;
    }

    function getFechaFin() 
    {
        return $this->fechaFin;
    }

    function getHoraInicio() 
    {
        return $this->horaInicio;
    }

    function getHoraFin() 
    {
        return $this->horaFin;
    }

    function getLugar() 
    {
        return $this->lugar;
    }

    function getLlaveImg() 
    {
        return $this->llaveImg;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function getTituloId() 
    {
        return $this->titulo."-".$this->id;
    }

    function getIniciativaId() 
    {
        return $this->iniciativa_id;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setLat($lat) 
    {
        $this->lat = $lat;
    }

    function setLng($lng) 
    {
        $this->lng = $lng;
    }

    function setAcceso($acceso) 
    {
        $this->acceso = $acceso;
    }

    function setTipoEvento($tipoEvento) 
    {
        $this->tipoEvento = $tipoEvento;
    }

    function setUrl($url) 
    {
        $this->url = $url;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setTextoCorto($textoCorto) 
    {
        $this->textoCorto = $textoCorto;
    }

    function setTextoLargo($textoLargo) 
    {
        $this->textoLargo = $textoLargo;
    }

    function setCreado($creado) 
    {
        $this->creado = $creado;
    }

    function setFechaEvento($fechaEvento) 
    {
        $this->fechaEvento = $fechaEvento;
    }

    function setFechaFin($fechaFin) 
    {
        $this->fechaFin = $fechaFin;
    }

    function setHoraInicio($horaInicio) 
    {
        $this->horaInicio = $horaInicio;
    }

    function setHoraFin($horaFin) 
    {
        $this->horaFin = $horaFin;
    }

    function setLugar($lugar) 
    {
        $this->lugar = $lugar;
    }

    function setLlaveImg($llaveImg) 
    {
        $this->llaveImg = $llaveImg;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }

    function setIniciativaId($iniciativa_id) 
    {
        $this->iniciativa_id = $iniciativa_id;
    }
    function setActorName($text) 
    {
        $this->actorName = utf8_decode($text);
    }

    function setActorId($text) 
    {
        $this->actorId = $text;
    }
}
