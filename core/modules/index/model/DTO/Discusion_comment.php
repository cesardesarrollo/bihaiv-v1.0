<?php
class Comment
{
    public $id;
    public $comentario;
    public $created_at;
    public $updated_at;
    public $tema_id;
    public $organizacion_id;

    function __construct() 
    {
        $this -> id = null;
        $this -> comentario = null;
        $this -> created_at = date('Y-m-d H:i:s');
        $this -> updated_at = date('Y-m-d H:i:s');
        $this -> tema_id = null;
        $this -> organizacion_id = null;
    }
    // GET
    function getId() 
    {
        return $this->id;
    }

    function getComentario() 
    {
        return $this -> comentario;
    }

    function getCreated_at() 
    {
        return $this -> created_at;
    }

    function getUpdated_at() 
    {
        return $this -> updated_at;
    }

    function getTema_id() 
    {
        return $this -> tema_id;
    }

    function getOrganizacion_id() 
    {
        return $this -> organizacion_id;
    }
    // SET
    function setId($id)
    {
        $this -> id = $id;
    }
    function setComentario($comentario)
    {
        $this -> comentario = $comentario;
    }
    function setOrganizacion_id($organizacion_id)
    {
        $this -> organizacion_id = $organizacion_id;
    }
    function setTema_id($tema_id)
    {
        $this -> tema_id = $tema_id;
    }

}
