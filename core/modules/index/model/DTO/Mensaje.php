<?php
class Mensaje
{
    
    public $id;
    public $texto;
    public $fecha_creado;
    public $fecha_leido;
    public $user_id_sender;
    public $user_id_receiver;
    
    function __construct() 
    {
        $this->id = "";
        $this->texto = "";
        $this->fecha_creado = "";
        $this->fecha_leido = "";
        $this->user_id_sender = "";
        $this->user_id_receiver = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getTexto() 
    {
        return $this->texto;
    }

    function getFecha_creado() 
    {
        return $this->fecha_creado;
    }

    function getFecha_leido() 
    {
        return $this->fecha_leido;
    }

    function getUser_id_sender() 
    {
        return $this->user_id_sender;
    }

    function getUser_id_receiver() 
    {
        return $this->user_id_receiver;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTexto($texto) 
    {
        $this->texto = $texto;
    }

    function setFecha_creado($fecha_creado) 
    {
        $this->fecha_creado = $fecha_creado;
    }

    function setFecha_leido($fecha_leido) 
    {
        $this->fecha_leido = $fecha_leido;
    }

    function setUser_id_sender($user_id_sender) 
    {
        $this->user_id_sender = $user_id_sender;
    }

    function setUser_id_receiver($user_id_receiver) 
    {
        $this->user_id_receiver = $user_id_receiver;
    }


    
    

}
