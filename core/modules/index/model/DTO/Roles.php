<?php
class Roles
{
    
    public $id;
    public $nombre;
    public $descripcion;
    public $description;
    public $descripcioncorta;
    public $shortdescription;
    public $activo;
    public $imagen;

    function __construct() 
    {
        $this->id = "";
        $this->nombre = "";
        $this->descripcion ="";
        $this->description ="";
        $this->descripcioncorta ="";
        $this->shortdescription ="";
        $this->activo = "";
        $this->imagen = "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getNombre() 
    {
        return $this->nombre;
    }

    function getDescripcion() 
    {
        return $this->descripcion;
    }
    function getDescription() 
    {
        return $this->description;
    }
    function getDescripcionCorta() 
    {
        return $this->descripcioncorta;
    }
    function getShortDescription() 
    {
        return $this->shortdescription;
    }

    function getActivo() 
    {
        return $this->activo;
    }

    function getImagen() 
    {
        return $this->imagen;
    }
    function setId($id) 
    {
        $this->id = $id;
    }

    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }
    
    function setDescription($description) 
    {
        $this->description = $description;
    }
    function setDescripcionCorta($descripcioncorta) 
    {
        $this->descripcioncorta = $descripcioncorta;
    }
    function setShortDescription($shortdescription) 
    {
        $this->shortdescription = $shortdescription;
    }

    function setActivo($activo) 
    {
        $this->activo = $activo;
    }

    function setImagen($imagen) 
    {
        $this->imagen = $imagen;
    }




}
