<?php
class UbicacionesEventos
{
    
    public $id;
    public $idEvento;
    public $idPais;
    public $idEstado;
    public $idMunicipio;
    public $idLocalidad;

    function __construct() 
    {
        $this->idEvento = "";
        $this->idPais = "";
        $this->idEstado = "";
        $this->idMunicipio = "";
        $this->idLocalidad = "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getIdPais() 
    {
        return $this->idPais;
    }

    function getIdEstado() 
    {
        return $this->idEstado;
    }

    function getIdMunicipio() 
    {
        return $this->idMunicipio;
    }

    function getIdLocalidad() 
    {
        return $this->idLocalidad;
    }
    function getIdEvento() 
    {
        return $this->idEvento;
    }
    

    function setId($id) 
    {
        $this->id = $id;
    }

    function setIdPais($idPais) 
    {
        $this->idPais = $idPais;
    }

    function setIdEstado($idEstado) 
    {
        $this->idEstado = $idEstado;
    }

    function setIdMunicipio($idMunicipio) 
    {
        $this->idMunicipio = $idMunicipio;
    }

    function setIdLocalidad($idLocalidad) 
    {
        $this->idLocalidad = $idLocalidad;
    }
    function setIdEvento($idEvento) 
    {
        $this->idEvento = $idEvento;
    }


}
