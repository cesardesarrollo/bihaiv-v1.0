<?php
class Iniciativa
{
    
    public $id;
    public $titulo;
    public $textoCorto;
    public $textoLargo;
    public $fechaIniciativa;
    public $fechaFin;
    public $horaIni;
    public $horaFin;
    public $llaveImg;
    public $lugar;
    public $Post_idPost;
    public $idAlcanze;
    
    function __construct() 
    {
        $this->id = "";
        $this->titulo = "";
        $this->textoCorto = "";
        $this->textoLargo = "";
        $this->fechaIniciativa = "";
        $this->fechaFin = "";
        $this->horaIni = "";
        $this->horaFin = "";
        $this->llaveImg = "";
        $this->lugar = "";
        $this->Post_idPost = "";
        $this->idAlcanze = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getTitulo() 
    {
        return $this->titulo;
    }

    function getTextoCorto() 
    {
        return $this->textoCorto;
    }

    function getTextoLargo() 
    {
        return $this->textoLargo;
    }

    function getFechaIniciativa() 
    {
        return $this->fechaIniciativa;
    }

    function getHoraIni() 
    {
        return $this->horaIni;
    }

    function getHoraFin() 
    {
        return $this->horaFin;
    }

    function getLlaveImg() 
    {
        return $this->llaveImg;
    }

    function getLugar() 
    {
        return $this->lugar;
    }

    function getPost_idPost() 
    {
        return $this->Post_idPost;
    }

    function getIdAlcanze() 
    {
        return $this->idAlcanze;
    }

    function getFechaFin() 
    {
        return $this->fechaFin;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setTitulo($titulo) 
    {
        $this->titulo = $titulo;
    }

    function setTextoCorto($textoCorto) 
    {
        $this->textoCorto = $textoCorto;
    }

    function setTextoLargo($textoLargo) 
    {
        $this->textoLargo = $textoLargo;
    }

    function setFechaIniciativa($fechaIniciativa) 
    {
        $this->fechaIniciativa = $fechaIniciativa;
    }

    function setHoraIni($horaIni) 
    {
        $this->horaIni = $horaIni;
    }

    function setHoraFin($horaFin) 
    {
        $this->horaFin = $horaFin;
    }

    function setLlaveImg($llaveImg) 
    {
        $this->llaveImg = $llaveImg;
    }

    function setLugar($lugar) 
    {
        $this->lugar = $lugar;
    }

    function setPost_idPost($Post_idPost) 
    {
        $this->Post_idPost = $Post_idPost;
    }

    function setIdAlcanze($idAlcanze) 
    {
        $this->idAlcanze = $idAlcanze;
    }

    function setFechaFin($fechaFin) 
    {
        $this->fechaFin = $fechaFin;
    }

}
