<?php
class RedesExperto
{
    
    public $id;
    public $nombre;
    public $url;
    public $experto_idExperto;
    
    function __construct() 
    {
        $this->id = "";
        $this->nombre = "";
        $this->url = "";
        $this->experto_idExperto = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getNombre() 
    {
        return $this->nombre;
    }

    function getUrl() 
    {
        return $this->url;
    }

    function getExperto_idExperto() 
    {
        return $this->experto_idExperto;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setNombre($nombre) 
    {
        $this->nombre = $nombre;
    }

    function setUrl($url) 
    {
        $this->url = $url;
    }

    function setExperto_idExperto($experto_idExperto) 
    {
        $this->experto_idExperto = $experto_idExperto;
    }


    
    
}
