<?php
class Objetivo
{
    
    public $id;
    public $descripcion;

    function __construct() 
    {
        $this->id = "";
        $this->descripcion = "";
    }
    
    function getId() 
    {
        return $this->id;
    }
    
    function getDescripcion() 
    {
        return $this->descripcion;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }
}
