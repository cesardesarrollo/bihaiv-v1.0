<?php
class Likes
{
    
    public $id;
    public $id_pos;
    public $id_usu;
    public $fecha_creado;
    
    function __construct() 
    {
        $this->id = "";
        $this->id_pos =  "";
        $this->id_usu =  "";
        $this->fecha_creado =  "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getId_pos() 
    {
        return $this->id_pos;
    }

    function getId_usu() 
    {
        return $this->id_usu;
    }

    function getFecha_creado() 
    {
        return $this->fecha_creado;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setId_pos($id_pos) 
    {
        $this->id_pos = $id_pos;
    }

    function setId_usu($id_usu) 
    {
        $this->id_usu = $id_usu;
    }

    function setFecha_creado($fecha_creado) 
    {
        $this->fecha_creado = $fecha_creado;
    }



    
    
}
