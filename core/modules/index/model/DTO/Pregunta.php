<?php
class Pregunta
{
    
    public $id;
    public $Pregunta;
    public $Orden;
    public $Cuestionario_idCuestionario;

    function __construct() 
    {
        $this->id = "";
        $this->Pregunta = "";
        $this->Orden = "";
        $this->Cuestionario_idCuestionario = "";
    }
    
    function getId() 
    {
        return $this->id;
    }

    function getPregunta() 
    {
        return $this->Pregunta;
    }

    function getOrden() 
    {
        return $this->Orden;
    }

    function getCuestionario_idCuestionario() 
    {
        return $this->Cuestionario_idCuestionario;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setPregunta($Pregunta) 
    {
        $this->Pregunta = $Pregunta;
    }

    function setOrden($Orden) 
    {
        $this->Orden = $Orden;
    }

    function setCuestionario_idCuestionario($Cuestionario_idCuestionario) 
    {
        $this->Cuestionario_idCuestionario = $Cuestionario_idCuestionario;
    }




    
}
