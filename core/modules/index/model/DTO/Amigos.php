<?php
class Amigos
{
    
    public $id;
    public $user_id_sender;
    public $user_id_receiver;
    public $estatus_aceptar;
    public $fechaAceptacion;
    public $fechaSolicitud;
    
    function __construct() 
    {
        $this->id = "";
        $this->user_id_sender = "";
        $this->user_id_receiver = "";
        $this->estatus_aceptar = "";
        $this->fechaAceptacion = "";
        $this->fechaSolicitud = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getUser_id_sender() 
    {
        return $this->user_id_sender;
    }

    function getUser_id_receiver() 
    {
        return $this->user_id_receiver;
    }

    function getEstatus_aceptar() 
    {
        return $this->estatus_aceptar;
    }

    function getFechaAceptacion() 
    {
        return $this->fechaAceptacion;
    }

    function getFechaSolicitud() 
    {
        return $this->fechaSolicitud;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setUser_id_sender($user_id_sender) 
    {
        $this->user_id_sender = $user_id_sender;
    }

    function setUser_id_receiver($user_id_receiver) 
    {
        $this->user_id_receiver = $user_id_receiver;
    }

    function setEstatus_aceptar($estatus_aceptar) 
    {
        $this->estatus_aceptar = $estatus_aceptar;
    }

    function setFechaAceptacion($fechaAceptacion) 
    {
        $this->fechaAceptacion = $fechaAceptacion;
    }

    function setFechaSolicitud($fechaSolicitud) 
    {
        $this->fechaSolicitud = $fechaSolicitud;
    }


}
