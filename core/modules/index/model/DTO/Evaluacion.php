<?php
class Evaluacion
{
    
    public $id;
    public $idCuestionariosUsuario;
    public $Usuario_id;
    public $fechaEvaluacion;
    public $respuesta;
    public $Comentarios;
    public $Confidencialidad;
    
    function __construct() 
    {
        $this->id = "";
        $this->idCuestionariosUsuario = "";
        $this->Usuario_id = "";
        $this->fechaEvaluacion = "";
        $this->respuesta = "";
        $this->Comentarios = "";
        $this->Confidencialidad = "";
    }

    function getId() 
    {
        return $this->id;
    }

    function getIdCuestionariosUsuario() 
    {
        return $this->idCuestionariosUsuario;
    }

    function getUsuario_id() 
    {
        return $this->Usuario_id;
    }

    function getFechaEvaluacion() 
    {
        return $this->fechaEvaluacion;
    }

    function getRespuesta() 
    {
        return $this->respuesta;
    }

    function getComentarios() 
    {
        return $this->Comentarios;
    }

    function getConfidencialidad() 
    {
        return $this->Confidencialidad;
    }

    function setId($id) 
    {
        $this->id = $id;
    }

    function setIdCuestionariosUsuario($idCuestionariosUsuario) 
    {
        $this->idCuestionariosUsuario = $idCuestionariosUsuario;
    }

    function setUsuario_id($Usuario_id) 
    {
        $this->Usuario_id = $Usuario_id;
    }

    function setFechaEvaluacion($fechaEvaluacion) 
    {
        $this->fechaEvaluacion = $fechaEvaluacion;
    }

    function setRespuesta($respuesta) 
    {
        $this->respuesta = $respuesta;
    }

    function setComentarios($Comentarios) 
    {
        $this->Comentarios = $Comentarios;
    }

    function setConfidencialidad($Confidencialidad) 
    {
        $this->Confidencialidad = $Confidencialidad;
    }




   
}
