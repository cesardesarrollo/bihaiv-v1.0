<?php
require_once 'DTO/Base.php';
require_once 'DTO/OpcionesRespuesta.php';

class DaoOpcionesRespuesta extends base
{
    
    public $tableName="opcionesrespuesta"; 

    public function add(OpcionesRespuesta $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (Texto, orden, Pregunta_idPregunta, Roles_idRol) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getOrden(), "int"),
              $this->GetSQLValueString($x->getPregunta_idPregunta(), "int"),
              $this->GetSQLValueString($x->getRoles_idRol(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(OpcionesRespuesta $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET Texto=%s, orden=%s, Pregunta_idPregunta=%s, Roles_idRol=%s  WHERE idOpcionesRespuesta = %s",
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getOrden(), "int"),
              $this->GetSQLValueString($x->getPregunta_idPregunta(), "int"),
              $this->GetSQLValueString($x->getRoles_idRol(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idOpcionesRespuesta=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idOpcionesRespuesta= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new OpcionesRespuesta();
        $x->setId($row['idOpcionesRespuesta']);
        $x->setTexto($row['Texto']);
        $x->setOrden($row['orden']);
        $x->setPregunta_idPregunta($row['Pregunta_idPregunta']); 
        $x->setRoles_idRol($row['Roles_idRol']);
        return $x;
    }
    
    public function getOpcionesByIdPregunta($Id_pre)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Pregunta_idPregunta=".$Id_pre." ORDER BY orden ASC";
         return $this->advancedQueryByObjetc($query);
    }
    
    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
            return $resp;
    }
    
    public function deleteByIdPre($Id_pre)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE Pregunta_idPregunta=".$Id_pre); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }
     
}


