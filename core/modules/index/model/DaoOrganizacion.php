<?php
require_once 'DTO/Base.php';
require_once 'DTO/Organizacion.php';

class DaoOrganizacion extends base
{

    public $tableName="organizacion";

    public function add(Organizacion $x)
    {

          $query=sprintf(
              "INSERT INTO ".$this->tableName." (Roles_idRol, telefono, urlWeb, ubicacion, mision, vision, quienSoy, lat, lng, idAlcanze, avatar, portada, Usuarios_idUsuarios) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getRoles_idRol(), "int"),
              $this->GetSQLValueString($x->getTelefono(), "text"),
              $this->GetSQLValueString($x->getUrlWeb(), "text"),
              $this->GetSQLValueString($x->getUbicacion(), "text"),
              $this->GetSQLValueString($x->getMision(), "text"),
              $this->GetSQLValueString($x->getVision(), "text"),
              $this->GetSQLValueString($x->getQuienSoy(), "text"),
              $this->GetSQLValueString($x->getLat(), "text"),
              $this->GetSQLValueString($x->getLng(), "text"),
              $this->GetSQLValueString($x->getIdAlcanze(), "int"),
              $this->GetSQLValueString($x->getAvatar(), "text"),
              $this->GetSQLValueString($x->getPortada(), "text"),
              $this->GetSQLValueString($x->getUsuarios_idUsuarios(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }


    public function update(Organizacion $x)
    {

          $query=sprintf(
              "UPDATE ".$this->tableName." SET Roles_idRol=%s, telefono=%s, urlWeb=%s, ubicacion=%s, mision=%s, vision=%s, quienSoy=%s, lat=%s, lng=%s, idAlcanze=%s, Avatar=%s, Portada=%s, Usuarios_idUsuarios=%s  WHERE id = %s",
              $this->GetSQLValueString($x->getRoles_idRol(), "int"),
              $this->GetSQLValueString($x->getTelefono(), "text"),
              $this->GetSQLValueString($x->getUrlWeb(), "text"),
              $this->GetSQLValueString($x->getUbicacion(), "text"),
              $this->GetSQLValueString($x->getMision(), "text"),
              $this->GetSQLValueString($x->getVision(), "text"),
              $this->GetSQLValueString($x->getQuienSoy(), "text"),
              $this->GetSQLValueString($x->getLat(), "text"),
              $this->GetSQLValueString($x->getLng(), "text"),
              $this->GetSQLValueString($x->getIdAlcanze(), "int"),
              $this->GetSQLValueString($x->getAvatar(), "text"),
              $this->GetSQLValueString($x->getPortada(), "text"),
              $this->GetSQLValueString($x->getUsuarios_idUsuarios(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);

        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByUserId($id)
    {
        $query = "select * from organizacion where Usuarios_idUsuarios = " . $id;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al consultar organizacion: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getAllByNoRolId($id)
    {
        $query="SELECT o.id, o.Roles_idRol, o.telefono, o.urlWeb, o.ubicacion, o.mision, o.vision, o.quienSoy, o.lat, o.lng, o.avatar, o.portada, o.idAlcanze, o.Usuarios_idUsuarios FROM ".$this->tableName." as o INNER JOIN user as u ON u.id = o.Usuarios_idUsuarios WHERE u.is_valid=1 AND u.is_active=1 AND u.activo=1 AND o.Roles_idRol!=".$id;
        return $this->advancedQueryByObjetc($query);
    }

    public function getAllByRolId($id)
    {
        $query="SELECT o.id, o.Roles_idRol, o.telefono, o.urlWeb, o.ubicacion, o.mision, o.vision, o.quienSoy, o.lat, o.lng, o.avatar, o.portada, o.idAlcanze, o.Usuarios_idUsuarios FROM ".$this->tableName." as o INNER JOIN user as u ON u.id = o.Usuarios_idUsuarios WHERE u.is_valid=1 AND u.is_active=1 AND u.activo=1 AND o.Roles_idRol=".$id;
        return $this->advancedQueryByObjetc($query);
    }


    public function createObject($row)
    {
        $x = new Organizacion();
        $x->setId($row['id']);
        $x->setRoles_idRol($row['Roles_idRol']);
        $x->setTelefono($row['telefono']);
        $x->setUrlWeb($row['urlWeb']);
        $x->setUbicacion($row['ubicacion']);
        $x->setMision($row['mision']);
        $x->setVision($row['vision']);
        $x->setQuienSoy($row['quienSoy']);
        $x->setLat($row['lat']);
        $x->setLng($row['lng']);
        $x->setAvatar($row['avatar']);
        $x->setPortada($row['portada']);
        $x->setIdAlcanze($row['idAlcanze']);
        $x->setUsuarios_idUsuarios($row['Usuarios_idUsuarios']);
        return $x;
    }

    public function getAll($offset=null,$limit=null,$buscar=null)
    {
        $query="";
        if ($offset===0) {
            $query=" LIMIT ".$limit;
        }
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" AND (user.name LIKE '%".$buscar."%' OR user.email LIKE '".$buscar."')";
        }

        $query="SELECT * FROM ".$this->tableName."
               JOIN user ON ".$this->tableName.".Usuarios_idUsuarios=user.id
               WHERE user.is_active=1 and user.is_valid=1 and user.activo=1  AND user.tipoRel=1
                     ".$queryBuscar." ".$query;
        return $this->advancedQueryByObjetc($query);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }
    public function updateRating($data)
    {
        $query = "INSERT INTO `organizacion_has_calificacion` (`calificacion`, `organizacion_id`, `organizacion_calificada_id`) VALUES (".$data['calificacion'].", ".$data['organizacion_id'].", ".$data['organizacion_calificada_id'].") ON DUPLICATE KEY UPDATE calificacion = ".$data['calificacion'];
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            /* Validamos que nuestra publicacion cumpla con los requisitos para
            * aparecer en nuestra base datos publica, en caso de que se cumplan los
            * requisitos se actualiza la bandera en la tabla publicaciones.
            */
            return $Result1;
        }
    }
    public function getRating($data)
    {
        $sql = "SELECT
            calificacion, organizacion_id, organizacion_calificada_id
        FROM
            organizacion_has_calificacion
        WHERE
            organizacion_id = ".$data['organizacion_id']."
      AND organizacion_calificada_id = ".$data['organizacion_calificada_id'];
        if ($resultSet =  $this -> getOneRow($sql) ) {
            $this -> response['class']  = 'success';
            $this -> response['msg']    = 'calificacion recuperada con éxito';
            $this -> response['calificacion'] = $resultSet['calificacion'];
            return true;
        } else {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Ocurrio un error inesperado';
            return false;
        }
    }
}
