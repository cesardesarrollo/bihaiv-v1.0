<?php
require_once 'DTO/Base.php';
require_once 'DTO/Comentarios.php';

class DaoComentarios extends base
{
    
    public $tableName="comentarios"; 

    public function add(Comentarios $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (id_usu, id_post, texto, fecha_creado) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getId_post(), "int"),
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getFecha_creado(), "date")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }

    public function update(Comentarios $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET id_usu=%s, id_post=%s, texto=%s, fecha_creado=%s  WHERE id_com = %s",
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getId_post(), "int"),
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id_com=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id_com= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function createObject($row)
    {
        $x = new Comentarios();
        $x->setId($row['id_com']);
        $x->setId_usu($row['id_usu']);
        $x->setId_post($row['id_post']);
        $x->setTexto($row['texto']); 
        $x->setFecha_creado($row['fecha_creado']);
        return $x;
    } 
    

            
}
