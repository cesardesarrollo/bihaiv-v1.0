<?php
require_once 'DTO/Base.php';
require_once 'DTO/Ubicaciones.php';

class DaoUbicaciones extends base
{
    
    public $tableName="ubicaciones"; 

    public function add(Ubicaciones $x)
    {

        $query=sprintf(
            "INSERT INTO ".$this->tableName." (idPais, idEstado, idMunicipio, idLocalidad, idOrganizacion, is_main, is_matriz) VALUES (%s ,%s ,%s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getIdPais(), "int"),
            $this->GetSQLValueString($x->getIdEstado(), "int"),
            $this->GetSQLValueString($x->getIdMunicipio(), "int"),
            $this->GetSQLValueString($x->getIdLocalidad(), "int"),
            $this->GetSQLValueString($x->getIdOrganizacion(), "int"),
            $this->GetSQLValueString($x->getIsMain(), "int"),
            $this->GetSQLValueString($x->getIsMatriz(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }

    }

    public function update(Ubicaciones $x)
    {
        $query=sprintf(
            "UPDATE ".$this->tableName." SET idPais=%s, idEstado=%s, idMunicipio=%s, idLocalidad=%s, idOrganizacion=%s, is_main=%s, is_matriz=%s  WHERE id = %s",
            $this->GetSQLValueString($x->getIdPais(), "int"),
            $this->GetSQLValueString($x->getIdEstado(), "int"),
            $this->GetSQLValueString($x->getIdMunicipio(), "int"),
            $this->GetSQLValueString($x->getIdLocalidad(), "int"),
            $this->GetSQLValueString($x->getIdOrganizacion(), "int"),
            $this->GetSQLValueString($x->getIsMain(), "int"),
            $this->GetSQLValueString($x->getIsMatriz(), "int"),
            $this->GetSQLValueString($x->getId(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
                return $x->getId();
        }
    }

    public function getMainByOrgId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE is_main = 1 AND idOrganizacion = ".$Id;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getMatrizByUserId($Id)
    {
        $query="SELECT ub.* FROM ".$this->tableName." as ub 
                INNER JOIN organizacion as o ON ub.idOrganizacion = o.id
                INNER JOIN user as u ON u.id = o.Usuarios_idUsuarios 
                WHERE ub.is_matriz = 1 AND u.id = $Id";
        return $this->advancedQueryByObjetc($query);
    }

    public function getMatrizByOrgId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE is_matriz = 1 AND idOrganizacion = ".$Id;
        return $this->advancedQueryByObjetc($query);
    }

    public function getActiveByOrgId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE is_main = 1 AND idOrganizacion = ".$Id;
        return $this->advancedQueryByObjetc($query);
    }

    public function getByOrgId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idOrganizacion = ".$Id;
        return $this->advancedQueryByObjetc($query);
    }

    public function getPaisIdByOrgId($Id)
    {
        $query="SELECT pais_id FROM ".$this->tableName." WHERE idOrganizacion = ".$Id." LIMIT 0,1";
        return $this->advancedQueryByObjetc($query);
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    // Obtiene alcances definidos en tabla alcanze
    public function getAlcanzes()
    {
        $query="SELECT * FROM alcanze ORDER BY orden";
        return $this->advancedQuery($query);
    }

    // Ubicaciones_iniciativas
    public function getByIniciativaId($Id)
    {
        $query="SELECT * FROM ubicaciones_iniciativas WHERE idIniciativa = ".$Id." LIMIT 0,1";
        return $this->advancedQueryByObjetc($query);
    }

    public function deleteUbicacionIniciativa($Id)
    {
        $query="DELETE FROM ubicaciones_iniciativas WHERE idIniciativa = ".$Id." LIMIT 0,1";
        return $this->advancedQueryByObjetc($query);
    }
    

    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
        return $resp;
    }

    public function createObject($row)
    {
        $x = new Ubicaciones();
        $x->setId($row['id']);
        $x->setIdPais($row['idPais']);
        $x->setIdEstado($row['idEstado']);
        $x->setIdMunicipio($row['idMunicipio']);
        $x->setIdLocalidad($row['idLocalidad']);
        $x->setIdOrganizacion($row['idOrganizacion']);
        $x->setIsMain($row['is_main']);
        $x->setIsMatriz($row['is_matriz']);
        
        return $x;
    }

}
