<?php
require_once 'DTO/Base.php';
require_once 'DTO/Cuestionario.php';

class DaoCuestionario extends base
{

    public $tableName="cuestionario";

    public function add(Cuestionario $x)
    {

          $query=sprintf(
              "UPDATE ".$this->tableName." SET activo=0 WHERE nivel=%s",
              $this->GetSQLValueString($x->getNivel(), "text")
          );
          $Result1=$this->_cnn->query($query);

          $query=sprintf(
              "INSERT INTO ".$this->tableName." (nombre, nivel, fechaCreacion, activo) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getNivel(), "text"),
              $this->GetSQLValueString($x->getFechaCreacion(), "date"),
              $this->GetSQLValueString($x->getActivo(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }


    public function update(Cuestionario $x)
    {

          $query=sprintf(
              "UPDATE ".$this->tableName." SET activo=0 WHERE nivel=%s",
              $this->GetSQLValueString($x->getNivel(), "text")
          );
          $Result1=$this->_cnn->query($query);

          $query=sprintf(
              "UPDATE ".$this->tableName." SET nombre=%s, nivel=%s, fechaCreacion=%s, activo=%s  WHERE idCuestionario = %s",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getNivel(), "text"),
              $this->GetSQLValueString($x->getFechaCreacion(), "date"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        /*
            $DaoOpcionesRespuesta = new DaoOpcionesRespuesta();
            $DaoPregunta = new DaoPregunta();
            $DaoOpcionesRespuesta->deleteByIdPre($_POST['Id_pre']);
            $DaoPregunta->delete($_POST['Id_pre']);
        */

        // Cuestionario
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idCuestionario=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al eliminar ".$this->tableName.": (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
        /*
        //Cuestionario Usuario
        $query = sprintf("DELETE FROM cuestionariosusuario WHERE idCuestionario=".$Id);
        $Result1=$this->_cnn->query($query);

        print_r($Result1);
        if (!$Result1) {
              throw new Exception("Error al eliminar cuestionariousuario: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {

        //estatuscuestionario

            $query = sprintf("DELETE FROM estatuscuestionario WHERE idCuestionariosUsuario=".$Id);
            $Result1=$this->_cnn->query($query);

            Core::debug($Result1);
            if (!$Result1) {
                throw new Exception("Error al eliminar cuestionariousuario: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {



                // Cuestionario
                $query = sprintf("DELETE FROM ".$this->tableName." WHERE idCuestionario=".$Id);
                $Result1=$this->_cnn->query($query);
                if (!$Result1) {
                    throw new Exception("Error al eliminar ".$this->tableName.": (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                } else {
                    return true;
                }



        }

        }*/



    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCuestionario= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }
    public function getCuestionariosUsuario($id)
    {
        $sql = "SELECT * FROM cuestionariosusuario WHERE idCuestionario = $id";
        $resultSet = $this -> getAllRows($sql);
        return $resultSet;
    }
    public function getEstatusCuestionario($id)
    {
        $sql = "SELECT * FROM estatuscuestionario WHERE idCuestionariosUsuario = $id";
        $resultSet = $this -> getOneRow($sql);
        return $resultSet;
    }
    public function getPreguntasCuestionario($id)
    {
        $sql = "SELECT * FROM pregunta WHERE Cuestionario_idCuestionario = $id";
        $resultSet = $this -> getAllRows($sql);
        return $resultSet;
    }
    public function getOpcionesRespuestaAndDelete($id)
    {
        $sql = "DELETE FROM `opcionesrespuesta` WHERE `Pregunta_idPregunta`= $id";
        $this->_cnn->query($sql);
    }
    public function deletePregunta($id)
    {
        $sql = "DELETE FROM `pregunta` WHERE `idPregunta`= $id ";
        $this->_cnn->query($sql);
    }
    public function deleteEstatusCuestionario($id)
    {
        $sql = "DELETE FROM `estatuscuestionario` WHERE `idEstatusCuestionario`= $id ";
        $this->_cnn->query($sql);
    }
    public function deleteCuestionarioUsuario($id)
    {
        $sql = "DELETE FROM `cuestionariosusuario` WHERE `idCuestionariosUsuario`= $id";
        $this->_cnn->query($sql);
    }


    public function createObject($row)
    {
        $x = new Cuestionario();
        $x->setId($row['idCuestionario']);
        $x->setNombre($row['nombre']);
        $x->setNivel($row['nivel']);
        $x->setFechaCreacion($row['fechaCreacion']);
        $x->setActivo($row['activo']);
        return $x;
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }

    public function getAll($offset=null,$limit=null,$buscar=null)
    {
        $query="";
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" AND nombre LIKE '%".$buscar."%'";
        }

        $query="SELECT * FROM ".$this->tableName." WHERE 1=1 ".$queryBuscar." ".$query;;
         return $this->advancedQueryByObjetc($query);
    }
}
