<?php
require_once 'DTO/Base.php';
require_once 'DTO/Evento.php';

require_once 'UserData.php';
require_once 'DaoColaboracion.php';
require_once 'DaoOrganizacion.php';

class DaoEvento extends base
{

    public $tableName="evento";

    public function add(Evento $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (titulo, textoCorto, textoLargo, creado, fechaEvento, fechaFin, horaInicio, horaFin, lugar, lat, lng, llaveImg, acceso, url, tipoEvento, user_id, iniciativa_id) VALUES (%s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getTextoCorto(), "text"),
              $this->GetSQLValueString($x->getTextoLargo(), "text"),
              $this->GetSQLValueString($x->getCreado(), "date"),
              $this->GetSQLValueString($x->getFechaEvento(), "date"),
              $this->GetSQLValueString($x->getfechaFin(), "date"),
              $this->GetSQLValueString($x->getHoraInicio(), "text"),
              $this->GetSQLValueString($x->getHoraFin(), "text"),
              $this->GetSQLValueString($x->getLugar(), "text"),
              $this->GetSQLValueString($x->getLat(), "text"),
              $this->GetSQLValueString($x->getLng(), "text"),
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getAcceso(), "text"),
              $this->GetSQLValueString($x->getUrl(), "text"),
              $this->GetSQLValueString($x->getTipoEvento(), "text"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getIniciativaId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function addIniciativaEvento($idIniciativa,$idEvento)
    {
          $query=sprintf(
              "INSERT INTO iniciativaeventos (idIniciativa, idEvento) VALUES (%s, %s)",
              $this->GetSQLValueString($idIniciativa, "text"),
              $this->GetSQLValueString($idEvento, "text")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Evento $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET titulo=%s, textoCorto=%s, textoLargo=%s, creado=%s, fechaEvento=%s, fechaFin=%s, horaInicio=%s, horaFin=%s, lugar=%s, lat=%s, lng=%s, llaveImg=%s, user_id=%s, acceso=%s, url=%s, tipoEvento=%s, iniciativa_id=%s WHERE idEvento = %s",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getTextoCorto(), "text"),
              $this->GetSQLValueString($x->getTextoLargo(), "date"),
              $this->GetSQLValueString($x->getCreado(), "date"),
              $this->GetSQLValueString($x->getFechaEvento(), "date"),
              $this->GetSQLValueString($x->getfechaFin(), "date"),
              $this->GetSQLValueString($x->getHoraInicio(), "text"),
              $this->GetSQLValueString($x->getHoraFin(), "text"),
              $this->GetSQLValueString($x->getLugar(), "text"),
              $this->GetSQLValueString($x->getLat(), "text"),
              $this->GetSQLValueString($x->getLng(), "text"),
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getAcceso(), "text"),
              $this->GetSQLValueString($x->getUrl(), "text"),
              $this->GetSQLValueString($x->getTipoEvento(), "text"),
              $this->GetSQLValueString($x->getIniciativaId(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idEvento=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }

    public function deleteIniciativaEventos($Id)
    {
        $query = sprintf("DELETE FROM iniciativaeventos WHERE idEvento=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
              return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT
       *,
       (SELECT
            u.name AS actor
        FROM
            organizacion o
                INNER JOIN
            user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            o.id = user_id) AS _actor,
        (SELECT
            u.id
        FROM
            organizacion o
                INNER JOIN
            user u ON u.id = o.Usuarios_idUsuarios
        WHERE
            o.id = user_id) AS _id_actor
       FROM ".$this->tableName." WHERE idEvento = ".$Id;
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            if ($Result1->num_rows > 0) {
                return $this->createObject($Result1->fetch_assoc());
            } else {
                return false;
            }
        }
    }

    public function createObject($row)
    {
        $x = new Evento();
        $x->setId($row['idEvento']);
        $x->setTitulo($row['titulo']);
        $x->setTextoCorto($row['textoCorto']);
        $x->setTextoLargo($row['textoLargo']);
        $x->setCreado($row['creado']);
        $x->setFechaEvento($row['fechaEvento']);
        $x->setFechaFin($row['fechaFin']);
        $x->setHoraInicio($row['horaInicio']);
        $x->setHoraFin($row['horaFin']);
        $x->setLugar($row['lugar']);
        $x->setLat($row['lat']);
        $x->setLng($row['lng']);
        $x->setLlaveImg($row['llaveImg']);
        $x->setUsuario_id($row['user_id']);
        $x->setAcceso($row['acceso']);
        $x->setUrl($row['url']);
        $x->setTipoEvento($row['tipoEvento']);
        $x->setIniciativaId($row['iniciativa_id']);

        $x->setActorName($row['_actor']);
        $x->setActorId($row['_id_actor']);
        return $x;
    }

    public function getEventosByIdUsu($Id_usu)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE user_id=".$Id_usu;
        return $this->advancedQuery($query);
    }

    public function getEventos($user_id = null)
    {
        $DaoColaboracion = new DaoColaboracion();
        $DaoOrganizacion = new DaoOrganizacion();

        $hoy = date('Y-m-d');
        $fecha_separada = explode('-', $hoy);
        $fecha_inicial = $fecha_separada[0] . '-' . ($fecha_separada[1]) . '-' . '01';
        $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1]) . '-' . '31';
        // $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . $fecha_separada[2];
        $whenisloged = "";
        $notLogued = '';

        if ($user_id !== null) {
            $whenisloged = " ( select estatus from asistenteevento where Evento_idEvento = e.idEvento and user_id = $user_id) as user_asistenteevento,";
        }elseif (is_null($user_id)) {
            $notLogued = ' e.acceso not like "Privado" AND ';
        }
        $query = "select e.titulo as _titulo,
                   e.textoLargo as _descripcion,
                   e.idEvento as _idevento,
                   (SELECT
                        u.name AS actor
                    FROM
                        organizacion o
                            INNER JOIN
                        user u ON u.id = o.Usuarios_idUsuarios
                    WHERE
                        o.id = e.user_id) AS _actor,
                  (SELECT
                      u.id
                  FROM
                      organizacion o
                          INNER JOIN
                      user u ON u.id = o.Usuarios_idUsuarios
                  WHERE
                      o.id = e.user_id) AS _id_actor,
                   e.user_id as _usuario,
                   e.fechaEvento as _fecha,
                   e.horaInicio as _horaInicio,
                   e.horaFin as _horaFin,
                   e.lugar as _lugar,
                   e.llaveImg as _avatar,
                   e.tipoEvento as _tipoevento,
                   e.acceso as _acceso,
                   1 as _tipo,
                   $whenisloged
                   'currentEvents' as tipoEvento
                  from evento e
                  where $notLogued e.fechaEvento between '". $fecha_inicial ."' and '" . $fecha_final. "'
                  order by e.fechaEvento";
        $resultSet = $this -> advancedQuery($query);
        foreach ($resultSet as $k => $v) {
            $resultSet[$k]['_actor'] = utf8_decode($v['_actor']);

            //Colaboradores
            $userdata = new UserData();
            
            $colaboraciones = $DaoColaboracion->getTrueByIdEvento($v['_idevento']);
            $array_colaboracion = array();
            foreach ($colaboraciones as $colaboracion) {
                $org = $DaoOrganizacion->getById($colaboracion['user_id']);
                $user = $userdata->getById($org->Usuarios_idUsuarios);

                if ($org->avatar === "assets/img/home/perfil.png") {
                    $org->avatar = "../../../../assets/img/home/perfil.png";
                }
                array_push($array_colaboracion, array('user_id' => $user->id, 'user_name' => $user->name, 'avatar' => $org->avatar ));
            }
            $resultSet[$k]['_colaboradores'] = ((count($array_colaboracion) > 0) ? $array_colaboracion : "");
        }
        return $resultSet;
    }

    public function getCoomingEvents($user_id = null)
    {
        $DaoColaboracion = new DaoColaboracion();
        $DaoOrganizacion = new DaoOrganizacion();

        $hoy = date('Y-m-d');
        /*$fecha_separada = explode('-', $hoy);
        $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . '01';*/
        $whenisloged = "";
        $notLogued = '';

        if ($user_id !== null) {
            $whenisloged = " ( select estatus from asistenteevento where Evento_idEvento = e.idEvento and user_id = $user_id) as user_asistenteevento,";
        }elseif (is_null($user_id)) {
            $notLogued = ' e.acceso not like "Privado" AND ';
        }
        $query = "select e.titulo as _titulo,
                   e.textoLargo as _descripcion,
                   e.idEvento as _idevento,
                   (SELECT
                        u.name AS actor
                    FROM
                        organizacion o
                            INNER JOIN
                        user u ON u.id = o.Usuarios_idUsuarios
                    WHERE
                        o.id = e.user_id) AS _actor,
                  (SELECT
                      u.id
                  FROM
                      organizacion o
                          INNER JOIN
                      user u ON u.id = o.Usuarios_idUsuarios
                  WHERE
                      o.id = e.user_id) AS _id_actor,
                   e.user_id as _usuario,
                   e.fechaEvento as _fecha,
                   e.horaInicio as _horaInicio,
                   e.horaFin as _horaFin,
                   e.lugar as _lugar,
                   e.llaveImg as _avatar,
                   e.tipoEvento as _tipoevento,
                   e.acceso as _acceso,
                   2 as _tipo,
                   $whenisloged
                   'commingEvents' as tipoEvento
                  from evento e
                  where $notLogued e.fechaEvento > '" . $hoy . "'
                  and tipoEvento like 'FESTIVAL'
                  order by e.fechaEvento asc";
        $resultSet = $this -> advancedQuery($query);
        foreach ($resultSet as $k => $v) {
            $resultSet[$k]['_actor'] = utf8_decode($v['_actor']);

            //Colaboradores
            $userdata = new UserData();
            $colaboraciones = $DaoColaboracion->getTrueByIdEvento($v['_idevento']);
            $array_colaboracion = array();
            foreach ($colaboraciones as $colaboracion) {
                $org = $DaoOrganizacion->getById($colaboracion['user_id']);
                $user = $userdata->getById($org->Usuarios_idUsuarios);
                array_push($array_colaboracion, array('user_id' => $user->id, 'user_name' => $user->name, 'avatar' => $org->avatar ));
            }
            $resultSet[$k]['_colaboradores'] = ((count($array_colaboracion) > 0) ? $array_colaboracion : "");
        }
        return $resultSet;
    }

    public function getLastEvents($user_id = null)
    {
        $DaoColaboracion = new DaoColaboracion();
        $DaoOrganizacion = new DaoOrganizacion();

        $hoy = date('Y-m-d');
        $fecha_separada = explode('-', $hoy);
        $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . $fecha_separada[2];
        $whenisloged = "";
        $notLogued = "";

        if ($user_id !== null) {
            $whenisloged = " ( select estatus from asistenteevento where Evento_idEvento = e.idEvento and user_id = $user_id) as user_asistenteevento,";
        }elseif (is_null($user_id)) {
            $notLogued = ' e.acceso not like "Privado" AND ';
        }
        $query = "select e.titulo as _titulo,
                   e.textoLargo as _descripcion,
                   e.idEvento as _idevento,
                   /*(select count(id) from heart where ref_id = e.idEvento ) as _likes,*/
                   e.user_id as _usuario,
                   (SELECT
                        u.name AS actor
                    FROM
                        organizacion o
                            INNER JOIN
                        user u ON u.id = o.Usuarios_idUsuarios
                    WHERE
                        o.id = e.user_id) AS _actor,
                  (SELECT
                      u.id
                  FROM
                      organizacion o
                          INNER JOIN
                      user u ON u.id = o.Usuarios_idUsuarios
                  WHERE
                      o.id = e.user_id) AS _id_actor,
                   e.fechaEvento as _fecha,
                   e.horaInicio as _horaInicio,
                   e.horaFin as _horaFin,
                   e.lugar as _lugar,
                   e.llaveImg as _avatar,
                   e.tipoEvento as _tipoevento,
                   e.acceso as _acceso,
                   3 as _tipo,
                   $whenisloged
                   'lastEvents' as tipoEvento
                  from evento e
                  where $notLogued e.fechaEvento < '" .$hoy ."'
                  and tipoEvento like 'FESTIVAL'
                  order by e.fechaEvento desc";
        $resultSet = $this -> advancedQuery($query);
        foreach ($resultSet as $k => $v) {
            $resultSet[$k]['_actor'] = utf8_decode($v['_actor']);

            //Colaboradores
            $userdata = new UserData();
            $colaboraciones = $DaoColaboracion->getTrueByIdEvento($v['_idevento']);
            $array_colaboracion = array();
            foreach ($colaboraciones as $colaboracion) {
                $org = $DaoOrganizacion->getById($colaboracion['user_id']);
                $user = $userdata->getById($org->Usuarios_idUsuarios);
                array_push($array_colaboracion, array('user_id' => $user->id, 'user_name' => $user->name, 'avatar' => $org->avatar ));
            }
            $resultSet[$k]['_colaboradores'] = ((count($array_colaboracion) > 0) ? $array_colaboracion : "");

        }
        return $resultSet;
    }

    public function filterEvents( $eventRange, $eventType, $eventDate, $eventName )
    {
        $where = "";
        if ($eventRange === 'currents') {
            $hoy = date('Y-m-d');
            $fecha_separada = explode('-', $hoy);
            $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . $fecha_separada[2];
            $inicio_mes = $fecha_separada[0] . '-' . ($fecha_separada[1]) . '-' . '01';
            $fin_mes    = $fecha_separada[0] . '-' . ($fecha_separada[1]) . '-' . '31';
            //FILTER BY 3 FIELDS
            if ($eventType != "" && $eventDate != "" && $eventName != "") {
                $where .= "where e.tipoEvento =  '$eventType' and e.fechaEvento= '$eventDate' and e.titulo like '%$eventName%' ";
            }
            //FILTER BY EVENT TYPE AND EVENT DATE
            else if ($eventType != "" && $eventDate != "" && $eventName === "" ) {
                $where .= ' where  e.tipoEvento ="' . $eventType . '" and e.fechaEvento="' .$eventDate . '"';
            }
            //FILTER BY EVENT TYPE
            else if ($eventType != "" && $eventDate === "" && $eventName === "" ) {
                $where .= ' where e.fechaEvento between "'. $inicio_mes . '" and "' . $fin_mes. '" and e.tipoEvento ="' . $eventType . '"';
            }
            //FILTER BY EVENT TYPE AND EVENT NAME
            else if ($eventType != "" && $eventDate === "" && $eventName != "" ) {
                $where .= ' where e.fechaEvento between "'. $inicio_mes .'" and "' . $fin_mes. '" and e.tipoEvento ="' . $eventType . '" and e.titulo like "%' . $eventName . '%"';
            }
            //FILTER BY EVENT EVENT DATE
            else if ($eventType === "" && $eventDate != "" && $eventName === "" ) {
                $where .= ' where e.fechaEvento ="' . $eventDate . '"';
            }
            //FILTER BY EVENT DATE && EVENT NAME
            else if ($eventType === "" && $eventDate != "" && $eventName != "") {
                $where .= ' where e.fechaEvento="' .$eventDate . '" and e.titulo like "%' . $eventName . '%"';
            }
            //FILTER BY EVENT NAME
            else if ($eventType === "" && $eventDate === "" && $eventName != "" ) {
                $where .= ' where e.fechaEvento between "'. $inicio_mes .'" and "' . $fin_mes. '" and
              e.titulo like "%' . $eventName . '%"';
            }
        }
        else if ($eventRange === 'coomings') {
            $hoy = date('Y-m-d');
            $fecha_separada = explode('-', $hoy);
            $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . '01';
            $fecha_final_fin_mes = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . '31';
            //FILTER BY 3 FIELDS
            if ($eventType != "" && $eventDate != "" && $eventName != "") {
                $where .= ' where e.tipoEvento ="' . $eventType . '" and e.fechaEvento="' .$eventDate . '" and e.titulo  LIKE "%' . $eventName . '%"';
            }
            //FILTER BY EVENT TYPE AND EVENT DATE
            else if ($eventType != "" && $eventDate != "" && $eventName === "" ) {
                $where .= 'where e.tipoEvento ="' . $eventType . '" and e.fechaEvento="' .$eventDate . '"';
            }
            //FILTER BY EVENT TYPE
            else if ($eventType != "" && $eventDate == "" && $eventName == "" ) {
                $where .= ' where e.fechaEvento > "' . $hoy . '" AND  e.tipoEvento = "'.$eventType.'"';
            }
            //FILTER BY EVENT TYPE AND EVENT NAME
            else if ($eventType != "" && $eventDate === "" && $eventName != "" ) {
                $where .= ' where e.fechaEvento > "' . $hoy . '" and e.tipoEvento = "' . $eventType . '" and e.titulo like "%' . $eventName. '%"';
            }
            //FILTER BY EVENT EVENT DATE
            else if ($eventType === "" && $eventDate != "" && $eventName === "" ) {
                $where .= ' where e.fechaEvento ="' . $eventDate . '"';
            }
            //FILTER BY EVENT DATE && EVENT NAME
            else if ($eventType === "" && $eventDate != "" && $eventName != "") {
                $where .= ' where e.fechaEvento="' .$eventDate . '" and e.titulo like "%' . $eventName . '%"';
            }
            //FILTER BY EVENT NAME
            else if ($eventType === "" && $eventDate === "" && $eventName != "" ) {
                $where .= ' where e.fechaEvento between "'. $hoy .'" and "' . $fecha_final_fin_mes. '" and
              e.titulo like "%' . $eventName. '%"';
            }
            /* Sin filtros */
            else if (!$eventType  AND !$eventDate AND !$eventName ) {
                $where .= "where e.fechaEvento between '$hoy' and '$fecha_final_fin_mes'";
            }
        }
        else if ($eventRange === 'latest') {
            $hoy = date('Y-m-d');
            $fecha_separada = explode('-', $hoy);
            $fecha_final = $fecha_separada[0] . '-' . ($fecha_separada[1] + 1) . '-' . $fecha_separada[2];
            //FILTER BY 3 FIELDS
            if ($eventType != "" && $eventDate != "" && $eventName != "") {
                $where .= ' where e.tipoEvento ="' . $eventType . '" and e.fechaEvento="' .$eventDate . '" and e.titulo like "%' . $eventName . '%"';
                // breakpoint($where);
            }
            //FILTER BY EVENT TYPE AND EVENT DATE
            else if ($eventType != "" && $eventDate != "" && $eventName === "" ) {
                $where .= ' where e.tipoEvento ="' . $eventType . '" and e.fechaEvento="' .$eventDate . '"';
            }
            //FILTER BY EVENT TYPE
            else if ($eventType != "" && $eventDate === "" && $eventName === "" ) {
                $where .= ' where e.fechaEvento < "' .$hoy .'" and e.tipoEvento ="' . $eventType. '"';
            }
            //FILTER BY EVENT TYPE AND EVENT NAME
            else if ($eventType != "" && $eventDate === "" && $eventName != "" ) {
                $where .= ' where e.fechaEvento < "' .$hoy .'" and e.tipoEvento ="' . $eventType . '" and e.titulo like "%' . $eventName . '%"';
            }
            //FILTER BY EVENT EVENT DATE
            else if ($eventType === "" && $eventDate != "" && $eventName === "" ) {
                $where .= ' where e.fechaEvento ="' . $eventDate . '"';
            }
            //FILTER BY EVENT DATE && EVENT NAME
            else if ($eventType === "" && $eventDate != "" && $eventName != "") {
                $where .= ' where e.fechaEvento="' .$eventDate . '" and e.titulo like "%' . $eventName . '%"';
            }
            //FILTER BY EVENT NAME
            else if ($eventType === "" && $eventDate === "" && $eventName != "" ) {
                $where .= " where e.fechaEvento < '$hoy' AND e.titulo like '%$eventName%'";
            }
            /* Sin filtros */
            else if (!$eventType  AND !$eventDate AND !$eventName ) {
                $where .= "where e.fechaEvento < '$hoy' ";
            }
        }
        // $where = str_replace('&quot;','"',htmlentities($where));
        // breakpoint($where);
        $query = "select
        e.titulo as _titulo,
        (SELECT
             u.name AS actor
         FROM
             organizacion o
                 INNER JOIN
             user u ON u.id = o.Usuarios_idUsuarios
         WHERE
             o.id = e.user_id) AS _actor,
       (SELECT
           u.id
       FROM
           organizacion o
               INNER JOIN
           user u ON u.id = o.Usuarios_idUsuarios
       WHERE
           o.id = e.user_id) AS _id_actor,
        e.textoLargo as _descripcion, e.idEvento as _idevento, e.user_id as _usuario, e.fechaEvento as _fecha, e.horaInicio as _horaInicio, e.horaFin as _horaFin, e.lugar as _lugar, e.llaveImg as _avatar, e.tipoEvento as _tipoevento, 1 as _tipo from evento e " . $where;
        if (!$resultSet = $this -> getAllRows($query)) {
            $resultSet = array();
        } else {
            foreach ($resultSet as $k => $v) {
                $resultSet[$k]['_actor'] = utf8_decode($v['_actor']);
            }
        }
        return $resultSet;
    }

    public function getCoomingEventsForCalendar($orgId)
    {
        $query = "select e.titulo as title,
                  e.idEvento as id,
                  e.tipoEvento as category,
                  e.fechaEvento as start,
                  DATE_ADD(e.fechaFin, INTERVAl 1 DAY) as end
                  from evento e where e.user_id = " . $orgId;
        $events =  $this->advancedQuery($query);
        foreach ($events as $key => $event) {
            if ($event['start'] < date('Y-m-d')) {
                $events[$key]['color'] = 'gray';
            } else {
                switch($event['category']){
                case 'TALLER':
                    $events[$key]['color'] = 'red';
                    break;
                case 'FESTIVAL':
                    $events[$key]['color'] = 'orange';
                    break;
                case 'CONFERENCIA':
                    $events[$key]['color'] = 'salmon';
                    break;
                case 'EXPOSICION':
                    $events[$key]['color'] = 'blue';
                    break;
                }
            }
        }
        return $events;
    }

    public function getCoomingEventsForTagging()
    {
        $query = "select e.titulo as _titulo, e.idEvento as _id from evento as e where e.fechaEvento > '" . date('Y-m-d'). "'";
        $eventos = $this->advancedQuery($query);
        $arr_eventos = array();
        foreach ($eventos as $evento) {
            array_push($arr_eventos, $evento['_titulo'] . "-" . $evento['_id']);
        }
        return $arr_eventos;
    }

    public function getAllEventsForReport()
    {
        $query = "select
            e.titulo as Título,
            e.textoLargo as Descripción,
            u.name as Publica,
            e.fechaEvento as Fecha,
            e.horaInicio as Inicia,
            e.horaFin as Finaliza,
            e.tipoEvento as Evento,
            e.lugar as Lugar,
            e.lat,
            e.lng
            from evento e
            LEFT JOIN
            organizacion as o ON o.id = e.user_id
            INNER JOIN 
            user u  ON  u.id = o.Usuarios_idUsuarios 
            ORDER BY e.fechaEvento DESC";
        return $this->advancedQuery($query);
    }

    public function getAllEvents()
    {
        $date = date('Y-m-d');
        $query = "select
            e.titulo as _titulo,
            e.textoLargo as _descripcion,
            e.idEvento as _idevento,
            e.user_id as _usuario,
            e.fechaEvento as _fecha,
            e.horaInicio as _horaInicio,
            e.horaFin as _horaFin,
            e.lugar as _lugar,
            e.llaveImg as _avatar,
            e.tipoEvento as _tipoevento,
            e.acceso as _acceso,
            2 as _tipo,
            e.lat,
            e.lng
            from evento e
            WHERE e.fechaEvento >= '$date'
            ORDER BY e.fechaEvento ASC";
        return $this->advancedQuery($query);
    }

    public function getAllEventsExceptOwns($key_current)
    {

        //Devuelve eventos excepto los correspondientes por idEvento en arraglo $key_current
        function unique_multidim_array($array, $key,$key_current) 
        {
            $temp_array = array();
            $i = 0;
            $key_array = array();

            foreach($array as $val) {
                if (!in_array($val[$key], $key_array)  &&  !in_array($val[$key], $key_current) ) {
                    $key_array[$i] = $val[$key];
                    $temp_array[$i] = $val;
                }
                $i++;
            }
            return $temp_array;
        }

        $query = "select * from evento";
        $events = $this->advancedQuery($query);
        return unique_multidim_array($events, 'idEvento', $key_current);

    }
}
