<?php
require_once 'DTO/Base.php';
require_once 'DTO/CuestionariosUsuario.php';

class DaoCuestionariosUsuario extends base
{
    
    public $tableName="cuestionariosUsuario"; 

    public function add(CuestionariosUsuario $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (fechaCreacion, user_id, idCuestionario, Roles_idRol) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getFechaCreacion(), "date"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getIdCuestionario(), "int"),
              $this->GetSQLValueString($x->getRoles_idRol(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(CuestionariosUsuario $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET fechaCreacion=%s, user_id=%s, idCuestionario=%s, Roles_idRol=%s  WHERE idCuestionariosUsuario = %s",
              $this->GetSQLValueString($x->getFechaCreacion(), "date"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getIdCuestionario(), "int"),
              $this->GetSQLValueString($x->getRoles_idRol(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idCuestionariosUsuario=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCuestionariosUsuario= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new CuestionariosUsuario();
        $x->setId($row['idCuestionariosUsuario']);
        $x->setFechaCreacion($row['fechaCreacion']);
        $x->setUsuario_id($row['user_id']);
        $x->setIdCuestionario($row['idCuestionario']); 
        $x->setRoles_idRol($row['Roles_idRol']);
        return $x;
    }
          

    public function getCuestionariosByIdUsuario()
    {
        $query="SELECT * FROM ".$this->tableName." WHERE user_id=".$tipo;
         return $this->advancedQuery($query);
    }


}
