<?php
/**
* @author cmagana
* @class FocusData
* @brief Modelo de base de datos para la tabla de enfoques de usuarios
**/
class FocusData
{
    public static $tablename = "enfoque";

    public function FocusData()
    {
        $this->texto           = "";
        $this->tipo           = 0;
        $this->post_id      = "";
        $this->fechaCreado = "NOW()";
    }

    public function add($post)
    {

        $this->uid           = $post['uid'];
        $this->texto           = $post['texto'];
        $this->tipo           = $post['tipo'];
        $this->fechaCreado  = date('Y-m-d');

        $sql = "insert into publicaciones (fechaCreado,tipo,user_id) ";
        $sql .= "value (\"$this->fechaCreado\",\"$this->tipo\",\"$this->uid\")";
        $resp = Executor::doit($sql);

        /* Si la respuesta fue positiva */
        if ($resp[0]) {
            $post_id = $resp[1];
            $sql = "insert into enfoque (texto,post_id) ";
            $sql .= "value (\"$this->texto\",\"$post_id\")";
            Executor::doit($sql);

            return $this->uid;
        }

    }

    public static function delete($id)
    {
        $sql = "delete from ".self::$tablename." where id=$id";
        Executor::doit($sql);
    }

    // partiendo de que ya tenemos creado un objecto FocusData previamente utilizamos el contexto
    public function update()
    {
        $sql = "update ".self::$tablename." set texto=\"$this->texto\" where id=$this->id";
        Executor::doit($sql);
    }


    public function activate()
    {
        $sql = "update ".self::$tablename." set is_active=1 where id=$this->id";    
        Executor::doit($sql);
    }

    public static function getByUserId($id)
    {
        $sql = "SELECT * FROM publicaciones as p INNER JOIN ".self::$tablename." as t ON p.id = t.post_id  WHERE p.tipo = 3 AND p.user_id = ".$id." ORDER BY id DESC LIMIT 0,5";
        $query = Executor::doit($sql);
        return Model::many($query[0], new FocusData());
    }

}

?>
