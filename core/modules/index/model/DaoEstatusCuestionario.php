<?php
require_once 'DTO/Base.php';
require_once 'DTO/EstatusCuestionario.php';

class DaoEstatusCuestionario extends base
{
    
    public $tableName="estatuscuestionario"; 

    public function add(EstatusCuestionario $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (fechaEstatus, estatus, idCuestionariosUsuario) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getFechaEstatus(), "date"),
              $this->GetSQLValueString($x->getEstatus(), "int"),
              $this->GetSQLValueString($x->getIdCuestionariosUsuario(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }

    public function update(EstatusCuestionario $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET fechaEstatus=%s, estatus=%s, idCuestionariosUsuario=%s  WHERE idEstatusCuestionario = %s",
              $this->GetSQLValueString($x->getFechaEstatus(), "date"),
              $this->GetSQLValueString($x->getEstatus(), "int"),
              $this->GetSQLValueString($x->getIdCuestionariosUsuario(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idEstatusCuestionario=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idEstatusCuestionario= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function createObject($row)
    {
        $x = new EstatusCuestionario();
        $x->setId($row['idEstatusCuestionario']);
        $x->setFechaCreacion($row['fechaEstatus']);
        $x->setUsuario_id($row['estatus']);
        $x->setIdCuestionario($row['idCuestionariosUsuario']); 
        return $x;
    }


    public function getEstatusByIdCuestionario($idCuestionariosUsuario)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCuestionariosUsuario=".$idCuestionariosUsuario;
         return $this->advancedQuery($query);
    }


}
