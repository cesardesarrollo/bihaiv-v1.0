<?php
require_once 'DTO/Base.php';
require_once 'DTO/Publicaciones.php';

class DaoPublicaciones extends base
{

    public $tableName="publicaciones";

    public function add(Publicaciones $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (fechaCreado, tipo, user_id, is_public, by_admin, by_expert, active) VALUES (%s ,%s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getFechaCreado(), "date"),
              $this->GetSQLValueString($x->getTipo(), "int"),
              $this->GetSQLValueString($x->getUser_id(), "int"),
              $this->GetSQLValueString($x->getIsPublic(), "text"),
              $this->GetSQLValueString($x->getByAdmin(), "int"),
              $this->GetSQLValueString($x->getByExpert(), "int"),
              $this->GetSQLValueString($x->getActive(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Publicaciones $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET fechaCreado=%s, tipo=%s, user_id=%s, active=%s, is_public=%s, by_admin=%s, by_expert=%s   WHERE id = %s",
              $this->GetSQLValueString($x->getFechaCreado(), "date"),
              $this->GetSQLValueString($x->getTipo(), "int"),
              $this->GetSQLValueString($x->getUser_id(), "int"),
              $this->GetSQLValueString($x->getActive(), "int"),
              $this->GetSQLValueString($x->getIsPublic(), "text"),
              $this->GetSQLValueString($x->getByAdmin(), "int"),
              $this->GetSQLValueString($x->getByExpert(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function getAll($offset=null,$limit=null,$buscar=null,$orderby=null, $tipo_publicacion = 3 )
    {
        $conditions="";
        if ($orderby!=null) {
            $conditions .= " ORDER BY ".$orderby;
        }
        if (($offset!=null || $offset===0)  && $limit!=null) {
            $conditions .= " LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar != null) {
            $queryBuscar="AND (a.titulo LIKE '%".$buscar."%' OR a.descripcion LIKE '%".$buscar."%')";
        }
        $query="SELECT * FROM ".$this->tableName." INNER JOIN aportacion as a ON a.post_id = ".$this->tableName.".id WHERE ".$this->tableName.".tipo = $tipo_publicacion ".$queryBuscar." ".$conditions;
        return $this->advancedQueryByObjetc($query);
    }

    public function getAllBySection($status=[0,1],$where=[],$offset=null,$limit=null,$orderby=null)
    {
        $conditions="";
        foreach ($where as $key => $value) {
            $conditions .= " AND ".$key."".$value;
        }

        $conditions .= ' GROUP BY p.id';

        if ($orderby!=null) {
            $conditions .= " ORDER BY ".$orderby;
        }

        if (($offset!=null || $offset===0)  && $limit!=null) {
            $conditions .= " LIMIT ".$limit." OFFSET ".$offset;
        }

        $status = implode(", ", $status);

        $query="SELECT
                p.*, a.*, o.*,r.idRol, cal.*, ca.idCategoriaAportacion, sa.idSeccionAportacion,
                IFNULL(ROUND(AVG(cal.calificacion)), 0) AS _calificacion
                FROM ".$this->tableName." AS p
                INNER JOIN aportacion AS a ON a.post_id = p.id
                LEFT JOIN organizacion AS o ON o.id = p.user_id
                LEFT JOIN roles AS r ON o.Roles_idRol = r.idRol
                LEFT JOIN calificacionaportacion as cal ON cal.Aportacion_idAportacion = a.idAportacion
                LEFT JOIN categoriasaportacion AS ca ON ca.idCategoriaAportacion = a.idCategoriaAportacion
                LEFT JOIN seccionesaportacion AS sa ON sa.idSeccionAportacion = ca.idSeccionAportacion
                WHERE p.tipo = 3 AND p.active IN (".$status.") ".$conditions;

        return $this->advancedQuery($query);
    }
    public function getAllBySectionEcosistema($data)
    {
        /* Set shit */
        $order_by = null;
        $where = '';

        /*Roles*/
        if ($data['order_by'] == 'tipo_actor') {
            $where = ' AND  o.Roles_idRol = '.$data['roles_id'].' ';
        }

        /* order by*/

        if (isset($data['order_by']) ) {
            switch ($data['order_by']) {
            case 'fecha_desc':
                $order_by = "ORDER BY p.fechaCreado DESC";
                break;

            case 'fecha_asc':
                $order_by = "ORDER BY p.fechaCreado ASC";
                break;

            case 'most_visited':
                $order_by = "ORDER BY ap.visitas DESC";
                break;

            case 'less_visited':
                $order_by = "ORDER BY ap.visitas ASC";
                break;

            default:
                $order_by = "";
                break;
            }
        }
        $sql = "SELECT
        	r.nombre,
            p.*,
            ap.*,
            IFNULL(ROUND(AVG(ca.calificacion)), 0) AS _calificacion
        FROM
            publicaciones AS p
                INNER JOIN
            aportacion AS ap ON p.id = ap.post_id
                LEFT JOIN
            calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
        		LEFT JOIN
        	organizacion o ON o.id = p.user_id
        		LEFT JOIN
        	roles r on r.idRol = o.Roles_idRol
        WHERE
            ap.idCategoriaAportacion = 0 AND p.active = 1 AND p.tipo = 3 AND p.is_public = 1 $where
        GROUP BY p.id
      $order_by";
        $resultSet = $this -> getAllRows($sql);
        foreach ($resultSet as $k => $v) {
            $resultSet[$k]['thumbnail']   =  APP_PATH.'uploads/aportaciones/'.$v['thumbnail'];
            $resultSet[$k]['file']        =  APP_PATH.'uploads/aportaciones/'.$v['file'];
            /* date Format*/
            $fecha = new DateTime($v['fechaCreado']);
            $resultSet[$k]['fechaCreado'] =  $fecha -> format('d M Y  h:i:s a');
        }
        return $resultSet;
    }
    public function getAllBySectionBihaiv($data)
    {
        /* Set shit */
        $order_by = null;
        /* order by*/
        if (isset($data['order_by']) ) {
            switch ($data['order_by']) {
            case 'fecha_desc':
                $order_by = "ORDER BY p.fechaCreado DESC";
                break;

            case 'fecha_asc':
                $order_by = "ORDER BY p.fechaCreado ASC";
                break;

            case 'most_visited':
                $order_by = "ORDER BY a.visitas DESC";
                break;

            case 'less_visited':
                $order_by = "ORDER BY a.visitas ASC";
                break;

            default:
                $order_by = "";
                break;
            }
        }

        $sql = "SELECT
            p.*,
            a.*
        FROM
            publicaciones AS p
                INNER JOIN
            aportacion AS a ON a.post_id = p.id
        WHERE
            p.tipo = 5 AND p.active = 1
      $order_by";
        $resultSet = $this -> getAllRows($sql);
        foreach ($resultSet as $k => $v) {
            $resultSet[$k]['thumbnail']   =  APP_PATH.'uploads/aportaciones/'.$v['thumbnail'];
            $resultSet[$k]['file']        =  APP_PATH.'uploads/aportaciones/'.$v['file'];
            /* date Format*/
            $fecha = new DateTime($v['fechaCreado']);
            $resultSet[$k]['fechaCreado'] =  $fecha -> format('d M Y  h:i:s a');
        }
        return $resultSet;
    }

    public function getPublicacionesMes($mes, $tipo = 'bihaiv')
    {
        $year = date('Y');
        if ($tipo == 'bihaiv') {
            /* Set shit */
            $sql = "SELECT
          	DATE_FORMAT(p.fechaCreado,'%Y-%m') as fechaSimple,
              IFNULL(count(DATE_FORMAT(p.fechaCreado,'%Y-%m')) , 0)  as count
          FROM
              publicaciones AS p
                  INNER JOIN
              aportacion AS a ON a.post_id = p.id
          WHERE
          	DATE_FORMAT(p.fechaCreado,'%Y-%m') = '$year-$mes' AND
              p.tipo = 5 AND p.active = 1
          group by fechaSimple";
            $resultSet = $this -> getOneRow($sql);
        }elseif ($tipo == 'ecosistema') {
            $sql = "SELECT
              *
          FROM
              publicaciones AS p
                  INNER JOIN
              aportacion AS ap ON p.id = ap.post_id
                  LEFT JOIN
              calificacionaportacion ca ON ca.Aportacion_idAportacion = ap.idAportacion
                  LEFT JOIN
              organizacion o ON o.id = p.user_id
                  LEFT JOIN
              roles r ON r.idRol = o.Roles_idRol
          WHERE
              DATE_FORMAT(p.fechaCreado,'%Y-%m') = '$year-$mes'
                  AND p.active = 1
                  AND p.tipo = 3
                  AND p.is_public = 1
                  AND ap.idCategoriaAportacion = 0
        GROUP BY p.id";
            $resultSet = $this -> getAllRows($sql);
            $resultSet = array('count' => count($resultSet) );
        }
        return $resultSet;
    }


    public function countAllBySection($status=[0,1],$where=[],$offset=null,$limit=null,$orderby=null)
    {
        $conditions="";
        foreach ($where as $key => $value) {
            $conditions .= " AND ".$key."".$value;
        }

        if ($orderby!=null) {
            $conditions .= " ORDER BY ".$orderby;
        }

        if (($offset!=null || $offset===0)  && $limit!=null) {
            $conditions .= " LIMIT ".$limit." OFFSET ".$offset;
        }

        $status = implode(", ", $status);

        $query="SELECT COUNT(*) AS count FROM ".$this->tableName." AS p
                INNER JOIN aportacion AS a ON a.post_id = p.id

                LEFT JOIN organizacion AS o ON o.id = p.user_id
                LEFT JOIN roles AS r ON o.Roles_idRol = r.idRol

                LEFT JOIN categoriasaportacion AS ca ON ca.idCategoriaAportacion = a.idCategoriaAportacion
                LEFT JOIN seccionesaportacion AS sa ON sa.idSeccionAportacion = ca.idSeccionAportacion
                WHERE p.tipo = 3 AND p.active IN (".$status.") ".$conditions;

        return $this->advancedQuery($query);
    }

    public function logicDelete($Id)
    {
        $query=sprintf(
            "UPDATE ".$this->tableName." SET active=0 WHERE id = %d",
            $Id, "int"
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getEnfoquesByOrgId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE tipo=1 AND user_id= ".$Id;

         return $this->advancedQueryByObjetc($query);

            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function advancedQuery($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $row_consulta);
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function createObject($row)
    {
        $x = new Publicaciones();
        $x->setId($row['id']);
        $x->setFechaCreado($row['fechaCreado']);
        $x->setTipo($row['tipo']);
        $x->setUser_id($row['user_id']);
        $x->setActive($row['active']);

        $x->setIsPublic($row['is_public']);
        $x->setByAdmin($row['by_admin']);
        $x->setByExpert($row['by_expert']);

        if (isset($row['_calificacion'])) {
            $x->setRating($row['_calificacion']);
        } else {
            $x->setRating(0);
        }

        return $x;
    }

}
