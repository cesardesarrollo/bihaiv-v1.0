<?php
require_once 'DTO/Base.php';
require_once 'DTO/RespuestasUsuario.php';

class DaoRespuestasUsuario extends base
{
    
    public $tableName="respuestasusuario"; 

    public function add(RespuestasUsuario $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (fechaRespondio, OpcionesRespuesta_idOpcionesRespuesta, user_id) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getFechaRespondio(), "date"),
              $this->GetSQLValueString($x->getOpcionesRespuesta_idOpcionesRespuesta(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(RespuestasUsuario $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET fechaRespondio=%s, OpcionesRespuesta_idOpcionesRespuesta=%s, user_id=%s  WHERE idRespuestasUsuario = %s",
              $this->GetSQLValueString($x->getFechaRespondio(), "date"),
              $this->GetSQLValueString($x->getOpcionesRespuesta_idOpcionesRespuesta(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idRespuestasUsuario=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idRespuestasUsuario= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new RespuestasUsuario();
        $x->setId($row['idRespuestasUsuario']);
        $x->setFechaRespondio($row['fechaRespondio']);
        $x->setOpcionesRespuesta_idOpcionesRespuesta($row['OpcionesRespuesta_idOpcionesRespuesta']);
        $x->setUsuario_id($row['user_id']); 
        return $x;
    }

}
