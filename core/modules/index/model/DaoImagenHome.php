<?php
require_once 'DTO/Base.php';
require_once 'DTO/ImagenHome.php';

class DaoImagenHome extends base
{

    public $tableName="imagenhome";

    public function add(ImagenHome $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (llaveImg, titulo, descripcion, description, portada) VALUES (%s, %s, %s, %s,%s)",
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getDescription(), "text"),
              $this->GetSQLValueString($x->getPortada(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }


    public function update(ImagenHome $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET llaveImg=%s, titulo=%s, descripcion=%s, description=%s, portada=%s WHERE idImagenHome = %s",
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getDescription(), "text"),
              $this->GetSQLValueString($x->getPortada(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idImagenHome=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idImagenHome= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function createObject($row)
    {
        $x = new ImagenHome();
        $x->setId($row['idImagenHome']);
        $x->setLlaveImg($row['llaveImg']);
        $x->setTitulo($row['titulo']);
        $x->setDescripcion($row['descripcion']);
        $x->setDescription($row['description']);
        $x->setPortada($row['portada']);
        return $x;
    }

    public function getAll($offset=null,$limit=null,$buscar=null)
    {
        $query="";
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" WHERE (titulo LIKE '%".$buscar."%' OR descripcion LIKE '%".$buscar."%')";
        }
        $query="SELECT * FROM ".$this->tableName." ".$queryBuscar." ".$query;
         return $this->advancedQueryByObjetc($query);
    }


    public function getAllInPortada($offset=null,$limit=null,$buscar=null)
    {
        $query="";
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" AND (nombre LIKE '%".$buscar."%' OR descripcion LIKE '".$buscar."')";
        }
        $query="SELECT * FROM ".$this->tableName." WHERE portada = 1 ".$queryBuscar." ".$query;
         return $this->advancedQueryByObjetc($query);
    }
    public function getVideoHome()
    {
        $sql = "SELECT * FROM videoshome WHERE portada = 1";
        if (!$video = $this -> getOneRow($sql) ) {
            $video = array();
        }
        return $video;
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }

    /* Videos */
    public function getAllVideosHome()
    {
        $resultSet =  $this -> getAllRows("SELECT * FROM videoshome WHERE seccion = 'CARGADOS'");
        if (!empty($resultSet)) {
            foreach ($resultSet as $k => $v) {
                $resultSet[$k]['portada'] = ($v['portada'] == 1) ? 'Si' : 'No';
                $resultSet[$k]['acciones'] = '<a href="#" class="btn btn-default open-modal" data-id="'.$v['idVideosHome'].'">Editar</a>';
                $resultSet[$k]['acciones'] .= '<a href="#" class="btn btn-default delete-tipo" data-id="'.$v['idVideosHome'].'">Eliminar</a>';
            }
        }
        return $resultSet;
    }
    public function saveVideoHome($data)
    {
        /* validate data */
        if (empty($data['descripcion'])) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Descripción es un campo requerido';
            return false;
        }elseif (empty($data['description'])) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Description es un campo requerido';
            return false;
        }

        if (empty($data)) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Ocurrio un error inesperado';
            return false;
        }
        if (empty($data['descripcion'])) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'La descripción no puede estar vacía.';
            return false;
        }
        if (empty($data['description'])) {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'La descripción en ingles no puede estar vacía.';
            return false;
        }
        if (!isset($data['portada'])) {
            $data['portada'] = '0';
        } else {
            $data['portada'] == 1;
            $this -> _cnn -> query("UPDATE `videoshome` SET `portada`='0'");
        }
        if (!empty($_FILES)) {
            /* Upload video to server */
            /* -------------------------------------------------------------------- */
            $target_dir = "../admin/files/videos-home/";
            /* create directory if not exist*/
            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }
            $videoExtension = pathinfo(basename($_FILES["video"]["name"]), PATHINFO_EXTENSION);
            $databaseFileName = md5(date('Ymd_h:i:s')).'.'.$videoExtension;
            $target_file = $target_dir . $databaseFileName;
            /* validar extension del archivo*/
            if ($videoExtension != 'mp4') {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Solo el formato .mp4 es valido, intentalo de nuevo con el formato correcto.';
                return false;
            }
            if (!move_uploaded_file($_FILES["video"]["tmp_name"], $target_file)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrio un error al intentar subir el video, intentelo mas tarde.';
                return false;
            }
            /* -------------------------------------------------------------------- */
        }
        if (empty($data['id'])) {
            // Guardar
            if (empty($_FILES)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrio un error al intentar subir el video, intentelo mas tarde.';
                return false;
            }
            $sql = "INSERT INTO `videoshome` (`ruta`, `descripcion`, `description`, `portada`, `seccion`) VALUES ('$databaseFileName', '".$data['descripcion']."', '".$data['description']."', '".$data['portada']."', 'CARGADOS')";
            if (!$Result1 = $this -> _cnn -> query($sql)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
                return false;
            } else {
                $this -> response['class']  = 'success';
                $this -> response['msg']    = 'Video guardado con éxito';
                return true;
            }
        } else {
            // Actualizar
            $id = $data['id'];
            if (!empty($_FILES)) {
                $sql = "UPDATE `videoshome` SET `ruta`='$databaseFileName', `descripcion`='".$data['descripcion']."', `description`='".$data['description']."', `portada`='".$data['portada']."' WHERE `idVideosHome`='$id'";
            } else {
                $sql = "UPDATE `videoshome` SET `descripcion`='".$data['descripcion']."', `description`='".$data['description']."', `portada`='".$data['portada']."' WHERE `idVideosHome`='$id'";
            }
            if (!$Result1 = $this -> _cnn -> query($sql)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
                return false;
            } else {
                $this -> response['class']  = 'info';
                $this -> response['msg']    = 'Video actualizado con éxito';
                return true;
            }
        }
    }
    public function deleteVideoHome($data)
    {
        if (isset($data['id'])) {
            $id = $data['id'];
            $sql = "DELETE FROM `videoshome` WHERE `idVideosHome`='$id'";
            if (!$Result1 = $this -> _cnn -> query($sql)) {
                $this -> response['status'] = false;
                $this -> response['class']  = 'warning';
                $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
                return false;
            } else {
                $this -> response['class']  = 'success';
                $this -> response['msg']    = 'Video eliminado correctamente';
                return true;
            }
        } else {
            $this -> response['status'] = false;
            $this -> response['class']  = 'warning';
            $this -> response['msg']    = 'Ocurrío un error, intentalo mas tarde';
            return false;
        }
    }
}
