<?php
require_once 'DTO/Base.php';
require_once 'DTO/Likes.php';

class DaoLikes extends base
{
    
    public $tableName="likes"; 

    public function add(Likes $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (id_pos, fecha_creado, id_usu) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getId_pos(), "int"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getId_usu(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }

    public function update(Likes $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET id_pos=%s, fecha_creado=%s, id_usu=%s  WHERE id_like = %s",
              $this->GetSQLValueString($x->getId_pos(), "int"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id_like=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id_like= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new Likes();
        $x->setId($row['id_like']);
        $x->setId_pos($row['id_pos']);
        $x->setFecha_creado($row['fecha_creado']);
        $x->setId_usu($row['id_usu']); 
        return $x;
    } 
    
            
}
