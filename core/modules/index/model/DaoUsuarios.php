<?php
require_once 'DTO/Base.php';
require_once 'DTO/Usuarios.php';

class DaoUsuarios extends base
{

    public $tableName="user";

    public function add(Usuarios $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (nombre, email, pass, recoverPass, fecha_creado, activo, tipoRel, llaveImg) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getEmail(), "text"),
              $this->GetSQLValueString($x->getPass(), "text"),
              $this->GetSQLValueString($x->getRecoverPass(), "text"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getLlaveImg(), "text")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function getAll()
    {
        $query = "SELECT
              *
          FROM
              ".$this->tableName."
          WHERE
              is_active = 1 AND is_valid = 1
                  AND tipoRel = 1
          ORDER BY name";
        $resultSet = $this -> _cnn -> query($query);
        $rows = array();
        while ($fields = $resultSet -> fetch_assoc()){
            $rows [ ] = $fields;
        }
        $resultSet -> close();
        return $rows;
    }

    public function update(Usuarios $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET nombre=%s, email=%s, pass=%s, recoverPass=%s, fecha_creado=%s, activo=%s, tipoRel=%s, llaveImg=%s  WHERE idUsuarios = %s",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getEmail(), "text"),
              $this->GetSQLValueString($x->getPass(), "text"),
              $this->GetSQLValueString($x->getRecoverPass(), "text"),
              $this->GetSQLValueString($x->getFecha_creado(), "date"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idUsuarios=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idUsuarios= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function createObject($row)
    {
        $x = new Usuarios();
        $x->setId($row['idUsuarios']);
        $x->setNombre($row['nombre']);
        $x->setEmail($row['email']);
        $x->setPass($row['pass']);
        $x->setRecoverPass($row['recoverPass']);
        $x->setFecha_creado($row['fecha_creado']);
        $x->setActivo($row['activo']);
        $x->setTipoRel($row['tipoRel']);
        $x->setLlaveImg($row['llaveImg']);
        return $x;
    }

}
