<?php
require_once 'DTO/Base.php';
require_once 'DTO/RedesExperto.php';

class DaoRedesExperto extends base
{
    
    public $tableName="redesexperto"; 

    public function add(RedesExperto $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (nombre, url, experto_idExperto) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getUrl(), "text"),
              $this->GetSQLValueString($x->getExperto_idExperto(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(RedesExperto $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET nombre=%s, url=%s, experto_idExperto=%s  WHERE id_red = %s",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getUrl(), "text"),
              $this->GetSQLValueString($x->getExperto_idExperto(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id_red=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id_red= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function createObject($row)
    {
        $x = new RedesExperto();
        $x->setId($row['id_red']);
        $x->setNombre($row['nombre']);
        $x->setUrl($row['url']);
        $x->setExperto_idExperto($row['experto_idExperto']); 
        return $x;
    }
    
    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
            return $resp;
    }
    
    public function getAll($id_experto)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE experto_idExperto= ".$id_experto;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getByNombre($Id,$nombre)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE experto_idExperto= ".$Id." AND nombre='".$nombre."'";
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }
}
