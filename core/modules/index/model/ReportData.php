<?php
/**
* @class ReportData
* @brief Modelo de base de datos para la tabla de reportes
**/
class ReportData
{
    public static $tablename = "report";


    public function ReportData()
    {
        $this->sender_id = "";
        $this->receptor_id = "";
        $this->is_readed = "";
        $this->is_accepted = "";
        $this->created_at = "NOW()";
        $this->comment = "";
        $this->type = "";
        $this->id = "";
        $this->comments_response = "";
    }

    public function getSender()
    {
        return UserData::getById($this->sender_id); 
    }
    
    public function getReceptor()
    {
        return UserData::getById($this->receptor_id); 
    }

    public function add()
    {
        $sql = "insert into report (sender_id,receptor_id,created_at,comment,type) ";
        $sql .= "value (\"$this->sender_id\",\"$this->receptor_id\",$this->created_at,\"$this->comment\",\"$this->type\")";
        return Executor::doit($sql);
    }

    public function delete()
    {
        $sql = "delete from ".self::$tablename." where id=$this->id";
        Executor::doit($sql);
    }

    public function accept($comments_response)
    {
        
        $sql = "select * from ".self::$tablename." where id=$this->id";
        $query = Executor::doit($sql);
        $Report = Model::one($query[0], new ReportData());
        if ($Report->comments_response !== "") {
            if ($comments_response !== "") {
                $new_comment = $comments_response." | ".$Report->comments_response;
            } else {
                $new_comment = $Report->comments_response;
            }
        } else {
            $new_comment = $comments_response;
        }
        $sql = "update ".self::$tablename." set is_accepted=1, comments_response='$new_comment' where id=$this->id";
        Executor::doit($sql);
    }

    public function read($comments_response)
    {
        $sql = "select * from ".self::$tablename." where id=$this->id";
        $query = Executor::doit($sql);
        $Report = Model::one($query[0], new ReportData());
        if ($Report->comments_response !== "") {
            if ($comments_response !== "") {
                $new_comment = $comments_response." | ".$Report->comments_response;
            } else {
                $new_comment = $Report->comments_response;
            }
        } else {
            $new_comment = $comments_response;
        }
        $sql = "update ".self::$tablename." set comments_response='$new_comment', is_readed=1 where id=$this->id";
        Executor::doit($sql);
    }

    public static function getById($id)
    {
        $sql = "select * from ".self::$tablename." where id=$id";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ReportData());
    }
    public static function countUnReads($user_id)
    {
        $sql = "select count(*) as c from ".self::$tablename." where receptor_id=$user_id and is_readed=0";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ReportData());
    }

    public static function countReports($user_id)
    {
        $sql = "select count(*) as c from ".self::$tablename." where (sender_id=$user_id or receptor_id=$user_id) and is_accepted=1";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ReportData());
    }

    public static function getReportship($user_id,$report_id)
    {
        $sql = "select * from ".self::$tablename." where (sender_id=$user_id and receptor_id=$report_id) or (receptor_id=$user_id and sender_id=$report_id) ";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ReportData());
    }

    public static function getAll()
    {
        $sql = "select * from ".self::$tablename;
        $query = Executor::doit($sql);
        return Model::many($query[0], new ReportData());

    }

    public static function getAllAdvanced($offset=null,$limit=null,$buscar=null)
    {

        $query="";
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" WHERE t.created_at LIKE '%".$buscar."%' OR u.name LIKE '%".$buscar."%' ";
        }
            
        $sql = "select t.* from ".self::$tablename ." AS t INNER JOIN user AS u ON u.id = t.receptor_id ". $queryBuscar." ".$query;
        $query = Executor::doit($sql);
        return Model::many($query[0], new ReportData());

    }

    public static function getReports($user_id)
    {
        $sql = "select * from ".self::$tablename." where (sender_id=$user_id or receptor_id=$user_id) and is_accepted=1";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ReportData());
    }

    public static function getUnAccepteds($user_id)
    {
        $sql = "select * from ".self::$tablename." where receptor_id=$user_id and is_accepted=0";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ReportData());
    }
    
    public static function getLike($q)
    {
        $sql = "select * from ".self::$tablename." where name like '%$q%'";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ReportData());
    }


}

?>
