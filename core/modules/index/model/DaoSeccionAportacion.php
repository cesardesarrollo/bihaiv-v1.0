<?php
require_once 'DTO/Base.php';
require_once 'DTO/SeccionAportacion.php';

class DaoSeccionAportacion extends base
{

    public $tableName="seccionesaportacion";

    public function update(SeccionAportacion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET titulo=%s, descripcion=%s, title=%s, description=%s, url=%s, activo=%s  WHERE idSeccionAportacion = %s",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getTitle(), "text"),
              $this->GetSQLValueString($x->getDescription(), "text"),
              $this->GetSQLValueString($x->getURL(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function getAll($status=[0,1],$where=[],$offset=null,$limit=null)
    {
        $conditions = "";
        foreach ($where as $key => $value) {
            $conditions .= " AND ".$key."=".$value; 
        }

        if ($offset!=null && $limit!=null) {
            $conditions .= " LIMIT ".$limit." OFFSET ".$offset;
        }

        $status = implode(", ", $status);

        $query = "SELECT * FROM ".$this->tableName." WHERE activo IN (".$status.") ".$conditions;
        
        return $this->advancedQueryByObjetc($query);
    }

    public function getAllWithSumary()
    {

        $query = "SELECT * FROM ".$this->tableName." WHERE idSeccionAportacion > 2";
        
        return $this->advancedQueryByObjetc($query);
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idSeccionAportacion= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al seleccionar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function advancedQueryByObjetc($query)
    {
        $resp = array();
        $new_query = $this->_cnn->query($query);

        if (!$new_query) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_query = $new_query->fetch_assoc();
            $total_rows_query = $new_query->num_rows;
            if ($total_rows_query > 0) {
                do{
                    array_push($resp, $this->createObject($row_query));
                }while($row_query = $new_query->fetch_assoc());
            }
        }

        return $resp;
    }

    public function createObject($row)
    {
        $x = new SeccionAportacion();
        $x->setId($row['idSeccionAportacion']);
        $x->setTitulo($row['titulo']);
        $x->setDescripcion($row['descripcion']);
        $x->setTitle($row['title']);
        $x->setDescription($row['description']);
        $x->setURL($row['url']);
        $x->setActivo($row['activo']);

        return $x;
    }

}
