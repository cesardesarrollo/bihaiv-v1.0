<?php
require_once 'DTO/Base.php';
require_once 'DTO/CalificacionEnfoque.php';

class DaoCalificacionEnfoque extends base
{
    
    public $tableName="calificacionenfoque"; 

    public function add(CalificacionEnfoque $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (calificacion, fechaCalificacion, Enfoque_idEnfoque, user_id) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getCalificacion(), "int"),
              $this->GetSQLValueString($x->getFechaCalificacion(), "date"),
              $this->GetSQLValueString($x->getEnfoque_idEnfoque(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(CalificacionEnfoque $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET calificacion=%s, fechaCalificacion=%s, Enfoque_idEnfoque=%s, user_id=%s  WHERE idCalificacionEnfoque = %s",
              $this->GetSQLValueString($x->getCalificacion(), "int"),
              $this->GetSQLValueString($x->getFechaCalificacion(), "date"),
              $this->GetSQLValueString($x->getEnfoque_idEnfoque(), "int"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idCalificacionEnfoque=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCalificacionEnfoque= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new CalificacionEnfoque();
        $x->setId($row['idCalificacionEnfoque']);
        $x->setCalificacion($row['calificacion']);
        $x->setFechaCalificacion($row['fechaCalificacion']);
        $x->setEnfoque_idEnfoque($row['Enfoque_idEnfoque']); 
        $x->setUsuario_id($row['user_id']);
        return $x;
    }
   
            
    public function getCalificacionesByIdEnfoque($Id_enfoque)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Enfoque_idEnfoque=".$Id_enfoque;
         return $this->advancedQuery($query);
    }


}
