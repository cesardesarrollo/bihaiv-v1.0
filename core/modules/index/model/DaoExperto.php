<?php
require_once 'DTO/Base.php';
require_once 'DTO/Experto.php';

class DaoExperto extends base
{

    public $tableName="experto";

    public function add(Experto $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (ubicacion, organizacion, tipoOrganizacion, rangoExperiencia, aniosExperiencia, cargoActual, cargoAnterior, bibliografia, Usuarios_idUsuarios,nonce, Acepto,lat,lng, telefono) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getUbicacion(), "int"),
              $this->GetSQLValueString($x->getOrganizacion(), "text"),
              $this->GetSQLValueString($x->getTipoOrganizacion(), "text"),
              $this->GetSQLValueString($x->getRangoExperiencia(), "text"),
              $this->GetSQLValueString($x->getAniosExperiencia(), "text"),
              $this->GetSQLValueString($x->getCargoActual(), "text"),
              $this->GetSQLValueString($x->getCargoAnterior(), "text"),
              $this->GetSQLValueString($x->getBibliografia(), "text"),
              $this->GetSQLValueString($x->getUsuarios_idUsuarios(), "int"),
              $this->GetSQLValueString($x->getNonce(), "text"),
              $this->GetSQLValueString($x->getAcepto(), "int"),
              $this->GetSQLValueString($x->getLat(), "text"),
              $this->GetSQLValueString($x->getLng(), "text"),
              $this->GetSQLValueString($x->getTelefono(), "text")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Experto $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET ubicacion=%s, organizacion=%s, tipoOrganizacion=%s, rangoExperiencia=%s, aniosExperiencia=%s, cargoActual=%s, cargoAnterior=%s, bibliografia=%s, Usuarios_idUsuarios=%s,nonce=%s, Acepto=%s,lat=%s,lng=%s, telefono=%s  WHERE idExperto = %s",
              $this->GetSQLValueString($x->getUbicacion(), "int"),
              $this->GetSQLValueString($x->getOrganizacion(), "text"),
              $this->GetSQLValueString($x->getTipoOrganizacion(), "text"),
              $this->GetSQLValueString($x->getRangoExperiencia(), "text"),
              $this->GetSQLValueString($x->getAniosExperiencia(), "text"),
              $this->GetSQLValueString($x->getCargoActual(), "text"),
              $this->GetSQLValueString($x->getCargoAnterior(), "text"),
              $this->GetSQLValueString($x->getBibliografia(), "text"),
              $this->GetSQLValueString($x->getUsuarios_idUsuarios(), "int"),
              $this->GetSQLValueString($x->getNonce(), "text"),
              $this->GetSQLValueString($x->getAcepto(), "int"),
              $this->GetSQLValueString($x->getLat(), "text"),
              $this->GetSQLValueString($x->getLng(), "text"),
              $this->GetSQLValueString($x->getTelefono(), "text"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idExperto=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idExperto= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getExpertByUserId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Usuarios_idUsuarios= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByNonce($nonce)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE nonce= '".$nonce."'";
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getAll($offset=null,$limit=null,$buscar=null)
    {

        $query="";
        if ($offset!=null && $limit!=null) {
            $query=" LIMIT ".$limit." OFFSET ".$offset;
        }
        $queryBuscar="";
        if ($buscar!=null) {
            $queryBuscar=" AND (user.name LIKE '%".$buscar."%' OR user.email LIKE '".$buscar."')";
        }

        $query="SELECT * FROM ".$this->tableName."
               JOIN user ON ".$this->tableName.".Usuarios_idUsuarios=user.id
               WHERE user.activo=1 AND ".$this->tableName.".Acepto=1 AND user.tipoRel=2
                     ".$queryBuscar." ".$query;
        return $this->advancedQueryByObjetc($query);
    }

    public function getAllExperts()
    {
        $query = "SELECT
            u.id AS _id,
            u.name AS _name,
            u.imagen AS _avatar,
            e.organizacion AS _organizacion,
            e.ubicacion AS _ubicacion
        FROM
            user u
                INNER JOIN
            experto e ON u.id = e.Usuarios_idUsuarios
      WHERE u.activo = 1 and  e.acepto=1";
        return $this->advancedQuery($query);
    }

    public function getByUserId($Id)
    {
        $query="select u.name as _name, u.imagen as _image, ex.idExperto as _idExperto, ex.organizacion as _organizacion, ex.tipoOrganizacion as _tipoOrganizacion, ex.rangoExperiencia as _rango, ex.aniosExperiencia as _anios, ex.cargoActual as _cargoActual, ex.cargoAnterior as _cargoAnterior, ex.bibliografia as _bio, ex.telefono as _telefono, (SELECT url FROM redesexperto WHERE experto_idExperto = _idExperto AND nombre ='facebook') as _facebook, (SELECT url FROM redesexperto WHERE experto_idExperto = _idExperto AND nombre ='twitter') as _twitter, (SELECT url FROM redesexperto WHERE experto_idExperto = _idExperto AND nombre ='linkedin') as _linkedin from user u inner join experto ex on u.id = ex.Usuarios_idUsuarios where ex.Usuarios_idUsuarios = ".$Id;

        return $this->advancedQuery($query);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }


    public function createObject($row)
    {
        $x = new Experto();
        $x->setId($row['idExperto']);
        $x->setUbicacion($row['ubicacion']);
        $x->setOrganizacion($row['organizacion']);
        $x->setTipoOrganizacion($row['tipoOrganizacion']);
        $x->setRangoExperiencia($row['rangoExperiencia']);
        $x->setAniosExperiencia($row['aniosExperiencia']);
        $x->setCargoActual($row['cargoActual']);
        $x->setCargoAnterior($row['cargoAnterior']);
        $x->setBibliografia($row['bibliografia']);
        $x->setUsuarios_idUsuarios($row['Usuarios_idUsuarios']);
        $x->setNonce($row['nonce']);
        $x->setAcepto($row['Acepto']);
        $x->setLat($row['lat']);
        $x->setLng($row['lng']);
        $x->setTelefono($row['telefono']);
        return $x;
    }


}
