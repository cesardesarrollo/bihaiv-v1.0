<?php
require_once 'DTO/Base.php';
require_once 'DTO/Objetivo.php';
require_once 'DaoColaboracion.php';
require_once 'DaoOrganizacion.php';
class ObjetivoEvento extends base
{

    public function ObjetivoEvento()
    {
        $this->objetivo_id = "";
        $this->evento_id = "";
    }
}

class DaoObjetivo extends base
{

    public $tableName="objetivo";

    public $daocolaboracion;

    public function add(Objetivo $x)
    {
        $query=sprintf(
            "INSERT INTO ".$this->tableName." (descripcion) VALUES (%s)",
            $this->GetSQLValueString($x->getDescripcion(), "text")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Objetivo $x)
    {
        $query=sprintf(
            "UPDATE ".$this->tableName." SET descripcion=%s WHERE id = %s",
            $this->GetSQLValueString($x->getDescripcion(), "text"),
            $this->GetSQLValueString($x->getId(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
                return $x->getId();
        }
    }

    public function deleteByEventId($Id)
    {
        $query = sprintf("DELETE FROM objetivo_evento WHERE evento_id = ".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id = ".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function addObjetivoEvento($objetivo_id, $evento_id)
    {
        $query=sprintf(
            "INSERT INTO objetivo_evento (objetivo_id, evento_id) VALUES (%s, %s)",
            $this->GetSQLValueString($objetivo_id, "text"),
            $this->GetSQLValueString($evento_id, "text")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function addObjetivoIniciativa($objetivo_id, $iniciativa_id)
    {
        $query=sprintf(
            "INSERT INTO objetivo_iniciativa (objetivo_id, iniciativa_id) VALUES (%s, %s)",
            $this->GetSQLValueString($objetivo_id, "text"),
            $this->GetSQLValueString($iniciativa_id, "text")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }
    
    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
        return $this->advancedQueryByObjetc($query);
    }


    public function getById($id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = $id";
        return $this->advancedQueryByObjetc($query);
    }
      
    public function getAllByEventId($id)
    {
        $sql="SELECT * FROM objetivo_evento WHERE evento_id = $id";
        $query = Executor::doit($sql);
        $response = Model::many($query[0], new ObjetivoEvento());

        $resp = array();
       
        foreach ($response as $objetivo_evento) {
            $objetivo = self::getById($objetivo_evento->objetivo_id);
            $resptemp= array();
            $resptemp['id'] =$objetivo_evento->objetivo_id;
            $resptemp['descripcion'] = $objetivo[0]['descripcion'];
            $resptemp['stars'] ='';
            $resptemp['calificacion'] = 0;
            $resp[]= $resptemp;
        }

        $daocolaboracion=new DaoColaboracion();
        $colaboradores=$daocolaboracion->getColaboradoresByIdEvento($id);

        $universo_actores = $descargas = 0;
        if ($universoResultSet = $this -> getOneRow("SELECT COUNT(*) as universo FROM colaboracion WHERE evento_id = ". $id) ) {
            $universo_actores = $universoResultSet['universo'];
        }
        if ($descargasResultSet = $this -> getOneRow("SELECT COUNT(*) as descargas FROM colaboracion WHERE evento_id =". $id."  AND estatus = 1") ) {
            $descargas = $descargasResultSet['descargas'];
        }

        for($i=0; $i<count($resp); $i++)
        {
            /* Calidad de un objetivo */
            $calidad = 0; //Promedio de calificación
            $sql="SELECT
                IFNULL(  AVG(oihc.calificacion)  , 0  ) AS calificacion,
                oihc.organizacion_id,
                oihc.objetivo_id,
                oihc.evento_id,
                o.descripcion
            FROM
                objetivo_evento_has_calificacion oihc
                    JOIN
                objetivo o
            ON o.id = oihc.objetivo_id
            WHERE
          o.id =".$resp[$i]['id']." and oihc.evento_id=".$id;
            if ($calidadResultSet = $this-> getOneRow($sql) ) {
                $calidad = $calidadResultSet['calificacion'];
            }

            $resp[$i]['calificacion'] = round($calidad);  


            if (count($colaboradores)==0) {
                 $resp[$i]['stars'] = self::getStars($resp[$i]['calificacion']);
              
            }else 
            {
                foreach ($colaboradores as $colaborador) {
                    if ($colaborador["userid"]==$_SESSION['user_id']) {
                         $resp[$i]['stars'] = '<div class="rateYo-obejtivos-eventos" data-calificacion="0" data-idobjetivoevento="'.$resp[$i]['id'].'"></div>';

                    }
                }
                if ($resp[$i]['stars']=='') {
                    $resp[$i]['stars'] = self::getStars($resp[$i]['calificacion']);
                }
            }
        } 
        
        echo json_encode($resp);
        
    }


    public function advancedQueryByObjetc($query)
    {
        $consulta = $this->getAllRows($query);
        if (!$consulta) {
            return array();
        }
        return $consulta;
    }

    public function createObject($row)
    {
        $x = new Objetivo();
        $x->setId($row['id']);
        $x->setDescripcion($row['descripcion']);
        return $x;
    }


    /* HER COMES NEW CHALLENGER!!*/
    public function updateRating($data)
    {
        $query = "INSERT INTO `objetivo_evento_has_calificacion` (`calificacion`, `organizacion_id`, `objetivo_id`, `evento_id`) VALUES (".$data['calificacion'].", ".$data['organizacion_id'].", ".$data['objetivo_id'].", ".$data['evento_id'].") ON DUPLICATE KEY UPDATE calificacion = ".$data['calificacion'];
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            /* Validamos que nuestra publicacion cumpla con los requisitos para
            * aparecer en nuestra base datos publica, en caso de que se cumplan los
            * requisitos se actualiza la bandera en la tabla publicaciones.
            */
            return $Result1;
        }
    }

    public function getStars($calificacion)
    {
        $stars = '';
        for ($i = 1; $i <= 5 ; $i++) {
            if ($calificacion == 0) {
                $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
            }elseif ($calificacion < $i) {
                $stars .= '<i style="color: orange;" class="fa fa-star-o" aria-hidden="true"></i>';
            } else {
                $stars .= '<i style="color: orange;" class="fa fa-star" aria-hidden="true"></i>';
            }
        }
        return $stars;
    }

}
