<?php
require_once 'DTO/Base.php';
require_once 'DTO/UbicacionesIniciativas.php';

class DaoUbicacionesIniciativas extends base
{
    
    public $tableName="ubicaciones_iniciativas"; 

    public function add(UbicacionesIniciativas $x)
    {

        $query=sprintf(
            "INSERT INTO ".$this->tableName." (idPais, idEstado, idMunicipio, idLocalidad, idIniciativa, is_main) VALUES (%s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getIdPais(), "int"),
            $this->GetSQLValueString($x->getIdEstado(), "int"),
            $this->GetSQLValueString($x->getIdMunicipio(), "int"),
            $this->GetSQLValueString($x->getIdLocalidad(), "int"),
            $this->GetSQLValueString($x->getIdIniciativa(), "int"),
            $this->GetSQLValueString($x->getIsMain(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }

    }

    public function update(UbicacionesIniciativas $x)
    {
        $query=sprintf(
            "UPDATE ".$this->tableName." SET idPais=%s, idEstado=%s, idMunicipio=%s, idLocalidad=%s, idIniciativa=%s, is_main=%s  WHERE id = %s",
            $this->GetSQLValueString($x->getIdPais(), "int"),
            $this->GetSQLValueString($x->getIdEstado(), "int"),
            $this->GetSQLValueString($x->getIdMunicipio(), "int"),
            $this->GetSQLValueString($x->getIdLocalidad(), "int"),
            $this->GetSQLValueString($x->getIdIniciativa(), "int"),
            $this->GetSQLValueString($x->getIsMain(), "int"),
            $this->GetSQLValueString($x->getId(), "int")
        );
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
                return $x->getId();
        }
    }

    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }    

    public function getPaisIdByIniId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idIniciativa = ".$Id." LIMIT 0,1";
        return $this->advancedQueryByObjetc($query);
    }

    public function getActiveByIniId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE is_main = 1 AND idIniciativa = ".$Id;
        return $this->advancedQueryByObjetc($query);
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE id=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
        return $resp;
    }

    public function getByIniciativaId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idIniciativa = ".$Id;
        return $this->advancedQueryByObjetc($query);
    }

    public function createObject($row)
    {
        $x = new UbicacionesIniciativas();
        $x->setId($row['id']);
        $x->setIdPais($row['idPais']);
        $x->setIdEstado($row['idEstado']);
        $x->setIdMunicipio($row['idMunicipio']);
        $x->setIdLocalidad($row['idLocalidad']);
        $x->setIdIniciativa($row['idIniciativa']);
        $x->setIsMain($row['is_main']);
        return $x;
    }

    // Obtiene alcances definidos en tabla alcanze
    public function getAlcanzes()
    {
        $query="SELECT * FROM alcanze";
        return $this->advancedQuery($query);
    }
}
