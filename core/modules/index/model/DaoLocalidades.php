<?php
require_once 'DTO/Base.php';
require_once 'DTO/Localidades.php';

class DaoLocalidades extends base
{
    
    public $tableName="localidades"; 

    public function getAll()
    {
        $query="SELECT * FROM ".$this->tableName;
         return $this->advancedQueryByObjetc($query);
    }
    
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE id = ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }
    
    public function getByName($nombre)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE nombre = '".$nombre."'";
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getByMunicipioId($MunicipioId)
    {
        $query="select id, clave, nombre from ".$this->tableName." WHERE municipio_id = ".$MunicipioId;
        return $this->advancedQueryByObjetc($query);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());  
            }
        }
            return $resp;
    }

    public function createObject($row)
    {
        $x = new Localidades();
        $x->setId($row['id']);
        $x->setClave($row['clave']);
        $x->setNombre($row['nombre']);
        /*$x->setLatitud($row['latitud']);
        $x->setLongitud($row['longitud']);
        $x->setLat($row['lat']);
        $x->setLng($row['lng']);
        $x->setAltitud($row['altitud']);
        $x->setActivo($row['activo']);
        $x->setMunicipioId($row['municipio_id']);*/
        return $x;
    }

}
