<?php
require_once 'DTO/Base.php';
require_once 'DTO/AsistenteEvento.php';

class DaoAsistenteEvento extends base
{
    
    public $tableName="asistenteevento"; 

    public function add(AsistenteEvento $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (fechaCreado, estatus, Evento_idEvento, user_id) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getFechaCreado(), "date"),
              $this->GetSQLValueString($x->getEstatus(), "text"),
              $this->GetSQLValueString($x->getEvento_idEvento(), "int"),
              $this->GetSQLValueString($x->getUser_id(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(AsistenteEvento $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET fechaCreado=%s, estatus=%s, Evento_idEvento=%s, user_id=%s  WHERE idAsistenteEvento = %s",
              $this->GetSQLValueString($x->getFechaCreado(), "date"),
              $this->GetSQLValueString($x->getEstatus(), "text"),
              $this->GetSQLValueString($x->getEvento_idEvento(), "int"),
              $this->GetSQLValueString($x->getUser_id(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idAsistenteEvento=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idAsistenteEvento= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }


    public function createObject($row)
    {
        $x = new AsistenteEvento();
        $x->setId($row['idAsistenteEvento']);
        $x->setFechaCreado($row['fechaCreado']);
        $x->setEstatus($row['estatus']);
        $x->setEvento_idEvento($row['Evento_idEvento']); 
        $x->setUser_id($row['user_id']);
        return $x;
    }
   
    public function getAsistentesByIdEve($Id_eve)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE Evento_idEvento=".$Id_eve;
         return $this->advancedQuery($query);
    }


}
