<?php
require_once 'DTO/Base.php';
require_once 'DTO/Iniciativa.php';

class DaoIniciativa extends base
{

    public $tableName="iniciativa";

    public function add(Iniciativa $x)
    {

          $query=sprintf(
              "INSERT INTO ".$this->tableName." (titulo, textoCorto, textoLargo, fechaIniciativa, fechaFin, horaIni, horaFin, llaveImg, post_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getTextoCorto(), "text"),
              $this->GetSQLValueString($x->getTextoLargo(), "date"),
              $this->GetSQLValueString($x->getFechaIniciativa(), "date"),
              $this->GetSQLValueString($x->getFechaFin(), "date"),
              $this->GetSQLValueString($x->getHoraIni(), "text"),
              $this->GetSQLValueString($x->getHoraFin(), "text"),
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getPost_idPost(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Iniciativa $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET titulo=%s, textoCorto=%s, textoLargo=%s, fechaIniciativa=%s, fechaFin=%s, horaIni=%s, horaFin=%s, llaveImg=%s, post_id=%s, idAlcanze=%s  WHERE idIniciativa = %s",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getTextoCorto(), "text"),
              $this->GetSQLValueString($x->getTextoLargo(), "date"),
              $this->GetSQLValueString($x->getFechaIniciativa(), "date"),
              $this->GetSQLValueString($x->getFechaFin(), "date"),
              $this->GetSQLValueString($x->getHoraIni(), "text"),
              $this->GetSQLValueString($x->getHoraFin(), "text"),
              $this->GetSQLValueString($x->getLlaveImg(), "text"),
              $this->GetSQLValueString($x->getPost_idPost(), "int"),
              $this->GetSQLValueString($x->getIdAlcanze(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id,$IdPost)
    {

        $query = sprintf("DELETE FROM objetivo_iniciativa WHERE iniciativa_id=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {

            throw new Exception("Error al eliminar objetivo_iniciativa: (" . $this->_cnn->errno . ") " . $this->_cnn->error);

        } else {

            $query = sprintf("DELETE FROM iniciativaeventos WHERE idIniciativa=".$Id);
            $Result1=$this->_cnn->query($query);
            if (!$Result1) {

                throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);

            } else {

                $query = sprintf("DELETE FROM ".$this->tableName." WHERE idIniciativa=".$Id);
                $Result1=$this->_cnn->query($query);
                if (!$Result1) {
                    throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                } else {
                        
                    $query = sprintf("DELETE FROM publicaciones WHERE id=".$IdPost);
                    $Result1=$this->_cnn->query($query);
                    if (!$Result1) {
                        throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                    } else {
                        return true;
                    }
                }

            }

        }

    }

    public function getIniciativaEventos($idIniciativa)
    {
        $query=sprintf(
            "SELECT * FROM iniciativaeventos WHERE idIniciativa=%s",
            $this->GetSQLValueString($idIniciativa, "int")
        );
        return $this->advancedQuery($query);
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idIniciativa= ".$Id;
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al recuperar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            if ($Result1->num_rows > 0) {
                return $this->createObject($Result1->fetch_assoc());
            } else {
                return false;
            }
            
        }
    }

    public function getByUserId($Id)
    {
        $sql="SELECT * FROM ".$this->tableName." AS i INNER JOIN publicaciones as p ON i.post_id = p.id WHERE p.user_id= ".$Id;
        $query = Executor::doit($sql);
        return Model::many($query[0], new Iniciativa());
    }

    public function getByUserIdWithLimits($Id, $limit_param1, $limit_param2)
    {
        $sql="SELECT * FROM ".$this->tableName." AS i INNER JOIN publicaciones as p ON i.post_id = p.id WHERE p.user_id= ".$Id." LIMIT ".$limit_param1.",".$limit_param2;
        $query = Executor::doit($sql);
        return Model::many($query[0], new Iniciativa());
    }

    public function getAll()
    {
        $sql = "select idIniciativa, upper(titulo) as titulo, post_id from ".$this->tableName." where fechaIniciativa >= ".date('Y-m-d');
        return $this->advancedQuery($sql);
    }

    public function advancedQueryByObjetc($query)
    {
            $resp=array();
            $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
            return $resp;
    }

    public function createObject($row)
    {
        $x = new Iniciativa();
        $x->setId($row['idIniciativa']);
        $x->setTitulo($row['titulo']);
        $x->setTextoCorto($row['textoCorto']);
        $x->setTextoLargo($row['textoLargo']);
        $x->setFechaIniciativa($row['fechaIniciativa']);
        $x->setFechaFin($row['fechaFin']);
        $x->setHoraIni($row['horaIni']);
        $x->setHoraFin($row['horaFin']);
        $x->setLugar($row['lugar']);
        $x->setLlaveImg($row['llaveImg']);
        $x->setPost_idPost($row['post_id']);
        $x->setIdAlcanze($row['idAlcanze']);
        return $x;
    }

    /* HER COMES NEW CHALLENGER!!*/
    public function updateRating($data)
    {
        $query = "INSERT INTO `objetivo_iniciativa_has_calificacion` (`calificacion`, `organizacion_id`, `objetivo_id`, iniciativa_id) VALUES (".$data['calificacion'].", ".$data['organizacion_id'].", ".$data['objetivo_id']." , ".$data['iniciativa_id'].") ON DUPLICATE KEY UPDATE calificacion = ".$data['calificacion'];
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
             throw  new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
        } else {
            /* Validamos que nuestra publicacion cumpla con los requisitos para
            * aparecer en nuestra base datos publica, en caso de que se cumplan los
            * requisitos se actualiza la bandera en la tabla publicaciones.
            */
        
        }
        return $Result1;
    }

    public function getByUserIdAndInCollaboration($id)
    {
        $query = "select Iniciativa_idIniciativa as iniciativa_id from colaboracion where estatus = 1 AND Iniciativa_idIniciativa > 0 AND user_id =" .$id;
        $iniciativas_colaboracion = $this->advancedQuery($query);

        $arr_ini = array();
        foreach($iniciativas_colaboracion as $iniciativa):
            $sql="SELECT * FROM ".$this->tableName." WHERE idIniciativa = ".$iniciativa['iniciativa_id'];
            $ini = $this->advancedQuery($sql);
            if (count($ini) > 0) {
                $arr_ini[] = $ini[0];
            }
        endforeach;

        $sql="SELECT * FROM ".$this->tableName." AS i INNER JOIN publicaciones as p ON i.post_id = p.id WHERE p.user_id= ".$id;
        $inis = $this->advancedQuery($sql);

        $arr_ini_final = array_merge($inis, $arr_ini);

        return $arr_ini_final;
    }
}
