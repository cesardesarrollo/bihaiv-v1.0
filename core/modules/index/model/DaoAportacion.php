<?php
require_once 'DTO/Base.php';
require_once 'DTO/Aportacion.php';

class DaoAportacion extends base
{

    public $tableName="aportacion";

    public function add(Aportacion $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (titulo, descripcion, fecha, thumbnail, file, idCategoriaAportacion, post_id) VALUES (%s ,%s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getFecha(), "date"),
              $this->GetSQLValueString($x->getThumbnail(), "text"),
              $this->GetSQLValueString($x->getFile(), "text"),
              $this->GetSQLValueString($x->getCategory(), "int"),
              $this->GetSQLValueString($x->getPost_idPost(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Aportacion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET titulo=%s, descripcion=%s, fecha=%s, thumbnail=%s, file=%s, idCategoriaAportacion=%s, post_id=%s  WHERE idAportacion = %s",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getDescripcion(), "text"),
              $this->GetSQLValueString($x->getFecha(), "date"),
              $this->GetSQLValueString($x->getThumbnail(), "text"),
              $this->GetSQLValueString($x->getFile(), "text"),
              $this->GetSQLValueString($x->getCategory(), "int"),
              $this->GetSQLValueString($x->getPost_idPost(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id,$IdPost)
    {

        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idAportacion=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {

            $query = sprintf("DELETE FROM publicaciones WHERE id=".$IdPost);
            $Result1=$this->_cnn->query($query);
            if (!$Result1) {
                throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                return true;
            }
        }

    }

    public function updateRating($data)
    {
        $query = "INSERT INTO `calificacionaportacion` (`calificacion`, `Aportacion_idAportacion`, `organizacion_id`) VALUES (".$data['calificacion'].", ".$data['id'].", ".$data['organizacion_id'].") ON DUPLICATE KEY UPDATE calificacion = ".$data['calificacion'];
      
        $Result1 = $this -> _cnn -> query($query);

        if (!$Result1) {
            // throw new Exception("Error al insertar: (" . $this -> _cnn -> errno . ") " . $this -> _cnn -> error);
            /* Erno  */
            $this -> response['msg']      = 'Ocurrio un error inesperado, contacte al administrador';
            $this -> response['class']    = 'danger';
            $this -> response['response'] = true;
        } else {
            if ($this -> _cnn -> affected_rows ==  1) {
                /* Se inserto la calificación por primera vez  */
                $this -> response['msg']      = 'Aportación calificada';
                $this -> response['class']    = 'success';
                $this -> response['response'] = true;
            }elseif ($this -> _cnn -> affected_rows ==  2 OR $this -> _cnn -> affected_rows ==  0) {
                /* La calificación se actualizo  */
                $this -> response['msg']      = 'Calificación actualizada';
                $this -> response['class']    = 'info';
                $this -> response['response'] = true;
            }
            /*
            * Validamos que nuestra publicacion cumpla con los requisitos para
            * aparecer en nuestra base datos publica, en caso de que se cumplan los
            * requisitos se actualiza la bandera en la tabla publicaciones.
            */
            if (isset($data['id'])) {
                $this -> evaluaAportacion($data['id']);
            }
            return $Result1;
        }
    }

    public function addVisitAportacion($aportacion_id = null)
    {
        $query = "UPDATE `aportacion` SET `visitas`= visitas + 1  WHERE `idAportacion`='$aportacion_id'";
        $Result1 = $this -> _cnn -> query($query);
        if (!$Result1) {
            return false;
        } else {
            $this -> evaluaAportacion($aportacion_id);
            return true;
        }
    }
    /*
    * Funcion para evaluar aportaciones en caso de aportacion cumpla con la
    * regla entonces pasa a la biblioteca de aportaciones publica.
    */
    private function evaluaAportacion($aportacion_id = null)
    {
        if (empty($aportacion_id) OR !is_numeric($aportacion_id)) {
            return false;
        }
        $sql = "SELECT
        	( SELECT IFNULL(ROUND(AVG(calificacion)), 0) AS calificacion FROM calificacionaportacion WHERE aportacion_idAportacion = a.idAportacion) AS calificacion,
          ( SELECT COUNT(calificacion) AS calificaron FROM calificacionaportacion WHERE aportacion_idAportacion = a.idAportacion) AS calificaron,
            a.*
        FROM
            aportacion a
        WHERE
      idAportacion = $aportacion_id";
        $aportacion = $this -> getOneRow($sql);

        if (empty($aportacion)) {
            return false;
        } else {

            /* Formula ========================================================== */

            /* Variables de formula */
            $UNIVERSO     = 0;
            $DESCARGAS    = $aportacion['visitas'];
            $calidad      = $aportacion['calificacion'];

            $descargas    = $DESCARGAS;
            $calificaron  = $aportacion['calificaron'];

            if ($universoResultSet = $this -> getOneRow('SELECT COUNT(*) as universo FROM user WHERE is_valid = 1 AND is_active = 1 AND activo = 1 AND tipoRel = 1') ) {
                $UNIVERSO = $universoResultSet['universo'];
            }

            /* Calcular alcance */
            $numerador   = (0.0025 * $DESCARGAS) * ( $UNIVERSO - 1 );
            $denominador = 0.25 * ( $UNIVERSO - $DESCARGAS );

            if ($DESCARGAS == $UNIVERSO) {
                $CONFIANZA = 1.96;
            } else {
                if ($denominador == 0) {
                    $CONFIANZA = 1.96;
                } else {
                    $CONFIANZA = sqrt($numerador / $denominador);
                }
            }

            $alcance = 0;
            if ($CONFIANZA < 1.28) {
                $alcance = 1;
            }elseif ($CONFIANZA >= 1.28 AND $CONFIANZA < 1.44) {
                $alcance = 1.41421356;
            }elseif ($CONFIANZA >= 1.44 AND $CONFIANZA < 1.65) {
                $alcance = 1.73205081;
            }elseif ($CONFIANZA >= 1.65 AND $CONFIANZA < 1.96) {
                $alcance = 2;
            }elseif ($CONFIANZA >= 1.96) {
                $alcance = 2.23606798;
            }

            /*
            * Asignar Calificacion a interes y Evalua el nivel de confianza
            */

            /* Calcular interes */
            $numerador = (0.0025 * $calificaron) * ( $descargas - 1 );
            $denominador = 0.25 * ( $descargas - $calificaron );

            if ($calificaron == $descargas) {
                $confianza = 1.96;
            } else {
                if ($denominador == 0) {
                    $confianza = 1.96;
                } else {
                    $confianza = sqrt($numerador / $denominador);
                }
            }

            $interes = 0;
            if ($confianza < 1.28) {
                $interes = 1;
            }elseif ($confianza >= 1.28 AND $confianza < 1.44) {
                $interes = 1.41421356;
            }elseif ($confianza >= 1.44 AND $confianza < 1.65) {
                $interes = 1.73205081;
            }elseif ($confianza >= 1.65 AND $confianza < 1.96) {
                $interes = 2;
            }elseif ($confianza >= 1.96) {
                $interes = 2.23606798;
            }

            /* Calcular nivel de impacto */
            $impacto = round((0.5 * $calidad) + (0.5 * $alcance * $interes));
            $impacto = ( $impacto < 1 ) ? 1 : $impacto;

            /*
            Core::debug( '$UNIVERSO' );
            Core::debug( $UNIVERSO );
            Core::debug( '$DESCARGAS');
            Core::debug( $DESCARGAS );
            Core::debug( '$calidad' );
            Core::debug( $calidad );
            Core::debug( '$CONFIANZA' );
            Core::debug( $CONFIANZA );
            Core::debug( '$alcance' );
            Core::debug( $alcance );
            Core::debug( '$confianza' );
            Core::debug( $confianza );
            Core::debug( '$interes' );
            Core::debug( $interes );
            Core::debug( '$impacto' );
            Core::debug( $impacto );
            */

            if ($impacto >= 3) {
                /* Pasa a la biblioteca de aportaciones publica */
                $sql = "UPDATE `publicaciones` SET `is_public` = '1' WHERE `id`= '".$aportacion['post_id']."'";
            } else {
                /* Se quita de la DB publica */
                $sql = "UPDATE `publicaciones` SET `is_public` = '0' WHERE `id`= '".$aportacion['post_id']."'";
            }
            $this -> _cnn -> query($sql);
            /* END Formula ====================================================== */
        }
    }

    public function getAllByCategory($category=null)
    {
        $where="";
        if ($category != null) {
            $where="WHERE idCategoriaAportacion = ".$category;
        }

        $query="SELECT * FROM ".$this->tableName." ".$where;

        return $this->advancedQueryByObjetc($query);
    }
    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idAportacion= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }
    public function getByPostId($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE post_id= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }
    public function advancedQueryByObjetc($query)
    {
        $resp=array();
        $consulta=$this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta= $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;

            if ($totalRows_consulta>0) {
                do{
                    array_push($resp, $this->createObject($row_consulta));
                }while($row_consulta= $consulta->fetch_assoc());
            }
        }
        return $resp;
    }
    public function createObject($row)
    {
        $x = new Aportacion();
        $x->setId($row['idAportacion']);
        $x->setTitulo($row['titulo']);
        $x->setDescripcion($row['descripcion']);
        $x->setFecha($row['fecha']);
        $x->setThumbnail($row['thumbnail']);
        $x->setFile($row['file']);
        $x->setCategory($row['idCategoriaAportacion']);
        $x->setPost_idPost($row['post_id']);
        return $x;
    }
}
