<?php
require_once 'DTO/Base.php';
require_once 'DTO/Oauth.php';

class DaoOauth extends base
{
    
    public $tableName="oauth"; 

    public function add(Oauth $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (servicio, clientId, clientSecret, apiKey, urlRed, accessToken, refreshToken, user_id) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getServicio(), "text"),
              $this->GetSQLValueString($x->getClientId(), "text"),
              $this->GetSQLValueString($x->getClientSecret(), "text"),
              $this->GetSQLValueString($x->getApiKey(), "text"),
              $this->GetSQLValueString($x->getUrlRed(), "text"),
              $this->GetSQLValueString($x->getAccessToken(), "text"),
              $this->GetSQLValueString($x->getRefreshToken(), "text"),
              $this->GetSQLValueString($x->getUsuario_id(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id; 
        }
    }


    public function update(Oauth $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET servicio=%s, clientId=%s, clientSecret=%s, apiKey=%s, urlRed=%s, accessToken=%s, accessTokenSecret=%s, refreshToken=%s, user_id=%s  WHERE idOauth = %s",
              $this->GetSQLValueString($x->getServicio(), "text"),
              $this->GetSQLValueString($x->getClientId(), "text"),
              $this->GetSQLValueString($x->getClientSecret(), "text"),
              $this->GetSQLValueString($x->getApiKey(), "text"),
              $this->GetSQLValueString($x->getUrlRed(), "text"),
              $this->GetSQLValueString($x->getAccessToken(), "text"),
              $this->GetSQLValueString($x->getAccessTokenSecret(), "text"),
              $this->GetSQLValueString($x->getRefreshToken(), "text"),
              $this->GetSQLValueString($x->getUsuario_id(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();  
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idOauth=".$Id); 
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idOauth= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function getOauthsByIdUsu($Id_usu)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE user_id=".$Id_usu;
        
        return $this->advancedQueryByObjetc($query);
    }

    public function advancedQueryByObjetc($query)
    {
        $resp = array();
        $new_query = $this->_cnn->query($query);

        if (!$new_query) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_query = $new_query->fetch_assoc();
            $total_rows_query = $new_query->num_rows;
            if ($total_rows_query > 0) {
                do{
                    array_push($resp, $this->createObject($row_query));
                }while($row_query = $new_query->fetch_assoc());
            }
        }

        return $resp;
    }

    public function createObject($row)
    {
        $x = new Oauth();
        $x->setId($row['idOauth']);
        $x->setServicio($row['servicio']);
        $x->setClientId($row['clientId']);
        $x->setClientSecret($row['clientSecret']); 
        $x->setApiKey($row['apiKey']);
        $x->setUrlRed($row['urlRed']); 
        $x->setAccessToken($row['accessToken']); 
        $x->setAccessTokenSecret($row['accessTokenSecret']); 
        $x->setRefreshToken($row['refreshToken']); 
        $x->setUsuario_id($row['user_id']); 
        return $x;
    }

}
