<?php
require_once 'DTO/Base.php';
require_once 'DTO/CategoriaAportacion.php';

class DaoCategoriaAportacion extends base
{

    public $tableName="categoriasaportacion";

    public function add(CategoriaAportacion $x)
    {
          $query=sprintf(
              "INSERT INTO ".$this->tableName." (titulo, idPadre, idSeccionAportacion, activo) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getIdPadre(), "int"),
              $this->GetSQLValueString($x->getIdSeccion(), "int"),
              $this->GetSQLValueString($x->getActivo(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(CategoriaAportacion $x)
    {
          $query=sprintf(
              "UPDATE ".$this->tableName." SET titulo=%s, idPadre=%s, idSeccionAportacion=%s, activo=%s  WHERE idCategoriaAportacion = %s",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getIdPadre(), "int"),
              $this->GetSQLValueString($x->getIdSeccion(), "int"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getId(), "int")
          );
          $Result1=$this->_cnn->query($query);
        if (!$Result1) {
                throw new Exception("Error al actualizar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id)
    {
        $query = sprintf("DELETE FROM ".$this->tableName." WHERE idCategoriaAportacion=".$Id);
        $Result1=$this->_cnn->query($query);
        if (!$Result1) {
              throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
             return true;
        }
    }

    public function getAll($status=[0,1],$where=[],$offset=null,$limit=null)
    {
        $conditions = "";
        foreach ($where as $key => $value) {
            $conditions .= " AND ".$key." ".$value; 
        }

        if ($offset!=null && $limit!=null) {
            $conditions .= " LIMIT ".$limit." OFFSET ".$offset;
        }

        $status = implode(", ", $status);

        $query = "SELECT * FROM ".$this->tableName." WHERE activo IN (".$status.") ".$conditions;
        
        return $this->advancedQueryByObjetc($query);
    }

    public function getAllopcion($option=null, $offset=null,$limit=null )
    {
        //echo $option;
        $conditions = "";
        if ($option!=null) {
            $conditions.=' and  ';
            if ($option=='Padres') {
                $conditions.='idpadre=0';
            } elseif ($option=='Hijos') { 
                $conditions.=' idpadre>0 ';
            }
        }

        if ($offset!=null && $limit!=null) {
            $conditions .= " LIMIT ".$limit." OFFSET ".$offset;
        }

        $status = '1';
        $query = "SELECT * FROM ".$this->tableName." WHERE activo IN (".$status.")". $conditions;
        return $this->advancedQueryByObjetc($query);
    }

    public function getById($Id)
    {
        $query="SELECT * FROM ".$this->tableName." WHERE idCategoriaAportacion= ".$Id;
            $Result1=$this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al seleccionar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->createObject($Result1->fetch_assoc());
        }
    }

    public function advancedQueryByObjetc($query)
    {
        $resp = array();
        $new_query = $this->_cnn->query($query);

        if (!$new_query) {
            throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_query = $new_query->fetch_assoc();
            $total_rows_query = $new_query->num_rows;
            if ($total_rows_query > 0) {
                do{
                    array_push($resp, $this->createObject($row_query));
                }while($row_query = $new_query->fetch_assoc());
            }
        }

        return $resp;
    }

    public function createObject($row)
    {
        $x = new CategoriaAportacion();
        $x->setId($row['idCategoriaAportacion']);
        $x->setTitulo($row['titulo']);
        $x->setActivo($row['activo']);
        $x->setIdPadre($row['idPadre']);
        $x->setIdSeccion($row['idSeccionAportacion']);

        return $x;
    }

}
