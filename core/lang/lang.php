<?php
//Función para la traducción
function translate($frase) {     
    $translation = null;
    $lang = "es";
    if ( isset($_COOKIE['bihaiv_lang'])) {    
         $lang = $_COOKIE['bihaiv_lang'];
    } 
    require($lang.'.php');
    $translation = $translate[$frase];             

    return $translation;     
}