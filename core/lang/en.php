﻿<?php

$translate = array(
    //Menú principal
    'Buscar actores' => 'Search Actors',
    'Iniciar sesión' => 'Login',
    'Únete' => 'Join Us',
    'Notificaciones' => 'Notifications',
    'Ver notificaciones' => 'View notifications',
    'Editar Información' => 'Edit Information',
    'Configuración' => 'Configuration',
    'Salir' => 'Log Out',
    'Perfil' => 'Profile',
    'Cómo funciona' => 'How it works',
    'label selectedFile'=>'File selected...',
    'label cambiarcontra'=>'Change password', 
    'Tipo actor'=>'Actor type',
    'label noAportaciones'=>'No contributions found',
    'label cargarFotoEvent'=>'Upload photo of event',
    'label cargarFotoIniciative'=>'Upload photo of iniciative',
    //Home
    'Historias del ecosistema' => 'Ecosystem Stories',
    'Mapa del ecosistema' => 'Ecosystem Map',
    'Calendario' => 'Calendar',
    'Mapa' => 'Map',
    'Foro' => 'Forum',
    'Mejores Prácticas' => 'Best Practices',
    'Expertos' => 'Experts',
    'Roles' => 'Roles',
    'Explora bihaiv' => 'Explore',
    'Etiqueta de video estático' => 'The actors ecosystem are high-impact entrepreneurship characters organizations and leaders working to promote it.',
    'Ver más' => 'Learn more',
    'Panal Descripción corta Roles' => 'Actors roles represent the <b> evolution <b> of traditional quadruple helix',
    'Panal Descripción corta Mapa' => 'For the first time a platform of <b> Social Intelligence </ b> that allows you to graphically identify the dynamics of interaction of all stakeholders Ecosystem High Impact Entrepreneurship..',
    'Panal Descripción corta Biblioteca' => 'A compendium of <b> hive intelligence </ b> that brings together the shared knowledge of all stakeholders in the ecosystem ..',
    'Panal Descripción corta Discusión' => 'A <b> neutral space </ b> for actors ecosystem create, develop and share knowledge, opinions, best practices ..',
    'Panal Descripción corta Eventos' => 'A shared database of <b> all events </ b> that are made in the Ecosystem of High Impact Entrepreneurship ..',
    'Panal Descripción corta Expertos' => 'Bihaiv up <b> an Expert Council </ b>, recognized leaders Entrepreneurship Ecosystem High Impact ...',
    'Biblioteca del Ecosistema' => 'Library ecosystem',
    'Eventos' => 'Events',
    'Acceso Miembros' => 'Member Login',
    'Consejo de Expertos' => 'Expert Counsil',
    'Ver mapa' => 'See map',
    'Foro de Discusión' => 'Discussion Forum',

    //Login
    'Iniciar Sesión' => 'Login',
    'Usuario' => 'User',
    'Contraseña' => 'Password',
    'Recuérdame' => 'Remember me',
    'Olvidaste tu Contraseña?' => 'Forgot your password?',
    'Email' => 'Email',
    '¡La dirección de email es incorrecta!' => 'Email is wrong!',

    //Registro
    'Registro' => 'Register',
    'Repetir Contraseña' => 'Password Repeat',
    '¡Todos los campos son requeridos!' => '¡All fields are required!',
    'Organización' => 'Organization',
    'País' => 'Country',
    'Correo Organizacional' => 'Organizational Email',
    'RFC' => 'RFC',
    'Acepto términos y condiciones' => ' I accept terms and conditions',
    'Ver términos y condiciones' => 'View terms and conditions',
    'Registrarme' => 'Register',
    'El password debe contener al menos 8 caracteres.' => ' The password must contain at least 8 characters.',
    'Los passwords ingresados no coinciden.' => ' The passwords entered do not match.',
    'Ha introducido caracteres invalidos.' => 'You entered invalid characters.',
    'RFC incorrecto.' => 'RFC is wrong.',


    //Menú
    'Inicio' => 'Home',
    'Foro de discusión' => 'Discussion Forum',
    'Catálogos' => 'Catalogs',
    'Biblioteca' => 'Library',
    'Red Social' => 'Social Network',

    //Home
    'Mis redes' => 'My Network',
    'Eliminar Aportación' => 'Delete aportation',

    //_userprofileinfo
    'Ver información de perfil' => 'View profile information',
    'Mis amigos' => 'My friends',
    'Amigo' => 'Friend',
    'Solicitud Enviada' => 'Request Sent',
    'Aceptar Solicitud' => 'Accept Request',
    'Acciones' => 'Actions',
    'Vincularse' => 'Join',
    'Vincular con' => 'Join with',
    'Mis conexiones' => 'My conections',

    //_usermodsugerencia
    'Vincularme' => 'Join me',
    'Sugerir vinculación' => 'Suggest link',
    'Mandar mensaje' => 'Send message',
    'Reportar' => 'Report',
    'Rol sin asignar' => 'Rol unassigned',
    'Ocultar sugerencias' => 'Hide tips',

    //_usermodenfoques
    'Enfoques' => 'Focuses',
    'Agregar Nuevo' => 'Add New',
    'Borrar Enfoque'=>'Estas seguro de eliminar esta aportación',
    'label editarEnfoque'=>'Your record was successfully edited',
    'label guardarEnfoque'=>'Your registration was successfully added',
    'label errorcalificacionenfoque'=>'An error occurred while trying to rate this contribution, try later.',
    'label guardarCal'=>'Saved rating',

        //Mensajes
    'label perfecto'=>'Perfect',
    'label atencion'=>'Attention',


    //_usermodiniciativas
    'Iniciativas de' => 'Initiatives of ',
    'Editar Iniciativa' => 'Edit Initiative',
    'Ver' => 'View',
    'Eliminar' => 'Delete',
    'Ver todas' => 'View All',
    'Actores Asociados' => 'Associative Actors',

    //Mapa
    'Mapa del Ecosistema' => 'Map Ecosystem',
    'Filtrar' => 'Filter',
    'Estado' => 'Status',
    'Mostrar' => 'Show',
    'Categoría' => 'Category',
    'Selecciona una..' => 'Choose one..',
    'Actores' => 'Actors',
    'Actividad' => 'Activity',
    'Colaboración' => 'Colaboration',
    'Todos' => 'All',
    'Selecciona un estado' => 'Choose one State',
    'Descripción sección mapa' => 'For the first time a platform of <b> Social Intelligence </ b> that allows you to graphically identify the dynamics of interaction of all actors Entrepreneurship Ecosystem High Impact, <b> to develop initiatives focused and efficient. </ B>',
    'Descripción corta sección mapa' => 'Social intelligence to develop initiatives focused and efficient.',
    'Descargar Reporte' => 'Download Report',
    'Reporte Actividad Por Iniciativas' => 'Activity Report By Initiatives',
    'Reporte Actividad Por Eventos' => 'Activity Report By Events',
    'Colaboración por estados' => 'Collaboration by state',
    'No se encontraron resultados' => 'No results found',
    'Debes seleccionar una categoria' => 'You must select a category',
    'Actividad Por Eventos' => 'Events for Activity',
    'Actividad Por Iniciativas' => 'Initiatives for Activity',

    //Foro
    'Descripción corta sección foro' => 'Neutral space to generate a critical mass on topics of interest ecosystem.',
    'Descripción sección foro' => '<b>Neutral space </b> for actors ecosystem create, develop and share knowledge, opinions, best practices, including <b> to work collaboratively on joint initiatives </b> that benefit for entrepreneurs.',
    'Temas nuevos' => 'Recent topics',
    'Más visitados' => 'Most visited',
    'Más votados' => 'Most voted',
    'Mis temas' => 'My topics',
    'Buscador' => 'Seeker',
    'Nuevo tema' => 'New topic',
    'Ingresa tu búsqueda...' => 'Enter your search...',
    'Ver tema' => 'View topic',
    'PRIVADO' => 'PRIVATE',
    'PUBLICO' => 'PUBLIC',
    'PÚBLICO' => 'PUBLIC',

    //Foro detalle
    'Viejos' => 'Olds',
    'Nuevos' => 'News',
    'Menos likes' => 'Less likes',
    'Mas likes' => 'More likes',
    'Ordenar comentarios' => 'Sort comments',
    'Responder' => 'Reply',
    'Escribir respuesta' => 'Write reply',
    'Me gusta' => 'Like',

    //Roles
    'Descripción corta sección roles' => 'Evolution of the traditional quadruple propeller for the value that the organizations contribute.',
    'Más información' => 'More information',
    '¿Necesitas más información?' => 'Do you need more information?',

    //Calendario
    'Descripción corta sección eventos' => 'All events result of collaboration between the actors through focus, efficiency and common goal.',
    'Próximamente' => 'Coming soon',
    'Este Mes' => 'This month',
    'Más antiguos' => 'Oldest',
    'Filtrar por' => 'Filter for',
    'Tipo evento' => 'Type event',
    'Exposición' => 'Exposition',
    'Conferencia' => 'Conference',
    'Taller' => 'Workshop',
    'Festival' => 'Festival',
    'Fecha evento' => 'Event date',
    'Nombre evento' => 'Event name',
    'Filtrar' => 'Filter',
    'Evento creado por' => 'Event created by',
    'Fecha' => 'Date',
    'Empieza' => 'Start',
    'Finaliza' => 'End',

    //Calendario detalle
    'Fecha inicio' => 'Start date',
    'Hora inicio' => 'Start time',
    'Hora finaliza' => 'End time',
    'Fecha finaliza' => 'End date',
    'Tipo de Evento' => 'Event type',
    'Url' => 'Url',
    'Acceso' => 'Login',
    'No hay colaboradores.' => 'No collaborators.',
    'No hay objetivos.' => 'No objetives.',
    'Ver todos los eventos' => 'See all events',
    'Regresar' => 'Return',
    'Objetivos' => 'Objetives',
    'Colaboradores' => 'Collaborators',
    'Editar Información'=>'Edit your information',

    //Listado de expertos
    'Listado de expertos' => 'List of experts',
    'Descripción corta sección expertos' => 'A Council of Experts that play a role of moderation and consultancy for the platform.',
    'Más información' => 'More information',
    '¿Necesitas más información?' => 'Do you need more information?',

    //Aportaciones del Ecosistema
    'Aportaciones del Ecosistema' => 'Ecosystem contributions',
    'Descripción corta sección Aportaciones' => 'Explore the most relevant knowledge decided by the same ecosystem as a whole.',

    //Contribuciones del ecosistema
    'Descripción corta sección contributions' => 'Explore the most relevant knowledge decided by the same ecosystem as a whole.',

    //Contribuciones del ecosistema
    'Descripción corta sección contributions' => 'Explore the most relevant knowledge decided by the same ecosystem as a whole.',

    //contributions_categories
    'Índice' => 'Index',
    'Vista previa del archivo' => 'File preview',
    'Aportaciones recientes' => 'Recent Contributions',

    //conversation
    'Mensajes' => 'Messages',
    'No hay Mensajes' => 'No Messages',
    'Mensaje' => 'Message',
    'Enviar' => 'Send',
    'Conversación' => 'Conversation',

    //conversations
    'Conversaciones' => 'Conversations',
    'Amigo' => 'Friend',

    //editinformation
    'Imagen de Perfil' => 'Profile image',
    'Imagen de Portada' => 'Cover image',
    'Teléfono' => 'Phone',
    'Correo Electrónico' => 'Email',
    'Dirección de tu sitio' => 'Web site',
    'Dirección principal' => 'Adress Web site',
    '¿Actualizar marcador?' => 'Reload marker?',
    'No' => 'No',
    'Si' => 'Si',
    'Alcance' => 'Scope',
    'Ubicacion(es) de alcance' => 'Scope Location(s)',
    'Agregar ubicación' => 'Add location',
    'País' => 'Country',
    'Estado' => 'Estate',
    'Municipio' => 'City',
    'Localidad' => 'Locality',
    '¿Influye en el ecosistema?' => 'Influence the ecosystem?',
    'Con Influencia' => 'With Influences',
    'Sin Influencia' => 'Within influences',
    'Guardar' => 'Save',
    'Eliminar' => 'Delete',
    'Influye' => 'Influence',
    'Visión' => 'Vision',
    'Misión' => 'Mission',
    'Acerca de mi' => 'About me',
    'Perfil de facebook' => 'Facebook profile',
    'Perfil de twitter' => 'Twitter profile',
    'Vincular twitter' => 'Join twitter',
    'Desvincular twitter' => 'Unlink twitter',
    'Actualizar Información' => 'Update information',
    'Selecciona un estado' => 'Selecct a state',
    'Selecciona un municipio' => 'Selecct a city',
    'Selecciona una localidad' => 'Selecct a locality',
    'Selecciona un país' => 'Selecct a country',
    'Ubicación eliminada' => 'Location delete',
    'Error al eliminar ubicación' => 'Error to delete location',

    //Notificaciones
    'Notificación' => 'Notification',
    'Notificaciones' => 'Notifications',
    'Colaborando' => 'Colaborating',
    'Rechazado' => 'Rejected',
    'No hay notificaciones' => 'No notifications',
    'Aceptar' => 'Acept',
    'Rechazar' => 'Reject',
    'Estatus' => 'Status',

    //Configuración
    'Configuración' => 'Configuration',
    'Nombre de usuario' => 'User name',
    'Tu nombre' => 'Your name',
    'Tu email' => 'Your email',
    'Email' => 'Email',
    'Guardar Configuración' => 'Save Configuration',
    'Contraseña Actual' => 'Current password',
    'Contraseña Nueva' => 'New password',
    'Repetir Contraseña' => 'Repeat password',
    'Actualizar Contraseña' => 'Update password',
    'Las contraseñas no coinciden' => 'Password do not match',

    //Expertos
    '¿Que es el consejo de expertos?' => 'What is the advice of experts?',
    'Descripción corta sección experts' => 'A Council of Experts that play a role of moderation and consultancy for the platform.',
    'Descripción sección experts' => 'Bihaiv integrates a Council of Experts, recognized leaders Ecosystem High Impact Entrepreneurship by their exposure, influence, ability and wisdom are regarded themselves as actors and play a role of moderation and consultancy for the platform.',
    'Mostrar listado de expertos' => 'Show list of experts',
    '¿Deseas postularte como experto?' => 'Want postulate as an expert?',
    'Envíanos tu solicitud' => 'Send us your request',
    'Nombre completo' => 'Complete name',
    'Correo' => 'Email',
    'Enviar' => 'Send',

    //Search
    'Buscar' => 'Search',
    'Término' => 'Term',
    'Usuario' => 'User',
    'No hay Resultados' => 'No Results',

    //SendMsg
    'Enviar Mensaje' => 'Send message',

    //FriendReqs
    'Solicitudes de Amistad' => 'Friend request',
    'Persona' => 'Person',
    'No hay solicitudes' => 'No request',
    'Aceptar Solicitud' => 'Acept request',

    //Frieds
    'Amigos' => 'Friends',
    'Amigo' => 'Friend',
    'Ver Perfil' => 'View Profile',
    'No hay Amigos' => 'No Friends',

    //MyFriends
    'Mis Amigos' => 'My friends',
    'No hay Amigos' => 'No friends',

    //_contributioncategories
    'No hay aportaciones asociadas a este tema' => 'No associated contributions to this topic',
    'No hay subtemas asociados' => 'No associated subtopics',
    'Vista previa de' => 'Preview of',

    //_contributionsfilter
    'Más recientes' => 'Most recent',
    'Por mes' => 'By month',
    'Por perfil de actor' => 'By the actor profile',
    'Mejor evaluados' => 'Highest rated',
    'Mes' => 'Month',
    'Más vistos' => 'Most Viewed',
    'months' => [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],

    //_contributionsgrid
    'Descargar' => 'Download',

    //RecenContributions
    'Vista previa de' => 'Preview of',

    //UserBadge
    'Solicitud de Amistad' => 'Friend request ',

    //UserModAportacion
    'Aportaciones de' => 'Contributions of',
    '!Este usuario aún no registra aportaciones!' => 'This user currently does not record contributions!',
    'Para calificar es necesario ingresar al menos una estrella' => 'For qualifying you need to enter at least one star',
    'Click para descargar' => 'Click for download',

    //UserModColaboraciones
    'Colaboraciones de' => 'Collaborations of',
    'esta colaborando en' => 'Is colaborating on',
    'creada por' => 'Create by',

    //Recordar Contraseña
    '¡Iniciar sesión!' => '¡Log in!',
    'Recordar contraseña' => 'Forgot Password',
    'Enviar contraseña' => 'Send Password',

    //RecoverAccount
    'Cambia tu password' => 'Change your password',
    'Repite password' => 'Repeat password',
    'Password' => 'Password',
    'Cambia tu passsword' => 'Change your password',
    '¡Esta cuenta no ha sido activada!' => 'This account has not been activated!',
    'Texto de correo enviado' => '<h1> Password change successful </ h1 <p> You have changed your password. Remember it!.</p>',
    '!Cambio de contraseña exitoso¡' => 'Password change sucessful',
    'Error al guardar nuevo password' => 'Error saving new password',
    'Password debe contener al menos 8 caracteres' => 'Password must contain at least 8 characters',
    'Las contraseñas no coinciden' => 'Passwords do not match',
    'Debe ingresar todos los campos' => 'You must enter all fields',

    //UserInfo
    'No se encontro el usuario' => 'The user is not found',
    'Nombre' => 'Name',
    'Ubicación' => 'Location',

    // Iniciativas
    'Eliminar Iniciativa' => 'Delete Initiative',

    //Modal info event
    'Evento' => 'Event',
    'Nombre del evento' => 'Name of the event',
    'Selecciona un tipo' => 'Event type',
    'Fecha Fin' => 'End date',
    'Hora fin' => 'End time',
    'URL al evento' => 'URL',
    'Lugar del evento' => 'Event location',
    'Mi ubicación' => 'My location',
    'Descripción' => 'Description',
    'Descripción del evento' => 'Description of the event',
    'Ligar con iniciativa' => 'Connect with initiative',
    'Colaboradores Actuales' => 'Current Collaborators',
    'Añadir Colaboradores' => 'Add Collaborators',
    'Cerrar' => 'Close',
    'Descarga ICS' => 'Download ICS',

    //modal new event
    'Nuevo Evento' => 'New Event',
    'Añadir Objetivo' => 'Add objetive',
    'Confirmar' => 'Confirm',
    'Público' => 'Public',
    'Privado' => 'Private',

    //modal info experts
    'Información del experto' => 'Expert Information',
    'Experiencia' => 'Experience',
    'Ramo' => 'Branch',
    'Años de experiencia' => 'Years of experience',
    'Cargo actual' => 'Actual position',
    'Cargo anterior' => 'Previous position',
    'Biografía' => 'Biography',

    // modal info focus
    'Editar mi enfoque' => 'Edit focus',
    'Guardar cambios' => 'Save changes',

    // modal info iniciativa
    'Iniciativa' => 'Initiative',
    'Fecha de inicio' => 'Start Date',
    'Fecha final' => 'End Date',
    'Descripción de la iniciativa' => 'Description of the initiative',
    'Eventos Actuales' => 'Current events',
    'Añadir Eventos' => 'Add events',
    'Selecciona una opción' => 'Select an option',

    // modal new iniciativa
    'Nueva Iniciativa' => 'New Initiative',
    'Añade un nuevo objetivo' => 'Add a new objetive',





    // modal sugest profile
    'Sugerir a' => 'Suggest to',
    'Actores con los que debería vincularse' => 'Actors with whom you should link',
    'Enviar solicitud' => 'Send request',

    // modal send message
    'Enviar Mensaje a' => 'Send message to',

    // modal report user_error
    'Reportar a' => 'Report to',
    'Tipo de reporte' => 'Type of report',
    'Seleccionar una opción' => 'Select an option',
    'Otro' => 'Other',
    'Abusivo' => 'Abusive',
    'Spam' => 'Spam',
    'Comentarios' => 'Comments',
    'Mensaje...' => 'Message...',

    // modal new topic
    'Nuevo tema de discusión' => 'New topic',
    'Título' => 'Title',
    'Dirigido a' => 'Addressed to',
    'Invitar actores' => 'Invite actors',

    'Agrega un titulo antes de continuar' => 'Add a title before continuing',
    'Selecciona a quien va dirigido antes de continuar' => 'Select who you are targeting before continuing',
    'Selecciona una de las opciones de privacidad antes de continuar' => 'Select one of the privacy options before continuing',
    'Selecciona por lo menos un actor para invitar al tema' => 'Select at least one actor to invite to the theme',
    'Agrega una descripción antes de continuar' => 'Add a description before continuing',

    // modal new share
    'Nueva Aportación' => 'New contribution',
    'Descripción de la aportación' => 'Description of contribution',
    'Nombre de la aportación' => 'Name of contribution',
    'Imagen' => 'Image',
    'Archivo' => 'Archive',
    'Además de un documento, es necesario subir una imagen JPG, JPEG o PNG' => 'In addition to a document, it is necessary to upload an image in  JPG, JPEG or PNG',
    'Además de una imagen, es necesario subir un archivo PDF, DOC, DOCX, PPTX, XLS o XLSX' => 'In addition to an image, you need to upload a PDF file, DOC, DOCX, PPTX, XLS or XLSX',
    
    // modal new focus
    'Tipo de enfoque' => 'Type of focus',
    '¡Solo tienes 1 día para la edición de este registro!' => 'You only have 1 day to edit this record!',
    'Texto descripción del porque solo un día para editar enfoque' => 'Text description of why just one day to edit approach...',
    'Ver menos' => 'See less',
    'Nuevo Enfoque' => 'New focus',

    // modal info rol
    'Descripción del rol' => 'Description of role',

    // como funciona
    'Titulo Como Funciona' => 'BIHAIV the first social intelligence platform for the high-impact entrepreneurship ecosystem',
    'Texto descriptivo Como Funciona' => '<p><b>BIHAIV</b> borns of understanding the need to increase the collaboration of the diferent actors of the ecosystem of high-impact entrepreneurship through theire bonding and articulation, with the objetive of reaching higher matururity levels, for the benefit of the entrepreneurs who depend on an environment that allow them to improve the chances of succes on their projects.</p>
    <p>For this reason, in order to be an active member, a registration procces must be carried out that evaluates the roles and capacities that each of the organizations interested on being part of the platform can fulfill and guarantee the main objective.</p>
    <p>If your organization want to be part of <b>BIHAIV</b>, you must follow the next steps:</p>',
    'Paso 1' => 'Register your basic contact information.',
    'Paso 2' => 'Answer the profile evaluation, with which you can know what type of actor represents your organization. This will have to be done within 5 days after receiving the confirmation email, otherwise the link will expire',
    'Paso 3' => 'After receiving your answer to the first questionnaire you can apply to become a member of BIHAIV, answering the second form with more specific information about of your organization. This information will be sent to existing (one for eacht type of actor) who will serve as reviewers and will determine the entry to the network depending of the level of impact of your organization int the high impact entrepreneurship ecosystem.',
    'Paso 4' => 'At the end of these two steps, you will receive a welcome email to the platform, or if applicable a feedback for your organization.',
    'Texto descriptivo paso 4' => '<p><b>The objective of the BIHAIV feedback</b>, in case your application being refused, we want show you the areas of opportunity that the requesting organization has in short, medium and long term to be considered an actor of the high impact entrepreneurship ecosystem.</p>
    <p><b>But we also want to offer you a right to reply</b> if you feel that the result in the evaluation and feedback does not correspond to your perception, you can request a review, which will automatically go to a new process, where the <b>expert advice of BIHAIV</b> will develop an unappealable opinion.</p>
    <p>It is possible to make a new application for admission after having worked on the feedback of the first application. This can be done after six months of the first postulation.</p>',
    'Fin Como Funciona' => '<p>Once insde of <b>BIHAIV</b>, what you will find is a social network that, unlike most, is not only focused on creating relationships, but on bringing real-life collaboration into the high-impact entrepreneurial ecosystem.</p>
    <p>All the information generated by each <b>Actor</b> dof the ecosystem is analyzed to create a series of <b>indicators</b>  that will help you improve and increase the level of <b>impact</b> that your organization has in the <b>High Impact Entrepreneurship Ecosystem</b>, while generating <b>social intelligence</b> which can allow each actor to make decisions based on what is called <b>hive intelligence</b>.</p>
    <p>For all who conform <b>BIHAIV</b>, each member represents an essential collaboration, so if you consider that the organization you represent is ready to join this great network, we invite you to take the first step <a href="home/registro.php"><b>by registering</b></a>.</p>',
    'label listaobjetivos'=>'Add objectives from to list of objectives',
    'label listaeventos'=>'add events from to list of events', 
    'label listainvitados'=>'Add guests from you  list  guests', 
    'label cargafoto'=>'Upload photo of the initiative', 
    'label addobjetivo'=>'Objective added to the list of objectives', 
    'label errordescobjetivo'=>'The description of the objective can not be empty', 
    'label error5maxObjetivo'=>'Select  like  max five objectives', 
    'label error3maxObjetivo'=>'Select  like  min five objectives', 
    'label errorfechalfinal'=>'The end date can not be an earlier date than the current date.',
    'label errorfechainicial'=>'The end date can not be earlier than the start date, try with a different date range',
    'label noNotificaciones'=>'You do not have any notifications yet, start linking!',
    'label noSugerencias'=>'You do not have any suggestions yet, start linking!',
    'label guardarEvento'=>'Your event has been successfully created',
    'label actualizarEvento'=>'Your event has been successfully updated',
    'Like correcto'=>'Like correct', 
    'Like incorrecto'=>'Like wrong',
    'label errorGuardar'=>'An error occurred trying to create your event, try again', 
    'label errorActualizar'=>'An error occurred trying to update your event, try again',
    'label guardarDiscusion'=>'Your discussión has been successfully created',
    'label errorGuardarDiscusion'=>'Ah, an error occurred trying to create your discussion, try again',
    'label guardarComentario'=>'Your comment has been added successfully.',
    'label errorguardarComentario'=>'An error occurred trying to create your comment, try again.',
    'label addvisita'=>'Visit saves successfully',
    'label erroraddvisita'=>'Error, save unsuccessfully',
    'label graciascalificar'=>'Thanks for rating this topic',
    'label erorrgraciascalificar'=>'An error occurred trying to rating your team, try again',
    'label guardarIniciativa'=>'Your Initiative has been successfully created',
    'label errorGuardarIniciativa'=>'An error occurred trying to create your initiative, try again.',
    'label errorcalificarIniciativa'=>'An error occurred trying to rating your initiative, try again.',
    'label procesoE'=>'Successful process',
    'label errorGenerico'=>'error occurred, try again.',
    'label novigentenotificacion'=>'This notification is no longer in effect',
    'label eliminadaAportacion'=>'Your contribution has been successfully deleted',
    'label erroreliminarAportacion'=>'An error occurred trying to delete your initiative, try again.',
    'label guardarAportacion'=>'Your contribution has been successfully created',
    'label descarga'=>'Download successfully registered',
    'label asistire'=>'I will attend', 
    'label reportadoUsuario'=>'You have already reported to this user.',
    'label addRegistro'=>'Your item has been successfully created',
    'label evaluacionOrg'=>'Evaluation for the organization',
    'label resultaAproba'=>'Result of approval',
    'label telefono'=>'Telephone',
    'privacidad'=>'*Privacy',
    'label notemas'=>'You do not have any topics around here yet. Register one!',
    'label noColaboracion'=>'You do not have any topics around here yet. Start to collaborate!',
    'label noIniciativas'=>'You do not have any initiative around here yet. Register one!',
    'label noEnfoques'=>'You do not have any focus around here yet. Register one!',
    //nivel graficas
    'label nivelImpacto'=>'Level of impact on the ecosystem',
    'label nivelIncidencia'=>'Level of incidence',
    'label nivelEnfoque'=>'Level of focus',
    'label nivelActividad'=>'Level of activity',
    'label nivelAportacion'=>'Level of contribution', 
    'label nivelColaboracion'=>'Level of collaboration',
    'label descargaReporte'=>'Download impact report',

    //module noticias
    'label notieneNoticia'=>'This user has no news yet!',
    'label Noticias'=>'Notifications',
    'label vermas'=>'More',
    'Mas recientes'=>'Most recent',
    'Mas viejos'=>'Most older',
    'Mas recientes'=>'Más vistas',
    'Menos recientes'=>'Menos vistas',
    'Mas vistas'=>'Most seen',
    'Menos vistas'=>'Least seen',

   // Forum detail
   'Debes iniciar sesión para realizar esta acción'=>'Debes iniciar sesión para realizar esta acción',
   'Para calificar es necesario ingresar al menos una estrella'=>'Para calificar es necesario ingresar al menos una estrella',
   'Escribe una respuesta antes de enviarla'=>'Escribe una respuesta antes de enviarla'

);
