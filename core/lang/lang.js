
// Inicializa objeto en la carga de este archivo
var lang = new Lang();

/*
 *	CLASE LANG
 *	===============
 */
function Lang(){

}

/*
 *	LEER COOKIE DE LENGUAJE
 *	@params : Nombre de Cookie
 *	=====================================================
 */
Lang.prototype._leerCookie = function( cookie ){
    cookieValue = document.cookie.match("(^|;) ?" + cookie + "=([^;]*)(;|$)");
    if (cookieValue) {
        return cookieValue[2];
    } else {
        return 'es';
    }
};


/*
 *	DICCIONARIO PARA LENGUAJE
 *	@params : Lenguaje, es o en
 *	=====================================================
 */
Lang.prototype._dictionary = function( lang ){
    
    var dictionary = {
        'es' : {
            'Aún no tienes comentarios para este tema': 'Aún no tienes comentarios para este tema',
            'Aún no tienes discusiones, ¡Registra una!': 'Aún no tienes discusiones, ¡Registra una!',
            'Lo sentimos no se encontraron resultados para tu búsqueda, intenta con una búsqueda nueva': 'Lo sentimos no se encontraron resultados para tu búsqueda, intenta con una búsqueda nueva',

            'Aún no tienes notificaciones, ¡Comienza a vincularte!': 'Aún no tienes notificaciones, ¡Comienza a vincularte!',

            'Aún no tienes aportaciones, ¡Registra una!': 'Aún no tienes aportaciones, ¡Registra una!',
            'Aún no tienes documentos': 'Aún no tienes documentos'
        },
        'en' : {
            'Aún no tienes comentarios para este tema': 'No comments for this topic',
            'Aún no tienes discusiones, ¡Registra una!': 'No topics registered yet, ¡Create a new one!',
            'Lo sentimos no se encontraron resultados para tu búsqueda, intenta con una búsqueda nueva': 'No results for this search',

            'Aún no tienes notificaciones, ¡Comienza a vincularte!': 'No notifications registered yet',

            'Aún no tienes aportaciones, ¡Registra una!': 'No aportations registered yet, ¡Create a new one!',
            'Aún no tienes documentos': 'No documents registered yet'
        }
    }
    return dictionary[lang];

};


/*
 *	TRANSLATE
 *	@params : Frase a traducir
 *	=====================================================
 */
Lang.prototype.translate = function( phrase ){

	var phrase      = phrase || null;
    var language    = this._leerCookie('bihaiv_lang');
    var dictionary  = this._dictionary(language); 

    return dictionary[phrase];
};


