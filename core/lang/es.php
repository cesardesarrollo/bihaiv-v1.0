<?php

$translate = array(
    //Menú principal
    'Buscar actores' => 'Buscar actores',
    'Iniciar sesión' => 'Iniciar sesión',
    'Únete' => 'Únete',
    'Notificaciones' => 'Notificaciones',
    'Ver notificaciones' => 'Ver notificaciones',
    'Editar Información' => 'Editar Información',
    'Configuración' => 'Configuración',
    'Salir' => 'Salir',
    'Perfil' => 'Perfil',
    'Cómo funciona' => 'Cómo Funciona',
    'label selectedFile'=>'Archivo seleccionado...',
    'Tipo actor'=>'Tipo de actor',
    'label noAportaciones'=>'No se encontraron aportaciones',
     //Index
    'Historias del ecosistema' => 'Historias del ecosistema',
    'Mapa del ecosistema' => 'Mapa del ecosistema',
    'Calendario' => 'Calendario',
    'label cambiarcontra'=>'Cambiar contraseña', 
    'Mapa' => 'Mapa',
    'Foro' => 'Foro',
    'Mejores Prácticas' => 'Mejores Prácticas',
    'Expertos' => 'Expertos',
    'Roles' => 'Roles',
    'label noEnfoques'=>'No tienes Registrado ningun enfoque',
    'Explora bihaiv' => 'Explora bihaiv',
    'Etiqueta de video estático' => 'Los actores del ecosistema de emprendimiento de alto impacto son las organizaciones y personajes lí­deres que trabajan para impulsarlo.',
    'Ver más' => 'Ver más',
    'Panal Descripción corta Roles' => 'Los roles de los Actores representan la <b>evolución</b> de la tradicional cuádruple hélice..',
    'Panal Descripción corta Mapa' => 'Por primera vez una plataforma de <b>Inteligencia Social</b> que permite identificar gráficamente las dinámicas de  interacción de todos los actores del Ecosistema de Emprendimiento de Alto Impacto..',
    'Panal Descripción corta Biblioteca' => 'Un compendio de <b>inteligencia de colmena</b> que agrupa el conocimiento compartido de todos los actores del ecosistema..',
    'Panal Descripción corta Discusión' => 'Un <b>espacio neutral</b> para que los actores del ecosistema creen, desarrollen y compartan conocimiento, opiniones, mejores prácticas..',
    'Panal Descripción corta Eventos' => 'Una base de datos compartida de <b>todos los eventos</b> que se realizan en el Ecosistema de Emprendimiento de Alto Impacto..',
    'Panal Descripción corta Expertos' => 'Bihaiv integra <b>un Consejo de Expertos</b>, lí­deres reconocidos del Ecosistema de Emprendimiento de Alto Impacto...',
    'Biblioteca del Ecosistema' => 'Biblioteca del Ecosistema',
    'Eventos' => 'Eventos',
    'Acceso Miembros' => 'Acceso Miembros',
    'Consejo de Expertos' => 'Consejo de Expertos',
    'Ver mapa' => 'Ver mapa',
    'Foro de Discusión' => 'Foro de Discusión',

    //Login
    'Iniciar Sesión' => 'Iniciar Sesión',
    'Usuario' => 'Usuario',
    'Contraseña' => 'Contraseña',
    'Recuérdame' => 'Recuérdame',
    'Olvidaste tu Contraseña?' => '¿Olvidaste tu Contraseña?',
    'Email' => 'Email',
    '¡La dirección de email es incorrecta!' => '¡La dirección de email es incorrecta!',

    //Registro
    'Registro' => 'Registro',
    'Repetir Contraseña' => 'Repetir Contraseña',
    '¡Todos los campos son requeridos!' => '¡Todos los campos son requeridos!',
    'Organización' => 'Organización',
    'País' => 'País',
    'Correo Organizacional' => 'Correo Organizacional',
    'RFC' => 'RFC',
    'Acepto términos y condiciones' => 'Acepto términos y condiciones',
    'Ver términos y condiciones' => 'Ver términos y condiciones',
    'Registrarme' => 'Registrarme',
    'El password debe contener al menos 8 caracteres.' => 'El password debe contener al menos 8 caracteres.',
    'Los passwords ingresados no coinciden.' => 'Los passwords ingresados no coinciden.',
    'Ha introducido caracteres invalidos.' => 'Ha introducido caracteres invalidos.',
    'RFC incorrecto.' => 'RFC incorrecto.',


    //Menú
    'Inicio' => 'Inicio',
    'Foro de discusión' => 'Foro de discusión',
    'Catálogos' => 'Catálogos',
    'Biblioteca' => 'Biblioteca',
    'Red Social' => 'Red Social',

    //Home
    'Mis redes' => 'Mis redes',
    'Eliminar Aportación' => 'Eliminar Aportación',

    //_userprofileinfo
    'Ver información de perfil' => 'Ver información de perfil',
    'Mis amigos' => 'Mis amigos',
    'Amigo' => 'Amigo',
    'Solicitud Enviada' => 'Solicitud Enviada',
    'Aceptar Solicitud' => 'Aceptar Solicitud',
    'Acciones' => 'Acciones',
    'Vincularse' => 'Vincularse',
    'Vincular con' => 'Vincular con',
    'Mis conexiones' => 'Mis conexiones',

    //_usermodsugerencia
    'Vincularme' => 'Vincularme',
    'Sugerir vinculación' => 'Sugerir vinculación',
    'Mandar mensaje' => 'Mandar mensaje',
    'Reportar' => 'Reportar',
    'Rol sin asignar' => 'Rol sin asignar',
    'Ocultar sugerencias' => 'Ocultar sugerencias',

    //_usermodenfoque
    'Enfoques' => 'Enfoques',
    'Agregar Nuevo' => 'Agregar Nuevo',
    'Borrar Enfoque'=>'Estas seguro de eliminar esta aportación',
    'label editarEnfoque'=>'Se editó correctamente tu registro',
    'label guardarEnfoque'=>'Se agrego correctamente tu registro',
    'label error5enfoques'=>'No puedes agregar más de 5 enfoques',
    'label errorcalificacionenfoque'=>'Ah ocurrido un error intentando calificar esta aportación, intentalo más tarde.',
    'label guardarCal'=>'Calificación guardada',
    //_usermodiniciativas
    'Iniciativas de' => 'Iniciativas de',
    'Editar Iniciativa' => 'Editar Iniciativa',
    'Ver' => 'Ver',
    'Eliminar' => 'Eliminar',
    'Ver todas' => 'Ver todas',
    'Actores Asociados' => 'Actores Asociados',
    //Mensajes
    'label perfecto'=>'Perfecto',
    'label atencion'=>'Atencion',
    //Mapa
    'Mapa del Ecosistema' => 'Mapa del Ecosistema',
    'Filtrar' => 'Filtrar',
    'Estado' => 'Estado',
    'Mostrar' => 'Mostrar',
    'Categoría' => 'Categoría',
    'Selecciona una..' => 'Selecciona una..',
    'Actores' => 'Actores',
    'Actividad' => 'Actividad',
    'Colaboración' => 'Colaboración',
    'Todos' => 'Todos',
    'Selecciona un estado' => 'Selecciona un estado',
    'Descripción sección mapa' => 'Por primera vez una plataforma de <b>Inteligencia Social</b> que permite identificar gráficamente las dinámicas de interacción de todos los actores del Ecosistema de Emprendimiento de Alto Impacto, <b>para desarrollar iniciativas enfocadas y eficientes.</b>',
    'Descripción corta sección mapa' => 'Inteligencia Social para desarrollar iniciativas enfocadas y eficientes.',
    'Descargar Reporte' => 'Descargar Reporte',
    'Reporte Actividad Por Iniciativas' => 'Reporte Actividad Por Iniciativas',
    'Reporte Actividad Por Eventos' => 'Reporte Actividad Por Eventos',
    'Colaboración por estados' => 'Colaboración por estados',
    'No se encontraron resultados' => 'No se encontraron resultados',
    'Debes seleccionar una categoria' => 'Debes seleccionar una categoria',
    'Actividad Por Eventos' => 'Actividad Por Eventos',
    'Actividad Por Iniciativas' => 'Actividad Por Iniciativas',

    //Foro
    'Descripción corta sección foro' => 'Espacio neutral para generar una masa crítica sobre los temas de interés del ecosistema.',
    'Descripción sección foro' => 'Un <b>espacio neutral</b> para que los actores del ecosistema creen, desarrollen y compartan conocimiento, opiniones, mejores prácticas, incluso <b>para trabajar colaborativamente en iniciativas conjuntas</b> que redunden en beneficio para los emprendedores.',
    'Temas nuevos' => 'Temas nuevos',
    'Más visitados' => 'Más visitados',
    'Más votados' => 'Más votados',
    'Mis temas' => 'Mis temas',
    'label notemas'=>'Aún no tienes temas por aquí. ¡Registra uno!',
    'Buscador' => 'Buscador',
    'Nuevo tema' => 'Nuevo tema',
    'Ingresa tu búsqueda...' => 'Ingresa tu búsqueda...',
    'Ver tema' => 'Ver tema',
    'PRIVADO' => 'PRIVADO',
    'PUBLICO' => 'PUBLICO',
    'PÚBLICO' => 'PÚBLICO',
    'label noColaboracion'=>'Aún no tienes colaboraciones, ¡Empieza a colaborar!',
    'label noIniciativas'=>'Aún no tienes iniciativas, ¡Registra una!',
    'label cargarFotoEvent'=>'Cargar foto del evento',
    'label cargarFotoIniciative'=>'Cargar foto de la iniciativa',
    //Foro detalle
    'Viejos' => 'Viejos',
    'Nuevos' => 'Nuevos',
    'Menos likes' => 'Menos likes',
    'Mas likes' => 'Mas likes',
    'Ordenar comentarios' => 'Ordenar comentarios',
    'Responder' => 'Responder',
    'Escribir respuesta' => 'Escribir respuesta',
    'Me gusta' => 'Me gusta',

    //Roles
    'Descripción corta sección roles' => 'Evolución de la tradicional cuádruple hélice por el valor que las organizaciones aportan.',
    'Más información' => 'Más información',
    '¿Necesitas más información?' => '¿Necesitas más información?',

    //Calendario
    'Eventos' => 'Eventos',
    'Descripción corta sección eventos' => 'Todos los eventos resultado de la colaboración entre los actores a través de enfoque, eficiencia y objetivo comunes.',
    'Próximamente' => 'Próximamente',
    'Este Mes' => 'Este Mes',
    'Más antiguos' => 'Más antiguos',
    'Filtrar por' => 'Filtrar por',
    'Tipo evento' => 'Tipo evento',
    'Exposición' => 'Exposición',
    'Conferencia' => 'Conferencia',
    'Taller' => 'Taller',
    'Festival' => 'Festival',
    'Fecha evento' => 'Fecha evento',
    'Nombre evento' => 'Nombre evento',
    'Filtrar' => 'Filtrar',
    'Evento creado por' => 'Evento creado por',
    'Fecha' => 'Fecha',
    'Empieza' => 'Empieza',
    'Finaliza' => 'Finaliza',

    //Calendario detalle
    'Fecha inicio' => 'Fecha inicio',
    'Hora inicio' => 'Hora inicio',
    'Hora finaliza' => 'Hora finaliza',
    'Fecha finaliza' => 'Fecha finaliza',
    'Tipo de Evento' => 'Tipo de Evento',
    'Url' => 'Url',
    'Acceso' => 'Acceso',
    'No hay colaboradores.' => 'No hay colaboradores.',
    'No hay objetivos.' => 'No hay objetivos.',
    'Ver todos los eventos' => 'Ver todos los eventos',
    'Regresar' => 'Regresar',
    'Objetivos' => 'Objetivos',
    'Colaboradores' => 'Colaboradores',

    //Listado de expertos
    'Listado de expertos' => 'Listado de expertos',
    'Descripción corta sección expertos' => 'Un Consejo de Expertos que juegan un rol de moderación y asesoría para la plataforma.',
    'Más información' => 'Más información',
    '¿Necesitas más información?' => '¿Necesitas más información?',

    //Aportaciones del Ecosistema
    'Aportaciones del Ecosistema' => 'Aportaciones del Ecosistema',
    'Descripción corta sección Aportaciones' => 'Explora el conocimiento más relevante decidido por el mismo ecosistema en su conjunto.',

    //Contribuciones del ecosistema
    'Descripción corta sección contributions' => 'Explora el conocimiento más relevante decidido por el mismo ecosistema en su conjunto.',

    //Contribuciones del ecosistema
    'Descripción corta sección contributions' => 'Explora el conocimiento más relevante decidido por el mismo ecosistema en su conjunto.',

    //contributions_categories
    'Índice' => 'Índice',
    'Vista previa del archivo' => 'Vista previa del archivo',
    'Aportaciones recientes' => 'Aportaciones recientes',

    //conversation
    'Mensajes' => 'Mensajes',
    'No hay Mensajes' => 'No hay Mensajes',
    'Mensaje' => 'Mensaje',
    'Enviar' => 'Enviar',
    'Conversación' => 'Conversación',

    //conversations
    'Conversaciones' => 'Conversaciones',
    'Amigo' => 'Amigo',

    //editinformation
    'Imagen de Perfil' => 'Imagen de Perfil',
    'Imagen de Portada' => 'Imagen de Portada',
    'Teléfono' => 'Teléfono',
    'Correo Electrónico' => 'Correo Electrónico',
    'Dirección de tu sitio' => 'Dirección de tu sitio',
    'Dirección principal' => 'Dirección principal',
    '¿Actualizar marcador?' => '¿Actualizar marcador?',
    'No' => 'No',
    'Si' => 'Si',
    'Alcance' => 'Alcance',
    'Ubicacion(es) de alcance' => 'Ubicación(es) de alcance',
    'Agregar ubicación' => 'Agregar ubicación',
    'Municipio' => 'Municipio',
    'Localidad' => 'Localidad',
    '¿Influye en el ecosistema?' => '¿Influye en el ecosistema?',
    'Con Influencia' => 'Con Influencia',
    'Sin Influencia' => 'Sin Influencia',
    'Guardar' => 'Guardar',
    'Eliminar' => 'Eliminar',
    'Influye' => 'Influye',
    'Visión' => 'Visión',
    'Misión' => 'Misión',
    'Acerca de mi' => 'Acerca de mi',
    'Perfil de facebook' => 'Perfil de facebook',
    'Perfil de twitter' => 'Perfil de twitter',
    'Vincular twitter' => 'Vincular twitter',
    'Desvincular twitter' => 'Desvincular twitter',
    'Actualizar Información' => 'Actualizar Información',
    'Selecciona un estado' => 'Selecciona un estado',
    'Selecciona un municipio' => 'Selecciona un municipio',
    'Selecciona una localidad' => 'Selecciona una localidad',
    'Selecciona un país' => 'Selecciona un país',
    'Ubicación eliminada' => 'Ubicación eliminada',
    'Error al eliminar ubicación' => 'Error al eliminar ubicación',

    //Notificaciones
    'Notificación' => 'Notificación',
    'Notificaciones' => 'Notificaciones',
    'Colaborando' => 'Colaborando',
    'Rechazado' => 'Rechazado',
    'No hay notificaciones' => 'No hay notificaciones',
    'Aceptar' => 'Aceptar',
    'Rechazar' => 'Rechazar',
    'Estatus' => 'Estatus',

    //Configuración
    'Configuración' => 'Configuración',
    'Nombre de usuario' => 'Nombre de usuario',
    'Tu nombre' => 'Tu nombre',
    'Tu email' => 'Tu email',
    'Email' => 'Email',
    'Guardar Configuración' => 'Guardar Configuración',
    'Contraseña Actual' => 'Contraseña Actual',
    'Contraseña Nueva' => 'Contraseña Nueva',
    'Repetir Contraseña' => 'Repetir Contraseña',
    'Actualizar Contraseña' => 'Actualizar Contraseña',
    'Las contraseñas no coinciden' => 'Las contraseñas no coinciden',

    //Expertos
    '¿Que es el consejo de expertos?' => '¿Que es el consejo de expertos?',
    'Descripción corta sección experts' => 'Un Consejo de Expertos que juegan un rol de moderación y asesoría para la plataforma.',
    'Descripción sección experts' => 'Bihaiv integra un Consejo de Expertos, líderes reconocidos del Ecosistema de Emprendimiento de Alto Impacto que por su exposición, influencia, capacidad y sabiduría son considerados por sí solos como actores y que juegan un rol de moderación y asesoría para la plataforma.',
    'Mostrar listado de expertos' => 'Mostrar listado de expertos',
    '¿Deseas postularte como experto?' => '¿Deseas postularte como experto?',
    'Envíanos tu solicitud' => 'Envíanos tu solicitud',
    'Nombre completo' => 'Nombre completo',
    'Correo' => 'Correo',
    'Enviar' => 'Enviar',

    //Search
    'Buscar' => 'Buscar',
    'Término' => 'Término',
    'Usuario' => 'Usuario',
    'No hay Resultados' => 'No hay Resultados',

    //SendMsg
    'Enviar Mensaje' => 'Enviar Mensaje',

    //FriendReqs
    'Solicitudes de Amistad' => 'Solicitudes de Amistad',
    'Persona' => 'Persona',
    'No hay solicitudes' => 'No hay solicitudes',
    'Aceptar Solicitud' => 'Aceptar Solicitud',

    //Frieds
    'Amigos' => 'Amigos',
    'Amigo' => 'Amigo',
    'Ver Perfil' => 'Ver Perfil',
    'No hay Amigos' => 'No hay Amigos',

    //MyFriends
    'Mis Amigos' => 'Mis Amigos',
    'No hay Amigos' => 'No hay Amigos',

    //_contributioncategories
    'No hay aportaciones asociadas a este tema' => 'No hay aportaciones asociadas a este tema',
    'No hay subtemas asociados' => 'No hay subtemas asociados',
    'Vista previa de' => 'Vista previa de',

    //_contributionsfilter
    'Más recientes' => 'Más recientes',
    'Por mes' => 'Por mes',
    'Por perfil de actor' => 'Por perfil de actor',
    'Mejor evaluados' => 'Mejor evaluados',
    'Mes' => 'Mes',
    'Más vistos' => 'Más vistos',
    'months' => ["Enero" , "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],

    //_contributionsgrid
    'Descargar' => 'Descargar',

    //RecenContributions
    'Vista previa de' => 'Vista previa de',

    //UserBadge
    'Solicitud de Amistad' => 'Solicitud de Amistad',

    //UserModAportacion
    'Aportaciones de' => 'Aportaciones de',
    'Agregar Nuevo' => 'Agregar Nuevo',
    '!Este usuario aún no registra aportaciones!' => '¡Este usuario aún no registra aportaciones!',
    'Para calificar es necesario ingresar al menos una estrella' => 'Para calificar es necesario ingresar al menos una estrella',
    'Click para descargar' => 'Click para descargar',

    //UserModColaboraciones
    'Colaboraciones de' => 'Colaboraciones de',
    'esta colaborando en' => 'esta colaborando en',
    'creada por' => 'creada por',

    //Recordar Contraseña
    '¡Iniciar sesión!' => '¡Iniciar sesión!',
    'Recordar contraseña' => 'Recordar contraseña',
    'Enviar contraseña' => 'Enviar contraseña',

    //RecoverAccount
    'Cambia tu password' => 'Cambia tu password',
    'Repite password' => 'Repite password',
    'Password' => 'Password',
    'Cambia tu passsword' => 'Cambia tu passsword',
    '¡Esta cuenta no ha sido activada!' => '¡Esta cuenta no ha sido activada!',
    'Texto de correo enviado' => '<h1>Cambio de contraseña exitoso</h1<p>Has cambiado tu contraseña. ¡Recuerdalo!.</p>',
    '!Cambio de contraseña exitoso¡' => '¡Cambio de contraseña exitoso!',
    'Error al guardar nuevo password' => 'Error al guardar nuevo password',
    'Password debe contener al menos 8 caracteres' => 'Password debe contener al menos 8 caracteres',
    'Las contraseñas no coinciden' => 'Las contraseñas no coinciden',
    'Debe ingresar todos los campos' => 'Debe ingresar todos los campos',

    //UserInfo
    'No se encontro el usuario' => 'No se encontro el usuario',
    'Nombre' => 'Nombre',
    'Ubicación' => 'Ubicación',

    // Iniciativas
    'Eliminar Iniciativa' => 'Eliminar Iniciativa',

    //Modal info event
    'Evento' => 'Evento',
    'Nombre del evento' => 'Nombre del evento',
    'Selecciona un tipo' => 'Selecciona un tipo',
    'Fecha Fin' => 'Fecha Fin',
    'Hora fin' => 'Hora fin',
    'URL al evento' => 'URL al evento',
    'Lugar del evento' => 'Lugar del evento',
    'Mi ubicación' => 'Mi ubicación',
    'Descripción' => 'Descripción',
    'Descripción del evento' => 'Descripción del evento',
    'Ligar con iniciativa' => 'Ligar con iniciativa',
    'Colaboradores Actuales' => 'Colaboradores Actuales',
    'Añadir Colaboradores' => 'Añadir Colaboradores',
    'Cerrar' => 'Cerrar',
    'Descarga ICS' => 'Descarga ICS',

    //modal new event
    'Nuevo Evento' => 'Nuevo Evento',
    'Añadir Objetivo' => 'Añadir Objetivo',
    'Confirmar' => 'Confirmar',
    'Público' => 'Público',
    'Privado' => 'Privado',

    //modal info experts
    'Información del experto' => 'Información del experto',
    'Experiencia' => 'Experiencia',
    'Ramo' => 'Ramo',
    'Años de experiencia' => 'Años de experiencia',
    'Cargo actual' => 'Cargo actual',
    'Cargo anterior' => 'Cargo anterior',
    'Biografía' => 'Biografía',

    // modal info focus
    'Editar mi enfoque' => 'Editar mi enfoque',
    'Guardar cambios' => 'Guardar cambios',

    // modal info iniciativa
    'Iniciativa' => 'Iniciativa',
    'Fecha de inicio' => 'Fecha de inicio',
    'Fecha final' => 'Fecha final',
    'Descripción de la iniciativa' => 'Descripción de la iniciativa',
    'Eventos Actuales' => 'Eventos Actuales',
    'Añadir Eventos' => 'Añadir Eventos',
    'Selecciona una opción' => 'Selecciona una opción',

    // modal new iniciativa
    'Nueva Iniciativa' => 'Nueva Iniciativa',
    'Añade un nuevo objetivo' => 'Añade un nuevo objetivo',




    // modal sugest profile
    'Sugerir a' => 'Sugerir a',
    'Actores con los que debería vincularse' => 'Actores con los que debería vincularse',
    'Enviar solicitud' => 'Enviar solicitud',

    // modal send message
    'Enviar Mensaje a' => 'Enviar Mensaje a',

    // modal report user_error
    'Reportar a' => 'Reportar a',
    'Tipo de reporte' => 'Tipo de reporte',
    'Seleccionar una opción' => 'Seleccionar una opción',
    'Otro' => 'Otro',
    'Abusivo' => 'Abusivo',
    'Spam' => 'Spam',
    'Comentarios' => 'Comentarios',
    'Mensaje..' => 'Mensaje..',

    // modal new topic
    'Nuevo tema de discusión' => 'Nuevo tema de discusión',
    'Título' => 'Título',
    'Dirigido a' => 'Dirigido a',
    'Invitar actores' => 'Invitar actores',

    'Agrega un titulo antes de continuar' => 'Agrega un titulo antes de continuar',
    'Selecciona a quien va dirigido antes de continuar' => 'Selecciona a quien va dirigido antes de continuar',
    'Selecciona una de las opciones de privacidad antes de continuar' => 'Selecciona una de las opciones de privacidad antes de continuar',
    'Selecciona por lo menos un actor para invitar al tema' => 'Selecciona por lo menos un actor para invitar al tema',
    'Agrega una descripción antes de continuar' => 'Agrega una descripción antes de continuar',

    // modal new share
    'Nueva Aportación' => 'Nueva Aportación',
    'Descripción de la aportación' => 'Descripción de la aportación',
    'Nombre de la aportación' => 'Nombre de la aportación',
    'Imagen' => 'Imagen',
    'Archivo' => 'Archivo',
    'Además de un documento, es necesario subir una imagen JPG, JPEG o PNG' => 'Además de un documento, es necesario subir una imagen JPG, JPEG o PNG',
    'Además de una imagen, es necesario subir un archivo PDF, DOC, DOCX, PPTX, XLS o XLSX' => 'Además de una imagen, es necesario subir un archivo PDF, DOC, DOCX, PPTX, XLS o XLSX',
    'privacidad'=>'*Privacidad',

    // modal new focus
    'Tipo de enfoque' => 'Tipo de enfoque',
    '¡Solo tienes 1 día para la edición de este registro!' => '¡Solo tienes 1 día para la edición de este registro!',
    'Texto descripción del porque solo un día para editar enfoque' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit maiores, incidunt quidem natus. Ut dolorum deleniti reiciendis tenetur illum, beatae cum, molestiae laborum, nisi omnis nulla laboriosam, ipsam nemo consectetur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit maiores, incidunt quidem natus. Ut dolorum deleniti reiciendis tenetur illum, beatae cum, molestiae laborum, nisi omnis nulla laboriosam, ipsam nemo consectetur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit maiores, incidunt quidem natus. Ut dolorum deleniti reiciendis tenetur illum, beatae cum, molestiae laborum, nisi omnis nulla laboriosam, ipsam nemo consectetur.',
    'Ver menos' => 'Ver menos',
    'Nuevo Enfoque' => 'Nuevo Enfoque',

    // modal info rol
    'Descripción del rol' => 'Descripción del rol',

    // como funciona
    'Titulo Como Funciona' => 'BIHAIV la primera plataforma de inteligencia social para el ecosistema de emprendimiento de alto impacto',
    'Texto descriptivo Como Funciona' => '<p><b>BIHAIV</b> nace de comprender la necesidad de incrementar la colaboración de los distintos actores del ecosistema de Emprendimiento de Alto Impacto a través de la vinculación y articulación de los mismos, con el objetivo de alcanzar mayores niveles de madurez del mismo para el beneficio de los emprendedores que dependen de un entorno que les permite mejorar las posibilidades de éxito de sus proyectos.</p>
    <p>Por esta razón, para ser un miembro activo debe cumplirse un proceso de registro que evalúa los roles y capacidades que pueden desarrollar cada una de las organizaciones interesadas en ser parte de la plataforma y así garantizar el cumplimiento del principal objetivo.</p>
    <p>deaggregate(object) <b>BIHAIV</b>, debes seguir los siguientes pasos:</p>',
    'Paso 1' => 'Registra tus datos básicos de contacto.',
    'Paso 2' => 'Responde la evaluación de perfil, con ella podrás conocer qué tipo de actor representa la organización a la que perteneces. Esto tendrás que hacerlo antes de 5 días luego de recibir el correo de confirmación, de lo contrario el enlace caducará.',
    'Paso 3' => 'Luego de recibir tu respuesta al primer cuestionario podrás postularte para ser miembro de BIHAIV, contestando un segundo formulario con información más específica sobre tu organización. Esta información será enviada a miembros ya pertenecientes (uno por cada tipo de actor) que fungirán como evaluadores pares y determinarán la entrada a la red dependiendo del nivel de incidencia de tu organización en el Ecosistema de Emprendimiento de Alto Impacto.',
    'Paso 4' => 'Al finalizar estos dos pasos, recibirás un correo de bienvenida a la plataforma, o en su caso una retroalimentación para tu organización.',
    'Texto descriptivo paso 4' => '<p><b>El objetivo de la retroalimentación de BIHAIV</b>, en caso de que tu solicitud sea rechazada, queremos darte a conocer las áreas de oportunidad que tiene la organización solicitante a corto, mediano y largo plazo para ser considerado un actor del Ecosistema de Emprendimiento de Alto Impacto.</p>
    <p><b>Pero también queremos ofrecerte derecho a réplica</b> Si consideras que el resultado en la evaluación y la retroalimentación no corresponden a tu percepción, puedes solicitar una revisión, que automáticamente pasará a un nuevo proceso, donde el <b>Consejo de Expertos de BIHAIV</b> desarrollará un dictamen inapelable.</p>
    <p>Es posible hacer una nueva solicitud de ingreso luego de haber trabajado en la retroalimentación de la primera solicitud. Esto puede hacerse luego de seis meses de haberse postulado por primera vez.</p>',
    'Fin Como Funciona' => '<p>Una vez dentro de <b>BIHAIV</b>, lo que encontrarás es una red social, que a diferencia de la mayoría, esta no está enfocada sólo en crear relaciones, sino en llevar la colaboración a la vida real en el ecosistema de emprendimiento de alto impacto.</p>
    <p>Toda la información que genera cada <b>Actor</b> del ecosistema es analizada para crear una serie de <b>indicadores</b> que te ayudarán a ir mejorando e incrementando el nivel de <b>impacto</b> que tu organización tiene en el <b>Ecosistema de Emprendimiento de Alto Impacto</b>, a la vez que se genera <b>inteligencia social</b> que puede permitir a cada actor tomar decisiones basado en lo que se denomina <b>inteligencia de colmena</b>.</p>
    <p>Para todos los que conformamos <b>BIHAIV</b>, cada miembro representa una colaboración esencial, por lo que si consideras que la organización a la que representas está lista para unirse a esta gran red, te invitamos a dar el primer paso <a href="home/registro.php"><b>registrándote</b></a>.</p>',
    'label listaobjetivos'=>'Agrega objetivos de tu lista de objetivos', 
    'label listaeventos'=>'Agrega eventos  de tu lista de eventos',
    'Editar Información'=>'Editar Información',
    'label cargafoto'=>'Cargar foto de la iniciativa', 
    'label listainvitados'=>'Agrega invitados de tu lista de invitados', 
    'label addobjetivo'=>'Objetivo añadido a lista de objetivos', 
    'label errordescobjetivo'=>'La descripción del objetivo no puede quedar vacia', 
    'label error5maxObjetivo'=>'Selecciona como máximo 5 objetivos', 
    'label error3maxObjetivo'=>'Selecciona por lo menos 3 objetivos', 
    'label errorfechalfinal'=>'La fecha final no puede ser una fecha anterior a la actual.',
    'label errorfechainicial'=>'La fecha final no puede ser anterior a la fecha inicial, intentalo con un rango de fechas diferente', 
    'label guardarEvento'=>'Tu evento ha sido creado exitosamente',
    'Like correcto'=>'Like correcto',
    'Like incorrecto'=>'Like incorrecto', 
    'label errorGuardar'=>'Ah ocurrido un error intentando crear tu evento, intenta nuevamente.',
    'label errorActualizar'=>' Ah ocurrido un error intentando actualizar tu evento, intenta más tarde.',
    'label guardarDiscusion'=>'Tu discusión ha sido creada exitosamente',
    'label actualizarEvento'=>'Tu evento ha sido actualizado exitosamente',
    'label noNotificaciones'=>'Aún no tienes notificaciones, ¡Comienza a vincularte!',
    'label noSugerencias'=>'Aún no tienes sugerencias, ¡Comienza a vincularte',
    'label guardarComentario'=>'Tu comentario ha sido agregado con éxito',
    'label errorguardarComentario'=>'Ah ocurrido un error intentando agregar tu comentario, intenta más tarde.',
    'label addvisita'=>'Visita agregada con éxito',
    'label erroraddvisita'=>'Error al agregar la visita',
    'label graciascalificar'=>'Gracias por calificar este tema',
    'label errorcalificarIniciativa'=>'Ah ocurrido un error intentando calificar esta iniciativa, intentalo más tarde.',
    'label erorrgraciascalificar'=>'Ah ocurrido un error intentando calificar este tema, intentalo más tarde.',
    'label guardarIniciativa'=>'Tu iniciativa ha sido creada exitosamente',
    'label errorGuardarIniciativa'=>'Ah ocurrido un error intentando crear tu evento, intenta nuevamente.',
    'label procesoE'=>'Proceso exitoso',
    'label errorGenerico'=>'"Ah ocurrido un error, intenta más tarde.',
    'label descarga'=>'Descarga registrada con éxito',
    'label asistire'=>'Asistiré', 
    'label reportadoUsuario'=>'Ya has reportado a este usuario.',
    'label addRegistro'=>'Se agrego correctamente tu registro',
    'label evaluacionOrg'=>'Evaluación para la organización',
    'label resultaAproba'=>'Resultado de aprobación',
    'label novigentenotificacion'=>'Esta notificación ya no se encuentra vigente',
    'label eliminadaAportacion'=>'Tu aportacion ha sido eliminada exitosamente',
    'label erroreliminarAportacion'=>'Ah ocurrido un error intentando eliminar tu aportacion, intenta más tarde.',
    'label guardarAportacion'=>'Tu aportación ha sido creada exitosamente',
    'label telefono'=>'Telefono',

   //nivel graficas
   'label nivelImpacto'=>'Nivel de impacto en el ecosistema',
   'label nivelIncidencia'=>'Nivel de incidencia',
   'label nivelEnfoque'=>'Nivel de enfoque', 
   'label nivelActividad'=>'Nivel de actividad',
   'label nivelAportacion'=>'Nivel de aportaciones', 
   'label nivelColaboracion'=>'Nivel de colaboraciones',
   'label descargaReporte'=>'Descargar reporte de impacto',

   //module noticias
   'label notieneNoticia'=>'Este usuario aún no tiene noticias!',
   'label Noticias'=>'Notificaciones',
   'label vermas'=>'Ver más',
   'Mas recientes'=>'Más recientes',
   'Mas viejos'=>'Más viejos',
   'Mas recientes'=>'Más vistas',
   'Menos recientes'=>'Menos vistas',
   'Mas vistas'=>'Most seen',
   'Menos vistas'=>'Least seen',

   // Forum detail
   'Debes iniciar sesión para realizar esta acción'=>'Debes iniciar sesión para realizar esta acción',
   'Para calificar es necesario ingresar al menos una estrella'=>'Para calificar es necesario ingresar al menos una estrella',
   'Escribe una respuesta antes de enviarla'=>'Escribe una respuesta antes de enviarla'

   
);
